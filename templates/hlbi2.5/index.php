<?php
/**
 * @package    Joomla.Site
 * @copyright  Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/* The following line loads the MooTools JavaScript Library
JHtml::_('behavior.framework', true);
 */

/* The following line gets the application object for things like displaying the site name */
$app = JFactory::getApplication();

/* The following line gets the template style */
$templateStyle = $this->params->get('templateStyle');

/* The following lines check the component output */
$doc =& JFactory::getDocument();                     
$component_buffer  = $doc->getBuffer('component');

/* The following lines check if the user is logged in */
$user =& JFactory::getUser();
$is_logged_in = !$user->guest;

?>
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template.css" type="text/css" />
<?php if (@$templateStyle == 'gcs') { ?>
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template_style_gcs.css" type="text/css" />
<?php } ?>
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<!--[if IE 7]>
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template_ie7.css" type="text/css" />
<![endif]-->
        <link rel="shortcut icon" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/favicon.ico" type="image/x-icon" />  
    <jdoc:include type="head" />
  </head>
  <body>
    <div id="wrapper">
      <div id="header-outer">
        <div id="header-inner">
<?php if($this->countModules('search') > 0) { ?>
      <div id="search-inner">
      <div id="search-top">
        <jdoc:include type="modules" name="search" title="Search" style="xhtml" />
      </div>
          </div>
<?php } ?>
          <div id="header-top">
            <div id="header-left">
              <div id="logo">
                <a href="/" title="HLBI"><img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/head-logo.jpg" alt="<?php echo htmlspecialchars($app->getCfg('sitename')); ?>" width="219" height="62" border="0" /></a>
              </div>
              <div id="strapline">
                <jdoc:include type="modules" name="strapline" title="Header strapline" style="xhtml" />
              </div>
            </div>
            <div id="header-right">
        <div id="top-social">
                <jdoc:include type="modules" name="top_social" title="Top social" style="xhtml" />
              </div>

<?php 
/**
 *  if logged in, display user_login module (user name with profile/logout options)
 *     if in GCS, display panel module (link to public site) and groups portal link in portal-link
 *     else don't display these
 */
if ($is_logged_in) { ?>
        <div id="user-panel">
        You are logged in as
        <jdoc:include type="modules" name="user_login" title="User login" style="xhtml" />
        </div>
<?php if($this->countModules('login_panel') > 0) { ?>
             <div id="login-panel">
        <jdoc:include type="modules" name="login_panel" title="Login panel" style="xhtml" />
              </div>
            </div>
          </div>
          <div id="header-bottom-outer">
          <div id="header-bottom">
            <div id="user3">
              <jdoc:include type="modules" name="user3" title="Top navigation" style="xhtml" />
            </div>
            <div id="portal-link">
              <jdoc:include type="modules" name="portal_link" title="Portal link" style="xhtml" />
            </div>
<?php } else { ?>
            </div>
          </div>
          <div id="header-bottom-outer">
          <div id="header-bottom">
            <div id="user3" class="FullWidth">
              <jdoc:include type="modules" name="user3" title="Top navigation" style="xhtml" />
            </div>
<?php
  }
?>
            <div id="intranet-link">
        <jdoc:include type="modules" name="intranet_link" title="Intranet link" style="xhtml" />
            </div>
<?php
/**
 *  else (not logged in) display nothing in header-right, and display login button in user-login
 */
} else { ?>
            </div>
          </div>
          <div id="header-bottom-outer">
          <div id="header-bottom">
            <div id="user3">
              <jdoc:include type="modules" name="user3" title="Top navigation" style="xhtml" />
            </div>
            <div id="user-login">
              <jdoc:include type="modules" name="user_login" title="User login" style="xhtml" />
            </div>
<?php } ?>
          </div>
          </div>
        </div>
      </div>
      <div id="content-outer">
        <div id="content-inner">
          <!-- Main Content Area -->
<?php if($this->countModules('breadcrumb') > 0) { ?>
          <div id="breadcrumb"><!-- Breadcrumb trail -->
            <jdoc:include type="modules" name="breadcrumb" title="Breadcrumb trail" style="xhtml" />
          </div>
<?php
}

$top_count = $this->countModules('top1') + $this->countModules('top2') + $this->countModules('top3') + $this->countModules('top4');
$image_navigation_overlap_class = ($top_count > 0) ? ' class="no-overlap"' : '';
if($this->countModules('slideshow') > 0 || $this->countModules('banner_top') > 0) { ?>
          <div id="image-navigation"<?php echo $image_navigation_overlap_class; ?>><!-- slideshow and banner -->
            <div id="slideshow">
              <jdoc:include type="modules" name="slideshow" title="Slideshow" style="xhtml"/>
            </div>
            <div id="banner_top">
              <jdoc:include type="modules" name="banner_top" title="Banner top" style="xhtml"/>
            </div>
          </div>
<?php
}

if ($top_count > 0) { ?>
      <div id="top-outer">
        <div id="top-inner">
          <div id="top-upper">
        <div id="top1">
          <jdoc:include type="modules" name="top1" title="Top 1" style="xhtml"/>
        </div>
        <div id="top2">
          <jdoc:include type="modules" name="top2" title="Top 2" style="xhtml"/>
        </div>
        <div id="top3">
          <jdoc:include type="modules" name="top3" title="Top 3" style="xhtml"/>
        </div>
        <div id="top4">
          <jdoc:include type="modules" name="top4" title="Top 4" style="xhtml"/>
        </div>
          </div>
        </div>
      </div>
<?php
}

if($this->countModules('user1') > 0) { ?>
          <div id="user-custom"><!-- custom -->
            <jdoc:include type="modules" name="user1" title="User custom" style="xhtml"/>
          </div>
<?php
}

#echo "<!-- component_buffer=$component_buffer -->";
if($this->countModules('left') > 0 || true || $this->countModules('right') > 0) {
  if($this->countModules('left') == 0 && $this->countModules('right') == 0) {
    $middle_content_class = ' class="FullWidth"';
  } elseif($this->countModules('left') == 0) {
    $middle_content_class = ' class="NoLeft"';
  } elseif($this->countModules('right') == 0) {
    $middle_content_class = ' class="NoRight"';
  } else $middle_content_class = '';

?>
          <div id="middle-content"<?php echo $middle_content_class; ?>>

<?php
  if($this->countModules('left') > 0) { ?>
            <div id="left-content"><!-- left column -->
              <jdoc:include type="modules" name="left" title="left content" style="xhtml"/>
            </div>
<?php } ?>
          
            <div id="centre-content"><!-- main content -->
        <div id="component-content"><!-- Article and Content -->
          <jdoc:include type="component" />
          <jdoc:include type="message" />
        </div>

<?php
  if($this->countModules('landingpage') > 0) { ?>
        <div id="lower-content"><!-- lower main content -->
          <jdoc:include type="modules" name="landingpage" title="lower content" style="xhtml"/>
        </div>
<?php } ?>
<?php 
$user_upper_count = $this->countModules('user2') + $this->countModules('user4');
$user_lower_count = $this->countModules('user5') + $this->countModules('user6') + $this->countModules('user7');
if($user_upper_count + $user_lower_count > 0) { ?>
      <div id="user-outer">
        <div id ="user-inner">
<?php if($user_upper_count > 0) { ?>
          <div id="user-upper">
            <div id="user2">
              <jdoc:include type="modules" name="user2" title="User 2" style="xhtml"/>
            </div>
            <div id="user4">
              <jdoc:include type="modules" name="user4" title="User 4" style="xhtml"/>
            </div>
          </div>
<?php
  }
  if($user_lower_count > 0) { ?>
          <div id="user-lower">
            <div id="user5">
              <jdoc:include type="modules" name="user5" title="User 5" style="xhtml"/>
            </div>
            <div id="user6">
              <jdoc:include type="modules" name="user6" title="User 6" style="xhtml"/>
            </div>
            <div id="user7">
              <jdoc:include type="modules" name="user7" title="User 7" style="xhtml"/>
            </div>
          </div>
<?php } ?>
        </div>
      </div>
<?php } ?>
            </div>
            
<?php
  if($this->countModules('right') > 0) { ?>
          <div id="right-content"><!-- Right column -->
            <jdoc:include type="modules" name="right" title="Right column" style="xhtml" />
          </div>
<?php
  }
?>

    </div>
<?php 
}
?>

        </div>
      </div>

      <div id="footer-outer">
        <div id ="footer-inner">
      <div id="footer1">
      <jdoc:include type="modules" name="footer1" style="xhtml" />
      </div>
      <div id="footer2">
      <jdoc:include type="modules" name="footer2" style="xhtml" />
      </div>
      <div id="footer3">
      <jdoc:include type="modules" name="footer3" style="xhtml" />
      </div>
      <div id="footer4">
      <jdoc:include type="modules" name="footer4" style="xhtml" />
      </div>
        </div>
        <div id="copyright-inner">
          <div id="copyright">
            <jdoc:include type="modules" name="copyright" title="Copyright" />
          </div>
        </div>
      </div>
    </div>

  </body>
</html>