<?php
defined('_JEXEC') or die;
?>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('#k2ModuleBox<?php echo modFilterfork2Helper::$module->id; ?> .k2Pagination a').bind('click',function(event){
			var form = document.getElementById('filterfork2HiddenForm');
			form.action = event.target.href;
			form.submit();
			return false;
		});
  	});
</script>

<form method='post' id="filterfork2HiddenForm" action='<?php echo JRoute::_("index.php?option=com_k2&view=itemlist&task=filterfork2&Itemid=".modFilterfork2Helper::getItemId(modFilterfork2Helper::$module->id)."&mid=".modFilterfork2Helper::$module->id); ?>'> 
	<input type='hidden' value="<?php echo base64_encode(serialize($this->filter)); ?>" name="filterPagination" />
</form>

<div id="k2ModuleBox<?php echo modFilterfork2Helper::$module->id; ?>" class="k2Filterk2Filter<?php if($this->componentParams->get('pageclass_sfx')) print( ' '.$this->componentParams->get('pageclass_sfx')); ?>">
<?php if(count($this->items)){ ?>
<ul id="k2FilterContainer">

<?php foreach ((array)$this->items as $keyItem=>$item): 	?>
	<?php
		// Define a CSS class for the last container on each row
		if( (($keyItem+1)%($this->params->get('num_columns'))==0) || count($this->items)<$this->params->get('num_columns') )
			$lastContainer= ' itemContainerLast';
		else
			$lastContainer='';
	?>
	<li class="itemContainer<?php echo $lastContainer; ?>" <?php echo (count($this->items)==1) ? '' : ' style="width:'.number_format(100/$this->params->get('num_columns'), 1).'%;"'; ?>>		
	  <!-- Plugins: BeforeDisplay -->

	  <?php echo $item->event->BeforeDisplay; ?>

	  <!-- K2 Plugins: K2BeforeDisplay -->

	  <?php echo $item->event->K2BeforeDisplay; ?>

	  

	  <?php if($this->params->get('itemAuthorAvatar')): ?>

			<a class="k2Avatar moduleItemAuthorAvatar" rel="author" href="<?php echo $item->authorLink; ?>">

				<img src="<?php echo $item->authorAvatar; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->author); ?>" style="width:<?php echo $itemAuthorAvatarWidth; ?>px;height:auto;" />

			</a>

	  <?php endif; ?>

	  

	  <?php if($this->params->get('itemTitle')): ?>

	  <a class="moduleItemTitle" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>

	  <?php endif; ?>

	  

	  <?php if($this->params->get('itemAuthor')): ?>

	  <div class="moduleItemAuthor">

		  <?php echo K2HelperUtilities::writtenBy($item->authorGender); ?>

	

				<?php if(isset($item->authorLink)): ?>

				<a rel="author" title="<?php echo K2HelperUtilities::cleanHtml($item->author); ?>" href="<?php echo $item->authorLink; ?>"><?php echo $item->author; ?></a>

				<?php else: ?>

				<?php echo $item->author; ?>

				<?php endif; ?>

				

				<?php if($this->params->get('userDescription')): ?>

				<?php echo $item->authorDescription; ?>

				<?php endif; ?>

				

		</div>

		<?php endif; ?>

		

	  <!-- Plugins: AfterDisplayTitle -->

	  <?php echo $item->event->AfterDisplayTitle; ?>
	<?php if($item->params->get('itemRating')): ?>
	<!-- Item Rating -->
	<div class="catItemRatingBlock">
		<span><?php echo JText::_('K2_RATE_THIS_ITEM'); ?></span>
		<div class="itemRatingForm">
			<ul class="itemRatingList">
				<li class="itemCurrentRating" id="itemCurrentRating<?php echo $item->id; ?>" style="width:<?php echo $item->votingPercentage; ?>%;"></li>
				<li><a href="#" rel="<?php echo $item->id; ?>" title="<?php echo JText::_('K2_1_STAR_OUT_OF_5'); ?>" class="one-star">1</a></li>
				<li><a href="#" rel="<?php echo $item->id; ?>" title="<?php echo JText::_('K2_2_STARS_OUT_OF_5'); ?>" class="two-stars">2</a></li>
				<li><a href="#" rel="<?php echo $item->id; ?>" title="<?php echo JText::_('K2_3_STARS_OUT_OF_5'); ?>" class="three-stars">3</a></li>
				<li><a href="#" rel="<?php echo $item->id; ?>" title="<?php echo JText::_('K2_4_STARS_OUT_OF_5'); ?>" class="four-stars">4</a></li>
				<li><a href="#" rel="<?php echo $item->id; ?>" title="<?php echo JText::_('K2_5_STARS_OUT_OF_5'); ?>" class="five-stars">5</a></li>
			</ul>
			<div id="itemRatingLog<?php echo $item->id; ?>" class="itemRatingLog"><?php echo $item->numOfvotes; ?></div>
			<div class="clr"></div>
		</div>
		<div class="clr"></div>
	</div>
	<?php endif; ?>
	  <!-- K2 Plugins: K2AfterDisplayTitle -->

	  <?php echo $item->event->K2AfterDisplayTitle; ?>

	  <!-- Plugins: BeforeDisplayContent -->

	  <?php echo $item->event->BeforeDisplayContent; ?>

	  <!-- K2 Plugins: K2BeforeDisplayContent -->

	  <?php echo $item->event->K2BeforeDisplayContent; ?>

	  

	  <?php if($this->params->get('itemImage') || $this->params->get('itemIntroText')): ?>

	  <div class="moduleItemIntrotext">

		  <?php if($this->params->get('itemImage') && isset($item->image)): ?>

		  <a class="moduleItemImage" href="<?php echo $item->link; ?>" title="<?php echo JText::_('K2_CONTINUE_READING'); ?> &quot;<?php echo K2HelperUtilities::cleanHtml($item->title); ?>&quot;">

			<img src="<?php echo $item->image; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->title); ?>"/>

		  </a>

		  <?php endif; ?>

		<?php if($this->params->get('itemIntroText')): ?>

		<?php echo $item->text; ?>

		<?php endif; ?>

	  </div>

	  <?php endif; ?>

	  <div class="clr"></div>

	  

	  <?php if($this->params->get('itemExtraFields') && count($item->extra_fields)): ?>

	  <div class="moduleItemExtraFields testExtraFields">

		  <b><?php echo JText::_('K2_ADDITIONAL_INFO'); ?></b>

		  <ul>

			<?php foreach ($item->extra_fields as $extraField): ?>

					<?php if($extraField->value): ?>

					<li class="type<?php echo ucfirst($extraField->type); ?> group<?php echo $extraField->group; ?>">

						<span class="moduleItemExtraFieldsLabel"><?php echo $extraField->name; ?></span>

						<span class="moduleItemExtraFieldsValue"><?php echo $extraField->value; ?></span>

						<div class="clr"></div>

					</li>

					<?php endif; ?>

			<?php endforeach; ?>

		  </ul>

	  </div>

	  <?php endif; ?>

	  <div class="clr"></div>

	  

	  <?php if($this->params->get('itemVideo')): ?>

	  <div class="moduleItemVideo">

		<?php echo $item->video ; ?>

		<span class="moduleItemVideoCaption"><?php echo $item->video_caption ; ?></span>

		<span class="moduleItemVideoCredits"><?php echo $item->video_credits ; ?></span>

	  </div>

	  <?php endif; ?>

	  <div class="clr"></div>

	  

	  <!-- Plugins: AfterDisplayContent -->

	  <?php echo $item->event->AfterDisplayContent; ?>

	  <!-- K2 Plugins: K2AfterDisplayContent -->

	  <?php echo $item->event->K2AfterDisplayContent; ?>

	  <!-- K2 Field: itemDateCreated-->

	  <?php if($this->params->get('itemDateCreated')): ?>

	 	 <span class="moduleItemDateCreated"><?php echo JText::_('K2_WRITTEN_ON') ; ?> <?php echo JHtml::_('date', $item->created ,JText::_('K2_SEARCHER_DATE_FORMAT_LC2')); ?></span>

	  <?php endif; ?>

	  

	  <?php if($this->params->get('itemCategory')): ?>

		  <?php echo JText::_('K2_IN') ; ?> <a class="moduleItemCategory" href="<?php echo $item->categoryLink; ?>"><?php echo $item->categoryname; ?></a>

	  <?php endif; ?>

	 

	  <?php if($this->params->get('itemTags') && count($item->tags)>0): ?>

	  <div class="moduleItemTags">

		<b><?php echo JText::_('K2_TAGS'); ?>:</b>

		<?php foreach ($item->tags as $tag): ?>

		<a href="<?php echo $tag->link; ?>"><?php echo $tag->name; ?></a>

		<?php endforeach; ?>

	  </div>

	  <?php endif; ?>

	  

	  <?php if($this->params->get('itemAttachments') && count($item->attachments)): ?>

			<div class="moduleAttachments">

				<?php foreach ($item->attachments as $attachment): ?>

				<a title="<?php echo K2HelperUtilities::cleanHtml($attachment->titleAttribute); ?>" href="<?php echo $attachment->link; ?>"><?php echo $attachment->title; ?></a>

				<?php endforeach; ?>

			</div>

	  <?php endif; ?>

	 

			<?php if($this->params->get('itemCommentsCounter') && $this->componentParams->get('comments')): ?>		

				<?php if(!empty($item->event->K2CommentsCounter)): ?>

					<!-- K2 Plugins: K2CommentsCounter -->

					<?php echo $item->event->K2CommentsCounter; ?>

				<?php else: ?>

					<?php if($item->numOfComments>0): ?>

					<a class="moduleItemComments" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">

						<?php echo $item->numOfComments; ?> <?php if($item->numOfComments>1) echo JText::_('K2_COMMENTS'); else echo JText::_('K2_COMMENT'); ?>

					</a>

					<?php else: ?>

					<a class="moduleItemComments" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">

						<?php echo JText::_('K2_BE_THE_FIRST_TO_COMMENT'); ?>

					</a>

					<?php endif; ?>

				<?php endif; ?>

			<?php endif; ?>

			

			<?php if($this->params->get('itemHits')): ?>

			<span class="moduleItemHits">

				<?php echo JText::_('K2_READ'); ?> <?php echo $item->hits; ?> <?php echo JText::_('K2_TIMES'); ?>

			</span>

			<?php endif; ?>

			<?php if($this->params->get('itemReadMore') && $item->fulltext): ?>

			<a class="moduleItemReadMore" href="<?php echo $item->link; ?>">

				<?php echo JText::_('K2_READ_MORE'); ?>

			</a>

			<?php endif; ?>

			

	  <!-- Plugins: AfterDisplay -->

	  <?php echo $item->event->AfterDisplay; ?>

	  <!-- K2 Plugins: K2AfterDisplay -->

	  <?php echo $item->event->K2AfterDisplay; ?>

	  <div class="clr"></div>
	</li>
	<?php if((($keyItem+1)%($this->params->get('num_columns'))==0)) : ?>

	<div class="clr"></div>

	<?php endif; ?>

	

<?php endforeach; ?>
</ul>
	

<div class="clr"></div>

	 
   <!-- Pagination -->
        <?php if($this->pagination->getPagesLinks()): ?>
        <div class="k2Pagination">
            <?php echo $this->pagination->getPagesLinks(); ?>
            <div class="clr"></div>
            <?php echo $this->pagination->getPagesCounter(); ?>
        </div>
        <?php endif; ?>
 
<div class="clr"></div>

     

	<?php }else{ ?>
        <dl id="system-message">
            <dt class="notice"><?php echo JText::_('K2_NOTICE'); ?></dt>
            <dd class="notice message fade">
                <ul>
                    <li><?php echo JText::_('PLG_FILTERFORK2_NO_RESULTS'); ?></li>
                </ul>
            </dd>
        </dl>    
	<?php } ?>
</div>