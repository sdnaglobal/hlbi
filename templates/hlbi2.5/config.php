<?php

/**
 * array of navigation and base links for buttons in Member Firms section
 * each entry is array ($nav => $base_link)
 * index is button number (1 - 5)
 */
$_member_firms_button_array = array (
	1 => array ('Services' => '/?option=com_fabrik&view=form&formid=17&Itemid=596'),
	2 => array ('Button 2' => '/?option=com_hlbi_memberfirm&view=default&Itemid=632'),
	3 => array ('Button 3' => '/?option=com_hlbi_memberfirm&view=default&Itemid=632'),
	4 => array ('Button 4' => '/?option=com_hlbi_memberfirm&view=default&Itemid=632'),
	5 => array ('Button 5' => '/?option=com_hlbi_memberfirm&view=default&Itemid=632')
};

?>