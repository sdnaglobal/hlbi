<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

/**
 *
 * @package		Joomla
 * @subpackage	System
 */
class plgCRMeryUserCreate extends JPlugin
{

	function onAfterPersonSave(&$row)
	{
		if($row->status=='created') 
		{
			$config = JFactory::getConfig();
			$db		= JFactory::getDbo();

			$params	= JComponentHelper::getParams('com_users');
			$system	= $params->get('new_usertype', 2);

			$useractivation = $params->get('useractivation');
			$sendpassword = $params->get('sendpassword', 1);

			$data['groups'][] = $system;

			$data['name'] = $row->first_name.' '.$row->last_name;
			$data['email'] = $row->email;
			$data['username'] = $row->email;

			$user = JUser::getInstance();

			// Check if the user needs to activate their account.
			if (($useractivation == 1) || ($useractivation == 2)) {
				$data['activation'] = JApplication::getHash(JUserHelper::genRandomPassword());
				$data['block'] = 1;
			}

			// Bind the data.
			if (!$user->bind($data)) {
				$this->setError(JText::sprintf('COM_USERS_REGISTRATION_BIND_FAILED', $user->getError()));
				return false;
			}

			// Load the users plugin group.
			JPluginHelper::importPlugin('user');

			// Store the data.
			if (!$user->save()) {
				$this->setError(JText::sprintf('COM_USERS_REGISTRATION_SAVE_FAILED', $user->getError()));
				return false;
			}

			$person = JTable::getInstance('People');
			$person->load($row->id);
			$person->uid = $user->id;
			$person->store();

			// Compile the notification mail values.
			$data = $user->getProperties();
			$data['fromname']	= $config->get('fromname');
			$data['mailfrom']	= $config->get('mailfrom');
			$data['sitename']	= $config->get('sitename');
			$data['siteurl']	= JUri::root();

			// Prepare the data for the user object.
			$data['email']		= $data['email1'];
			$data['password']	= $data['password1'];



			// Handle account activation/confirmation emails.
			if ($useractivation == 2)
			{
				// Set the link to confirm the user email.
				$uri = JURI::getInstance();
				$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
				$data['activate'] = $base.JRoute::_('index.php?option=com_users&task=registration.activate&token='.$data['activation'], false);

				$emailSubject	= JText::sprintf(
					'COM_USERS_EMAIL_ACCOUNT_DETAILS',
					$data['name'],
					$data['sitename']
				);

				if ($sendpassword)
				{
					$emailBody = JText::sprintf(
						'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY',
						$data['name'],
						$data['sitename'],
						$data['siteurl'].'index.php?option=com_users&task=registration.activate&token='.$data['activation'],
						$data['siteurl'],
						$data['username'],
						$data['password_clear']
					);
				}
				else
				{
					$emailBody = JText::sprintf(
						'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY_NOPW',
						$data['name'],
						$data['sitename'],
						$data['siteurl'].'index.php?option=com_users&task=registration.activate&token='.$data['activation'],
						$data['siteurl'],
						$data['username']
					);
				}
			}
			elseif ($useractivation == 1)
			{
				// Set the link to activate the user account.
				$uri = JURI::getInstance();
				$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
				$data['activate'] = $base.JRoute::_('index.php?option=com_users&task=registration.activate&token='.$data['activation'], false);

				$emailSubject	= JText::sprintf(
					'COM_USERS_EMAIL_ACCOUNT_DETAILS',
					$data['name'],
					$data['sitename']
				);

				if ($sendpassword)
				{
					$emailBody = JText::sprintf(
						'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY',
						$data['name'],
						$data['sitename'],
						$data['siteurl'].'index.php?option=com_users&task=registration.activate&token='.$data['activation'],
						$data['siteurl'],
						$data['username'],
						$data['password_clear']
					);
				}
				else
				{
					$emailBody = JText::sprintf(
						'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY_NOPW',
						$data['name'],
						$data['sitename'],
						$data['siteurl'].'index.php?option=com_users&task=registration.activate&token='.$data['activation'],
						$data['siteurl'],
						$data['username']
					);
				}
			}
			else
			{

				$emailSubject	= JText::sprintf(
					'COM_USERS_EMAIL_ACCOUNT_DETAILS',
					$data['name'],
					$data['sitename']
				);

				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_BODY',
					$data['name'],
					$data['sitename'],
					$data['siteurl']
				);
			}

			// Send the registration email.
			$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody);	
			
		} else {
			$user = JUser::getInstance($row->uid);
			$user->name = $row->first_name.' '.$row->last_name;
			$user->email = $row->email;
			$user->save();
		}

	return true;

	}
}