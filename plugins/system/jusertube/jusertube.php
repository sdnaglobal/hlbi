<?php
/**
 * @package			JUserTube 
 * @version			6.1.0
 *
 * @author			Md. Afzal Hossain <afzal.csedu@gmail.com>
 * @link			http://www.srizon.com
 * @copyright		Copyright 2012 Md. Afzal Hossain All Rights Reserved
 * @license			http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die;

// import library dependencies
jimport('joomla.plugin.plugin');

class plgSystemJusertube extends JPlugin {
	function onAfterInitialise(){
		$GLOBALS['jqlibraryju'] = $this->params->get('library','srizonjq');
	}
	
	function onAfterRoute(){
		$app = JFactory::getApplication();
		if ($app->isAdmin()) return;
		$srz_format = JRequest::getVar('format');
		if($srz_format == 'feed') return;
		if($srz_format == 'raw') return;

		$GLOBALS['pageprotocol'] = $this->params->get('pageprotocol','http');
		$GLOBALS['srizon_showgplus'] = $this->params->get('showgplus','yes');
		$GLOBALS['srizon_showfblike'] = $this->params->get('showfblike','yes');
		$GLOBALS['srizon_showfbcomment'] = $this->params->get('showfbcomment','yes');
		$GLOBALS['srizon_fbcmntpage'] = $this->params->get('fbcmntpage','5');
		$GLOBALS['srizon_fbappid'] = $this->params->get('fbappid','');
		
		$GLOBALS['loadtb'] = $this->params->get('loadtb','yes');
		$GLOBALS['loadmag'] = $this->params->get('loadmag','yes');
		$GLOBALS['loadrespslider'] = $this->params->get('loadrespslider','yes');
		$GLOBALS['loadslidergrid'] = $this->params->get('loadslidergrid','yes');
	}
	
	function onAfterRender() {
		$app = JFactory::getApplication();
		$library = $this->params->get('library','srizonjq');
		$resloadpos = $this->params->get('resloadpos','last');
		if ($app->isAdmin()) return; 
		$srz_format = JRequest::getVar('format');
		if($srz_format == 'feed') return;
		if($srz_format == 'raw') return;
		
		$rspath = JURI::base(true) . '/media/jusertube/';
		
		$head_res='';
		
		if(!isset($GLOBALS['srizonmodernizr']) && $GLOBALS['loadrespslider'] == 'yes') {
			$GLOBALS['srizonmodernizr'] = 1;
			$head_res .= '<script type="text/javascript" src="'.$rspath.'modernizr.js"'.'></script>'."\n";
		}
		
		// load jquery +
		if (!isset($GLOBALS['srizonjq18']) and $library=='srizonjq') {
			$GLOBALS['srizonjq18'] = 1;
			$head_res .= '<script type="text/javascript" src="'.$rspath.'srizonjq1.8.min.js"'.'></script>'."\n";
		}
		if (!isset($GLOBALS['srizonjq']) and $library=='jquery') {
			$GLOBALS['srizonjq'] = 1;
			$head_res .= '<script type="text/javascript" src="'.$rspath.'jquery1.8.min.js"'.'></script>'."\n";
		}
		// load jquery -
		
		// load easing +
		if (!isset($GLOBALS['srizonjqeasing13']) && $GLOBALS['loadslidergrid']=='yes') {
			$GLOBALS['srizonjqeasing13'] = 1;
			if($library=='srizonjq'){
				$head_res .= '<script type="text/javascript" src="'.$rspath.'srizonjq.easing.1.3.min.js"'.'></script>'."\n";
			}
			else{
				$head_res .= '<script type="text/javascript" src="'.$rspath.'srizonjq.easing.1.3.min.jq.js"'.'></script>'."\n";
			}
		}
		// load easing -
		
		// load thickbox +		
		if (!isset($GLOBALS['srizontb']) && $GLOBALS['loadtb']=='yes') {
			$GLOBALS['srizontb'] = 1;
			$srizontbdeclaration = 'var srztb_pathToImage = "'.$rspath.'loadingAnimation.gif";';
			$head_res .= '<script type="text/javascript">'.$srizontbdeclaration.'</script>'."\n";
			$head_res .= '<link rel="stylesheet" href="'.$rspath.'srizontb.css" type="text/css" />'."\n";
			if($library=='srizonjq'){
				$head_res .= '<script type="text/javascript" src="'.$rspath.'srizontb2.js"'.'></script>'."\n";
			}
			else{
				$head_res .= '<script type="text/javascript" src="'.$rspath.'srizontb2.jq.js"'.'></script>'."\n";
			}
		}
		//load thickbox -
		
		// magnific popup start +
		if(!isset($GLOBALS['srizonmag']) && $GLOBALS['loadmag'] == 'yes'){
			$GLOBALS['srizonmag'] = 1;
			if($library=='srizonjq'){
				$head_res .= '<script type="text/javascript" src="'.$rspath.'mag-popup.js"'.'></script>'."\n";
			}
			else{
				$head_res .= '<script type="text/javascript" src="'.$rspath.'mag-popup.jq.js"'.'></script>'."\n";
			}
			
			$head_res .= '<link rel="stylesheet" href="'.$rspath.'mag-popup.css" type="text/css" />'."\n";
		}
		// magnific popup end -
		
		// elastislide start +
		if(!isset($GLOBALS['srizonrespslider']) && $GLOBALS['loadrespslider'] == 'yes'){
			$GLOBALS['srizonrespslider'] = 1;
			if($library=='srizonjq'){
				//$head_res .= '<script type="text/javascript" src="'.$rspath.'elasti.jqpp.js"'.'></script>'."\n";
				$head_res .= '<script type="text/javascript" src="'.$rspath.'elastislide.js"'.'></script>'."\n";
				
			}
			else{
				//$head_res .= '<script type="text/javascript" src="'.$rspath.'elasti.jqpp.jq.js"'.'></script>'."\n";
				$head_res .= '<script type="text/javascript" src="'.$rspath.'elastislide.jq.js"'.'></script>'."\n";
			}
		
			$head_res .= '<link rel="stylesheet" href="'.$rspath.'elastislide.css" type="text/css" />'."\n";
		}
		//elastislide end-
		if($GLOBALS['loadslidergrid'] == 'yes'){
			if($library=='srizonjq'){
				$head_res .= '<script type="text/javascript" src="'.$rspath.'jusertubeslider.js"'.'></script>'."\n";
			}
			else{
				$head_res .= '<script type="text/javascript" src="'.$rspath.'jusertubeslider.jq.js"'.'></script>'."\n";
			}
		}
		
		
		$body = JResponse::getBody();
		if($GLOBALS['srizon_showfbcomment'] == 'yes'){
			$headplusmeta = '<head>'."\n".'    <meta property="fb:app_id" content="'.$GLOBALS['srizon_fbappid'].'" />';
			$body = str_replace('<head>',$headplusmeta,$body);
		}
		if($resloadpos == 'last'){
			$headplusres = $head_res.'</head>';
			$body = str_replace('</head>',$headplusres,$body);
		}
		else{
			$pos = strpos($body,'</title>');
			$body = substr_replace($body,"\n".$head_res,$pos+8,0);
		}
		
		JResponse::setBody($body);
	}
}