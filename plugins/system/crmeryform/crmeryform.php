<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

/**
 * Joomla! System Remember Me Plugin
 *
 * @package		Joomla
 * @subpackage	System
 */
class plgSystemCrmeryform extends JPlugin
{

	function onBeforeRender()
	{
		$config = JFactory::getConfig();
		if ( $config->get('captcha') ){
			JPluginHelper::importPlugin('captcha');
			$dispatcher = JDispatcher::getInstance();
			$dispatcher->trigger('onInit','dynamic_recaptcha_1');
		}

		return true;
	}

	function onAfterRender()
	{

		$mainframe = JFactory::getApplication();
		if(!$mainframe->isAdmin()) {
			$buffer = JResponse::getBody();

			$regex  = '#\[crmeryform([0-9]*)\]#';
			$buffer = preg_replace_callback($regex, array('plgSystemCrmeryform', 'loadForm'), $buffer);

			JResponse::setBody($buffer);

		}

		return true;
	}


	function loadForm(&$matches)
	{
		$db = JFactory::getDBO();

		$query = $db->getQuery(true);
		$query->select('html')->from('#__crmery_formwizard')->where('id='.$matches[1]);
		$db->setQuery($query);
		$html = $db->loadResult();

		return $html;
	}
}