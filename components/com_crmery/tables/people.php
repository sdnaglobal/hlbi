<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class TablePeople extends JTable
{
    var $id 						= null;
    var $uid                        = null;
    var $owner_id 					= null;
	var $first_name					= null;
	var $last_name	 				= null;
	var $company_id					= null;
	var $position					= null;
	var $phone						= null;
	var $email						= null;
	var $source						= null;
	var $assignee_id 				= null;
	var $fax						= null;
	var $website					= null;
	var $facebook_url				= null;
	var $twitter_user				= null;
	var $linkedin_url				= null;
	var $created					= null;
	var $status_id					= null;
	var $type						= null;
	var $info						= null;
    var $modified                   = null;
	var $home_address_1             = null;
    var $home_address_2             = null;
    var $home_city                  = null;
    var $home_state                 = null;
    var $home_zip                   = null;
    var $home_country               = null;
    var $work_address_1             = null;
    var $work_address_2             = null;
    var $work_city                  = null;
    var $work_state                 = null;
    var $work_zip                   = null;
    var $work_country               = null;
    var $assignment_note            = null;
    var $mobile_phone               = null;
    var $home_email                 = null;
    var $other_email                = null;
    var $home_phone                 = null;
    var $published                  = null;
	var $message                  = null;

 
    /**
     * Constructor
     *
     * @param object Database connector object
     */
    function __construct( &$db ) {
        parent::__construct('#__crmery_people', 'id', $db);
    }
}