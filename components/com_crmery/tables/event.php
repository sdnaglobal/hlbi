<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class TableEvent extends JTable
{
    var $id 			    = null;
    var $owner_id 		    = null;
	var $name 			    = null;
	var $description 	    = null;
	var $created		    = null;
	var $category		    = null;
	var $type			    = null;
	var $assignee_id	    = null;
	var $due_date		    = null;
	var $repeat			    = null;
    var $repeat_end         = null;
	var $start_time		    = null;
	var $end_time		    = null;
	var $all_day		    = null;
	var $modified		    = null;
    var $completed          = null;
    var $actual_close       = null;
    var $excludes           = null;
    var $parent_id          = null;
    var $published          = null;
 
    /**
     * Constructor
     *
     * @param object Database connector object
     */
    function __construct( &$db ) {
        parent::__construct('#__crmery_events', 'id', $db);
    }
}