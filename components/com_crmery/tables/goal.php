<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class TableGoal extends JTable
{
    var $id                     = null;
    var $owner_id               = null;
    var $name                   = null;
    var $goal_type              = null;
    var $assigned_type          = null;
    var $assigned_id            = null;
    var $stage_id               = null;
    var $category_id            = null;
    var $amount                 = null;
    var $leaderboard            = null;
    var $start_date             = null;
    var $end_date               = null;
    var $created                = null;
    var $published              = null;
    var $goal_date              = null;
    
    
    /**
     * Constructor
     *
     * @param object Database connector object
     */
    function __construct( &$db ) {
        parent::__construct('#__crmery_goals', 'id', $db);
    }
}