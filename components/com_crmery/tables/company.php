<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class TableCompany extends JTable
{
    var $id 					= null;
    var $owner_id 				= null;
	var $name 					= null;
	var $description 			= null;
	var $website				= null;
 	var $created				= null;
	var $notes					= null;
	var $phone				   	= null;
	var $modified				= null;
    var $address_1              = null;
    var $address_2              = null;
    var $address_city           = null;
    var $address_state          = null;
    var $address_zip            = null;
    var $address_country        = null;
    var $published              = null;
	
    /**
     * Constructor
     *
     * @param object Database connector object
     */
    function __construct( &$db ) {
        parent::__construct('#__crmery_companies', 'id', $db);
    }
}