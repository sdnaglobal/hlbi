<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class TableNote extends JTable
{
    var $id 					= null;
    var $owner_id               = null;
	var $deal_id				= null;
	var $person_id				= null;
	var $name 					= null;
	var $note					= null;
	var $category_id			= null;
	var $company_id				= null;
	var $created				= null;
	var $modified				= null;
    var $published              = null;
		
    /**
     * Constructor
     *
     * @param object Database connector object
     */
    function __construct( &$db ) {
        parent::__construct('#__crmery_notes', 'id', $db);
    }
}