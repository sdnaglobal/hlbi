<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class TableCUser extends JTable
{
    var $id                     = null;
    var $uid                    = null;
    var $role_type              = null;
    var $admin                  = null;
    var $exports                = null;
    var $can_delete             = null;
    var $team_id                = null;
    var $first_name             = null;
    var $last_name              = null;
    var $modified               = null;
    var $created                = null;
    var $time_zone              = null;
    var $date_format            = null;
    var $time_format            = null;
    var $daily_agenda           = null;
    var $morning_coffe          = null;
    var $weekly_team_report     = null;
    var $weekly_personal_report = null;
    var $reminder_notifications = null;
    var $sms_number             = null;
    var $text_messages          = null;
    var $fullscreen             = null;
    var $home_page_chart        = null;
    
    /**
     * Constructor
     *
     * @param object Database connector object
     */
    function __construct( &$db ) {
        parent::__construct('#__crmery_users', 'id', $db);
    }
}