<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class TableDeal extends JTable
{
    var $id 					= null;
	var $name 					= null;
	var $summary	 			= null;
	var $company_id				= null;
	var $amount					= null;
	var $stage_id				= null;
	var $source_id				= null;
	var $probability			= null;
	var $status_id				= null;
	var $expected_close 		= null;
 	var $created				= null;
	var $notes					= null;
	var $category				= null;
	var $owner_id				= null;
	var $modified				= null;
    var $archived               = null;
    var $actual_close           = null;
	var $last_viewed			= null;
	var $published              = null;

    /**
     * Constructor
     *
     * @param object Database connector object
     */
    function __construct( &$db ) {
        parent::__construct('#__crmery_deals', 'id', $db);
    }
}