<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerCheckLogin extends JController
{

	function execute(){

		$return = array();

		$userId = CrmeryHelperUsers::getLoggedInUser()->id;
		if ( $userId > 0 ){
			$loggedIn = true;
		}else{
			$loggedIn = false;
		}
		$return['loggedIn'] = $loggedIn;
		$return['redirect'] = CrmeryHelperConfig::getLoginRedirect();

		echo json_encode($return);

	}

}