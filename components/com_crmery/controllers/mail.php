<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerMail extends JController
{
	//Constructor
	function __construct(){
		parent::__construct();
		JRequest::setVar('format','raw');
	}

	/**
     *  Remove emails
     */
    function removeEmail(){
        $model =& JModel::getInstance('Mail','CrmeryModel');
        $model->removeEmail();
    }

    function saveEmail(){
    	$model =& Jmodel::getInstance('Mail','CrmeryModel');
    	$model->saveEmail();
    }

	function brewCoffee()
	{
		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('u.id, u.first_name, u.last_name, user.email');
		$query->from('#__crmery_users AS u');
		$query->leftJoin('#__users AS user ON u.uid = user.id');
		$query->where('u.morning_coffee=1');
		$query->where("u.published=1");

		$db->setQuery($query);
		$people = $db->loadObjectList();

		$p = count($people);
		for($i=0;$i<$p;$i++) {
			$person = $people[$i];
			$layout = $this->loadStats($person->id);
			$layout->assignRef('user',$person);
			$this->sendMail($layout, $person->email);
		}

	}	

	function mailDailyAgenda()
	{

	}

	function mailWeeklyTeamUSE()
	{

	}

	function mailWeeklyPersonalUSE() 
	{

	}

	function loadStats($person_id)
	{

		//Set the user ID for helpers
		JRequest::setVar('user_id',$person_id);

		//Load stats model
		$statsModel =& JModel::getInstance('Stats','CrmeryModel');
		$statsModel->set('person_id',$person_id);

		//Load view
		$coffeeView = CrmeryHelperView::getView('emails','coffee.report');

		//Get Total Deals Amount
		$activeDeals = $statsModel->getActiveDealsAmount();
		$coffeeView->assignRef('totalDealsAmount',$activeDeals);
		
		//Get Stage Details
		$stages = $statsModel->getStages();
		$coffeeView->assignRef('stages',$stages);
		
		//Get Number of Converted Leads
		$leads = $statsModel->getLeads();
		$coffeeView->assignRef('numConvertedLeads',$leads['contact']);
		
		//Get Number of New Leads
		$coffeeView->assignRef('numNewLeads',$leads['lead']);
		
		//Get Note Details
		$notes = $statsModel->getNotes();
		$coffeeView->assignRef('notes',$notes);
		
		//Get ToDo Details
		$todos = $statsModel->getTodos();
		$coffeeView->assignRef('todos',$todos);
		
		//Get Deal Activity
		$dealActivity = $statsModel->getDealActivity();
		$coffeeView->assignRef('dealActivity',$dealActivity);
		
		//Get Lead Activity
		$coffeeView->assignRef('leadActivity',$leadActivity);
		
		//Get Contact Activity
		$coffeeView->assignRef('contactActivity',$contactActivity);

		return $coffeeView;	
	}

	function sendMail($layout,$recipient)
	{	
		$mailer =& JFactory::getMailer();
		$mailer->isHTML(true);
		$mailer->Encoding = 'base64';
		
		$config =& JFactory::getConfig();
		$sender = array( 
    				$config->getValue( 'config.mailfrom' ),
   					$config->getValue( 'config.fromname' ) 
   				);
 
		$mailer->setSender($sender);		
		$mailer->addRecipient($recipient);

		$mailer->setSubject(CRMText::_('COM_CRMERY_COFFEE_REPORT_SUBJECT').' '.CrmeryHelperDate::formatDate(date('Y-m-d')));
		
		ob_start();
		
		$layout->display();
		$body = ob_get_contents();
		
		ob_end_clean();

		$mailer->setBody($body);
		$send =& $mailer->Send();
		if ( $send !== true ) {
			echo 'Error sending email: ' . $send->message;
		}
	}
}