<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerPeople extends JController
{
		//main display function
		function display(){
			
			JRequest::setVar('view','people');
			JRequest::setVar('layout','default');
			
			parent::display();
			
		}
	
		function update(){
			
			//load model
			$model = $this->getModel('tasks');
			//get tasks
			$tasks = $model->getTasks();
			
			//return json list of tasks
			echo json_encode($tasks);
			
		}

		function save(){
		    $model = & JModel::getInstance('people','CrmeryModel');
		    if ( $id = $model->store() ){
				
				$newPersonLink = JRoute::_('index.php?option=com_crmery&view=people&id='.$id);
	    		$this->setRedirect($newPersonLink);
		    } else {
		    	$link = JRoute::_('index.php?option=com_crmery&view=people');
	      		$msg = JText::_('COM_CRMERY_ERROR_SAVING'); 	
	    		$this->setRedirect($link, $msg);
		    }
		}
}
	