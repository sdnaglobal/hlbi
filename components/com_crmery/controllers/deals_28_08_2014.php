<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.controller');

class CrmeryControllerDeals extends JController
{
 
	public function __construct(){
		parent::__construct();
		 
		//load required js libs
		$document = & JFactory::getDocument();
		$document->addScript( JURI::base().'components/com_crmery/media/js/deal_manager.js' );
		
	}

	//main display function
	function display(){
		
		JRequest::setVar('view','deals');
		JRequest::setVar('layout','default');
		
		parent::display();
		
	}

	//add deal function
	function add(){
		
		JRequest::setVar('view','deals');
		JRequest::setVar('layout','add');
		
		parent::display();
		
	}
	
	function save(){
	    $model = & JModel::getInstance('deal','CrmeryModel');
	    if ( $id = $model->store() ){
			$newDealLink = JRoute::_('index.php?option=com_crmery&view=deals&id='.$id);
    		$this->setRedirect($newDealLink);
	    } else {
	    	$link = JRoute::_('index.php?option=com_crmery&view=deals');
      		$msg = JText::_('COM_CRMERY_ERROR_SAVING'); 	
    		$this->setRedirect($link, $msg);
	    }
	}

	

}