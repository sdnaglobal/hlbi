<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerEvents extends JController
{
	
	/**
	 * Save Events
	 */
	function save(){
	    $model = & JModel::getInstance('event','CrmeryModel');
		$link = 'index.php?option=com_crmery&view=dashboard';
		
		//TODO Error handling
	    if ($model->store()) {
			$msg = CRMText::_('COM_CRMERY_SUCCESSFULLY_SAVED'); 	
    		$this->setRedirect($link, $msg);
	    } else {
      		$msg = CRMText::_('COM_CRMERY_ERROR_SAVING'); 	
    		$this->setRedirect($link, $msg);
	    }
		
	}

	/**
	 * Remove Events
	 */
	function remove(){
		$model = & JModel::getInstance('event','CrmeryModel');
		if($model->removeEvent()) {
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('success'=>false));
		}
	}

	/**
	 * Mark events as completed
	 */
	function markComplete(){
		$model = & JModel::getInstance('event','CrmeryModel');
		if($model->markComplete()){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('success'=>false));
		}
	}

	/**
	 * Mark events incomplete
	 */
	function markIncomplete(){
		$model = & JModel::getInstance('event','CrmeryModel');
		$model->markIncomplete();	
	}

	/**
	 * Postpone events
	 */
	function postponeEvent(){
		$model =& JModel::getInstance('event','CrmeryModel');
		$model->postponeEvent();
	}

}
	