<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerGraphs extends JController
{
        /**
         * Retrieve graph data and pass back as json for displaying charts
         * @param $_POST['filter'] type to filter by
         * @param $_POST['id] id of type to filter by
         * @return $json json object
         */
        function getGraphData(){
            
            //get graph data from model
            $model =& JModel::getInstance('graphs','CrmeryModel');
            $type = JRequest::getVar('filter');
            if ( $type == 'company' ){
                $graph_data = $model->getGraphData('company');
            }else{
                $graph_data = $model->getGraphData($type,JRequest::getVar('id'));
            }
            
            //return data
            echo json_encode($graph_data);
            
        }
        
}
    