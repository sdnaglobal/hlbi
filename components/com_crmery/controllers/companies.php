<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerCompanies extends JController
{
		//main display function
		function display(){
			
			JRequest::setVar('view','companies');
			JRequest::setVar('layout','default');
			
			parent::display();
			
		}
	
		function update(){
			
			//load model
			$model = $this->getModel('tasks');
			//get tasks
			$tasks = $model->getTasks();
			
			//return json list of tasks
			echo json_encode($tasks);
			
		}
		
		function getList(){
			
			//load model
			$model = $this->getModel('company');
			//get companies
			$list = $model->getCompanies();
			
			echo json_encode($list);
			
		}

		function save(){
		    $model = & JModel::getInstance('company','CrmeryModel');
		    if ( $row = $model->store() ){
				$newCompanyLink = JRoute::_('index.php?option=com_crmery&view=companies&id='.$row->id);
	    		$this->setRedirect($newCompanyLink);
		    } else {
		    	$session = JFactory::getSession();
		    	$link = JRoute::_('index.php?option=com_crmery&view=companies&layout=edit');
	      		$msg = $session->get("Company.error") ? $session->get("Company.error_message") : JText::_('COM_CRMERY_ERROR_SAVING'); 	
	    		$this->setRedirect($link, $msg);
		    }
		}
}
	