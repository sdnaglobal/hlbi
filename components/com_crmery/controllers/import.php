<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerImport extends JController
{
		//Constructor
		function __construct(){
			parent::__construct();
		}
	
		//main display function
		function display($tpl=null){
			//display
			parent::display($tpl);
		}

		/**
		 * Import CSV Files into database
		 */
		function import(){
			$success = false;
			$data = JRequest::get('post');
			$session = JFactory::getSession();
			if ( is_array($data) && count($data) > 0 ){
				$import_type = $data['import_type'];
				unset($data['import_type']);
				switch($import_type){
                case "companies":
                    $import_model = "company";
                    break;
                case "deals":
                    $import_model = "deal";
                    break;
                case "people":
                    $import_model = "people";  
                    break;
            	}
            	if ( isset($import_model) ) {		
            		$model =& JModel::getInstance("Import","CrmeryModel");
					$success = $model->importCSVData($data['import_id'],$import_model);
				}
			}
			if ( $success == "redirect" ){
				$app = JFactory::getApplication();
				$app->redirect("index.php?option=com_crmery&controller=import&task=batch");
			}

			$type = $session->get("batch_type");
			switch ( $type ){
				case "company":
					$typeView = "companies";
				break;
				case "deal":
					$typeView = "deals";
				break;
				case "people":
					$typeView = "people";
				break;
			}
			$view = $type ? $typeView : "";
			$view = $view == "" ? $view : "&view=".$view;

			if ( $success == true ){
				$msg = CRMText::_('COM_CRMERY_IMPORT_WAS_SUCCESSFUL');
				$this->setRedirect(JRoute::_('index.php?option=com_crmery'.$view),$msg);
			}else{
				$msg = CRMText::_('COM_CRMERY_ERROR_IMPORTING');
				$this->setRedirect(JRoute::_('index.php?option=com_crmery'.$view),$msg);
			}
		}

		function batch(){

			$session = JFactory::getSession();
			$success = true;
			$app = JFactory::getApplication();

			if (JFile::exists(JPATH_BASE."/tmp/crmery_import.php")){
				include_once(JPATH_BASE."/tmp/crmery_import.php");
			}

			if ( isset($batchData) && count($batchData) > 0 ){
				$model =& JModel::getInstance("Import","CrmeryModel");
				$success = $model->importCSVData($batchData,$batchType);
			}

			if ( $success == "redirect" && JFile::exists(JPATH_BASE."/tmp/crmery_import.php") ){
				$app->redirect("index.php?option=com_crmery&controller=import&task=batch");
			}

			$type = $session->get("batch_type");
			switch ( $type ){
				case "company":
					$typeView = "companies";
				break;
				case "deal":
					$typeView = "deals";
				break;
				case "people":
					$typeView = "people";
				break;
			}
			$view = $type ? $typeView : "";

			jimport("joomla.filesystem.file");
			if (JFile::exists(JPATH_BASE."/tmp/crmery_import.php")){
				JFile::delete(JPATH_BASE."/tmp/crmery_import.php");
			}

			if ( $success == true ){
				$msg = CRMText::_('COM_CRMERY_IMPORT_WAS_SUCCESSFUL');
				$app->redirect('index.php?option=com_crmery&view='.$view);
			}else{
				$msg = CRMText::_('COM_CRMERY_ERROR_IMPORTING');
				$app->redirect('index.php?option=com_crmery&view='.$view);
			}

			$view = $view == "" ? $view : "&view=".$view;
			$app->redirect('index.php?option=com_crmery'.$view);

		}


}