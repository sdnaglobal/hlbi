<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerCalendar extends JController
{
		//main display function
		function display(){
			
			JRequest::setVar('view','calendar');
			JRequest::setVar('layout','default');
			
			parent::display();
			
		}
	
		function update(){
			
			//load model
			$model = $this->getModel('tasks');
			//get tasks
			$tasks = $model->getTasks();
			
			//return json list of tasks
			echo json_encode($tasks);
			
		}
}
	