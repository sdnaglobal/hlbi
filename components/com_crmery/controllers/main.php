<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerMain extends JController
{

		function trash(){
			$item_id = JRequest::getVar('item_id');
			$item_type = JRequest::getVar('item_type');

			switch($item_type){
				case "deals":
					$table = "deal";
				break;
				case "events":
					$table = "event";
				break;
				case "companies":
					$table = "company";
				break;
				case "documents":
					$table = "document";
				break;
				case "conversations":
					$table = "conversation";
				break;
				case "goals":
					$table = "goal";
				break;
				case "notes":
					$table = "note";
				break;
				case "people":
					$table = "people";
				break;
				case "reports":
					$table = "report";
				break;
				default:
					$table = $item_type;
				break;
			}

			//Load Tables
            $oldRows = array();

            if ( is_array($item_id) ){
            	foreach($item_id as $id){
            		$oldRows[$id] = JTable::getInstance($table,'Table');
            		$oldRows[$id]->load($id);
            	}
            }else{
            	$oldRows = JTable::getInstance($table,'Table');
        		$oldRows->load($item_id);
            }
			
			//ADD TO MODELS * trash model *
			$db =& JFactory::getDBO();
			$query = $db->getQuery(true);
			$query->update("#__crmery_".$item_type)->set("published=-1");
				if ( is_array($item_id) ){
					$query->where("id IN(".implode(',',$item_id).")");
				}else{
					$query->where("id=".$item_id);
				}
			$db->setQuery($query);
			if ( $db->query() ){
				$data['success'] = true;
			}else{
				$data['success'] = false;
				$data['error_msg'] = $db->errorMsg;
			}

			$redirect = JRequest::getVar('page_redirect');
			if ( $redirect ){
				$msg = ( $data['success'] ) ? CRMText::_('COM_CRMERY_SUCCESSULLY_REMOVED_ITEM') : CRMText::_('COM_CRMERY_ERROR_REMOVING_ITEM');
				$this->setRedirect(JRoute::_('index.php?option=com_crmery&view='.$redirect),$msg);
			}else{
				echo json_encode($data);
			}

			$rows = array();

			if ( is_array($item_id) ){
				foreach($item_id as $id){
					$rows[$id] = JTable::getInstance($table,'Table');
					$rows[$id]->load($id);
				}
			}else{
				$rows = JTable::getInstance($table,'Table');
				$rows->load($item_id);
			}

            $status = "deleted";

            if ( is_array($item_id) ){
            	foreach ($item_id as $id){
            		$oldSaveRow = $oldRows[$id];
            		$newSaveRow = $rows[$id];
					CrmeryHelperActivity::saveActivity($oldSaveRow, $newSaveRow, $table, $status);            		
            	}
            }else{
            	CrmeryHelperActivity::saveActivity($oldRows, $rows,$table, $status);
            }


		}
		
		/*
		 * main method to save to tables
		 */
		function save(){

		    $model = & JModel::getInstance(JRequest::getVar('model'),'CrmeryModel');

			//if we are requesting a return redirect set up redirect link
			if ( JRequest::getVar('return') ) {
    			$link = JRoute::_('index.php?option=com_crmery&view='.JRequest::getVar('return'));
			}
			
		    if ( $db_id = $model->store() ) {

				$msg = CRMText::_('COM_CRMERY_SUCCESSFULLY_SAVED');

				//redirect if set else return json
				if ( JRequest::getVar('return') ) {  	

	    			$this->setRedirect($link, $msg);

				}else{
					
					//if deal information is being edited
					if ( JRequest::getVar('model') == 'deal' ) {
						$model = JModel::getInstance('deal','CrmeryModel');
						$id = JRequest::getVar('id') ? JRequest::getVar('id') : $db_id;
						$model->set('_id',$id);
						$model->set('user_override','all');
						$deal = $model->getDeals();
						$return = $deal[0];
					}
					
					//if people information is being edited
					if ( JRequest::getVar('model') == 'people' ) {
						$model = JModel::getInstance('people','CrmeryModel');
						$person = $model->getPerson(JRequest::getVar('id'));
						$return = $person[0];
					}
					
					//if conversation information is being edited
					if ( JRequest::getVar('model') == 'conversation') {
						$model = JModel::getInstance('conversation','CrmeryModel');
						$db =& JFactory::getDBO();
						$conversation = $model->getConversation($db_id);
						$return = $conversation[0];
					}
					
					//if note information is being edited
					if ( JRequest::getVar('model') == 'note' ){
						$model = JModel::getInstance('note','CrmeryModel');
						$db =& JFactory::getDBO();
						$note = $model->getNote($db_id);
						$return = $note[0];
					}
					
					//if event information is being edited
					if ( JRequest::getVar('model') == 'event' ) {

						//get model
						$model = JModel::getInstance('event','CrmeryModel');
						$db =& JFactory::getDBO();
						
						//determine whether we are inserting a new entry or editing an entry
						$event_id = $db_id;

						//get and return event
						$return = $model->getEvent($event_id);

					}

					//document info
					if ( JRequest::getVar("model") == "document" ){
						echo json_encode($model->getDocument($db_id));
					}
					
					//return reqeusts
					if ( isset($return) ){
						echo json_encode($return);
					}
					
				}

		    } else {

	      		$msg = CRMText::_('COM_CRMERY_ERROR_SAVING');

				//redirect if set else return json info
	    		if ( JRequest::getVar('return') ) {  	
	    			
	    			$this->setRedirect($link, $msg);
	    			
				}else{

					$return = JRequest::get( 'post' );
					echo json_encode($return);

				}

		    }
			
		}

		function addPersonToCompany(){
			$person_id = JRequest::getVar("person_id");
			$company_id = JRequest::getVar('company_id');
			$person_name = JRequest::getVar('person_name');
			if ( $person_id > 0 ){
				$data = array('id'=>$person_id,'company_id'=>$company_id);
			}else{
				$name_string = explode(" ",$person_name);
				$first_name = array_key_exists(0,$name_string) ? $name_string[0] : "";
				$last_name = array_key_exists(1,$name_string) ? $name_string[1] : "";
				if ( $first_name == "" && $last_name == "" ){
					echo json_encode(array('success'=>false));
					exit();
				}
				$data = array('first_name'=>$first_name,'last_name'=>$last_name,'company_id'=>$company_id);
			}
			$model =& JModel::getInstance('people','CrmeryModel');
			if ( $model->store($data) ){
				$success = true;
			}else{
				$success = false;
			}
			echo json_encode(array('success'=>$success));
		}
		
		/*
		 * Method to save CF tables via AJAX
		 */
		function saveCf(){

			$return = array();
			
			//get post data
			$data = JRequest::get('post');
			//get db Object
			$db = & JFactory::getDBO();
			$query = $db->getQuery(true);
			$table = $data['table'];
			$loc = $data['loc'];
			unset($data['table']);
			unset($data['loc']);

			//write to tables if there is no association already in cf tables
			$query->select('* FROM #__crmery_'.$table.'_cf');
			
			//loop to see if we have matches in database
			$overrides = array('tmpl');
			foreach ( $data as $key => $value ){
				if ( !in_array($key,$overrides) ){
					$query->where($key . " = '" . $value . "'");
				}
			}
			
			$db->setQuery($query);
			$results = $db->loadAssocList();

			//determine if we found any results
			if ( count($results) == 0 ) {
				
				$query->insert('#__crmery_'.$table.'_cf');
				//timestamp
				$data['created'] = date('Y-m-d H:i:s');
				$date = CrmeryHelperDate::formatDBDate(date('Y-m-d H:i:s'));
				//loop through data to get query string
				foreach ( $data as $key => $value ) {
					if ( !in_array($key,$overrides) ){ 
						// determine key and key values
						$query->set($key . " = '" . $value . "'");
					}
				}
				//execute query			
				$db->setQuery($query);
				$db->query();

				//if return data requested
				if ( $table == 'people' ){
					
					//determine which page we want are wanting to send information back to
					if ( $loc == 'deal' ) {
						$model = JModel::getInstance('people','CrmeryModel');
						$return = $model->getPerson(array_key_exists('person_id',$data) ? $data['person_id']:"");
						$return = $return[0];
					}
					
					if ( $loc == 'person' ) {
						$model = JModel::getInstance('deal','CrmeryModel');
						$return = $model->getDeals(array_key_exists('deal_id',$data) ? $data['deal_id']:"");
						$return = $return[0];
					}
				}
			
			}else{
				
				$return = array('error'=>true);
				
			}
			
			//return json data
			echo json_encode($return);
			
		}
		
		//ajax function to retrieve all people for autocomplete searches
		function getPeople(){
			
			//open model
			$model = & JModel::getInstance('people','CrmeryModel');
			//retrieve all people
			$people = $model->getPeopleList();
			//return results as json object
			echo json_encode($people);
			
		}
		
		//ajax function to retrieve all people for autocomplete searches
		function getDeals(){
			
			//open model
			$model = & JModel::getInstance('deal','CrmeryModel');
			//retrieve all people
			$deals = $model->getDealList();
			//return results as json object
			echo json_encode($deals);
			
		}
        
        //ajax function to retrieve all event & task associations for autocomplete searches
        function getTaskAssociations(){
            
            //open model
			$view = $_REQUEST['page'];
			
			if($view == 'people')
			{
				
				$model = & JModel::getInstance('people','CrmeryModel');
				//retrieve all people
				$people = $model->getPeopleList();
				//echo '<pre>';print_r($people);
				//die();
				if ( count($people) ){
					foreach ( $people as $index => $row ){
						
						$people[$index]['type'] = "referral";
						$people[$index]['association_link'] = JRoute::_('index.php?option=com_crmery&view=people&layout=person&id='.$row['id']);
					}
				}
				$results = $people;
				
			}
			elseif($view == 'deals')
			{	
				//open model
				$model = & JModel::getInstance('deal','CrmeryModel');
				//retrieve all people
				$deals = $model->getDealList();
				if ( count($deals) ){
					foreach ( $deals as $index => $row ){
						$deals[$index]['type'] = 'client';
						$deals[$index]['association_link'] = JRoute::_('index.php?option=com_crmery&view=deals&layout=deal&id='.$row['id']);
					}
				}
				$results = $deals;
			}elseif($view == 'companies' )	
            {
				//open model
				$model = & JModel::getInstance('company','CrmeryModel');
				//retrieve all people
				$companies = $model->getCompanyList();
				if ( count($companies) ){
					foreach ( $companies as $index => $row ){
						$companies[$index]['type'] = 'firm';
						$companies[$index]['association_link'] = JRoute::_('index.php?option=com_crmery&view=companies&layout=company&id='.$row['id']);
					}
				}
				$results = $companies;
			 }else
			 {
				$model = & JModel::getInstance('people','CrmeryModel');
					//retrieve all people
					$people = $model->getPeopleList();
					if ( count($people) ){
						foreach ( $people as $index => $row ){
							
							$people[$index]['type'] = "person";
							$people[$index]['association_link'] = JRoute::_('index.php?option=com_crmery&view=people&layout=person&id='.$row['id']);
						}
					}
					
				//open model
					$model = & JModel::getInstance('deal','CrmeryModel');
					//retrieve all people
					$deals = $model->getDealList();
					if ( count($deals) ){
						foreach ( $deals as $index => $row ){
							$deals[$index]['type'] = 'client';
							$deals[$index]['association_link'] = JRoute::_('index.php?option=com_crmery&view=deals&layout=deal&id='.$row['id']);
						}
					}
					
				//open model
					$model = & JModel::getInstance('company','CrmeryModel');
					//retrieve all people
					$companies = $model->getCompanyList();
					if ( count($companies) ){
						foreach ( $companies as $index => $row ){
							$companies[$index]['type'] = 'firm';
							$companies[$index]['association_link'] = JRoute::_('index.php?option=com_crmery&view=companies&layout=company&id='.$row['id']);
						}
					}
				$results = array_merge($people,$deals,$companies);
			}	
            //merge our results to a grand object
            //$results = array_merge($people,$deals,$companies);
            
            //return results as json object
            echo json_encode($results);
            
        }
		
		//ajax function to populate note dropdowns
		function getNoteDropdowns(){
			
			//grab people
			$model =& JModel::getInstance('people','CrmeryModel');
			$people = $model->getPeopleList();
			
			//grab deals
			$model =& JModel::getInstance('deal','CrmeryModel');
			$deals = $model->getDealList();
			
			//grab notes categories
			$model =& JModel::getInstance('note','CrmeryModel');
			$categories = $model->getNoteCategories();
			
			//construct data obj
			$data = array('people'=>$people,'deals'=>$deals,'categories'=>$categories);
			
			//encode and return results
			echo json_encode($data);
			
		}
		
		// Function for getting Partner **** Customization part
		function loadpartner() // 
		{		   
		   $data = JRequest::get('post');		  
		   
		   $db = & JFactory::getDBO();	   
		   
		   $query = $db->getQuery(true);
			
		   $query->select('* FROM #__crmery_company_partners');
		   
		  //loop to see if we have matches in database
			$overrides = array('tmpl');
			foreach ( $data as $key => $value ){
				if ( !in_array($key,$overrides) ){
					$query->where($key . " = '" . $value . "'");
				}
			}
			
			$results = $db->setQuery($query);
			$results = $db->loadAssocList();			
			
			//echo "test";
			
		    //print_r($results);
		    echo json_encode($results);		
		}
		
		// Function for getting Country **** Customization part
		function loadcountry() // 
		{
            $ownerId =  CrmeryHelperUsers::getUserId(); 
 
			$db =& JFactory::getDbo();
			$query = $db->getQuery(true);
			
			$query->select("country_id")
				->from("#__crmery_users");
				$query->where('id='.$ownerId);
			$db->setQuery($query);
			$countryIds = $db->loadObjectList();		
			
			if ( count($countryIds) > 0 ){
				foreach ( $countryIds as $countryId ){
				   $countryId= $countryId->country_id;
				}
			} 
		
		   $data = JRequest::get('post');	

           $region_id=$_REQUEST['region_id'];
		   
		   $db = & JFactory::getDBO();	   
		   
		   $query = $db->getQuery(true);
			
		   $query->select('* FROM #__crmery_currencies');
           $query->where('region_id='.$region_id.' and country_id IN ('.$countryId.')');			  
			
			$results = $db->setQuery($query);
			$results = $db->loadAssocList();			
			
			echo "<select name='country_id' id='country_id' class='inputbox' onChange='getState(this.value)'>";
			echo "<option value=''>Any Country</option>";
			
			foreach($results as $key=> $value)
			{			
			  echo "<option value='".$value['country_id']."'>".$value['country_name']."</option>";
			}
			echo "</select>"; 			
		   
		    //echo json_encode($results);		
		}
		
		// Function for getting State **** Customization part
		function loadstate() // 
		{	
            $ownerId =  CrmeryHelperUsers::getUserId(); 
 
			$db =& JFactory::getDbo();
			$query = $db->getQuery(true);
			
			$query->select("state_id")
				->from("#__crmery_users");
				$query->where('id='.$ownerId);
			$db->setQuery($query);
			$stateIds = $db->loadObjectList();		
			
			if ( count($stateIds) > 0 ){
				foreach ( $stateIds as $stateId ){
				   $stateId= $stateId->state_id;
				}
			}
			
		   $data = JRequest::get('post');	

           $country_id=$_REQUEST['country_id'];
		   
		   $db = & JFactory::getDBO();	   
		   
		   $query = $db->getQuery(true);
			
		   $query->select('* FROM #__crmery_states');
          // $query->where('country_id='.$country_id.' and state_id IN ('.$stateId.')');			
           $query->where('country_id='.$country_id);			
			
			$results = $db->setQuery($query);
			$results = $db->loadAssocList();			
			
			echo "<select name='state_id' id='state_id' class='inputbox'>";
			echo "<option value=''>Select State</option>";
			
			foreach($results as $key=> $value)
			{			
			  echo "<option value='".$value['state_id']."'>".$value['state_name']."</option>";
			}
			echo "</select>"; 			
		   
		    //echo json_encode($results);		
		}
		
		// Function for getting Firm Admin **** Customization part
		function loadfirmadmin() // 
		{	
            			
		   $data = JRequest::get('post');			  
		   
           $company_id=$_REQUEST['company_id'];
          // echo '<br>-->'.$company_id=$data['company_id'];
		  //echo '<br>';
		   $db = & JFactory::getDBO();	   
		   
		   $query = $db->getQuery(true);
			
		   $query->select('id,first_name,last_name FROM #__crmery_users');
           $query->where('role_type="manager" and company_id='.$company_id);	         
		   $query->order('first_name');
		   
			$results = $db->setQuery($query);
			$results = $db->loadAssocList();
			
			if(count($results) <1)
			{
			    $pquery = $db->getQuery(true);
				
			    $pquery->select('partner_id');
			    $pquery->from('FROM #__crmery_companies');
			    $pquery->where('id='.$company_id);	         
			
				$db->setQuery($pquery);
				$presults = $db->loadAssocList();
				$prow = $presults->partner_id;
				
			    $query = $db->getQuery(true);
			
				$query->select('id,first_name,last_name FROM #__crmery_users');
				$query->where('role_type="manager" and id='.$prow);	         
				$query->order('first_name');
				
				
				$results = $db->setQuery($query);
				$results = $db->loadAssocList();
			
			}
			 
            if(count($results)>0)
             {			
			
			echo "<select name='assignee_id' id='firmadmin_id' class='inputbox required' onChange='getFirmAdminData(this.value)'>";
			echo "<option value=''>Select Firm Admin</option>";
			
			foreach($results as $key=> $value)
			{			
			  echo "<option value='".$value['id']."'>".$value['first_name']." ".$value['last_name']."</option>";
			}
			echo "</select>"; 
            }else
             {
			    echo "<select name='assignee_id' id='firmadmin_id' class='inputbox'>";
			    echo "<option value=''>No Firm Admin assign</option>";			 
			 }			
		   
		    //echo json_encode($results);		
		}
		
		// load firm details on typing page=add referral 
		function loadfirmdetails()
		{
		   $data = JRequest::get('post');	
		   
		   $fname = $data['q'];          	 
		   $cntryID = (int)$data['cntryID'];          	 
		   $state = (int)$data['state'];          	 
		  
		   $db = & JFactory::getDBO();	   
		   
		   $query = $db->getQuery(true);
			
		   $query = "SELECT f.*,c.country_name FROM #__crmery_companies as f, #__crmery_currencies as c WHERE f.name Like '%$fname%' and c.country_id=f.country_id and f.published>0" ;
		   
		   if($cntryID>0)
		   {
			 $query .= " and f.country_id=$cntryID";
		   }
		   if($state>0)
		   {
			 $query .= " and f.state_id=$state";
		   }	 
		    
		//  $query = " SELECT f.*, c.country_name FROM #__crmery_companies as f LEFT JOIN #__crmery_currencies as c ON f.country_id=c.country_id where c.country_name LIKE '%$fname%' and f.published>0 ";


		   $db->setQuery($query);
		   $results = $db->loadObjectList();	
		 // echo '<pre>'; print_r($results);
		   if(count($results) >0)
			{
				foreach($results as $result)
				{
				   $showtxt = $result->name.', '.$result->country_name;
				   $rsArr .= '<div class="searchresult" onclick="fillbox(\''.$result->id.'~~~'.$showtxt.'\')" >'.$showtxt  .'</div>';
				}  
			}
			else
			{
				$rsArr = "<div class=\"searchresult\"> Nothing Found </div>";
			}
		   
		  // ob_clean();
		  echo $rsArr;		
		  // exit;
		
		}
		 
		 // function to load drop down list of firms by country id
		 function loadfirmdetailsByCountryID()
		 {
			$data = JRequest::get('post');	

			$fname = $data['q'];          	 
			$cntryID = (int)$data['cntryID'];          	 
			$state = (int)$data['state'];          	 

			$db = & JFactory::getDBO();	   

			$query = $db->getQuery(true);

			$query = "SELECT f.*,c.country_name FROM #__crmery_companies as f, #__crmery_currencies as c WHERE f.name Like '%$fname%' and c.country_id=f.country_id and f.published>0 " ;

			if($cntryID>0)
			{
			 $query .= " and f.country_id=$cntryID";
			}
			if($state>0)
			{
			 $query .= " and f.state_id=$state";
			}	 

			//  $query = " SELECT f.*, c.country_name FROM #__crmery_companies as f LEFT JOIN #__crmery_currencies as c ON f.country_id=c.country_id where c.country_name LIKE '%$fname%' and f.published>0 ";

			$query .= ' ORDER BY f.name ASC';
			$db->setQuery($query);
			$results = $db->loadObjectList();	
			// echo '<pre>'; print_r($results);
			
			if(count($results) >0)
			{
				$rsArrSelect  = '<select name="firmdropdownbycntry" onchange="fillbox(this.value)">';
				$rsArrSelect .= '<option value="">Select Firm </option>';	
				
				foreach($results as $result)
				{
				   $showtxt = $result->name.', '.$result->country_name;
				   $rsArr .= '<option value="'.$result->id.'~~~'.$showtxt.'" >'.$result->name  .'</option>';
				}
				
				$rsArrSelect .= $rsArr.'</select> or start typing below'; 
				
			}
			else
			{
				//$rsArr = "<div class=\"searchresult\"> Nothing Found </div>";
			}

			// ob_clean();
			echo $rsArrSelect;		
		    // exit;
		 
		 }
		 
		 
		// Function for getting Firm Admin Data **** Customization part
		function loadfirmadmindata() // 
		{		   
		   $data = JRequest::get('post');	
		   
		   $uid=$data['id'];          	 
		   
		   $db = & JFactory::getDBO();	   
		   
		   $query = $db->getQuery(true);
			
		   $query->select('c.first_name,c.last_name,j.email,f.name from #__crmery_users as c');
		   $query->leftjoin('#__users as j ON c.uid=j.id');
		   $query->leftjoin('#__crmery_companies as f ON c.company_id=f.id');
		   $query->where('c.id='.$uid);
		   
		   
		   
		  //loop to see if we have matches in database
			/* $overrides = array('tmpl');
			foreach ( $data as $key => $value ){
				if ( !in_array($key,$overrides) ){
					$query->where($key . " = '" . $value . "'");
				}
			} */		
			
			$results = $db->setQuery($query);
			$results = $db->loadAssocList();			
			
			//echo "test";
			
		    //print_r($results);
		    echo json_encode($results);		
		} 
		
	// function to show details of lead on add referral page
	function loadleaddetails()
	{
	   $data = JRequest::get('post');	
	   
	   $id=$data['leadid'];          	 
	   
	   $db = & JFactory::getDBO();	   
	   
	   $query = $db->getQuery(true);
	   $query = "SELECT * FROM #__crmery_deals where id=$id";
	
	   $db->setQuery($query);
	   $results = $db->loadObjectList();	
	   $result = $results[0]; 
	   
	   $db = & JFactory::getDBO();	   
	   $qry = $db->getQuery(true);
	   $qry = "SELECT value FROM #__crmery_deal_custom_cf where deal_id=$id";
	   
	   $db->setQuery($qry);
	   $rs = $db->loadObjectList();
	   
	   if($rs)
	   {
		    foreach($rs as $r)
			{
				$txt .= ','.$r->value;
			}
	   }
	   
	   
	   $txt_str =  $result->name.', Amount('.$result->amount.') '.$txt; 
		
	   echo $txt_str;		
		   
		   
	}
		
	// function to check assignment of lead to a firm

	function checkleadassignment()
	{
		$cnt = 0;
		
		$data = JRequest::get('post');	

		  
		$firmadmin_id = (int)$data['firmadmin_id'];          	 
		$dealValue = (int)$data['dealValue'];  

	   
		$db =& JFactory::getDbo();
		$cquery = $db->getQuery(true);

		$cquery->select("person_id")
			->from("#__crmery_people_cf");
			$cquery->where("association_id=$dealValue");

		$db->setQuery($cquery);
		$persons = $db->loadObjectList();

		if($persons)
		{	
			$person_id = array();
			foreach($persons as $per)
			{
				$person_id[] =$per->person_id;
			}
			$fid = implode(',',$person_id);
			
			$db =& JFactory::getDbo();
			$pquery = $db->getQuery(true);

			$pquery->select("count(id) as count")
				->from("#__crmery_people");
			$pquery->where("id IN($fid) and assignee_id=$firmadmin_id and (Accepted=1 or Accepted=0) and published>0");
		 
			$db->setQuery($pquery);
			$referral_lead = $db->loadObjectList();
			$cnt = $referral_lead[0]->count;
		}

		echo $cnt;
	}
		

}
	