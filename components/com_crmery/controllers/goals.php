<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerGoals extends JController
{
		//main display function
		function display(){
			
			JRequest::setVar('view','goals');
			JRequest::setVar('layout','default');
			
			parent::display();
			
		}
	
        function edit(){
            //get model
            $model =& JModel::getInstance('goal','CrmeryModel');
            
            //store data
            $link = JRoute::_('index.php?option=com_crmery&view=goals');
            if ( $model->store() ){
                $msg = CRMText::_('COM_CRMERY_SUCCESS');
                $this->setRedirect($link, $msg);    
            }else{
                $msg = CRMText::_('COM_CRMERY_FAILURE');
                $this->setRedirect($link, $msg);
            }
            
        }
        
        /**
         * Delete a goal entry
         * @param int $_POST['goal_id'] containing goal id to delete
         * @return json $results, $results['error']=0 if no error, 1 if error  
         */
        function deleteGoalEntry(){
            //get id to delete
            $goal_id = JRequest::getVar('goal_id');
            
            //get db
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            
            //form query
            $query->delete("#__crmery_goals")->where("id=$goal_id");
            
            //set query
            $db->setQuery($query);
            $results = array();
            if ( $db->query() ){
                $results['error'] = 0;
            }else{
                $results['error'] = 1;
            }
            
            //return success
            echo json_encode($results);
        }
}
	