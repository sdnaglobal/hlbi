<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerReports extends JController
{
		//main display function
		function display(){
			
			JRequest::setVar('view','reports');
			JRequest::setVar('layout','default');
			
			parent::display();
			
		}
	
		function update(){
			
			//load model
			$model = $this->getModel('tasks');
			//get tasks
			$tasks = $model->getTasks();
			
			//return json list of tasks
			echo json_encode($tasks);
			
		}
        
        
        function edit(){
            //get model
            $model =& JModel::getInstance('report','CrmeryModel');
            
            //store data
            $link = 'index.php?option=com_crmery&view=reports&layout=custom_reports';
            if ( $model->store() ){
                $msg = CRMText::_('COM_CRMERY_CUSTOM_REPORT_SUCCESSFULLY_ADDED');
                $this->setRedirect($link, $msg);    
            }else{
                $msg = CRMText::_('COM_CRMERY_PROBLEM_CREATING_CUSTOM_REPORT');
                $this->setRedirect($link, $msg);
            }
            
        }
        
        /**
         * Remove custom reports
         */
        function deleteReport(){
            
            //gen return info
            $return = array();
            $return['error'] = true;
            
            //get model
            $model = & JModel::getInstance('report','CrmeryModel');
            if ( $model->deleteReport(JRequest::getVar('id')) ){
                $return['error'] = false;
            }
            
            //return json info
            echo json_encode($return);
            
        }
        
}
	