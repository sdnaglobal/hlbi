<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerAjax extends JController
{

        function saveAjax(){

            $item_id = JRequest::getVar('item_id');
            $item_type = JRequest::getVar('item_type');
            $field = JRequest::getVar('field');
            $value = JRequest::getVar('value');

            $db =& JFactory::getDBO();

            $data = array('id'=>$item_id,$field=>$db->escape($value));
			
            $post_data = JRequest::get('post');
            
			$data = array_merge($data,$post_data);
			
			
            $model =& JModel::getInstance($item_type,'CrmeryModel');

            $returnRow = TRUE;
            $return = $model->store($data,$returnRow);

            echo json_encode($return);

        }

        function addPersonToDeal(){

            $data = array();
            $model =& JModel::getInstance('people','CrmeryModel');

            $personId = JRequest::getVar("person_id");

            if ( $personId > 0){
                $data['id'] = $personId;
            } else { 
                $name = explode(" ",JRequest::getVar("name"));
                $firstName = array_key_exists(0,$name) ? $name[0] : "";
                $lastName = array_key_exists(1,$name) ? $name[1] : "";
                $data['first_name'] = $firstName;
                $data['last_name'] = $lastName;

                $id = $model->checkPersonName($data['first_name'].' '.$data['last_name']);
                if ( $id )
                    $data['id'] = $id;

            }
            
            $data['deal_id'] = JRequest::getVar("deal_id");

            if ( $model->store($data) ){
                $success = true;
            }else{
                $success = false;
            }

            echo json_encode(array('success'=>$success));

        }

        function uploadAvatar(){
            
            $model =& JModel::getInstance('avatar','CrmeryModel');
            $item_id = JRequest::getVar('item_id');
            if ( $avatar = $model->saveAvatar() ){

                echo '<script type="text/javascript">
                        window.top.window.modalMessage("'.CRMText::_('COM_CRMERY_SUCCESS_MESSAGE').'","'.CRMText::_('COM_CRMERY_AVATAR_UPLOAD_SUCCESS').'");
                        window.top.window.updateAvatar('.$item_id.',"'.$avatar.'");
                        </script>';
            }else{
                echo '<script type="text/javascript">
                            window.top.window.modalMessage("'.CRMText::_('COM_CRMERY_ERROR').'","'.CRMText::_('COM_CRMERY_AVATAR_UPLOAD_ERROR').'");
                            </script>';
            }

        }
        
        //get deal stages
        function filterDeals(){
            
            //set view
            $view = JController::getView('deals','raw');
            $view->setLayout('list');
            
            //get filters
            $type = JRequest::getVar('type');
            $stage = JRequest::getVar('stage');
            $user = JRequest::getVar('user');
            $close = JRequest::getVar('close');
            $team = JRequest::getVar('team_id');
            
            //get deals
            $model =& JModel::getInstance('deal','CrmeryModel');
            $deals = $model->getDeals(null,$type,$user,$stage,$close,$team);
            
            //assign references
            $view->assignRef('deals',$deals);

            //display
            $view->display();
            
        }
        
        //get deal stages
        function filterCompanies(){
            
            //set view
            $view = JController::getView('companies','raw');
            $view->setLayout('list');
            
            //get filters
            $type = JRequest::getVar('type');
            $user = JRequest::getVar('user');
            $team = JRequest::getVar('team_id');
            $category = JRequest::getVar("category_id");
            
            //get deals
            $model =& JModel::getInstance('company','CrmeryModel');
            $model->set("_category",$category);
            $companies = $model->getCompanies(null,$type,$user,$team);
            
            //assign references
            $view->assignRef('companies',$companies);
            
            //display
            $view->display();
            
        }

        //filter people
        function filterPeople(){
            
            //set view
            $view = JController::getView('people','raw');
            $view->setLayout('list');
            
            //get deals
            $model =& JModel::getInstance('people','CrmeryModel');
            $people = $model->getPeople();
            
            //assign references
            $view->assignRef('people',$people);
            
            //display
            $view->display();
            
        }
		
		
		function updateBillingYear()
		{
			$year = JRequest::getVar('year');
			
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);
			
			$current_financial_year_pure = CrmeryHelperCrmery::getFinancialYear();
			$current_financial_year = date('Y-m-d H:i:s', strtotime('+1 year', strtotime($current_financial_year_pure)));
			
			$current_financial_date = explode("-",$current_financial_year_pure);
			$current_financial_year_new = $current_financial_date[0];
			
			$billingDateYear = "31/10/".$current_financial_year_new;
			
			$query->clear()
					->update("#__crmery_people")
					->set("currentfinancialyear = '$current_financial_year'" );
			
			$db->setQuery($query);
			$db->query();
			
			
			
			$db =& JFactory::getDBO();
			$query1 = $db->getQuery(true);
			$query1->select("c.*")
					->from("#__crmery_people_custom AS c");
			$query1->where('c.id= 4');
			
			$db->setQuery($query1);
			$results = $db->loadAssocList();
			
			if ( count ( $results ) > 0 ){
				foreach ( $results as $key => $result ){
					$results[$key]['values'] = json_decode($result['values']);
				}
        	}	
			
			$res = array_merge($results[$key]['values'], (array)$billingDateYear);
			$resu = json_encode($res);
			
			$query2 = $db->getQuery(true);
			$query2->clear()
					->update("`#__crmery_people_custom`")
					->set("`values` = '$resu'" );
					$query2->where('id= 4');
			
			
			//echo $query2;	
			$db->setQuery($query2);
			$db->query();
			
			//set view
			$view = JController::getView('dashboard','html');
			$view->setLayout('default');
			
			 //display
			 $view->display();
		}	
        
        //filter documents
        function filterDocuments(){
            
            //set view
            $view = JController::getView('documents','raw');
            $view->setLayout('list');
            
            //get deals
            $model =& JModel::getInstance('document','CrmeryModel');
            $documents = $model->getDocuments();
            
            //assign references
            $view->assignRef('documents',$documents);
            
            //display
            $view->display();
            
        }
        
        //save user profile data
        function saveProfile(){
            
            //set error
            $error = true;

            $data = JRequest::get('post');
            $data['id'] = CrmeryHelperUsers::getUserId();

            //get model and store data
            $model =& JModel::getInstance('user','CrmeryModel');
            if ( $model->store($data) ){
                $error = false;
            }
            
            //return results
            $results = array ( 'error' => $error );

            if ( array_key_exists('fullscreen',$data) ){
                $append = CrmeryHelperUsers::isFullscreen() ? "/?&tmpl=component" : "" ;
                $results['url'] = JRoute::_($data['url'].$append);
            }

            echo json_encode($results);
            
        }
        
        //get individual goals
        function getIndividualGoals(){
            
            //get model
            $model =& JModel::getInstance('goal','CrmeryModel');
            
            //get data
            $goals = $model->getIndividualGoals(JRequest::getVar('id'));
            
            //pass data to view
            $view = CrmeryHelperView::getView('goals','filters', array(array('ref'=>'goals','data'=>$goals )));
            
            //display view
            $view->display();
            
        }
        
        //get team goals
        function getTeamGoals(){
             
            //get model
            $model =& JModel::getInstance('goal','CrmeryModel');
            
            //get data
            $goals = $model->getTeamGoals(JRequest::getVar('id'));
            
            //pass data to view
            $view = CrmeryHelperView::getView('goals','filters', array(array('ref'=>'goals','data'=>$goals )));
            
            //display view
            $view->display();
            
        }
        
        //get a leaderboard
        function getLeaderBoard(){
            
            //get model
            $model =& JModel::getInstance('goal','CrmeryModel');
            
            //get data
            $leaderboard = $model->getLeaderBoards(JRequest::getVar('id'));
            
            //pass data to view
            $view = CrmeryHelperView::getView('goals','leaderboard', array(array('ref'=>'leaderboard','data'=>$leaderboard )));
            
            //display view
            $view->display();
            
        }
        
       //update the user filter columns
       function updateColumns(){
           
           //get the location of the page
           $loc = JRequest::getVar('loc');
           
           //get new data to insert into user tables
           $column = JRequest::getVar('column');
           
           //get model
           $model =& JModel::getInstance('user','CrmeryModel');
           $model->updateColumns($loc,$column); 
           
       }

       /**
        * Run items through template system
        * @return [type] [description]
        */
       function createTemplate(){

            $return = array();
            $return['success'] = FALSE;

            $model =& JModel::getInstance('template','CrmeryModel');

            if ( $model->createTemplate() ){
                $return['success'] = TRUE;
            }

            echo json_encode($return);

       }

       function getNoteEntry(){

            $note_id = JRequest::getVar('note_id');

            $model = JModel::getInstance('note','CrmeryModel');
            $note = $model->getNote($note_id);

            $note_view = CrmeryHelperView::getView('note','entry',array(array('ref'=>'note','data'=>$note[0])));
            $note_view->display();

       }

       function getConvoEntry(){

            $convo_id = JRequest::getVar('convo_id');

            $model = JModel::getInstance('conversation','CrmeryModel');
            $convo = $model->getConversation($convo_id);

            $convo_view = CrmeryHelperView::getView('deals','conversation_entry',array(array('ref'=>'conversation','data'=>$convo[0])));
            $convo_view->display();

       }

       function removePersonFromDeal(){

            $person_id = JRequest::getVar('person_id');
            $deal_id = JRequest::getVar('deal_id');

            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);

            $query->select("COUNT(*)")
                    ->from("#__crmery_people")
                    ->where("id=".$person_id);
            $db->setQuery($query);
            $count = $db->loadResult();

            if ( $count ){
                $query->clear()
                    ->delete("#__crmery_people_cf")
                    ->where("association_id=".$deal_id)
                    ->where("association_type='deal'")
                    ->where("person_id=".$person_id);
                $db->setQuery($query);
                if ( $db->query() ){
                    $success = true;
                }else{
                    $success = false;
                }
                $query->clear()
                    ->update("#__crmery_deals")
                    ->set("primary_contact_id=0")
                    ->where("primary_contact_id=".$person_id)
                    ->where("id=".$deal_id);
                $db->setQuery($query);
                if ( $db->query() ){
                    $success = true;
                } else { 
                    $success = $success ? $success : false;
                }
            }else{
                $success = false;
            }

            $data = array('success'=>$success);
            echo json_encode($data);
       }

       function getCalendarEvents(){
            $data = JRequest::get('get');
            $start_date = CrmeryHelperDate::formatDBDate(date("Y-m-d 00:00:00",$data['start']));
            $end_date = CrmeryHelperDate::formatDBDate(date("Y-m-d 00:00:00",$data['end']));
            $model =& JModel::getInstance('Event','CrmeryModel');
            $model->set('start_date',"$start_date");
            $model->set('end_date',"$end_date");
            $model->set('loc',"calendar");

            $user = CrmeryHelperUsers::getLoggedInUser();
            if ( !$user ){
                $model->set('public',1);
            }

            $events = $model->getEvents();
            echo json_encode($events);
            exit();
       }

       /**
        * Download CSV
        * @return [type] [description]
        */
       function downloadCsv(){
        
            ob_start();
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="export.csv"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            // header('Content-Length: ' . filesize($path));
            ob_clean();
            flush();

            $model =& JModel::getInstance('export','CrmeryModel');
            echo $model->getCsv();

            exit();

       }

       /**
        * Download CSV
        * @return [type] [description]
        */
       function downloadVcard(){
        
            ob_start();
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="export.vcf"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            // header('Content-Length: ' . filesize($path));
            ob_clean();
            flush();

            $model =& JModel::getInstance('export','CrmeryModel');
            echo $model->getVcard();

            exit();

       }

       /**
        * Download example import templates
        * @return [type] [description]
        */
       function downloadImportTemplate(){

            $template_type = JRequest::getVar('template_type');
        
            $path = JPATH_SITE.'/components/com_crmery/media/import_templates/import_'.$template_type.'.csv';

            ob_start();
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="import_'.$template_type.'.csv"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($path));
            ob_clean();
            flush();

            readfile($path);

            exit();

       }

       function checkCompanyName()
      {
            $company_name = JRequest::getVar('company_name');
            $companyModel = JModel::getInstance('Company','CrmeryModel');
            $existingCompany = $companyModel->checkCompanyName($company_name);            

            if($existingCompany!="") {
                /** ucwords(CRMText::_('COM_CRMERY_COMPANY_EXISTS')) **/
                echo json_encode(array('success' => true, 'company_id' => $existingCompany,'message' => ""));
            } else {
                echo json_encode(array('success' => true, 'message' => ucwords(CRMText::_('COM_CRMERY_COMPANY_WILL_BE_CREATED'))));
            }
       }

       function checkEmailName()
      {
            $emailExists = CrmeryHelperCrmery::checkEmailName(JRequest::getVar('email'));
            if ( $emailExists ){
                $success = true;
                $msg = CRMText::_('COM_CRMERY_EMAIL_EXISTS');
            }else{
                $success = true;
                $msg = CRMText::_('COM_CRMERY_EMAIL_IS_AVAILABLE');
            }
            $return = array('success'=>$success,'message'=>$msg,'email_exists'=>$emailExists);
            echo json_encode($return);
       }

       function checkDealName()
      {
            $deal_name = JRequest::getVar('deal_name');
            $dealModel = JModel::getInstance('Deal','CrmeryModel');
            $existingDeal = $dealModel->checkDealName($deal_name);            

            if($existingDeal!="") {
                /** ucwords(CRMText::_('COM_CRMERY_DEAL_EXISTS')) **/ 
                echo json_encode(array('success' => true, 'deal_id' => $existingDeal,'message' => ""));
            } else {
                echo json_encode(array('success' => true, 'message' => ucwords(CRMText::_('COM_CRMERY_DEAL_WILL_BE_CREATED'))));
            }
       }

       function checkPersonName()
      {
            $person_name = JRequest::getVar('person_name');
            $personModel = JModel::getInstance('People','CrmeryModel');
            $existingPerson = $personModel->checkPersonName($person_name);            

            if($existingPerson!="") {
                /** ucwords(CRMText::_('COM_CRMERY_PERSON_EXISTS')) **/
                echo json_encode(array('success' => true, 'person_id' => $existingPerson,'message' => ""));
            } else {
                echo json_encode(array('success' => true, 'message' => ucwords(CRMText::_('COM_CRMERY_PERSON_WILL_BE_CREATED'))));
            }
       }

       function previewDocument(){
            $model =& JModel::getInstance('document','CrmeryModel');
            $document = $model->getDocument();

            $filename = basename($document->name);
            $file_extension = strtolower(substr(strrchr($filename,"."),1));

            switch( $file_extension ) {
                case "gif": $ctype="image/gif"; break;
                case "png": $ctype="image/png"; break;
                case "jpeg":
                case "jpg": $ctype="image/jpg"; break;
                case "pdf": $ctype= "application/pdf"; break;
                default: $ctype = "application/".$file_extension; break;
            }

            header('Content-type: '.$ctype);
            ob_clean();
            flush();
            readfile($document->path);
            exit;
       }

       function toggleMailingList(){
            $data = JRequest::get('post');
            $subscribe = $data['subscribe'];
            $success = FALSE;
            if ( !$subscribe ){
                if ( CrmeryHelperMailinglists::addMailingList($data) ){
                    $success = TRUE;
                }
            }else{
                if ( CrmeryHelperMailinglists::removeMailingList($data) ){
                    $success = TRUE;
                }
            }
            $return = array('success'=>$success);
            echo json_encode($return);
       }

       function shareItem(){

            $return = array();

            if ( CrmeryHelperCrmery::shareItem() ){
                $return['success'] = true;
            }else{
                $return['success'] = false;
            }

            echo json_encode($return);

       }

       function unshareItem(){

            $return = array();

            if ( CrmeryHelperCrmery::unshareItem() ){
                $return['success'] = true;
            }else{
                $return['success'] = false;
            }

            echo json_encode($return);

       }

       
        function saveWizardForm(){

            $config = JFactory::getConfig();
            $post = JRequest::get('post');
            if ( $config->get('captcha') && array_key_exists('recaptcha_response_field',$post)){
                JPluginHelper::importPlugin('captcha');
                $dispatcher = JDispatcher::getInstance();
                $res = $dispatcher->trigger('onCheckAnswer',$post['recaptcha_response_field']);
                if(!$res[0]){
                    die('Invalid Captcha');
                }
            }

            $type = JRequest::getVar('save_type');
            switch($type){
                case "lead":
                case "contact":
                    $model =& JModel::getInstance('people','CrmeryModel');
                break;
                case "company":
                    $model =& JModel::getInstance('company','CrmeryModel');
                break;
                case "deal":
                    $model =& JModel::getInstance('deal','CrmeryModel');
                break;
            }
            $model->store();
            header('Location: '.base64_decode(JRequest::getVar('return')));
       }

       function toggleCompanyPrivacy(){

            $company_id = JRequest::getVar('company_id');
            $model = JModel::getInstance('Company','CrmeryModel');

            $return = array('success'=>false);
            $company = $model->getCompanies($company_id);
            $company = array_key_exists(0,$company) ? $company[0] : false;

            if ( $company ){
                $privacy = $company['public'] == 0 ? 1 : 0;
                $data = array('id'=>$company['id'],'public'=>$privacy);
                if ( $model->store($data) ){
                    $return['success'] = true;
                    $return['text'] = $privacy == 1 ? CRMText::_('COM_CRMERY_MAKE_PRIVATE') : CRMText::_('COM_CRMERY_MAKE_PUBLIC');
                }
            }

            echo json_encode($return);

       }
        
}