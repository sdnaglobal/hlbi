<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerDocuments extends JController
{
        //main display function
        function display(){
            
            JRequest::setVar('view','documents');
            JRequest::setVar('layout','default');
            
            parent::display();
            
        }
    
        function update(){
            
            //load model
            $model = $this->getModel('tasks');
            //get tasks
            $tasks = $model->getTasks();
            
            //return json list of tasks
            echo json_encode($tasks);
            
        }
        
        //upload documents
        function uploadDocument(){
                        
               $model = & JModel::getInstance('document','CrmeryModel');
               if(!$id=$model->store()){
                    $error = $model->getError();
                   echo '<script type="text/javascript">window.top.window.uploadError("'.$error.'");</script>';
                   return;
               }else{
                   // success, exit with code 0 for Mac users, otherwise they receive an IO Error
                   $db = & JFactory::getDBO();
                   echo '<script type="text/javascript">window.top.window.uploadSuccess('.$id.');</script>';
                   exit(0);
               }
        }

        /**
         * Remove item associations from database and delete local files associate with documents
         */
        function deleteDocument(){
            
            //gen return info
            $return = array();
            $return['error'] = true;
            
            //get model
            $model = & JModel::getInstance('document','CrmeryModel');
            if ( $model->deleteDocument(JRequest::getVar('id')) ){
                $return['error'] = false;
            }
            
            //return json info
            echo json_encode($return);
            
        }
}
    