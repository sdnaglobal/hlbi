<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerCharts extends JController
{
		
		function display(){
		
			$model = $this->getModel('charts');
			$tasks = $model->getWorkReferredBySource();
		
			parent::display();
			
		}
	
		function update(){
			
			$model = $this->getModel('tasks');
			$tasks = $model->getTasks();
			echo json_encode($tasks);
			
		}
}
	