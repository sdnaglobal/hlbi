/**
 * Globals
 */
var new_event_date = null;
var menu = true;
var curr_cal_event = null;
var cloning = false;

jQuery(document).ready(function(){
	
	//construct calendar object
	jQuery("#calendar").fullCalendar({
		
		theme:true,
		
		events:function(start, end, callback) {
	        jQuery.ajax({
	            url: base_url+'index.php?option=com_crmery&controller=ajax&task=getCalendarEvents&format=raw&tmpl=component',
	            dataType: 'JSON',
	            data: {
	                start: Math.round(start.getTime() / 1000),
	                end: Math.round(end.getTime() / 1000)
	            },
	            success: function(data) {
	                callback(data);
	            }
	        });
    	},

		editable:true,

		//Rendering events
		eventRender: function(event,element){

			//Update css for completed events
			if ( event.completed == 1 ){
				jQuery(element).css('text-decoration','line-through');
			}
			
			jQuery(element).addClass("calendar_"+event.type);
			jQuery(element).addClass("assignee_id_"+event.assignee_id);
			
			if ( typeof event.assignee_color != 'undefined' ){
				jQuery("div.assignee_id_"+event.assignee_id+" .fc-event-skin").css('background','#'+event.assignee_color);
				jQuery("div.assignee_id_"+event.assignee_id+" .fc-event-skin").css('borderColor','#'+event.assignee_color);
				jQuery("div.assignee_id_"+event.assignee_id).css('borderColor','#'+event.assignee_color);
			}else{
				jQuery(element).addClass("calendar_"+event.type+"_bg");
			}
		},

		eventAfterRender: function(event,element,view){

				//Update css for completed events
				if ( event.completed == 1 ){
					jQuery(element).css('text-decoration','line-through');
				}

				if ( cloning || event.server || event.clone ){

					return true;

				}else{

					if ( event.repeats != "none" && !event.cloned && event.update_future_events ){

						calEvents = new Array();
						cloning = true;

						var newEvent = new Object();

						if ( event.type == 'event' ){
							// Split timestamp into [ Y, M, D, h, m, s ]
							var st = event.start_time.split(/[- :]/);	
							var et = event.end_time.split(/[- :]/);
							// Apply each element to the Date function
							newEvent.start_time = new Date(st[0], st[1]-1, st[2]/*, st[3], st[4], st[5]);*/); 
							newEvent.end_time = new Date(et[0], et[1]-1, et[2]/*, et[3], et[4], et[5]);*/);
						}else{
							//Get due date time stamp
							var dt = event.due_date.split(/[- :]/);
							newEvent.due = new Date(dt[0], dt[1]-1, dt[2]/*, dt[3], dt[4], dt[5]);*/);
						}

						switch ( event.repeats ){

							/**
							 * DAILY
							 */
							case 'daily':

								if ( event.type == "event" ){
									var nextMonth = newEvent.start_time.getMonth()+2;
									var currMonth = newEvent.start_time.getMonth()+1;
									newEvent.start_time.setDate(newEvent.start_time.getDate()-1);
									newEvent.end_time.setDate(newEvent.end_time.getDate()-1);
								}else{
									var nextMonth = newEvent.due.getMonth()+2;
									var currMonth = newEvent.due.getMonth()+1;
									newEvent.due.setDate(newEvent.due.getDate()-1);
								}	
								
								var counter = 1;

								var buffer = false;
								while ( ( currMonth < nextMonth ) && !buffer ){

									if ( event.type == "event" ){
									
										newEvent.start_time.setDate(newEvent.start_time.getDate()+1);

										var year = newEvent.start_time.getFullYear();
										var month = newEvent.start_time.getMonth()+1;
										var day = newEvent.start_time.getDate()+1;
										var hour = newEvent.start_time.getHours();
										var minute = newEvent.start_time.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.start = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;

										newEvent.end_time.setDate(newEvent.end_time.getDate()+1);

										var year = newEvent.end_time.getFullYear();
										var month = newEvent.end_time.getMonth()+1;
										var day = newEvent.end_time.getDate()+1;
										var hour = newEvent.end_time.getHours();
										var minute = newEvent.end_time.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.end = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;

									}else{

										newEvent.due.setDate(newEvent.due.getDate()+1);

										var year = newEvent.due.getFullYear();
										var month = newEvent.due.getMonth()+1;
										var day = newEvent.due.getDate()+1;
										var hour = newEvent.due.getHours();
										var minute = newEvent.due.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.start = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
										newEvent.end = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
										newEvent.due_date = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
									}

									newEvent.allDay = event.allDay;
									newEvent.assignee_id = event.assignee_id;
									newEvent.association_id = event.association_id;
									newEvent.association_type = event.association_type;
									newEvent.category_id = event.category_id;
									newEvent.completed = event.completed;
									newEvent.description = event.description;
									newEvent.name = event.name;
									newEvent.owner_id = event.owner_id;
									newEvent.parent_id = event.id;
									newEvent.title = event.title;
									newEvent.type = event.type;
									newEvent.clone = true;
									newEvent.repeats = event.repeats;

									calEvents.push(jQuery.extend({},newEvent));

									if ( event.type == "event" ){
										currMonth = newEvent.start_time.getMonth()+1;
									}else{
										currMonth = newEvent.due.getMonth()+1;
									}
									
									counter++;
									if ( counter == 31 ) buffer = true;

								}

							break;

							/**
							 * WEEKDAYS
							 */
							case 'weekdays':

								if ( event.type == "event" ){
									var nextMonth = newEvent.start_time.getMonth()+2;
									var currMonth = newEvent.start_time.getMonth()+1;
									newEvent.start_time.setDate(newEvent.start_time.getDate()-1);
									newEvent.end_time.setDate(newEvent.end_time.getDate()-1);
								}else{
									var nextMonth = newEvent.due.getMonth()+2;
									var currMonth = newEvent.due.getMonth()+1;
									newEvent.due.setDate(newEvent.due.getDate()-1);
								}	
								
								var counter = 1;

								var buffer = false;
								while ( ( currMonth < nextMonth ) && !buffer ){

									if ( event.type == "event" ){
									
										newEvent.start_time.setDate(newEvent.start_time.getDate()+1);

										var year = newEvent.start_time.getFullYear();
										var month = newEvent.start_time.getMonth()+1;
										var day = newEvent.start_time.getDate()+1;
										var hour = newEvent.start_time.getHours();
										var minute = newEvent.start_time.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.start = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;

										newEvent.end_time.setDate(newEvent.end_time.getDate()+1);

										var year = newEvent.end_time.getFullYear();
										var month = newEvent.end_time.getMonth()+1;
										var day = newEvent.end_time.getDate()+1;
										var hour = newEvent.end_time.getHours();
										var minute = newEvent.end_time.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.end = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;

									}else{

										newEvent.due.setDate(newEvent.due.getDate()+1);

										var year = newEvent.due.getFullYear();
										var month = newEvent.due.getMonth()+1;
										var day = newEvent.due.getDate()+1;
										var hour = newEvent.due.getHours();
										var minute = newEvent.due.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.start = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
										newEvent.end = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
										newEvent.due_date = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
									}

									newEvent.allDay = event.allDay;
									newEvent.assignee_id = event.assignee_id;
									newEvent.association_id = event.association_id;
									newEvent.association_type = event.association_type;
									newEvent.category_id = event.category_id;
									newEvent.completed = event.completed;
									newEvent.description = event.description;
									newEvent.name = event.name;
									newEvent.owner_id = event.owner_id;
									newEvent.parent_id = event.id;
									newEvent.title = event.title;
									newEvent.type = event.type;
									newEvent.clone = true;
									newEvent.repeats = event.repeats;

									if ( event.type == "event" ){
										var day = newEvent.start_time.getDay();
										currMonth = newEvent.start_time.getMonth()+1;
									}else{
										var day = newEvent.due.getDay();
										currMonth = newEvent.due.getMonth()+1;
									}

									if( day > -1 && day < 5 ){
										calEvents.push(jQuery.extend({},newEvent));
									}
									
									counter++;
									if ( counter == 31 ) buffer = true;

								}

							break;

			                /**
							 * WEEKLY
							 */
							case 'weekly':

								if ( event.type == "event" ){
									var nextMonth = newEvent.start_time.getMonth()+2;
									var currMonth = newEvent.start_time.getMonth()+1;
									newEvent.start_time.setDate(newEvent.start_time.getDate()-1);
									newEvent.end_time.setDate(newEvent.end_time.getDate()-1);
								}else{
									var nextMonth = newEvent.due.getMonth()+2;
									var currMonth = newEvent.due.getMonth()+1;
									newEvent.due.setDate(newEvent.due.getDate()-1);
								}
								
								var counter = 1;

								var buffer = false;
								while ( ( currMonth < nextMonth ) && !buffer ){

									if ( event.type == "event" ){
									
										newEvent.start_time.setDate(newEvent.start_time.getDate()+7);

										var year = newEvent.start_time.getFullYear();
										var month = newEvent.start_time.getMonth()+1;
										var day = newEvent.start_time.getDate()+1;
										var hour = newEvent.start_time.getHours();
										var minute = newEvent.start_time.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.start = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;

										newEvent.end_time.setDate(newEvent.end_time.getDate()+7);

										var year = newEvent.end_time.getFullYear();
										var month = newEvent.end_time.getMonth()+1;
										var day = newEvent.end_time.getDate()+1;
										var hour = newEvent.end_time.getHours();
										var minute = newEvent.end_time.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.end = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;

									}else{

										newEvent.due.setDate(newEvent.due.getDate()+7);

										var year = newEvent.due.getFullYear();
										var month = newEvent.due.getMonth()+1;
										var day = newEvent.due.getDate()+1;
										var hour = newEvent.due.getHours();
										var minute = newEvent.due.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.start = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
										newEvent.end = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
										newEvent.due_date = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;

									}

									newEvent.allDay = event.allDay;
									newEvent.assignee_id = event.assignee_id;
									newEvent.association_id = event.association_id;
									newEvent.association_type = event.association_type;
									newEvent.category_id = event.category_id;
									newEvent.completed = event.completed;
									newEvent.description = event.description;
									newEvent.name = event.name;
									newEvent.owner_id = event.owner_id;
									newEvent.parent_id = event.id;
									newEvent.title = event.title;
									newEvent.type = event.type;
									newEvent.clone = true;
									newEvent.repeats = event.repeats;

									if ( event.type == "event" ){
										currMonth = newEvent.start_time.getMonth()+1;
									}else{
										currMonth = newEvent.due.getMonth()+1;
									}

									calEvents.push(jQuery.extend({},newEvent));
									
									counter++;
									if ( counter == 31 ) buffer = true;

								}

							break;


			                //Weekly Monday Wednesday and Friday
			                case 'weekly-mwf':

								if ( event.type == "event" ){
									var nextMonth = newEvent.start_time.getMonth()+2;
									var currMonth = newEvent.start_time.getMonth()+1;
									newEvent.start_time.setDate(newEvent.start_time.getDate()-1);
									newEvent.end_time.setDate(newEvent.end_time.getDate()-1);
								}else{
									var nextMonth = newEvent.due.getMonth()+2;
									var currMonth = newEvent.due.getMonth()+1;
									newEvent.due.setDate(newEvent.due.getDate()-1);
								}	
								
								var counter = 1;

								var buffer = false;
								while ( ( currMonth < nextMonth ) && !buffer ){

									if ( event.type == "event" ){
									
										newEvent.start_time.setDate(newEvent.start_time.getDate()+1);

										var year = newEvent.start_time.getFullYear();
										var month = newEvent.start_time.getMonth()+1;
										var day = newEvent.start_time.getDate()+1;
										var hour = newEvent.start_time.getHours();
										var minute = newEvent.start_time.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.start = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;

										newEvent.end_time.setDate(newEvent.end_time.getDate()+1);

										var year = newEvent.end_time.getFullYear();
										var month = newEvent.end_time.getMonth()+1;
										var day = newEvent.end_time.getDate()+1;
										var hour = newEvent.end_time.getHours();
										var minute = newEvent.end_time.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.end = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;

									}else{

										newEvent.due.setDate(newEvent.due.getDate()+1);

										var year = newEvent.due.getFullYear();
										var month = newEvent.due.getMonth()+1;
										var day = newEvent.due.getDate()+1;
										var hour = newEvent.due.getHours();
										var minute = newEvent.due.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.start = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
										newEvent.end = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
										newEvent.due_date = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
									}

									newEvent.allDay = event.allDay;
									newEvent.assignee_id = event.assignee_id;
									newEvent.association_id = event.association_id;
									newEvent.association_type = event.association_type;
									newEvent.category_id = event.category_id;
									newEvent.completed = event.completed;
									newEvent.description = event.description;
									newEvent.name = event.name;
									newEvent.owner_id = event.owner_id;
									newEvent.parent_id = event.id;
									newEvent.title = event.title;
									newEvent.type = event.type;
									newEvent.clone = true;
									newEvent.repeats = event.repeats;

									if ( event.type == "event" ){
										var day = newEvent.start_time.getDay();
										currMonth = newEvent.start_time.getMonth()+1;
									}else{
										var day = newEvent.due.getDay();
										currMonth = newEvent.due.getMonth()+1;
									}

									if( day == 0 || day == 2 || day == 4 ){
										calEvents.push(jQuery.extend({},newEvent));
									}
									
									counter++;
									if ( counter == 31 ) buffer = true;

								}

							break;

			                    
			                //Weekly Tuesday Thursday
			                case 'weekly-tr':

								if ( event.type == "event" ){
									var nextMonth = newEvent.start_time.getMonth()+2;
									var currMonth = newEvent.start_time.getMonth()+1;
									newEvent.start_time.setDate(newEvent.start_time.getDate()-1);
									newEvent.end_time.setDate(newEvent.end_time.getDate()-1);
								}else{
									var nextMonth = newEvent.due.getMonth()+2;
									var currMonth = newEvent.due.getMonth()+1;
									newEvent.due.setDate(newEvent.due.getDate()-1);
								}	
								
								var counter = 1;

								var buffer = false;
								while ( ( currMonth < nextMonth ) && !buffer ){

									if ( event.type == "event" ){
									
										newEvent.start_time.setDate(newEvent.start_time.getDate()+1);

										var year = newEvent.start_time.getFullYear();
										var month = newEvent.start_time.getMonth()+1;
										var day = newEvent.start_time.getDate()+1;
										var hour = newEvent.start_time.getHours();
										var minute = newEvent.start_time.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.start = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;

										newEvent.end_time.setDate(newEvent.end_time.getDate()+1);

										var year = newEvent.end_time.getFullYear();
										var month = newEvent.end_time.getMonth()+1;
										var day = newEvent.end_time.getDate()+1;
										var hour = newEvent.end_time.getHours();
										var minute = newEvent.end_time.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.end = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;

									}else{

										newEvent.due.setDate(newEvent.due.getDate()+1);

										var year = newEvent.due.getFullYear();
										var month = newEvent.due.getMonth()+1;
										var day = newEvent.due.getDate()+1;
										var hour = newEvent.due.getHours();
										var minute = newEvent.due.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.start = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
										newEvent.end = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
										newEvent.due_date = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
									}

									newEvent.allDay = event.allDay;
									newEvent.assignee_id = event.assignee_id;
									newEvent.association_id = event.association_id;
									newEvent.association_type = event.association_type;
									newEvent.category_id = event.category_id;
									newEvent.completed = event.completed;
									newEvent.description = event.description;
									newEvent.name = event.name;
									newEvent.owner_id = event.owner_id;
									newEvent.parent_id = event.id;
									newEvent.title = event.title;
									newEvent.type = event.type;
									newEvent.clone = true;
									newEvent.repeats = event.repeats;

									if ( event.type == "event" ){
										var day = newEvent.start_time.getDay();
										currMonth = newEvent.start_time.getMonth()+1;
									}else{
										var day = newEvent.due.getDay();
										currMonth = newEvent.due.getMonth()+1;
									}

									if( day == 1 || day == 3 ){
										calEvents.push(jQuery.extend({},newEvent));
									}
									
									counter++;
									if ( counter == 31 ) buffer = true;

								}

							break;

			                    
			                /**
			                 * MONTHLY
			                 */
			                case 'monthly':

								if ( event.type == "event" ){
									var nextYear = newEvent.start_time.getFullYear()+1;
									var currYear = newEvent.start_time.getFullYear();
									newEvent.start_time.setDate(newEvent.start_time.getDate()+1);
									newEvent.end_time.setDate(newEvent.end_time.getDate()+1);
								}else{
									var nextYear = newEvent.due.getFullYear()+1;
									var currYear = newEvent.due.getFullYear();
									newEvent.due.setDate(newEvent.due.getDate()+1);
								}	
								
								var counter = 1;

								var buffer = false;
								while ( ( currYear < nextYear ) && !buffer ){

									if ( event.type == "event" ){
									
										newEvent.start_time.setMonth(newEvent.start_time.getMonth()+1);

										var year = newEvent.start_time.getFullYear();
										var month = newEvent.start_time.getMonth()+1;
										var day = newEvent.start_time.getDate()-1;
										var hour = newEvent.start_time.getHours();
										var minute = newEvent.start_time.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.start = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;

										newEvent.end_time.setMonth(newEvent.end_time.getMonth()+1);

										var year = newEvent.end_time.getFullYear();
										var month = newEvent.end_time.getMonth()+1;
										var day = newEvent.end_time.getDate()-1;
										var hour = newEvent.end_time.getHours();
										var minute = newEvent.end_time.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.end = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;

									}else{

										newEvent.due.setMonth(newEvent.due.getMonth()+1);

										var year = newEvent.due.getFullYear();
										var month = newEvent.due.getMonth()+1;
										var day = newEvent.due.getDate()-1;
										var hour = newEvent.due.getHours();
										var minute = newEvent.due.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.start = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
										newEvent.end = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
										newEvent.due_date = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
									}

									newEvent.allDay = event.allDay;
									newEvent.assignee_id = event.assignee_id;
									newEvent.association_id = event.association_id;
									newEvent.association_type = event.association_type;
									newEvent.category_id = event.category_id;
									newEvent.completed = event.completed;
									newEvent.description = event.description;
									newEvent.name = event.name;
									newEvent.owner_id = event.owner_id;
									newEvent.parent_id = event.id;
									newEvent.title = event.title;
									newEvent.type = event.type;
									newEvent.clone = true;
									newEvent.repeats = event.repeats;

									if ( event.type == "event" ){
										currYear = newEvent.start_time.getFullYear();
									}else{
										currYear = newEvent.due_date.getFullYear();
									}

									calEvents.push(jQuery.extend({},newEvent));
									
									counter++;
									if ( counter == 31 ) buffer = true;

								}

							break;	
			                    

			                //Yearly
		                	case 'yearly':

								if ( event.type == "event" ){
									var nextNextYear = newEvent.start_time.getFullYear()+2;
									var nextYear = newEvent.start_time.getFullYear()+1;
									newEvent.start_time.setDate(newEvent.start_time.getDate());
									newEvent.end_time.setDate(newEvent.end_time.getDate());
								}else{
									var nextNextYear = newEvent.due.getFullYear()+2;
									var nextYear = newEvent.due.getFullYear()+1;
									newEvent.due.setDate(newEvent.due.getDate());
								}	
								
								var counter = 1;

								var buffer = false;
								while ( ( nextYear < nextNextYear ) && !buffer ){

									if ( event.type == "event" ){
									
										newEvent.start_time.setFullYear(newEvent.start_time.getFullYear()+1);

										var year = newEvent.start_time.getFullYear();
										var month = newEvent.start_time.getMonth()+1;
										var day = newEvent.start_time.getDate();
										var hour = newEvent.start_time.getHours();
										var minute = newEvent.start_time.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.start = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;

										newEvent.end_time.setFullYear(newEvent.end_time.getFullYear()+1);

										var year = newEvent.end_time.getFullYear();
										var month = newEvent.end_time.getMonth()+1;
										var day = newEvent.end_time.getDate();
										var hour = newEvent.end_time.getHours();
										var minute = newEvent.end_time.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.end = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;

									}else{

										newEvent.due.setFullYear(newEvent.due.getFullYear()+1);

										var year = newEvent.due.getFullYear();
										var month = newEvent.due.getMonth()+1;
										var day = newEvent.due.getDate();
										var hour = newEvent.due.getHours();
										var minute = newEvent.due.getMinutes();
										var seconds = "00";

										month = ( month > 9 ) ? month : "0"+month;
										day = ( day > 9 ) ? day : "0"+day;
										hour = ( hour > 9 ) ? hour : "0"+hour;
										minute = ( minute > 9 ) ? minute : "0"+minute;

										newEvent.start = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
										newEvent.end = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
										newEvent.due_date = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
									}

									newEvent.allDay = event.allDay;
									newEvent.assignee_id = event.assignee_id;
									newEvent.association_id = event.association_id;
									newEvent.association_type = event.association_type;
									newEvent.category_id = event.category_id;
									newEvent.completed = event.completed;
									newEvent.description = event.description;
									newEvent.name = event.name;
									newEvent.owner_id = event.owner_id;
									newEvent.parent_id = event.id;
									newEvent.title = event.title;
									newEvent.type = event.type;
									newEvent.clone = true;
									newEvent.repeats = event.repeats;

									if ( event.type == "event" ){
										nextYear = newEvent.start_time.getFullYear()+1;
									}else{
										nextYear = newEvent.due.getFullYear()+1;
									}

									calEvents.push(jQuery.extend({},newEvent));
									
									counter++;
									if ( counter == 31 ) buffer = true;

								}

							break;

						}

						jQuery("#calendar").fullCalendar('addEventSource',calEvents);
						cloning = false;
						event.cloned = true;

					}
				}

		},
		
		eventClick: function(calEvent,jsEvent,view){
			
			//set event object
			window.calEvent = calEvent;
			showEvent(calEvent,jsEvent);
			
		},
		
	});


});

function showEvent(calendarEvent,javascriptEvent)
{
	
	var id = calendarEvent.id;
	var type = calendarEvent.type;
	var event = calendarEvent;

	var parentString = "";
	if ( event != null ){
		parentString = '&parent_id='+event.parent_id;
	}else{
		event = null;
	}

	var dataString = "";
	if ( typeof loc != 'undefined' && loc == 'calendar' ){
		dataString += "&date="+jQuery.datepicker.formatDate("yy-mm-dd",event.start);
	}

	jQuery.ajax({
		type	:	'POST',
		url		:	base_url+'index.php?option=com_crmery&view=publiccalendar&layout=view_'+type+'&id='+id+'&tmpl=component&format=raw'+parentString,
		data 	: 	dataString,
		success	:	function(data){
			jQuery('#view_'+type).html(data);
			jQuery.fx.speeds._default = 1000;
			jQuery('#view_'+type).dialog({
				
					dialogClass:'com_crmery',
					autoOpen:false,
					modal:true,
					resizable: false,
					position:['center','center'],
					width:500,
					modal:true
				
			});
			//open dialog
			jQuery('#view_'+type).dialog('open');
		}
	});
				
}

//close a task event modal dialog
function closeTaskEvent(type){
	
	jQuery.fx.speeds._default = 1000;
	jQuery("#view_"+type).dialog('close');
	
}