var geocoder = new google.maps.Geocoder();
var latlng = null;
var map = null;

jQuery(document).ready(function(){
  /** clean any forms on the page **/
  //cleanPageForms();
 
  //reset type=date inputs to text
  jQuery( document ).bind( "mobileinit", function(){
    jQuery.mobile.page.prototype.options.degradeInputs.date = true;
  });

  jQuery.datepicker.setDefaults({
    onSelect: function (event,ui) {
      jQuery(ui.settings.altField+'_hidden').val(ui.selectedYear+"-"+("0"+(ui.selectedMonth+1)).slice(-2)+"-"+("0"+(ui.selectedDay)).slice(-2));
    }
  });


 jQuery('#map_canvas').gmap().bind('init',function(event, map) {

     navigator.geolocation.getCurrentPosition(function(position){
        var latlng = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
          geocoder.geocode({'location':latlng}, function(results,status) {
            if(status=='OK') {
              jQuery('#from').val(results[0].formatted_address);
              jQuery('#map_canvas').gmap('displayDirections', 
                {'origin': jQuery('#from').val(), 
                'destination': jQuery('#to').val(), 
                'travelMode':
                google.maps.DirectionsTravelMode.DRIVING }, {},
                function(result, status){
                    if (status === 'OK'){
                        center = result.routes[0].bounds.getCenter();
                        jQuery('#map_canvas').gmap('option', 'center', center);
                    } else {
                    } 
                });
            } else {
              codeAddress();
            }
        });
      });
  });

    jQuery('submit').click(function() {
      jQuery('#map_canvas').gmap('displayDirections', { 
        'origin': jQuery('#from').val(), 
        'destination': jQuery('#to').val(), 
        'travelMode': google.maps.DirectionsTravelMode.DRIVING 
      }, { 'panel': document.getElementById('directions')}, function(response, status) {
        ( status === 'OK' ) ? jQuery('#results').show() : jQuery('#results').hide();
      });
      return false;
    });

});

function codeAddress() {
    geocoder.geocode( { 'address': jQuery('#to').val()}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        var myOptions = {
        zoom: 8,
        center: results[0].geometry.location,
        mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

        var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location
        });
      }
    });
  }

function cleanPageForms(){
  /*
  jQuery("form :input").each(function(){
    if ( jQuery(this)attr('type') != "hidden" ){
      jQuery(this).val('');
    }
  });
   */
}

//add note Entries
function addTaskEntry(){
  
  if(loc=="events") {
    jQuery('#task_edit').submit();
    return;
  }

  //generate data string for ajax call
  var dataString = '';
 
  var form = jQuery('#task_edit :input');  
  jQuery(form).each(function(){
    dataString += "&"+this.name+"="+jQuery(this).val();
  });
  
  //determine which page we are adding a note from
  if ( loc == "company" ){
    dataString += "&association_id="+company_id;
    dataString += "&association_type=company";
  }
  if ( loc == "deal" ){
    dataString += "&association_id="+deal_id;
    dataString += "&association_type=deal";

  }
  if ( loc == "person" ){
    dataString += "&association_id="+person_id;
    dataString += "&association_type=person";

  }

  dataString += "&model=event";

  var due = jQuery('#due_date_hidden').val();
  var t = due.split(/[-]/);

  // Apply each element to the Date function
  var due_date = new Date(t[0], t[1]-1, t[2]);

  //make ajax call
  jQuery.ajax({
    type  : "POST",
    url   : 'index.php?option=com_crmery&controller=main&model=event&task=save&format=raw&tmpl=component',
    data  : dataString,
    dataType: 'json',
    success : function(data){


      var date = jQuery.datepicker.formatDate(userDateFormat, due_date);

      var owner = owner_first_name + " " + owner_last_name;
      var task = jQuery("input[name=name]").val();

      var html = '<li>';
          html += '<a href="'+base_url+'index.php?option=com_crmery&view=events&layout=event&id='+data.id+'">'
          html += '<div class="ui-li-count"><b>'+date+'</b></div>';
          html += '<h3 class="ui-li-heading"><b>'+task+'</b></h3>';
          html += '<p class="ui-li-desc">'+owner+'</p>';         
         html += '</a></li>';

         jQuery("#events").prepend(html);
         jQuery("#events").listview('refresh');
         jQuery(form).each(function(){
            jQuery(this).val('');
         });

        alert(Joomla.JText._('COM_CRMERY_ADDED_MESSAGE','Added'));
        
    }
  });
  
}

//add note Entries
function addNoteEntry(ele){
  
  //generate data string for ajax call
  var dataString = '';
  if (!ele){
    var form = jQuery('#note :input');  
  }else{
    var form = jQuery('#'+ele+' :input');
  }
  
  jQuery(form).each(function(){
    dataString += "&"+this.name+"="+jQuery(this).val();
  });
  
  //determine which page we are adding a note from
  if ( loc == "company" ){
    dataString += "&company_id="+company_id;
  }
  if ( loc == "deal" ){
    dataString += "&deal_id="+deal_id;
  }
  if ( loc == "person" ){
    dataString += "&person_id="+person_id;
  }

  dataString += "&model=note";

  //make ajax call
  jQuery.ajax({
    type  : "POST",
    url   : 'index.php?option=com_crmery&controller=main&model=note&task=save&format=raw&tmpl=component',
    data  : dataString,
    dataType: 'json',
    success : function(data){

      var today = new Date();
      var date = jQuery.datepicker.formatDate(userDateFormat, today);

      var owner = owner_first_name + " " + owner_last_name;
      var note = jQuery("textarea[name=note]").val();

      var html = '<li>';
          html += '<div class="ui-li-aside"><b>'+date+'</b></div>';
          html += '<h3 class="ui-li-heading"><b>'+owner+'</b></h3>';
          html += '<p class="ui-li-desc">'+note+'</p>';         
         html += '</li>';

         jQuery("#notes").prepend(html);
         jQuery("#notes").listview('refresh');
         jQuery(form).each(function(){
            jQuery(this).val('');
         });

        alert(Joomla.JText._('COM_CRMERY_ADDED_MESSAGE','Added'));
        
    }
  });
  
}

/** Mark events complete **/
function markEventComplete(event_id){

  var completed_value = jQuery("#completed").is(":checked") ? 1 : 0;
  var dataString = "event_id="+event_id+"&completed="+completed_value;

  jQuery.ajax({
      url:'index.php?option=com_crmery&controller=events&task=markComplete&format=raw&tmpl=component',
      type:'post',
      data:dataString,
      dataType:'json',
      success:function(data){
        var event_status = ( completed_value == 1 ) ? "INCOMPLETE" : "COMPLETE";
        jQuery("#completed_label .ui-btn-text").text(Joomla.JText._('COM_CRMERY_MARK_'+event_status));
        event_bool = completed_value == 1 ? true : false;
        jQuery("input[type='checkbox']").prop("checked",event_bool).checkboxradio("refresh");
        alert(Joomla.JText._('COM_CRMERY_ADDED_MESSAGE','Added'));

      }
  });

}

//function to store updated information to user tables    
function save(form){

  var valid = true;

  //Remove Default Placeholder text
  jQuery("[placeholder]").parents("form").submit(function() {
    jQuery(this).find("[placeholder]").each(function() {
      var input = jQuery(this);
      if (jQuery(input).val() == jQuery(input).attr("placeholder")) {
        jQuery(input).val("");
      }
    });
  });
  
  jQuery(form).find(':input').each(function(){
    var input = jQuery(this);
    if ( jQuery(input).hasClass('required') && jQuery(input).val() != "" ){
      if(jQuery(input).hasClass('required_highlight')){
        jQuery(input).removeClass('required_highlight');
      }
    }
    if ( jQuery(input).hasClass('required') && jQuery(input).val() == "" ){
      jQuery(input).addClass('required_highlight');
      jQuery(input).focus();
      valid = false;
    }
  });

  if ( valid ){
    return true;
  }else{
    return false;
  }
      
}