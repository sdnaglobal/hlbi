
jQuery(document).ready(function(){

	if ( typeof company_names != 'undefined' ){
		jQuery("input[name=company]").autocomplete({
			source: function(request, response) {
	        var results = jQuery.ui.autocomplete.filter(company_names, request.term);
	        	response(results.slice(0, 10));
	    	},
			select:function(event,ui){
				jQuery("#company_name").val(ui.item.label);
				jQuery("#company_id").val(ui.item.value);
				return false;
			},
			search:function(){

			}
		});
	}
	
	jQuery(".date_input").datepicker('destroy');
	jQuery(".date_input").datepicker({
			yearRange: '1800:+100',
			dateFormat:userDateFormat,
			changeYear:true,
			changeMonth:true, 
			onSelect:function(event,ui){
				jQuery("#"+jQuery(this).attr('id')+'_hidden').val(ui.selectedYear+"-"+("0"+(ui.selectedMonth+1)).slice(-2)+"-"+("0"+(ui.selectedDay)).slice(-2));
				var form_name = jQuery(this).attr('id')+"_form";
				people_update(form_name);
			}
	});

	
});

//bind work address slide down
	function bind_area(loc){
		jQuery("#"+loc+"_button").fadeOut('fast');
		jQuery("#"+loc+"_info").slideDown('fast');
	}
	
	
	function getData(id)
	{
	  //alert(id);
	  var dataString='';  
	  
	  jQuery.ajax({	 	   
	  
	    type	:	"POST",
		url		:	'index.php?option=com_crmery&controller=main&task=loadpartner&format=raw',
		data	:	'id='+id+'&'+dataString,
		dataType:	'json',
		success	:	function(data){	    

         $.each(data, function (linktext, link) {		 
			//console.log(linktext);
			//console.log(link);
			var Fname=link.name;
			var Lname=link.lname;
			var position=link.position;
			var phone=link.phone;
			var email=link.email;
			
			jQuery('input[name="first_name"]').val(Fname);
            jQuery('input[name="last_name"]').val(Lname);
            jQuery('input[name="position"]').val(position);	
            jQuery('input[name="phone"]').val(phone);				
			jQuery('input[name="email"]').val(email);	
			
			   });		
			   
			   
			   
			   
				
        //var json_obj = jQuery.parseJSON(data);			
		
		//var obj = jQuery.parseJSON('{"id":"9","name":"Test.","lname":"Muller","position":"Firm Partner","phone":"977777777","email":"partner1@gmail.com","values":"","type":"number","required":"0","multiple_selections":"0","created":"2013-09-06 04:10:59","modified":"2013-09-06 04:10:59","ordering":null}');
		
		/* var json = $.parseJSON(obj);
		$(json).each(function(i,val){
			$.each(val,function(k,v){
			
			   alert(k);
			
				  console.log(k+" : "+ v);     
		});
		}); */	
		
		
		/* $.getJSON('index.php?option=com_crmery&controller=main&task=loadpartner&format=raw&id'+id, function(data) {
		var items = [];	 	
		 
			  $.each(data, function (linktext, link) {
			console.log(linktext);
			console.log(link);
			   });
		}); */		
		
		

				//modalMessage(Joomla.JText._('COM_CRMERY_SUCCESS_MESSAGE','Success'), Joomla.JText._('COM_CRMERY_GENERIC_UPDATED','Successfully updated'));
			
		}
		
	});
	  
	}
	
	

		//main function to make all ajax calls
//pass this the FORM name you want submitted
function people_update(loc){

	window.checkLogin();
	
	//generate data string for ajax call
	var dataString = '';
	var $form = jQuery('form[name='+loc+'] :input');
	$form.each(function(){
		dataString += "&"+this.name+"="+jQuery(this).val();
	});
	
	//make ajax call
	jQuery.ajax({
		type	:	"POST",
		url		:	'index.php?option=com_crmery&controller=main&model=people&task=save&format=raw&tmpl=component',
		data	:	'id='+id+'&'+dataString,
		dataType:	'json',
		success	:	function(data){

				modalMessage(Joomla.JText._('COM_CRMERY_SUCCESS_MESSAGE','Success'), Joomla.JText._('COM_CRMERY_GENERIC_UPDATED','Successfully updated'));
			
		}
		
	});
	
}


// Customization for Firm Admin drop down
 
 
 function getXMLHTTP() { //fuction to return the xml http object
		var xmlhttp=false;	
		try{
			xmlhttp=new XMLHttpRequest();
		}
		catch(e)	{		
			try{			
				xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try{
				req = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		 	
		return xmlhttp;
	}	
	
	function getFirmAdmin(strURL) 
	{		
	  //alert(strURL); return false;
	
		un=strURL;
		//alert(un);
		//strURL="";
		//strURL+="index.php?option=com_crmery&controller=main&task=loadcountry&format=raw&region_id="+un;
		//alert(strURL);
		var req = getXMLHTTP();
		//alert(req);
		if (req) 
		{
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('firmadmin').innerHTML=req.responseText;
                        document.getElementById('nofirmadmin').style.display='none';
                        document.getElementById('firmadminid').style.display='block';						
                        document.getElementById('firmadmin').style.display='block';						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			//req.open("GET", strURL, true);
			//req.send(null);
			
			
			var url = "index.php?option=com_crmery&controller=main&task=loadfirmadmin&format=raw&company_id="+un;
			var params = "company_id="+un;
			req.open("post", url, true);
			req.send(params);
			
			
		}		
	}

	
function showResults(val)
{ 
  if(val == '')
  {
	document.getElementById("livesearch").style.display="none";
	return;
  }	
    var e = document.getElementById("countryArr");
	var cntryValue = e.options[e.selectedIndex].value;

	// if(cntryValue == 224)
	// {
		// var s = document.getElementById("state_id");
		// var stateValue = s.options[s.selectedIndex].value;
	// }
	// else 
		 var stateValue = 0;
	
  var res1 = document.getElementById("livesearch");
     $.ajax({
		type	: "POST",
		url	: "index.php?option=com_crmery&controller=main&task=loadfirmdetails&format=raw",
		data	: {"q":val,"cntryID":cntryValue,"state":stateValue}
		}).done(function(html){ $(res1).html(html); });
      document.getElementById("livesearch").style.border="1px solid #A5ACB2";
	  document.getElementById("livesearch").style.display="block";
}
function getfirmdropdownbycountry(val)
{
	  document.getElementById("company_id").value = '';
	   document.getElementById("companyname").value='';
   if(val == '')
   {
	 document.getElementById("firmdropdownbycntry").style.display="none";
	 return;
   }	
   
	var res1 = document.getElementById("firmdropdownbycntry");
     $.ajax({
		type	: "POST",
		url	: "index.php?option=com_crmery&controller=main&task=loadfirmdetailsByCountryID&format=raw",
		data	: {"cntryID":val}
		}).done(function(html){ 
		$('#firmdropdownbycntry').html(html); 
		//alert(html);
		});
     // document.getElementById("livesearch").style.border="1px solid #A5ACB2";
	 // document.getElementById("livesearch").style.display="block";
	  
}
function fillbox(valname)
{
	if(valname == '')
    {
	   document.getElementById("company_id").value = '';
	   document.getElementById("companyname").value='';
	   return;
    }	
   
   var val =  valname.split('~~~');
	document.getElementById("company_id").value = val[0];
	document.getElementById("companyname").value = val[1];
	getFirmAdmin(val[0]);
	
	//dropdown1 = document.getElementById('freindId');
    //var textbox = document.getElementById('textbox');
    //var a = dropdown1.options[dropdown1.selectedIndex].value = val;
	
	
	document.getElementById("livesearch").style.display="none";
}

function showleadinfo(val)
{
 var res1 = document.getElementById("showleadinfo");
	if(val <1)
	  {
	   res1.style.display="none";
		return;
	  }	
	
	 $.ajax({
		type	: "POST",
		url	: "index.php?option=com_crmery&controller=main&task=loadleaddetails&format=raw",
		data	: "leadid="+val
		}).done(function(html){ $(res1).html(html);  });
	  res1.style.border="1px solid #A5ACB2";
	  res1.style.display="block";


}


  function getFirmAdminData(id)
	{
	  //alert(id);
	  var dataString='';  
	  
	 // checkleadassignment(id); //  function to check if this lead is already assign to selected firm admin or not
	 
		var e = document.getElementById("deal_id");
		var dealValue = e.options[e.selectedIndex].value;
		
		$.ajax({
			type	: "POST",
			url	: "index.php?option=com_crmery&controller=main&task=checkleadassignment&format=raw",
			data	: {"dealValue":dealValue,"firmadmin_id":id}
			}).done(function(html){ 
			if(html>0){			
				alert("This Client is already assign to selected firm and firm admin, you can\'t reassign until user decline the first request.");
			 } else{

					  jQuery.ajax({	 	   
						  
							type	:	"POST",
							url		:	'index.php?option=com_crmery&controller=main&task=loadfirmadmindata&format=raw',
							data	:	'id='+id+'&'+dataString,
							dataType:	'json',
							success	:	function(data){	    

							 $.each(data, function (linktext, link) {		 
								//console.log(linktext);
								//console.log(link);
								
								var Fname ="";
								var Lname ="";
								var email ="";
								var company ="";
								
								var Fname=link.first_name;
								var Lname=link.last_name;			
								var email=link.email;
								var company=link.name;
								
								document.getElementById('hideshowfirstdata').style.display='block';	
								  
								jQuery('input[name="first_name"]').val(Fname);
								if(Lname != '') {
									jQuery('input[name="last_name"]').val(Lname);            				     }
								else{
									jQuery('input[name="last_name"]').val(Fname);								}
								jQuery('input[name="email"]').val(email);
								jQuery('input[name="company"]').val(company);			
								
								
									jQuery('#edit_form').find(':input').each(function(){
										var input = jQuery(this);
										if ( jQuery(input).hasClass('required') && jQuery(input).val() != "" ){
											if(jQuery(input).hasClass('required_highlight')){
												jQuery(input).removeClass('required_highlight');
											}
										}
							       });		
	
		
								   });		
									
							}
							
						}); // 
	
				}			 
				//return html; 
			});

			
	
	
	
	  
	}
	
	function checkleadassignment(id)
	{
	
		var e = document.getElementById("deal_id");
		var dealValue = e.options[e.selectedIndex].value;
		
		$.ajax({
			type	: "POST",
			url	: "index.php?option=com_crmery&controller=main&task=checkleadassignment&format=raw",
			data	: {"dealValue":dealValue,"firmadmin_id":id}
			}).done(function(html){ 
			if(html>0){			
				alert("This Client is already assign to selected firm and firm admin, you can\'t reassign until user decline the first request.");
				
				
			}	
				return html; 
			});
			
		 
	  

	
	}
	
	
	
	function getState(strURL) 
	{		
	  //alert(strURL); return false;
	
		un=strURL;
		if(un !=224)
		{
			 document.getElementById('state').style.display='none';		
			return;
		}
		//strURL="";
		//strURL+="index.php?option=com_crmery&controller=main&task=loadcountry&format=raw&region_id="+un;
		//alert(strURL);
		var req = getXMLHTTP();
		//alert(req);
		if (req) 
		{
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('state').innerHTML=req.responseText;
                       // document.getElementById('nostate').style.display='none';
                        document.getElementById('state').style.display='block';						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			//req.open("GET", strURL, true);
			//req.send(null);			
			
			var url = "index.php?option=com_crmery&controller=main&task=loadstate&format=raw&country_id="+un;
			var params = "country_id="+un;
			req.open("post", url, true);
			req.send(params);
			
			
		}		
	} 

