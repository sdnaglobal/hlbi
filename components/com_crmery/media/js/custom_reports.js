var newId = 0;

jQuery(document).ready(function(){

	newId = jQuery("#custom_field_holders").find("ul").children("li").length;
	
	//make items draggable
	draggable();
	
	
	jQuery(".holder").droppable({
		drop:function(event,ui){
			if ( jQuery(ui.draggable).hasClass('data') ){
				jQuery.when(jQuery(this).html(jQuery(ui.draggable).html()))
				.then(function(){ jQuery(this).attr('id',jQuery(ui.draggable).attr('id')); })
				.then(function(){ jQuery(this).attr('name',jQuery(ui.draggable).attr('name')); })
				.then(function(){if ( jQuery(this).attr("id") != "calculate_field" ){jQuery(ui.draggable).remove();}})
				.then(function(){jQuery(this).append('<div class="remove"><a onclick="removeBlock(this)" class="remove"></a></div>');})
				.then(function(){jQuery(this).css('top','0px');})
				.then(function(){jQuery(this).css('left','0px');})
				.then(function(){jQuery(this).fadeIn('fast');})
				.then(function(){jQuery(this).effect("highlight",{},3000);})
				.then(function(){jQuery(this).addClass('added_data');})
				.then(function(){jQuery(this).removeClass('data');})
				.then(function(){jQuery(this).removeClass('highlight_holder');})
				.then(function(){jQuery('.holders').sortable('refresh');})
				.then(function(){jQuery('.holders').sortable('destroy');})
				.then(function(){sortable();});
				if ( jQuery(this).attr("id") == "calculate_field" )
				{
					// var newId = 0;
					// jQuery("#custom_field_holders").find("ul").children("li").each(function(idx,element){
					// 	if ( jQuery(element).hasClass('added_data') ){
					// 		var id = jQuery(element).attr('id');
					// 		if ( id.indexOf('calculate_field') !== -1 )
					// 		{
					// 			newId++;
					// 		}
					// 	}
					// });
					newId++;
					jQuery(this).attr("id","calculate_field_"+newId);
					var html = jQuery("#calculate_field_modal").html();
					jQuery(this).append(html);
					jQuery(this).find("a.edit_calculation_fields_link").attr('id',"edit_calculation_fields_link_"+newId);
					jQuery(this).find("a.hide_calculation_fields_link").attr('id',"hide_calculation_fields_link_"+newId);
					jQuery(this).find("a.remove_calculation_fields_link").attr('id',"remove_calculation_fields_link_"+newId);
					jQuery(this).find("div.calculate_field_modal").attr("id","edit_calculation_fields_modal_"+newId);
					openCalculationFieldsModal(newId);
				}
			}
		},
		over:function(event,ui){
			if ( jQuery(ui.draggable).hasClass('data') ){
				jQuery(this).addClass('highlight_holder');
			}
		},
		out:function(event,ui){
			if ( jQuery(ui.draggable).hasClass('data') ){
				jQuery(this).removeClass("highlight_holder");
			}
		}
	});
	
	//bind delete button to delete custom reports
	jQuery('.delete_custom_report').bind('click',function(){deleteReport(this);});
	
	//make items sortable
	sortable();

	//make calculation fields input sanitized
	sanitize();

	
});

//remove elements from the custom fields blocks
function removeBlock(ele){
	
	//block to remove and re-append to the custom fields columns
	var block = jQuery(ele).parentsUntil('li').parent('li');
    jQuery(block).appendTo('#custom_field_columns ul.columns');
    jQuery(block).css('display','inline-table');
    jQuery(block).addClass('data');
    jQuery(block).removeClass('added_data');
    
    //make items draggable
    draggable();
   
   //add a placeholder
   jQuery("#custom_field_holders ul.holders").append("<li class=\"holder\">"+Joomla.JText._('COM_CRMERY_DRAG_FIELD_HERE')+"</li>");
   
}

//make items draggable
function draggable(){
	
	jQuery(".data").draggable({
		revert:true,
		start:function(event,ui){
			sorting = true;
		},
		stop:function(event,ui){
			sorting = false;
		},
		opacity:.5,
	});
	
}

//make items sortable
function sortable(){
	jQuery('.holders').sortable({});
	jQuery(".sortable").sortable({});
}


//validate custom fields
function validateCustomForm(form){

	var validated = save(jQuery(form));

	if ( validated ){
		var field_count = 0;
		var fields = jQuery('.holders').sortable('toArray');
		var field_data = new Object();
		jQuery(fields).each(function(index,value){
			if ( value.length > 0 ){
				field_data[value] = jQuery("#"+value).attr('name');
			}
		});
		
		
		if ( Object.keys(field_data).length > 0 ){
			
			jQuery(field_data).each(function(index,fields){
				for ( var i in fields){
					if ( i.indexOf('calculate_field_') !== -1 )
					{	
						var id = parseInt(i.replace("calculate_field_",""));
						if ( !checkFieldOperations(id) )
						{
							return false;
						}else{
							var name = fields[i];
							var value = "";
							jQuery("#edit_calculation_fields_modal_"+id).find("div.dropdown-operations ul li").children(":input").each(function(idx,element){
								value += jQuery(element).val();
							});
							jQuery("#post").append("<input type='hidden' name=fields["+i+"] value='"+value+"' />");
							jQuery("#post").append("<input type='hidden' name=calculation_names["+i+"] value='"+name+"' />");
						}						

					}else{

						var value = fields[i];
						jQuery("#post").append("<input type='hidden' name=fields["+i+"] value='"+value+"' />");

					}
				}
			});
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}

	
}

//delete document
function deleteReport(e){

	window.checkLogin();
	
	if ( confirm(Joomla.JText._('COM_CRMERY_ARE_YOU_SURE_DELETE_REPORT')) ){
	
		var search = 'custom_report_';
		var id =jQuery(e).parentsUntil('tr').parent('tr').attr('id').replace(search,'');
		//make ajax call
		jQuery.ajax({
			
			type	:	'post',
			url		:	'index.php?option=com_crmery&controller=reports&task=deleteReport&format=raw&tmpl=component',
			data	:	'id='+id,
			dataType:	'json',
			success	:	function(data){
				if ( !data.error ){
					jQuery.when(jQuery("#custom_report_"+id).fadeOut("slow"))
					.then(function(){
						jQuery("#custom_report_"+id).remove();
					});
					modalMessage(Joomla.JText._(COM_CRMERY_SUCCESS_MESSAGE,'Success'), Joomla.JText._(COM_CRMERY_GENERIC_UPDATED,'Successfully updated'));
				}else{
					//TODO error handling
				}
			}
			
		});
	
	}
}


function editCalculationFields(element)
{	
	var id = jQuery(element).attr("id").replace("edit_calculation_fields_link_","");
	openCalculationFieldsModal(id);
}

function openCalculationFieldsModal(id)
{

	if ( !jQuery("#edit_calculation_fields_modal_"+id).is(":visible") ){
		jQuery.fx.speeds._default = 1000;
		jQuery("#edit_calculation_fields_modal_"+id).dialog({
			
			dialogClass:'com_crmery',
			autoOpen:true,
			resizable: false,
			minHeight: 60,
			modal:true,
			minWidth:500

		});
		jQuery("#edit_calculation_fields_modal_"+id).dialog('show');
	}
	jQuery("#edit_calculation_fields_modal_"+id).find('.calculation_field_name').bind('keyup',function(){
		updateName(this,id);
	});


}

function updateName(element,id)
{
	var name = jQuery(element).val().substring(0,15);
	id = id > 0 ? id : jQuery(this).data('field-id');
		jQuery("#calculate_field_"+id).find('span.field-label').html(name);
		jQuery("#calculate_field_"+id).attr('name',name);
}

function hideCalculationFields(element)
{	
	var id = jQuery(element).attr("id").replace("hide_calculation_fields_link_","");
	if ( checkFieldOperations(id) ){
		jQuery("#edit_calculation_fields_modal_"+id).dialog('close');
	}
}

function removeCalculationField(element)
{
	var id = jQuery(element).attr("id").replace("remove_calculation_fields_link_","");
	jQuery("#edit_calculation_fields_modal_"+id).dialog('close');
	jQuery("#calculate_field_"+id).remove();
   
   //add a placeholder
   jQuery("#custom_field_holders ul.holders").append("<li class=\"holder\">"+Joomla.JText._('COM_CRMERY_DRAG_FIELD_HERE')+"</li>");

   //make items draggable
    draggable();
}

function addField(element)
{
	var html = jQuery("#dropdown-template-field").html();
	jQuery(element).parentsUntil('.calculate_field_modal').parent('.calculate_field_modal').find('.dropdown-operations ul').append(html);
	sortable();
	sanitize();
}

function addOperator(element)
{
	var html = jQuery("#dropdown-template-operator").html();
	jQuery(element).parentsUntil('.calculate_field_modal').parent('.calculate_field_modal').find('.dropdown-operations ul').append(html);
	sortable();
	sanitize();
}

function addInput(element)
{
	var html = jQuery("#dropdown-template-input").html();
	jQuery(element).parentsUntil('.calculate_field_modal').parent('.calculate_field_modal').find('.dropdown-operations ul').append(html);
	sortable();
	sanitize();
}

function deleteField(element)
{
	jQuery(element).parent('li').fadeOut('fast').remove();
}

function sanitize()
{
	jQuery("input[name=calculation_field_input]").unbind();
	jQuery("input[name=calculation_field_input]").on("keypress change", (function (e){
		  var charCode = (e.which) ? e.which : e.keyCode;
		  var ret = true;
		  //check for anything other than numbers or parentheses
		  if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 40 && charCode != 41 && charCode != 46) {
		    ret = false;
		  }
		  return ret;
	}));
}

function editFields(element)
{
	if ( !jQuery(element).hasClass('editing') ){
		jQuery(element).addClass('editing');
		jQuery(element).parentsUntil('.calculate_field_modal').parent('.calculate_field_modal').find(".hand").show().css('display','inline-block');
		jQuery(element).parentsUntil('.calculate_field_modal').parent('.calculate_field_modal').find(".delete").show().css('display','inline-block');
	}else{
		jQuery(element).removeClass('editing');
		jQuery(element).parentsUntil('.calculate_field_modal').parent('.calculate_field_modal').find(".hand").hide().css('display','none');
		jQuery(element).parentsUntil('.calculate_field_modal').parent('.calculate_field_modal').find(".delete").hide().css('display','none');
	}
}

function checkFieldOperations(id)
{	

	var name = jQuery("#edit_calculation_fields_modal_"+id+" .calculation_field_name").val();
	var value = "";
	var openCount = 0;
	var closedCount = 0;
	var fieldCount = 0;
	var opCount = 0;
	var fieldTemp = false;
	var opTemp = false;
	var totalFields = 0;

	jQuery("#edit_calculation_fields_modal_"+id).find("div.dropdown-operations ul li").children(":input").each(function(idx,element){
		if ( jQuery(element).attr('name') == 'calculation_field' ){
			if ( fieldTemp != false )
			{
				fieldCount++;
				fieldTemp = false;
				opTemp = false;
			}else{
				fieldTemp = true;
				opTemp = false;
			}

			totalFields++;

		}else if ( jQuery(element).attr('name') == 'operation_field' ){
			if ( opTemp != false )
			{
				opCount++;
				opTemp = false;
				fieldTemp = false;
			}else{
				opTemp = true;
				fieldTemp = false;
			}

		}else if ( jQuery(element).attr('name') == 'calculation_field_input' ){
			var str = jQuery(element).val();
			var length = str.length;
			if ( length == 0 || str == "" )
			{	

				window.modalMessage(Joomla.JText._("COM_CRMERY_INVALID_FIELD_LENGTH"));
				openCalculationFieldsModal(id);
				jQuery(element).focus();
				return false;
			}
			for ( p=0; p<length; p++ ){
				if ( str.charAt(p)=="(" )
				{	
					openCount++;
				}
				if ( str.charAt(p)==")" )
				{
					closedCount++;
					if ( closedCount > openCount )
					{
						openCalculationFieldsModal(id);
						window.modalMessage(Joomla.JText._("COM_CRMERY_INVALID_PARENTHESES"));
						jQuery(element).focus();
					}
					return false;
				}
			}

		}
	});

	if ( ( openCount > closedCount ) || 
		 ( closedCount > openCount) )
	{	
		openCalculationFieldsModal(id);
		window.modalMessage(Joomla.JText._("COM_CRMERY_INVALID_PARENTHESES"));
		return false;
	}

	if ( fieldCount > 0 )
	{
		openCalculationFieldsModal(id);
		window.modalMessage(Joomla.JText._("COM_CRMERY_INVALID_OPERATIONS"));
		return false;
	}
	if ( opCount > 0 )
	{
		openCalculationFieldsModal(id);
		window.modalMessage(Joomla.JText._("COM_CRMERY_INVALID_OPERATIONS"));
		return false;
	}
	if ( !( totalFields > 0 ) )
	{
		openCalculationFieldsModal(id);
		window.modalMessage(Joomla.JText._('COM_CRMERY_NO_FIELDS_DEFINED'));
		return false;
	}
	if ( name.length == 0 )
	{
		openCalculationFieldsModal(id);
		window.modalMessage(Joomla.JText._("COM_CRMERY_NAME_YOUR_FIELD"));
		return false;
	}

	return true;


}