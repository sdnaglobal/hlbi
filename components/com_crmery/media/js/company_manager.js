jQuery(document).ready(function(){

	if ( typeof company_names != 'undefined' ){
		jQuery("input[name=company]").autocomplete({
			source:company_names,
			select:function(event,ui){
				jQuery("#company_name").val(ui.item.label);
				jQuery("#company_id").val(ui.item.value);
				return false;
			},
			search:function(){
				jQuery("#company_name").val(ui.item.label);
				jQuery("#company_id").val(ui.item.value);
				return false;
			}
		});
	}

	jQuery(".date_input").datepicker('destroy');
	jQuery(".date_input").datepicker({
			yearRange: '1800:+100',		
			dateFormat:userDateFormat,
			changeYear:true,
			changeMonth:true, 
			onSelect:function(event,ui){
				jQuery("#"+jQuery(this).attr('id')+'_hidden').val(ui.selectedYear+"-"+("0"+(ui.selectedMonth+1)).slice(-2)+"-"+("0"+(ui.selectedDay)).slice(-2));
				var form_name = jQuery(this).attr('id')+"_form";
				company_update(form_name);
			}
	});
	
});

//bind work address slide down
	function bind_area(loc){
		jQuery("#"+loc+"_button").fadeOut('fast');
		jQuery("#"+loc+"_info").slideDown('fast');
	}


	//main function to make all ajax calls
//pass this the FORM name you want submitted
function company_update(loc){
	
	//generate data string for ajax call
	var dataString = '';
	var $form = jQuery('form[name='+loc+'] :input');
	$form.each(function(){
		dataString += "&"+this.name+"="+jQuery(this).val();
	});
	
	//make ajax call
	jQuery.ajax({
		type	:	"POST",
		url		:	'index.php?option=com_crmery&controller=main&model=company&task=save&format=raw&tmpl=component',
		data	:	'id='+id+'&'+dataString,
		dataType:	'json',
		success	:	function(data){

				modalMessage(Joomla.JText._('COM_CRMERY_SUCCESS_MESSAGE','Success'), Joomla.JText._('COM_CRMERY_GENERIC_UPDATED','Successfully updated'));
			
		}
		
	});
	
}

 // Customization for country drop down
 
 
 function getXMLHTTP() { //fuction to return the xml http object
		var xmlhttp=false;	
		try{
			xmlhttp=new XMLHttpRequest();
		}
		catch(e)	{		
			try{			
				xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try{
				req = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		 	
		return xmlhttp;
	}	
	
	function getCountry(strURL) 
	{		
	  //alert(strURL); return false;
	
		un=strURL
		//strURL="";
		//strURL+="index.php?option=com_crmery&controller=main&task=loadcountry&format=raw&region_id="+un;
		//alert(strURL);
		var req = getXMLHTTP();
		//alert(req);
		if (req) 
		{
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('country').innerHTML=req.responseText;
                        document.getElementById('nocountry').style.display='none';
                        document.getElementById('country').style.display='block';						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			//req.open("GET", strURL, true);
			//req.send(null);
			
			
			var url = "index.php?option=com_crmery&controller=main&task=loadcountry&format=raw&region_id="+un;
			var params = "region_id="+un;
			req.open("post", url, true);
			req.send(params);
			
			
		}		
	}
 
  function getState(strURL) 
	{		
	  //alert(strURL); return false;
	
		un=strURL;
		if(un !=224)
		{
			document.getElementById('statetextbox').style.display='block';
			document.getElementById('state').style.display='none';		
			return;
		}
		//strURL="";
		//strURL+="index.php?option=com_crmery&controller=main&task=loadcountry&format=raw&region_id="+un;
		//alert(strURL);
		var req = getXMLHTTP();
		//alert(req);
		if (req) 
		{
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('state').innerHTML=req.responseText;
                       // document.getElementById('nostate').style.display='none';
                        document.getElementById('state').style.display='block';						
                        document.getElementById('statetextbox').style.display='none';						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			//req.open("GET", strURL, true);
			//req.send(null);			
			
			var url = "index.php?option=com_crmery&controller=main&task=loadstate&format=raw&country_id="+un;
			var params = "country_id="+un;
			req.open("post", url, true);
			req.send(params);
			
			
		}		
	} 
 
 
 
 function getCountry___(id)
	{
	  //alert(id);
	  var dataString='';  
	  
	  jQuery.ajax({	 	   
	  
	    type	:	"POST",
		url		:	'index.php?option=com_crmery&controller=main&task=loadcountry&format=raw',
		data	:	'region_id='+id+'&'+dataString,
		dataType:	'json',
		success	:	function(data){	    
		
		//alert('test');
		//alert(data);
		
		jQuery('#country').val=data;
		

         $.each(data, function (linktext, link) {		 
			console.log(linktext);
			console.log(link);
			var Fname=link.name;
			var Lname=link.lname;
			var position=link.position;
			var phone=link.phone;
			var email=link.email;
			
			jQuery('input[name="first_name"]').val(Fname);
            jQuery('input[name="last_name"]').val(Lname);
            jQuery('input[name="position"]').val(position);	
            jQuery('input[name="phone"]').val(phone);				
			jQuery('input[name="email"]').val(email);	
			
			   });		
			   
			   
			   
			   
				
        //var json_obj = jQuery.parseJSON(data);			
		
		//var obj = jQuery.parseJSON('{"id":"9","name":"Test.","lname":"Muller","position":"Firm Partner","phone":"977777777","email":"partner1@gmail.com","values":"","type":"number","required":"0","multiple_selections":"0","created":"2013-09-06 04:10:59","modified":"2013-09-06 04:10:59","ordering":null}');
		
		/* var json = $.parseJSON(obj);
		$(json).each(function(i,val){
			$.each(val,function(k,v){
			
			   alert(k);
			
				  console.log(k+" : "+ v);     
		});
		}); */	
		
		
		/* $.getJSON('index.php?option=com_crmery&controller=main&task=loadpartner&format=raw&id'+id, function(data) {
		var items = [];	 	
		 
			  $.each(data, function (linktext, link) {
			console.log(linktext);
			console.log(link);
			   });
		}); */		
		
		

				//modalMessage(Joomla.JText._('COM_CRMERY_SUCCESS_MESSAGE','Success'), Joomla.JText._('COM_CRMERY_GENERIC_UPDATED','Successfully updated'));
			
		}
		
	});
	  
	}





