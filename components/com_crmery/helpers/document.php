<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 class CrmeryHelperDocument extends JObject
 {
    
    //get users total associated documents
    function getTotalDocuments(){
        
        //db
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //select
        $query->select("count(*)");
        $query->from("#__crmery_documents AS d");
        
        //filter depending on user access
        // $role = CrmeryHelperUsers::getRole();
        $user_id = CrmeryHelperUsers::getUserId();
        // $team_id = CrmeryHelperUsers::getTeamId();

        // if ( $role != 'exec' ){
        //     if ( $role == 'manager' ){
        //         $query->join('inner','#__crmery_users AS u ON d.owner_id = u.id');
        //         $query->where('u.team_id='.$team_id);
        //     }else{
        //         $query->where(array("d.owner_id=".$user_id,'d.shared=1'),'OR');
        //     }
        // }
        
        $query->where(array("d.owner_id=".$user_id,'d.shared=1'),'OR');
        $query->where("d.published=1");
        
        //return count
        $db->setQuery($query);
        $result = $db->loadResult();
        return $result;
    }
    
    //function to get possible document association types
    function getAssocTypes(){
        return array(   'all'=>CRMText::_('COM_CRMERY_ALL_DOCUMENTS'),
                        'deals'=>CRMText::_('COM_CRMERY_DOCUMENTS_DEALS'),
                        'people'=>CRMText::_('COM_CMRERY_DOCUMENTS_PEOPLE'),
                        'companies'=>CRMText::_('COM_CRMERY_DOCUMENTS_COMPANIES'),
                        'emails'=>CRMText::_('COM_CRMERY_DOCUMENTS_EMAILS'),
                        'shared'=>CRMText::_('COM_CRMERY_DOCUMENTS_SHARED'));
    }
    
    //get different document doctypes
    function getDocTypes(){
        return array(   'all'=>CRMText::_('COM_CRMERY_ALL_TYPES'),
                        'spreadsheets'=>CRMText::_('COM_CRMERY_SPREADSHEETS'),
                        'images'=>CRMText::_('COM_CRMERY_IMAGES'),
                        'documents'=>CRMText::_('COM_CRMERY_DOCUMENTS'),
                        'pdfs'=>CRMText::_('COM_CRMERY_PDFS'),
                        'presentations'=>CRMText::_('COM_CRMERY_PRESENTATIONS'),
                        'others'=>CRMText::_('COM_CRMERY_OTHERS'));
    }
    
 }