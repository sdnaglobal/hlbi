<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 class CrmeryHelperDropdown extends JObject
 {

 	function generateDropdown($type,$selection=null,$name=null, $raw=false) {
	
	
	
	
	$person = $this->person;
	
	$companyId = $person['company_id'];
 		//base html
 		$html = ''; 
		
		//grab db
		$db = JFactory::getDbo();
		
		//generate query based on type
 		$query = $db->getQuery(true);
		
		 $member_id = CrmeryHelperUsers::getUserId();
	            $member_role = CrmeryHelperUsers::getRole();
	            $team_id = CrmeryHelperUsers::getTeamId();
				
		switch ( $type ) {
			case "company":
				$query->select('id,name FROM #__crmery_companies AS c where c.published > 0');
				break;
			case "usercompany":
				if($member_role == 'manager' || $member_role == 'basic' )
					$query->select('id,name FROM #__crmery_companies AS c where c.partner_id='.$member_id.' and c.published > 0');
				else
				{
					$dbs =& JFactory::getDbo();
					$querys = $dbs->getQuery(true);
					$querys->select("r.region_id,r.country_id")
					->from("#__crmery_users AS r");
					$querys->where("r.id='".$member_id."'");						

					$dbs->setQuery($querys);
					$regIds = $dbs->loadObjectList();
					
					if ( count($regIds) > 0 ){
						foreach ( $regIds as $regId ){						
							$regionId=$regId->region_id;						
						  }
					 }	
					 
					$query->select('id,name FROM #__crmery_companies AS c where (c.region_id IN('.$regionId.') or c.owner_id='.$member_id.') and c.published > 0');
					}
				
				break;

				case "usercompanyedit":
				if($member_role == 'manager' ){
				
					//$query->select('id,name FROM #__crmery_companies AS c where c.partner_id='.$member_id.' and c.published > 0');
				//$query->select('c.id,c.name FROM #__crmery_companies AS c where (c.id=(SELECT company_id  FROM #__crmery_users where id='.$member_id.') or partner_id='.$member_id.') and c.published > 0');
				//$query->select('c.id,c.name FROM #__crmery_companies AS c where (c.id= '.$selection.') and c.published > 0');
				if($selection != '')
				{
					$query->select('c.id,c.name FROM #__crmery_companies AS c where (c.id= '.$selection.') and c.published > 0');
				}
				else
				{
					$query->select('c.id,c.name FROM #__crmery_companies AS c where (c.id=(SELECT company_id  FROM #__crmery_users where id='.$member_id.') or partner_id='.$member_id.') and c.published > 0');
				}
				
					//echo $query;
				}elseif($member_role == 'basic')	
				{
				
					$query->select('c.id,c.name FROM #__crmery_companies AS c where (c.id=(SELECT inp_company_id  FROM #__crmery_users where id='.$member_id.') or partner_id='.$member_id.') and c.published > 0');
				}
				else
				{
				
					$dbs =& JFactory::getDbo();
					$querys = $dbs->getQuery(true);
					$querys->select("r.region_id,r.country_id")
					->from("#__crmery_users AS r");
					$querys->where("r.id='".$member_id."'");						

					$dbs->setQuery($querys);
					$regIds = $dbs->loadObjectList();
					
					if ( count($regIds) > 0 ){
						foreach ( $regIds as $regId ){						
							$regionId=$regId->region_id;						
						  }
					 }	
					 
					$query->select('id,name FROM #__crmery_companies AS c where (c.region_id IN('.$regionId.') or c.owner_id='.$member_id.') and c.published > 0');
					}
				
				break;
				
			case "partner":
				$query->select('id,name,lname FROM #__crmery_company_partners');
				$query->order('name');
				break;	
            case "firmadmin":
				$query->select('id,first_name,last_name FROM #__crmery_users where role_type="manager" AND company_id='.$companyId);
				$query->order('first_name');
				break;	

				
			case "stage":
				$query->select('id,name FROM #__crmery_stages');
				$query->order('ordering');
				break;
			case "source":
				$query->select('id,name FROM #__crmery_sources');
				$query->order('ordering');
				break;
			case "deal_status":
				$query->select('id,name FROM #__crmery_deal_status');
				$query->order('ordering');
				break;
			case "people_status":
				$query->select('id,name FROM #__crmery_people_status');
				$query->order('ordering');
				break;
			case "deal": 
				$query->select('d.*');
				$query->from("#__crmery_deals AS d");
				$query->where("d.published > 0");
				$query->order('name');
				//$query->leftJoin('#__crmery_users AS user ON user.id = d.owner_id');
				/** ---------------------------------------------------------------
	             * Filter data using member role permissions
	             */
	           
	            if ( $member_role != 'exec'){
	                 //manager filter
					  
						if($member_role == 'manager') 
							 {$field="company_id"; } 
						 if($member_role == 'basic')
						 {
						   $field="inp_company_id";
						 }
						  						
						
						 $db =& JFactory::getDbo();
                          $querysu = $db->getQuery(true);
						   $querysu->select("u.$field")
						   ->from("#__crmery_users AS u");
						   $querysu->where("u.id=".CrmeryHelperUsers::getLoggedInUser()->id);
						  
						   $db->setQuery($querysu);
							$comId = $db->loadObjectList();
							if ( count($comId) > 0 ){
							foreach ( $comId as $comid ){						
								$cmId=$comid->$field;						
							  }
							}
							
							
					 // get firm that assigned to user 
						   $dbcc =& JFactory::getDbo();
						   $querycc = $dbcc->getQuery(true);
						   $querycc->select("cc.id")
						   ->from("#__crmery_companies AS cc");
						   $querycc->where("cc.partner_id=".CrmeryHelperUsers::getLoggedInUser()->id);
						  
						   $dbcc->setQuery($querycc);
						   $assigncomId = $dbcc->loadObjectList();
							
							if ( count($assigncomId) > 0 ){
							$id_cmId = array();
								foreach ( $assigncomId as $assigId ){						
									$id_cmId[]=$assigId->id;						
								  }
								
							} 	
							 if($cmId > 0) 
								$id_cmId[]=$cmId;
						  
						if(count($id_cmId) > 0)
						{						 
							$allids = implode(',',$id_cmId);
							$query->where("(d.owner_id = ".$member_id." or d.company_id IN($allids))");
							//echo '<br>-->'.$query; 
						}
						else
							$query->where("d.owner_id = ".$member_id);						
						
						 
	                if ( $member_role == 'manager' ){
	                    //$query->where("(user.team_id = ".$team_id." or d.company_id IN($allids))");
	                }else{
	                //basic user filter
	                    //$query->where("(d.owner_id = ".$member_id." or d.company_id IN($allids))");
	                }
	            }
				//echo $query;
			break;
		}
		
		//run query and grab results
		if ( $query!="" ){
			$db->setQuery($query);
			//echo $query;
			$row = $db->loadAssocList();
		}

		if ( !isset($row) ){
			$row = array();
		} else if ( !is_array($row) && !(count($row) > 0)){
			$row = array();
		}
		
		if($raw) {
			return $row;
		}

		//determine which kind of dropdown we are generating
		$selected = ( $selection == null ) ? "selected='selected'" : '';
 		switch ( $type ) {
			case "company":
				$name = $name ? $name : "name=company_id";
				$html = '
					<select class="inputbox" '.$name.' id="company_id" onChange="getFirmAdmin(this.value);">';
						$html .= "<option value='' ".$selected.">Select Firm";
						foreach ( $row as $company => $info ){
							$selected = ( $info['id'] == $selection ) ? "selected='selected'" : '';
							$html .= '<option value="'.$info['id'].'" '.$selected.' >'.$info['name'].'</option>';
						}
				$html .= '</select>';
				break;
				case "usercompany":
				$name = $name ? $name : "name=company_id";
				$html = '
					<select class="inputbox" '.$name.' id="company_id" onChange="getFirmAdmin(this.value);">';
						$html .= "<option value='' ".$selected.">Select Firm";
						foreach ( $row as $company => $info ){
							$selected = ( $info['id'] == $selection ) ? "selected='selected'" : '';
							$html .= '<option value="'.$info['id'].'" '.$selected.' >'.$info['name'].'</option>';
						}
				$html .= '</select>';
				break;
				
				case "usercompanyedit":
				$name = $name ? $name : "name=company_id";
				$html = '
					<select class="inputbox" '.$name.' id="company_id" >';
						//$html .= "<option value='' ".$selected.">Select Firm";
						
						foreach ( $row as $company => $info ){
							$selected = ( $info['id'] == $selection ) ? "selected='selected'" : '';
							$html .= '<option value="'.$info['id'].'" '.$selected.' >'.$info['name'].'</option>';
						}
				$html .= '</select>';
				break;
				
				
			case "partner":
				$name = $name ? $name : "name=partner_id";
				$html = '
					<select class="inputbox" '.$name.' id="partner_id" onchange="getData(this.value)">';
						$html .= "<option value='0' ".$selected.">Select Partner";
						foreach ( $row as $partner => $info ){
							$selected = ( $info['id'] == $selection ) ? "selected='selected'" : '';
							$html .= '<option value="'.$info['id'].'" '.$selected.' >'.$info['name'].' '.$info['lname'].'</option>';
						}
				$html .= '</select>';
				break;

            case "firmadmin":
				$name = $name ? $name : "name=firmadmin_id";
				$html = '
					<select class="inputbox" '.$name.' id="firmadmin_id" name="firmadmin_id" onchange="getFirmAdminData(this.value)">';
						$html .= "<option value='0' ".$selected.">Select Firm Admin";
						foreach ( $row as $partner => $info ){
							$selected = ( $info['id'] == $selection ) ? "selected='selected'" : '';
							$html .= '<option value="'.$info['id'].'" '.$selected.' >'.$info['first_name'].' '.$info['last_name'].'</option>';
						}
				$html .= '</select>';
				break;

				
				
			case "stage":
				$name = $name ? $name : "name=stage_id";
				$html = '
			 		<select class="inputbox" '.$name.' id="stage_id">';
						$html .= "<option value='0' ".$selected.">Select stage";
						foreach ( $row as $stage => $info ) {
			 				$selected = ( $info['id'] == $selection ) ? "selected='selected'" : '';
							$html .= '<option value="'.$info['id'].'" '.$selected.' '.$name.' >'.$info['name'].'</option>';
						}
						
					$html .='</select>';
				break;
			case "source":
				$name = $name ? $name : "name=source_id";
				$html = '<select class="inputbox" '.$name.' id="source_id">';
						$html .= "<option value='0' ".$selected.">Select source";
						if(count($row) > 0) {
							foreach ( $row as $source => $info ) {
				 				$selected = ( $info['id'] == $selection ) ? "selected='selected'" : '';
								$html .= '<option value="'.$info['id'].'" '.$selected.' '.$name.' >'.$info['name'].'</option>';
							}
						}
					$html .='</select>';
				break;
			case "probability":
				$name = $name ? $name : "name=probability";
				$html = '
					<select class="inputbox" '.$name.' id="probability_id">';
						$html .= "<option value='0' ".$selected.">Select probability";
						for( $i=5; $i<=95; $i+=5 ){
								$selected = ( $i == $selection ) ? "selected='selected'" : '';
								$html .= '<option value="'.$i.'" '.$selected.' '.$name.' >'.$i.'%</option>';
							}
				$html .= '</select>';
				break;
			case "deal_status":
				$name = $name ? $name : "name=status_id";
				$html = '
					<select class="inputbox" '.$name.' id="status_id">';
					$html .= "<option value='0' ".$selected.">Select status...";
						foreach ( $row as $status => $info ) {
			 				$selected = ( $info['id'] == $selection ) ? "selected='selected'" : '';
							$html .= '<option value="'.$info['id'].'" '.$selected.' '.$name.' >'.$info['name'].'</option>';
						}
						
					$html .='</select>';
				break;
			case "people_status":
				$name = $name ? $name : "name=status_id";
				$html = '
					<select class="inputbox" '.$name.' id="status_id">';
					$html .= "<option value='0' ".$selected.">Select status...";
						foreach ( $row as $status => $info ) {
			 				$selected = ( $info['id'] == $selection ) ? "selected='selected'" : '';
							$html .= '<option value="'.$info['id'].'" '.$selected.' '.$name.' >'.$info['name'].'</option>';
						}
						
					$html .='</select>';
				break;
			case "deal":
				$name = $name ? $name : "name=deal_id";
				$html = '
					<select  class="inputbox required" '.$name.' id="deal_id" onchange="showleadinfo(this.value)">';
					//$html .= "<option value='0' ".$selected.">Select Client";
					$html .= "<option value='' ".$selected.">Select Client";
						foreach ( $row as $deal => $info ) {
			 				$selected = ( $info['id'] == $selection ) ? "selected='selected'" : '';
							$html .= '<option value="'.$info['id'].'" '.$selected.' '.$name.' >'.$info['name'].'</option>';
							//$html .='<div id="'.$info['id'].'"> '.$info['name'].',</div>';
						}
						
					$html .='</select>';
					
					
				break;

			default:

				$model = CRMeryHelperDropdown::getModelFromType($type);

				$html 	 = '<ul>';
				$html 	.= '<li><a href="javascript:void(0)" onclick="saveAjax(\''.$type.'\',\''.$model.'\',\'Lead\')">'.CRMText::_('COM_CRMERY_PERSON_LEAD').'</a></li>';
				$html 	.= '<li><a href="javascript:void(0)" onclick="saveAjax(\''.$type.'\',\''.$model.'\',\'Contact\')">'.CRMText::_('COM_CRMERY_PEOPLE_CONTACT').'</a></li>';
				$html 	.= "</ul>";
				
				break;
		}
		
		return $html;
 
 	}

 	function getModelFromType($type) {
 		if(stripos($type,'person')!==false) {
 			$model = 'people';
 		}

 		return $model;

 	}

        /**
         * Get Leaderboards
         * @param none
         * @return mixed $list goals with leaderboards matched
         */
        function getLeaderBoards(){
            //load database
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            
            //load goals associate with user depending on team//role that have a leaderboard flag in the database
            $query->select("g.*")->from("#__crmery_goals AS g")->where("g.leaderboard=1");
            
            //return goals
            $db->setQuery($query);
            $results = $db->loadAssocList();
            
            //generate dropdown list array
            $list = array();

            if(count($results) > 0) {
	            foreach ( $results as $key=>$goal ){
	                $list[$goal['id']] = $goal['name']; 
	            }
			}
			            
            //return results
            return $list;
        }
        
        /**
         * Get team names for dropdowns
         * @param mixed $return array of team names for dropdown
         */
        function getTeamNames(){
            
            //get all teams
            $teams = CrmeryHelperUsers::getTeams();
            
            //generate array
            $return = array();
            $managerTeam = CrmeryHelperUsers::getTeamId();
            if ( is_array($teams) && count($teams) > 0 ){
	            foreach( $teams as $key=>$value ){
	                $return[$value['team_id']] = $value['team_name'] . CRMText::_('COM_CRMERY_TEAM_APPEND');
	            }
	        }
	        unset($return[$managerTeam]);
            
            //return array
            return $return;
            
        }
        
        /**
         * Get user names for dropdowns
         * @param mixed $return array of user names for dropdown
         */
        function getUserNames(){
            
            //get all teams
            $users = CrmeryHelperUsers::getUsers();
            
            //generate array
            $return = array();
            if ( is_array($users) && count($users) > 0 ){
	            foreach( $users as $key=>$value ){
	                $return[$value['id']] = $value['first_name'] . ' ' . $value['last_name'];
	            }
        	}
            
            //return array
            return $return;
            
        }

        function getOwnerDropdown($selected=null,$name="owner_id",$class="class='inputbox'")
        {

        	$users = self::getUserNames();

            $arr = array();
            foreach ( $users as $value => $label ){
              $arr[] = JHTML::_('select.option', $value, $label);
            }
            return JHTML::_('select.genericlist', $arr, $name, $class, 'value', 'text', $selected);

        }

        /**
         * Get person contact types
         * @param  [type] $contact_types [description]
         * @return [type]                [description]
         */
        
        function getContactTypes($contact_type_name=null){

        	$contact_types = array('contact' => CRMText::_('COM_CRMERY_CONTACT'), 'lead' => CRMText::_('COM_CRMERY_LEAD'));

        	if ( !in_array($contact_type_name,$contact_types) ){
        		$currentValue = '0'; //Set this value from DB, etc. 
				$arr = array();
				foreach ( $contact_types as $name => $value ){
				  $arr[] = JHTML::_('select.option', $name, $value);
				}
				return JHTML::_('select.genericlist', $arr, 'type', 'class="inputbox"', 'value', 'text', $currentValue);
        	}else{
        		return $contact_type_name;
        	}

        }

        function getPeopleList(){
        	//open model
            $model = & JModel::getInstance('people','CrmeryModel');
            //retrieve all people
            $people = $model->getPeopleList();
            $people_list = array();
            if ( count($people) ){
                foreach ( $people as $index => $row ){
                    $people_list[$row['id']] = $row['first_name'].' '.$row['last_name'];
                }
            }
            
            return $people_list;
        }

        //load the navigation menu
        function getMemberRoles(){
            return array(   ' '=>'HLB Admin',
							'exec'=>'Federation Admin',
                            'manager'=>'Firm Admin',
                            'basic'=>'Firm Inputer'    );
        }
        
        
        //load teams to assign to users
        public function getTeams($team=null){
            //get database
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            //query string
            //u.id//
            $query->select("t.team_id AS id,u.first_name,u.last_name,u.team_id,IF(t.name!='',t.name,CONCAT(u.first_name,' ',u.last_name)) AS team_name");
            $query->from("#__crmery_users AS u");
            $query->leftJoin("#__crmery_teams AS t on t.leader_id = u.id");
            $query->where("u.role_type='manager'");

            // $query->where("u.published=1");

            //get results
            $db->setQuery($query);
            $results = $db->loadAssocList();
            //generate users object
            $users = array();
            if ( is_array($results) && count($results) > 0 ){
	            foreach ( $results as $key=>$user ){
	                $users[$user['id']] = $user['team_name'];
	            }
	        }
	        if( $team > 0 ){
	        	// unset($users[$team]);
	        }
            //return
            return $users;
        }
        
        public function getManagers($remove=null){
            //get database
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            //query string
            $query->select("u.id,u.first_name,u.last_name,u.team_id");
            $query->from("#__crmery_users AS u");
            $query->where("u.role_type='manager'");

            // $query->where("u.published=1");
            
            //get results
            $db->setQuery($query);
            $results = $db->loadAssocList();
            //generate users object
            $users = array();
            foreach ( $results as $key=>$user ){
                if ( $user['id'] == $remove ){
                    unset($results[$key]);
                }else{
                    $users[$user['id']] = $user['first_name'] . ' ' . $user['last_name'];
                }
            }
            //return
            return $users;
        }
        
        function getSources(){
            return array(  'per' => 'Per Lead/Deal',
                            'flat'=> 'Flat Fee'
                            );
        }
        
        
        function getTemplateTypes(){
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);

            $query->select("id,name")->from("#__crmery_events_categories");
            $db->setQuery($query);

            $query->order('ordering');

            $types = $db->loadObjectList();
            $return = array();

            if ( $types ){
            	foreach ( $types as $type ){
            		$return[$type->id] = $type->name;
            	}
            }

            return $return;
        }

        function showImportTypes($selected="deals",$name="import_type",$class='class="inputbox"'){

            $import_types = array(
                'deals'=>JText::_('COM_CRMERY_DEALS'),
                'people'=>JText::_('COM_CRMERY_PEOPLE'),
                'companies'=>JText::_('COM_CRMERY_COMPANIES')
                );

            $arr = array();
            foreach ( $import_types as $value => $label ){
              $arr[] = JHTML::_('select.option', $value, $label);
            }
            return JHTML::_('select.genericlist', $arr, $name, $class, 'value', 'text', $selected);

        }

        function getFormTypes($selected="lead",$name="type",$class="class='inputbox'"){

        	$import_types = array(
                'lead'=>JText::_('COM_CRMERY_LEAD'),
                'contact'=>JText::_('COM_CRMERY_CONTACT')
                // 'company'=>JText::_('COM_CRMERY_COMPANY')
                // 'deal'=>JText::_('COM_CRMERY_DEAL')
                );

            $arr = array();
            foreach ( $import_types as $value => $label ){
              $arr[] = JHTML::_('select.option', $value, $label);
            }
            return JHTML::_('select.genericlist', $arr, $name, $class, 'value', 'text', $selected);

 		}

 		function getFormFields($type){
 			$arr = array();
 			
 			switch ( $type ){
 				case "people":
 					$base = array(
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_PERSON_FIRST')),'name'=>'first_name','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_PERSON_LAST')),'name'=>'last_name','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_PERSON_COMPANY')),'name'=>'company_name','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_PERSON_POSITION')),'name'=>'position','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_PERSON_PHONE')),'name'=>'phone','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_PERSON_EMAIL')),'name'=>'email','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_PERSON_SOURCE')),'name'=>'source_name','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_HOME_ADDRESS_1_NULL')),'name'=>'home_address_1','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_HOME_ADDRESS_2_NULL')),'name'=>'home_address_2','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_HOME_CITY_NULL')),'name'=>'home_city','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_HOME_STATE_NULL')),'name'=>'home_state','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_HOME_ZIP_NULL')),'name'=>'home_zip','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_HOME_COUNTRY_NULL')),'name'=>'home_country','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_PERSON_FAX')),'name'=>'fax','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_PERSON_WEBSITE')),'name'=>'website','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_PERSON_FACEBOOK_URL')),'name'=>'facebook_url','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_PERSON_TWITTER_USER')),'name'=>'twitter_user','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_PERSON_LINKEDIN_URL')),'name'=>'linkedin_url','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_PERSON_STATUS')),'name'=>'status_name','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_WORK_ADDRESS_1_NULL')),'name'=>'work_address_1','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_WORK_ADDRESS_2_NULL')),'name'=>'work_address_2','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_WORK_CITY_NULL')),'name'=>'work_city','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_WORK_STATE_NULL')),'name'=>'work_state','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_WORK_ZIP_NULL')),'name'=>'work_zip','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_WORK_COUNTRY_NULL')),'name'=>'work_country','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_PERSON_MOBILE_PHONE')),'name'=>'mobile_phone','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_PERSON_HOME_EMAIL')),'name'=>'home_email','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_PERSON_OTHER_EMAIL')),'name'=>'other_email','type'=>'text'),
 							array('display'=>ucwords(CRMText::_('COM_CRMERY_PERSON_HOME_PHONE')),'name'=>'home_phone','type'=>'text'),
 						);
					$custom = CrmeryHelperCustom::generateCustom('people');
					if ( count($custom) > 0 ){
						$custom = CrmeryHelperCustom::cleanCustomForm($custom);
					}
					$arr = array_merge($base,$custom);
 				break;
 				case "deal":
 					$base = array(


 						);
 				break;
 				case "company":
 					$base = array(


 						);
 				break;
 			}
 			return $arr;
 		}

 		

 		function generateDealStatuses($selected=null,$name="status_id",$class="class='inputbox'"){

        	$db =& JFactory::getDBO();
        	$query = $db->getQuery(true);
        	$query->select("id,name")->from("#__crmery_deal_status");

        	$query->order('ordering');

        	$db->setQuery($query);

        	$statuses = $db->loadAssocList();

        	$options = array();
        	$options[0] = CRMText::_('COM_CRMERY_NONE_STATUS');
        	if ( count ($statuses) >  0 ){
        		foreach ( $statuses as $status ){
        			$options[$status['id']] = $status['name'];
        		}
        	}

            $arr = array();
            foreach ( $options as $value => $label ){
              $arr[] = JHTML::_('select.option', $value, $label);
            }

            return JHTML::_('select.genericlist', $arr, $name, $class, 'value', 'text', $selected);

 		}

 		function getCustomCalculationFieldOptions()
 		{
 			$base = array(
	            "d.amount"                        => ucwords(CRMText::_("COM_CRMERY_AMOUNT")),
	            "d.probability"                   => ucwords(CRMText::_("COM_CRMERY_DEAL_PROBABILITY")),
	            "d.expected_close"                => ucwords(CRMText::_("COM_CRMERY_DEAL_CLOSE")),
	            "d.modified"                      => ucwords(CRMText::_("COM_CRMERY_MODIFIED")),
	            "d.created"                       => ucwords(CRMText::_("COM_CRMERY_CREATED")),
	            "d.actual_close"                  => ucwords(CRMText::_("COM_CRMERY_DEALS_ACTUAL_CLOSE")),
        	);

            $custom = CrmeryHelperDeal::getUserCustomFields();
	        for ( $i=0; $i<count($custom); $i++ ){
	            $field = $custom[$i];
	            $base["custom_".$field['id'].".value"] = $field['name'];
	        }
	        return $base;
 		}

 		function getCustomOperationFieldOptions()
 		{
 			$options = array('+'=>'+','-'=>'-','*'=>'*','%'=>'%','^'=>'^','>'=>'>','<'=>'<','/'=>'/');
 			return $options;
 		}

 		function getCustomCalculationFields($selected=null,$name="calculation_field",$class="class='inputbox'")
 		{
 			
 			$base = self::getCustomCalculationFieldOptions();

	        $arr = array();
	        foreach ( $base as $value => $label ){
              $arr[] = JHTML::_('select.option', $value, $label);
            }

            return JHTML::_('select.genericlist', $arr, $name, $class, 'value', 'text', $selected);
 		}

 		function getCustomOperationFields($selected=null,$name="operation_field",$class="class='inputbox'")
 		{

 			$base = self::getCustomOperationFieldOptions();

 			$arr = array();
            foreach ( $base as $value => $label ){
              $arr[] = JHTML::_('select.option', $value, $label);
            }

            return JHTML::_('select.genericlist', $arr, $name, $class, 'value', 'text', $selected);

 		}
		
      // CUSTOMIZATIONS ARE DOING HERE  	
	
	  function getPartners(){

			$db =& JFactory::getDbo();
			$query = $db->getQuery(true);

			$query->select("r.id,r.name")
				->from("#__crmery_company_partners AS r");

			$query->order("r.name");

			$db->setQuery($query);
			$categories = $db->loadObjectList();
			
			$return = array('any'=>CRMText::_('COM_CRMERY_ANY_PARTNER'));
			if ( count($categories) > 0 ){
				foreach ( $categories as $category ){
				 
				    $fname=$category->name;
					//$lname=$category->lname;				 
					$return[$category->id] =$fname;
				}
			}
			return $return;

		}		
		
		  function getPartnerDropdown($selected=null,$name="partner_id",$class="class='inputbox' Onchange='getData(this.value)'"){

            $partners = self::getPartners();

            $options = array();
            if ( count ($partners) >  0 ){
                foreach ( $partners as $id => $partner ){
                    $options[$id] = $partner;
                }
            }

            $arr = array();
            foreach ( $options as $value => $label ){
              $arr[] = JHTML::_('select.option', $value, $label);
            }

            return JHTML::_('select.genericlist', $arr, $name,$class,'value', 'text', $selected);

        }

      public function getRegions($remove=null){
            //get database
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            //query string
            $query->select('*');
            $query->from("#__crmery_region");
            //$query->where("u.role_type='manager'");

            // $query->where("u.published=1");
            
            //get results
            $db->setQuery($query);
            $results = $db->loadAssocList();
            //generate users object
            $regions = array();
            foreach ( $results as $key=>$region ){
               // if ( $user['id'] == $remove ){
                  //  unset($results[$key]);
                //}else{
                    $regions[$region['region_id']] = $region['region_name'];
                //}
            }
            //return
            return $regions;
        } 
     
	   public function getCountry($remove=null){
            //get database
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            //query string
            $query->select('country_id,country_name');
            $query->from("#__crmery_currencies");
            //$query->where("u.role_type='manager'");

            // $query->where("u.published=1");
            
            //get results
            $db->setQuery($query);
            $results = $db->loadAssocList();
            //generate users object
            $countries = array();
            foreach ( $results as $key=>$country ){
               // if ( $user['id'] == $remove ){
                  //  unset($results[$key]);
                //}else{
                    $countries[$country['country_id']] = $country['country_name'];
                //}
            }
            //return
            return $countries;
        }
		
		public function getState($remove=null){
            //get database
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            //query string
            $query->select('state_id,state_name');
            $query->from("#__crmery_states");
            //$query->where("u.role_type='manager'");

            // $query->where("u.published=1");
            
            //get results
            $db->setQuery($query);
            $results = $db->loadAssocList();
            //generate users object
            $states = array();
            foreach ( $results as $key=>$state ){
               // if ( $user['id'] == $remove ){
                  //  unset($results[$key]);
                //}else{
                    $states[$state['state_id']] = $state['state_name'];
                //}
            }
            //return
            return $states;
        }
		
		public function getFirm($remove=null){
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            //query string
            $query->select('*');
            $query->from("#__crmery_companies");
            //$query->where("u.role_type='manager'");

            // $query->where("u.published=1");
            
            //get results
            $db->setQuery($query);
            $results = $db->loadAssocList();
            //generate users object
            $firmss = array();
            foreach ( $results as $key=>$firm ){
               // if ( $user['id'] == $remove ){
                  //  unset($results[$key]);
                //}else{
                    $firmss[$firm['id']] = $firm['name'];
                //}
            }
            //return
            return $firmss;
        }	
        
 }