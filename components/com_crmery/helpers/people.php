<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 class CrmeryHelperPeople extends JObject
 {
 	
    //get an individual person
	function getPerson($id){
		
		//get db object
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		
		//generate query
		$query->select("p.first_name,p.last_name,p.id,p.company_id,c.name as company_name FROM #__crmery_people AS p");
		$query->leftJoin("#__crmery_companies AS c ON c.id = p.company_id");
		$query->where('p.id='.$id);
		$db->setQuery($query);
		
		//return results
		$row = $db->loadAssocList();
		return $row;
		
    }
    
    //return statuses
    function getStatusList($idsOnly = FALSE){
        //get db
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //select statuses from db
        $query->select("*");
        $query->from("#__crmery_people_status");

        $query->order("ordering");

        $db->setQuery($query);
        
        //return statuses
        return $db->loadAssocList();
    }
    
    //return people filter types
    function getPeopleTypes($filters=TRUE){
        if ( $filters ){
            return array(   'all' => CRMText::_('COM_CRMERY_ALL_PEOPLE'),
                            'leads' => CRMText::_('COM_CRMERY_ALL_LEADS'),
                            'not_leads' => CRMText::_('COM_CRMERY_ALL_PEOPLE_WHO_ARE_NOT_LEADS'),
                            'shared' => CRMText::_('COM_CRMERY_ALL_PEOPLE_WHO_ARE_SHARED')
                        );
        }else{
            return array( 'contact' => CRMText::_('COM_CRMERY_CONTACT') , 'client' => CRMText::_('COM_CRMERY_LEAD') );
        }
    }
    
    //return stages
    function getStages(){
        return array(   'all'=> CRMText::_('COM_CRMERY_INCLUDE_EVERYTHING'),
                        'today'=> CRMText::_('COM_CRMERY_TASKS_DUE_TODAY'),
                        'tomorrow'=> CRMText::_('COM_CRMERY_TASKS_DUE_TOMORROW'),
                        'past_thirty'=> CRMText::_('COM_CRMERY_UPDATED_PAST_THIRTY'),
                        'recently_added'=> CRMText::_('COM_CRMERY_RECENTLY_ADDED'),
                        'last_import'=>  CRMText::_('COM_CRMERY_PART_OF_LAST_IMPORT')
                    );
    }
    
    //get column filters
    function getColumnFilters(){
        return array(   'avatar'        =>  ucwords(CRMText::_('COM_CRMERY_AVATAR')),
						'amount_last_year'=>  ucwords(CRMText::_('COM_CRMERY_AMOUNT_LAST_YEAR')),
                        'first_name'    =>  ucwords(CRMText::_('COM_CRMERY_PEOPLE_FIRST_NAME')),
                        'last_name'     =>  ucwords(CRMText::_('COM_CRMERY_PEOPLE_LAST_NAME')),
                        'company'       =>  ucwords(CRMText::_('COM_CRMERY_COMPANY')),
                        'email'         =>  ucwords(CRMText::_('COM_CRMERY_EMAIL')),
                        'phone'         =>  ucwords(CRMText::_('COM_CRMERY_PHONE')),
                        'mobile_phone'  =>  ucwords(CRMText::_('COM_CRMERY_MOBILE_PHONE')),
                        'position'      =>  ucwords(CRMText::_('COM_CRMERY_TITLE')),
                        'owner'         =>  ucwords(CRMText::_('COM_CRMERY_OWNER')),
                        'assigned'      =>  ucwords(CRMText::_('COM_CRMERY_ASSIGNED_TO')),
                        'status'        =>  ucwords(CRMText::_('COM_CRMERY_STATUS')),
                        'source'        =>  ucwords(CRMText::_('COM_CRMERY_SOURCE')),
                        'type'          =>  ucwords(CRMText::_('COM_CRMERY_TYPE')),
                        'next_task'     =>  ucwords(CRMText::_('COM_CRMERY_NEXT_TASK')),
                        'notes'         =>  ucwords(CRMText::_('COM_CRMERY_NOTES')),
                        'city'          =>  ucwords(CRMText::_('COM_CRMERY_CITY')),
                        'state'         =>  ucwords(CRMText::_('COM_CRMERY_STATE')),
                        'postal_code'   =>  ucwords(CRMText::_('COM_CRMERY_POSTAL_CODE')),
                        'country'       =>  ucwords(CRMText::_('COM_CRMERY_COUNTRY')),
                        'added'         =>  ucwords(CRMText::_('COM_CRMERY_ADDED')),
						'updated'       =>  ucwords(CRMText::_('COM_CRMERY_UPDATED'))              
                    );
    }

    //get selected column filters
    function getSelectedColumnFilters(){
        
        //get the user session data
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        $query->select("people_columns");
        $query->from("#__crmery_users");
        $query->where("id=".CrmeryHelperUsers::getUserId());
        $db->setQuery($query);
        $results = $db->loadResult();
        
        //unserialize columns
        $columns = unserialize($results); 
        if ( is_array($columns) ){
            return $columns;
        }else{
            //if it is empty then load a default set
            return CrmeryHelperPeople::getDefaultColumnFilters();
        }
    }
    
    //get default column filters
    function getDefaultColumnFilters(){
        return array( 'avatar','company','email','phone','position','type','status','source','notes');
    }
		
		
 }
 	