<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class CrmeryHelperTranscriptlists extends JObject
{

	function getRooms($associationId=null,$associationType=null){

		$autoId = JRequest::getVar('id') ? JRequest::getVar('id') : JRequest::getVar('association_id');
		$autoType = JRequest::getVar('layout') ? JRequest::getVar('layout') : JRequest::getVar('association_type');

		$associationId = $associationId ? $associationId : $autoId;
		$associationType = $associationType ? $associationType : $autoType;

		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);

		$query->select("id,name")
			->from("#__banter_rooms")
			->where("association_id=".$associationId)
			->where("association_type='".$associationType."'");

		$db->setQuery($query);

		$rooms =  $db->loadObjectList();

		return $rooms;

	}

	function getTranscripts($roomId=null){

		$roomId = $roomId ? $roomId : JRequest::getVar('room_id');

		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);

		$query->select("t.*,r.name AS room_name")
			->from("#__banter_transcripts AS t")
			->leftJoin("#__banter_rooms AS r ON r.id = t.room_id")
			->where("t.room_id=".$roomId);

		$db->setQuery($query);
		$transcripts = $db->loadObjectList();

		return $transcripts;

	}

}