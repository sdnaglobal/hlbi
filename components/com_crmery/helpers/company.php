<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 class CrmeryHelperCompany extends JObject
 {
 	//get a specific 
	function getCompany($id){
		
		//get db object
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		
		//generate query
		$query->select("name,id FROM #__crmery_companies");
		$query->where('id='.$id);
		$db->setQuery($query);
		
		//return results
		$row = $db->loadAssocList();
		return $row;
		
	}
    
    //get company filter types
    function getTypes(){
        return array(   'all'=>CRMText::_('COM_CRMERY_ALL_COMPANIES'),
                        'today'=>CRMText::_('COM_CRMERY_COMPANIES_TASKS_TODAY'),
                        'tomorrow'=>CRMText::_('COM_CRMERY_COMPANIES_TASKS_TOMORROW'),
                        'updated_thirty'=>CRMText::_('COM_CRMERY_COMPANIES_UPDATED_LAST_MONTH'),
                        'recent'=>CRMText::_('COM_CRMERY_RECENTLY_ADDED'),
                        'past'=>CRMText::_('COM_CRMERY_CONTACTED_LONG_AGO'));
    }
    
    //get column filters
    function getColumnFilters(){
        return array(   'avatar'        =>  ucwords(CRMText::_('COM_CRMERY_AVATAR')),
                        'description'   =>  ucwords(CRMText::_('COM_CRMERY_EDIT_TASK_DESCRIPTION')),
                        'owner'         =>  ucwords(CRMText::_('COM_CRMERY_COMPANIES_OWNER')),
                        'phone'         =>  ucwords(CRMText::_('COM_CRMERY_PEOPLE_PHONE')),
                        'fax'           =>  ucwords(CRMText::_('COM_CRMERY_COMPANY_FAX')),
                        'email'         =>  ucwords(CRMText::_('COM_CRMERY_COMPANY_EMAIL')),
                        'address'       =>  ucwords(CRMText::_('COM_CRMERY_PERSON_ADDRESS')),
                        'country'       =>  ucwords(CRMText::_('COM_CRMERY_PEOPLE_COUNTRY')),
                        'next_task'     =>  ucwords(CRMText::_('COM_CRMERY_PEOPLE_TASK')),
                        'notes'         =>  ucwords(CRMText::_('COM_CRMERY_PEOPLE_NOTES')),
                        'added'         =>  ucwords(CRMText::_('COM_CRMERY_COMPANIES_ADDED')),
                        'updated'       =>  ucwords(CRMText::_('COM_CRMERY_COMPANIES_UPDATED'))
                    );
    }

    //get selected column filters
    function getSelectedColumnFilters(){
        
        //get the user session data
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        $query->select("companies_columns");
        $query->from("#__crmery_users");
        $query->where("id=".CrmeryHelperUsers::getUserId());
        $db->setQuery($query);
        $results = $db->loadResult();
        
        //unserialize columns
        $columns = unserialize($results); 
        if ( is_array($columns) ){
            return $columns;
        }else{
            //if it is empty then load a default set
            return CrmeryHelperCompany::getDefaultColumnFilters();
        }
    }
    
    //get default column filters
    function getDefaultColumnFilters(){
        return array( 'avatar','phone','notes','added','updated','category' );
    }

    function getCategories(){

        $db =& JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select("c.id,c.name")
            ->from("#__crmery_company_categories AS c");

        $query->order("c.ordering");

        $db->setQuery($query);
        $categories = $db->loadObjectList();
        
        $return = array(' '=>CRMText::_('Select Category'));
        if ( count($categories) > 0 ){
            foreach ( $categories as $category ){
                $return[$category->id] = $category->name;
            }
        }
        return $return;

    }
	
	
	// CUSTOMIZATION STARTS FROM HERE

   function getRegions(){
   
        $ownerId =  CrmeryHelperUsers::getUserId(); 
 
        $db =& JFactory::getDbo();
        $query = $db->getQuery(true);
		
		$query->select("region_id")
            ->from("#__crmery_users");
			$query->where('id='.$ownerId);
		$db->setQuery($query);
        $regions = $db->loadObjectList();		
		
		if ( count($regions) > 0 ){
            foreach ( $regions as $region ){
               $regionIds= $region->region_id;
            }
        }
   

        $db =& JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select("r.region_id,r.region_name")
            ->from("#__crmery_region AS r");
		$query->where('r.region_id IN('.$regionIds.')');

        $query->order("r.region_name");

        $db->setQuery($query);
        $categories = $db->loadObjectList();
        
        $return = array(''=>CRMText::_('Select Region'));
        if ( count($categories) > 0 ){
            foreach ( $categories as $category ){
                $return[$category->region_id] = $category->region_name;
            }
        }
        return $return;

    }
 function getCountries(){

       $ownerId =  CrmeryHelperUsers::getUserId(); 
 
        $db =& JFactory::getDbo();
        $query = $db->getQuery(true);
		
		$query->select("country_id")
            ->from("#__crmery_users");
			$query->where('id='.$ownerId);
		$db->setQuery($query);
        $countryIds = $db->loadObjectList();		
		
		if ( count($countryIds) > 0 ){
            foreach ( $countryIds as $countryId ){
               $countryId= $countryId->country_id;
            }
        }
		
		$db =& JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select("r.country_id,r.country_name")
            ->from("#__crmery_currencies AS r");
			$query->where('r.country_id IN('.$countryId.')');

        $query->order("r.country_name");

        $db->setQuery($query);
        $categories = $db->loadObjectList();
        
        $return = array(''=>CRMText::_('Select Country'));
        if ( count($categories) > 0 ){
            foreach ( $categories as $category ){
                $return[$category->country_id] = $category->country_name;
            }
        }
        return $return;

    }
	
 function getStates(){
 
       $ownerId =  CrmeryHelperUsers::getUserId(); 
 
        $db =& JFactory::getDbo();
        $query = $db->getQuery(true);
		
		$query->select("state_id")
            ->from("#__crmery_users");
			$query->where('id='.$ownerId);
		$db->setQuery($query);
        $stateIds = $db->loadObjectList();		
		
		if ( count($stateIds) > 0 ){
            foreach ( $stateIds as $stateId ){
               $stateId= $stateId->state_id;
            }
        } 
 

        $db =& JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select("r.state_id,r.state_name")
            ->from("#__crmery_states AS r");
			$query->where('r.state_id IN('.$stateId.')');

        $query->order("r.state_name");

        $db->setQuery($query);
        $categories = $db->loadObjectList();
        
        $return = array(''=>CRMText::_('Select State'));
        if ( count($categories) > 0 ){
            foreach ( $categories as $category ){
                $return[$category->state_id] = $category->state_name;
            }
        }
        return $return;

    }	
	
  function getPartners(){

        $db =& JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select("r.id,r.name,r.lname")
            ->from("#__crmery_company_partners AS r");

        $query->order("r.name");

        $db->setQuery($query);
        $categories = $db->loadObjectList();
        
        $return = array(' '=>CRMText::_('Select Partner'));
        if ( count($categories) > 0 ){
            foreach ( $categories as $category ){
			
			    $fname=$category->name;
				$lname=$category->lname;			    
                $return[$category->id] = $fname." ".$lname;
            }
        }
        return $return;

    }

   function getFirmAdmin(){
   
       $member_id = CrmeryHelperUsers::getUserId();
       $member_role = CrmeryHelperUsers::getRole();

	   // getting region Id of logged in user starts here 
	   
	    $db =& JFactory::getDbo();
        $query_rg = $db->getQuery(true);

        $query_rg->select("r.region_id")
            ->from("#__crmery_users AS r");
        $query_rg->where("r.id=".$member_id);
	   
	    $db->setQuery($query_rg);
        $regionIds = $db->loadObjectList();
		
		if ( count($regionIds) > 0 ){
            foreach ( $regionIds as $regionId ){
			
			    $regId=$regionId->region_id;				
            }
        }
         // getting region Id of logged in user ends here 	   

        $db =& JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select("r.id,r.first_name,r.last_name")
            ->from("#__crmery_users AS r");
       // $query->where("r.role_type='manager'");
        $query->where("r.role_type='manager' and r.region_id IN(".$regId.")");
        $query->order("r.first_name");

        $db->setQuery($query);
        $categories = $db->loadObjectList();
        
        $return = array(' '=>CRMText::_('Select Partner'));
        if ( count($categories) > 0 ){
            foreach ( $categories as $category ){
			
			    $fname=$category->first_name;
				$lname=$category->last_name;			    
                $return[$category->id] = $fname." ".$lname;
            }
        }
        return $return;

    }
	
	

    function getCategoryDropdown($selected=null,$name="category_id",$class="class='inputbox'"){

            $categories = self::getCategories();

            $options = array();
            if ( count ($categories) >  0 ){
                foreach ( $categories as $id => $category ){
                    $options[$id] = $category;
                }
            }

            $arr = array();
            foreach ( $options as $value => $label ){
              $arr[] = JHTML::_('select.option', $value, $label);
            }

            return JHTML::_('select.genericlist', $arr, $name, $class, 'value', 'text', $selected);

        }
		
	function getRegionDropdown($selected=null,$name="region_id",$class="class='inputbox' onChange='getCountry(this.value)'"){

            $regions = self::getRegions();

            $options = array();
            if ( count ($regions) >  0 ){
                foreach ( $regions as $id => $region ){
                    $options[$id] = $region;
                }
            }

            $arr = array();
            foreach ( $options as $value => $label ){
              $arr[] = JHTML::_('select.option', $value, $label);
            }

            return JHTML::_('select.genericlist', $arr, $name, $class, 'value', 'text', $selected);

        }	
		
	function getCountryDropdown($selected=null,$name="country_id",$class="class='inputbox'"){

            $countries = self::getCountries();

            $options = array();
            if ( count ($countries) >  0 ){
                foreach ( $countries as $id => $country ){
                    $options[$id] = $country;
                }
            }

            $arr = array();
            foreach ( $options as $value => $label ){
              $arr[] = JHTML::_('select.option', $value, $label);
            }

            return JHTML::_('select.genericlist', $arr, $name, $class, 'value', 'text', $selected);

        }	

    function getStateDropdown($selected=null,$name="state_id",$class="class='inputbox'"){

            $states = self::getStates();

            $options = array();
            if ( count ($states) >  0 ){
                foreach ( $states as $id => $state ){
                    $options[$id] = $state;
                }
            }

            $arr = array();
            foreach ( $options as $value => $label ){
              $arr[] = JHTML::_('select.option', $value, $label);
            }

            return JHTML::_('select.genericlist', $arr, $name, $class, 'value', 'text', $selected);

        }

    function getFirmAdminDropdown($selected=null,$name="partner_id",$class="class='inputbox'"){

            $partners = self::getFirmAdmin();
			
            $options = array();
            if ( count ($partners) >  0 ){
                foreach ( $partners as $id => $partner ){
                    $options[$id] = $partner;
                }
            }

            $arr = array();
            foreach ( $options as $value => $label ){
              $arr[] = JHTML::_('select.option', $value, $label);
            }

            return JHTML::_('select.genericlist', $arr, $name,$class,'value', 'text', $selected);

        }

     // Drop down list of Owner list
	 
	 function getOwnerAdmin(){
   
       $member_id = CrmeryHelperUsers::getUserId();
       $member_role = CrmeryHelperUsers::getRole();

	   // getting region Id of logged in user starts here 
	   
	    $db =& JFactory::getDbo();
        $query_rg = $db->getQuery(true);

        $query_rg->select("r.region_id")
            ->from("#__crmery_users AS r");
        $query_rg->where("r.id=".$member_id);
	   
	    $db->setQuery($query_rg);
        $regionIds = $db->loadObjectList();
		
		if ( count($regionIds) > 0 ){
            foreach ( $regionIds as $regionId ){
			
			    $regId=$regionId->region_id;				
            }
        }
         // getting region Id of logged in user ends here 	   

        $db =& JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select("r.id,r.first_name,r.last_name")
            ->from("#__crmery_users AS r");
        $query->where("r.role_type='exec' or r.role_type='' and r.region_id IN(".$regId.")");
        $query->order("r.first_name");

        $db->setQuery($query);
        $categories = $db->loadObjectList();
        
        $return = array(' '=>CRMText::_('Select Owner'));
        if ( count($categories) > 0 ){
            foreach ( $categories as $category ){
			
			    $fname=$category->first_name;
				$lname=$category->last_name;			    
                $return[$category->id] = $fname." ".$lname;
            }
        }
        return $return;

    }

    function getFirmOwnerDropdown($selected=null,$name="owner_id",$class="class='inputbox'"){

            $owners = self::getOwnerAdmin();

            $options = array();
            if ( count ($owners) >  0 ){
                foreach ( $owners as $id => $owner ){
                    $options[$id] = $owner;
                }
            }

            $arr = array();
            foreach ( $options as $value => $label ){
              $arr[] = JHTML::_('select.option', $value, $label);
            }

            return JHTML::_('select.genericlist', $arr, $name,$class,'value', 'text', $selected);

        }
	
		
		
 }
 	