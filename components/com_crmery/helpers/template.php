<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 include(JPATH_SITE.'/components/com_crmery/libraries/Mobile_Detect.php');
 jimport('joomla.application.component.model');
 
 class CrmeryHelperTemplate extends JObject
 {
    function startCompWrap() {
        
        $user = CrmeryHelperUsers::getLoggedInUser();

        if ( $user ){
            if ( CrmeryHelperUsers::isFullscreen() && !(JRequest::getVar('view')=="print") ){
                echo '<div id="logged_in_user">'.CrmeryHelperConfig::getConfigValue('welcome_message').' '.CrmeryHelperUsers::getFirstName()."!";
                    echo self::displayLogout();
                echo '</div>';
            }
        }

        echo '<div id="com_crmery">';
          echo '<div id="message" style="display:none;"></div>';
          echo '<div id="google-map" style="display:none;"></div>';
          echo '<script type="text/javascript">var base_url = "'.JURI::base().'";</script>';

          if ( $user ){
            echo '<div id="edit_note_entry" style="display:none;"></div>';
            echo '<div id="edit_convo_entry" style="display:none;"></div>';
            echo '<div id="document_preview_modal" style="display:none;"></div>';
            echo CrmeryHelperTemplate::getEventDialog();
            echo CrmeryHelperTemplate::getAvatarDialog();
            echo '<script type="text/javascript">var userDateFormat = "'.CrmeryHelperUsers::getDateFormat(FALSE).'";</script>';
            echo '<script type="text/javascript">var user_id = "'.CrmeryHelperUsers::getUserId().'";</script>';
          }

          if(self::isMobile()) { echo "<div class='page' data-role='page' data-theme='b' id=''>"; }

          echo '<div id="edit_task" style="display:none;"></div>
                <div id="edit_event" style="display:none;"></div>';
        
    }

    function displayLogout(){
        $returnURL = base64_encode(JURI::root());
		$member_role = CrmeryHelperUsers::getRole(); // Customization here 
        $string  = '<form class="inline-form" action="index.php?option=com_users&task=user.logout" method="post">';
        $string .= '<input type="hidden" name="return" value="'.$returnURL.'" />';
        $string .= '<input type="submit" class="button" value="'.CRMText::_('COM_CRMERY_LOGOUT').'" />';
        $string .= JHtml::_('form.token');
		$db=&JFactory::getDBO();
		$user=&JFactory::getUser();
		$sql="select * from #__crmery_users where uid='".$user->id."'";
		$db->setQuery($sql);
		$res=$db->query();
		$row=$db->loadObject();
		$notice_message="";
		if($member_role =="basic")
		{
			if($row->inp_company_id==0){
		
				$notice_message='<div class="nofirmassgined">Sorry But No Firm has been assigned to you till now</div>';
			}
		}else if($member_role =="manager"){
		//echo $row->company_id;
			if($row->company_id==0){
				
				$sqll="select count(id) as cnt from #__crmery_companies where partner_id='".$row->id."'";
				$db->getQuery(true);
				$db->setQuery($sqll);
				$resl=$db->query();
				$rowl=$db->loadObject();
				
				if($rowl->cnt <= 0)
					$notice_message='<div class="nofirmassgined">Sorry But No Firm has been assigned to you till now</div>';
			} 
		}
        $string .= '</form><br>'.$notice_message;
        return $string;
    }
        
    function loadToolbar() {

        //load menu
        $menu_model =& JModel::getInstance('Menu','CrmeryModel');
        $list = $menu_model->getMenu();
        
        //Get controller to select active menu item
        $controller = JRequest::getVar('controller');
        $view = JRequest::getVar('view');
        $class = "";

        //generate html
        $list_html  = '<div id="com_crmery_toolbar">';
        $list_html .= '<div id="com_crmery_menu_wrap">';
        $list_html .= '<ul class="crmery_menu">';
		
		$member_role = CrmeryHelperUsers::getRole(); // Customization here 
		$db=&JFactory::getDBO();
		$user=&JFactory::getUser();
		$sql="select * from #__crmery_users where uid='".$user->id."'";
		$db->setQuery($sql);
		$res=$db->query();
		$row=$db->loadObject();
		$notallowed=0;
		if($member_role =="basic")
		{
			if($row->inp_company_id==0){
				$notallowed=1;
			}
		}else if($member_role =="manager"){
			if($row->company_id==0){
			
				$sqll="select count(id) as cnt from #__crmery_companies where partner_id='".$row->id."'";
				$db->getQuery(true);
				$db->setQuery($sqll);
				$resl=$db->query();
				$rowl=$db->loadObject();
				
				if($rowl->cnt <= 0)
					$notallowed=1;
			} 
		}
		
		if($member_role == "manager")
		{
			$member_id = CrmeryHelperUsers::getUserId();
        
			if($member_id == '193')
			{
				$list->menu_items = $list->menu_items;
			}
			else
			{
				$rep = array(0=>'reports');
				$list->menu_items =  array_diff($list->menu_items, $rep );
			}
		
		}
		if($member_role == "")
		{
			$tdash = array(0=>'charts');
			$list->menu_items =  array_diff($list->menu_items, $tdash );
		
		}
		
		foreach ( $list->menu_items as $name ) {
		
		    $class = $name==$controller || $name==$view? "active" : "";
			
			if($member_role =="basic")
			 {
			   if($name=="companies" || $name=="reports")
			   {            
               }else{
			   if($notallowed==0){
			   $list_html .= '<li><a class="'.$class.'" href="'.JRoute::_('index.php?option=com_crmery&view='.$name).'">'.ucwords(CRMText::_('COM_CRMERY_MENU_'.strtoupper($name))).'</a></li>';
			   }
			   }
			}else{
			 if($notallowed==0){
			 if($name=="reports")
			   {
			   
			   		$member_id = CrmeryHelperUsers::getUserId();
        			if($member_id == '193')
					{
						$list_html .= '<li><a class="'.$class.'" href="'.JRoute::_('index.php?option=com_crmery&layout=firm_reports&view='.$name).'">'.ucwords(CRMText::_('COM_CRMERY_MENU_'.strtoupper($name))).'</a></li>';
					}
					else
			   		{
			      
						$list_html .= '<li><a class="'.$class.'" href="'.JRoute::_('index.php?option=com_crmery&layout=country_regiontoregion&view='.$name).'">'.ucwords(CRMText::_('COM_CRMERY_MENU_'.strtoupper($name))).'</a></li>';
					}
			   }
			   else
			   {
					$list_html .= '<li><a class="'.$class.'" href="'.JRoute::_('index.php?option=com_crmery&view='.$name).'">'.ucwords(CRMText::_('COM_CRMERY_MENU_'.strtoupper($name))).'</a></li>';
				}	
					
			 }
			
			}

	   }
		
		
        $list_html .= '</ul>';
        $list_html .= '</div>';
        $list_html .= '<div id="user_functions">';		
		
		
        if($member_role!="basic" && $member_role!="manager")
         {		
        $list_html .= '<a href="javascript:void(0);" id="create_button" class="dropdown_arrow">'.CRMText::_('COM_CRMERY_CREATE').'</a>';
		 }
		
		
        $list_html .= '<div id="create" style="display:none;">';
        $list_html .= "<ul>";
        $list_html .= '<li><a href="'.JRoute::_('index.php?option=com_crmery&view=companies&layout=edit').'">'.ucwords(CRMText::_('COM_CRMERY_NEW_COMPANY')).'</a></li>';
       // $list_html .= '<li><a href="'.JRoute::_('index.php?option=com_crmery&view=people&layout=edit').'">'.ucwords(CRMText::_('COM_CRMERY_NEW_PERSON')).'</a></li>';
        $list_html .= '<li><a href="'.JRoute::_('index.php?option=com_crmery&view=deals&layout=edit').'">'.ucwords(CRMText::_('COM_CRMERY_NEW_DEAL')).'</a></li>';
     //   $list_html .= '<li><a href="'.JRoute::_('index.php?option=com_crmery&view=goals&layout=add').'">'.ucwords(CRMText::_('COM_CRMERY_NEW_GOAL')).'</a></li>';
    //    $list_html .= '<li><a href="javascript:void(0);" onclick="addTaskEvent(\'task\');">'.ucwords(CRMText::_('COM_CRMERY_NEW_TASK')).'</a></li>';
     //   $list_html .= '<li><a href="javascript:void(0);" onclick="addTaskEvent(\'event\');">'.ucwords(CRMText::_('COM_CRMERY_NEW_EVENT')).'</a></li>';
        $list_html .= '</ul>';
        $list_html .= '</div>';
        //$list_html .= ' - ';
       // $list_html .= '<a href="'.JRoute::_('index.php?option=com_crmery&view=profile').'" >'.CRMText::_('COM_CRMERY_PROFILE').'</a>';
        //$list_html .= ' - ';
        
        if ( CrmeryHelperConfig::getConfigValue('toolbar_help') == 1 ){
            $h = CrmeryHelperConfig::getConfigValue("help_url");
            $helpLink = ( $h == "" || $h == null ) ? "http://www.crmery.com/support" : $h;
            $list_html .= '<a href="'.$helpLink.'">'.CRMText::_('COM_CRMERY_HELP').'</a>';
        }

        if ( CrmeryHelperConfig::getconfigValue("fullscreen") == 0 ){
            $list_html .= '<a href="javascript:void(0);" onclick="fullscreenToggle();">'.CRMText::_('COM_CRMERY_FULLSCREEN').'</a>';
        }
        
        $list_html .= '<a href="javascript:void(0);" onclick="showSiteSearch(\''.$view.'\');" class="site_search"><span class="magnifying_glass"></span></a>';
        //$list_html .= ' - ';
        //$list_html .= '<a href="'.JRoute::_('index.php?option=com_users&task=logout').'" />Logout</a>';
        $list_html .= '</div>';
        $list_html .= '</div>';
        $list_html .= '<div id="site_search">';
        $list_html .= '<input class="inputbox site_search" name="site_search_input" id="site_search_input" placeholder="'.CRMText::_('COM_CRMERY_SEARCH_SITE').'" value="" />';
        $list_html .= '</div>';
        
        //return html
        //echo "<pre>"; print_r($list_html); die;
		
		echo $list_html;
    }
    
    function endCompWrap(){

        if(self::isMobile()) {

            if(JRequest::getVar('view')!='dashboard') {
                $footer_menu = self::loadFooterMenu();
                echo $footer_menu;
            }

            //Ends starting page div
            echo '</div>';
        }

        echo '</div>';
    }
    
    function loadFooterMenu() 
    {
        $str = '<div data-role="footer" data-position="fixed" data-id="crmeryFooter">        
                    <div data-role="navbar" data-iconpos="top">
                        <ul>
                            <li><a data-icon="agenda" data-iconpos="top" id="agendaButton" href="'.JRoute::_('index.php?option=com_crmery&view=events').'">'.ucwords(CRMText::_('COM_CRMERY_AGENDA')).'</a></li>
                            <li><a data-icon="deals" data-iconpos="top" id="dealsButton" href="'.JRoute::_('index.php?option=com_crmery&view=deals').'">'.ucwords(CRMText::_('COM_CRMERY_DEALS_HEADER')).'</a></li>
                            <li><a data-icon="leads" data-iconpos="top" id="leadsButton" href="'.JRoute::_('index.php?option=com_crmery&view=people&type=leads').'">'.ucwords(CRMText::_('COM_CRMERY_LEADS')).'</a></li>
                            <li><a data-icon="contacts" data-iconpos="top" id="contactsButton" href="'.JRoute::_('index.php?option=com_crmery&view=people&type=not_leads').'">'.ucwords(CRMText::_('COM_CRMERY_CONTACTS')).'</a></li>
                            <li><a data-icon="companies" data-iconpos="top" id="CompaniesButton" href="'.JRoute::_('index.php?option=com_crmery&view=companies').'">'.ucwords(CRMText::_('COM_CRMERY_COMPANIES')).'</a></li>
                        </ul>
                    </div>
                </div>';

        return $str;
    }

    function loadReportMenu(){
	
	$member_role = CrmeryHelperUsers::getRole();
	if($member_role !="basic" && $member_role !="manager"){	
        $str = "<div id='reports_menu'>";
			
            $str .= "<ul>";
        /*    $str .= '<li><a href="'.JRoute::_('index.php?option=com_crmery&view=reports').'" >'.ucwords(CRMText::_('COM_CRMERY_DASHBOARD')).'</a></li>';
            $str .= "<li class='seperator'></li>";
            $str .= '<li><a href="'.JRoute::_('index.php?option=com_crmery&view=reports&layout=sales_pipeline').'" >'.ucwords(CRMText::_('COM_CRMERY_SALES_PIPELINE')).'</a></li>';
            $str .= "<li class='seperator'></li>";
            $str .= '<li><a href="'.JRoute::_('index.php?option=com_crmery&view=reports&layout=source_report').'" >'.ucwords(CRMText::_('COM_CRMERY_SOURCE_REPORT')).'</a></li>';
            $str .= "<li class='seperator'></li>";
            $str .= '<li><a href="'.JRoute::_('index.php?option=com_crmery&view=reports&layout=roi_report').'" >'.ucwords(CRMText::_('COM_CRMERY_ROI_REPORT')).'</a></li>';
            $str .= "<li class='seperator'></li>";
            $str .= '<li><a href="'.JRoute::_('index.php?option=com_crmery&view=reports&layout=deal_milestones').'" >'.ucwords(CRMText::_('COM_CRMERY_DEAL_MILESTONES')).'</a></li>';
            $str .= "<li class='seperator'></li>";
            $str .= '<li><a href="'.JRoute::_('index.php?option=com_crmery&view=reports&layout=notes').'" >'.ucwords(CRMText::_('COM_CRMERY_NOTES')).'</a></li>';
			*/
            /* $str .= "<li class='seperator'></li>";
            $str .= '<li><a href="'.JRoute::_('index.php?option=com_crmery&view=reports&layout=custom_reports').'" >'.ucwords(CRMText::_('COM_CRMERY_CUSTOM_REPORTS')).'</a></li>'; */
	
			$str .= "<li class='seperator'></li>";
            $str .= '<li id="country_regiontoregion"><a href="'.JRoute::_('index.php?option=com_crmery&view=reports&layout=country_regiontoregion').'" >'.ucwords(CRMText::_('Region reports')).'</a></li>';
			
			$str .= "<li class='seperator'></li>";
            $str .= '<li id="country_regiontoregion"><a href="'.JRoute::_('index.php?option=com_crmery&view=reports&layout=country_reports').'" >'.ucwords(CRMText::_('Country reports')).'</a></li>';
			
			$str .= "<li class='seperator'></li>";
            $str .= '<li id="country_regiontoregion"><a href="'.JRoute::_('index.php?option=com_crmery&view=reports&layout=firm_reports').'" >'.ucwords(CRMText::_('Firm reports')).'</a></li>';
			
			//$str .= "<li class='seperator'></li>";
            //$str .= '<li id="region_based_report"><a href="'.JRoute::_('index.php?option=com_crmery&view=reports&layout=region_based_report').'" >'.ucwords(CRMText::_('Region Based Report')).'</a></li>';			
		 
			//$str .= "<li class='seperator'></li>";
         //   $str .= '<li><a href="'.JRoute::_('index.php?option=com_crmery&view=reports&layout=referred_country_region').'" >'.ucwords(CRMText::_('COM_CRMERY_COUNTRY_TOTAL')).'</a></li>';
			
			
            $str .= "</ul>"; 
        $str .= "</div>";
		}
		else
			$str = '';
        return $str;
    }

    function getEventDialog(){
        $html  = "";
        $html .= '<div id="new_event_dialog" style="display:none;">';
            $html .= '<div class="new_events">';
                $html .= '<a href="javascript:void(0);" class="task" onclick="addTaskEvent(\'task\')">'.CRMText::_('COM_CRMERY_ADD_TASK').'</a><a  href="javascript:void(0);" class="event" onclick="addTaskEvent(\'event\')">'.CRMText::_('COM_CRMERY_ADD_EVENT').'</a><a href="javascript:void(0);" class="complete" onclick="jQuery(\'#new_event_dialog\').dialog(\'close\');">'.CRMText::_('COM_CRMERY_DONE').'</a>';
            $html .= '</div>';
        $html .= '</div>';
        return $html;
    }

    function getAvatarDialog(){
        $html = "<iframe id='avatar_frame' name='avatar_frame' style='display:none;border:0px;width:0px;height:0px;opacity:0;'></iframe>";
        $html .= '<div class="filters" id="avatar_upload_dialog" style="display:none;">';
            $html .= "<div id='avatar_message'>".CRMText::_('COM_CRMERY_SELECT_A_FILE_TO_UPLOAD').'</div>';
                        $html .= '<div class="input_upload_button" >';
                            $html .= '<form id="avatar_upload_form" method="POST" enctype="multipart/form-data" target="avatar_frame" >';
                                $html .= "<input type='hidden' name='option' value='com_crmery' />";
                                $html .= '<input class="button" type="button" id="upload_avatar_button" value="'.CRMText::_('COM_CRMERY_UPLOAD_FILE').'" />';
                                $html .= '<input type="file" id="upload_input_invisible_avatar" name="avatar" />';
                            $html .= '</form>';
                        $html .= '</div>';
        $html .= "</div>";

        return $html;
    }

    function isMobile()
    {

        $mobile_detect = new Mobile_Detect();
        $mobile_auto = $mobile_detect->isMobile();
        $mobile_manual = JRequest::getVar('mobile');

        if ( !is_array($_SESSION) ){
            session_start();
        }

        if ( $mobile_manual ){
            if ( $mobile_manual == "no"){
                JRequest::setVar('mobile','no');
                $_SESSION['mobile'] = "no";
                $mobile = false;
            }else if ( $mobile_manual == "yes" ){
                JRequest::setVar('mobile','yes');
                $_SESSION['mobile'] = "yes";
                $mobile = true;
            }else{
                JRequest::setVar('mobile','no');
                $_SESSION['mobile'] = "no";
                $mobile = false;
            }
        }else if($mobile_auto) {
            JRequest::setVar('mobile','yes');
            $_SESSION['mobile'] = "yes";
            $mobile = true;
        } else {
            JRequest::setVar('mobile','no');
            $_SESSION['mobile'] = "no";
            $mobile = false;
        }

        return $mobile;
    }

    function loadJavascriptLanguage()
    {
        CRMText::script("COM_CRMERY_NAME_YOUR_FIELD");
        CRMText::script("COM_CRMERY_NO_FIELDS_DEFINED");
        CRMText::script("COM_CRMERY_INVALID_OPERATIONS");
        CRMText::script("COM_CRMERY_INVALID_PARENTHESES");
        CRMText::script("COM_CRMERY_INVALID_FIELD_LENGTH");
        CRMText::script("COM_CRMERY_ARE_YOU_SURE_DELETE_REPORT");
        CRMText::script("COM_CRMERY_DRAG_FIELD_HERE");
        CRMText::script("COM_CRMERY_ARE_YOU_SURE_DELETE_REPORT");
        CRMText::script('COM_CRMERY_PLEASE_SELECT_A_USER');
        CRMText::script('COM_CRMERY_SUCCESS_MESSAGE');
        CRMText::script('COM_CRMERY_ADD_NEW_NOTE');
        CRMText::script('COM_CRMERY_ADD');
        CRMText::script('COM_CRMERY_SUCCESS_MESSAGE');
        CRMText::script('COM_CRMERY_GENERIC_UPDATED');
        CRMText::script('COM_CRMERY_DEALS_BY_STAGE');
        CRMText::script('COM_CRMERY_TOTAL');
        CRMText::script('COM_CRMERY_DEALS_BY_STATUS');
        CRMText::script('COM_CRMERY_REVENUE_FROM_LEAD_SOURCES');
        CRMText::script('COM_CRMERY_CURRENCY');
        CRMText::script('COM_CRMERY_YEARLY_COMMISSIONS');
        CRMText::script('COM_CRMERY_MONTHLY_COMMISSIONS');
        CRMText::script('COM_CRMERY_YEARLY_REVENUE');
        CRMText::script('COM_CRMERY_MONTHLY_REVENUE');
        CRMText::script('COM_CRMERY_WEEK');
        CRMText::script('COM_CRMERY_DELETE_GOALS');
        CRMText::script('COM_CRMERY_DELETE_GOAL_CONFIRMATION');
        CRMText::script('COM_CRMERY_TODAY');
        CRMText::script('COM_CRMERY_MONTH');
        CRMText::script('COM_CRMERY_WEEK');
        CRMText::script('COM_CRMERY_DAY');
        CRMText::script('COM_CRMERY_DAYS');
        CRMText::script('COM_CRMERY_EDIT_NOTE');
        CRMText::script('COM_CRMERY_JANUARY');
        CRMText::script('COM_CRMERY_FEBRUARY');
        CRMText::script('COM_CRMERY_MARCH');
        CRMText::script('COM_CRMERY_APRIL');
        CRMText::script('COM_CRMERY_MAY');
        CRMText::script('COM_CRMERY_JUNE');
        CRMText::script('COM_CRMERY_JULY');
        CRMText::script('COM_CRMERY_AUGUST');
        CRMText::script('COM_CRMERY_SEPTEMBER');
        CRMText::script('COM_CRMERY_OCTOBER');
        CRMText::script('COM_CRMERY_NOVEMBER');
        CRMText::script('COM_CRMERY_DECEMBER');
        CRMText::script('COM_CRMERY_JAN');
        CRMText::script('COM_CRMERY_FEB');
        CRMText::script('COM_CRMERY_MAR');
        CRMText::script('COM_CRMERY_APR');
        CRMText::script('COM_CRMERY_MAY');
        CRMText::script('COM_CRMERY_JUN');
        CRMText::script('COM_CRMERY_JUL');
        CRMText::script('COM_CRMERY_AUG');
        CRMText::script('COM_CRMERY_SEP');
        CRMText::script('COM_CRMERY_OCT');
        CRMText::script('COM_CRMERY_NOV');
        CRMText::script('COM_CRMERY_DEC');
        CRMText::script('COM_CRMERY_SUNDAY');
        CRMText::script('COM_CRMERY_MONDAY');
        CRMText::script('COM_CRMERY_TUESDAY');
        CRMText::script('COM_CRMERY_WEDNESDAY');
        CRMText::script('COM_CRMERY_THURSDAY');
        CRMText::script('COM_CRMERY_FRIDAY');
        CRMText::script('COM_CRMERY_SATURDAY');
        CRMText::script('COM_CRMERY_SUN');
        CRMText::script('COM_CRMERY_MON');
        CRMText::script('COM_CRMERY_TUE');
        CRMText::script('COM_CRMERY_WED');
        CRMText::script('COM_CRMERY_THU');
        CRMText::script('COM_CRMERY_FRI');
        CRMText::script('COM_CRMERY_SAT');
        CRMText::script('COM_CRMERY_EDIT');
        CRMText::script('COM_CRMERY_DELETE_CONFIRMATION');
        CRMText::script('COM_CRMERY_DELETE_LIST_CONFIRMATION');
        CRMText::script('COM_CRMERY_DELETE_PERSON_FROM_DEAL_CONFIRM');
        CRMText::script('COM_CRMERY_MARK_COMPLETE');
        CRMText::script('COM_CRMERY_MARK_INCOMPLETE');
        CRMText::script('COM_CRMERY_SUCCESSFULLY_REMOVED_EVENT');
        CRMText::script('COM_CRMERY_ITEM_SUCCESSFULLY_UPDATED');
        CRMText::script('COM_CRMERY_OK');
        CRMText::script('COM_CRMERY_ADDED');
        CRMText::script('COM_CRMERY_VERIFY_ALERT');
        CRMText::script('COM_CMERY_ARE_YOU_SURE_DELETE_REPORT');
        CRMText::script('COM_CRMERY_DEALS_BY_STAGE');
        CRMText::script('COM_CRMERY_DEALS_BY_STATUS');
        CRMText::script('COM_CRMERY_TOTAL');
        CRMText::script('COM_CRMERY_UPDATE');
        CRMText::script('COM_CRMERY_UPLOADING');
        CRMText::script('COM_CRMERY_YOUR_DOCUMENT_IS_BEING_UPLOADED');
        CRMText::script('COM_CRMERY_MESSAGE_DETAILS');
        CRMText::script('COM_CRMERY_CLICK_TO_EDIT');
        CRMText::script('COM_CRMERY_UPDATED_PRIMARY_CONTACT');
        CRMText::script('COM_CRMERY_MANAGE_MAILING_LISTS');
        CRMText::script('COM_CRMERY_MAILING_LIST_LINKS');
        CRMText::script('COM_CRMERY_REMOVE');
        CRMText::script('COM_CRMERY_ADD');
        CRMText::script('COM_CRMERY_EMAIL');
        CRMText::script('COM_CRMERY_ACTIVE_DEAL');
        CRMText::script('COM_CRMERY_START_TYPING_DEAL');
        CRMText::script('COM_CRMERY_START_TYPING_PERSON');
        CRMText::script('COM_CRMERY_SUCCESSFULLY_SHARED_ITEM');
        CRMText::script('COM_CRMERY_SUCCESSFULLY_UNSHARED_ITEM');
        CRMText::script('COM_CRMERY_SHARE_ITEM');
        CRMText::script('COM_CRMERY_OUT_OF');
        CRMText::script('COM_CRMERY_FILENAME');
        CRMText::script('COM_CRMERY_DUPLICATE_COMPANY');
        CRMText::script('COM_CRMERY_DOCUMENT_INFO');
        CRMText::script('COM_CRMERY_SHOW_FILTERS');
        CRMText::script('COM_CRMERY_RESET_FILTERS');

        /** Here we load custom javascript variables that are not loaded through Joomla! language **/
        $document =& JFactory::getDocument();
        $document->addScriptDeclaration("var comCrmeryCurrency='".CrmeryHelperConfig::getCurrency()."';");
    }

    function getListEditActions(){
        $list_html  = "";

        if ( CrmeryHelperUsers::canDelete() ){
            $list_html .= "<div id='list_edit_actions'>";
            $list_html .= CRMText::_('COM_CRMERY_PERFORM')."<a class='dropdown' id='list_edit_actions_dropdown_link'>".CRMText::_('COM_CRMERY_ACTIONS')."</a>".CRMText::_('COM_CRMERY_ON_THE')."<span id='items_checked'></span> ".CRMText::_('COM_CRMERY_ITEMS');
            $list_html .= '<div class="filters" id="list_edit_actions_dropdown">';
            $list_html .= '<ul>';
            $list_html .= '<li><a onclick="deleteListItems()">'.CRMText::_('COM_CRMERY_DELETE').'</a></li>';
            $list_html .= '</ul>';
            $list_html .= "</div>";
            $list_html .= "</div>";
        }

        return $list_html;
    }

    function getEventListEditActions(){
        $list_html  = "";

        $list_html .= "<div id='list_edit_actions'>";
        $list_html .= CRMText::_('COM_CRMERY_PERFORM')."<a class='dropdown' id='list_edit_actions_dropdown_link'>".CRMText::_('COM_CRMERY_ACTIONS')."</a>".CRMText::_('COM_CRMERY_ON_THE')."<span id='items_checked'></span> ".CRMText::_('COM_CRMERY_ITEMS');
        $list_html .= '<div class="filters" id="list_edit_actions_dropdown">';
        $list_html .= '<ul>';
        $list_html .= '<li><a onclick="deleteListItems()">'.CRMText::_('COM_CRMERY_DELETE').'</a></li>';
        $list_html .= '</ul>';
        $list_html .= "</div>";
        $list_html .= "</div>";
        
        return $list_html;
    }
 
 }