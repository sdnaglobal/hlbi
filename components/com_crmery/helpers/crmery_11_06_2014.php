<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 class CrmeryHelperCrmery extends JObject
 {

        function percent2Color($value,$brightness = 255, $max = 100,$min = 0, $thirdColorHex = '00')
        {       
                // Calculate first and second color (Inverse relationship)
                $value = number_format($value);
                $first = (1-($value/$max))*$brightness;
                $second = ($value/$max)*$brightness;

                // Find the influence of the middle color (yellow if 1st and 2nd are red and green)
                $diff = abs($first-$second);
                $influence = ($brightness-$diff)/2;     
                $first = intval($first + $influence);
                $second = intval($second + $influence);

                // Convert to HEX, format and return
                $firstHex = str_pad(dechex($first),2,0,STR_PAD_LEFT);     
                $secondHex = str_pad(dechex($second),2,0,STR_PAD_LEFT);

                return $firstHex . $secondHex . $thirdColorHex ; 

                // alternatives:
                // return $thirdColorHex . $firstHex . $secondHex; 
                // return $firstHex . $thirdColorHex . $secondHex;
        }

        /**
         * Get task and event templates
         * @param  [String] $type ["deal","person"]
         * @param  [int] $id [Optional ID to get all events with a template]
         * @return [mixed] $results
         */
        function getTaskTemplates($type,$id=null){
                $db =& JFactory::getDBO();
                $query = $db->getQuery(true);
                $query->select("t.*")->from("#__crmery_templates AS t")->where("t.type=".$db->quote($type));
                $db->setQuery($query);
                return $db->loadAssocList();
        }

        /**
         * Method to store custom field cf data associated with items
         * @param int $id : The id of the item we wish to store associated data
         * @param mixed $cf_data : The data to be stored
         * @return void
         * 
         */
        function storeCustomCf($id,$cf_data,$type){
            
            //Get DBO
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            
            //date generation
            $date = CrmeryHelperDate::formatDBDate(date('Y-m-d H:i:s'));


            
            //Loop through $cf_data array to update/insert
            for ( $i=0; $i<count($cf_data); $i++ ){
                
                //assign the data
                $row = $cf_data[$i];
                if ( is_array($row['custom_field_value']) ){
                    $row['custom_field_value'] = serialize($row['custom_field_value']);
                }
                
                //mysql query
                $query->clear();
                $query->update('#__crmery_'.$type.'_custom_cf');
                $query->set($type."_id=".$id.
                             ",custom_field_id=".$row['custom_field_id'].
                             ",value='".$row['custom_field_value']."'".
                             ",modified='$date'");
                $query->where($type."_id=$id AND custom_field_id=".$row['custom_field_id']);                                                                         
                $db->setQuery($query);
                $db->query();
                //if we do not successfully update any data then perform an insert
                if ( $db->getAffectedRows() <= 0 ){
                    $query->clear();
                    $query->insert('#__crmery_'.$type.'_custom_cf');
                    $query->set($type."_id=".$id.
                             ",custom_field_id=".$row['custom_field_id'].
                             ",value='".$row['custom_field_value']."'".
                             ",modified='$date'");
                    $db->setQuery($query);
                    $db->query();
                }
            }

        }

        function checkEmailName($email){

            $db =& JFactory::getDBO();
            $query = $db->getQuery(TRUE);

            $query->select("email")
                ->from("#__crmery_users_email_cf")
                ->where("email='".$email."'");

            $db->setQuery($query);

            $results = $db->loadObjectList();

            if ( count($results) > 0 ){

                return TRUE;

            }else{

                $query->clear()
                        ->select("j.email")
                        ->from("#__crmery_users AS u")
                        ->leftJoin("#__users AS j ON j.id = u.uid")
                        ->where("j.email='".$email."'");

                $db->setQuery($query);

                $results = $db->loadObjectList();

                if ( count($results) > 0 ){

                    return TRUE;

                }

            }

            return FALSE;
        }

        function getBytes($val) {
            $val = trim($val);
            $last = strtolower($val[strlen($val)-1]);
            switch($last) {
                // The 'G' modifier is available since PHP 5.1.0
                case 'g':
                    $val *= 1024;
                case 'm':
                    $val *= 1024;
                case 'k':
                    $val *= 1024;
            }

            return $val;
        }

        function shareItem($itemId=null,$itemType=null,$userId=null){
            $itemId = $itemId ? $itemId : JRequest::getVar('item_id');
            $itemType = $itemType ? $itemType : JRequest::getVar('item_type');
            $userId = $userId ? $userId : JRequest::getVar('user_id');

            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);

            $query->insert("#__crmery_shared")
                ->columns('item_id,item_type,user_id')
                ->values($itemId.",".$db->Quote($itemType).",".$userId);

            $db->setQuery($query);
            $db->query();

            return true;
        }

        function unshareItem($itemId=null,$itemType=null,$userId=null){
            $itemId = $itemId ? $itemId : JRequest::getVar('item_id');
            $itemType = $itemType ? $itemType : JRequest::getVar('item_type');
            $userId = $userId ? $userId : JRequest::getVar('user_id');

            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);

            $query->delete("#__crmery_shared")
                ->where('item_id='.$itemId)
                ->where('item_type='.$db->Quote($itemType))
                ->where('user_id='.$userId);

            $db->setQuery($query);
            $db->query();

            return true;
        }

        function showShareDialog(){

            $document =& JFactory::getDocument();
            $document->addScriptDeclaration('var users='.json_encode(CrmeryHelperUsers::getAllSharedUsers()).';');

            $html = "<div style='display:none;' id='share_item_dialog'>";
            $html .= '<div>'.CRMText::_('COM_CRMERY_SHARE_DESC').'</div>';
            $html .= '<input id="shared_user_name" class="inputbox" type="text" placeholder="'.CRMText::_('COM_CRMERY_BEGIN_TYPING_USER').'" />';
            $html .= '<input type="hidden" name="shared_user_id" id="shared_user_id" />';
            $html .= '<a class="button" href="javascript:void(0);" onclick="shareItem();">'.CRMText::_('COM_CRMERY_ADD').'</a>';
            $html .= '<div id="shared_user_list">';

            $itemId = JRequest::getVar('id');
            $itemType = JRequest::getVar('layout');

            $users = CrmeryHelperUsers::getItemSharedUsers($itemId,$itemType);
            if ( count ( $users ) > 0 ){
                foreach ( $users as $user ){
                    $html .= '<div id="shared_user_'.$user->value.'">'.$user->label." - <a href='javascript:void(0);' onclick='unshareItem(".$user->value.");'>".CRMText::_('COM_CRMERY_REMOVE')."</a></div>";
                }
            }

            $html .= '</div>';
            $html .= "</div>";
            return $html;

        }

        function getAssociationName($associationType=null,$associationId=null){
            $associationType = $associationType ? $associationType : JRequest::getVar('association_type');
            $associationId = $associationId ? $associationId : JRequest::getVar('association_id');

            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);

            switch ($associationType){
                case "company":
                    $select = "name";
                    $table = "companies";
                break;
                case "person":
                    $select = "CONCAT(first_name,' ',last_name)";
                    $table = "people";
                break;
                case "deal":
                    $select = "name";
                    $table = "deals";
                break;
            }

            $query->select($select)->from("#__crmery_".$table)->where("id=".$associationId);
            $db->setQuery($query);

            $name = $db->loadResult();
            return $name;
        }

        function loadHelpers()
        {   
            $helpers = array(   'custom','crmtext','template','dropdown','view',
                                'company','deal','people','users','document',
                                'styles','date','charts','event','note',
                                'activity','mailinglists','transcriptlists',
                                'banter','tweets','quote'
                            );
            foreach ( $helpers as $helper )
            {
                require_once(JPATH_SITE.'/components/com_crmery/helpers/'.$helper.".php");
            }
            JModel::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_crmery/models/');
            require_once(JPATH_ADMINISTRATOR.'/components/com_crmery/helpers/config.php');
        }

        function splitExpr($expression)
        {   
            
            //original expression copied and used for calculations
            $exp = $expression;

            //terms to be returned
            $terms = array();

            //get fields and operations
            $fields = CrmeryHelperDropdown::getCustomCalculationFieldOptions();
            $ops = CrmeryHelperDropdown::getCustomOperationFieldOptions();

            //split the string into characters
            $split = str_split($exp);

            //first detect any operators or parenthesis
            if ( count($split) > 0 ){
                foreach ( $split as $pos => $term )
                {                
                    if ( array_key_exists($term,$ops) )
                    {
                        $exp = substr_replace($exp,"#",$pos,1);
                        $terms['operations'][$pos] = $term;
                        $terms['expression'][$pos] = $term;
                        $terms['expression_details'][$pos] = array('type'=>'operation','value'=>$term);
                    }
                    else if ( $term == "(" || $term == ")" )
                    {
                        $exp = substr_replace($exp,"#",$pos,1);
                        $terms['parenthesis'][$pos] = $term;
                        $terms['expression'][$pos] = $term;
                        $terms['expression_details'][$pos] = array('type'=>'parenthesis','value'=>$term);
                    }
                }
            }

            //next we detect any fields from the system
            if ( count($fields) > 0 ){
                foreach ( $fields as $key => $field )
                {   
                    if ( strstr($exp,$key) ){
                        while ( strstr($exp,$key) )
                        {

                            $pos = strpos($exp,$key);
                            $length = strlen($key);
                            $exp = substr_replace($exp,"#",$pos,$length);
                            $pos = strpos($expression,$key);
                            $terms['found'][$pos] = $key;
                            $terms['terms'][$pos] = $key;
                            $terms['expression'][$pos] = $key;
                            $terms['expression_details'][$pos] = array('type'=>'field','value'=>$key);
                            $terms['strings'][] = $exp;
                        }
                    }
                }
            }

            //finaly we extract any custom user input
            $exp = str_replace("#"," ",$exp);
            $split = explode(" ",$exp);
            $terms['split'] = $split;
            $terms['string'] = $exp;

            if ( count($split) > 0 ){
                foreach ( $split as $term )
                {   
                    if ( strlen(trim($term)) > 0 ){
                        $pos = strpos($expression,$term); //note here that we are finding the position relative to the ORIGINAL string
                        $terms['input'][$pos] = $term;
                        $terms['expression'][$pos] = $term;
                        $terms['expression_details'][$pos] = array('type'=>'input','value'=>$term);
                    }
                }
            }
            
            //sort the expression terms for priority
            ksort($terms['expression']);
            ksort($terms['expression_details']);

            //here we reconstruct the expression from the above operations to perform validity
            $terms['original_expression'] = "";
            foreach ( $terms['expression'] as $term ){
                $terms['original_expression'] .= $term;
            }
            $terms['input_expression'] = $expression;

            //validity
            $terms['string_validated'] = ( $terms['original_expression'] == $expression ) ? true : false;
            
            //return the terms
            return $terms;
        }

}

?>