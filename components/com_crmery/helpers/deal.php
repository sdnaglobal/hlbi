<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 class CrmeryHelperDeal extends JObject
 {
 	
	function getDeal($id){
		
		//get db object
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		
		//generate query
		$query->select("name,id FROM #__crmery_deals");
		$query->where('id='.$id);
		$db->setQuery($query);
		
		//return results
		$row = $db->loadAssocList();
		return $row;
		
	}
    
    //function to return filter types for deals
    function getDealTypes(){
        return array(   'all'=>CRMText::_('COM_CRMERY_ALL_DEALS'),
                        'today'=>CRMText::_('COM_CRMERY_DEALS_TASKS_TODAY'),
                        'tomorrow'=>CRMText::_('COM_CRMERY_DEALS_TASKS_TOMORROW'),
                        'updated_thirty'=>CRMText::_('COM_CRMERY_DEALS_UPDATED_LAST_MONTH'),
                        'valuable'=>CRMText::_('COM_CRMERY_DEALS_MOST_VALUABLE'),
                        'past'=>CRMText::_('COM_CRMERY_PAST_DUE_DEALS'),
                        'not_updated_thirty'=>CRMText::_('COM_CRMERY_DEALS_NOT_UPDATED'),
                        'shared'=>CRMText::_('COM_CRMERY_SHARED_DEALS'),
                        'archived'=>CRMText::_('COM_CRMERY_ARCHIVED_DEALS'));
    }

    function checkStage($stageId){

        if ( $stageId == "all" ){
            return false;
        }

        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select("id")->from("#__crmery_stages")->where("id=".$stageId);
        $db->setQuery($query);
        $id = $db->loadResult();

        if ( $id > 0 ){
            return true;
        }else{
            return false;
        }

    }
    
    //function to return deal stages
    function getStages($stage_name=null,$stagesOnly=FALSE,$idsOnly=TRUE){
        //get db
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query->select("*");
        $query->from("#__crmery_stages");
        
        if ( $stage_name ){
            $query->where("name='".$db->getEscaped($stage_name)."'");
        }

        if ( !$stagesOnly ){
            $base = array( 'all'=>CRMText::_('COM_CRMERY_ALL_STAGES'),'active'=>CRMText::_('COM_CRMERY_ACTIVE_STAGES'));
        }else{
            $base = array();
        }

        $query->order('ordering');

        $db->setQuery($query);
        $results = $db->loadAssocList();

        if ( $idsOnly ){
            $stages = array();
            foreach ( $results as $key => $stage ){
                $stages[$stage['id']] = $stage['name'];
            }
        } else {
            $stages = $results;
        }
        
        return $base + $stages;
    }

    //function to return deal stages
    function getNonInactiveStages(){
        //get db
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query->select("id");
        $query->from("#__crmery_stages");
        $query->order('ordering');
        $query->where('percent>0');

        $db->setQuery($query);
        $results = $db->loadResultArray();

        return $results;
    }

    function getPrimaryContact($deal_id){
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select("primary_contact_id")->from("#__crmery_deals")->where("id=".$deal_id);
        $db->setQuery($query);
        return $db->loadResult();
    }

    //get stages for sorting sources
    function getSourceStages(){
        //get db
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query->select("*");
        $query->from("#__crmery_stages");
        
        //filter by active and closed stages
        $inactive_stage_ids = CrmeryHelperDeal::getInactiveStages();
        $query->where("id NOT IN(".implode(',',$inactive_stage_ids).")");

        $query->order('ordering');
        
        //merge arrays
        $base = array ( 'all'=>'all stages','active'=>'active stages');
        $db->setQuery($query);
        $results = $db->loadAssocList();
        $stages = array();
        if ( count($results) > 0 ){
            foreach ( $results as $key => $stage ){
                $stages[$stage['id']] = $stage['name'];
            }
        }
        return $base + $stages;
    }

    //function to return active stages
    function getActiveStages($idsOnly=FALSE){
        //get db
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query->select("*");
        $query->from("#__crmery_stages");
        $query->where("percent > 0");
        $query->where("percent < 100");
        $query->where("won=0");

        $query->order('ordering');
        //return results
        $db->setQuery($query);
        $results = $db->loadAssocList();

        $stages = array();
        if ( $idsOnly ){
            if ( count($results) > 0 ){
                foreach ( $results as $key => $stage ){
                    $stages[$stage['id']] = $stage['name'];
                }
            }
        }else{
            return $results;
        }
        
        return $stages;
    }

    function getActiveStageIds(){

        //get db
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query->select("id");
        $query->from("#__crmery_stages");
        $query->where("percent > 0");
        $query->where("percent < 100");
        $query->where("won=0");
        $query->order('ordering');
        //return results
        $db->setQuery($query);
        $results = $db->loadResultArray();

        return $results;

    }

    //function to return deal stages
    function getGoalStages(){
        //get db
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query->select("*");
        $query->from("#__crmery_stages");

        $query->order('ordering');
        
        //merge arrays
        $db->setQuery($query);
        $results = $db->loadAssocList();
        $stages = array();
        foreach ( $results as $key => $stage ){
            $stages[$stage['id']] = $stage['name'];
        }
        return $stages;
    }
    
    function getDealFilters()
    {
        return array (  'all'=>strtolower(CRMText::_('COM_CRMERY_ALL')),
                        'this_week'=>strtolower(CRMText::_('COM_CRMERY_THIS_WEEK')),
                        'next_week'=>strtolower(CRMText::_('COM_CRMERY_NEXT_WEEK')),
                        'this_month'=>strtolower(CRMText::_('COM_CRMERY_THIS_MONTH')),
                        'next_month'=>strtolower(CRMText::_('COM_CRMERY_NEXT_MONTH')));
    }

    //get closing filters for deals
    function getClosing(){
        return CrmeryHelperDeal::getDealFilters();
    }

     function getExpectedClosingDates(){
        return array (  'all'=>strtolower(CRMText::_('COM_CRMERY_ALL')),
                        'this_week'=>strtolower(CRMText::_('COM_CRMERY_THIS_WEEK')),
                        'next_week'=>strtolower(CRMText::_('COM_CRMERY_NEXT_WEEK')),
                        'this_month'=>strtolower(CRMText::_('COM_CRMERY_THIS_MONTH')),
                        'next_month'=>strtolower(CRMText::_('COM_CRMERY_NEXT_MONTH')));
    }

    function getActualClosingDates(){
        return array (  'all'=>strtolower(CRMText::_('COM_CRMERY_ALL')),
                        'this_week'=>strtolower(CRMText::_('COM_CRMERY_THIS_WEEK')),
                        'last_week'=>strtolower(CRMText::_('COM_CRMERY_LAST_WEEK')),
                        'last_month'=>strtolower(CRMText::_('COM_CRMERY_LAST_MONTH')));
    }
    
    //get closing filters for deals
    function getModified(){
         return CrmeryHelperDeal::getDealFilters();
    }
    
    //get closing filters for deals
    function getCreated(){
        return CrmeryHelperDeal::getDealFilters();
    }
    
    /**
     * Get amounts for dropdowns
     */
    function getAmounts(){
        return array (  'small' => CRMText::_('COM_CRMERY_SMALL'), 'medium' => CRMText::_('COM_CRMERY_MEDIUM'), 'large' => CRMText::_('COM_CRMERY_LARGE') );
    }
    
    //get won stage
    function getWonStages(){
        //get db
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //search for 100% stage id
        $query->select("s.id")->from("#__crmery_stages AS s")->where("s.won=1");
        
        //return id
        $db->setQuery($query);
        return $db->loadResultArray();
    }
    
    //get lost stage
    function getInactiveStages(){
        //get db
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //search for 0% stage id
        $query->select("s.id")->from("#__crmery_stages AS s")->where('s.percent=0');
        
        //return id
        $db->setQuery($query);
        $stages = $db->loadResultArray();

        $base = array(0);
        return $stages + $base;
    }

    function getClosedStages(){
        //get db
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //search for 100% stage id
        $query->select("s.id")->from("#__crmery_stages AS s")->where('s.percent=100');
        
        //return id
        $db->setQuery($query);
        return $db->loadResultArray();
    }
    
    //get deal statuses
    function getStatuses($status_name=null,$returnColors=FALSE){
        //get db
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query->select("*");
        $query->from("#__crmery_deal_status");

        if ( $status_name ){
            $query->where("name='".$db->getEscaped($status_name)."'");
        }

        $query->order('ordering');
        
        //merge arrays
        $db->setQuery($query);
        $results = $db->loadAssocList();
        $statuses = array();
        if ( $returnColors ){
            $statuses[0] = array('id'=>0,'label'=>CRMText::_('COM_CRMERY_NONE_STATUS'),'color'=>'ffffff');
        }else{
            $statuses[0] = ucwords(CRMText::_('COM_CRMERY_NONE_STATUS'));
        }
        
        if ( count($results) > 0 ){
            foreach ( $results as $key => $status ){
                if ( $returnColors ){
                    $statuses[$status['id']] = array('id'=>$status['id'],'label'=>$status['name'],'color'=>$status['color']);
                }else{
                    $statuses[$status['id']] = $status['name'];
                }
            }
        }

        return $statuses;
    }
    
    //get deal sources
    function getSources($source_name=null){
        //get db
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query->select("*");
        $query->from("#__crmery_sources");

        $query->order('ordering');

        if ( $source_name ){
            $query->where("name='".$db->getEscaped($source_name)."'");
        }
        
        //merge arrays
        $db->setQuery($query);
        $results = $db->loadAssocList();
        $sources = array();
        foreach ( $results as $key => $source ){
            $sources[$source['id']] = $source['name'];
        }
        return $sources;
    }
    
    //get user created custom fields from database
    function getUserCustomFields($id=null){
        
        //get dbo
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //gen query string
        $query->select("*");
        $query->from("#__crmery_deal_custom");
        
        //specific field
        if ( $id ){
            $query->where("id=$id");
        }
        
        //run query and return results
        $db->setQuery($query);
        return $db->loadAssocList();
        
    }
    
    
    //get all custom fields for reports
	function getAllCustomFields(){
	    $base = array (
            "summary"                       => ucwords(CRMText::_("COM_CRMERY_SUMMARY")),
            "amount"                        => ucwords(CRMText::_("COM_CRMERY_AMOUNT")),
            "name"                          => ucwords(CRMText::_("COM_CRMERY_DEALS_NAME")),
            "owner_id"                      => ucwords(CRMText::_("COM_CRMERY_DEAL_OWNER")),
            "stage_id"                      => ucwords(CRMText::_("COM_CRMERY_DEAL_STAGE")),
            "probability"                   => ucwords(CRMText::_("COM_CRMERY_DEAL_PROBABILITY")),
            "status_id"                     => ucwords(CRMText::_("COM_CRMERY_DEAL_STATUS")),
            "expected_close"                => ucwords(CRMText::_("COM_CRMERY_DEAL_CLOSE")),
            "modified"                      => ucwords(CRMText::_("COM_CRMERY_MODIFIED")),
            "created"                       => ucwords(CRMText::_("COM_CRMERY_CREATED")),
            "source_id"                     => ucwords(CRMText::_("COM_CRMERY_REPORTS_SOURCE")),
            "actual_close"                  => ucwords(CRMText::_("COM_CRMERY_DEALS_ACTUAL_CLOSE")),
            "primary_contact_name"          => ucwords(CRMText::_("COM_CRMERY_PRIMARY_CONTACT_NAME")),
            "primary_contact_email"         => ucwords(CRMText::_("COM_CRMERY_PRIMARY_CONTACT_EMAIL")),
            "primary_contact_phone"         => ucwords(CRMText::_("COM_CRMERY_PRIMARY_CONTACT_PHONE")),
            "primary_contact_city"          => ucwords(CRMText::_("COM_CRMERY_PRIMARY_CONTACT_CITY")),
            "primary_contact_state"         => ucwords(CRMText::_("COM_CRMERY_PRIMARY_CONTACT_STATE")),
            "primary_contact_company_name"  => ucwords(CRMText::_("COM_CRMERY_PRIMARY_CONTACT_COMPANY_NAME")),
            "company_name"                  => ucwords(CRMText::_("COM_CRMERY_DEAL_COMPANY")),
            "company_category"              => ucwords(CRMText::_('COM_CRMERY_COMPANY_CATEGORY_NAME'))
        );
        $custom = CrmeryHelperDeal::getUserCustomFields();
        for ( $i=0; $i<count($custom); $i++ ){
            $field = $custom[$i];
            $base["custom_".$field['id']] = $field['name'];
        }
        $base = $base + array('calculate_field' => ucwords(CRMText::_('COM_CRMERY_CALCULATE_FIELD')));
        return $base;
	}

    //get column filters
    function getColumnFilters(){
        $arr = array(   'company'           => ucwords(CRMText::_('COM_CRMERY_DEALS_COMPANY')),
                        'primary_contact'   => ucwords(CRMText::_('COM_CRMERY_PRIMARY_CONTACT')),
                        'contacts'          => ucwords(CRMText::_('COM_CRMERY_DEALS_CONTACTS')),
                        'summary'           => ucwords(CRMText::_('COM_CRMERY_DEALS_SUMMARY')),
                        'amount'            => ucwords(CRMText::_('COM_CRMERY_DEALS_AMOUNT')),
                        'status'            => ucwords(CRMText::_('COM_CRMERY_DEALS_STATUS')),
                        'stage'             => ucwords(CRMText::_('COM_CRMERY_DEALS_STAGE')),
                        'source'            => ucwords(CRMText::_('COM_CRMERY_DEAL_SOURCE')),
                        'expected_close'    => ucwords(CRMText::_('COM_CRMERY_DEALS_EXPECTED_CLOSE')),
                        'actual_close'      => ucwords(CRMText::_('COM_CRMERY_DEALS_ACTUAL_CLOSE')),
                        'owner'             => ucwords(CRMText::_('COM_CRMERY_DEALS_OWNER')),
                        'next_action'       => ucwords(CRMText::_('COM_CRMERY_DEALS_NEXT')),
                        'deals_due'         => ucwords(CRMText::_('COM_CRMERY_DEALS_DUE')),
                        'notes'             => ucwords(CRMText::_('COM_CRMERY_DEALS_NOTES')),
                        'created'           => ucwords(CRMText::_('COM_CRMERY_PEOPLE_ADDED')),
                        'modified'          => ucwords(CRMText::_('COM_CRMERY_PEOPLE_UPDATED'))
                    );
        $arr = CrmeryHelperQuote::hasCrmQuote() ? $arr + CrmeryHelperQuote::getColumnFilters() : $arr;
        return $arr;
    }

    //get selected column filters
    function getSelectedColumnFilters(){
        
        //get the user session data
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        $query->select("deals_columns");
        $query->from("#__crmery_users");
        $query->where("id=".CrmeryHelperUsers::getUserId());
        $db->setQuery($query);
        $results = $db->loadResult();
        
        //unserialize columns
        $columns = unserialize($results); 
        if ( is_array($columns) ){
            return $columns;
        }else{
            //if it is empty then load a default set
            return CrmeryHelperDeal::getDefaultColumnFilters();
        }
    }
    
    //get default column filters
    function getDefaultColumnFilters(){
        return array( 'company','primary_contact','amount','stage','expected_close','next_action','deals_due','notes','created','modified' );
    }

    function downloadDocument(){

        $model =& JModel::getInstance('document','CrmeryModel');
        $document = $model->getDocument();

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename($document->name));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($document->path));
        ob_clean();
        flush();
        readfile($document->path);
        exit;
    }
    
		
 }
 	