<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryHelperActivity extends JObject
{

	var $limit = null;
	var $type = null;
	var $type_id = null;

	function saveActivity($old_info, $new_info, $model, $action_type) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$user_id = CrmeryHelperUsers::getUserId();
		$date = CrmeryHelperDate::formatDBDate(date('Y-m-d H:i:s'));

		if($action_type=='created' || $action_type=="deleted" || $action_type=="uploaded") {
			$query->clear();
			$query->insert('#__crmery_history');
			$query->set('type='.$db->Quote($model).', type_id='.$db->Quote($new_info->id).', user_id='.$db->Quote($user_id).', date='.$db->Quote($date).',new_value='.$db->Quote($new_info->id).', action_type='.$db->Quote($action_type).', field="id"');
			$db->setQuery($query);
			$db->query();
		} else {
			$differences = self::recursive_array_diff((array)$old_info,(array)$new_info);

			if(count($differences) > 0) {
				foreach($differences as $key => $old_value) {
				$query->clear();
				$query->insert('#__crmery_history');
				$query->set('type='.$db->Quote($model).', type_id='.$db->Quote($new_info->id).', user_id='.$db->Quote($user_id).', date='.$db->Quote($date).', old_value='.$db->Quote($old_value).', new_value='.$db->Quote($new_info->$key).', action_type='.$db->Quote($action_type).', field='.$db->Quote($key));
				$db->setQuery($query);
				$db->query();
				}
			}
		}
	}

	function saveUserLoginHistory(){
		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);

		$user_id = CrmeryHelperUsers::getUserId();
		$today = CrmeryHelperDate::formatDBDate(date("Y-m-d"));

		$query->clear();
		$query->select("COUNT(id)");
		$query->from("#__crmery_login_history");
		$query->where("date='".$today."'");
		$query->where("user_id=".$user_id);
		$db->setQuery($query);
		$existing = $db->loadResult();

		if ( !$existing ){
			$query->clear();
			$query->insert("#__crmery_login_history");
			$query->set("user_id=".$user_id.",date='".$today."'");
			$db->setQuery($query);
			$db->query();
		}

	}

	function getActivity() {

		/** Large SQL Selections **/
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        $db->setQuery("SET SQL_BIG_SELECTS=1");
        $db->query();

		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		$query->select('h.*, CONCAT(u.first_name," ", u.last_name) AS owner_name, c.name as company_name, CONCAT(p.first_name," ", p.last_name) AS person_name, 
						d.name as deal_name, e.name as event_name, note_cat.name as notes_category_name, 
						event_cat.name as events_category_name, old_event_cat.name as events_category_name_old, old_note_cat.name AS notes_category_name_old,
						doc.name AS document_name,status.name AS deal_status_name_old, status2.name AS deal_status_name,deal_source.name AS deal_source_name_old,deal_source_2.name AS deal_source_name,
						deal_stage.name AS deal_stage_name_old,deal_stage_2.name AS deal_stage_name,CONCAT(deal_owner.first_name," ",deal_owner.last_name) AS deal_owner_name_old,
						CONCAT(deal_owner_2.first_name," ",deal_owner_2.last_name) AS deal_owner_name

						');

		if ( CrmeryHelperQuote::hasCRMQuote() )
		{
			CrmeryHelperQuote::buildActivitySelect($query);
		}

		$query->from('#__crmery_history AS h');
		$query->leftJoin('#__crmery_users AS u ON u.id = h.user_id');
		$query->leftJoin('#__crmery_companies AS c ON c.id = h.type_id AND h.type="company"');
		$query->leftJoin('#__crmery_notes AS n ON n.id = h.type_id AND h.type="note"');
		$query->leftJoin('#__crmery_deals AS d on d.id = h.type_id AND h.type="deal"');
		$query->leftJoin('#__crmery_people AS p on p.id = h.type_id AND h.type="person"');
		$query->leftJoin('#__crmery_goals AS g on g.id = h.type_id AND h.type="goal"');
		$query->leftJoin('#__crmery_events AS e on e.id = h.type_id AND h.type="event"');
		$query->leftJoin("#__crmery_events_cf AS ecf on e.id = ecf.event_id");
		$query->leftJoin('#__crmery_reports AS r on r.id = h.type_id AND h.type="report"');
		$query->leftJoin('#__crmery_documents AS doc ON doc.id = h.type_id AND h.type="document"');
		$query->leftJoin('#__crmery_notes_categories as note_cat ON note_cat.id = h.new_value AND h.field="category_id" AND h.type="notes"');
		$query->leftJoin('#__crmery_events_categories as event_cat ON event_cat.id = h.new_value AND h.field="category_id" AND h.type="events"');
		$query->leftJoin('#__crmery_notes_categories as old_note_cat ON old_note_cat.id = h.old_value AND h.field="category_id" AND h.type="notes"');
		$query->leftJoin('#__crmery_events_categories as old_event_cat ON old_event_cat.id = h.old_value AND h.field="category_id" AND h.type="events"');
		$query->leftJoin("#__crmery_deal_status AS status ON status.id = h.old_value AND h.type='deal'");
		$query->leftJoin("#__crmery_deal_status AS status2 ON status2.id = h.new_value AND h.type='deal'");
		$query->leftJoin("#__crmery_sources AS deal_source ON deal_source.id = h.old_value AND h.type='deal'");
		$query->leftJoin("#__crmery_sources AS deal_source_2 ON deal_source_2.id = h.new_value AND h.type='deal'");
		$query->leftJoin("#__crmery_stages AS deal_stage ON deal_stage.id = h.old_value AND h.type='deal'");
		$query->leftJoin("#__crmery_stages AS deal_stage_2 ON deal_stage_2.id = h.new_value AND h.type='deal'");
		$query->leftJoin("#__crmery_users AS deal_owner ON deal_owner.id = h.old_value AND h.type='deal'");
		$query->leftJoin("#__crmery_users AS deal_owner_2 ON deal_owner_2.id = h.new_value AND h.type='deal'");
		if ( CrmeryHelperQuote::hasCRMQuote() )
		{
			CrmeryHelperQuote::buildActivityJoin($query);
		}

		if ( isset($this->type) && strlen($this->type) > 0 && isset($this->type_id) && $this->type_id > 0 )
		{
			if ( $this->type == "quote" ){
				CrmeryHelperQuote::buildActivityWhere($query,$this->type_id);
			}else{
				$query->where("h.type_id=".$this->type_id);
				$query->where("h.type='".$this->type."'");
			}
		}

		$member_id = CrmeryHelperUsers::getUserId();
        $member_role = CrmeryHelperUsers::getRole();
        $team_id = CrmeryHelperUsers::getTeamId();
        if ( $member_role != 'exec'){
             //manager filter
            if ( $member_role == 'manager' ){
                $query->where('u.team_id = '.$team_id);
            }else{
            //basic user filter
                $query->where(array('h.user_id = '.$member_id));
            }
        }

        //TODO: Add assignees to the display (massive left join)
        $query->where('h.field!="assignee_id" AND h.field!="repeats"');
        $query->order('h.date DESC');

        if ( !isset($this->type) && !isset($this->type_id) ){
	        $typeId = JRequest::getVar('id');
	        $type = JRequest::getVar('layout');
	        if (  $typeId && $type ){
	        	switch ( $type ){
	        		case "deal":
	        			$query->where("( (h.type_id=".$typeId." AND h.type='deal' ) OR 
	        							 (n.deal_id=".$typeId.") OR
	        							 (ecf.association_id=".$typeId." AND ecf.association_type='deal') OR
	        							 (doc.association_id=".$typeId." AND doc.association_type='deal') 
	        							)");
	        		break;
	        		case "company":
	        			$query->where("( (h.type_id=".$typeId." AND h.type='company' ) OR 
	        							 (n.company_id=".$typeId.") OR
	        							 (ecf.association_id=".$typeId." AND ecf.association_type='company') OR
	        							 (doc.association_id=".$typeId." AND doc.association_type='company') 
	        							)");
	        		break;
	        		case "person":
	    				$query->where("( (h.type_id=".$typeId." AND h.type='person' ) OR 
	        							 (n.person_id=".$typeId.") OR
	        							 (ecf.association_id=".$typeId." AND ecf.association_type='person') OR
	        							 (doc.association_id=".$typeId." AND doc.association_type='person') 
	        							)");
	        		break;
	        	}
	        }
    	}

        if ( $this->limit != null ){
        	$query .= " LIMIT ".$this->limit;
        }else{
        	$query .= " LIMIT 10";
        }

        $db->setQuery($query);
        $activity = $db->loadObjectList();

        return $activity;

	}


	function recursive_array_diff($a1, $a2) { 
	    $r = array(); 
	    foreach ($a1 as $k => $v) {
	    	if($k[0]!='_' && $k!='modified') {
		        if (array_key_exists($k, $a2)) { 
		            if (is_array($v)) { 
		                $rad = self::recursive_array_diff($v, $a2[$k]); 
		                if (count($rad)) { $r[$k] = $rad; } 
		            } else { 
		                if ($v != $a2[$k]) { 
		                    $r[$k] = $v; 
		                }
		            }
		        } else { 
		            $r[$k] = $v; 
		        } 
		    }
	    } 
	    return $r; 
	}



}