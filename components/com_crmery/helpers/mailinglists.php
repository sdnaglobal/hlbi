<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class CrmeryHelperMailinglists extends JObject
{

	var $listId 	= NULL;
	var $peopleIds 	= NULL;
	var $subscriber = NULL;

	function __construct(){
		$this->listId = JRequest::getVar('list_id');
		$this->peopleIds = JRequest::getVar('people_ids');
		$this->subscriber = $this->getSubscriberId();
	}

	public function getSubscriberId(){

		$id = JRequest::getVar('id');
		$view =  JRequest::getVar('loc') ? JRequest::getVar('loc') : JRequest::getVar('view');

		switch ( $view ){
			case "companies":
				$modelName = "company";
			break;
			case "person":
				$modelName = "people";
			break;
			default:
				$modelName = $view;
			break;
		}

		$model =& JModel::getInstance($modelName,'CrmeryModel');
		$email = $model->getEmail($id);

		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);

		$query->select("subid")
			->from("#__acymailing_subscriber")
			->where("email='".$email."'");

		$db->setQuery($query);
		$id = $db->loadResult();

		return $id;

	}

	public function onProfilePage(){
		$id = JRequest::getVar('id');
		$view =  JRequest::getVar('loc') ? JRequest::getVar('loc') : JRequest::getVar('view');
		return ( $id && $view ) ? true : false;
	}

	/**
	 * Get Acymailing Mailing Lists
	 * @param  [type] $listId=NULL [description]
	 * @return [type] [description]
	 */
	public function getMailingLists($all=FALSE){

		$subid = self::getSubscriberId();
		$onProfilePage = self::onProfilePage();
		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);

		if ( !$subid && $onProfilePage ){
			// $error = new stdClass();
			// $error->error=CRMText::_('COM_CRMERY_NO_EMAIL_PRESENT');
			// return $error;
		}

		$query->select("DISTINCT list.listid,list.name,list.description,list.color")
				->from("#__acymailing_list AS list");

				if ( $subid ){
					$query->select("subscribed.listid AS isSubscribed");
					$query->leftJoin("#__acymailing_listsub AS subscribed ON subscribed.listid = list.listid AND subscribed.status=1 AND subscribed.subid = ".$subid);
				}

				if ( !$all || ( $subid && !$onProfilePage ) ){
					// $query->where("subscribed.subid=".$subid);
				}

				$query->where("list.published=1");
				$query->where("list.visible=1");
				$query->order("ordering ASC");

		$db->setQuery($query);

		$lists = $db->loadObjectList();

		if ( count($lists) > 0 ){
			foreach ( $lists as $list ){
				if ( !isset($list->isSubscribed) ){
					$list->isSubscribed = 0;
				}
			}
		}

		return $lists;

	}

	/**
	 * Get Acymailing List Newsletters
	 * @param  [type] $listId=NULL [description]
	 * @return [type]              [description]
	 */
	public function getNewsletters($listId=NULL){

		$listId = $listId ? $listId : ( ( isset($this->listId) && $this->listId > 0 ) ? $this->listId : JRequest::getVar('list_id') );
		$subId = self::getSubscriberId();

		if ( $subId ){

			$db =& JFactory::getDBO();
			$query = $db->getQuery(true);
			
			$query->select("mail.mailid,mail.subject,mail.published,mail.senddate,user.open,user.opendate")
					->from("#__acymailing_mail AS mail")
					->leftJoin("#__acymailing_listmail AS listmail ON listmail.mailid = mail.mailid")
					->leftJoin("#__acymailing_userstats AS user ON mail.mailid = user.mailid")
					->where("listmail.listid=".$listId)
					->where("user.subid=".$subId)
					->where("mail.published=1")
					->where("mail.visible=1");

			$db->setQuery($query);

			$newsletters = $db->loadObjectList();
			return $newsletters;

		}else{

			return array();

		}

	}

	/**
	 * Add CRMery People to Acymailing Lists
	 */
	public function addMailingList($data){

		$id = $data['id'];
		$loc = JRequest::getVar('loc');
		$listid = $data['listid'];

		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);

		switch ( $loc ){
			case "company":
				$modelName = "company";
				$funcName = "Companies";
			break;
			case "person":
				$modelName = "people";
				$funcName = "Person";
			break;
			default:
				$model = $loc;
				$funcName = ucwords($loc);
			break;
		}

		$model = JModel::getInstance($modelName,'CrmeryModel');
		$func = "get".$funcName;
		$item = $model->$func($id);
		$item = $item[0];

		$time = time();

		$query->select("subid")
				->from("#__acymailing_subscriber")
				->where("email='".$item['email']."'");

		$db->setQuery($query);
		$subid = $db->loadResult();

		if ( $subid ){

			$query->clear();
			$query->select("status")
				->from("#__acymailing_listsub")
				->where("subid=".$subid)
				->where("listid=".$listid);

			$db->setQuery($query);
			$isSubscribed = $db->loadResult();

			if ( $isSubscribed ){

				$query->clear();
				$query->update("#__acymailing_listsub")
					->set("status=1")
					->set("subdate='".$time."'")
					->where("subid=".$subid)
					->where("listid=".$listid);

				$db->setQuery($query);
				$db->query();

			} else {

				$query->clear();
				$query->insert("#__acymailing_listsub")
					->columns("listid,subid,subdate,status")
					->values($listid.','.$subid.',\''.$time.'\',1');

				$db->setQuery($query);
				$db->query();

			}

		}else{

			$query->insert("#__acymailing_subscriber")
				->columns("email,name,created,confirmed,enabled,accept,html")
				->values($db->Quote($item['email']).','.$db->Quote($item['first_name'].' '.$item['last_name']).','.$time.','.'1,1,1,1');

			$db->setQuery($query);
			$db->query();

			$subid = $db->insertid();

			$query->clear();
			$query->insert("#__acymailing_listsub")
				->columns("listid,subid,subdate,status")
				->values($listid.','.$subid.',\''.$time.'\',1');

			$db->setQuery($query);
			$db->query();

		}

		return true;

	}

	/**
	 * Remove CRMery People from Acymailing Lists
	 */
	public function removeMailingList($data){

		$id = $data['id'];
		$listid = $data['listid'];

		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);

		$time = time();

		$values = array("unsubdate='".$time."'",'status=-1');

		$query->update("#__acymailing_listsub")->set($values)->where("listid=".$listid)->where("subid=".self::getSubscriberId());
		$db->setQuery($query);
		$db->query();

		return true;

	}

	public function getLinks(){

		$data = JRequest::get('post');
		$mailid = $data['mailid'];
		$subid = self::getSubscriberId();

		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);

		$query->select("click.click,url.name,url.url")
				->from("#__acymailing_urlclick AS click")
				->leftJoin("#__acymailing_url AS url ON url.urlid = click.urlid")
				->where("click.subid=".$subid)
				->where("click.mailid=".$mailid);

		$db->setQuery($query);
		return $db->loadObjectList();

	}


}