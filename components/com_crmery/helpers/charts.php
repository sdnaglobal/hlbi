<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 class CrmeryHelperCharts extends JObject
 {
    //get a specific 
    function getDashboardCharts(){
        
        return  array(  'dealStatusPie'         =>  ucwords(CRMText::_('COM_CRMERY_DEALS_BY_STATUS_PIE_CHART')),
                        'dealStatusBar'         =>  ucwords(CRMText::_('COM_CRMERY_DEALS_BY_STATUS_BAR_CHART')),
                        'dealStagePie'          =>  ucwords(CRMText::_('COM_CRMERY_DEALS_BY_STAGE_PIE_CHART')),
                        'dealStageBar'          =>  ucwords(CRMText::_('COM_CRMERY_DEALS_BY_STAGE_BAR_CHART')),
                        'yearlyCommissions'     =>  ucwords(CRMText::_('COM_CRMERY_YEAR_TO_DATE_COMMISSIONS')),
                        'monthlyCommissions'    =>  ucwords(CRMText::_('COM_CRMERY_COMMISSIONS_THIS_MONTH')),
                        'yearlyRevenue'         =>  ucwords(CRMText::_('COM_CRMERY_YEAR_T0_DATE_REVENUE')),
                        'monthlyRevenue'        =>  ucwords(CRMText::_('COM_CRMERY_REVENUE_THIS_MONTH')));
        
    }
        
        
 }
    