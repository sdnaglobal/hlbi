<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 class CrmeryHelperDate extends JObject
 {
     /**
      * Get timezone dropdown info for profile
      */
     function getTimezones(){

             $list = timezone_identifiers_list();
             $zones =  array();

             foreach( $list as $zone ){
                $zones[$zone] = $zone;
             }

            return $zones;
     }
     
     /*
      * Get date formates for profile
      */
     function getDateFormats(){
         return array( 'm/d/y' => 'mm/dd/yy', 'd/m/y' => 'dd/mm/yy' );
     }
     
     /*
      * Get time formates for profile
      */
     function getTimeFormats(){
         return array( 'g:i A' => '7:30 PM', 'H:i' => '19:30' );
     }
     
     /*
      * Get goal dates for filtering
      */
     function getGoalDates(){
         return array(     'this_week'     => CRMText::_("COM_CRMERY_THIS_WEEK"),
                            'next_week'     => CRMText::_("COM_CRMERY_NEXT_WEEK"),
                            'this_month'    => CRMText::_("COM_CRMERY_THIS_MONTH"),
                            'next_month'    => CRMText::_("COM_CRMERY_NEXT_MONTH"),
                            'this_quarter'  => CRMText::_("COM_CRMERY_THIS_QUARTER"),
                            'next_quarter'  => CRMText::_("COM_CRMERY_NEXT_QUARTER"),
                            'this_year'     => CRMText::_("COM_CRMERY_THIS_YEAR"),
                            'custom'        => CRMText::_("COM_CRMERY_CUSTOM") );
     }
     
     /*
      * Get Created dates for filtering
      */
     function getCreatedDates(){
         return array(     'this_week'     => CRMText::_("COM_CRMERY_THIS_WEEK"),
                            'last_week'     => CRMText::_("COM_CRMERY_LAST_WEEK"),
                            'this_month'    => CRMText::_("COM_CRMERY_THIS_MONTH"),
                            'last_month'    => CRMText::_("COM_CRMERY_LAST_MONTH"),
                            'today'         => CRMText::_("COM_CRMERY_TODAY"),
                            'yesterday'     => CRMText::_("COM_CRMERY_YESTERDAY"), );
     }
     
     /*
      * Get the weeks in a month for report page data creation
      */
     function getWeeksInMonth($current_month){
            $month = intval(date('m',strtotime($current_month))); //force month to single integer if '0x'
            $year = intval(date('Y',strtotime($current_month)));
            $suff = array('st','nd','rd','th','th','th'); //week suffixes
            $end = date('t',mktime(0,0,0,$month,1,$year)); //last date day of month: 28 - 31
            $start = date('w',mktime(0,0,0,$month,1,$year)); //1st day of month: 0 - 6 (Sun - Sat)
            $last = 7 - $start; //get last day date (Sat) of first week
            $noweeks = ceil((($end - ($last + 1))/7) + 1); //total no. weeks in month
            $output = ""; //initialize string
            $weeks = array();
            $monthlabel = str_pad($month, 2, '0', STR_PAD_LEFT);
            for($x=1;$x<$noweeks+1;$x++){
                if($x == 1){
                    $startdate = "$year-$monthlabel-01";
                    $day = $last - 6;
                }else{
                    $day = $last + 1 + (($x-2)*7);
                    $day = str_pad($day, 2, '0', STR_PAD_LEFT);
                    $startdate = "$year-$monthlabel-$day";
                }
                if($x == $noweeks){
                    $enddate = "$year-$monthlabel-$end";
                }else{
                    $dayend = $day + 6;
                    $dayend = str_pad($dayend, 2, '0', STR_PAD_LEFT);
                    $enddate = "$year-$monthlabel-$dayend";
                }
                    $output .= "{$x}{$suff[$x-1]} week -> Start date=$startdate End date=$enddate <br />";
                    $weeks[] = array ( 'start_date'=>$startdate,'end_date'=>$enddate );
            }
            return $weeks;
     }

    /*
     * Get month names for report page charts and data creation
     */
    function getMonthNames(){
        return array(      CRMText::_('COM_CRMERY_JANUARY'),
                           CRMText::_('COM_CRMERY_FEBRUARY'),
                           CRMText::_('COM_CRMERY_MARCH'),
                           CRMText::_('COM_CRMERY_APRIL'),
                           CRMText::_('COM_CRMERY_MAY'),
                           CRMText::_('COM_CRMERY_JUNE'),
                           CRMText::_('COM_CRMERY_JULY'),
                           CRMText::_('COM_CRMERY_AUGUST'),
                           CRMText::_('COM_CRMERY_SEPTEMBER'),
                           CRMText::_('COM_CRMERY_OCTOBER'),
                           CRMText::_('COM_CRMERY_NOVEMBER'),
                           CRMText::_('COM_CRMERY_DECEMBER')  );
    }
    
    /*
     * Get abbreviated month names for report page charts and data creation
     */
    function getMonthNamesShort($startDate=null,$endDate=null){
        $dates = array( CRMText::_('COM_CRMERY_JAN'),
                        CRMText::_('COM_CRMERY_FEB'),
                        CRMText::_('COM_CRMERY_MAR'),
                        CRMText::_('COM_CRMERY_APR'),
                        CRMText::_('COM_CRMERY_MAY'),
                        CRMText::_('COM_CRMERY_JUN'),
                        CRMText::_('COM_CRMERY_JUL'),
                        CRMText::_('COM_CRMERY_AUG'),
                        CRMText::_('COM_CRMERY_SEP'),
                        CRMText::_('COM_CRMERY_OCT'),
                        CRMText::_('COM_CRMERY_NOV'),
                        CRMText::_('COM_CRMERY_DEC')  );

        $startDate = $startDate == null ? date("Y-01-01") : $startDate;
        $endDate = $endDate == null ? date("Y-12-31") : $endDate;

        $total = self::getMonths($startDate,$endDate);
        $return = array();

        for ( $i=0; $i<$total; $i++ ){
          $key = ( (int)date("m",strtotime("$startDate + $i months")) - 1 ) % 12;
          $return[] = $dates[$key];
        }

        return $return;

    }
    
    /*
     * Get month start and end dates and names for report page charts and data creation
     */
    function getMonthDates($startDate=null,$endDate=null){
        $startDate = $startDate == null ? date('Y-01-01 00:00:00') : $startDate;
        $endDate = $endDate == null ? date('Y-12-31 00:00:00') : $endDate;
        $month_names = CrmeryHelperDate::getMonthNames();
        $months = array();

        $total = self::getMonths($startDate,$endDate);

        for ( $i=0; $i<$total; $i++ ){
          $key = ( (int)date("m",strtotime("$startDate + $i months")) - 1 ) % 12;
          $months[$i] = array( 'name'=>$month_names[$key],'date'=>date('Y-m-d 00:00:00',strtotime("$startDate + $i months")));
        }

        return $months;
    }

    function getMonths($start, $end) {
        $startParsed = date_parse_from_format('Y-m-d', $start);
        $startMonth = $startParsed['month'];
        $startYear = $startParsed['year'];

        $endParsed = date_parse_from_format('Y-m-d', $end);
        $endMonth = $endParsed['month'];
        $endYear = $endParsed['year'];

        return ($endYear - $startYear) * 12 + ($endMonth - $startMonth) + 1;
    }

    function getTimeIntervals(){

        $starttime = "7:00:00";
        $temptime = strtotime($starttime);
        $nextday = strtotime($starttime." + 1 day");

        $times = array();
        do {
              $times[date("H:i:s",$temptime)] = date(CrmeryHelperUsers::getTimeFormat(),$temptime);
              $temptime = date("Y-m-d H:i:s",$temptime+(15*60));
              $temptime = strtotime($temptime);
        } while ($temptime<$nextday);

        return $times;

    }

    function getSiteTimezone(){

        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select("timezone")->from("#__crmery_config")->where("id=1");
        $db->setQuery($query);
        return $db->loadResult();

    }

    function formatDBDate($date, $time=true){
        $userTz = CrmeryHelperUsers::getTimezone();
        $dateTime = new DateTime($date, new DateTimeZone($userTz));

        $utc = "UTC";
        $dateTime->setTimeZone(new DateTimeZone($utc));

        if(!$time) {
            $mysql = "Y-m-d";
        } else {
            $mysql = "Y-m-d H:i:s";
        }

        $time = $dateTime->format($mysql);

        return $time;
    }

    function formatDate($date,$time=false,$useUserDateFormat=TRUE){

      if ( strtotime($date) <= 0 ){
        return "";
      }

      try{

        $dateTime = CrmeryHelperDate::convertDateToUserTimezone($date);
        $date_format = CrmeryHelperUsers::getDateFormat();

        if ( $useUserDateFormat ){
          $date = $dateTime->format($date_format);
        }else{
          $date =  $dateTime->format("Y-m-d H:i:s");
        }

        if($time) {
            if ( !(date("H:i:s",strtotime($date))=="00:00:00")){
              if ( is_string($time) ){
                $time_format = date($time,strtotime($date));
              }else{
                $time_format = self::formatTime($date);
              }
              $date .=' '.$time_format;
            }
        }

        return $date;
      }catch(Exception $e){
        return "";
      }
        
    }

    function formatDateString($date){
      $dateFormat = CrmeryHelperUsers::getDateFormat();
      return date($dateFormat,strtotime($date));
    }

    function formatTimeString($time,$timeFormatOverride=null){
      $timeFormat = $timeFormatOverride ? $timeFormatOverride : CrmeryHelperUsers::getTimeFormat();
      return date($timeFormat,strtotime($time));
    }


    function formatTime($time,$timeFormatOverride=null){

          $exp = explode(" ",$time);
          if ( count($exp) < 1 ){
            $time = "0000-00-00 ".$time;
          }


          $time_format = $timeFormatOverride ? $timeFormatOverride : CrmeryHelperUsers::getTimeFormat();
          $dateTime = self::convertDateToUserTimezone($time);
          
          return $dateTime->format($time_format);

    }

    function convertDateToUserTimezone($date,$returnString=false){

        $userTz = CrmeryHelperUsers::getTimezone();
        $utc = "UTC";
        
        $dateTime = new DateTime($date, new DateTimeZone($utc));
        $dateTime->setTimezone(new DateTimeZone($userTz));

        return $returnString ? $dateTime->format("Y-m-d H:i:s") : $dateTime;

    }


    function getCurrentTime($string=FALSE,$showTime=FALSE){
        $timezone = CrmeryHelperUsers::getTimezone();
        $current    = new DateTime();
        $current->setTimezone(new DateTimeZone($timezone));
        if ( $string ){
          $format = $showTime ? "Y-m-d H:i:s" : "Y-m-d";
          return $current->format($format);
        }
        return $current;
    }

    function getElapsedTime($date,$showDays=TRUE,$showMonths=TRUE,$showYears=TRUE, $showHours=TRUE, $showMinutes=TRUE,$showSeconds=FALSE){

        $date = self::convertDateToUserTimezone($date,true);
        $time = time() - strtotime($date); // to get the time since that moment

        $tokens = array (
            31536000 => CRMText::_('COM_CRMERY_YEAR'),
            2592000 => CRMText::_('COM_CRMERY_MONTH'),
            604800 => CRMText::_('COM_CRMERY_WEEK'),
            86400 => CRMText::_('COM_CRMERY_DAY'),
            3600 => CRMText::_('COM_CRMERY_HOUR'),
            60 => CRMText::_('COM_CRMERY_MINUTE'),
            1 => CRMText::_('COM_CRMERY_SECOND'),
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
        }

        return CrmeryHelperDate::formatDate($date);
        
    }

    function pluralize( $count, $text )
    {
        return $count . ( ( $count == 1 ) ? ( " $text " ) : ( " ${text}s " ) );
    }

    function x_week_range(&$start_date, &$end_date, $date) {
      $ts = strtotime($date);
      $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
      $start_date = date('Y-m-d H:i:s', $start);
      $end_date = date('Y-m-d H:i:s', strtotime('next saturday', $start));
    }

    function getRelativeDate($display_date) 
    {

            $now                  = self::getCurrentTime(TRUE,FALSE);
            $tomorrow             = self::getCurrentTime(TRUE,FALSE)." + 1 days";
            $day_after_tomorrow   = self::getCurrentTime(TRUE,FALSE)." + 2 days";

            $this_week            = null;
            $next_week            = null;
            self::x_week_range($this_week,$next_week,self::getCurrentTime(true,FALSE));

            $week_after_next = null;
            $week_after_next_week = null;
            self::x_week_range($week_after_next,$week_after_next_week,self::getCurrentTime(true,FALSE)." + 1 week");
            
            $next_month           = date("Y-m-1",strtotime(self::getCurrentTime(TRUE,FALSE)." +1 month"));
            $next_next_month      = date("Y-m-1",strtotime(self::getCurrentTime(TRUE,FALSE)." +2 months"));

            if(strtotime($display_date) < strtotime($now)) {
                $current_heading = CRMText::_('COM_CRMERY_LATE_ITEMS');
            } elseif(strtotime($display_date) >= strtotime($now) && strtotime($display_date) < strtotime($tomorrow) ) {
                $current_heading = CRMText::_('COM_CRMERY_TODAY');
            } elseif(strtotime($display_date) >= strtotime($tomorrow) && strtotime($display_date) < strtotime($day_after_tomorrow) ) {
                $current_heading = CRMText::_('COM_CRMERY_TOMORROW');
            } elseif(strtotime($display_date) >= strtotime($day_after_tomorrow) && strtotime($display_date) < strtotime($week_after_next)) {
                $current_heading = CRMText::_('COM_CRMERY_THIS_WEEK');
            } elseif(strtotime($display_date) >= strtotime($week_after_next) && strtotime($display_date) < strtotime($week_after_next_week)) {
                $current_heading = CRMText::_('COM_CRMERY_NEXT_WEEK');
            } elseif(strtotime($display_date) >= strtotime($week_after_next_week) && strtotime($display_date) < strtotime($next_month)) {
                $current_heading = CRMText::_('COM_CRMERY_THIS_MONTH');
            } elseif(strtotime($display_date) >= strtotime($next_month) && strtotime($display_date) < strtotime($next_next_month)){
                $current_heading = CRMText::_('COM_CRMERY_NEXT_MONTH');
            } else {
                $current_heading = CRMText::_('COM_CRMERY_IN_THE_FUTURE');
            }

        return $current_heading;
    }

}