<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 class CrmeryHelperBanter extends JObject
 {	
 	
	public function hasBanter()
	{
        jimport('joomla.filesystem.folder');
        if ( JFolder::exists(JPATH_ROOT.'/administrator/components/com_banter') ){
            return 1;
        }else{
            return 0;
        }
	}

 	function getAssociationName($associationId,$associationType){
 		$db =& JFactory::getDBO();
 		$query = $db->getQuery(true);

 		switch ( $associationType ){
 			case "deal":
 				$table = "deals";
 				$select = "name";
 			break;
 			case "person":
 				$table = "people";
 				$select = "CONCAT(first_name,' ',last_name)";
 			break;
 			case "company":
 				$table = "companies";
 				$select = "name";
			break;
 		}

 		$query->select($select)
 			->from("#__crmery_".$table)
 			->where("id=".$associationId);

		$db->setQuery($query);
		$result = $db->loadResult();

		return $result;
 	}

 	function getAssociationLink($associationId,$associationType){

 		switch( $associationType ){
 			case "deal":
 				$view = "deals";
 			break;
 			case "person":
 				$view = "people";
 			break;
 			case "company":
 				$view = "companies";
 			break;
 		}

 		return JRoute::_('index.php?option=com_crmery&view='.$view."&id=".$associationId);
 	}

 }