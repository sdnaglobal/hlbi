<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryHelperCustom extends JObject
{

	function generateCustom($type,$id=null) {
 					
 		//base html
 		$return = array(); 
		
		//grab db
		$db = JFactory::getDbo();
		
		//generate query based on type
 		$query = $db->getQuery(true);
		//determine specific entry to generate
		$query->select("cf.* FROM #__crmery_".$type."_custom AS cf");

		$query->order("cf.ordering");
		
		//set query
		$db->setQuery($query);
		$row = $db->loadAssocList();
		
		//determine selected values
        if ( $id ) {
            $custom_data = self::getCustomData($id,$type);
			
        }

        if( is_array($row) && count($row) > 0 ) {

			//loop for explosion delims
			foreach ( $row as $custom ) {

				//retrieve custom values
				$custom['values'] = json_decode($custom['values']);

                //determine selected values
                if ( $id && $custom['type'] != 'forecast' ){
                    $custom['selected'] = ( array_key_exists($custom['id'],$custom_data) ) ? $custom_data[$custom['id']] : CRMText::_('COM_CRMERY_CLICK_TO_EDIT'); 
                }
				//append items to array
				$return[] = $custom;
			}
		}

		//return
		return $return;
	
	}

	function getCustomFields($type){

		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);

		$query->select("cf.*")
			->from("#__crmery_".$type."_custom AS cf");

		$db->setQuery($query);
		$results = $db->loadObjectList("name");

		$fields = array();
		if ( count($results) > 0 ){
			foreach($results as $key => $field){
				$fields[strtolower($key)] = $field;
				//retrieve custom values
				$field->values = json_decode($field->values);
			}
		}

		return $fields;

	}

	function getCustomCsvData($itemId,$itemType){

		switch($itemType){
			case "people":
				$table = "people";
			break;
			case "companies":
				$table = "company";
			break;
			case "deals":
				$table = "deal";
			break;
			default:
				$table = $itemType;
			break;
		}

		$customData = self::getCustomData($itemId,$table,"name","values",true,true);
		return $customData;
	}



	//get custom data to prefill dropdowns
    function getCustomData($id,$type,$key="id",$label="keys",$delimitArray=false,$displayData=false){
        //get dbo
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query->select("cf.*,f.name,f.values,f.type,f.multiple_selections,cf.custom_field_id FROM #__crmery_".$type."_custom AS f");
        $query->leftJoin("#__crmery_".$type."_custom_cf AS cf ON f.id=cf.custom_field_id AND cf.".$type."_id=".$id);
        //return results
        $db->setQuery($query);
        $db_results = $db->loadAssocList();
		
		/*echo "<pre>";
		print_r($db_results);
		die;
		*/
		$results = array();
        if ( count($db_results) != 0 ){
	
				
            foreach ( $db_results as $id => $row ){
			
					

            	switch ( $key ){
            		case "id":
            			$keyName = $row['custom_field_id'];
        			break;
        			case "name":
        				$keyName = strtolower($row['name']);
    				break;
    				default:
    					$keyName = $key;
					break;
            	}
				
				

            	if ( $row[$type.'_id'] > 0 ){
					//die;
            		$valueData = self::getCustomValue($type,$row['custom_field_id'],$row['value'],null,$displayData);
            	}else{
            		$valueData = $row['multiple_selections'] == 1 ? array() : "";
            	}

            	if ( $delimitArray ){
            		$values = $row['multiple_selections'] == 1 ? implode(",",($valueData)) : $valueData;
            	}else{
            		$values = $valueData;
            	}

            	$results[$keyName] = $values;
            	
            }
        }
		
		return $results;
        
    }


    /**
     * Get custom field values from picklists // forecasts // otherwise return the value as it was an input field
     */
    function getCustomValue($customType,$customNameOrId,$customValue,$itemId=null,$displayValues=false){
    	$db =& JFactory::getDBO();
    	$query = $db->getQuery(true);

    	$id = strpos($customNameOrId,"custom_") !== false ? str_replace("custom_","",$customNameOrId) : $customNameOrId;

    	$query->select("c.name,c.type,c.values,c.multiple_selections")
    		->from("#__crmery_".$customType."_custom AS c")
			->where("c.id=".$id);
		$db->setQuery($query);

		$custom = $db->loadObject();
		switch ( $custom->type ){
			case "forecast":
				if ( $itemId > 0 ){
					$query->clear();
					$query->select("(d.amount * ( d.probability / 100 )) AS amount")
						->from("#__crmery_deals AS d")
						->where("d.id=".$itemId);
					$db->setQuery($query);
					$result = $db->loadResult();
					return $result;
				}else{
					return $customValue;
				}
			break;
			case "currency":
				return $customValue;
			break;
			case "picklist":
				
				$values = json_decode($custom->values);
				if ( $custom->multiple_selections == 0 ){
					$key = array_key_exists($customValue,$values) ? $values[$customValue] : CRMText::_('COM_CRMERY_NONE');
					
					$valueKey = array_search($customValue,$values);
					if ( $displayValues ){
						$value = array_key_exists($customValue,$values) ? $values[$customValue] : "";
						return $value;
					}else{
						return $key;
					}
				}else{
					if ( !is_array($customValue) ){
						$customValue = @unserialize($customValue);
					}
					$keys = array();
					$keyValues = array();
					if ( is_array($customValue) ){
						foreach ( $customValue as $key => $value ){
							if ( array_key_exists($value,$values) && $values[$value] == 1 ){
								$keys[] = $key;
							}
							if ( in_array($value,$customValue) ){
								$keyValues[$key] = $value;
							}
						}
					}
					if ( $displayValues == true ){
						$returnValues = array();
						foreach ( $keyValues as $valueKey => $toggled ){
							if ( $toggled == 1 && array_key_exists($valueKey,$values) ){
								$returnValues[] = $values[$valueKey];
							}
						}
						return $returnValues;
					} else {
						return count($keys) > 0 ? $keys : $keyValues;
					}
				}
			break;
			case "date":
				return $customValue;
			break;
			default:
				return $customValue;
			break;
		}

		return $customValue;
    }

    function cleanCustomForm($data){
		if ( count($data) > 0 ){
			foreach ( $data as $key => $field ){
				if ( $field['type'] == "date" || $field['type'] == "forecast" ){
					unset($data[$key]);
				}else{
					$data[$key]['display'] = $field['name'];
					$data[$key]['name'] = "custom_".$field['id'];
				}
			}
		}
		return $data;
	}

	function getCustomTypes($type){
    	switch ( $type ){
    		case "deal":
	            $arr = array(  'number'    =>  JText::_('COM_CRMERY_NUMBER'),
	                            'text'      =>  JText::_('COM_CRMERY_ADMIN_GENERIC_TEXT'),
	                            'currency'  =>  JText::_('COM_CRMERY_CURRENCY'),
	                            'picklist'  =>  JText::_('COM_CRMERY_PICKLIST'),
	                            'forecast'  =>  JText::_('COM_CRMERY_FORECAST'),
	                            'date'      =>  JText::_('COM_CRMERY_DATE')  );
        	break;
        	case "company":
	            $arr = array(  'number'    =>  JText::_('COM_CRMERY_NUMBER'),
	                            'text'      =>  JText::_('COM_CRMERY_ADMIN_GENERIC_TEXT'),
	                            'currency'  =>  JText::_('COM_CRMERY_CURRENCY'),
	                            'picklist'  =>  JText::_('COM_CRMERY_PICKLIST'),
	                            'date'      =>  JText::_('COM_CRMERY_DATE')  );
        	break;
        	case "people":
	            $arr = array(  'number'    =>  JText::_('COM_CRMERY_NUMBER'),
	                            'text'      =>  JText::_('COM_CRMERY_ADMIN_GENERIC_TEXT'),
	                            'currency'  =>  JText::_('COM_CRMERY_CURRENCY'),
	                            'picklist'  =>  JText::_('COM_CRMERY_PICKLIST'),
	                            'date'      =>  JText::_('COM_CRMERY_DATE')  );
        	break;
    	}
    	return $arr;
    }

    public function isReported($reportId)
    {
    	$db =& JFactory::getDbo();
    	$query = $db->getQuery(true);
    	$query->select("reported")
    		->from("#__crmery_deal_custom")
    		->where("id=".$reportId);
		$db->setQuery($query);
		return $db->loadResult();
    }

}