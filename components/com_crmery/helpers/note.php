<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 class CrmeryHelperNote extends JObject
 {

    //get category listings for notes
    function getCategories() {
 
        //grab db
        $db = JFactory::getDbo();
        
        //generate query 
        $query = $db->getQuery(true);
        $query->select("name,id");
        $query->from("#__crmery_notes_categories");
        $query->order('ordering');
        
        //return results
        $db->setQuery($query);
        $results = $db->loadAssocList();
        $categories = array();
        foreach ( $results as $key=>$category ){
            $categories[$category['id']] = $category['name']; 
        }
        return $categories;
        
    }

        
 }