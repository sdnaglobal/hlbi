<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 class CrmeryHelperUsers extends JObject
 {
    
    //get users depending on logged in member type
    function getUsers($id=null,$idsOnly=FALSE){
        
        //filter based on current logged in user
        $user = CrmeryHelperUsers::getUserId();
        $user_role = CrmeryHelperUsers::getRole();
        $results = array();
        
        //user role filters
        if( $user_role != 'basic' ){
            
            //get db
            $db = JFactory::getDBO();
            $query = $db->getQuery(true);

            $select = ( $idsOnly ) ? "id AS value,CONCAT(first_name,' ',last_name) AS label" : "*";
            
            //get users
            $query->select($select);
            $query->from("#__crmery_users");
            
            //exec
            if ( $id ){
                $query->where("id=$id");
            }else if ( $user_role == 'exec') {
                // $query->where("id<>".$user);
            //manager    
            }else if ( $user_role == 'manager' ){
                $team_id = CrmeryHelperUsers::getTeamId();
                $query->where('team_id='.$team_id/*.' AND id <> '.$user*/);
            }
            
            //load results
            $query->where("published=1");
            $db->setQuery($query);
            $results = $db->loadAssocList();

        }
        
        //assign other user info
        if ( !$idsOnly ){
            if ( count($results) > 0 ){
                foreach ( $results as $key=>$user ){
                    $results[$key]['emails'] = CrmeryHelperUsers::getEmails($user['id']);
                }
            }
        }
        
        //return
        return $results;
    }

    function getFirstName($id=null){

        $id = $id ? $id : self::getLoggedInUser()->id;

        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->clear()->select("first_name")->from("#__crmery_users")->where("id=".$id);
        $db->setQuery($query);
        return $db->loadResult();

    }
	
	// get logged user firm ids
	 function getFirmsIds($id=null){

        $id = $id ? $id : self::getLoggedInUser()->id;
		$user_role = CrmeryHelperUsers::getRole();
		 if($user_role == 'manager')
		 {
			$field = 'company_id';
		 }
		 else
		 {
			$field = 'inp_company_id';
		 }
		 

        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->clear()->select($field)->from("#__crmery_users")->where("id=".$id);
        $db->setQuery($query);
        return $db->loadResult();

    }
	

    //get all company users
    function getCompanyUsers($id=null){
        
        //filter based on current logged in user
        $user = JFactory::getUser();
        $user_role = CrmeryHelperUsers::getRole();
        $results = array();
        
        //get db
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
            
        //get users
        $query->select("*");
        $query->from("#__crmery_users");
        
        //load results
        $query->where("published=1");
        $db->setQuery($query);
        $results = $db->loadAssocList();
        
        //assign other user info
        foreach ( $results as $key=>$user ){
            $results[$key]['emails'] = CrmeryHelperUsers::getEmails($user['id']);
        }
        
        //return
        return $results;
    }

    /**
     * Get user email address for a user
     * @param int $id user id to get emails for
     * @return mixed $results db results
     */
    function getEmails($id=null){

        //Crmery User ID
        if ( !$id ){
            $id = CrmeryHelperUsers::getUserId();
        }
        
        //get dbo
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query->select("*")->from("#__crmery_users_email_cf")->where("member_id=".$id);
        
        //load and return results
        $db->setQuery($query);
        $email_cf = $db->loadAssocList();

        $query->clear()
                ->select("j.email,u.id AS member_id")
                ->from("#__crmery_users AS u")
                ->leftJoin("#__users AS j ON j.id=u.uid")
                ->where("u.id=".$id);

        $primary = $db->loadAssocList();

        $emails = array_merge($email_cf,$primary);

        return $emails;
        
    }
    
    //return current logged in CRMery user ID based on Joomla Id
    function getUserId(){
        
        //get db
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //logged in user
        $user = JFactory::getUser();
        
        //get id
        $query->select("id");
        $query->from("#__crmery_users");
        $query->where('uid='.$user->id);
        
        //return id
        $db->setQuery($query);
        return $db->loadResult();
        
    }
    
    //return user role
    function getRole($user_id=null){
        //get db
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //logged in user
        if ( !$user_id ){
            $user_id = JFactory::getUser()->id;
        }
        
        //get id
        $query->select("role_type");
        $query->from("#__crmery_users");
        $query->where('uid='.$user_id);
        
        //return id
        $db->setQuery($query);
        return $db->loadResult();
    }
    
    //return user team id
    function getTeamId($user_id=null){
       //get db
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        
        $user_id = $user_id ? $user_id : CrmeryHelperUsers::getUserId();

        if ( !$user_id ){
            return 0;
        }
        
        //get id
        $query->select("team_id");
        $query->from("#__crmery_users");
        $query->where('id='.$user_id);
        
        //return id
        $db->setQuery($query);
        $result = $db->loadResult();

        return $result;

    }
    
    //return teams to execs
    function getTeams($id=null){
        //get db
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query->select("t.*,u.first_name,u.last_name,IF(t.name!='',t.name,CONCAT(u.first_name,' ',u.last_name)) AS team_name");
        $query->from("#__crmery_teams AS t");
        $query->leftJoin("#__crmery_users AS u ON u.id = t.leader_id AND u.published=1");
        
        //search for specific team
        if ( $id ){
            $query->where("t.team_id=$id");
        }

        $user_role = CrmeryHelperUsers::getRole();
        $user_id = CrmeryHelperUsers::getUserId();
        if ( $user_role == 'manager' ){
            $team_id = CrmeryHelperUsers::getTeamId();
            $query->where('t.team_id='.$team_id);
        }
        $query->where("u.published=1");
        
        //return results
        $db->setQuery($query);
        $teams = $db->loadAssocList();

        return $teams;
    }
    /**
     * Get users associated with a specific team
     * @param int $id specific team id requested
     * @return mixed $results results from database
     */
    function getTeamUsers($id=null,$idsOnly=FALSE){
        //get db
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        if ( $idsOnly ){
            $select = "u.id";
        }else{
            $select = "u.*";
        }

        $id = $id ? $id : CrmeryHelperUsers::getTeamId();

        //query
        $query->select($select)->from("#__crmery_users AS u")->where("u.team_id=$id AND u.published=1");
        
        //return results
        $db->setQuery($query);


        if ( $idsOnly ){
            $users = $db->loadResultArray();
        }else{
            $users = $db->loadAssocList();
        }

        return $users;
    }

    function getAllSharedUsers(){

        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);

        $query->select("id AS value,CONCAT(first_name,' ',last_name) AS label")
            ->from("#__crmery_users")
            ->where("published=1");

        $role = CrmeryHelperUsers::getRole();

        switch ( $role ){
            case "manager":
            case "basic":
                $query->where("team_id=".CrmeryHelperUsers::getTeamId());
            break;  
        }

        $db->setQuery($query);
        $users = $db->loadObjectList();

        return $users;

    }

    function getItemSharedUsers($itemId,$itemType){

        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);

        $query->select("s.user_id AS value,CONCAT(u.first_name,' ',u.last_name) AS label")
            ->from("#__crmery_shared AS s")
            ->leftJoin("#__crmery_users AS u ON u.id = s.user_id AND u.published=1");

        $query->where("s.item_id=".$itemId);
        $query->where("s.item_type=".$db->Quote($itemType));

        $db->setQuery($query);
        $users = $db->loadObjectList();

        return $users;

    }
    
    /**
     * Get deal count associated with users
     * @param $id int User Id to filter for
     * @param $team int Team Id associated to user
     * @param $role String User role to filter for
     * @return int Count of deals returned from database
     */
    function getDealCount($id,$team,$role){
        
        //get db
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
       
        //query
        $query->select('count(*)');
        $query->from('#__crmery_deals AS d');
        $query->leftJoin("#__crmery_users AS u ON u.id = d.owner_id AND u.published=1");
        
        //filter based on id and role
        if  ( $role != 'exec' ){
            if ( $role == 'manager' ){
                $query->where("u.team_id=$team");
            }else{
                $query->where("d.owner_id=$id");
            }
        }
        $query->where("d.published=1");
        
        //return results
		// echo '<br>**************<br>'.$query;
        $db->setQuery($query);
        return $db->loadResult();
    }

    /**
     * Get deal count associated with users
     * @param $id int User Id to filter for
     * @param $team int Team Id associated to user
     * @param $role String User role to filter for
     * @return int Count of deals returned from database
     */
    function getEventCount($id,$team,$role){
        
        //get db
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query->select('count(*)');
        $query->from('#__crmery_events AS e');
        $query->leftJoin("#__crmery_users AS u ON ( u.id = e.owner_id OR u.id = e.assignee_id ) AND u.published=1");
        
        //filter based on id and role
        if  ( $role != 'exec' ){
            if ( $role == 'manager' ){
                $query->where("u.team_id=$team");
            }else{
                $query->where("e.owner_id=$id");
            }
        }
        $query->where("e.published=1");
        
        //return results
        $db->setQuery($query);
        $count = $db->loadResult();

        return $count;
    }
    
    /**
     * Get people count associated with users
     * @param $id int User Id to filter for
     * @param $team int Team Id associated to user
     * @param $role String User role to filter for
     * @return int Count of people returned from database
     */
    function getPeopleCount($id=null,$team=null,$role=null){
        
        //get db
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        if ( !$id ){
            $id = CrmeryHelperUsers::getUserId();
        }
        if ( !$team ){
            $team = CrmeryHelperUsers::getTeamId();
        }
        if ( !$role ){
            $role = CrmeryHelperUsers::getRole();
        }
        
        //query
        $query->select('count(*)');
        $query->from('#__crmery_people AS p');
        $query->leftJoin("#__crmery_users AS u ON ( u.id = p.owner_id OR u.id = p.assignee_id ) AND u.published=1");
        
        //filter based on id and role
        if  ( $role != 'exec' ){
            if ( $role == 'manager' ){
                $query->where("u.team_id=$team");
            }else{
                $query->where("( p.owner_id=$id OR p.assignee_id=$id )");
            }
        }

        $query->where("p.published=1");
        
        //return results
        $db->setQuery($query);
        return $db->loadResult();
    }

    /**
     * Get people count associated with users
     * @param $id int User Id to filter for
     * @param $team int Team Id associated to user
     * @param $role String User role to filter for
     * @return int Count of people returned from database
     */
    function getLeadCount($id=null,$team=null,$role=null){
        
        //get db
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        if ( !$id ){
            $id = CrmeryHelperUsers::getUserId();
        }
        if ( !$team ){
            $team = CrmeryHelperUsers::getTeamId();
        }
        if ( !$role ){
            $role = CrmeryHelperUsers::getRole();
        }
        
        //query
        $query->select('count(*)');
        $query->from('#__crmery_people AS p');
        $query->leftJoin("#__crmery_users AS u ON ( u.id = p.owner_id OR u.id = p.assignee_id ) AND u.published=1");
        
        //filter based on id and role
        if  ( $role != 'exec' ){
            if ( $role == 'manager' ){
                $query->where("u.team_id=$team");
            }else{
                $query->where("( p.owner_id=$id OR p.assignee_id=$id )");
            }
        }

        $query->where("p.type='lead'");

        $query->where("p.published=1");
        
        //return results
        $db->setQuery($query);
        return $db->loadResult();
    }

    /**
     * Get people count associated with users
     * @param $id int User Id to filter for
     * @param $team int Team Id associated to user
     * @param $role String User role to filter for
     * @return int Count of people returned from database
     */
    function getContactCount($id=null,$team=null,$role=null){
        
        //get db
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        if ( !$id ){
            $id = CrmeryHelperUsers::getUserId();
        }
        if ( !$team ){
            $team = CrmeryHelperUsers::getTeamId();
        }
        if ( !$role ){
            $role = CrmeryHelperUsers::getRole();
        }
        
        //query
        $query->select('count(*)');
        $query->from('#__crmery_people AS p');
        $query->leftJoin("#__crmery_users AS u ON ( u.id = p.owner_id OR u.id = p.assignee_id ) AND u.published=1");
        
        //filter based on id and role
        if  ( $role != 'exec' ){
            if ( $role == 'manager' ){
                $query->where("u.team_id=$team");
            }else{
                $query->where("( p.owner_id=$id OR p.assignee_id=$id )");
            }
        }

        $query->where("p.type='contact'");

        $query->where("p.published=1");
        
        //return results
        $db->setQuery($query);
        return $db->loadResult();
    }

    /**
     * Get people emails associated with users
     * @param $id int User Id to filter for
     * @param $team int Team Id associated to user
     * @param $role String User role to filter for
     * @return int Count of people returned from database
     */
    function getPeopleEmails($id=null){
        
        //get db
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        if ( !$id ){
            $id = CrmeryHelperUsers::getUserId();
        }
        
        //query
        $query->select('p.id,p.email');
        $query->from('#__crmery_people AS p');
        $query->leftJoin("#__crmery_shared AS shared ON shared.item_id=p.id AND shared.item_type='person'");
        $query->where("(p.owner_id=$id OR p.assignee_id=$id OR shared.user_id=$id)");
        
        //return results
        $db->setQuery($query);
        $results = $db->loadAssocList();

        //clean results
        $return = array();
        if ( $results ){
            foreach ( $results as $key=>$user ){
                $return[$user['id']] = $user['email'];
            }
        }

        return $return;
    }
    
    /**
     * Get company count associated with users
     * @param $id int User Id to filter for
     * @param $team int Team Id associated to user
     * @param $role String User role to filter for
     * @return int Count of companies returned from database
     */
    function getCompanyCount($id,$team,$role){
        
        //get db
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query->select('count(*)');
        $query->from('#__crmery_companies AS c');
        $query->leftJoin("#__crmery_users AS u ON u.id = c.owner_id AND u.published=1");
        $query->where("c.published=1");
        
        //return results
        $db->setQuery($query);
        return $db->loadResult();
    }
    
    /**
     * Get commission rates for users
     * @param int $id user id requested else logged in user id is used
     * @return int commission rate
     */
    function getCommissionRate($id=null){
       //get db
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //logged in user
        if ( $id == null ){
            $user = JFactory::getUser();
            $user_id = CrmeryHelperUsers::getUserId();
        }else{
            $user_id = $id;
        }
        
        //get id
        $query->select("commission_rate");
        $query->from("#__crmery_users");
        $query->where('id='.$user_id);
        $query->where("published=1");
        
        //return id
        $db->setQuery($query);
        return $db->loadResult();
    }


    function isFullscreen() {

        if ( CrmeryHelperConfig::getConfigValue("fullscreen") == 1 ){
            return true;
        }else{
            
            $db = JFactory::getDBO();
            $user_id = CrmeryHelperUsers::getUserId();

            if ( $user_id > 0 ){

                $query = $db->getQuery(true);
                $query->select('fullscreen');
                $query->from('#__crmery_users');
                $query->where('id='.$user_id);
                $db->setQuery($query);

                return $db->loadResult();

            }
        }

    }

    function getDateFormat($php=TRUE){

        //get db
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //logged in user
        $user = JFactory::getUser();
        $user_id = CrmeryHelperUsers::getUserId();

        if ( $user_id > 0 ){
        
            //get id
            $query->select("date_format");
            $query->from("#__crmery_users");
            $query->where('id='.$user_id);
            
            //return id
            $db->setQuery($query);
            $format = $db->loadResult();
            $format = $format ? $format : "m/d/y";

        } else { 

            return "m/d/y";

        }

        if ( !$php ){
            $format = str_replace("m","mm",$format);
            $format = str_replace("d","dd",$format);
        }

        return $format;

    }

    function getTimeFormat($id=null){

        //get db
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //logged in user
        if ( $id == null ){
            $user = JFactory::getUser();
            $user_id = CrmeryHelperUsers::getUserId();
        }else{
            $user_id = $id;
        }

        if ( $user_id > 0 ){
        
            //get id
            $query->select("time_format");
            $query->from("#__crmery_users");
            $query->where('id='.$user_id);
            
            //return id
            $db->setQuery($query);
            $time = $db->loadResult();
            $time = $time ? $time : CrmeryHelperConfig::getConfigValue("time_format");
            return $time;

        } else {

            return CrmeryHelperConfig::getConfigValue('time_format');

        }

    }

    function getTimezone($id=null){

        //get db
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //logged in user
        if ( $id == null ){
            $user = JFactory::getUser();
            $user_id = CrmeryHelperUsers::getUserId();
        }else{
            $user_id = $id;
        }

        if ( $user_id > 0 ){
        
            //get id
            $query->select("time_zone");
            $query->from("#__crmery_users");
            $query->where('id='.$user_id);

             //return id
            $db->setQuery($query);
            $tz =  $db->loadResult();
            $tz = $tz ? $tz : CrmeryHelperConfig::getConfigValue('timezone');
            return $tz;

        } else{

            return CrmeryHelperConfig::getConfigValue('timezone');

        }

    }

    function getLoggedInUser()
    {
        $user_id = JFactory::getUser()->id;
        $db = JFactory::getDBO();

        $query = $db->getQuery(true);
        $query->select('c.*,u.email');
        $query->from('#__crmery_users AS c');
        $query->where('c.uid = '.$db->Quote($user_id));
        $query->where("c.published=1");
        $query->leftJoin("#__users AS u ON u.id=c.uid");
        $db->setQuery($query);
        $user = $db->loadObject();
        if ( $user->id > 0 ){
            $user->emails = CrmeryHelperUsers::getEmails($user->id);
            $user->document_bypass = unserialize($user->document_bypass);
        }

        return $user;
    }

    function getDocumentBypass(){
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        $userId = CrmeryHelperUsers::getLoggedInUser()->id;
        $query->select("document_bypass")->from("#__crmery_users")->where("id=".$userId);
        $db->setQuery($query);
        $result = $db->loadResult();
        if ( $result != "" && !is_null($result) ){
            try {
                $return = unserialize($result);
            }catch(Exception $e){
                $return = array();
            }
        }else{
            $return = array();
        }
        return $return;
    }

    function getUser($user_id,$array=FALSE)
    {
        $db = JFactory::getDBO();

        $query = $db->getQuery(true);
        $query->select('c.*');
        $query->from('#__crmery_users AS c');
        $query->where('c.id = '.$db->Quote($user_id));
        $query->where("c.published=1");
        $db->setQuery($query);

        if ( !$array ){
            $user = $db->loadObject();
        }else{
            $user = $db->loadResultArray();
        }

        return $user;
    }

    /** Determine if logged in user ( or specified user ) is an administrator **/
    function isAdmin($user_id=null){

        $db = JFactory::getDBO();

        $user_id = $user_id ? $user_id : CrmeryHelperUsers::getUserId();

        $query = $db->getQuery(true);
        $query->select('c.admin');
        $query->from('#__crmery_users AS c');
        $query->where('c.id = '.$db->Quote($user_id));
        $db->setQuery($query);
        $user = $db->loadObject();

        return $user->admin == 1;

    }

    /** Determine if logged in user ( or specified user ) can delete items **/
    function canDelete($user_id=null){

        $db = JFactory::getDBO();

        $user_id = $user_id ? $user_id : CrmeryHelperUsers::getUserId();

        $query = $db->getQuery(true);
        $query->select('c.admin,c.can_delete');
        $query->from('#__crmery_users AS c');
        $query->where('c.id = '.$db->Quote($user_id));
        $db->setQuery($query);
        $user = $db->loadObject();

        return ( $user->admin == 1 || $user->can_delete == 1 );

    }

     /** Determine if logged in user ( or specified user ) can export items **/
    function canExport($user_id=null){

        $db = JFactory::getDBO();

        $user_id = $user_id ? $user_id : CrmeryHelperUsers::getUserId();

        $query = $db->getQuery(true);
        $query->select('c.exports,c.admin');
        $query->from('#__crmery_users AS c');
        $query->where('c.id = '.$db->Quote($user_id));
        $db->setQuery($query);
        $user = $db->loadObject();

        return ( $user->exports == 1 || $user->admin == 1 );

    }

    function formatAmount($amount){
        return number_format((float)$amount,2);
    }

    function getGraphDate($graphField,$format=false){

        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);

        $userId = CrmeryHelperUsers::getUserId();

        $query->select($graphField)->from("#__crmery_users")->where("id=".$userId);
        $db->setQuery($query);

        $result = $db->loadResult();

        if ( strtotime($result) <= 0 ){
            switch ( $graphField ){
                case "dashboard_graph_date_start":
                case "report_graph_date_start":
                    $result = date("Y-01-01");
                break;
                case "dashboard_graph_date_end":
                case "report_graph_date_end":
                    $result = date("Y-12-31");
                break;
            }
        }

        return $format ? CrmeryHelperDate::formatDateString($result) : $result;

    }

    function getHomePageChart(){
        $user_id = CrmeryHelperUsers::getUserId();

        $db =& JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select("home_page_chart")->from("#__crmery_users")->where("id=".$user_id);
        $db->setQuery($query);

        $chart = $db->loadResult();
        return ( $chart != "" && !is_null($chart) ) ? $chart : "dealStagePie";
    }

    function getFloats($type){
        $userId = CrmeryHelperUsers::getUserId();

        $db =& JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select($type."_floats_right,".$type."_floats_left")
            ->from("#__crmery_users")
            ->where("id=".$userId);

        $db->setQuery($query);
        $floats = $db->loadAssoc();

        if ( $floats ){

            $return = array();
            $return["left"] = explode(",",$floats[$type."_floats_left"]);
            $return["right"] = explode(",",$floats[$type."_floats_right"]);

            if ( ( count($return['left']) == 0  && count($return['right']) == 0 ) || ( $floats[$type.'_floats_left'] == "" && $floats[$type.'_floats_right'] == "" ) ){
                return CrmeryHelperUsers::getDefaultFloats($type);
            }

            return $return;

        }else{

            return CrmeryHelperUsers::getDefaultFloats($type);

        }

    }

    function getDefaultFloats($type){

        switch ( $type ){
            case "dashboard":
                $return = array();
                $return['left'] = explode(",","tasks_events_float,deals_float");
                $return['right'] = explode(",","sales_float,inbox_float,latest_float");
                return $return;
            break;
            case "goal":
                $return = array();
                $return['left'] = explode(",","leaderboard_float");
                $return['right'] = explode(",","individual_goals_float,team_goals_float,company_goals_float");
                return $return;
            break;
            case "sales_dashboard":
                $return = array();
                $return['left'] = explode(",","deal_stage_float,deal_status_float");
                $return['right'] = explode(",","yearly_commission_float,yearly_revenue_float,monthly_commission_float,monthly_revenue_float");
                return $return;
            break;
        }
        return array('left'=>array(),'right'=>array());

    }

 }