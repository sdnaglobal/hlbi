<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 class CrmeryHelperStyles extends JObject
 {
     
        //get base stylesheets
        function getBaseStyle(){
            return JURI::base()."components/com_crmery/media/css/default.css";
        }
        
        //dynamically generate styles
        function getDynamicStyle(){
            //database
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            
            //query
            $query->select("b.*");
            $query->from("#__crmery_branding AS b");
            $query->where("assigned=1");
            
            //return results
            $db->setQuery($query);
            $theme = $db->loadAssocList();
            $theme = $theme[0];
            
            //assign style declarations
            $style  = "#com_crmery_toolbar{background:#".$theme['header']." !important;}";
            $style .= "#com_crmery_toolbar ul.crmery_menu a:hover{background:#".$theme['tabs_hover']." !important;}";
            $style .= "#com_crmery_toolbar ul.crmery_menu a:hover{color:#".$theme['tabs_hover_text']." !important;}";
            $style .= "table.com_crmery_table th, table.com_crmery_table thead tr{background:#".$theme['table_header_row']." !important;}";
            $style .= "table.com_crmery_table th{color:#".$theme['table_header_text']." !important;}";
            $style .= ".google_map_center{background:#".$theme['table_header_text'].";border:3px solid #".$theme['table_header_row'].";}";
            $style .= "div#com_crmery a, .ui-widget-content .actions a {color: #".$theme['link']." !important;}";
            $style .= "div#com_crmery a:hover, .ui-widget-content a:hover { color: #".$theme['link_hover']." !important;}";

            //return
            return $style;
        }
        
        //load all styles
        function loadStyleSheets(){

            $document =& JFactory::getDocument();

            $view = JRequest::getVar('view');
            if ( $view == "print" ){
                $document->addStyleSheet( JURI::base().'components/com_crmery/media/css/print.css' );
            }
							
			if(CrmeryHelperTemplate::isMobile()) {
                $document->addStyleSheet( JURI::base().'components/com_crmery/media/css/mobile.css' );
				$document->addStyleSheet( JURI::base().'components/com_crmery/media/css/jquery.mobile.min.css' );
                $document->addStyleSheet( JURI::base().'components/com_crmery/media/css/jquery.mobile.datepicker.css' );
			} else {
	            //base stylesheet
	            $base_style = CrmeryHelperStyles::getBaseStyle();
	            
	            //dynamic stylesheet
	            $dyn_style = CrmeryHelperStyles::getDynamicStyle();
	            
	            //add sheets to document
	            $document->addStyleSheet($base_style,'text/css',null,array('data-id'=>"crmeryStylesheet"));
	            $document->addStyleDeclaration($dyn_style);

                //add IE styles....
                $ie = 'components/com_crmery/media/css/ie.css';
                $ieCss = JFile::read($ie);
                $document->addCustomTag("<!--[if IE]><style type='text/css'>".$ieCss."</style><![endif]--> ");
	            
	            //misc styles
                $document->addStyleSheet( JURI::base().'components/com_crmery/media/css/jquery.cluetip.css' );           

			     if(CrmeryHelperUsers::isFullscreen()) {
                    $document->addStyleSheet( JURI::base().'components/com_crmery/media/css/fullscreen.css' );                               
                 }

            }            


            jimport('joomla.environment.browser');
            $browser = &JBrowser::getInstance();
            $browserType = $browser->getBrowser();
            $browserVersion = $browser->getMajor();
            if(($browserType == 'msie') && ($browserVersion < 8))
            {
              $document->addStyleSheet( JURI::base().'components/com_crmery/media/css/ie.css' );   
            }


        }


        
 }
    