<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 class CrmeryHelperQuote extends JObject
 {	
 	
	public function hasCRMQuote()
	{
        jimport('joomla.filesystem.folder');
        if ( JFolder::exists(JPATH_ROOT.'/administrator/components/com_crmquote') ){
            return 1;
        }else{
            return 0;
        }
	}

	public function loadLanguage()
	{
		require_once(JPATH_SITE."/components/com_crmquote/helpers/crmquote.php");
		CRMQuoteHelper::loadLanguage();
	}

	public function getDockView(&$deal)
	{	
		require_once(JPATH_SITE."/components/com_crmquote/helpers/crmquote.php");
		return CRMQuoteHelper::getDockView($deal);
	}

	public function getColumns()
	{
		require_once(JPATH_SITE."/components/com_crmquote/helpers/crmquote.php");
		return CRMQuoteHelper::getColumns();
	}

	public function getCustomReportHeader($alias)
	{
		require_once(JPATH_SITE."/components/com_crmquote/helpers/crmquote.php");
		return CRMQuoteHelper::getCustomReportHeader($alias);
	}

	public function constructQuery(&$query)
	{
		require_once(JPATH_SITE."/components/com_crmquote/helpers/crmquote.php");
		return CRMQuoteHelper::constructQuery($query);
	}

	public function getCustomReportRow($row,$id)
	{
		require_once(JPATH_SITE."/components/com_crmquote/helpers/crmquote.php");
		return CRMQuoteHelper::getCustomReportRow($row,$id);
	}

	public function getColumnFilters()
	{
		require_once(JPATH_SITE."/components/com_crmquote/helpers/crmquote.php");
		return CRMQuoteHelper::getColumnFilters();
	}

	public function getDealHeaders()
	{
		require_once(JPATH_SITE."/components/com_crmquote/helpers/crmquote.php");
		return CRMQuoteHelper::getDealHeaders();
	}

	public function getDealRow($deal)
	{
		require_once(JPATH_SITE."/components/com_crmquote/helpers/crmquote.php");
		return CRMQuoteHelper::getDealRow($deal);
	}

	public function buildActivitySelect(&$query)
	{
		require_once(JPATH_SITE."/components/com_crmquote/helpers/crmquote.php");
		CRMQuoteHelper::buildActivitySelect($query);
	}

	public function buildActivityJoin(&$query)
	{
		require_once(JPATH_SITE."/components/com_crmquote/helpers/crmquote.php");
		CRMQuoteHelper::buildActivityJoin($query);
	}

	public function buildActivityWhere(&$query,$quoteId=null)
	{
		require_once(JPATH_SITE."/components/com_crmquote/helpers/crmquote.php");
		CRMQuoteHelper::buildActivityWhere($query,$quoteId);
	}

}