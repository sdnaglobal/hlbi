<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 class CrmeryHelperEvent extends JObject
 {

    //get category listings for events
    function getCategories($appendLanguage=FALSE) {
 
        //grab db
        $db = JFactory::getDbo();
        
        //generate query 
        $query = $db->getQuery(true);
        $query->select("name,id");
        $query->from("#__crmery_events_categories");

        $query->order("ordering");
        
        //return results
        $db->setQuery($query);
        $results = $db->loadAssocList();
        $categories = array();
        if ( $results && is_array($results) && count($results) > 0 ){
            foreach ( $results as $key=>$category ){
                $categories[$category['id']] = $appendLanguage ? CRMText::_('COM_CRMERY_OF_TYPE').' '.$category['name'] : $category['name']; 
            }
        }
        return $categories;
        
    }
    
    //get repeat intervals
    function getRepeatIntervals() {
        
        return array(
            'none'      => CRMText::_("COM_CRMERY_DOESNT_REPEAT"), 
            'daily'     => CRMText::_("COM_CRMERY_DAILY"),
            'weekdays'  => CRMText::_("COM_CRMERY_EVERY_WEEKDAY"),
            'weekly'    => CRMText::_("COM_CRMERY_WEEKLY"),
            'weekly-mwf'=> CRMText::_("COM_CRMERY_WEEKLY_MWF"),
            'weekly-tr' => CRMText::_("COM_CRMERY_WEEKLY_TTH"),
            'monthly'   => CRMText::_("COM_CRMERY_MONTHLY"),
            'yearly'    => CRMText::_("COM_CRMERY_YEARLY")
        );
        
    }

    function getEventStatuses(){

        return array(
                '0' =>  CRMText::_('COM_CRMERY_INCOMPLETE'),
                '1' =>  CRMText::_('COM_CRMERY_COMPLETED')
            );

    }

    function getEventTypes(){

        return array(
                'all'   =>  CRMText::_('COM_CRMERY_TASKS_SLASH_EVENTS'),
                'task'  =>  CRMText::_('COM_CRMERY_TASKS'),
                'event' =>  CRMText::_('COM_CRMERY_EVENTS')
            );

    }


    function getEventDueDates(){

        return array(
                'any'           => CRMText::_('COM_CRMERY_DUE_ANY_TIME'),
                'today'         => CRMText::_('COM_CRMERY_DUE_TODAY'),
                'tomorrow'      => CRMText::_('COM_CRMERY_DUE_TOMORROW'),
                'this_week'     => CRMText::_('COM_CRMERY_DUE_THIS_WEEK'),
                'past_due'      => CRMText::_('COM_CRMERY_PAST_DUE'),
                'not_past_due'  => CRMText::_('COM_CRMERY_NOT_PAST_DUE')
            );

    }

    function getEventAssociations(){

        return array(
                'any'       => CRMText::_('COM_CRMERY_ANYTHING'),
                'person'    => CRMText::_('COM_CRMERY_PEOPLE'),
                'deal'     => CRMText::_('COM_CRMERY_DEALS'),
                'company'   => CRMText::_('COM_CRMERY_COMPANIES')
            );

    }

        
 }