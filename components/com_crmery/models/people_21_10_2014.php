<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryModelPeople extends JModel
{

        var $_total = null;    
        var $_view = null;
        var $_layout = null;
        var $_id = null;
        var $person = null;
        var $recent = false;
        var $published = 1;
        var $company_id = null;
        var $event_id = null;
        var $type = null;
        var $limit = 1;
        var $limitStart = null;
        var $order = null;
        
        /**
         * Constructor
         */
        function __construct(){
            parent::__construct();
            $this->_view = JRequest::getCmd('view');
            $this->_layout = str_replace('_filter','',JRequest::getCmd('layout'));
            $this->_id = JRequest::getVar('id');
        }


        /**
         * Method to store a record
         *
         * @return    boolean    True on success
         */
        function store($data=null)
        {
            //Load Tables
            $row =& JTable::getInstance('people','Table');
            $oldRow =& JTable::getInstance('people','Table');
            if ( $data == null ){
              $data = JRequest::get( 'post' );
            }

            if ( array_key_exists('first_name',$data) ){
                $data['first_name'] = trim($data['first_name']);
            }

            if ( array_key_exists('last_name',$data) ){
                $data['last_name'] = trim($data['last_name']);
            }

            if ( array_key_exists('edit_screen',$data) ){
                if ( array_key_exists('type',$data) ){
                    $data['type'] = "lead";
                }else{  
                    $data['type'] = "contact";
                }
            }
            
            //date generation
            $date = CrmeryHelperDate::formatDBDate(date('Y-m-d H:i:s'));
            
			
            if ( !array_key_exists('id',$data) ){
                $data['created'] = $date;
                $data['owner_id'] = array_key_exists('owner_id',$data) && $data['owner_id'] > 0 ? $data['owner_id'] : CrmeryHelperUsers::getUserId();
                $data['assignee_id'] = array_key_exists('assignee_id',$data) ? $data['assignee_id'] : CrmeryHelperUsers::getUserId();
                $status = "created";
				
            } else {
                $row->load($data['id']);
                $oldRow->load($data['id']);
                $status = "updated";
				
				//$data['assignee_id'] = array_key_exists('assignee_id',$data) ? $data['assignee_id'] : CrmeryHelperUsers::getUserId();
				 
            }
			
			
            $data['modified'] = $date;

            //generate custom field string
            $customArray = array();
            foreach( $data as $name => $value ){
                if( strstr($name,'custom_') && !strstr($name,'_input') && !strstr($name,"_hidden") ){
                    $id = str_replace('custom_','',$name);
                    $customArray[] = array('custom_field_id'=>$id,'custom_field_value'=>$value);
                    unset($data[$name]);
                }
            }

            if((array_key_exists('company_name',$data) && $data['company_name']!="")  || (array_key_exists('company',$data) && $data['company'] != "")) {
                
                $company_name = array_key_exists('company_name',$data) ? $data['company_name'] : $data['company'];

                $companyModel = JModel::getInstance('Company','CRMeryModel');
                $existingCompany = $companyModel->checkCompanyName($company_name);

                if($existingCompany=="") {
                    $cdata = array();
                    $cdata['name'] = $company_name;
                    $data['company_id'] = $companyModel->store($cdata)->id;
                } else {
                    $data['company_id'] = $existingCompany;
                }
            }

            if ( array_key_exists('company_id',$data) && is_array($data['company_id']) ){
                $company_name = $data['company_id']['value'];
                $companyModel = JModel::getInstance('Company','CRMeryModel');
                $existingCompany = $companyModel->checkCompanyName($company_name);
                if($existingCompany=="") {
                    $cdata = array();
                    $cdata['name'] = $company_name;
                    $data['company_id'] = $companyModel->store($cdata)->id;
                }else{
                    $data['company_id'] = $existingCompany;
                }
            }

            /** retrieving joomla user id **/
            if ( array_key_exists('email',$data) ){
                $data['uid'] = self::associateJoomlaUser($data['email']);
            }

            // Bind the form fields to the table
            if (!$row->bind($data)) {
                $this->setError($this->_db->getErrorMsg());
                return false;
            }

            $row->custom_fields = $row->id > 0 ? CrmeryHelperCustom::getCustomCSVData($row->id,"people") : array();
            $dispatcher =& JDispatcher::getInstance();
            $dispatcher->trigger('onBeforePersonSave', array(&$row));
         
            // Make sure the record is valid
            if (!$row->check()) {
                $this->setError($this->_db->getErrorMsg());
                return false;
            }
         
            // Store the web link table to the database
            if (!$row->store()) {
                $this->setError($this->_db->getErrorMsg());
                return false;
            }


            $person_id = array_key_exists('id',$data) ? $data['id'] : $this->_db->insertId();

			
            /** Updating the joomla user **/
            if ( array_key_exists('uid',$data) && $data['uid'] != "" ){
                self::updateJoomlaUser($data);
            }
            
            CrmeryHelperActivity::saveActivity($oldRow, $row,'person', $status);

            //if we receive no custom post data do not modify the custom fields
            if ( count($customArray) > 0 ){
                CrmeryHelperCrmery::storeCustomCf($person_id,$customArray,'people');
            }

            //bind to cf tables for deal & person association
            if ( array_key_exists('deal_id',$data) && $data['deal_id'] != 0 ) {
                $data['id'] = $row->id;
                $deal = array(
                            'association_id = '.$data['deal_id'],
                            'association_type="deal"',
                            'person_id = '.$row->id, 
                            "created = '$date'"
                        );
                if(!$this->dealsPeople($deal,$data)) {
                    
                    return false;
                }
            }
            
            //Pass Status to plugin & form ID if available
            $row->status    = $status;
            if ( isset($data) && is_array($data) && array_key_exists('form_id',$data) ){
                $row->form_id   =  $data['form_id'];
            }else{
                $row->form_id   = '';
            }

            $row->custom_fields = $row->id > 0  ? CrmeryHelperCustom::getCustomCSVData($row->id,"people") : array();
            $dispatcher =& JDispatcher::getInstance();
            $dispatcher->trigger('onAfterPersonSave', array(&$row));

            //assign any default template systems ( events )
            if ( $status == "created" ){
                include_once(JPATH_SITE."/components/com_crmery/models/template.php");
                $templateModel = new CrmeryModelTemplate();
                $templateModel->processDefaults($person_id,"person");
             		 
		     	$db = JFactory::getDbo();
				// code to add referral in history
				$deal_id_history = $_POST['deal_id'];
				$insert_history = "INSERT INTO #__crmery_history set 
							`type`='person',
							`type_id`='$deal_id_history',
							`user_id`='$user_id',
							`date`='$date',
							`action_type`='created',
							`field`='id'";
				$db->setQuery($insert_history);  
				$db->query();
				
			 	$message = $data['message'];
				
		 
				// start code to send email if new referral is created
			 	$ref_temp = substr(JURI::base(),0,-1);
			 	$referralurl = $ref_temp.JRoute::_('index.php?option=com_crmery&view=people&layout=person&id='.$person_id);
			  	//exit;
		        //$referralurl = $referralurl = JURI::base().JRoute::_('index.php?option=com_crmery&view=people&layout=person&id='.$person_id);
		
				//$referralurl =  JURI::base().'index.php?option=com_crmery&view=people&layout=person&id='.$person_id;
				//$referralurl =  JURI::base().JRoute::_('index.php?option=com_crmery&view=people&layout=person&id='.$person['id']);
				
			  	$config =& JFactory::getConfig();
				$sitename = $config->getValue( 'config.sitename' );
				
				$mainframe = JFactory::getApplication();
				//$fromEmail = $mainframe->getCfg('mailfrom');
				
				$fromEmail = 'ab@ab.com';
				
				$mail =& JFactory::getMailer();		
				
				$sender = array($fromEmail,$sitename);
				$mail->setSender($sender);
				$assignedto = $data['first_name'].' '.$data['last_name']; 
				
				 
				// email to current user (to referral owner)
				
				$Referralcode = 'RC00'.$person_id;
				$ClientName = $this->get_client_name_by_id_for_email($data['deal_id']);
				
				$recipient = $user->email;
				//$recipient = "sandeep.kumar@sdnaglobal.com";
				$ref_firm = $this->get_firm_name_by_id_for_email($data['referring_firm_id']);
				
				$rec_firm = $this->get_firm_name_by_id_for_email($data['company_id']);
				$rec_email = $data['email'];
				
				
				$user = JFactory::getUser();
				
				$mySubject = 'You have created a new Referral on '.$sitename;
				$recipient = $user->email;
				//$recipient = "sandeep.kumar@sdnaglobal.com";
				
				$mail->setSubject($mySubject);
				$mail->addRecipient($recipient);
				$myMessage = "Dear User". "<br />";
				$myMessage .= "Thanks for creating a new referral on $sitename."."<br />";
				$myMessage .= "If $assignedto accepted/declined the referral, you will be notified by an email."."<br />";
				
				$myMessage .= '<br />Referral Details::';
				$myMessage .= '<br />Referral Code  :'.$Referralcode;
				$myMessage .= '<br />Client Name :'.$ClientName;
				$myMessage .= '<br />Referring Email :'.$recipient;
				$myMessage .= '<br />Referring Firm:'.$ref_firm;
				//$myMessage .= '<br />Notes: ';
				$myMessage .= '<br />Receiving Email:'.$rec_email;
				$myMessage .= '<br />Receiving Firm:'.$rec_firm;

				$myMessage .= "<br /><br><a href='$referralurl' >Click and View Details of Referral</a> ";
				$myMessage .= "<br />";
				$myMessage .= "<br />";
				$myMessage .= "Regards,"."<br />";
				$myMessage .= "$sitename "."<br />";
			 
				// echo '<br /><br />-->'.$myMessage;
				$mail->setBody($myMessage);
				$mail->IsHTML(true);
				$send =& $mail->Send(); 
				
			/* 	$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= 'From: www'.$sitename.'<'.$fromEmail.'>' . "\r\n" .
							'X-Mailer: PHP/' . phpversion();   //////// */
						
				//if(mail($recipient, $mySubject,$myMessage, $headers))
					//die('yes --');
					
				
			
				// send email to assignee user
				
			    $mySubject1 = 'You have received a new Referral on '.$sitename;
				$recipient1 = $data['email'];
				//$recipient1 = "sandeep.kumar@sdnaglobal.com";
				$mail1 =& JFactory::getMailer();		
				
				$mail1->setSubject($mySubject1);
				$mail1->addRecipient($recipient1);
				$myMessage1 = "Dear User". "<br />";
				$myMessage1 .= "You have received a new referral on $sitename from $user->name."."<br />";
				$myMessage1 .= "Please review and accept/decline the referral."."<br />";
				$myMessage1 .= "Once you accept/decline the referral $user->name will be notified by email."."<br />";
				
				$myMessage1 .= "<br/>".$message."<br />";

				$myMessage1 .= '<br />Referral Details::';
				$myMessage1 .= '<br />Referral Code  :'.$Referralcode;
				$myMessage1 .= '<br />Client Name :'.$ClientName;
				$myMessage1 .= '<br />Referring Email :'.$recipient;
				$myMessage1 .= '<br />Referring Firm:'.$ref_firm;
				//$myMessage .= '<br />Notes: ';
				$myMessage1 .= '<br />Receiving Email:'.$rec_email;
				$myMessage1 .= '<br />Receiving Firm:'.$rec_firm;
				
			    $myMessage1 .= "<br /><br /><a href='$referralurl' >Click and View Details of Referral</a> ";
				$myMessage1 .= "<br />";
				$myMessage1 .= "<br />";
				$myMessage1 .= "Regards,"."<br />";
				$myMessage1 .= "$sitename "."<br />";
			 
				// echo '<br /><br />-->'.$myMessage1;;
				$mail1->setBody($myMessage1);
				$mail1->IsHTML(true);
				$send =& $mail1->Send(); 
				
				// send email to referral user
		 	
				$mySubject2 = 'New Referral is created on '.$sitename;
				
				$db = JFactory::getDbo();
				
				$pquery = $db->getQuery(true);
				$pquery = "SELECT * FROM #__crmery_users WHERE role_type = ''";
			          
			
				$db->setQuery($pquery);
				$presults = $db->loadAssocList();
				$presult = $presults[0];
				$user_id_admin = $presult->uid;
				$superuser = JFactory::getUser($user_id_admin);
				
				$recipient2 = $superuser->email;
				//$recipient2 = "sandeep.kumar@sdnaglobal.com";
				
				$mail2 =& JFactory::getMailer();		
				
				$mail2->setSubject($mySubject2);
				$mail2->addRecipient($recipient2);
				$myMessage2 = "Dear admin". "<br />";
				$myMessage2 .= "A new referral is created on $sitename from $user->name and assigned to $assignedto."."<br />";
				$myMessage2 .= "Once $assignedto accepted/declined the referral $user->name will be notified by email."."<br />";

				$myMessage2 .= '<br />Referral Details::';
				$myMessage2 .= '<br />Referral Code  :'.$Referralcode;
				$myMessage2 .= '<br />Client Name :'.$ClientName;
				$myMessage2 .= '<br />Referring Email :'.$recipient;
				$myMessage2 .= '<br />Referring Firm:'.$ref_firm;
				//$myMessage .= '<br />Notes: ';
				$myMessage2 .= '<br />Receiving Email:'.$rec_email;
				$myMessage2 .= '<br />Receiving Firm:'.$rec_firm;

				
				$myMessage2 .= "<br /><br /><a href='$referralurl' >Click and View Details of Referral</a> ";
				$myMessage2 .= "<br />";
				$myMessage2 .= "<br />";
				$myMessage2 .= "Regards,"."<br />";
				$myMessage2 .= "$sitename "."<br />";

				$mail2->setBody($myMessage2);
				$mail2->IsHTML(true);
				$send =& $mail2->Send(); 
				// echo '<br /><br />'. $myMessage2;
			  // die('<br>--ethe--');
				// end code to send email if new referral is created
				 
			 // comment end to stop email sending temporary	
				
			}
			

            return $person_id;
        }
		
		function get_firm_name_by_id_for_email($id)
		{
			$db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            $query->select("name")->from("#__crmery_companies")->where("id=".$id);
            $db->setQuery($query);
            return $db->loadResult();	
			 
		}
		
		function get_client_name_by_id_for_email($id)
		{
			$db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            $query->select("name")->from("#__crmery_deals")->where("id=".$id);
            $db->setQuery($query);
            return $db->loadResult();	
			 
		}
		

        function updateJoomlaUser($data){

            $name = $data['first_name'].' '.$data['last_name'];

            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            $query->update("#__users")->set(array("email=".$db->Quote($data['email']),"name=".$db->Quote($name)))->where("id=".$data['uid']);
            $db->setQuery($query);
            $db->query();
        }

        function associateJoomlaUser($email){
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            $query->select("id")->from("#__users")->where("email=".$db->Quote($email));
            $db->setQuery($query);
            return $db->loadResult();
        }
        
        /*
         * Method to link deals and people in cf tables
         */
        
        function dealsPeople($cfdata,$data){
            
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);

            $query->select("count(*)")
                ->from("#__crmery_people_cf")
                ->where("association_id=".$data['deal_id'])
                ->where("association_type='deal'")
                ->where("person_id=".$data['id']);

            $db->setQuery($query);
            $result = $db->loadResult();

            if ( $result > 0 ){

                return true;

            } else { 

                $query->clear();
                $query->insert('#__crmery_people_cf');
                $query->set($cfdata);
                $db->setQuery($query);
                
                if($db->query()){
                    return true;
                }else{
                    return false;
                }

            }
            
        }
        
        
        /**
         * Build our query
         */
        function _buildQuery(){

            /** Large SQL Selections **/
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            $db->setQuery("SET SQL_BIG_SELECTS=1");
            $db->query();

            $view = JRequest::getVar('view');
            $layout = JRequest::getVar('layout');
            //retrieve person id
            if ( !$this->_id ){
                
                //get filters
                $type   = JRequest::getVar('type') ? JRequest::getVar('type') : $this->type;
                $user   = JRequest::getVar('user');
                $stage  = JRequest::getVar('stage');
                $tag    = JRequest::getVar('tag');
                $status = JRequest::getVar('status');
                $team   = JRequest::getVar('team_id');
                $val   = JRequest::getVar('val');
                
                //get session data
                $session    = JFactory::getSession();
                
                //determine whether we are filtering with a team or user
                if ( $team ){
                    $session->set('people_user_filter',null);
                }
                if ( $user ){
                    $session->set('people_team_filter',null);
                }
                
                //set user session data
                if ( $type != null ) {
                    $session->set('people_type_filter',$type);
                } else {
                    $sess_type  = $session->get('people_type_filter');
                    $type = $sess_type;
                }
                if ( $user != null ) {
                    $session->set('people_user_filter',$user);
                } else {
                    $sess_user  = $session->get('people_user_filter');
                    $user = $sess_user;
                }
                if ( $stage != null ) {
                    $session->set('people_stage_filter',$stage);
                } else {
                    $sess_stage = $session->get('people_stage_filter');
                    $stage = $sess_stage;
                }
                if ( $tag != null ) {
                    $session->set('people_tag_filter',$tag);
                } else {
                    $sess_tag   = $session->get('people_tag_filter');
                    $tag = $sess_tag;
                }
                if ( $status != null ){
                    $session->set('people_status_filter',$status);
                }else{
                    $sess_status= $session->get('people_status_filter');
                    $status = $sess_status;
                }
                if ( $team != null ){
                    $session->set('people_team_filter',$team);
                }else{
                    $sess_team  = $session->get('people_team_filter');
                    $team = $sess_team;
                }
            }

            $export = JRequest::getVar('export');

            if ( $export ){

                $query->select('p.id,p.id as ref_amount_id,p.first_name,p.last_name,p.position,p.phone,p.email,p.home_address_1,p.home_address_2,p.owner_id,p.referring_firm_id,p.Accepted,'.
                            'p.home_city,p.home_state,p.home_zip,p.home_country,p.fax,p.website,p.facebook_url,p.twitter_user,'.
                            'p.linkedin_url,p.created,p.type,p.info,p.modified,p.work_address_1,p.work_address_2,'.
                            'p.work_city,p.work_state,p.work_zip,p.work_country,p.assignment_note,p.mobile_phone,p.home_email,'.
                            'p.other_email,p.home_phone,c.name as company_name, CONCAT(u2.first_name," ",u2.last_name) AS assignee_name, u2.country_id as assignee_country_id,'.
                            'u.first_name AS owner_first_name,'.
                            'u.last_name AS owner_last_name, u.country_id as owner_country_id, stat.name as status_name,'.
                            'source.name as source_name,'.
                            "event.id AS event_id,event.type AS event_type,event.name AS event_name,IF(event.type='task',event.due_date,event.start_time) AS event_due_date");
                $query->from('#__crmery_people AS p');
                $query->leftJoin('#__crmery_companies AS c ON c.id = p.company_id AND c.published=1');
                $query->leftJoin('#__crmery_people_status AS stat ON stat.id = p.status_id');
                $query->leftJoin('#__crmery_sources AS source ON source.id = p.source_id');
                $query->leftJoin("#__crmery_shared AS shared ON shared.item_id=p.id AND shared.item_type='person'");
                $query->leftJoin("#__crmery_users AS u ON u.id = p.owner_id");
                $query->leftJoin("#__crmery_users AS u2 ON u2.id = p.assignee_id");
				


            }else{
					
                $query->select('p.*,c.name as company_name, CONCAT(u2.first_name," ",u2.last_name) AS assignee_name,u.first_name AS owner_first_name, 
                            u.last_name AS owner_last_name, stat.name as status_name,stat.color as status_color,
                            source.name as source_name,event.id as event_id,
                            event.name as event_name, event.type as event_type, 
                            IF(event.type="task",event.due_date,event.start_time) AS event_due_date,event.description as event_description');
                $query->from('#__crmery_people AS p ');
                $query->leftJoin('#__crmery_companies AS c ON c.id = p.company_id AND c.published=1');
				$query->leftJoin('#__crmery_companies AS cr ON cr.id = p.referring_firm_id AND cr.published=1');

				$query->leftJoin('#__crmery_people_status AS stat ON stat.id = p.status_id');
                $query->leftJoin('#__crmery_sources AS source ON source.id = p.source_id');
                $query->leftJoin("#__crmery_shared AS shared ON shared.item_id=p.id AND shared.item_type='person'");
                $query->leftJoin("#__crmery_users AS u ON u.id = p.owner_id");
                $query->leftJoin("#__crmery_users AS u2 ON u2.id = p.assignee_id");

            }

            
            // group ids
            $query->group("p.id");
            
            /** ---------------------------------------------------------------
             * Filter data using member role permissions
             */
            $member_id = CrmeryHelperUsers::getUserId();
            $member_role = CrmeryHelperUsers::getRole();
            $team_id = CrmeryHelperUsers::getTeamId();
			
            $this->company_id = JRequest::getVar('company_id') ? JRequest::getVar('company_id') : $this->company_id;
			
			if($member_role !="")
			 {
			
            if ( (isset($this->company_id) && $this->company_id > 0 ) || ( isset($user) && $user == "all" ) || ( isset($owner_filter) && $owner_filter == "all" ) ){
					   
			
                if ( $member_role != 'exec'){
                     //manager filter
                    if ( $member_role == 'manager' ){
                        $query->where("(u.team_id=$team_id OR u2.team_id=$team_id OR shared.user_id=$member_id)");
                
				}else{
                    //basic user filter
                        $query->where("(p.owner_id=$member_id OR p.assignee_id=$member_id OR shared.user_id=$member_id)");   
                    }
                }
			
				
				
            }else if ( isset($team) && $team ){
                $query->where("(u.team_id=$team OR u2.team_id=$team)");
            }else if ( isset($user) && $user != "all" ){  	
					$db =& JFactory::getDbo();
								  $querys = $db->getQuery(true);
								   $querys->select("r.region_id,r.country_id")
									->from("#__crmery_users AS r");
								$querys->where("r.id='".$member_id."'");						

								$db->setQuery($querys);
								$regIds = $db->loadObjectList();
								
								if ( count($regIds) > 0 ){
									foreach ( $regIds as $regId ){						
										$regionId=$regId->region_id;						
									  }
								 }	
								 
								//$query->where("c.region_id IN(".$regionId.")");  
				 
								$region_query = $db->getQuery(true);
								  $region_query->select("r.id")
									->from("#__crmery_companies AS r");
								$region_query->where("r.region_id IN(".$regionId.")");						

								$db->setQuery($region_query);
								$firm_Ids = $db->loadObjectList();
								
								if ( count($firm_Ids) > 0 ){
									$firmId = array();
									foreach ( $firm_Ids as $firm_Id ){						
										$firmId[]=$firm_Id->id;						
									  }
									  $firm_Ids = implode(',',$firmId);
									 // echo '<pre>';print_r($firmId);
									$query->where("p.company_id IN(".$firm_Ids.")");	 
								 }else{
								      $query->where("(p.owner_id=$user OR p.assignee_id=$user)");
								 }
            }else{
                if ( !(isset($owner_filter)) ){
                    if ( $this->_id ){
                        if ( $member_role == "basic" ){
                            $query->where("(p.owner_id=$member_id OR p.assignee_id=$member_id OR shared.user_id=$member_id)");    
                        }
                        if ( $member_role == "manager" ){
                            $team_members = CrmeryHelperUsers::getTeamUsers($team_id,TRUE);
                            $team_members = array_merge($team_members,array(0=>$member_id));
							
							 if($member_role == 'manager') 
							 {$field="company_id"; } 
							 if($member_role == 'basic')
							 {
							   $field="inp_company_id";
							 }
						  						
						
						 $db =& JFactory::getDbo();
                          $querysu = $db->getQuery(true);
						   $querysu->select("u.$field")
						   ->from("#__crmery_users AS u");
						   $querysu->where("u.id=".CrmeryHelperUsers::getLoggedInUser()->id);
						  
						   $db->setQuery($querysu);
							$comId = $db->loadObjectList();
							$firm_Ids = array();
							if ( count($comId) > 0 ){
							foreach ( $comId as $comid ){						
								$firm_Ids[] =$comid->$field;						
							  }
							}
							
							
                            $query->where("(p.owner_id IN(".implode(',',$team_members).") OR p.assignee_id IN(".implode(',',$team_members).") OR shared.user_id=".$member_id." OR p.company_id IN(".implode(',',$firm_Ids).")  OR p.referring_firm_id IN(".implode(',',$firm_Ids)."))");
							// $query->where("(p.owner_id IN(".implode(',',$team_members).") OR p.assignee_id IN(".implode(',',$team_members).") OR shared.user_id=".$member_id.")"); 
                        }
                    }else{
					
					      // Customization start here
						  
						 if ( $member_role == 'manager' || $member_role == 'basic' ) // CUSTOMIZING HERE  
						  {
						     if($member_role == 'manager') 
							 {$field="company_id"; } 
							 if($member_role == 'basic')
							 {
							   $field="inp_company_id";
							 }
						  						
						
						 $db =& JFactory::getDbo();
                          $querysu = $db->getQuery(true);
						   $querysu->select("u.$field")
						   ->from("#__crmery_users AS u");
						   $querysu->where("u.id=".CrmeryHelperUsers::getLoggedInUser()->id);
						  
						   $db->setQuery($querysu);
							$comId = $db->loadObjectList();
							if ( count($comId) > 0 ){
							foreach ( $comId as $comid ){						
								$cmId=$comid->$field;						
							  }
							}

							$db =& JFactory::getDbo();
                          $querys = $db->getQuery(true);
						  
						  $querys->select("c.firmadmin_id")
							->from("#__crmery_people AS c");
						  $querys->where("c.company_id=".$cmId);
                          
                          $db->setQuery($querys);
						  $auids = $db->loadObjectList();
						
							$uId = array();
							$ownId = array();
							
							if ( count($auids) > 0 ){ 
							foreach ( $auids as $auid ){						
								$ownId=$auid->firmadmin_id;
												
							   }
							}
							 //$ownId=6;
							 
						$query->where("(p.company_id=$cmId or p.referring_firm_id=$cmId or p.owner_id=$member_id)");
							
							//$query->where("(p.owner_id=$ownId and p.company_id=$cmId OR p.assignee_id=$ownId OR shared.user_id=$ownId)");
							//echo '<br><br>-->>'.$query;	
						  }else { 
							
							// IS CODE start
							if ( $member_role == 'exec' ){    // CUSTOMIZING HERE  						
								
								$db =& JFactory::getDbo();
								  $querys = $db->getQuery(true);
								   $querys->select("r.region_id,r.country_id")
									->from("#__crmery_users AS r");
								$querys->where("r.id='".$member_id."'");						

								$db->setQuery($querys);
								$regIds = $db->loadObjectList();
								
								if ( count($regIds) > 0 ){
									foreach ( $regIds as $regId ){						
										$regionId=$regId->region_id;						
									  }
								 }

								$dbuser =& JFactory::getDbo();
								  $query_us = $dbuser->getQuery(true);
								   $query_us->select("r.id")
									->from("#__crmery_users AS r");
								$query_us->where("r.region_id IN('".$regionId."')");						

								$dbuser->setQuery($query_us);
								$usrIds = $dbuser->loadObjectList();
								
								if ( count($usrIds) > 0 ){
									$usr_Ids = array();
									foreach ( $usrIds as $usrId ){						
										$usr_Ids[]=$usrId->id;						
									  }
									 $usr_Ids[] = $member_id;
									 $usr_Id = implode(',',$usr_Ids); 
								 }	

								 
								 
								//$query->where("c.region_id IN(".$regionId.")");  
				 
								$region_query = $db->getQuery(true);
								  $region_query->select("r.id")
									->from("#__crmery_companies AS r");
								$region_query->where("r.region_id IN(".$regionId.")");						

								$db->setQuery($region_query);
								$firm_Ids = $db->loadObjectList();
								
								if ( count($firm_Ids) > 0 ){
									$firmId = array();
									foreach ( $firm_Ids as $firm_Id ){						
										$firmId[]=$firm_Id->id;						
									  }
									  $firm_Ids = implode(',',$firmId);
									 // echo '<pre>';print_r($firmId);
									$query->where("(p.company_id IN(".$firm_Ids.") OR p.owner_id IN($usr_Id) OR p.assignee_id IN($usr_Id) OR shared.user_id IN($usr_Id)) ");	 
								 }
								else								 
									$query->where("(OR p.owner_id IN($usr_Id) OR p.assignee_id IN($usr_Id) OR shared.user_id IN($usr_Id))");  
							
 //echo '<br><br>'.$query;
 							 
							} 
							
                       }
                    }
                }
             }
            }
            
            
            //searching for specific person
            if ( $this->_id ){
                if ( is_array($this->_id) ){
                        $query->where("p.id IN (".implode(',',$this->_id).")");
                }else{
                        $query->where("p.id=$this->_id");
                }
                 //get next event
                $query->leftJoin("#__crmery_events AS event ON event.id = (   
                                    SELECT event2.id 
                                    FROM #__crmery_events_cf AS ecf2
                                    LEFT JOIN #__crmery_events AS event2 ON 
                                        event2.id=ecf2.event_id AND 
                                        ecf2.association_type='person'
                                    WHERE ecf2.association_id=p.id AND
                                          ecf2.association_type='person' AND
                                          event2.completed=0
                                    ORDER BY CASE WHEN event2.type='task' THEN due_date END ASC,
                                             CASE WHEN event2.type='event' THEN start_Time END ASC
                                    LIMIT 1 )");
            }

            if ( !$this->_id ){

                if ( !$export ){

                    //filter data
                    if ( $type AND $type != 'all' ){
                        switch($type){
                            case 'leads':
                                $query->where("p.type='lead'");
                            break;
                            case 'not_leads':
                                $query->where("p.type='contact'");
                            break;
                            case "shared";
                                $query->where("( shared.user_id=".$member_id." OR ( p.owner_id=".$member_id." AND shared.user_id IS NOT NULL ) )");
                            break;
                        }
                    }
					
					// new filter starts here 
					
					  if ( $val AND $val != 'unset' ){
                        switch($val){
                            case 'assigned':
                                $query->where("p.assignee_id=".$member_id."");
                            break;
                            case 'firm':
                                $query->where("p.company_id=".$cmId."");
                            break;
                            case 'owner':
                                $query->where("p.owner_id=".$member_id."");
                            break;
                            case 'firmowner':
                                $query->where("p.referring_firm_id=".$cmId."");
                            break;
                            case "3":
                                 $query->where("p.Accepted=0");
                            break;
						    case "1":
                                 $query->where("p.Accepted=1");
                            break;
						    case "2":
                                 $query->where("p.Accepted=2");
                            break;
                        }
						
                    }
					
                
                    //search with status
                    if ( $status AND $status != 'any' ){
                        $query->where('p.status_id='.$status);
                    }
                
                    //get current date
                    $date = CrmeryHelperDate::formatDBDate(date('Y-m-d 00:00:00'));
                
                    //filter for type
                    if ( $stage != null  && $stage != 'all' ){
                        
                        //filter for deals//tasks due today
                        if ( $stage == 'today' ){
                            $today = CrmeryHelperDate::formatDBDate(date('Y-m-d'),false)." 00:00:00";
                            $tomorrow = CrmeryHelperDate::formatDBDate(date('Y-m-d',strtotime($today." +1 days")),false)." 00:00:00";
                            $query->leftJoin("#__crmery_events_cf AS ecf ON ecf.association_type='person' AND ecf.association_id=p.id");
                            $query->leftJoin("#__crmery_events AS event ON event.id=ecf.event_id");
                            $query->where("( event.due_date > '$today' AND event.due_date < '$tomorrow' )");
                            $query->where("event.published>0 AND event.completed=0");
                        }
                        
                        //filter for companies and deals//tasks due tomorrow
                        else if ( $stage == "tomorrow" ){
                            $tomorrow = CrmeryHelperDate::formatDBDate(date('Y-m-d 00:00:00',time() + (1*24*60*60)));
                            $dayAfterTomorrow = CrmeryHelperDate::formatDBDate(date("Y-m-d 00:00:00",strtotime($tomorrow." +1 days")));
                            $query->leftJoin("#__crmery_events_cf AS ecf ON ecf.association_type='person' AND ecf.association_id=p.id");
                            $query->leftJoin("#__crmery_events AS event ON event.id=ecf.event_id");
                            $query->where("event.due_date >'".$tomorrow."' AND event.due_date < '$dayAfterTomorrow'");
                            $query->where("event.published>0 AND event.completed=0");
                        } else {

                            //get next event
                            $query->leftJoin("#__crmery_events AS event ON event.id = (   
                                                SELECT event2.id 
                                                FROM #__crmery_events_cf AS ecf2
                                                LEFT JOIN #__crmery_events AS event2 ON 
                                                    event2.id=ecf2.event_id AND 
                                                    ecf2.association_type='person'
                                                WHERE ecf2.association_id=p.id AND
                                                      ecf2.association_type='person' AND
                                                      event2.completed=0
                                                ORDER BY CASE WHEN event2.type='task' THEN due_date END ASC,
                                                         CASE WHEN event2.type='event' THEN start_Time END ASC
                                                LIMIT 1 )");

                        }
                        
                        //filter for people updated in the last 30 days
                        if ( $stage == "past_thirty" ){
                            $last_thirty_days = CrmeryHelperDate::formatDBDate(date('Y-m-d 00:00:00',time() - (30*24*60*60)));
                            $query->where("p.modified >'$last_thirty_days'");
                        }
                        
                        //filter for recently added people
                        if ( $stage == "recently_added" ){
                            $last_five_days = CrmeryHelperDate::formatDBDate(date('Y-m-d 00:00:00',time() - (5*24*60*60)));
                            $query->where("p.modified >'$last_five_days'");
                        }
                        
                        
                        //filter for last imported people
                        if ( $stage == "last_import" ){
                            
                        }
                        
                    } else {
                        //get latest task entry
                        
                        if($this->recent) {
                            $query->where(  "( event.due_date IS NULL OR event.due_date=(SELECT MIN(e2.due_date) FROM #__crmery_events_cf e2cf ".
                                            "LEFT JOIN #__crmery_events as e2 on e2.id = e2cf.event_id ".
                                            "WHERE e2cf.association_id=p.id AND e2.published>0 AND e2.completed=0) )");
                        }

                         //get next event
                        $query->leftJoin("#__crmery_events AS event ON event.id = (   
                                            SELECT event2.id 
                                            FROM #__crmery_events_cf AS ecf2
                                            LEFT JOIN #__crmery_events AS event2 ON 
                                                event2.id=ecf2.event_id AND 
                                                ecf2.association_type='person'
                                            WHERE ecf2.association_id=p.id AND
                                                  ecf2.association_type='person' AND
                                                  event2.completed=0
                                            ORDER BY CASE WHEN event2.type='task' THEN due_date END ASC,
                                                     CASE WHEN event2.type='event' THEN start_Time END ASC
                                            LIMIT 1 )");
                    }

                }else{
                     //get next event
                    $query->leftJoin("#__crmery_events AS event ON event.id = (   
                                        SELECT event2.id 
                                        FROM #__crmery_events_cf AS ecf2
                                        LEFT JOIN #__crmery_events AS event2 ON 
                                            event2.id=ecf2.event_id AND 
                                            ecf2.association_type='person'
                                        WHERE ecf2.association_id=p.id AND
                                              ecf2.association_type='person' AND
                                              event2.completed=0
                                        ORDER BY CASE WHEN event2.type='task' THEN due_date END ASC,
                                                 CASE WHEN event2.type='event' THEN start_Time END ASC
                                        LIMIT 1 )");
                }

                
                /** company filter **/
                if ( $this->company_id ){
                    $query->where("p.company_id=".$this->company_id);
                }

                if ( $this->event_id ){
                    $query->where("event.id=".$this->event_id);
                }

                /** person name filter **/
                $people_filter = $this->getState('People.'.$view.'_name');
                if ( $people_filter != null ){
					$people_filter = str_replace('RC00','',strtoupper($people_filter));
                    $query->where("( p.first_name LIKE '%".$db->escape($people_filter)."%' OR p.last_name LIKE '%".$db->escape($people_filter)."%' OR CONCAT(p.first_name,' ',p.last_name) LIKE '%".$db->escape($people_filter)."%' OR CONCAT(u.first_name,' ',u.last_name) LIKE '%".$db->escape($people_filter)."%'  OR cr.name LIKE '%".$db->escape($people_filter)."%'  OR p.id='".$db->escape((int)$people_filter)."')");
                }
				//echo '<br><br>-->'.$val;
				//echo '<br><br>-->'.$query;
            }
			
			//echo $query;

            $query->where("p.published=".$this->published);
			
			//echo $query."<br>";	
            //return query string
			  //  echo '<br /> <br  />'.$member_role;
				//	echo '<br /> <br  />'.$query;
            return $query;
        }
        
        /*
         * Method to access people
         * 
         * @return array     
         */
        function getPeople(){

            //Get query
            $db =& JFactory::getDBO();
            $query = $this->_buildQuery();
            //echo $query;
			//die();
			//echo $query;
			
            $view = JRequest::getVar('view');
            $layout = JRequest::getVar('layout');
			$export = JRequest::getVar('export');
 
            /** ------------------------------------------
             * Set query limits/ordering and load results
             */
			if(!$export) {
			$limit = $this->getState($this->_view.'_limit');
            $limitStart = $this->getState($this->_view.'_limitstart');
            if ( $this->order != null ){
                $query->order($this->order);
				
            }else{
				$this->getState('People.filter_order') . ' ' . $this->getState('People.filter_order_Dir');
                $query->order($this->getState('People.filter_order') . ' ' . $this->getState('People.filter_order_Dir'));
				
            }
            if (  !$this->_id && $limit != 0 ){
                if ( $limitStart >= $this->getTotal() ){
                    $limitStart = 0;
                    $limit = 10;
                    $limitStart = ($limit != 0) ? (floor($limitStart / $limit) * $limit) : 0;
                    $this->setState($this->_view.'_limit', $limit);
                    $this->setState($this->_view.'_limitstart', $limitStart);
                }
                if ( $this->limit > 0 ){
                    $query .= " LIMIT ".($limit)." OFFSET ".($limitStart);
                }
            }
			
			}
			
			$db->setQuery($query);
            $people = $db->loadAssocList();
			
			//echo "<pre>"; print_r($people); echo "</pre>";
            
            //generate query to join deals
            if ( count($people) > 0 ){

               		
                    if ( !$export ){   
						
                        if ( $this->_id ){
						//generate query to join notes
                        foreach ( $people as $key => $person ) {
                         
                            //Deals
                            $dealModel =& JModel::getInstance('Deal','CrmeryModel');
                            $dealModel->set('person_id',$person['id']);
                            $dealModel->set('user_override','all');
                            //$people[$key]['deals'] = $dealModel->getDeals();;
                           
                            // Notes
                            $notesModel = JModel::getInstance('Note','CRMeryModel');
                            $people[$key]['notes'] = $notesModel->getNotes($person['id'],'person');

                            // Docs
                            $docsModel = JModel::getInstance('Document','CRMeryModel');
                            $docsModel->set('person_id',$person['id']);
                            $people[$key]['documents'] = $docsModel->getDocuments();

                            // Tweets
                            if($person['twitter_user']!="" && $person['twitter_user']!=" ") {
                                $people[$key]['tweets'] = CrmeryHelperTweets::getTweets($person['twitter_user']);
                            }

                            //Calculate total pipeline
                            $people[$key]['total_pipeline'] = $this->calculatePipeline($person['id']);
                            
                            //Calculate won deal amount
                            $people[$key]['won_deal_amount'] = $this->calculateWonDealAmount($person['id']);

                        }

                    }

                }
            }
            $dispatcher =& JDispatcher::getInstance();
            $dispatcher->trigger('onPersonLoad', array(&$people));
            
            //return results
            return $people;
            
        }

        public function calculatePipeline($personId){
            $activeStages = CrmeryHelperDeal::getActiveStageIds();

            if ( count($activeStages) > 0 ){

                $db =& JFactory::getDbo();
                $query = $db->getQuery(true);

                //get cf amounts
                $query->select("deal.amount,deal.id");
                $query->from("#__crmery_people_cf AS pcf");
                $query->leftJoin("#__crmery_deals AS deal ON ( deal.id = pcf.association_id OR deal.primary_contact_id = pcf.person_id ) AND deal.published=1 AND deal.archived=0 AND deal.stage_id IN (".implode(',',$activeStages).")");
                $query->where("pcf.person_id=".$personId);
                $query->where("pcf.association_type='deal'");

                $db->setQuery($query);
                $cfAmounts = $db->loadAssocList('id');

                //get primary contact amounts
                $query->clear();
                $query->select("deal.amount,deal.id");
                $query->from("#__crmery_deals AS deal");
                $query->where("deal.primary_contact_id=".$personId);
                $query->where("deal.stage_id IN(".implode(',',$activeStages).")");
                $query->where("deal.published=1");
                $query->where("deal.archived=0");

                $db->setQuery($query);
                $pcAmounts = $db->loadAssocList('id');


                //merge any unique ids
                $array = $cfAmounts + $pcAmounts;
                


                //return sum
                $sum = 0;
                if ( is_array($array) && count($array) > 0 ){
                    foreach ( $array as $a ){
                        $sum += $a['amount'];
                    }
                }
                return $sum;

            } else { 
                return 0;
            }

        }

        public function calculateWonDealAmount($personId){
        
            $wonStages = CrmeryHelperDeal::getWonStages();

            if ( count($wonStages) > 0 ){

                $db =& JFactory::getDbo();
                $query = $db->getQuery(true);

                //get cf amounts
                $query->select("deal.amount,deal.id");
                $query->from("#__crmery_people_cf AS pcf");
                $query->leftJoin("#__crmery_deals AS deal ON ( deal.id = pcf.association_id OR deal.primary_contact_id = pcf.person_id ) AND deal.published=1 AND deal.archived=0 AND deal.stage_id IN (".implode(',',$wonStages).")");
                $query->where("pcf.person_id=".$personId);
                $query->where("pcf.association_type='deal'");

                $db->setQuery($query);
                $cfAmounts = $db->loadAssocList('id');

                //get primary contact amounts
                $query->clear();
                $query->select("deal.amount,deal.id");
                $query->from("#__crmery_deals AS deal");
                $query->where("deal.primary_contact_id=".$personId);
                $query->where("deal.stage_id IN(".implode(',',$wonStages).")");
                $query->where("deal.published=1");
                $query->where("deal.archived=0");

                $db->setQuery($query);
                $pcAmounts = $db->loadAssocList('id');

                //merge any unique ids
                $array = $cfAmounts + $pcAmounts;

                //return sum
                $sum = 0;
                if ( is_array($array) && count($array) > 0 ){
                    foreach ( $array as $a ){
                        $sum += $a['amount'];
                    }
                }
                return $sum;

            }else{
                return 0;
            }

        }

        /*
         * Method to retrieve person
         */
        function getPerson($id){
            $db =& JFactory::getDBO();
            //generate query
            //
            $query = $db->getQuery(true);
            $query->select('p.*,c.name as company_name,stat.name as status_name,
                            source.name as source_name, owner.name as owner_name');
            $query->from('#__crmery_people AS p');
            $query->leftJoin('#__crmery_companies AS c ON c.id = p.company_id AND c.published>0');
            $query->leftJoin('#__crmery_people_status AS stat ON stat.id = p.status_id');
            $query->leftJoin('#__crmery_sources AS source ON source.id = p.source_id');
            $query->leftJoin('#__users AS owner ON p.owner_id = owner.id');
            
            //searching for specific person
            $query->where("p.published=".$this->published);
            $query->where("p.id='".$id."'");        
            
            //run query and grab results
            $db->setQuery($query);
            $people = $db->loadAssocList();

            $this->_total = count($people);

            //generate query to join deals
            if ( count($people) ){
                foreach ( $people as $key => $person ) {
                    /* Deals */
                    $dealModel =& JModel::getInstance('Deal','CrmeryModel');
                    $dealModel->set('person_id',$person['id']);
                    $people[$key]['deals'] = $dealModel->getDeals();;
                   
                    /* Notes */
                    $notesModel = JModel::getInstance('Note','CRMeryModel');
                    $people[$key]['notes'] = $notesModel->getNotes($person['id'],'person');

                    /* Docs */
                    $docsModel = JModel::getInstance('Document','CRMeryModel');
                    $docsModel->set('person_id',$person['id']);
                    $people[$key]['documents'] = $docsModel->getDocuments();

                    /* Tweets */
                    if($person['twitter_user']!="" && $person['twitter_user']!=" ") {
                        $people[$key]['tweets'] = $this->getTweets($person['twitter_user']);
                    }

                }
            }
            
            //return results
            if($id) {
                $this->person = $people[0];
            }

            $dispatcher =& JDispatcher::getInstance();
            $dispatcher->trigger('onPersonLoad', array(&$people));
            
            return $people;
        }

        /*
         * Method to retrieve list of names and ids
         */
        function getPeopleList(){
            
             //db object
            $db =& JFactory::getDBO();
            //gen query
            $query = $db->getQuery(true);
            $query->select("DISTINCT(p.id),p.first_name,p.last_name");
            $query->from("#__crmery_people AS p");
           // $query->leftJoin("#__crmery_people_cf AS dcf ON dcf.person_id = p.id AND dcf.association_type='deal'");
            $query->leftJoin("#__crmery_shared AS shared ON shared.item_id=p.id AND shared.item_type='person'");
            $query->leftJoin("#__crmery_users AS u ON u.id = p.owner_id");
            $query->leftJoin("#__crmery_users AS u2 ON u2.id = p.assignee_id");

            //filter based on member access roles
            $user_id = CrmeryHelperUsers::getUserId();
            $member_role = CrmeryHelperUsers::getRole();
            $team_id = CrmeryHelperUsers::getTeamId();				
		
		  if ( $member_role != '' ){
            if ( $member_role != 'exec' ){
                
				
				$user_firms = CrmeryHelperUsers::getFirmsIds();
					
					$query->where("p.referring_firm_id=$user_firms or p.company_id=$user_firms");
                // if ( $member_role == 'manager' ){
                    // $query->where("(u.team_id=$user_id OR u2.team_id=$user_id OR shared.user_id=$user_id)");
                // }else{
                    // $query->where("(p.owner_id=$user_id OR p.assignee_id=$user_id OR shared.user_id=$user_id)");
                // }                
              }				  
			}
            $query->where("p.published=".$this->published);

            $associationType = JRequest::getVar('association');
            $associationId = JRequest::getVar('association_id');
            
            if ( $associationType == "company" ){
                $query->where("p.company_id=".$associationId);
            }
            if ( $associationType == "deal" ){
                $query->where("dcf.association_id=".$associationId." AND dcf.association_type='deal'");
            }

            //set query
            $db->setQuery($query);

            //load list
            $row = $db->loadAssocList();
            $blank = array(array('first_name'=>CRMText::_('COM_CRMERY_NONE'),'last_name'=>'','id'=>0));
            $return = array_merge($blank,$row);
            
            //return results
            return $return;
            
        }
        
        /**
         * Get total number of rows for pagination
         */
        function getTotal() {
          if ( empty ( $this->_total ) ){
              $query = $this->_buildQuery();
              $this->_total = $this->_getListCount($query);
          }
          return $this->_total;
       }

        /**
         * Generate pagination
         */
        function getPagination() {
          // Lets load the content if it doesn't already exist

          if (empty($this->_pagination)) {
             jimport('joomla.html.pagination');
             $total = $this->getTotal();
             $total = $total ? $total : 0;
             $this->_pagination = new CrmeryPagination( $total, $this->getState($this->_view.'_limitstart'), $this->getState($this->_view.'_limit'),null,JRoute::_('index.php?option=com_crmery&view=people'));
          }
          return $this->_pagination;
        }
        
        /**
         * Populate user state requests
         */
        function populateState(){
            //get states
            $app = JFactory::getApplication();
            $view = JRequest::getCmd('view');
            
            //TODO add these limits to the switch statement to support multiple pages and layouts
            // Get pagination request variables
            $limit = $app->getUserStateFromRequest($view.'_limit','limit',10);
            $limitstart = $app->getUserStateFromRequest($view.'_limitstart','limitstart',0);
            
            // In case limit has been changed, adjust it
            $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
     
            $this->setState($view.'_limit', $limit);
            $this->setState($view.'_limitstart', $limitstart);
            
            //set default filter states for reports
            $filter_order           = $app->getUserStateFromRequest('People.filter_order','filter_order','p.created');
            $filter_order_Dir       = $app->getUserStateFromRequest('People.filter_order_Dir','filter_order_Dir','desc');
            $people_filter          = $app->getUserStateFromRequest('People.'.$view.'_name','name',null);
            
            //set states for reports
            $this->setState('People.filter_order',$filter_order);
            $this->setState('People.filter_order_Dir',$filter_order_Dir);
            $this->setState('People.filter_order_Dir',$filter_order_Dir);
            $this->setState('People.'.$view.'_name',$people_filter);

        }

        function getDropdowns() 
        {
            $dropdowns['person_type'] = CRMeryHelperDropDown::generateDropdown('person_type',$this->person['type']);

            return $dropdowns;
        }

        function searchForContact($contact_name,$idsOnly=TRUE){

            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);

            $select = $idsOnly ? "CONCAT(first_name,' ',last_name) AS label,id AS value" : "*";

            $query->select($select)
                ->from("#__crmery_people")
                ->where("published=".$this->published)
                ->where("LOWER(first_name) LIKE '%".$db->escape(ucwords($contact_name))."%' OR LOWER(last_name) LIKE '%".$db->escape(ucwords($contact_name))."%'");

            $db->setQuery($query);

            $return = $db->loadObjectList();

            return $return;

        }

         /**
         * Checks for existing company by name
         * @param  [var] $name company name to check
         * @return [int]       ID of existing company
         */
        function checkPersonName($name) 
        {
            $db = JFactory::getDBO();
            $query = $db->getQuery(true);
            $query->select('p.id');
            $query->from('#__crmery_people AS p');
            $query->where('LOWER(CONCAT(p.first_name," ",p.last_name)) = "'.$db->escape(strtolower($name)).'"');
            $db->setQuery($query);
            $existingPerson = $db->loadResult();

            return $existingPerson;
        }

        function getPeopleNames($json=FALSE){
            $names = $this->getPeopleList();
            $return = array();
            if ( count($names) > 0 ){
                foreach ( $names as $key => $person ){
                    $personName = "";
                    $personName .= array_key_exists('first_name',$person) ? $person['first_name'] : "";
                    $personName .= array_key_exists('last_name',$person) ? " ".$person['last_name'] : "";
                    $return[] = array('label'=>$personName,'value'=>$person['id']);
                }   
            }
            return $json ? json_encode($return) : $return;
        }

        function getEmail($person_id){

            $db = JFactory::getDBO();
            $query = $db->getQuery(true);
            $query->select('p.email');
            $query->from('#__crmery_people AS p');
            $query->where('p.id='.$person_id);
            $db->setQuery($query);
            $uid = $db->loadResult();

            return $uid;

        }


}
    