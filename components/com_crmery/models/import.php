<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.model');

class CrmeryModelImport extends JModel
{   

    protected $db = null;
    protected $query = null;

    /**
     * 
     *
     * @access  public
     * @return  void
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Import a CSV File
     * @param  [String] $data
     * @param  [String] $model [ Model to import ]
     * @return [Boolean] $success
     */
    function importCSVData($data,$modelName){

        $success = false;
        $session = JFactory::getSession();
        $session->set("batch_type",$modelName);

        unset($data['headers']);

        if ( count($data) > 0 && isset($modelName) ){
            $model =& JModel::getInstance($modelName,'CrmeryModel');
            $i = 0;

            foreach ( $data as $import ){

                if ( $i == 1000 ){
                    jimport("joomla.filesystem.file");
                    $file = '<?php $batchType="'.$modelName.'"; $batchData='.var_export($data,true).";";
                    if (JFile::exists(JPATH_BASE."/tmp/crmery_import.php")){
                        JFile::delete(JPATH_BASE."/tmp/crmery_import.php");
                    }
                    JFile::write(JPATH_BASE."/tmp/crmery_import.php",$file);
                    return "redirect";
                }

                if ( $id = $this->checkDuplicate($modelName,$import)){
                    $import['id'] = $id;
                }

                $import['import'] = true;
                $item = $model->store($import);
                $import = null;
                unset($import);
                $i++;

            }

            if (JFile::exists(JPATH_BASE."/tmp/crmery_import.php")){
                JFile::delete(JPATH_BASE."/tmp/crmery_import.php");
            }
            $success = true;
            return true;
        }

        return $success;
    }

    private function checkDuplicate($modelName,$import){

        switch ( $modelName ){
            case "people":

                $db = $this->db != null ? $this->db : JFactory::getDBO();
                $query = $this->query != null ? $this->query : $db->getQuery(true);
                $query->clear()
                    ->select("id")
                    ->from("#__crmery_people")
                    ->where("first_name='".$db->getEscaped($import['first_name'])."'")
                    ->where("last_name='".$db->getEscaped($import['last_name'])."'")
                    ->where("email='".$db->getEscaped($import['email'])."'")
                    ->where("published=1");

                $db->setQuery($query);
                return $db->loadResult();

            break;
            case "company":

                /*
                $db = $this->db != null ? $this->db : JFactory::getDBO();
                $query = $this->query != null ? $this->query : $db->getQuery(true);
                $query->clear()
                    ->select("id")
                    ->from("#__crmery_companies")
                    ->where("name='".$db->getEscaped($import['name'])."'")
                    ->where("published=1");

                $db->setQuery($query);
                return $db->loadResult();
                */
               return false;

            break;
            case "deal":
                return false;
            break;
        }

    }

    /**
     * Read a CSV File
     * @param  [String] $file
     * @return [Mixed]  $data
     */
    function readCSVFile($file){
        ini_set("auto_detect_line_endings", "1");
        $data = array();
        $line = 1;
        $headers = array();
        $i = -2;
        $db =& JFactory::getDBO();
        $table = $db->getTableColumns("#__crmery_".JRequest::getVar('import_type'));
        $special_headers = array('company_id','company_name','stage_name','source_name','status_name','primary_contact_name','assignee_name','owner_name','type');

        switch ( JRequest::getVar("import_type") ){
            case "deals":
                $customType = "deal";
            break;
            case "people":
                $customType = "people";
            break;
            case "companies":
                $customType = "company";
            break;
            default:
                $customType = $table;
            break;
        }
        $customFields = CrmeryHelperCustom::getCustomFields($customType);

        if (($handle = fopen($file, "r")) !== FALSE) {

            while (($read = fgetcsv($handle, 1000, ",")) !== FALSE) {

                $i++;
                $num = count($read);

                if ( $line == 1 ){

                    $headers = $read;
                    $data['headers'] = $headers;

                }else{

                    $line_data = array();

                    for ($c=0; $c < $num; $c++) {

                        $header_name = array_key_exists($c,$headers) ? $headers[$c] : FALSE;

                        if ( $header_name ){

                            if ( in_array($header_name,$special_headers) ){

                                switch($header_name){

                                    case "company_id":
                                        $model =& JModel::getInstance('company','CrmeryModel');
                                        $new_header = "company_id";
                                        $company_name = $model->getCompanyName($read[$c]);
                                        if ( $company_name != "" ){
                                            $name = $company_name;
                                        }else{
                                            $name = "";
                                        }
                                        $special_data = $read[$c];

                                    break;

                                    case "company_name":

                                        $model =& JModel::getInstance('company','CrmeryModel');
                                        $company_id = $model->getCompanyList($read[$c]);
                                        if ( count($company_id) > 0 && array_key_exists(0,$company_id) ){
                                            $new_header = "company_id";
                                            $name = $company_id[0]['id'];
                                        }else{
                                            $name = $read[$c];
                                            $new_header = "company_name";
                                        }
                                        $special_data = $name;

                                    break;

                                    case "stage_name":

                                        $new_header = "stage_id";
                                        $stage_id = CrmeryHelperDeal::getStages($read[$c]);
                                        if ( count($stage_id) ){
                                            $keys = array_keys($stage_id);
                                            $stage_id = $keys[0];
                                        }
                                        $special_data = $stage_id;

                                    break;

                                    case "source_name":

                                        $new_header = "source_id";
                                        $source_id = CrmeryHelperDeal::getSources($read[$c]);
                                        if ( count($source_id) ){
                                            $keys = array_keys($source_id);
                                            $source_id = $keys[0];
                                        }
                                        $special_data = $source_id;

                                    break;

                                    case "status_name":

                                        $new_header = "status_id";
                                        $status_id = CrmeryHelperDeal::getStatuses($read[$c]);
                                        if ( count($status_id) ){
                                            $keys = array_keys($status_id);
                                            $status_id = $keys[0];
                                        }
                                        $special_data = $status_id;

                                    break;

                                    case "primary_contact_name":

                                        $model =& JModel::getInstance('people','CrmeryModel');
                                        $contact = $model->searchForContact($read[$c]);
                                        if ( $contact != "" ){
                                            $new_header = "primary_contact_id";
                                            $special_data = array('label'=>$contact[0]->label,'value'=>$contact[0]->value);
                                        }else{
                                            $new_header = "primary_contact_name";
                                            $special_data = $read[$c];
                                        }
                                        $special_data = $read[$c];

                                    break;

                                    case "assignee_name":

                                        $new_header = "assignee_id";
                                        $model =& JModel::getInstance('user','CrmeryModel');
                                        $contact = $model->searchForUser($read[$c]);
                                        $special_data = $contact ? $contact : CrmeryHelperUsers::getUserId();

                                    break;

                                    case "owner_name":

                                        $new_header = "owner_id";
                                        $model =& JModel::getInstance('user','CrmeryModel');
                                        $contact = $model->searchForUser($read[$c]);
                                        $special_data = $contact ? $contact : CrmeryHelperUsers::getUserId();

                                    break;

                                    case "type":

                                        $new_header = "type";
                                        $special_data = array('dropdown' => ucwords(CrmeryHelperDropdown::getContactTypes($read[$c])));
                                        $special_data = $read[$c];

                                    break;
                                }

                                $line_data[$new_header] = $special_data;

                            }else{

                                if ($pos = strrpos($header_name,"_custom")){

                                    $keyName = strtolower(substr($header_name,0,$pos));
                                    $key = array_key_exists($keyName,$customFields) ? $customFields[$keyName] : false;
                                    if ( $key ){
                                        $newFieldName = substr($header_name,$pos+1)."_".$key->id;
                                        $special_data = array(
                                                'id' => $key->id,
                                                'label'=>$key->name,
                                                'values' => is_array($key->values) ? array_values($key->values) : $key->values,
                                                'type'=>"custom",
                                                'custom_type'=>$key->type,
                                                'multiple_selections'=>$key->multiple_selections
                                        );
                                        if ( $key->multiple_selections == 1 ){
                                            $exp = explode(",",$read[$c]);
                                            $customValues = count($exp) > 0 ? $exp : array($read[$c]);
                                            // $special_data['value']=CrmeryHelperCustom::getCustomValue($customType,$key->id,$customValues);
                                            $special_data = array_keys(array_intersect($key->values,$customValues));
                                            $arr = array('-1'=>'1');
                                            if ( count($special_data) > 0 ){
                                                foreach($special_data as $key){
                                                    $arr[$key] = 1;
                                                }
                                            }
                                            $special_data = $arr;
                                        }else if ( $key->type=="picklist" ){
                                            // $special_data['value']=CrmeryHelperCustom::getCustomValue($customType,$key->id,$read[$c]);
                                            $special_data = array_keys(array_intersect($key->values,array($read[$c])));
                                            $special_data = array_key_exists(0,$special_data) ? $special_data[0] : "";
                                        }else{
                                            $special_data = $read[$c];
                                        }

                                        $line_data[$newFieldName] = $special_data;
                                    }

                                } else if ( array_key_exists($header_name,$table) ){

                                    $line_data[$header_name] = $read[$c];

                                }
                            }

                        }

                    }

                    if ( count($line_data) > 0 ){

                        $data[] = $line_data;
                        
                    }

                }

                $line++;

            }

            fclose($handle);

        }

        return $data;

    }



}