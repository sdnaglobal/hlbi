<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryModelMail extends JModel
{

        // Get config settings
        private $hostname = null;
        private $username = null;
        private $password = null;
        private $service  = null;
        private $port     = null;
        private $errors   = null;
        private $alerts   = null;

        /**
         * IMAP CONNECTION
         */
        private $imap = null;
        private $stucture = null;
        private $attachments = null;
        
        /**
         * 
         *
         * @access  public
         * @return  void
         */
        function __construct()
        {
            parent::__construct();
            /**Initialize Configurations**/
            $this->_getConfig();
        }
        
        /**
         * Method to store a record
         *
         * @return    boolean    True on success
         */
        function store()
        {
            
        }

        /**
         * Get imap configuration
         */
        private function _getConfig(){
            $config = CrmeryHelperConfig::getImapConfig();
            $this->hostname = $config->imap_host;
            $this->username = $config->imap_user;
            $this->password = $config->imap_pass;
            $this->service  = $config->imap_service;
            $this->port     = $config->imap_port;
             // Validate config
            if (strlen($this->hostname) == 0 || strlen($this->username) == 0  || strlen($this->password) == 0 || strlen($this->service) == 0 || strlen($this->port) == 0 ) {
                return false;
            }else{
                return true;
            }
        }

        /**
         * CONNECT TO IMAP
         * @param  [String] $hostname=null [ if we wish to override the default configurations in the constructor ]
         * @param  [String] $username=null [ if we wish to override the default configurations in the constructor ]
         * @param  [String] $password=null [ if we wish to override the default configurations in the constructor ]
         * @return [BOOlean]                [success]
         */
        private function _connect($hostname=null,$username=null,$password=null,$service=null,$port=null){

            if ( $hostname ){
                $this->hostname = $hostname;
            }
            if ( $username ){
                $this->username = $username;
            }
            if ( $password ){
                $this->password = $password;
            }
            if ( $service ){
                $this->service = $service;
            }
            if ( $port ){
                $this->port = $port;
            }

            $connected = FALSE;

            if ( strlen($this->hostname) == 0 || strlen($this->username) == 0  || strlen($this->password) == 0 || strlen($this->service) == 0 || strlen($this->port) == 0 ) {

                return false;

            }else{

                /* try to connect to default ssl */
                $error = error_reporting();
                error_reporting(0);
                if ( $this->imap = @imap_open('{'.$this->hostname.':'.$this->port.'/'.$this->service.'/ssl/novalidate-cert}INBOX',$this->username,$this->password) ){
                    $error = error_reporting($error);
                    $connected = true;
                }else{
                    $error = error_reporting($error);
                    $connected = false;
                }

                /* try to connect to fallback */
                if ( !$connected ){
                    $error = error_reporting();
                    error_reporting(0);
                    if ( $this->imap = @imap_open('{'.$this->hostname.'/'.$this->service.'/notls}INBOX',$this->username,$this->password) ){
                        $error = error_reporting($error);
                        $connected = true;
                    }else{
                        $error = error_reporting($error);
                        $connected = false;
                    }
                }

                /** SUPPRESS IMAP ERRORS **/
                $this->errors    = imap_errors();
                $this->alerts    = imap_alerts();

            }

            return $connected;
        }

        public function testConnection($host,$user,$pass,$service,$port)
        {
            $connection = $this->_connect($host,$user,$pass,$service,$port);
            return $connection;
        }

        public function getErrors()
        {
            return $this->errors;
        }

        public function getAlerts()
        {
            return $this->alerts;
        }

        /**
         * CLOSE CONNECTION
         */
        private function _close(){
            if ( $this->imap ){
                imap_expunge($this->imap);
                imap_close($this->imap, CL_EXPUNGE);
                imap_errors();
                imap_alerts();
            }
        }

        /**
         * Build search string for imap searches
         * @return [Mixed] [Searches]
         */
        private function _buildSearch(){

            //Associated emails
            $emails = CrmeryHelperUsers::getEmails();

            $searchStrings = false;

            if ( $emails ){
                $searchStrings = array();
                for ( $i=0; $i<count($emails); $i++ ){
                    $email = $emails[$i];
                    $searchStrings[] = 'UNSEEN FROM "'.$email['email'].'"';
                }
            }

            // $searchString = "UNSEEN";

            return $searchStrings;

        }

        /**
         * SEARCH IMAP
         * @param  Mixed $filter search filter as String or as Array of searches
         * @param  string $params option filter params
         * @return mixed $data emails
         */
        private function _search($filter="ALL",$params=null){

            if ( is_array($filter) ){
                $emails = array();
                foreach ( $filter as $string ){
                    $search = imap_search($this->imap,$string);
                    if ( $search ){
                        $emails = array_merge($emails,$search);
                    }
                }
            }else{
                $emails = imap_search($this->imap,$filter . " " . $params );
            }

            /* if emails are returned, cycle through each... */
            $data = array();

            if($emails) {
              /* for every email... */
              foreach($emails as $email_number) {
                /* get information specific to this email */
                $overview = imap_fetch_overview($this->imap,$email_number,0);
                $this->structure = imap_fetchstructure($this->imap,$email_number,0);


                /* commented out to speed up email retrieval */
                $headers = imap_rfc822_parse_headers(imap_fetchheader($this->imap,$email_number));


                switch ( strtolower($this->structure->subtype) ){
                    case "plain":
                        $partNum = 1;
                    break;
                    case "alternative":
                        $partNum = 1;
                    break;
                    case "mixed":
                        $partNum = 1.2;
                    break;
                    case "html":
                        $partNum = 1.2;
                    break;
                }
                $partNum=1;

                $message =  quoted_printable_decode($this->filterEmail(imap_fetchbody($this->imap,$email_number,"1.2",FT_PEEK)));
                if ( $message == "" ){
                    $message = quoted_printable_decode($this->filterEmail(imap_fetchbody($this->imap,$email_number,"1",FT_PEEK)));
                }
                // $message =  quoted_printable_decode($this->filterEmail(imap_fetchbody($this->imap,$email_number,"1",FT_PEEK)));

                /* get any possible attachments, commented out to speed up email retrieval, we do not want to do this unless we are automatically associating an email with a user */
                // $attachments = $this->getAttachments($email_number);

                $email = array( 
                        'overview'      => $overview[0],
                        'structure'     => $this->structure,
                        'headers'       => $headers,
                        'message'       => $message,
                        // 'attachments'   => $attachments
                    );
                $data[$email_number] = $email;

              }

            }

            return $data;
        }


        public function getAttachments($email_number) {
            $this->attachments = array();
            $email = @imap_fetchstructure($this->imap, $email_number,0);
            imap_errors();
            imap_alerts();
            $bypassFilenames = CrmeryHelperUsers::getDocumentBypass();
            if ( is_object($email) ){

                $parts = $this->create_part_array($email);
                $apple = $this->detectApple($email);

                for ( $i=0; $i<count($parts); $i++ ){
                    $part = $parts[$i];
                    if ( array_key_exists('part_object',$part) && $part['part_object']->ifdparameters ){
                        for ( $i2=0; $i2<count($part['part_object']->dparameters); $i2++){
                            if ( $apple ){
                                $attachment = ( isset($part['part_object']->dparameters[$i2]->attribute) && 
                                               strtoupper($part['part_object']->dparameters[$i2]->attribute) == "FILENAME"  && 
                                               !in_array($part['part_object']->dparameters[$i2]->value,$bypassFilenames) ) //check for apple mail attachments
                                                ? TRUE : FALSE;
                            }else{
                                $attachment = ( strtoupper($part['part_object']->disposition) == "ATTACHMENT" ) ? TRUE : FALSE;
                            }


                            if ( $attachment )
                            {   
                                $param = $part['part_object']->dparameters[$i2];
                                $param->encoding = $part['part_object']->encoding;
                                $param->attachment = @imap_fetchbody($this->imap, $email_number, $part['part_number'],FT_PEEK);    
                                if($param->encoding == 3) { // 3 = BASE64
                                    $param->attachment = base64_decode($param->attachment);
                                }
                                elseif($param->encoding == 4) { // 4 = QUOTED-PRINTABLE
                                    $param->attachment = quoted_printable_decode($param->attachment);
                                }
                                $this->attachments[]=$param;
                            }

                        }
                    }
                }
            }else{
                return false;
            }
            return $this->attachments;
        }

        private function detectApple($structure){
            if ( count($structure->parameters) > 0 ){
                foreach ($structure->parameters as $param){
                    if ( stripos($param->value,"apple-mail") !== false ){
                        return true;
                    }
                }
            }
            return false;
        }

        /**
         * Store email attachments to local device
         * @param  [mixed] $attachments [array of attachments]
         * @param  [String] $location [storage location]
         */
        private function _storeAttachments($attachments,$key,$location="person"){
            if ( is_array($attachments) && count($attachments) > 0 ){
                $model =& JModel::getInstance('document','CrmeryModel');
                foreach ( $attachments as $attachment_key => $attachment ){
                        $attachment->association_id = $key;
                        $attachment->association_type = $location;
                        $attachment->email = 1;
                        $model->store($attachment);
                }
            }
        }
        
        public function storeAttachments($email_id, $person_id,$location="person") {

            $this->structure = @imap_fetchstructure($this->imap,$email_id);
            $attachments = $this->getAttachments($email_id);
            $this->_storeAttachments($attachments,$person_id,$location);
            imap_errors();
            imap_alerts();

        }

        /**
         * Associate emails with correct users
         */
        private function _associateEmails($emails){
            
            //Pull all IDS from #__crmery_people WHERE owner_id EQUALS current logged in user
            $people = CrmeryHelperUsers::getPeopleEmails();

            //If any emails match up with our TO then we automatically insert them as notes and store any attachments
            if ( count($emails) > 0 ){
                foreach ( $emails as $email_key => $email ){

                    $this->structure = $email['structure'];

                    $address = $email['headers']->to[0]->mailbox."@".$email['headers']->to[0]->host;

                    if ( $key = array_search($address,$people) ){

                        $data = array();
                        $data['note'] = $email['message'];
                        $data['owner_id'] = CrmeryHelperUsers::getUserId();
                        $data['person_id'] = $key;

                        $model =& JModel::getInstance('note','CrmeryModel');
                        $model->store($data);

                        $attachments = $this->getAttachments($email['overview']->msgno);
                        if ( count($attachments) > 0 ){
                            $email['attachments'] = $attachments;
                            $this->_storeAttachments($email['attachments'],$key);
                        }

                        $this->_delete($email['overview']->msgno);
                        unset($emails[$email_key]);
                    }
                }
            }

            return $emails;
        }

        /**
         * DELETE EMAILS
         * @param  mixed $message_ids message id(s) that should be deleted
         */
        private function _delete($message_ids){
            if ( is_array($message_ids) ){
                foreach ( $message_ids as $id ){
                    imap_delete($this->imap,$id);
                }
            } else {
                imap_delete($this->imap,$message_ids);
            }
        }
        

        /**
         * Retrieve user emails // inbox
         * @return [type] [description]
         */
        public function getMail(){

            // Validate config
            if (strlen($this->hostname) == 0 || strlen($this->username) == 0  || strlen($this->password) == 0 ) {
                return false;
            }

            /* grab emails */
            if ( $this->_connect() ){

                /** construct and perform imap search **/
                $where = $this->_buildSearch();
                $emails = array();
                if ( $where ){
                    $emails = $this->_search($where);
                }
                if ( $emails ){
                    /** Associate emails and autoinsert entries into database, returns nonassociated emails **/
                    $emails = $this->_associateEmails($emails);
                }

                /* close the connection */
                $this->_close();
                return $emails;

            }
        }

        /**
         * GET INDIVIDUAL EMAILS
         * @param  [type] $email_id [description]
         * @return [type]           [description]
         */
        public function getEmail($email_id,$msgOnly=TRUE){

            $this->_connect();
            $email = @imap_fetchstructure($this->imap, $email_id,0);
            imap_errors();
            imap_alerts();

            if ( is_object($email) ){

                switch ( strtolower($email->subtype) ){
                        case "plain":
                            $partNum = 1;
                        break;
                        case "alternative":
                            $partNum = 1;
                        break;
                        case "mixed":
                            $partNum = 1.2;
                        break;
                        case "html":
                            $partNum = 1.2;
                        break;
                    }

                $message =  quoted_printable_decode($this->filterEmail(imap_fetchbody($this->imap,$email_id,"1.2",FT_PEEK)));
                if ( $message == "" ){
                    $message = quoted_printable_decode($this->filterEmail(imap_fetchbody($this->imap,$email_id,"1",FT_PEEK)));
                }

                imap_errors();
                imap_alerts();

                if ( !$msgOnly ) {

                    $overview = @imap_fetch_overview($this->imap,$email_id,0);
                    imap_errors();
                    imap_alerts();

                    // $headers = imap_rfc822_parse_headers(imap_fetchheader($this->imap,$email_id));


                    switch ( strtolower($email->subtype) ){
                        case "plain":
                            $partNum = 1;
                        break;
                        case "alternative":
                            $partNum = 1;
                        break;
                        case "mixed":
                            $partNum = 1.2;
                        break;
                        case "html":
                            $partNum = 1.2;
                        break;
                    }

                   $attachments = $this->getAttachments($email_id);

                    $emailInfo = array( 
                            'overview'      => $overview[0],
                            'structure'     => $email,
                            // 'headers'       => $headers,
                            'message'       => $message,
                            'attachments'   => $attachments
                        );

                }

            }else{
                return false;
            }

            $this->_close();

            return $msgOnly ? $message : $emailInfo;
        }

        //filter emails based on user defined filters in their profile
        function filterEmail($content){

            return preg_replace("/<[^<>]*src[^<>]*>/i", "", $content);

            /*
            //strip all html elements from email
            return strip_tags($content);

            //remove images from html
            $content = preg_replace("/<img.*?>/i","",$content);
            return $content;

            // $filters = CrmeryHelperUsers::getDocumentBypass();
            if ( count($filters) > 0 ){
                foreach ( $filters as $filter ){
                    if ( $filter != "" ){
                        // $content = preg_replace("/<(.?)*+\s*+name=\"".$filter."\"+\s+(>|\/>)/i","",$content);
                    }
                }
            }
            return $content;
            */
        }

        function create_part_array($structure, $prefix="") {
           $part_array = array();

            if (sizeof($structure->parts) > 0) {
                foreach ($structure->parts as $count => $part) {
                    $this->add_part_to_array($part, $prefix.($count+1), $part_array);
                }
            }
           
           return $part_array;
        }

        function add_part_to_array($obj, $partno, & $part_array) {

            if ($obj->type == TYPEMESSAGE) {
                $this->parse_message($obj->parts[0], $partno.".");
            }
            else {
                if (array_key_exists('parts',$obj) && sizeof($obj->parts) > 0) {
                    foreach ($obj->parts as $count => $p) {
                            $this->add_part_to_array($p, $partno.".".($count+1), $part_array);
                    }
                }
            }
               
            $part_array[] = array('part_number' => $partno, 'part_object' => $obj);

        }

        function parse_message($obj, $prefix="") {
        /* Here you can process the data of the main "part" of the message, e.g.: */
          // do_anything_with_message_struct($obj);

          if (sizeof($obj->parts) > 0)
            foreach ($obj->parts as $count=>$p)
              $this->parse_part($p, $prefix.($count+1));
        }

        function parse_part($obj, $partno) {
        /* Here you can process the part number and the data of the parts of the message, e.g.: */
          // do_anything_with_part_struct($obj,$partno);

          if ($obj->type == TYPEMESSAGE)
            $this->parse_message($obj->parts[0], $partno.".");
          else
            if (sizeof($obj->parts) > 0)
              foreach ($obj->parts as $count=>$p)
                $this->parse_part($p, $partno.".".($count+1));
        }

        /**
         * Remove user emails from inbox
         */
        public function removeEmail($message_id=null){
            if ( $this->_connect() ) {
                if ( !$message_id ){
                    $message_id = JRequest::getVar('id');
                }
                if ( $message_id != null || $message_id != 0 ){
                    $this->_delete($message_id);
                }
                $this->_close();
            }
        }

        public function saveEmail($email_id=null){

            if ( !$email_id ){
                $email_id = JRequest::getVar('id');
            }

            $email = $this->getEmail($email_id,FALSE);

            $person_id = JRequest::getVar('person_id');
            $deal_id = JRequest::getVar('deal_id');
            $person_name = JRequest::getVar('person_name');
            $deal_name = JRequest::getVar('deal_name');

            $data = array(
                    'deal_id'       => $deal_id,
                    'person_id'     => $person_id,
                    'note'          => $email['message'],
                    'person_name'   => $person_name,
                    'deal_name'     => $deal_name,
                    'name'          => $email['overview']->subject
                );

            $noteModel =& JModel::getInstance('Note','CrmeryModel');
            $noteModel->store($data);

            $this->_connect();

            if ( $person_id ){
                try{ 
                    $this->storeAttachments($email_id,$person_id,"person");
                }catch(Exception $e){

                }
            }
            if ( $deal_id ){
                try{
                    $this->storeAttachments($email_id,$deal_id,"deal");
                }catch(Exception $e){

                }
            }

            $this->_delete($email_id);
            $this->_close();

        }
        
}