<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.model');

class CrmeryModelContacts extends JModel
{

	var $person_id = null;
	var $deal_id = null;
	var $company_id = null;
	var $event_id = null;
	var $query = null;

	public function buildSelect(){

		$this->query->select("DISTINCT(p.id),p.*,
							u.first_name as owner_first_name,u.last_name as owner_last_name,
							c.id as company_id,c.name as company_name,IF(p.id=d2.primary_contact_id, 1, 0) AS is_primary_contact");

		$this->query->from("#__crmery_people AS p");
		$this->query->leftJoin("#__crmery_people_cf as cf ON cf.person_id = p.id");
    	$this->query->leftJoin("#__crmery_companies AS c ON c.id = p.company_id");
    	$this->query->leftJoin("#__crmery_deals AS d ON d.id = cf.association_id AND cf.association_type = 'deal'");
    	$this->query->leftJoin("#__crmery_deals AS d2 ON d2.primary_contact_id = p.id");
    	$this->query->leftJoin("#__crmery_shared AS shared ON shared.item_id=p.id AND shared.item_type='person'");
        $this->query->leftJoin("#__crmery_users AS u ON u.id = p.owner_id");
        $this->query->leftJoin("#__crmery_users AS u2 ON u2.id = p.assignee_id");

	}

	public function buildWhere(){

		if ( $this->deal_id ){
			$this->query->where("(cf.association_id = ".$this->deal_id." OR d2.id = ".$this->deal_id.")");
		}

		if ( $this->event_id ){
			$event_model = JModel::getInstance('event','CrmeryModel');
			$event = $event_model->getEvent($this->event_id);
			if ( array_key_exists('association_type',$event) && $event['association_type'] != null ){
				switch ( $event['association_type'] ){
					case "person":
						$this->query->where("p.id=".$event['association_id']);
					break;
					case "deal":
						$this->query->where("cf.association_id=".$event['association_id']);
						$this->query->where("cf.association_type='deal'");
					break;
					case "company":
						$this->query->where("p.company_id=".$event['association_id']);
					break;
				}
			}else{
				return false;
			}
		}

		if ( $this->company_id ){
			$this->query->where("p.company_id=".$this->company_id);
		}

		//filter based on member access roles
        $user_id = CrmeryHelperUsers::getUserId();
        $member_role = CrmeryHelperUsers::getRole();
        $team_id = CrmeryHelperUsers::getTeamId();
        
        if ( $member_role != 'exec' ){
            
            if ( $member_role == 'manager' ){
                $this->query->where("(p.owner_id=$user_id OR p.assignee_id=$user_id OR u.team_id=$team_id OR u2.team_id=$team_id OR shared.user_id=$user_id)");
            }else{
                $this->query->where("(p.owner_id=$user_id OR p.assignee_id=$user_id OR shared.user_id=$user_id)");
            }
            
        }

		$this->query->where("p.published>0");
		return true;
	}

	public function buildOrder(){
		$this->query->order("is_primary_contact DESC");
	}

	public function getContacts(){

		$db =& JFactory::getDBO();
        $this->query = $db->getQuery(true);

        $this->buildSelect();
        if ( !$this->buildWhere() ){
        	return false;
        }

        $this->buildOrder();
		
		//echo $this->query;
		//die;
        $db->setQuery($this->query);
        $people = $db->loadAssocList();

        return $people;

	}

}