<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.model');

class CrmeryModelExport extends JModel
{

	function __construct(){			
		parent::__construct();
	}

	/**
	 * Dynamically download CSV files based on type requested
	 * @return [type] [description]
	 */
	public function getCsv(){

		//Determine request type
		$download_type = JRequest::getVar('list_type');

		//Generate CSV data based on request type
		$data = $this->getCsvData($download_type);
		// echo '<pre>';		
		//	print_r($data['rows']);
		if($download_type == 'people')
		{
			if(count($data['rows']>0))
			{
				$temp_data_new = array();
				foreach($data['rows'] as  $newrows)
				{
					$temp_data = array();
					 foreach($newrows as $newkey=>$newrow)
					 {	
						//echo '<br>$newkey=>'.$newkey ;
						//echo '<br>$newkey=>'.$newrow; 
						if($newkey == 'id')
						{
							$temp_data['Referral_Code'] = CRMText::_('COM_CRMERY_REFERRAL_CODE_PREFIX').$newrow;	
							$deal_name = ''; 
							$db = JFactory::getDbo();
							$query_deal = $db->getQuery(true);
							$query_deal = "SELECT d.name FROM #__crmery_deals as d, #__crmery_people_cf as cf where d.id=cf.association_id and cf.person_id=".$newrow." and cf.association_type='deal'";

							$db->setQuery($query_deal);
							$deal_name = $db->loadResult();
							$temp_data['Client_Name'] = $deal_name;	
						}	

						if($newkey == 'owner_first_name')
							$temp_data['Referring_Partner_First_Name'] = $newrow;
							
						if($newkey == 'owner_last_name')
							$temp_data['Referring_Partner_Last_Name'] = $newrow;	
							
						if($newkey == 'referring_firm_id')
						{	
							$ref_firms = CrmeryHelperCompany::getCompany($newrow);
							 
							$ref_firm_name_ = $ref_firms[0]['name'];

							$temp_data['Referring_Firm'] = $ref_firm_name_;					
						}
						
						if($newkey == 'owner_country_id')
						{	
							$cntry_id = @explode(',',$newrow);
							$cntryid = $cntry_id[0];
							
							$cntry_name = $this->get_country_name_by_id($cntryid);
							$temp_data['Referring_Country'] = $cntry_name;					
						}
						
						if($newkey == 'company_name')
						{	
							$temp_data['Receiving_Firm'] = $newrow;					
						}

						if($newkey == 'assignee_name')
						{	
							$temp_data['Receiving_Partner'] = $newrow;					
						}

						if($newkey == 'assignee_country_id')
						{	
							$cntry_id = @explode(',',$newrow);
							$cntryid = $cntry_id[0];
							
							$cntry_name = $this->get_country_name_by_id($cntryid);
							$temp_data['Receiving_Country'] = $cntry_name;					
						}
					
						if($newkey == 'ref_amount_id')
						{	
							$refvalue = $this->get_amount_billed_by_referral_id($newrow,date('Y')-1);
							$refvalue = number_format($refvalue,2,'.',',');
							$amount_gbp = 'Amount_in_GBP_'.(date('Y')-1); 
							$temp_data[$amount_gbp] = $refvalue;					
						}

						if($newkey == 'source_name')
						{	
							$temp_data['Source'] = $newrow;					
						}

						if($newkey == 'created')
						{	
							$temp_data['Added'] = $newrow;					
						}
						
						if($newkey == 'Accepted')
						{	
							if($newrow == 1){  
							   $temp_data['Referral_Status'] = 'Accepted';
							 }
							 else
							 {
								$temp_data['Referral_Status'] = 'Pending';
							 }
						}
						
					}
					$temp_data_new[] = $temp_data;
				}
				
			}
			 
			 
			 if ( count($temp_data_new) ){
				$header = array_keys($temp_data_new[0]);
			}
			
			//return csv file
			return($this->generateCsv($header,$temp_data_new));
		}
		else{
			return($this->generateCsv($data['header'],$data['rows']));
		}
	}

	
	/*  csv people function start */
	
function get_country_name_by_id($cntryid)
{
	$db = JFactory::getDbo();
	$query_cntry = $db->getQuery(true);
	$query_cntry = $db->getQuery(true);
	$query_cntry = "SELECT country_name FROM #__crmery_currencies where country_id=".$cntryid;

	$db->setQuery($query_cntry);
	return $cntry_name = $db->loadResult();
	
}

function get_amount_billed_by_referral_id($ref_id,$year)
{

	$db = JFactory::getDbo();
	$billing = $db->getQuery(true);
	$billing->select('SterlingAmount');
	$billing->from('#__crmery_referral_payments');
	$billing->where('FeePaid=1 and referral_ID='.$ref_id);
	$billing->where(' YEAR(STR_TO_DATE(BillingYear, "%d/%m/%Y"))='.$year);
	$db->setQuery($billing);
 
	$billingdetails = $db->loadObjectList();
 
	$GBPsum = 0;

	foreach($billingdetails as $bill)
	{ 	
		$GBPsum = ($GBPsum+$bill->SterlingAmount);
	}
	return $GBPsum;
	
}

/* csv people function end

  */
	/**
	 * Get CSV data
	 * @param  [type] $data_type [description]
	 * @return [type]            [description]
	 */
	public function getCsvData($data_type){

		$data = array();

		$export_ids = JRequest::getVar('ids');
		 
		switch($data_type){
			case "deals":
				$model =& JModel::getInstance('deal','CrmeryModel');
				$model->set("_id",$export_ids);
				$data = $model->getDeals($export_ids);
			break;
			case "companies":
				$model =& JModel::getInstance('company','CrmeryModel');
				$data = $model->getCompanies($export_ids);
			break;
			case "people":
				$model =& JModel::getInstance('people','CrmeryModel');
				$model->set("_id",$export_ids);
				$data = $model->getPeople();
				
			break;
			case "sales_pipeline":
	            $model =& JModel::getInstance('deal','CrmeryModel');
	            $model->set("_id",$export_ids);
    		    $data = $model->getReportDeals($export_ids);
			break;
			case "source_report":
				$model =& JModel::getInstance('deal','CrmeryModel');
				$model->set("_id",$export_ids);
        		$data = $model->getDeals($export_ids);
			break;
			case "roi_report":
				$model =& JModel::getInstance('source','CrmeryModel');
        		$data = $model->getRoiSources($export_ids);
			break;
			case "notes":
				$model = JModel::getInstance('note','CrmeryModel');
        		$data = $model->getNotes(NULL,NULL,FALSE);
			break;
			case "custom_report":
        		$model =& JModel::getInstance('report','CrmeryModel');
        		$data = $model->getCustomReportData(JRequest::getVar('report_id'));
			break;

		}	

		$data_type = $data_type == "custom_report" ? "deals" : $data_type;
		$getCustomFields = array("deals","companies","people");
 
	
		if ( in_array($data_type,$getCustomFields) ){
			if ( count($data) > 0 ){
				foreach ( $data as $key => $item ){
					$customData = CrmeryHelperCustom::getCustomCsvData($item['id'],$data_type);
					foreach ( $customData as $name => $value ){
						$data[$key][$name."_custom"] = $value;
					}
					//unset($data[$key]['id']);
				}
			}
		}
	 
		if ( count($data) ){
			$header = array_keys($data[0]);
		}

		return array('header'=>$header,'rows'=>$data);

	}

	/**
	 * Generate CSV
	 * @param  [type] $header [description]
	 * @param  [type] $data   [description]
	 * @return [type]         [description]
	 */
	public function generateCsv($header,$data){

		$str = "";

		if ( count($header) ){
			$str .= implode(',',$header)."\r\n";
		}

		if ( count($data) ){
			foreach ( $data as $row ){
				$arr = array();
				foreach ( $row as $cell ){
					$arr[] = '"'.$cell.'"';
				}
				$str .= implode(",",$arr)."\r\n";
			}
		}

		return $str;

	}

	/**
	 * Generate vcards for people
	 * @return [type] [description]
	 */
	public function getVcard(){

		$person_id = JRequest::getVar('person_id');

		$model =& JModel::getInstance('people','CrmeryModel');

		$person = $model->getPerson($person_id);
		$person = $person[0];

		$str = "";

		$str .= "BEGIN:VCARD\r\n";
		$str .= "VERSION:4.0\r\n";
		$str .= "N:".$person['last_name'].";".$person['first_name'].";;;\r\n";
		$str .= "FN: ".$person['first_name']." ".$person['last_name']."\r\n";
		$str .= "ORG:".$person['company_name']."\r\n";
		$str .= "TITLE:".$person['position']."\r\n";
		$str .= 'TEL;TYPE="work,voice";VALUE=uri:tel:+'.$person['phone']."\r\n";
		$str .= 'TEL;TYPE="mobile,voice";VALUE=uri:tel:+'.$person['mobile_phone']."\r\n";
		$str .= 'ADR;TYPE=work:;;'.$person['work_address_1']." ".$person['work_address_2'].';'.$person['work_city'].';'.$person['work_state'].';'.$person['work_zip'].';'.$person['work_country']."\r\n";
		$str .= "EMAIL:".$person['email']."\r\n";
		$str .= "END:VCARD\r\n";

		return $str;

	}

}