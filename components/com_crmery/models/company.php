<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryModelCompany extends JModel
{
		var $_total     = null;
        var $_view      = null;
        var $_layout    = null;
        var $_user      = null;
        var $_team      = null;
        var $_id        = null;
        var $_type      = null;
        var $_category  = null;
        var $published  = 1;
        var $show_filters = null;
        
        /**
         * Constructor
         */
        function __construct() {
            parent::__construct();
            $this->_view = JRequest::getCmd('view');
            $this->_layout = str_replace('_filter','',JRequest::getCmd('layout'));
        }
        
		/**
		 * Method to store a record
		 *
		 * @return    boolean    True on success
		 */
		function store($data=null)
		{
			//Load Tables
			$row =& JTable::getInstance('company','Table');
            $oldRow =& JTable::getInstance('company','Table');

            if ( $data == null ){
		      $data = JRequest::get( 'post' );
            }

            if ( array_key_exists('name',$data) ){
                $data['name'] = trim($data['name']);
            }
			
			//date generation
			$date = CrmeryHelperDate::formatDBDate(date('Y-m-d H:i:s'));
			
			if ( !array_key_exists('id',$data) ){
				$data['created'] = $date;
                $status = 'created';
                $data['owner_id'] = array_key_exists('owner_id',$data) ? $data['owner_id'] : CrmeryHelperUsers::getUserId();

                //detect if we are importing a company
                $importing = array_key_exists('import',$data);
                if ( !$importing ){
                    $companyExists = $this->checkCompanyName($data['name']);
                    $session = JFactory::getSession();
                    if ( $companyExists != "" ){
                        $session->set("Company.error",true);
                        $session->set("Company.error_message",CRMText::_('COM_CRMERY_DUPLICATE_COMPANY'));
                        $session->set("Company.editing",$data);
                        return false;
                    }
                }

			} else {

                $row->load($data['id']);
                $oldRow->load($data['id']);
                $status = 'updated';
            }
			
			$data['modified'] = $date;
			
			if($date['country_id'] > 0)
			{
				$cid = $date['country_id'];
			    $dbcm =& JFactory::getDbo();
				$cquery = $dbcm->getQuery(true);

				$cquery->select("c.country_name")
					->from("#__crmery_currencies AS c");
			   // $query->where("r.role_type='manager'");
				$cquery->where("c.country_id=$cid");
				

				$dbcm->setQuery($cquery);
				$countrys = $dbcm->loadObjectList();
				$country = $countrys[0];
				$data['address_country'] = $country->country_name;
			}
			
			$data['modified'] = $date;

            //generate custom field string
            $customArray = array();
            foreach( $data as $name => $value ){
                if( strstr($name,'custom_') && !strstr($name,'_input') && !strstr($name,"_hidden") ){
                    $id = str_replace('custom_','',$name);
                    $customArray[] = array('custom_field_id'=>$id,'custom_field_value'=>$value);
                    unset($data[$name]);
                }
            }
			
		    // Bind the form fields to the table
		    if (!$row->bind($data)) {
		        $this->setError($this->_db->getErrorMsg());
                $session->set("Company.error",true);
                $session->set("Company.error_message",CRMText::_('COM_CRMERY_ERROR_SAVING'));
                $session->set("Company.editing",$data);
		        return false;
		    }

            $row->custom_fields = $row->id > 0 ? CrmeryHelperCustom::getCustomCSVData($row->id,"company") : array();
            $dispatcher =& JDispatcher::getInstance();
            $dispatcher->trigger('onBeforeCompanySave', array(&$row));      

		    // Make sure the record is valid
		    if (!$row->check()) {
		        $this->setError($this->_db->getErrorMsg());
                $session->set("Company.error",true);
                $session->set("Company.error_message",CRMText::_('COM_CRMERY_ERROR_SAVING'));
                $session->set("Company.editing",$data);
		        return false;
		    }
	 
		    // Store the web link table to the database
		    if (!$row->store()) {
		        $this->setError($this->_db->getErrorMsg());
                $session->set("Company.error",true);
                $session->set("Company.error_message",CRMText::_('COM_CRMERY_ERROR_SAVING'));
                $session->set("Company.editing",$data);
		        return false;
		    }

            $id = array_key_exists('id',$data) ? $data['id'] : $this->_db->insertId();
			
			
			// CUSTOMIZATION START HERE FOR ASSIGNING FIRM ADMIN TO FIRM
			
				           $last_InsId=$this->_db->insertId();
				
				           $db =& JFactory::getDbo();
                           $querysu = $db->getQuery(true);
						   $querysu->update("#__crmery_users AS u SET u.company_id='".$last_InsId."'");					   
						   $querysu->where("u.id=".$data['partner_id']);						  
						   $db->setQuery($querysu);
						   $db->query();
				         //exit();
			
			// CUSTOMIZATION END HERE FOR ASSIGNING FIRM ADMIN TO FIRM		
		
		 
            CrmeryHelperActivity::saveActivity($oldRow, $row,'company', $status);

            //if we receive no custom post data do not modify the custom fields
            if ( count($customArray) > 0 ){
                CrmeryHelperCrmery::storeCustomCf($id,$customArray,'company');
            }

            $row->custom_fields = $row->id > 0 ? CrmeryHelperCustom::getCustomCSVData($row->id,"company") : array();
            $dispatcher =& JDispatcher::getInstance();
            $dispatcher->trigger('onAfterCompanySave', array(&$row));

            //assign any default template systems ( events )
            if ( $status == "created" ){
                include_once(JPATH_SITE."/components/com_crmery/models/template.php");
                $templateModel = new CrmeryModelTemplate();
                $templateModel->processDefaults($row->id,"company");
            }

		    return $row;
		}

        /**
         * Build our db query object
         */
        function _buildQuery(){

            /** Large SQL Selections **/
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            $db->setQuery("SET SQL_BIG_SELECTS=1");
            $db->query();
            
            $user = $this->_user;
            $team = $this->_team;
            $category = $this->_category;
            $id = $this->_id;
            $type = $this->_type;
            $view = JRequest::getVar('view');

            if ( !$id ){

                $session = JFactory::getSession();
                
                //determine whether we are searching for a team or user
                if ( $user ){
                    $session->set('company_team_filter',null);
                }
                if ( $team ){
                    $session->set('company_user_filter',null);
                }
                
                //set user session data
                if ( $type != null ) {
                    $session->set('company_type_filter',$type);
                } else {
                    $sess_type = $session->get('company_type_filter');
                    $type = $sess_type;
                }
                if ( $user != null ) {
                    $session->set('company_user_filter',$user);
                } else {
                    $sess_user = $session->get('company_user_filter');
                    $user = $sess_user;
                }
                if ( $team != null ){
                    $session->set('company_team_filter',$team);
                }else{
                    $sess_team = $session->get('company_team_filter');
                    $team = $sess_team;
                }

                if ( $category != null ){
                    $session->set('company_category_filter',$category);
                }else{
                    $sess_category = $session->get('company_category_filter');
                    $category = $sess_category;
                }

            }
            
            $db =& JFactory::getDBO();
            
            //generate query for base companies
            $query = $db->getQuery(true);
            $export = JRequest::getVar('export');

            if ( $export ){

                $select_string  = 'DISTINCT(c.id),c.name,c.description,c.address_1,c.address_2,c.address_city,';
                $select_string .= 'c.address_state,c.address_zip,c.address_country,c.website,c.created,c.modified,';
                $select_string .= 'c.phone,c.fax,c.email,c.twitter_user,c.facebook_url,c.flickr_url,c.youtube_url,';
                $select_string .= 'u.first_name AS owner_first_name, u.last_name AS owner_last_name,';
                $select_string .= "cat.name AS category_name,";
                $select_string .= "event.id AS event_id,event.type AS event_type,event.name AS event_name,IF(event.type='task',event.due_date,event.start_time) AS event_due_date";

                $query->select($select_string);
                $query->from("#__crmery_companies as c");
                $query->leftJoin("#__crmery_users AS u on u.id = c.owner_id");
                $query->leftJoin("#__crmery_company_categories as cat on cat.id = c.category_id");
                $query->leftJoin("#__crmery_shared AS shared ON shared.item_id=c.id AND shared.item_type='company'");

            }else{

                $query->select('DISTINCT(c.id),c.*,cat.name AS category_name,event.id AS event_id,event.type AS event_type,event.name AS event_name,IF(event.type="task",event.due_date,event.start_time) AS event_due_date');
                $query->select("u.first_name AS owner_first_name,u.last_name AS owner_last_name");
                $query->from("#__crmery_companies as c");
                $query->leftJoin("#__crmery_users AS u on u.id = c.owner_id");
                $query->leftJoin("#__crmery_company_categories as cat on cat.id = c.category_id");
                $query->leftJoin("#__crmery_shared AS shared ON shared.item_id=c.id AND shared.item_type='company'");
							

            }

            if ( !$id ){
				
                //get current date
                $date = CrmeryHelperDate::formatDBDate(date('Y-m-d 00:00:00'));
                
                //filter for type
                if ( $type != null && $type != "all" ){
                    
                    //filter for deals//tasks due today
                    if ( $type == 'today' ){
                        $today = CrmeryHelperDate::formatDBDate(date('Y-m-d'),false)." 00:00:00";
                        $tomorrow = CrmeryHelperDate::formatDBDate(date('Y-m-d',strtotime($today." +1 days")),false)." 00:00:00";
                        $query->leftJoin("#__crmery_events_cf AS ecf ON ecf.association_type='company' AND ecf.association_id=c.id");
                        $query->leftJoin("#__crmery_events AS event ON event.id=ecf.event_id");
                        $query->where("( event.due_date > '$today' AND event.due_date < '$tomorrow' )");
                        $query->where("event.published>0 AND event.completed=0");
                    }
                    
                    //filter for companies and deals//tasks due tomorrow
                    else if ( $type == "tomorrow" ){
                        $tomorrow = CrmeryHelperDate::formatDBDate(date('Y-m-d 00:00:00',time() + (1*24*60*60)));
                        $dayAfterTomorrow = CrmeryHelperDate::formatDBDate(date("Y-m-d 00:00:00",strtotime($tomorrow." +1 days")));
                        $query->leftJoin("#__crmery_events_cf AS ecf ON ecf.association_type='company' AND ecf.association_id=c.id");
                        $query->leftJoin("#__crmery_events AS event ON event.id=ecf.event_id");
                        $query->where("event.due_date >'".$tomorrow."' AND event.due_date < '$dayAfterTomorrow'");
                        $query->where("event.published>0 AND event.completed=0");
                    } else {

                        //get next event
                        $query->leftJoin("#__crmery_events AS event ON event.id = (   
                                            SELECT event2.id 
                                            FROM #__crmery_events_cf AS ecf2
                                            LEFT JOIN #__crmery_events AS event2 ON 
                                                event2.id=ecf2.event_id AND 
                                                ecf2.association_type='company'
                                            WHERE ecf2.association_id=c.id AND
                                                  ecf2.association_type='company' AND
                                                  event2.completed=0
                                            ORDER BY CASE WHEN event2.type='task' THEN due_date END ASC,
                                                     CASE WHEN event2.type='event' THEN start_Time END ASC
                                            LIMIT 1 )");

                    }
                    
                    //filter for companies updated in the last 30 days
                    if ( $type == "updated_thirty" ){
                        $last_thirty_days = CrmeryHelperDate::formatDBDate(date('Y-m-d 00:00:00',time() - (30*24*60*60)));
                        $query->where("c.modified >'$last_thirty_days'");
                    }
                    
                     //filter for past companies// last contacted 30 days ago or longer
                    if ( $type == "past" ){
                        $last_thirty_days = CrmeryHelperDate::formatDBDate(date('Y-m-d 00:00:00',time() - (30*24*60*60)));
                        $query->where("c.modified <'$last_thirty_days'");
                    }
                    
                    //filter for recent companies
                    if ( $type == "recent" ) {
                        $last_thirty_days = CrmeryHelperDate::formatDBDate(date('Y-m-d 00:00:00',time() - (30*24*60*60)));
                        $query->where("c.modified >'$last_thirty_days'");
                    }
                    
                     $query->group("c.id");
                    
                } else {
				

                    //get next event
                    $query->leftJoin("#__crmery_events AS event ON event.id = (   
                                        SELECT event2.id 
                                        FROM #__crmery_events_cf AS ecf2
                                        LEFT JOIN #__crmery_events AS event2 ON 
                                            event2.id=ecf2.event_id AND 
                                            ecf2.association_type='company'
                                        WHERE ecf2.association_id=c.id AND
                                              ecf2.association_type='company' AND
                                              event2.completed=0
                                        ORDER BY CASE WHEN event2.type='task' THEN due_date END ASC,
                                                 CASE WHEN event2.type='event' THEN start_Time END ASC
                                        LIMIT 1 )");

                }

                $session = JFactory::getSession()->get('registry');
				
				

                /** company name filter **/
                $company_name = $session->get('Company.'.$view.'_name');
				
                if ( $company_name != null ){
                    $query->where("( c.name LIKE '%".$db->escape($company_name)."%' )");
                    $this->show_filters = 1;
                }

                /** company phone filter **/
                $company_phone = $session->get('Company.'.$view.'_phone');
                if ( $company_phone != null ){
                    $query->where("( c.phone LIKE '%".$db->escape($company_phone)."%' )");
                    $this->show_filters = 1;
                }

                /** company fax filter **/
                $company_fax = $session->get('Company.'.$view.'_fax');
                if ( $company_fax != null ){
                    $query->where("( c.fax LIKE '%".$db->escape($company_fax)."%' )");
                    $this->show_filters = 1;
                }

                //company email filter
                $company_email = $session->get('Company.'.$view.'_email');
                if ( $company_email != null ){
                    $query->where("( c.email LIKE '%".$db->escape($company_email)."%' )");
                    $this->show_filters = 1;
                }
                
                //company address filter
                $company_address = $session->get('Company.'.$view.'_address');
                if ( $company_address != null ){
                    $query->where("(    ( c.address_1 LIKE '%".$db->escape($company_address)."%' ) OR
                                        ( c.address_2 LIKE '%".$db->escape($company_address)."%' ) OR
                                        ( c.address_city LIKE '%".$db->escape($company_address)."%' ) OR
                                        ( c.address_state LIKE '%".$db->escape($company_address)."%' ) OR
                                        ( c.address_zip LIKE '%".$db->escape($company_address)."%' ) ) ");
                    $this->show_filters = 1;
                }
                
                //company country filter
                $company_country = $session->get('Company.'.$view.'_country');
                if ( $company_country != null ){
                    $query->where("( c.address_country LIKE '%".$db->escape($company_country)."%' )");
                    $this->show_filters = 1;
                }
                
                //company description filter
                $company_description = $session->get('Company.'.$view.'_description');
                if ( $company_description != null ){
                    $query->where("( c.description LIKE '%".$db->escape($company_description)."%' )");
                    $this->show_filters = 1;
                }               

            }  else {
			
                //get next event
                $query->leftJoin("#__crmery_events AS event ON event.id = (   
                                    SELECT event2.id 
                                    FROM #__crmery_events_cf AS ecf2
                                    LEFT JOIN #__crmery_events AS event2 ON 
                                        event2.id=ecf2.event_id AND 
                                        ecf2.association_type='company'
                                    WHERE ecf2.association_id=c.id AND
                                          ecf2.association_type='company' AND
                                          event2.completed=0
                                    ORDER BY CASE WHEN event2.type='task' THEN due_date END ASC,
                                             CASE WHEN event2.type='event' THEN start_Time END ASC
                                    LIMIT 1 )");

            }
			
            
            //search for specific companies
            if ( $id != null ){
                if ( is_array($id) ){
                    $query->where("c.id IN (".implode(',',$id).")");
                }else{
                    $query->where("c.id=$id");
                }
            }

            /** ---------------------------------------------------------------
             * Filter data using member role permissions
             */
            $member_id = CrmeryHelperUsers::getUserId();			
			
            $member_role = CrmeryHelperUsers::getRole();
            $team_id = CrmeryHelperUsers::getTeamId();
			
			if($member_role!="")  // CUSTOMIZING HERE  
			{
			
             if ( $id > 0 ){
			 	
                if ( $member_role == "manager" ){
                    //$query->where('( c.owner_id = '.$member_id.' OR u.team_id = '.$team_id." OR shared.user_id=".$member_id." OR c.public=1 )");
                }else if ( $member_role == "basic" ){
                    //$query->where(array('( c.owner_id = '.$member_id." OR shared.user_id=".$member_id." OR c.public=1 )"));
                }
            }else{
			
				
                if ( ( isset($user) && $user == "all" ) ){
					
                    if ( $member_role != 'exec'){
                         //manager filter
                        if ( $member_role == 'manager' ){
                            $query->where('( c.owner_id = '.$member_id.' OR u.team_id = '.$team_id." OR shared.user_id=".$member_id." OR c.public=1 )");
                        }else{
                        //basic user filter
                            $query->where('( c.owner_id = '.$member_id." OR shared.user_id=".$member_id."  OR c.public=1 )");
                        }
                    }
                }else if ( $team ){
				
                    $query->where("u.team_id=$team");
                }else if ( $user && $user != "all" ){
				
                    $query->where("c.owner_id=".$user);
                }else{
				
                    if ( !(isset($user)) ){							
					   
					   
					   if ( $member_role == 'manager' && $member_id == '193'){    // CUSTOMIZING HERE  						
						 //$member_id;
						 $db =& JFactory::getDbo();
                         $querys = $db->getQuery(true);
						 $querys->select("r.id")
								->from("#__crmery_companies AS r");
						 $querys->where("r.country_id = '81'");				
						 //echo $querys;		

						 $db->setQuery($querys);
						 $auids = $db->loadObjectList();
						 
						 //echo "<pre>"; print_r($auids); echo "</pre>";
						 if ( count($auids) > 0 )
						 {
						 	foreach ( $auids as $auid )
							{						
								$comId=$auid->id;						
						 	}
						 }		
						
						$query->where("c.id='$comId' or c.country_id='81'"); 
						//echo $query;									
						}
					   elseif ( $member_role == 'manager' ){    // CUSTOMIZING HERE  						
						 //$member_id;
						 $db =& JFactory::getDbo();
                         $querys = $db->getQuery(true);
						 $querys->select("r.company_id")
								->from("#__crmery_users AS r");
						 $querys->where("r.id='$member_id'");						

						 $db->setQuery($querys);
						 $auids = $db->loadObjectList();
						 if ( count($auids) > 0 )
						 {
						 	foreach ( $auids as $auid )
							{						
								$comId=$auid->company_id;						
						 	}
						 }		
						
						$query->where("c.id='$comId'"); 
						//echo $query;									
						}
						
						else{			
					   
						if ( $member_role == 'exec' ){    // CUSTOMIZING HERE  						
						
						$db =& JFactory::getDbo();
                          $querys = $db->getQuery(true);
						   $querys->select("r.region_id,r.country_id")
							->from("#__crmery_users AS r");
						$querys->where("r.id='".$member_id."'");						

						$db->setQuery($querys);
						$regIds = $db->loadObjectList();
						
						if ( count($regIds) > 0 ){
							foreach ( $regIds as $regId ){						
								$regionId=$regId->region_id;						
							  }
						 }						
						 $query->where("(c.region_id IN(".$regionId.") or c.owner_id='".$member_id."')");   		
					   } 
				// IS code end 
					   
						}
						
                    }
                }
             }
			
          }
		  

            //filter based on category
            if ( $category && $category != "all" && $category > 0 ){
                $query->where("c.category_id=".$category);
            }

            if ( JRequest::getVar('advanced_filter') ){
                $this->show_filters = 1;
            }
            
            //set user state requests
            $query->order($this->getState('Company.filter_order') . ' ' . $this->getState('Company.filter_order_Dir'));

           // $query->where("c.published=".$this->published);
            $query->where("c.published>0");
            //echo $query."<br>";
          // echo '<br><br><br>query--->>'.$query;
			//return query object
            return $query;
                        
        }
		
		/*
		 * Method to access companies
		 * 
		 * @return mixed	 
		 */
		function getCompanies($id=null,$type=null,$user=null,$team=null){
			
            $this->_id = $id ? $id : $this->_id; 
            $this->_type = $type;
            $this->_user = $user;
            $this->_team = $team;
            
           
		   
		    //get session data
            $session = JFactory::getSession();
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            //get query string
            $query = $this->_buildQuery();
			
			//echo $query."<br>";
            
            /** ------------------------------------------
             * Set query limits and load results
             */
            
            if(!CrmeryHelperTemplate::isMobile()) {
                $limit = $this->getState($this->_view.'_limit');
                $limitStart = $this->getState($this->_view.'_limitstart');
                if (  !$this->_id && $limit != 0 ){
                    if ( $limitStart >= $this->getTotal() ){
                        $limitStart = 0;
                        $limit = 10;
                        $limitStart = ($limit != 0) ? (floor($limitStart / $limit) * $limit) : 0;
                        $this->setState($this->_view.'_limit', $limit);
                        $this->setState($this->_view.'_limitstart', $limitStart);
                    }
                    $query .= " LIMIT ".($limit)." OFFSET ".($limitStart);
                }
            }
            
			//echo $query;
            //run query and grab results of companies
            $db->setQuery($query);
            $companies = $db->loadAssocList();
			
            
            //generate query to join people
            if ( count($companies) ){

                $export = JRequest::getVar('export');

                if ( !$export ){

                    foreach ( $companies as $key => $company ) {

                        if ( $this->_id ){

                            /* Tweets */
                            if($company['twitter_user']!="" && $company['twitter_user']!=" ") {
                                $companies[$key]['tweets'] = CrmeryHelperTweets::getTweets($company['twitter_user']);
                            }

                            //generate people query
                            $contactModel = JModel::getInstance('Contacts','CrmeryModel');
                            $contactModel->set('company_id',$company['id']);
                            $companies[$key]['people'] = $contactModel->getContacts();
                            
                            //generate deal query
                            $dealModel = JModel::getInstance('Deal','CrmeryModel');
                            $dealModel->set('company_id',$company['id']);
                            $dealModel->set('user_override','all');
                            $deals = $dealModel->getDeals();
                            $companies[$key]['pipeline'] = 0;
                            $companies[$key]['won_deals'] = 0;
                            for($i=0;$i<count($deals);$i++) {
                                $deal = $deals[$i];
                                $companies[$key]['pipeline'] += $deal['amount'];
                                if($deal['percent']==100) {
                                    $companies[$key]['won_deals'] += $deal['amount'];                            
                                }
                            }   
                            $companies[$key]['deals'] = $deals;

                        

                            //Get Associated Notes
                            $notesModel =& JModel::getInstance('Note','CRMeryModel');
                            $companies[$key]['notes'] = $notesModel->getNotes($company['id'],'company');
                        
                            // Get Associated Documents
                            $documentModel = JModel::getInstance('Document','CrmeryModel');
                            $documentModel->set('company_id',$company['id']);
                            $companies[$key]['documents']  = $documentModel->getDocuments();

                        }
                        
                        $companies[$key]['address_formatted'] = ( strlen($company['address_1']) > 0 ) ? $company['address_1'].
                                                                $company['address_2'].", ".
                                                                $company['address_city'].' '.$company['address_state'].', '.$company['address_zip'].
                                                                ' '.$company['address_country'] : "";
                    }

                }
                
            } 
			
			//echo "<pre>"; print_r($companies); echo "</pre>";

            $dispatcher =& JDispatcher::getInstance();
            $dispatcher->trigger('onCompanyLoad',array(&$companies));      

            //return results
            return $companies;            
		}

        /**
         * method to get list of companies
         */
        
        function getCompanyList($company_name=null){
            
            //db object
            $db =& JFactory::getDBO();
            //gen query
            $query = $db->getQuery(true);
            $query->select("DISTINCT(c.id),c.name,c.id");
            $query->from("#__crmery_companies AS c");
            $query->leftJoin("#__crmery_users AS u on u.id = c.owner_id");
            $query->leftJoin("#__crmery_shared AS shared ON shared.item_id=c.id AND shared.item_type='company'");

            //member access roles
            $member_id = CrmeryHelperUsers::getUserId();
            $member_role = CrmeryHelperUsers::getRole();
            $team_id = CrmeryHelperUsers::getTeamId();
            if ( $member_role == "manager" ){
                $query->where('( u.team_id = '.$team_id." OR shared.user_id=".$member_id." OR c.public=1 )");
            }else if ( $member_role == "basic" ){
                $query->where(array('(c.owner_id = '.$member_id." OR shared.user_id=".$member_id." OR c.public=1  )"));
            }


            if ( $company_name ){
                $company_name = ucwords($company_name);
                $query->where("LOWER(c.name) LIKE '%".$db->escape($company_name)."%'");
            }

            $query->where("c.published=".$this->published);

            //set query
            $db->setQuery($query);
            //load list
            $row = $db->loadAssocList();

            //return results
            return $row;
            
        }

        function getCompanyNames($json=FALSE){
            $names = $this->getCompanyList();
            $return = array();
            if ( count($names) > 0 ){
                foreach ( $names as $key => $name ){
                    $return[] = array('label'=>$name['name'],'value'=>$name['id']);
                }   
            }
            return $json ? json_encode($return) : $return;
        }

        /**
         * Checks for existing company by name
         * @param  [var] $name company name to check
         * @return [int]       ID of existing company
         */
        function checkCompanyName($name) 
        {
            $db = JFactory::getDBO();
            $query = $db->getQuery(true);
            $query->select('c.id');
            $query->from('#__crmery_companies AS c');
            $query->where('LOWER(c.name) = "'.$db->escape(strtolower($name)).'"');
            $db->setQuery($query);
            $existingCompany = $db->loadResult();

            return $existingCompany;
        }

        function getCompanyName($idOrName){
            $db = JFactory::getDBO();
            $query = $db->getQuery(true);
            $query->select('c.name');
            $query->from('#__crmery_companies AS c');
            $query->where("c.id='".$idOrName."' OR c.name='".$db->escape($idOrName)."'");
            $db->setQuery($query);
            $company_name = $db->loadResult();

            return $company_name;
        }

        /**
         * Get total number of rows for pagination
         */
        function getTotal() {
          if ( empty ( $this->_total ) ){
              $query = $this->_buildQuery();
              $this->_total = $this->_getListCount($query);
              $this->_total = $this->_total ? $this->_total : 0;
          }
          return $this->_total;
       }

        /**
         * Generate pagination
         */
        function getPagination() {
          // Lets load the content if it doesn't already exist
         // if (empty($this->_pagination)) {
             $total = $this->getTotal();
             $total = $total ? $total : 0;
             $this->_pagination = new CrmeryPagination( $total, $this->getState($this->_view.'_limitstart'), $this->getState($this->_view.'_limit'),'',JRoute::_('index.php?option=com_crmery&view=companies'));
          // }
          return $this->_pagination;
        }

        function getEmail($company_id){

            $db = JFactory::getDBO();
            $query = $db->getQuery(true);
            $query->select('c.email');
            $query->from('#__crmery_companies AS c');
            $query->where('c.id='.$company_id);
            $db->setQuery($query);
            $uid = $db->loadResult();

            return $uid;

        }

        /**
         * Populate user state requests
         */
        function populateState(){
            //get states
            $app = JFactory::getApplication();
            
            //determine view so we set correct states
            $view = JRequest::getCmd('view');
            $layout = str_replace("_filter","",JRequest::getCmd('layout'));
            
            // Get pagination request variables
            $limit = $app->getUserStateFromRequest($view.'_limit','limit',10);
            $limitstart = $app->getUserStateFromRequest($view.'_limitstart','limitstart',0);
            
            // In case limit has been changed, adjust it
            $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
     
            $this->setState($view.'_limit', $limit);
            $this->setState($view.'_limitstart', $limitstart);
            
            //set default filter states for reports
            $filter_order           = $app->getUserStateFromRequest('Company.filter_order','filter_order','c.name');
            $filter_order_Dir       = $app->getUserStateFromRequest('Company.filter_order_Dir','filter_order_Dir','asc');

            $filters = array('name'=>'company_name','phone'=>'company_phone','fax'=>'fax','email'=>'email','address'=>'address','country'=>'country','description'=>'description');
            $reset = JRequest::getVar('reset_filters',false);
            foreach ( $filters as $stateVar => $postVar )
            {   
                if ( $reset )
                {
                    $app->setUserState('Company.'.$view.'_'.$stateVar,null);
                }
                else
                {
                    $val = $app->getUserStateFromRequest('Company.'.$view.'_'.$stateVar,$postVar,null);
                    $app->setUserState('Company.'.$view.'_'.$stateVar,$val);
                }
            }
            
            //set states for reports
            $this->setState('Company.filter_order',$filter_order);
            $this->setState('Company.filter_order_Dir',$filter_order_Dir);
        }
		
}