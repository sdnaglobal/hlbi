<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.model');

class CrmeryModelStats extends JModel
{

    var $person_id = null;
    var $access = null;
    var $users = null;
    var $today = null;
    var $previousDay = null;

    public function __construct(){
        $this->previousDay = CrmeryHelperDate::formatDBDate(date('Y-m-d')." - 1 day");
        $this->today = CrmeryHelperDate::formatDBDate(date('Y-m-d'));
    	$this->access = CrmeryHelperUsers::getRole($this->person_id);
    }

    public function getDistinctEntries($type,$field){

        $results = array();
        $this->users = $this->getUsers($this->person_id,$this->access);

        if ( $this->users && is_array($this->users) && count($this->users) > 0 )
        {

            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);

            $query->select("DISTINCT h.type_id");
            $query->from("#__crmery_history AS h");
            $query->where("h.field='".$field."' AND h.type='".$type."'");
            $query->where("(h.date >= '".$this->previousDay."' AND h.date < '".$this->today."')");
            $query->where("h.user_id IN(".implode(',',$this->users).")");
            $db->setQuery($query);
            $results = $db->loadResultArray();

        }

        return $results;

    }

    public function joinField($ids,$type,$field){
        $results = array();

        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);

        if ( count ( $ids ) > 0 ){
            foreach ( $ids as $id ){
                $query->clear();
                $query->select("h.type_id,".$type.".*,h.new_value");
                $query->from("#__crmery_history AS h");
                $query->leftJoin("#__crmery_".$type." AS ".$type." ON ".$type.".id = h.type_id AND ".$type.".published=1");
                $query->where("h.type_id=".$id);
                $query->where("(h.date >= '".$this->previousDay."' AND h.date < '".$this->today."')");
                $query->where("h.field='".$field."'");
                $query->order("h.date DESC LIMIT 1");
                $db->setQuery($query);
                $results[] = $db->loadObject();

            }
        }

        return $results;
    }

    public function getUsers($user_id,$user_role){

        if( $user_role != 'basic' ){
            
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            
            $query->select("id");
            $query->from("#__crmery_users");

            //if manager
            if ( $user_role == "manager" ){
            	$team_id = CrmeryHelperUsers::getTeamId($user_id);
                $query->where('team_id='.$team_id);
            }
            //if exec there is no where clause, load all users
            
            //load results
            $db->setQuery($query);
            $results = $db->loadResultArray();

        }else{
        	$results = array(0=>$user_id);
        }

        return $results;
    }


    public function getActiveDealsAmount(){

    	/** get unique history **/
        $deal_ids = $this->getDistinctEntries('deal','stage_id');
        $result = 0;

        if ( $deal_ids && is_array($deal_ids) && count($deal_ids) > 0 ){

            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);

            $query->clear();
            $query->select("SUM(d.amount)");
            $query->from("#__crmery_deals AS d");
            $query->where("d.id IN(".implode(',',$deal_ids).')');
            $query->where("(h.date >= '".$this->previousDay."' AND h.date < '".$this->today."')");
            $query->where("d.published=1");

        	$db->setQuery($query);
        	$result = $db->loadResult();

        }

    	return $result;

    }

    public function getStages(){

		$db =& JFactory::getDBO();
    	$query = $db->getQuery(true);

        /** Select distinct history entries **/
        $results = $this->getDistinctEntries('deal','stage_id');

        /** Get most recent entry from the above **/
        $deals = $this->joinField($results,'deals','stage_id');

        /** Merge with all possible stages **/
        $query->clear();
        $query->select("s.name,s.color,s.id,0 AS amount");
        $query->from("#__crmery_stages AS s");
        $db->setQuery($query);
        $stages = $db->loadAssocList('id');

        /** Sum amounts from above **/
        if ( count ($deals) > 0 ){
            foreach ( $deals as $deal ){
                if ( array_key_exists($deal->new_value,$stages) ){
                    $stages[$deal->new_value]['amount'] += $deal->amount;
                }
            }   
        }

        usort($stages,'self::sortAmount');

    	return $stages;

    }

    function sortAmount($a,$b) {
          return $a['amount']<$b['amount'];
    }

    public function getLeads(){

    	$db =& JFactory::getDBO();
    	$query = $db->getQuery(true);

    	/** person ids **/
        $person_ids = $this->getDistinctEntries('person','type');
        $people = $this->joinField($person_ids,'people','type');
        $leads = array('lead'=>0,'contact'=>0);
        if ( count($people) > 0 ){
            foreach ( $people as $person ){
                $leads[$person->type]++;
            }
        }

    	return $leads;

    }

    public function getNotes(){

        $note_ids = $this->getDistinctEntries('note','id');
        $totals = array();

        if ( $note_ids && is_array($note_ids) && count($note_ids) > 0 )
        {

            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);

            $query->clear();
            $query->select("c.*");
            $query->from("#__crmery_notes_categories AS c");

            $query->order('c.ordering');

            $db->setQuery($query);

            $categories = $db->loadAssocList();

            if ( count($categories) > 0 ){
                foreach ( $categories as $category ){
                    $query->clear();
                    $query->select("COUNT(n.id)");
                    $query->from("#__crmery_notes AS n");
                    $query->where("n.category_id = ".$category['id']);
                    $query->where("n.id IN(".implode(',',$note_ids).")");
                    $query->where("n.published=1");
                    $db->setQuery($query);
                    $totals[$category['name']] = $db->loadResult();
                }
            }

        }

        return $totals;

    }

    public function getTodos(){

        $events = $this->getDistinctEntries('event','id');
        $totals = array();

        if ( $events && is_array($events) && count($events) > 0 )
        {

            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);

            $query->clear();
            $query->select("c.*");
            $query->from("#__crmery_events_categories AS c");
            
            $query->order("c.ordering");

            $db->setQuery($query);

            $categories = $db->loadAssocList();

            if ( count($categories) > 0 ){
                foreach ( $categories as $category ){
                    $query->clear();
                    $query->select("COUNT(e.id) AS total,SUM(e.completed) AS completed");
                    $query->from("#__crmery_events AS e");
                    $query->where("e.category_id = ".$category['id']);
                    $query->where("e.id IN(".implode(',',$events).")");
                    $query->where("e.published=1");
                    $db->setQuery($query);
                    $results = $db->loadObject();
                    if ( count($results) > 0 ){
                        $totals[$category['name']] = $results;
                    }else{
                        $totals[$category['name']] = new stdClass;
                        $totals[$category['name']]->total = 0;
                        $totals[$category['name']]->completed = 0;
                    }

                }
            }

        }

        return $totals;

    }

    public function getDealActivity(){

    }



}