<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryModelNote extends JModel
{
        
        var $_id = null;
        var $published = 1;
        var $public_notes = null;
        var $_view = "Report.notes";

        /**
         * 
         *
         * @access  public
         * @return  void
         */
        function __construct()
        {
            parent::__construct();
            
        }
        
        /**
         * Method to store a record
         *
         * @return    boolean    True on success
         */
        function store($data=null)
        {

            //Load Tables
            $row =& JTable::getInstance('note','Table');
            $oldRow =& JTable::getInstance('note','Table');

            if ( $data == null ){
                $data = JRequest::get( 'post' );
            }

            if ( array_key_exists('name',$data) ){
                $data['name'] = trim($data['name']);
            }

            if ( array_key_exists('is_email',$data) ){
                $model =& JModel::getInstance('mail','CrmeryModel');
                $email = $model->getEmail($data['email_id']);
                $data['note'] = $email;
            }

            /** check for and automatically associate and create primary contacts or people **/
            if ( array_key_exists('person_name',$data) && $data['person_name'] != "" ){
                $peopleModel =& JModel::getInstance('people','CrmeryModel');
                $existingPerson = $peopleModel->checkPersonName($data['person_name']);

                if($existingPerson=="") {
                    $pdata = array();
                    $name = explode(" ",$data['person_name']);
                    $pdata['first_name'] = array_key_exists(0,$name) ? $name[0] : "";
                    $pdata['last_name'] = array_key_exists(1,$name) ? $name[1] : "";
                    $data['person_id'] = $peopleModel->store($pdata);
                } else {
                    $data['person_id'] = $existingPerson;
                }

            }

            /** check for and automatically associate and create deals **/
            if ( array_key_exists('deal_name',$data) && $data['deal_name'] != "" ){
                $dealModel =& JModel::getInstance('deal','CrmeryModel');
                $existingDeal = $dealModel->checkDealName($data['deal_name']);

                if($existingDeal=="") {
                    $pdata = array();
                    $pdata['name'] = $data['deal_name'];
                    $data['deal_id'] = $dealModel->store($pdata);
                } else {
                    $data['deal_id'] = $existingDeal;
                }

            }
            
            //date generation
            $date = CrmeryHelperDate::formatDBDate(date('Y-m-d H:i:s'));
            
            if ( !array_key_exists('id',$data) ){
                $data['created'] = $date;
                $status = "created";
            } else {
                $row->load($data['id']);
                $oldRow->load($data['id']);
                $status = "updated";
            }
            
            $data['modified'] = $date;
            $data['owner_id'] = CrmeryHelperUsers::getUserId();
            
            // Bind the form fields to the table
            if (!$row->bind($data)) {
                $this->setError($this->_db->getErrorMsg());
                return false;
            }

            $dispatcher =& JDispatcher::getInstance();
            $dispatcher->trigger('onBeforeNoteSave', array(&$row));            
         
            // Make sure the record is valid
            if (!$row->check()) {
                $this->setError($this->_db->getErrorMsg());
                return false;
            }
         
            // Store the web link table to the database
            if (!$row->store()) {
                $this->setError($this->_db->getErrorMsg());
                return false;
            }

            if ( array_key_exists('id',$data) ){
                $id = $data['id'];
            }else{
                $id = $this->_db->insertId();
            }
            

            CrmeryHelperActivity::saveActivity($oldRow, $row,'note', $status);


            //Store email attachments
            if ( array_key_exists('is_email',$data) ){
                $model =& JModel::getInstance("mail",'CrmeryModel');
                $model->storeAttachments($data['email_id'], $data['person_id']);
            }

            $dispatcher =& JDispatcher::getInstance();
            $dispatcher->trigger('onAfterNoteSave', array(&$row));
            
            return $id;
        }
        
        
        /*
         * Method to access a note
         * 
         * @return array     
         */
        function getNote($id){
            
            //grab db
            $db =& JFactory::getDBO();
            
            //initialize query
            $query = $db->getQuery(true);
            
            //gen query string
            $query->select("n.*,cat.name as category_name,comp.name as company_name,deal.name as deal_name,"
                        ."person.first_name as person_first_name,person.last_name as person_last_name,"
                        ."owner.first_name as owner_first_name, owner.last_name as owner_last_name");
            $query->from("#__crmery_notes as n");
            $query->leftJoin("#__crmery_notes_categories AS cat ON cat.id = n.category_id");
            $query->leftJoin("#__crmery_companies AS comp ON comp.id = n.company_id AND comp.published>0");
            $query->leftJoin("#__crmery_deals AS deal ON deal.id = n.deal_id AND deal.published>0");
            $query->leftJoin("#__crmery_people AS person on person.id = n.person_id AND person.published>0");
            $query->leftJoin('#__crmery_users AS owner ON owner.id = n.owner_id');
            $query->where("n.id=".$id);
            $query->where("n.published=".$this->published);
            
            //load results
            $db->setQuery($query);
            $results = $db->loadAssocList();
            
            //clean results
            if ( count ( $results ) > 0 ){
                foreach ( $results as $key => $note ){
                    $results[$key]['created_formatted'] = CrmeryHelperDate::formatDate($note['created']);
                }
            }

            $dispatcher =& JDispatcher::getInstance();
            $dispatcher->trigger('onNoteLoad', array(&$results));            
            
            //return results
            return $results;
        }

        function _buildQuery($object_id = NULL,$type = NULL, $display =  true)
        {

            //grab db
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);

            //gen query string
            $query->select("n.*,cat.name as category_name,comp.name as company_name,
                            comp.id as company_id,deal.name as deal_name,deal.id as deal_id,
                            person.id as person_id,person.first_name as person_first_name,
                            person.last_name as person_last_name, owner.first_name as owner_first_name,
                            event.name as event_name, event.id as event_id,
                            owner.last_name as owner_last_name"); 
            $query->from("#__crmery_notes as n");
            $query->leftJoin("#__crmery_notes_categories AS cat ON cat.id = n.category_id");
            $query->leftJoin("#__crmery_companies AS comp ON comp.id = n.company_id AND comp.published>0");
            $query->leftJoin("#__crmery_events AS event ON event.id = n.event_id AND event.published>0");
            $query->leftJoin("#__crmery_deals AS deal ON deal.id = n.deal_id AND deal.published>0");
            $query->leftJoin("#__crmery_people AS person on n.person_id = person.id AND person.published>0");
            $query->leftJoin("#__crmery_users AS owner ON n.owner_id = owner.id");
            
            //filters
            //company
            $view = JRequest::getVar('view');
            $layout = str_replace('_filter','',JRequest::getVar('layout'));

            $company_filter = $this->getState('Note.'.$view.'.'.$layout.'.company_name');
            if ( $company_filter != null ){
                $query->where("comp.name LIKE '%".$company_filter."%'");
            }
            //deal
            $deal_filter = $this->getState('Note.'.$view.'.'.$layout.'.deal_name');
            if ( $deal_filter != null ){
                $query->where("deal.name LIKE '%".$deal_filter."%'");
            }
            //person
             $person_filter = $this->getState('Note.'.$view.'.'.$layout.'.person_name');
            if ( $person_filter != null ){
                
            }

            if($object_id) {
                switch($type) {
                    case 'person':
                        $query->where('n.person_id ='.$object_id);
                    break;

                    case 'company':
                        $query->where('(n.company_id ='.$object_id.' OR deal.company_id = '.$object_id.' OR person.company_id = '.$object_id.")");
                    break;

                    case 'deal':
                        $query->where('n.deal_id='.$object_id);
                    break;
                    case "event":
                        $query->where("n.event_id=$object_id");
                    break;
                }
            }

            //owner
            $owner_filter = $this->getState('Note.'.$view.'.'.$layout.'.owner_id');
            if ( $owner_filter != null && $owner_filter != "all" ){
                $owner_type = $this->getState('Note.'.$view.'.'.$layout.'.owner_type');
                switch($owner_type){
                    case "team":
                        $team_member_ids = CrmeryHelperUsers::getTeamUsers($owner_filter,TRUE);
                        $query->where("n.owner_id IN (".implode(',',$team_member_ids).")");
                    break;
                    case "member":
                        $query->where("n.owner_id=".$owner_filter);
                    break;
                }
            }

            //created
             $created_filter = $this->getState('Note.'.$view.'.'.$layout.'.created');
            if ( $created_filter != null ){

                $date = date("Y-m-d H:i:s");
                if ( $created_filter == "this_week" ){
                    $date_info = getDate(strtotime($date));
                    $today = $date_info['wday'];
                    $days_to_remove = -1 + $today;
                    $days_to_add = 7 - $today;
                    $beginning_of_week = CrmeryHelperDate::formatDBDate(date('Y-m-d 00:00:00',strtotime($date." - $days_to_remove days")));
                    $end_of_week = CrmeryHelperDate::formatDBDate(date('Y-m-d 00:00:00',strtotime($date." + $days_to_add days")));
                    $query->where("n.modified >= '$beginning_of_week'");
                    $query->where("n.modified < '$end_of_week'");
                }
                
                if ( $created_filter == "last_week" ){
                    $date_info = getDate(strtotime($date));
                    $today = $date_info['wday'];
                    $days_to_remove = -1 + $today;
                    $beginning_of_this_week = CrmeryHelperDate::formatDBDate(date('Y-m-d 00:00:00',strtotime($date." - $days_to_remove days")));
                    $beginning_of_last_week = CrmeryHelperDate::formatDBDate(date('Y-m-d 00:00:00',strtotime($date." - 7 days")));
                    $query->where("n.created >= '$beginning_of_last_week'");
                    $query->where("n.created < '$beginning_of_this_week'");
                }
                
                if ( $created_filter == "this_month" ){
                    $this_month = CrmeryHelperDate::formatDBDate(date('Y-m-1 00:00:00'));
                    $next_month = date('Y-m-1 00:00:00', strtotime($date." +1 month"));
                    $query->where("n.created >= '$this_month'");
                    $query->where("n.created < '$next_month'");
                }
                
                if ( $created_filter == "last_month" ){
                    $this_month = CrmeryHelperDate::formatDBDate(date('Y-m-1 00:00:00'));
                    $last_month = date('Y-m-1 00:00:00', strtotime($date." -1 month"));
                    $query->where("n.created >= '$last_month'");
                    $query->where("n.created < '$this_month'");
                }
                
                if ( $created_filter == "today" ){
                    $today = CrmeryHelperDate::formatDBDate(date("Y-m-d 00:00:00"));
                    $tomorrow = date('Y-m-d 00:00:00', strtotime(date("Y-m-d", strtotime($date)) . "+1 day"));
                    $query->where("n.created >= '$today'");
                    $query->where("n.created < '$tomorrow'");
                }
                
                if ( $created_filter == "yesterday" ){
                    $today = CrmeryHelperDate::formatDBDate(date("Y-m-d 00:00:00"));
                    $yesterday = date('Y-m-d 00:00:00', strtotime(date("Y-m-d", strtotime($date)) . "-1 day"));
                    $query->where("n.created >= '$yesterday'");
                    $query->where("n.created < '$today'");
                }
                
            }

            //category
             $category_filter = $this->getState('Note.'.$view.'.'.$layout.'.category_id');
            if ( $category_filter != null ){
                $query->where("n.category_id=".$category_filter);
            }

             if ( $this->_id ){
                if ( is_array($this->_id) ){
                    $query->where("n.id IN (".implode(',',$this->_id).")");
                }else{
                    $query->where("n.id=$this->_id");
                }
            }

            /** ---------------------------------------------------------------
             * Filter data using member role permissions
             */
            $member_id = CrmeryHelperUsers::getUserId();
            $member_role = CrmeryHelperUsers::getRole();
            $team_id = CrmeryHelperUsers::getTeamId();
            if ( $this->public_notes != true ){
                if ( $member_role != 'exec'){
                     //manager filter
                    if ( $member_role == 'manager' ){
                        $query->where('owner.team_id = '.$team_id);
                    }else{
                    //basic user filter
                        $query->where(array('n.owner_id = '.$member_id));
                    }
                }
            }

            if ( $this->getState('Note.'.$view.'.'.$layout.'.filter_order') != null )
                $query->order($this->getState('Note.'.$view.'.'.$layout.'.filter_order') . ' ' . $this->getState('Note.'.$view.'.'.$layout.'.filter_order_Dir'));

            $query->where("n.published=".$this->published);
            $query->order("n.modified DESC");

            return $query;
        }
        
        /*
         * Method to access notes
         * 
         * @return array     
         */
        function getNotes($object_id = NULL,$type = NULL, $display = true){
            
            //grab db
            $db =& JFactory::getDBO();
            
            //initialize query
            $query = $this->_buildQuery($object_id,$type,$display);

            $view = JRequest::getVar('view');
            if(!CrmeryHelperTemplate::isMobile()) {
                $limit = $this->getState($view.'_limit');
                $limitStart = $this->getState($view.'_limitstart');
                if (  !$this->_id && $limit != 0 ){
                    if ( $limitStart >= $this->getTotal() ){
                        $limitStart = 0;
                        $limit = 10;
                        $limitStart = ($limit != 0) ? (floor($limitStart / $limit) * $limit) : 0;
                        $this->setState($view.'_limit', $limit);
                        $this->setState($view.'_limitstart', $limitStart);
                    }
                }
            }

            //load results
            $db->setQuery($query,$limitStart,$limit);
            $results = $db->loadAssocList();

            //clean results
            if(count($results) > 0) {
                foreach ( $results as $key => $note ){
                    $results[$key]['created_formatted'] = CrmeryHelperDate::formatDate($note['created']);
                }
            }

            $dispatcher =& JDispatcher::getInstance();
            $dispatcher->trigger('onNoteLoad', array(&$results));            

            if(!$display) {
                //return results
                return $results;
            } else {

                $config = array('name'=>'note');
                $notesView = new CrmeryHelperView($config);
                $notesView->assignRef('notes',$results);

                return $notesView;
            }
        }
        
        /*
         * method to get list of deals
         */
        
        function getNoteCategories(){
            
            //db object
            $db =& JFactory::getDBO();
            //gen query
            $query = $db->getQuery(true);
            $query->select("name,id FROM #__crmery_notes_categories");
            $query->order('ordering');
            //set query
            $db->setQuery($query);
            //load list
            $row = $db->loadAssocList();
            $blank = array(array('name'=>CRMText::_('COM_CRMERY_NONE'),'id'=>0));
            $return = array_merge($blank,$row);
            //return results
            return $return;
            
        }
        
        function populateState(){

            $app = JFactory::getApplication();
            $view = JRequest::getVar('view');
            $layout = str_replace('_filter','',JRequest::getVar('layout'));

            // Get pagination request variables
            $limit = $app->getUserStateFromRequest($view.'_limit','limit',10);
            $limitstart = $app->getUserStateFromRequest($view.'_limitstart','limitstart',0);
            
            // In case limit has been changed, adjust it
            $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
            $this->setState($view.'_limit', $limit);
            $this->setState($view.'_limitstart', $limitstart);

            //get states
            $filter_order = $app->getUserStateFromRequest('Note.'.$view.'.'.$layout.'.filter_order','filter_order','comp.name');
            $filter_order_Dir = $app->getUserStateFromRequest('Note.'.$view.'.'.$layout.'.filter_order_Dir','filter_order_Dir','asc');
            
            //set default filter states
            $company_filter = $app->getUserStateFromRequest('Note.'.$view.'.'.$layout.'.company_name','company_name',null);
            $deal_filter = $app->getUserStateFromRequest('Note.'.$view.'.'.$layout.'.deal_name','deal_name',null);
            $person_filter = $app->getUserStateFromRequest('Note.'.$view.'.'.$layout.'.person_name','person_name',null);
            $owner_filter = $app->getUserStateFromRequest('Note.'.$view.'.'.$layout.'.owner_id','owner_id',null);
            $owner_type = $app->getUserStateFromRequest('Note.'.$view.'.'.$layout.'.owner_type','owner_type',null);
            $created_filter = $app->getUserStateFromRequest('Note.'.$view.'.'.$layout.'.created','created',null);
            $category_filter = $app->getUserStateFromRequest('Note.'.$view.'.'.$layout.'.category_id','category_id',null);
            
            //set states
            $this->setState('Note.'.$view.'.'.$layout.'.filter_order', $filter_order);
            $this->setState('Note.'.$view.'.'.$layout.'.filter_order_Dir',$filter_order_Dir);
            $this->setState('Note.'.$view.'.'.$layout.'.company_name',$company_filter);
            $this->setState('Note.'.$view.'.'.$layout.'.deal_name',$deal_filter);
            $this->setState('Note.'.$view.'.'.$layout.'.person_name',$person_filter);
            $this->setState('Note.'.$view.'.'.$layout.'.owner_id',$owner_filter);
            $this->setState('Note.'.$view.'.'.$layout.'.owner_type',$owner_type);
            $this->setState('Note.'.$view.'.'.$layout.'.created',$created_filter);
            $this->setState('Note.'.$view.'.'.$layout.'.category_id',$category_filter);
        }

        /**
         * Get total number of rows for pagination
         */
        function getTotal() {
          if ( empty ( $this->_total ) ){
              $query = $this->_buildQuery();
              $this->_total = $this->_getListCount($query);
              $this->_total = $this->_total ? $this->_total : 0;
          }
          return $this->_total;
       }

        function getPagination() {
          // Lets load the content if it doesn't already exist
         // if (empty($this->_pagination)) {
             $view = JRequest::getVar('view');
             $total = $this->getTotal();
             $total = $total ? $total : 0;
             $this->_pagination = new CrmeryPagination( $total, $this->getState($view.'_limitstart'), $this->getState($view.'_limit'),'',JRoute::_('index.php?option=com_crmery&view=reports&layout=notes'));
          // }
          return $this->_pagination;
        }
}
