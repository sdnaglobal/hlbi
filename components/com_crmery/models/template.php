<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryModelTemplate extends JModel
{
        
        /**
         * Constructor
         */
        function __construct() {
            parent::__construct();
        }

        /**
         * Run items through template system
         */
        public function createTemplate($associationId=null,$associationType=null){

                $template_id = JRequest::getVar('template_id');
                $association_id = $associationId ? $associationId : JRequest::getVar('association_id');
                $association_type = $associationType ? $associationType : JRequest::getVar('association_type');

                $template = $this->getTemplate($template_id);

                $current_date = date("Y-m-d 00:00:00");

                if ( count($template) > 0 ){

                        $event_model =& JModel::getInstance('event','CrmeryModel');

                        foreach ( $template as $event ){

                                unset($event['id']);

                                $event['association_id'] = $association_id;
                                $event['association_type'] = $association_type;
                                $event['category_id'] = $event['type'];
                                $event['type'] = "task";

                                $event['due_date'] = CrmeryHelperDate::formatDBDate(date("Y-m-d",strtotime($current_date." +".$event['day']." days")),false);
                                $event['due_date_hour'] = "00:00:00";

                                if ( !$event_model->store($event) ){
                                        return FALSE;
                                }
                        }       
                }

                return TRUE;

        }

        public function processDefaults($associationId=null,$associationType=null){

            $association_id = $associationId ? $associationId : JRequest::getVar('association_id');
            $association_type = $associationType ? $associationType : JRequest::getVar('association_type');

            $templates = $this->getDefaults($associationType);

            $current_date = date("Y-m-d 00:00:00");

            if ( count($templates) > 0 ){

                    foreach ( $templates as $templateInfo ){

                        $template = $this->getTemplate($templateInfo['id']);
                        JModel::addIncludePath(JPATH_SITE."/components/com_crmery/models/");
                        $event_model =& JModel::getInstance('event','CrmeryModel');

                        if ( is_array($template) ){
                            foreach ( $template as $event ){

                                    unset($event['id']);

                                    $event['association_id'] = $association_id;
                                    $event['association_type'] = $association_type;
                                    $event['category_id'] = $event['type'];
                                    $event['type'] = "task";

                                    $event['due_date'] = CrmeryHelperDate::formatDBDate(date("Y-m-d",strtotime($current_date." +".$event['day']." days")),false);
                                    $event['due_date_hour'] = "00:00:00";

                                    if ( !$event_model->store($event) ){
                                            return FALSE;
                                    }
                            }       
                        }

                    }
            }

            return TRUE;

        }

        public function getDefaults($associationType=null){

            $associationType = $associationType ? $associationType : JRequest::getVar("association_type");
            $db =& JFactory::getDbo();
            $query = $db->getQuery(true);

            switch ( $associationType ){
                case "people":
                case "person":
                    $type = "person";
                break;  
                case "deal":
                case "deals":
                    $type = "deal";
                break;
                case "company":
                case "companies":
                    $type = "company";
                break;
            }

            if ( isset($type) ){

                $query->select("t.*")
                    ->from("#__crmery_templates AS t")
                    ->where("t.type='".$type."'")
                    ->where("t.default=1");

                $db->setQuery($query);

                $templates = $db->loadAssocList();
                return $templates;

            }

            return array();

        }

        /**
         * Get template events
         * @param  [type] $template_id [description]
         * @return [type]              [description]
         */
        public function getTemplate($template_id){

                $db =& JFactory::getDBO();
                $query = $db->getQuery(TRUE);

                $query->select("t.*")
                        ->from("#__crmery_template_data AS t")
                        ->where("t.template_id=".$template_id);

                $db->setQuery($query);
                $events = $db->loadAssocList();

                return $events;

        }


}