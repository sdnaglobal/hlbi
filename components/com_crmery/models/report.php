<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryModelReport extends JModel
{

    var $published = 1;
	
	//	REFERRLAS STARTING AND TILL YEAR
	
	
	function getStartingLastYear()
	{
	
		$db =& JFactory::getDbo();
		$querysu = $db->getQuery(true);

		$querysu = "SELECT min(`created`) as start, max(`created`) as till  FROM `#__crmery_people` ";
		$db->setQuery($querysu);
		
		$yearAll = $db->loadAssoc();
		return $yearAll;
	}
	
	
	
	//	Region Reports
	/*function getRegionReport($billingYear)
	{
		$db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        $query = "Select T1.region_id, T1.Region_Name, T1.Received_Amount, T2.Referred_Amount From (Select r.region_id,r.region_name as Region_Name, COALESCE(round(sum(rp.SterlingAmount),2),0) as Received_Amount from `#__crmery_region` r left join #__crmery_companies co on co.region_id = r.region_id left join #__crmery_people p on p.company_id = co.id left join #__crmery_referral_payments rp on rp.referral_id = p.id where YEAR(STR_TO_DATE(rp.BillingYear, '%d/%m/%Y'))='2013' and rp.FeePaid=1 and p.published>0 and p.Accepted=1 group by r.region_id) T1 left join (Select r1.region_id,r1.region_name as Region_Name, COALESCE(round(sum(rp1.SterlingAmount),2),0)as Referred_Amount from #__crmery_region r1 left join #__crmery_companies co1 on co1.region_id = r1.region_id left join #__crmery_people p1 on p1.referring_firm_id = co1.id left join #__crmery_referral_payments rp1 on rp1.referral_id = p1.id where YEAR(STR_TO_DATE(rp1.BillingYear, '%d/%m/%Y'))='2013' and rp1.FeePaid=1 and p1.published>0 and p1.Accepted=1 group by r1.region_id) T2 ON T2.region_id = T1.region_id ";
		
		
        		
		$db->setQuery($query);
        $results = $db->loadAssocList();
        return $results;
		
	
	}*/
	
	
	function getRegionReport($billingYear)
	{
		$db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        $query = "SELECT reg.Region_Name,cl.Received_Amount, cr.Referred_Amount from #__crmery_region reg left outer join 

(Select r.region_id,r.region_name as Region_Name, COALESCE(round(sum(rp.SterlingAmount),2),0) as Received_Amount from `#__crmery_region` r left join  kjz42_crmery_currencies cu on cu.region_id = r.region_id left join #__crmery_companies co on co.country_id = cu.country_id left join #__crmery_people p on p.company_id = co.id left join #__crmery_referral_payments rp on rp.referral_id = p.id where rp.BillingYear LIKE '%$billingYear%' and rp.FeePaid=1 and p.published>0 and p.Accepted=1 group by r.region_id) cl on reg.region_id = cl.region_id

left outer join

(Select r1.region_id,r1.region_name as Region_Name, COALESCE(round(sum(rp1.SterlingAmount),2),0)as Referred_Amount from #__crmery_region r1 left join  kjz42_crmery_currencies cu1 on cu1.region_id = r1.region_id left join #__crmery_companies co1 on co1.country_id = cu1.country_id left join #__crmery_people p1 on p1.referring_firm_id = co1.id left join #__crmery_referral_payments rp1 on rp1.referral_id = p1.id where rp1.BillingYear LIKE '%$billingYear%' and rp1.FeePaid=1 and p1.published>0 and p1.Accepted=1 group by r1.region_id) cr on reg.region_id = cr.region_id order by reg.Region_Name  ";
		
		
        		
		$db->setQuery($query);
        $results = $db->loadAssocList();
        return $results;
		
	
	}
	
	
	
	
	//	Country Reports
	/*function getCountryReport($billingYear)
	{
		$db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        $query = "SELECT T1.country_name, T1.Referred_Amount, T2.Received_Amount From 
(select cu.country_name, COALESCE(round(sum(rp.SterlingAmount),2),0) as Referred_Amount from #__crmery_currencies cu left join #__crmery_companies c on c.country_id = cu.country_id left join #__crmery_people p on p.referring_firm_id = c.id left join #__crmery_referral_payments rp on rp.referral_ID = p.id where YEAR(STR_TO_DATE(rp.BillingYear, '%d/%m/%Y'))='2014' and rp.FeePaid=1 and p.published>0 and p.Accepted=1 group by cu.country_id) T1

left join (

select cu.country_name, COALESCE(round(sum(rp.SterlingAmount),2),0) as Received_Amount from #__crmery_currencies cu left join #__crmery_companies c on c.country_id = cu.country_id left join #__crmery_people p on p.company_id = c.id left join #__crmery_referral_payments rp on rp.referral_ID = p.id where YEAR(STR_TO_DATE(rp.BillingYear, '%d/%m/%Y'))='2014' and rp.FeePaid=1 and p.published>0 and p.Accepted=1 group by cu.country_id ) T2
on T1.country_name = T2.country_name ";
		
        		
		$db->setQuery($query);
        $results = $db->loadAssocList();
        return $results;
		
	
	}*/
	
	
	function getCountryReport($billingYear)
	{
		$db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        $query = "select cou.country_name,cl.Received_Amount, cr.Referred_Amount  from kjz42_crmery_currencies cou left outer join 

(select cu.country_id, cu.country_name, COALESCE(round(sum(rp.SterlingAmount),2),0) as Received_Amount from kjz42_crmery_currencies cu left join kjz42_crmery_companies c on c.country_id = cu.country_id left join kjz42_crmery_people p on p.company_id = c.id left join kjz42_crmery_referral_payments rp on rp.referral_ID = p.id where rp.BillingYear LIKE '%$billingYear%' and rp.FeePaid=1 and p.published>0 and p.Accepted=1 group by cu.country_id) cl on cou.country_id = cl.country_id 

left outer join

(select cu.country_id, cu.country_name, COALESCE(round(sum(rp.SterlingAmount),2),0) as Referred_Amount from kjz42_crmery_currencies cu left join kjz42_crmery_companies c on c.country_id = cu.country_id left join kjz42_crmery_people p on p.referring_firm_id = c.id left join kjz42_crmery_referral_payments rp on rp.referral_ID = p.id where rp.BillingYear LIKE '%$billingYear%' and rp.FeePaid=1 and p.published>0 and p.Accepted=1 group by cu.country_id ) cr on cou.country_id = cr.country_id order by cou.country_name ";
		
        		
		$db->setQuery($query);
        $results = $db->loadAssocList();
        return $results;
		
	
	}
	
	
	
	
	
	//	Firm Reports
	/*function getFirmReport($billingYear)
	{
		$db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        $query = "SELECT T1.name, T1.Referred_Amount, T2.Received_Amount from 
		
		(select c.name, sum(rp.SterlingAmount) as Referred_Amount from #__crmery_companies c left join #__crmery_people p on p.referring_firm_id = c.id left join #__crmery_referral_payments rp on rp.referral_ID = p.id where YEAR(STR_TO_DATE(rp.BillingYear, '%d/%m/%Y'))='2014' and rp.FeePaid=1 and p.published>0 and p.Accepted=1  group by c.name ) T1
		left join
		
		(select c2.name, sum(rp2.SterlingAmount) as Received_Amount from #__crmery_companies c2 left join #__crmery_people p2 on p2.company_id = c2.id left join #__crmery_referral_payments rp2 on rp2.referral_ID = p2.id where YEAR(STR_TO_DATE(rp2.BillingYear, '%d/%m/%Y'))='2014' and rp2.FeePaid=1 and p2.published>0 and p2.Accepted=1 group by c2.name ) T2 on T1.name = T2.name ";
		
        		
		$db->setQuery($query);
        $results = $db->loadAssocList();
        return $results;
		
	
	}*/
	
	
	function getFirmReport($billingYear)
	{
		$db =& JFactory::getDBO();
        $query = $db->getQuery(true);
		
		$member_id = CrmeryHelperUsers::getUserId();
        
        if($member_id == '193')
		{
			$where = " where comp.country_id=81";
			
		}
		else
		{
			$where = "";
			
		}
		
		
		
		$query = "Select comp.Id,comp.name,cl.Received_Amount, cr.Referred_Amount from kjz42_crmery_companies comp left outer join
  
(select c2.id, c2.name, COALESCE(round(sum(rp2.SterlingAmount),2),0) as Received_Amount from kjz42_crmery_companies c2 left join kjz42_crmery_people p2 on p2.company_id = c2.id left join kjz42_crmery_referral_payments rp2 on rp2.referral_ID = p2.id where rp2.BillingYear LIKE '%$billingYear%' and rp2.FeePaid=1 and p2.published>0 and p2.Accepted=1 group by c2.name) cl on comp.id=cl.id 
left outer join 
(select c.id, c.name, COALESCE(round(sum(rp.SterlingAmount),2),0) as Referred_Amount from kjz42_crmery_companies c left join kjz42_crmery_people p on p.referring_firm_id = c.id left join kjz42_crmery_referral_payments rp on rp.referral_ID = p.id where rp.BillingYear LIKE '%$billingYear%' and rp.FeePaid=1 and p.published>0 and p.Accepted=1 group by c.name)cr on comp.id=cr.id $where order by comp.name  ";

 		
		$db->setQuery($query);
        $results = $db->loadAssocList();
        return $results;
		
	
	}
	
	
	

}

