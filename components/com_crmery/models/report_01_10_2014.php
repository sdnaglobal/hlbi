<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryModelReport extends JModel
{

    var $published = 1;
	
	//	REFERRLAS STARTING AND TILL YEAR
	
	function getStartingLastYear()
	{
	
		$db =& JFactory::getDbo();
		$querysu = $db->getQuery(true);

		$querysu = "SELECT min(`created`) as start, max(`created`) as till  FROM `kjz42_crmery_people` ";
		$db->setQuery($querysu);
		
		$yearAll = $db->loadAssoc();
		return $yearAll;
	}
	
	
	
	//	Region Reports
	function getRegionReport($billingYear)
	{
		$db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        $query = "Select T1.region_id, T1.Region_Name, T1.Received_Amount, T2.Referred_Amount From (Select r.region_id,r.region_name as Region_Name, COALESCE(round(sum(rp.SterlingAmount),2),0) as Received_Amount from `kjz42_crmery_region` r left join kjz42_crmery_companies co on co.region_id = r.region_id left join kjz42_crmery_people p on p.assignee_id = co.partner_id left join kjz42_crmery_referral_payments rp on rp.referral_id = p.id where rp.BillingYear LIKE '%$billingYear%' group by r.region_id) T1 left join (Select r1.region_id,r1.region_name as Region_Name, COALESCE(round(sum(rp1.SterlingAmount),2),0)as Referred_Amount from kjz42_crmery_region r1 left join kjz42_crmery_companies co1 on co1.region_id = r1.region_id left join kjz42_crmery_people p1 on p1.company_id = co1.id left join kjz42_crmery_referral_payments rp1 on rp1.referral_id = p1.id where rp1.BillingYear LIKE '%$billingYear%' group by r1.region_id) T2 ON T2.region_id = T1.region_id ";
		
        		
		$db->setQuery($query);
        $results = $db->loadAssocList();
        return $results;
		
	
	}
	
	//	Country Reports
	function getCountryReport($billingYear)
	{
		$db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        $query = "SELECT T1.country_name, T1.Referred_Amount, T2.Received_Amount From 
(select cu.country_name, COALESCE(round(sum(rp.SterlingAmount),2),0) as Referred_Amount from kjz42_crmery_currencies cu left join kjz42_crmery_companies c on c.country_id = cu.country_id left join kjz42_crmery_people p on p.referring_firm_id = c.id left join kjz42_crmery_referral_payments rp on rp.referral_ID = p.id where rp.BillingYear LIKE '%$billingYear%' group by cu.country_id) T1

left join (

select cu.country_name, COALESCE(round(sum(rp.SterlingAmount),2),0) as Received_Amount from kjz42_crmery_currencies cu left join kjz42_crmery_companies c on c.country_id = cu.country_id left join kjz42_crmery_people p on p.company_id = c.id left join kjz42_crmery_referral_payments rp on rp.referral_ID = p.id where rp.BillingYear LIKE '%$billingYear%' group by cu.country_id ) T2
on T1.country_name = T2.country_name ";
		
        		
		$db->setQuery($query);
        $results = $db->loadAssocList();
        return $results;
		
	
	}
	
	//	Firm Reports
	function getFirmReport($billingYear)
	{
		$db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        $query = "SELECT T1.name, T1.Referred_Amount, T2.Received_Amount from (select c.name, sum(SterlingAmount) as Referred_Amount from kjz42_crmery_companies c left join kjz42_crmery_people p on p.referring_firm_id = c.id left join kjz42_crmery_referral_payments rp on rp.referral_ID = p.id where rp.BillingYear LIKE '%$billingYear%'  group by c.name ) T1 inner join (select c.name, sum(SterlingAmount) as Received_Amount from kjz42_crmery_companies c left join kjz42_crmery_people p on p.company_id = c.id left join kjz42_crmery_referral_payments rp on rp.referral_ID = p.id where rp.BillingYear LIKE '%$billingYear%' group by c.name ) T2 on T1.name = T2.name ";
		
        		
		$db->setQuery($query);
        $results = $db->loadAssocList();
        return $results;
		
	
	}
	

}

