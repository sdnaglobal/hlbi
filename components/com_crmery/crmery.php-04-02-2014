<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
ini_set('pcre.backtrack_limit', 1000000);

//sessions
jimport( 'joomla.session.session' );

// Component Helper
jimport('joomla.application.component.helper');

// Require helpers
require_once (JPATH_SITE.'/administrator/components/com_crmery/helpers/config.php');
require_once (JPATH_COMPONENT.'/helpers/crmery.php');
CrmeryHelperCrmery::loadHelpers();

if ( CrmeryHelperQuote::hasCrmQuote() ){
	CrmeryHelperQuote::loadLanguage();
}

//crmery libraries
include(JPATH_SITE.'/components/com_crmery/libraries/pagination.php');
include(JPATH_SITE.'/administrator/components/com_crmery/libraries/bootstrap.php');

$tz = CrmeryHelperDate::getSiteTimezone();
// date_default_timezone_set($tz);

//load tables
JTable::addIncludePath(JPATH_COMPONENT.'/tables');

//load models
JModel::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_crmery/models');

//Load plugins
JPluginHelper::importPlugin('crmery');

// Require the base controller
require_once (JPATH_COMPONENT.'/controller.php');

// Require specific controller if requested
$controller = "";
if($controller = JRequest::getVar('controller')) {
	require_once (JPATH_COMPONENT.'/controllers/'.$controller.'.php');
}

//load user toolbar
$format = JRequest::getVar('format');
$overrides = array('ajax','mail');
$view = JRequest::getVar('view');
$publicViews = array('publiccalendar');
if( $format != "raw" && !in_array($controller,$overrides) ) {

	// Set a default view if none exists
	if ( ! JRequest::getCmd( 'view' ) ) {
	        JRequest::setVar('view', 'dashboard' );
	}

	//get user object
	$cuser = CrmeryHelperUsers::getLoggedInUser();
    //if the user is logged in continue else redirect to joomla login
    if ( in_array($view,$publicViews) ){

   		CrmeryHelperTemplate::startCompWrap();

   		//Grab document instance
		$document = & JFactory::getDocument();

   		//load scripts
		$document->addScript( JURI::base().'components/com_crmery/media/js/jquery.js' );
		$document->addScript( JURI::base().'components/com_crmery/media/js/jquery.tools.min.js' );
		$document->addScript( JURI::base().'media/system/js/core.js');

		//load styles
		$document->addStyleSheet( JURI::base().'components/com_crmery/media/css/public.css' );

		if(CrmeryHelperTemplate::isMobile()) {
	  		 JRequest::setVar('tmpl','component');	
			 $document->addScript('http://maps.google.com/maps/api/js?sensor=false');
			 $document->addScript( JURI::base().'components/com_crmery/media/js/jquery.mobile.1.0.1.min.js' ); 
			 $document->addScript( JURI::base().'components/com_crmery/media/js/jquery.mobile.datepicker.js' ); 
			 $document->addScript( JURI::base().'components/com_crmery/media/js/jquery.mobile.map.js' ); 
			 $document->addScript( JURI::base().'components/com_crmery/media/js/jquery.mobile.map.extensions.js' ); 
			 $document->addScript( JURI::base().'components/com_crmery/media/js/jquery.mobile.map.services.js' ); 
			 $document->setMetaData('viewport','width=device-width, initial-scale=1');
   		}
    
    }else if ( $cuser->uid > 0 ){
   		
    	//Grab document instance
		$document = & JFactory::getDocument();
		
		//load scripts
		$document->addScript( JURI::base().'components/com_crmery/media/js/jquery.js' );
		$document->addScript( JURI::base().'components/com_crmery/media/js/jquery-ui.js' );
		$document->addScript( JURI::base().'components/com_crmery/media/js/jquery.tools.min.js' );

		//start component wrappers
		CrmeryHelperTemplate::startCompWrap();
   		
   		if(CrmeryHelperTemplate::isMobile()) {
	  		 JRequest::setVar('tmpl','component');	
			 $document->addScript('http://maps.google.com/maps/api/js?sensor=false');
			 $document->addScript( JURI::base().'components/com_crmery/media/js/jquery.mobile.1.0.1.min.js' ); 
			 $document->addScript( JURI::base().'components/com_crmery/media/js/jquery.mobile.datepicker.js' ); 
			 $document->addScript( JURI::base().'components/com_crmery/media/js/jquery.mobile.map.js' ); 
			 $document->addScript( JURI::base().'components/com_crmery/media/js/jquery.mobile.map.extensions.js' ); 
			 $document->addScript( JURI::base().'components/com_crmery/media/js/jquery.mobile.map.services.js' ); 
			 $document->addScript( JURI::base().'components/com_crmery/media/js/crmery.mobile.js');
			 $document->setMetaData('viewport','width=device-width, initial-scale=1');
   		} else {
	    	//load task events javascript which will be used throughout page redirects
	    	$document->addScript( JURI::base().'components/com_crmery/media/js/timepicker.js');
	    	$document->addScript( JURI::base().'components/com_crmery/media/js/crmery.js' );
	    	$document->addScript( JURI::base().'components/com_crmery/media/js/filters.js');
	    	$document->addScript( JURI::base().'components/com_crmery/media/js/autogrow.js');
	    	$document->addScript( JURI::base().'components/com_crmery/media/js/jquery.cluetip.min.js');
	    	
	    	//start component div wrapper
	    	if ( JRequest::getVar('view') != "print"){
	    		CrmeryHelperTemplate::loadToolbar();
	    	}
		}

		$document->addScript( JURI::base().'media/system/js/core.js');
		
		CrmeryHelperActivity::saveUserLoginHistory();

		//load styles
		CrmeryHelperStyles::loadStyleSheets();	   
		
   }else{
       $app = JFactory::getApplication();
       $app->redirect(CrmeryHelperConfig::getLoginRedirect());
   }
}

CrmeryHelperTemplate::loadJavascriptLanguage(); 

// Create the controller
$classname	= 'CrmeryController'.$controller;
$controller = new $classname();

if(CrmeryHelperUsers::isFullscreen()) {
	JRequest::setVar('tmpl', 'component' );
}

// Perform the Request task
$controller->execute( JRequest::getVar('task'));

//end componenet wrapper
if( $format != "raw" && $controller != "ajax" ) { 
	CrmeryHelperTemplate::endCompWrap();
}

// Redirect if set by the controller
$controller->redirect();

?>