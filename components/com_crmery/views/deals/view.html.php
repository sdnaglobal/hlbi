<?php



/*------------------------------------------------------------------------

# CRMery

# ------------------------------------------------------------------------

# @author CRMery

# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.

# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Website: http://www.crmery.com

-------------------------------------------------------------------------*/

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' ); 



jimport( 'joomla.application.component.view');



class CrmeryViewDeals extends CrmeryHelperView

{

    function display($tpl = null)

    {

	 
		unset($_SESSION['yearSess']); 
        //retrieve deal list from model

        $model = & JModel::getInstance('deal','CrmeryModel');

        $state = $model->getState();

        $dealList = array();
		
		$dealListInfo = array();

        $deal = array();

        $doc =& JFactory::getDocument();

        

        //session info

        $session = JFactory::getSession();

        $member_role = CrmeryHelperUsers::getRole();

        $user_id = CrmeryHelperUsers::getUserId();

        $team_id = CrmeryHelperUsers::getTeamId();

        

        //determine if we are requesting a specific deal or all deals

        //if id requested

        if( JRequest::getVar('id') ) {
		
            $model->set('_id',JRequest::getVar('id'));

            $dealListInfo = $model->getDealsInfo();	
			
			
            if ( is_null($dealListInfo[0]['id']) ){ 		


               $app = JFactory::getApplication();

              // $app->redirect(JRoute::_('index.php?option=com_crmery&view=deals'),CRMText::_('COM_CRMERY_NOT_AUTHORIZED'));

            }

        }else{
		//else load all deals

            

            $pagination = $model->getPagination();



            if ( JRequest::getVar('layout') != 'edit' ){ 



                if(CrmeryHelperTemplate::isMobile()) {

                    $model->set("limit",0);

                    $model->set('ordering','d.name ASC');

                    $model->set("archived",null);

                    $model->set("user_override",'all');

                }



                $dealList = $model->getDeals();

            }

        }

     
        //determine if we are editing an existing deal entry

        if( count($dealListInfo) == 1 ){
			 //grab deal object

            $deal = $dealListInfo[0];

            $deal['header'] = ucwords(CRMText::_('COM_CRMERY_DEAL_EDIT')); 

        }else{
			//else we are creating a new entry

            $deal = array();

            $deal['name'] = "";

            $deal['summary'] = "";

            $deal['company_id'] = ( JRequest::getVar('company_id') ) ? JRequest::getVar('company_id') : null;

            $deal['person_id'] = ( JRequest::getVar('person_id') ) ? JRequest::getVar('person_id') : null;

            

            //get company name to prefill data and hidden fields

            if ( $deal['company_id'] ) {

                $company = CrmeryHelperCompany::getCompany($deal['company_id']);

                $deal['company_name'] = $company[0]['name'];

                $deal['company_id'] = $company[0]['id'];

            }

            

            //if a person is specified prefill data

            if ( $deal['person_id'] ) {

                $person = CrmeryHelperPeople::getPerson($deal['person_id']);

                

                $deal['person_name'] = $person[0]['last_name'] . ', ' . $person[0]['first_name']; 

                $deal['person_id'] = $person[0]['id'];

                

                //assign company if person is associated with company

                if ( $person[0]['company_id'] ){

                    $deal['company_id'] = $person[0]['company_id'];

                    $deal['company_name'] = $person[0]['company_name'];

                }

                

            }

             

            //assign rest of null data

            $deal['amount'] = "";

            $deal['stage_id'] = 0;

            $deal['source_id'] = 0;

            $deal['probability'] = 0;

            $deal['status_id'] = 0;

            $deal['expected_close'] = "";

            $deal['header'] = ucwords(CRMText::_('COM_CRMERY_DEAL_HEADER')); 

        }

        

        //load javalibs

        if(!CrmeryHelperTemplate::isMobile()) {

            $doc->addScript( JURI::base().'components/com_crmery/media/js/deal_manager.js' );

        }

        

        //dropdown info

        //get deal type filters

        $deal_types = CrmeryHelperDeal::getDealTypes();

        $deal_type_name = $session->get('deal_type_filter');

        $deal_type_name = ( $deal_type_name ) ? $deal_types[$deal_type_name] : $deal_types['all'];

        

        //get column filters

        $column_filters = CrmeryHelperDeal::getColumnFilters();

        $selected_columns = CrmeryHelperDeal::getSelectedColumnFilters();

        

        //get member access info

        $teams = CrmeryHelperUsers::getTeams();

       // $users = CrmeryHelperUsers::getUsers();

        $stages = CrmeryHelperDeal::getStages();

        

        //get deal stage filters

        $stage_name = $session->get('deal_stage_filter');

        $stage_name = ( $stage_name ) ? $stages[$stage_name] : $stages['all'];

        

        //get session data to prefill filters

        $user_filter = $session->get('deal_user_filter');

        $team_filter = $session->get('deal_team_filter');

        if ( $user_filter == "all" && $user_filter != $user_id ){

            $user_name = CRMText::_('COM_CRMERY_ALL_USERS');

        }else if ( $user_filter == "all" ){

            $user_name = CRMText::_('COM_CRMERY_ME');

        }else if ( $user_filter AND $user_filter != $user_id AND $user_filter != 'all' ){

            $user_info = CrmeryHelperUsers::getUsers($user_filter);

            $user_info = $user_info[0];

            $user_name = $user_info['first_name'] . " " . $user_info['last_name'];

        }else if ( $team_filter ){

            $team_info = CrmeryHelperUsers::getTeams($team_filter);

            $team = $team_info[0];

            $user_name = $team['team_name'].CRMText::_('COM_CRMERY_TEAM_APPEND');

        }else{

            $user_name = CRMText::_('COM_CRMERY_ME');

        }



        //get closing time filters

        $closing_names = CrmeryHelperDeal::getClosing();

        $closing_name = $session->get('deal_close_filter');

        $closing_name = ( $closing_name ) ? $closing_names[$closing_name] : $closing_names['all'];

        

        //get total deals associated with user

		//echo '<br /><br />userid->'.$user_id .' <br>team id-->'.$team_id .' member role->'.$member_role;

        $total_deals = CrmeryHelperUsers::getDealCount($user_id,$team_id,$member_role);

        //echo '0000-->'.$total_dealaas = CrmeryHelperUsers::getDeal($user_id,$team_id,$member_role);



        //Load Events & Tasks for person

        $layout = $this->getLayout();
		
		//echo $layout;
		//die;

        if ( $layout == "deal" ){

                $model = & JModel::getInstance('event','CrmeryModel');

                $events = $model->getEvents("deal",null,JRequest::getVar('id'));

                $ref = array( array('ref'=>'events','data'=>$events) );

                $eventDock = CrmeryHelperView::getView('events','event_dock',$ref);

                $this->assignRef('event_dock',$eventDock);





                $ref = array(array('ref'=>'contacts','data'=>$dealListInfo[0]['people']));

                $contact_info = CrmeryHelperView::getView('contacts','default',$ref);

                $primary_contact_id = CrmeryHelperDeal::getPrimaryContact($dealListInfo[0]['id']);

                $contact_info->assignRef('primary_contact_id',$primary_contact_id);

                $this->assignRef('contact_info',$contact_info);



                $document_list = CrmeryHelperView::getView('documents','document_row',array(array('ref'=>'documents','data'=>$deal['documents'])));

                $this->assignRef('document_list',$document_list);



                $custom_fields_view = CrmeryHelperView::getView('custom','default');

                $type = "deal";

                $custom_fields_view->assignRef('type',$type);

                $custom_fields_view->assignRef('item',$dealListInfo[0]);

                $this->assignRef('custom_fields_view',$custom_fields_view);



                if ( CrmeryHelperBanter::hasBanter() ){

                    $room_list = new CrmeryHelperTranscriptlists();

                    $room_lists = $room_list->getRooms();

                    $transcripts = array();

                    if ( is_array($room_lists) && count($room_lists) > 0 ) { 

                        $transcripts = $room_list->getTranscripts($room_lists[0]->id);

                    }

                    $banter_dock = CrmeryHelperView::getView('banter','default');

                    $banter_dock->assignRef('rooms',$room_lists);

                    $banter_dock->assignRef('transcripts',$transcripts);

                    $this->assignRef('banter_dock',$banter_dock);

                }



                if ( CrmeryHelperQuote::hasCRMQuote() )

                {

                    $deal = &$dealListInfo[0];

                    $quote_dock = CrmeryHelperQuote::getDockView($deal);

                    $this->assignRef('quote_dock',$quote_dock);

                }



                 /** get latest activities **/

                $activityHelper = new CRMeryHelperActivity;

                $activity = $activityHelper->getActivity();

                $latest_activities = CrmeryHelperView::getView('activities','default');

                $latest_activities_view = CrmeryHelperView::getView('activities','latest_activities');

                $latest_activities_view->assignRef('activity',$activity);

                $latest_activities->assignRef('latest_activities',$latest_activities_view);

                $this->assignRef('latest_activities',$latest_activities);

                

        }



        if ( $layout == "default" ){

            $deal_list = CrmeryHelperView::getView('deals','list',array(array('ref'=>'dealList','data'=>$dealList)));

            $total = $model->getTotal();



            $deal_list->assignRef('total',$total);

            $deal_list->assignRef('pagination',$pagination);

            $this->assignRef('deal_list',$deal_list);

            $this->assignRef('state',$state);

            $doc->addScriptDeclaration("

            loc = 'deals';

            order_url = 'index.php?option=com_crmery&view=deals&layout=list&format=raw&tmpl=component';

            order_dir = '".$state->get('Deal.filter_order_Dir')."';

            order_col = '".$state->get('Deal.filter_order')."';");



            $deal_name = $state->get('Deal.deals_name');

            $this->assignRef('deal_filter',$deal_name);

        }



        if ( CrmeryHelperTemplate::isMobile() ){

            $add_note = CrmeryHelperView::getView('note','edit');

            $this->assignRef('add_note',$add_note);



            $add_task = CrmeryHelperView::getView('events','edit_task');

            $association_type = 'deal';

            $association_id = JRequest::getVar('id');

            $add_task->assignRef('association_type',$association_type);

            $add_task->assignRef('association_id', $association_id);



            $this->assignRef('add_task', $add_task);

        }



        if ( $layout == "edit" ){

            $item = JRequest::getVar('id') && array_key_exists(0,$dealListInfo) ? $dealListInfo[0] : array('id'=>'');

            $edit_custom_fields_view = CrmeryHelperView::getView('custom','edit');

            $type = "deal";

            $edit_custom_fields_view->assignRef('type',$type);

            $edit_custom_fields_view->assignRef('item',$item);

            $this->assignRef('edit_custom_fields_view',$edit_custom_fields_view);





            $companyModel =& JModel::getInstance('company','CrmeryModel');

            $json = TRUE;

            $companyNames = $companyModel->getCompanyNames($json);

            $doc->addScriptDeclaration("var company_names=".$companyNames.";");



            $peopleModel =& JModel::getInstance('people','CrmeryModel');

            $json = TRUE;

            $peopleNames = $peopleModel->getPeopleNames($json);

            $doc->addScriptDeclaration("var people_names=".$peopleNames.";");

        }



        $closed_stages = CrmeryHelperDeal::getClosedStages();

        

        //assign results to view

        $this->assignRef('closed_stages',$closed_stages);

        $this->assignRef('dealList',$dealList);
		
		$this->assignRef('dealListInfo',$dealListInfo);

        $this->assignRef('totalDeals',$total_deals);

        $this->assignRef('deal',$deal);

        $this->assignRef('deal_types',$deal_types);

        $this->assignRef('deal_type_name',$deal_type_name);

        $this->assignRef('user_id',$user_id);

        $this->assignRef('member_role',$member_role);

        $this->assignRef('teams',$teams);

      //  $this->assignRef('users',$users);

        $this->assignRef('stages',$stages);

        $this->assignRef('stage_name',$stage_name);

        $this->assignRef('user_name',$user_name);

        $this->assignRef('closing_names',$closing_names);

        $this->assignRef('closing_name',$closing_name);

        $this->assignRef('state',$state);

        $this->assignRef('column_filters',$column_filters);

        $this->assignRef('selected_columns',$selected_columns);

                

        //display

        parent::display($tpl);

    }

    

}

?>

