<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$n = count($this->deals);
for ( $i=0; $i<$n; $i++ ) {
	$deal = $this->deals[$i];
	$k = $i%2;
	$data = array(
			array('ref'=>'deal','data'=>$deal),
			array('ref'=>'offset','data'=>$k)
		);
	$view = CrmeryHelperView::getView('deals','deal_dock_entry',$data);
	$view->display();
} ?>