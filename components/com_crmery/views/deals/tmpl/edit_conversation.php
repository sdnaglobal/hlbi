<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$conversation = $this->conversation;
?>
<form id="convo_edit" method="POST" action="<?php echo 'index.php?option=com_crmery&controller=main&model=conversations&task=save'; ?>" onsubmit="return save(this)" >
	<input type="hidden" name="id" value="<?php echo $conversation['id']; ?>" />
	<div id="editForm">
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_SENTENCE'); ?></div>
			<div class="crmeryValue">
				<textarea class="inputbox" name="conversation"><?php echo $conversation['conversation']; ?></textarea>
			</div>
		</div>
		<div class="actions">
			<a href="javascript:void(0);" onclick="addConvoEntry('convo_edit');" class="button"><?php echo CRMText::_('COM_CRMERY_SAVE_BUTTON'); ?></a>
			<a href="javascript:void(0);" onclick="window.top.window.jQuery('.ui-dialog-content').dialog('close');"><?php echo CRMText::_('COM_CRMERY_CANCEL_BUTTON'); ?></a>
		</div>
	</div>
</form>