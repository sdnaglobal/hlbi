<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$deal = $this->deal;
//echo "<pre>"; print_r($deal); echo "</pre>";
//echo 'layout->'.$this->getLayout();

?>
<?php $format = JRequest::getVar('format'); ?>
<?php if ( $format != "raw" ){ ?>
	<h1><?php echo ucwords($deal['header']); ?></h1>
<?php } ?>
<form id="edit_form" method="POST" name="new_deal" action="<?php echo 'index.php?option=com_crmery&controller=deals&task=save'; ?>" onsubmit="return save(this)">
	<div id="editForm">
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DEAL_NAME'); ?><span class="required">*</span></div>
			<div class="crmeryValue wide"><input class="inputbox required" type="text" name="name" placeholder="<?php echo CRMText::_('COM_CRMERY_DEAL_NAME_NULL'); ?>" value="<?php echo $deal['name']; ?>" /></div>
		</div>
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DEAL_SUMMARY'); ?></div>
			<div class="crmeryValue wide"><textarea class="inputbox" name="summary" cols="50" placeholder="<?php echo CRMText::_('COM_CRMERY_DEAL_SUMMARY_NULL'); ?>" rows="5"><?php echo $deal['summary']; ?></textarea></div>
		</div>
		
			<?php if ( $format != "raw" ){ ?>
				<?php echo $this->edit_custom_fields_view->display(); ?>
			<?php } ?>	
	
		<div class="crmeryRow" style="display:none;">
			<div class="crmeryField"><?php echo "Client Company Name"; ?></div>
			<div class="crmeryValue">
					<input type="text" class="inputbox" name="client_company_name" id="client_company_name" value="<?php if ( array_key_exists('client_company_name',$deal) ) echo $deal['client_company_name']; ?>" />
					 
					<div id="company_message"></div>
			</div>
		</div>
		
	
			
		
		<!-- <div class="crmeryRow" >
			<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_PRIMARY_CONTACT')); ?></div>
			<div class="crmeryValue">
					<input type="text" onkeyup="checkPersonName(this);" class="inputbox" name="primary_contact_name" id="primary_contact_name" value="<?php if ( array_key_exists('primary_contact_id',$deal) && $deal['primary_contact_id'] > 0 ) echo $deal['primary_contact_first_name'].' '.$deal['primary_contact_last_name']; ?>" />
					<input type="hidden" name="primary_contact_id" id="primary_contact_id" value="<?php if ( array_key_exists('primary_contact_id',$deal) ) echo $deal['primary_contact_id']; ?>" />
					<div id="person_message"></div>
			</div>
		</div>  --> 
		
		
	
		
			<?php if ( array_key_exists('person_id',$deal) && !is_null($deal['person_id']) ) {
				echo '<div class="crmeryRow">';
				echo	'<div class="crmeryField">'.ucwords(CRMText::_('COM_CRMERY_PERSON')).'</div>';
				echo	'<div class="crmeryValue">'.$deal['person_name'].'</div>';
				echo '</div>';
			} 
			
			?>
			<!-- <div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DEAL_AMOUNT') ?><!-- <span class="required">*</span>  -- > </div>
				<div class="crmeryValue"><input class="inputbox" type="text" name="amount" value="<?php echo $deal['amount']; ?>" /></div>
			</div>-->
			<div class="crmeryRow" style="display:none;">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DEAL_STAGE'); ?></div>
				<div class="crmeryValue">
					<?php echo CrmeryHelperDropdown::generateDropdown('stage',$deal['stage_id']); ?>
				</div>
			</div>
			<div class="crmeryRow" style="display:none;">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DEAL_SOURCE'); ?></div>
				<div class="crmeryValue">
					<?php echo CrmeryHelperDropdown::generateDropdown('source',$deal['source_id']); ?>
				</div>
			</div>
			<div class="crmeryRow" style="display:none;">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DEAL_PROBABILITY'); ?></div>
				<div class="crmeryValue">
					<div id="probability_slider"></div>
					<span id="probability_label"><?php if(array_key_exists('probability',$deal) && $deal['probability'] != 0 && $deal['probability'] != "" ) echo $deal['probability']."%"; ?></span>
					<input type="hidden" id="probability_input_hidden" name="probability" value="<?php if ( array_key_exists('probability',$deal) ) echo $deal['probability']; ?>" />
				</div>
			</div>
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DEAL_STATUS'); ?></div>
				<div class="crmeryValue">
					<?php echo CrmeryHelperDropdown::generateDealStatuses($deal['status_id']); ?>
				</div>
			</div>
			<?php $closedStages = CrmeryHelperDeal::getClosedStages(); ?>
			<?php if ( array_key_exists('stage_id',$deal) && !in_array($deal['stage_id'],$closedStages) && array_key_exists('expected_close',$deal) ){ ?>
			<div class="crmeryRow" style="display:none;">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DEAL_CLOSE'); ?></div>
				<div class="crmeryValue">
					<input class="inputbox date_input" type="text" id="expected_close" name="expected_close_input" value="<?php echo CrmeryHelperDate::formatDate($deal['expected_close']); ?>">
					<input type="hidden" id="expected_close_hidden" name="expected_close" value="<?php if ( array_key_exists('expected_close',$deal) && !is_null($deal['expected_close']) && $deal['expected_close'] != "" && $deal['expected_close'] != "0000-00-00"   ) { echo $deal['expected_close']; } else { echo date("Y-m-d"); } ?>" />
				</div>
			</div>
			<?php } ?>
			<?php if ( array_key_exists('stage_id',$deal) && in_array($deal['stage_id'],$closedStages) && array_key_exists('actual_close',$deal) ){ ?>
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DEAL_ACTUAL_CLOSE'); ?></div>
				<div class="crmeryValue">
					<input class="inputbox date_input required" type="text" id="actual_close" name="actual_close_input" value="<?php echo CrmeryHelperDate::formatDate($deal['actual_close']); ?>">
					<input type="hidden" id="actual_close_hidden" name="actual_close" value="<?php echo $deal['actual_close']; ?>" />
				</div>
			</div>
			<?php } ?>
			
		   <div class="crmeryRow">
				<div class="crmeryField"> Firm Name </div>
				<div class="crmeryValue">					
					<?php  //echo CrmeryHelperDropdown::getPartnerDropdown($deal['partner_id']);
                              echo CrmeryHelperDropdown::generateDropdown('usercompanyedit',$deal['company_id'],'name="company_id"');

					?>
				</div>
			</div>	
			
			<?php if ( $format != "raw" ){ ?>
				<?php //echo $this->edit_custom_fields_view->display(); ?>
				<span class="actions"><input class="button" type="submit" value="<?php echo CRMText::_('COM_CRMERY_SAVE_BUTTON'); ?>"> <a href="javascript:void(0);" onclick="window.history.back()"><?php echo CRMText::_('COM_CRMERY_CANCEL_BUTTON'); ?></a></span>
			<?php } else { ?>
				<div class="actions"><a href="javascript:void(0);" onclick="saveListItem(<?php echo JRequest::getVar('id'); ?>);" class="button"><?php echo CRMText::_('COM_CRMERY_SAVE_BUTTON'); ?></a><a href="javascript:void(0);" onclick="window.top.window.jQuery('.ui-dialog-content').dialog('close');"><?php echo CRMText::_('COM_CRMERY_CANCEL_BUTTON'); ?></a></div>
			<?php } ?>
	<?php
		if ( array_key_exists('id',$deal) ){
			echo '<input class="inputbox" type="hidden" name="id" value="'.$deal['id'].'" />';
		}
		if ( array_key_exists('person_id',$deal) AND JRequest::getVar('person_id') ) { 
			echo '<input class="inputbox" type="hidden" name="person_id" value="'.$deal['person_id'].'" />';
		}
		if ( array_key_exists('company_id',$deal) AND JRequest::getVar('company_id') ) {
			echo '<input class="inputbox" type="hidden" name="company_id" value="'.$deal['company_id'].'" />';
		}
	?>
	</div>
</form>