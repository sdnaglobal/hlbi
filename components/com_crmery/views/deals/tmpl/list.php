<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<?php if ( !(JRequest::getVar('id')) ){ ?>
<thead>
    <tr>
        <th class="checkbox_column"><input type="checkbox" onclick="selectAll(this);" /></th>
        <th class="checkbox_column"><div class="sort_order"><a class="d.id" onclick="sortTable('d.id',this)"><?php echo CRMText::_('COM_CRMERY_DEALS_REFERENCE_NUMBER'); ?></a></div> </th>
        <th class="name" ><div class="sort_order"><a class="d.name" onclick="sortTable('d.name',this)"><?php echo CRMText::_('COM_CRMERY_DEALS_NAME'); ?></a></div></th>
        <th class="company"><div class="sort_order"><a class="c.name" onclick="sortTable('c.name',this)"><?php echo CRMText::_('COM_CRMERY_DEALS_COMPANY'); ?></a></div></th>
       <!-- <th class="primary_contact"><div class="sort_order"><a class="p.last_name" onclick="sortTable('p.last_name',this)"><?php echo CRMText::_('COM_CRMERY_PRIMARY_CONTACT'); ?></a></div></th> -->
        <th class="summary" ><?php echo CRMText::_('COM_CRMERY_DEALS_SUMMARY'); ?></th>
       <!-- <th class="amount" ><div class="sort_order"><a class="d.amount" onclick="sortTable('d.amount',this)"><?php echo CRMText::_('COM_CRMERY_DEALS_AMOUNT'); ?></a></div></th> -->
        <th class="status" ><div class="sort_order"><a class="d.status_id" onclick="sortTable('d.status_id',this)"><?php echo CRMText::_('COM_CRMERY_DEALS_STATUS'); ?></a></div></th>
        <!-- <th class="stage" ><div class="sort_order"><a class="stage.ordering" onclick="sortTable('stage.ordering',this)"><?php //echo CRMText::_('COM_CRMERY_DEALS_STAGE'); ?></a></div></th> -->
        <th class="source" ><div class="sort_order"><a class="source.ordering" onclick="sortTable('source.ordering',this)"><?php echo CRMText::_('COM_CRMERY_DEAL_SOURCE'); ?></a></div></th>
        <th class="owner" ><div class="sort_order"><a class="d.owner_id" onclick="sortTable('d.owner_id',this)"><?php echo CRMText::_('COM_CRMERY_DEALS_OWNER'); ?></a></div></th>
        <th class="next_action" ><?php echo CRMText::_('COM_CRMERY_DEALS_NEXT'); ?></th>
        <th class="deals_due" ><?php echo CRMText::_('COM_CRMERY_DEALS_DUE'); ?></th>
      <!-- <th class="expected_close" ><div class="sort_order"><a class="d.expected_close" onclick="sortTable('d.expected_close',this)"><?php //echo CRMText::_('COM_CRMERY_DEALS_EXPECTED_CLOSE'); ?></a></div></th> 
        <th class="actual_close" ><div class="sort_order"><a class="d.actual_close" onclick="sortTable('d.actual_close',this)"><?php //echo CRMText::_('COM_CRMERY_DEALS_ACTUAL_CLOSE'); ?></a></div></th>
		-->
        <th class="contacts" ><?php echo CRMText::_('COM_CRMERY_DEALS_CONTACTS'); ?></th>
        <th class="notes" ><?php echo CRMText::_('COM_CRMERY_DEALS_NOTES'); ?></th>
        <th class="created" ><div class="sort_order"><a class="d.created" onclick="sortTable('d.created',this)"><?php echo CRMText::_('COM_CRMERY_DEALS_CREATED'); ?></a></div></th>
        <th class="modified" ><div class="sort_order"><a class="d.modified" onclick="sortTable('d.modified',this)"><?php echo CRMText::_('COM_CRMERY_DEALS_MODIFIED'); ?></a></div></th>
        <?php if ( CrmeryHelperQuote::hasCrmQuote() ){
            echo CrmeryHelperQuote::getDealHeaders();
        }?>
    </tr>
</thead>
<tbody>
<?php } ?>
<?php
   // $stages = CrmeryHelperDeal::getStages(null,TRUE,FALSE);
    $statuses = CrmeryHelperDeal::getStatuses(null,true);
    //$sources = CrmeryHelperDeal::getSources(null);
   // $users = CrmeryHelperUsers::getUsers(null,TRUE);
	$n = count($this->dealList);
	
	function get_referrals_by_clientID_and_total_amount_currentYear($dealid)
	{
		$db = JFactory::getDbo();
		$query_deal = $db->getQuery(true);
		//$query_deal = "SELECT cf.person_id, cp.id FROM #__crmery_people_cf as cf where cf.association_id=".$deal['id']." and cf.association_type='deal'";
		$query_deal = "SELECT cf.person_id, cp.id FROM #__crmery_people_cf as cf, #__crmery_people as cp where cf.association_id=".$dealid." and cf.association_type='deal' and cp.id=cf.person_id and cp.published>0 ";


		$db->setQuery($query_deal);
		$referrals = $db->loadObjectlist();
			
		$total_amount = 0;
		$total_amount_year = 0;
			
		foreach($referrals as $referral)
		{
			$total_amount = get_amount_billed_by_referral_id($referral->person_id,date('Y'));
			$total_amount_year = ($total_amount_year+$total_amount);
			 
		}
		return $total_amount_year;

	}

	
	$k = 0;
		for($i=0;$i<$n;$i++) {
			$deal = $this->dealList[$i];
			
			//$deals_amount  = get_referrals_by_clientID_and_total_amount_currentYear($deal['id']);
			$k = $i%2;
            $deal['status_name'] = $deal['status_name'] == "" ? "none" : $deal['status_name'];
			echo "<tr id='list_row_".$deal['id']."' class='crmery_row_".$k."' >";
                echo '<td><input class="export" type="checkbox" name="ids[]" value="'.$deal['id'].'" /></td>';
                echo '<td>'.CRMText::_('COM_CRMERY_DEALS_REFERENCE_PREFIX').$deal['id'].'</td>';
				echo '<td class="list_edit_button" id="list_'.$deal['id'].'"><div class="title_holder"><a href="'.JRoute::_('index.php?option=com_crmery&view=deals&layout=deal&id='.$deal['id']).'">'.$deal['name']."</a></div></td>";
				echo "<td class='company' ><a href='".JRoute::_('index.php?option=com_crmery&view=companies&layout=company&id='.$deal['company_id'])."'>".$deal['company_name']."</a></td>";
               // echo "<td class='primary_contact' ><a href='".JRoute::_('index.php?option=com_crmery&view=people&layout=person&id='.$deal['primary_contact_id'])."'>".$deal['primary_contact_first_name'] . ' ' . $deal['primary_contact_last_name']."</a></td>";
				echo "<td class='summary' >".$deal['summary']."</td>";
				//echo "<td class='amount' ><span class='amount'>".CrmeryHelperConfig::getCurrency().CrmeryHelperUsers::formatAmount($deals_amount)."</span></td>";
				echo "<td class='status' ><a href='javascript:void(0);' class='dropdown status_link' id='deal_status_".$deal['id']."_link'>".$deal['status_name'].'<div class="status-dot" style="background-color:#'.$deal['status_color'].' !important;"></div></a></td>';
                echo '<div class="filters" data-item="deal" data-field="status_id" data-item-id="'.$deal['id'].'" id="deal_status_'.$deal['id'].'">';
                    echo '<ul>';
                        if (count($statuses)) { foreach($statuses as $id => $status ){
                            echo '<li><a href="javascript:void(0)" class="status_select dropdown_item" data-value="'.$id.'">'.$status['label'].'<div class="status-dot" style="background-color:#'.$status['color'].' !important;"></div></a></li>';
                        }}
                    echo '</ul>';
                echo '</div>';
              /*  $stage_color = CRMText::_('COM_CRMERY_DEFAULT_DEAL_STAGE_COLOR');
                if ( count($stages) > 0 ){ foreach ( $stages as $stage ){
                        if ( $stage['id'] == $deal['stage_id'] ){
                                $stage_color =  $stage['color'];
                        }
                    }}
                $stage_name = ( array_key_exists('stage_id',$deal) && $deal['stage_id'] != 0 ) ? $deal['stage_name'] : CRMText::_('COM_CRMERY_CLICK_TO_EDIT');                    
				echo "<td class='stage' ><a href='javascript:void(0);' class='dropdown' id='deal_stage_".$deal['id']."_link'>".$stage_name.'<div class="status-dot" style="background-color: #'.$stage_color.'"></div></a></td>';
                echo '<div class="filters" data-item="deal" data-field="stage_id" data-item-id="'.$deal['id'].'" id="deal_stage_'.$deal['id'].'">';
                    echo '<ul>';
                        if ( count($stages) ){ foreach($stages as $id => $stage ) {
                            echo '<li><a href="javascript:void(0)" class="stage_select dropdown_item" data-value="'.$stage['id'].'">'.$stage['name'].'<div class="status-dot" style="background-color: #'.$stage['color'].'"></div></a></li>';
                        }}
                    echo '</ul>';
                echo '</div>'; */

				$source_name = ( array_key_exists('source_id',$deal) && $deal['source_id'] != 0 ) ? $deal['source_name'] : CRMText::_('COM_CRMERY_CLICK_TO_EDIT');                    
                echo "<td class='source' ><a href='javascript:void(0);' class='dropdown' id='deal_source_".$deal['id']."_link'>".$source_name.'</a></td>';
                echo '<div class="filters" data-item="deal" data-field="source_id" data-item-id="'.$deal['id'].'" id="deal_source_'.$deal['id'].'">';
                    echo '<ul>';
                        if ( count($sources) ){ foreach($sources as $id => $source ) {
                            echo '<li><a href="javascript:void(0)" class="source_select dropdown_item" data-value="'.$id.'">'.$source.'</a></li>';
                        }}
                    echo '</ul>';
                echo '</div>';
                echo "<td class='owner'><a href='javascript:void(0)' class='dropdown' id='deal_owner_".$deal['id']."_link'>".$deal['owner_first_name']." ".$deal['owner_last_name']."</a></td>";
                // echo '<div class="filters" data-item="deal" data-field="owner_id" data-item-id="'.$deal['id'].'" id="deal_owner_'.$deal['id'].'">';
                    // echo '<ul>';
                        // echo '<li><a href="javascript:void(0)" class="owner_select dropdown_item" data-value="'.CrmeryHelperUsers::getUserId().'">'.CRMText::_('COM_CRMERY_ME').'</a></li>';
                        // if ( count($users) ){ foreach ( $users as $key => $user ){
                            // echo '<li><a href="javascript:void(0)" class="owner_select dropdown_item" data-value="'.$user['value'].'">'.$user['label'].'</a></li>';
                        // }}
                    // echo '</ul>';
                // echo '</div>';
                //if the deal has an associated task
                if ( array_key_exists('event_id',$deal) ){
                    echo "<td class='next_action' ><a onclick=\"editEvent(".$deal['event_id'].",'".$deal['event_type']."')\">".$deal['event_name']."</a></td>";
                    echo "<td class='deals_due' >".CrmeryHelperDate::formatDate($deal['event_due_date'])."</td>";    
                } else {
                    echo '<td class="next_action" >'.CRMText::_("COM_CRMERY_NONE").'</td>';
                    echo '<td class="deals_due" ></td>';
                }
              //  $expected_close = $deal['expected_close'] != "0000-00-00 00:00:00" ? CrmeryHelperDate::formatDate($deal['expected_close']) : CRMText::_('COM_CRMERY_NOT_SET');
               // echo "<td id='expected_close_".$deal['id']."' class='expected_close'>".$expected_close."</td>";
               // $actual_close = $deal['actual_close'] != "0000-00-00 00:00:00" && $deal['closed'] != 0 ? CrmeryHelperDate::formatDate($deal['actual_close']) : $actual_close = CRMText::_('COM_CRMERY_ACTIVE_DEAL');
              //  echo "<td id='actual_close".$deal['id']."' class='actual_close'>".$actual_close."</td>";
				echo '<td class="contacts" ><a href="javascript:void(0);" onclick="showDealContactsDialogModal('.$deal['id'].');"><img src="'.JURI::base().'components/com_crmery/media/images/card.png'.'"/></a></td>';
				echo '<td class="notes"><a href="javascript:void(0);" onclick="openNoteModal(\''.$deal['id'].'\',\'deal\');"><img src="'.JURI::base().'components/com_crmery/media/images/notes.png'.'"/></a></td>';
                echo "<td class='created'>".CrmeryHelperDate::formatDate($deal['created'])."</td>";
                echo "<td class='modified'>".CrmeryHelperDate::formatDate($deal['modified'])."</td>";
                if ( CrmeryHelperQuote::hasCrmQuote() ){
                    echo CrmeryHelperQuote::getDealRow($deal);
                }
			echo "</tr>";
		}
	?>
<?php if ( !(JRequest::getVar('id')) ){ ?>
</tbody>
<tfoot>
    <tr>
       <td colspan="30"><?php echo $this->pagination->getListFooter(); ?></td>
    </tr>
</tfoot>
<script type="text/javascript">
    var total = <?php echo $this->total; ?>;
    jQuery("#deals_matched").html(total);
    window.top.window.assignFilterOrder();
</script>
<?php } ?>

<?php 

function get_amount_billed_by_referral_id($ref_id,$year)
{
	$db = JFactory::getDbo();
	$billing = $db->getQuery(true);
	$billing->select('LocalAmount,LocalCurrencyCode');
	$billing->from('#__crmery_referral_payments');
	$billing->where('FeePaid=1 and referral_ID='.$ref_id);
	$billing->where(' YEAR(STR_TO_DATE(BillingYear, "%d/%m/%Y"))='.$year);
	$db->setQuery($billing);
	//echo '<br>-->>'.$year;
	//echo '<br>$billing->'.$billing;
	$billingdetails = $db->loadObjectList();
	$total_gbp = 0;

	//print_r($billingdetails);

	foreach($billingdetails as $bill)
	{ 	
	
		$sum = ($sum+$bill->LocalAmount);
		//echo '<br>sum-->'.$sum = ($bill->sum);
		//echo 'refid-->'.$referalid->id;
		$currencycode = $bill->LocalCurrencyCode;
	
		$ccode = $db->getQuery(true);
		$ccode->select('value_in_GBP');
		$ccode->from('#__crmery_currencies');
		$ccode->where("currency_code='$currencycode'");
		$db->setQuery($ccode);
		//echo '<br>'.$ccode;
		$ccodevalue = $db->loadObjectList();
		
		$ccode_value = $ccodevalue[0];
		$gbp = $ccode_value->value_in_GBP;
		//$bill->LocalAmount;
		$gbp_price = ($gbp*$bill->LocalAmount); 
		
		// echo '<br>gbp->'.$gbp = $ccode_value->value_in_GBP;
		// echo '<br>local->'.$bill->LocalAmount;
		// echo '<br>multiply->'.$gbp_price = ($gbp*$bill->LocalAmount); 					
		
		$total_gbp = ($total_gbp+$gbp_price); 
	}
	return $total_gbp;
	
}

?>

