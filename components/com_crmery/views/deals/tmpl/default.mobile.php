<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<script type="text/javascript">
	var loc = 'deals';
</script>

	<div data-role='header' data-theme='b'>
		<h1><?php echo ucwords(CRMText::_('COM_CRMERY_DEALS_HEADER')); ?></h1>
			<a href="<?php echo JRoute::_('index.php?option=com_crmery&view=dashboard'); ?>" data-icon="back" class="ui-btn-left">
			<?php echo CRMText::_('COM_CRMERY_BACK'); ?>
		</a>
	</div>

	<div data-role="content">	
	<ul class="ui-listview" data-role="listview" data-filter="true" data-autodividers="true" data-theme="c">
		<?php
			$n = count($this->dealList);
			$k = 0;
				for($i=0;$i<$n;$i++) {
					$deal = $this->dealList[$i];
					$k = $i%2;
					if($i==0 || substr_compare(strtolower($deal['name']),strtolower($this->dealList[$i-1]['name']),0,1) ) { 
						echo "<li data-role='list-divider'>".ucfirst(substr($deal['name'],0,1))."</li>";
					}
					echo '<li data-filtertext="'.$deal['name'].'" ><a href="'.JRoute::_('index.php?option=com_crmery&view=deals&layout=deal&id='.$deal['id']).'">'.$deal['name'].'</a></li>';
					
				}
		?>
	</ul>
	</div><!-- /content -->
