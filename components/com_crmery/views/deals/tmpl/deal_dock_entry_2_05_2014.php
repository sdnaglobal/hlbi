<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$deal = $this->deal; $k = $this->offset;
echo '<tr class="crmery_row_'.$k.'">';
	echo '<td>'.CRMText::_('COM_CRMERY_DEALS_REFERENCE_PREFIX').$deal['id'].'</td>';
	echo '<td><a href="'.JRoute::_('index.php?option=com_crmery&view=deals&layout=deal&id='.$deal['id']).'">'.$deal['name'].'</a></td>';
	echo '<td>'.$deal['owner_first_name'].' '.$deal['owner_last_name'].'</td>';
	echo '<td>'.$deal['status_name'].'<div class="status-dot" style="background-color:#'.$deal['status_color'].' !important;"></div></td>';
	//echo '<td><span class="amount">'.CrmeryHelperConfig::getConfigValue('currency').CrmeryHelperUsers::formatAmount($deal['amount']).'</span></td>';
echo '</tr>'; ?>