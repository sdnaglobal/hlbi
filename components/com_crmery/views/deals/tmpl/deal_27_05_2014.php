<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

//define deal
$deal = $this->dealList[0];

function get_user_name_by_id($u_id)
{
	$db = JFactory::getDbo();
	$query_cntry = $db->getQuery(true);
	$query_cntry = $db->getQuery(true);
	$query_cntry = "SELECT CONCAT(first_name,last_name) as username FROM #__crmery_users where id=".$u_id;

	$db->setQuery($query_cntry);
	return $username = $db->loadResult();
	
}

function get_firm_name_by_id($f_id)
{
	$db = JFactory::getDbo();
	$query_cntry = $db->getQuery(true);
	$query_cntry = $db->getQuery(true);
	$query_cntry = "SELECT name FROM #__crmery_companies where id=".$f_id;

	$db->setQuery($query_cntry);
	return $firmname = $db->loadResult();
	
}

function get_amount_billed_by_referral_id($ref_id,$year)
{
	$db = JFactory::getDbo();
	$billing = $db->getQuery(true);
	$billing->select('LocalAmount,LocalCurrencyCode,SterlingAmount');
	$billing->from('#__crmery_referral_payments');
	$billing->where('FeePaid=1 and referral_ID='.$ref_id);
	$billing->where(' YEAR(STR_TO_DATE(BillingYear, "%d/%m/%Y"))='.$year);
	$db->setQuery($billing);
	//echo '<br>-->>'.$year;
	//echo '<br>$billing->'.$billing;
	$billingdetails = $db->loadObjectList();
	$GBPsum = 0;

	//print_r($billingdetails);

	foreach($billingdetails as $bill)
	{ 	
	
		$sum = ($sum+$bill->LocalAmount);
		$GBPsum = ($GBPsum+$bill->SterlingAmount);
		//echo '<br>sum-->'.$sum = ($bill->sum);
		//echo 'refid-->'.$referalid->id;
	/*	$currencycode = $bill->LocalCurrencyCode;
	
		$ccode = $db->getQuery(true);
		$ccode->select('value_in_GBP');
		$ccode->from('#__crmery_currencies');
		$ccode->where("currency_code='$currencycode'");
		$db->setQuery($ccode);
		//echo '<br>'.$ccode;
		$ccodevalue = $db->loadObjectList();
		
		$ccode_value = $ccodevalue[0];
		$gbp = $ccode_value->value_in_GBP;
		//$bill->LocalAmount;
		$gbp_price = ($gbp*$bill->LocalAmount); 
		
		// echo '<br>gbp->'.$gbp = $ccode_value->value_in_GBP;
		// echo '<br>local->'.$bill->LocalAmount;
		// echo '<br>multiply->'.$gbp_price = ($gbp*$bill->LocalAmount); 					
		
		$total_gbp = ($total_gbp+$gbp_price); */
	}
	return $GBPsum;
	//return $total_gbp;
	
}


?>

<script type="text/javascript">
	var id = <?php echo $deal['id']; ?>;
	var deal_id = <?php echo $deal['id']; ?>;
	var loc = "deal";
	var AMOUNT = <?php $deal['amount'] = ( $deal['amount'] == 0 ) ? 0 : $deal['amount']; echo $deal['amount']; ?>;
	var archived = <?php echo $deal['archived']; ?>;
	var association_type = 'deal';
</script>

<iframe id="hidden" name="hidden" style="display:none;width:0px;height:0px;border:0px;"></iframe>

<div class="rightColumn">
	<div class="infoContainer">
			<h2><?php echo ucwords(CRMText::_('COM_CRMERY_CONTACT_INFO')); ?></h2>
			<?php $this->contact_info->display(); ?>
	</div>
	<div class="infoContainer" id='event_dock'>
		<h2><?php echo ucwords(CRMText::_('COM_CRMERY_TASKS_AND_EVENTS')); ?></h2>
		<?php $this->event_dock->display(); ?>
	</div>
	<?php if ( isset($this->banter_dock) ){ 
		$this->banter_dock->display();
	}?>
</div>	

<div class="leftColumn">
<ul class="entry_buttons">
	<li><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=deals&layout=edit&id='.$deal['id']); ?>" class=""><?php echo ucwords(CRMText::_("COM_CRMERY_EDIT_BUTTON")); ?></a></li>
</ul>
<div class="actions_container">

	<span class="actions textMiddle">
	<?php echo CRMText::_('COM_CRMERY_DEALS_REFERENCE_NUMBER')?>:<?php echo CRMText::_('COM_CRMERY_DEALS_REFERENCE_PREFIX')?><?php echo $deal['id']; ?>&nbsp;&nbsp;&nbsp;
	<a href="javascript:void(0);" id="actions_button" class="dropdown_arrow"><?php echo CRMText::_('COM_CRMERY_ACTION_BUTTON'); ?></a>
		
		<div id="actions">
			<ul>
				<li><a id="archive" onclick=""><?php if($deal['archived']==0) echo CRMText::_('COM_CRMERY_ARCHIVE'); if($deal['archived']==1) echo CRMText::_('COM_CRMERY_UNARCHIVE'); ?></a></li>

				<?php if ( $deal['owner_id'] == CrmeryHelperUsers::getUserId() ){ ?>
					<li><a onclick="shareItemDialog();" ><?php echo CRMText::_('COM_CRMERY_SHARE'); ?></a></li>
				<?php } ?>

				<form id="delete_form" method="POST" action="<?php echo JRoute::_('index.php?option=com_crmery&controller=main&task=trash'); ?>" >
					<input type="hidden" name="item_id" value="<?php echo $deal['id']; ?>" />
					<input type="hidden" name="item_type" value="deals" />
					<input type="hidden" name="page_redirect" value="deals" />
					<?php if ( CrmeryHelperUsers::canDelete() || $deal['owner_id'] == CrmeryHelperUsers::getUserId() ) { ?>
						<li><a onclick="deleteItem(this)"><?php echo CRMText::_('COM_CRMERY_DELETE'); ?></a></li>
					<?php } ?>
				</form>

				<form class="print_form" method="POST" target="_blank" action="<?php echo JRoute::_('index.php?option=com_crmery&view=print'); ?>">
					<input type="hidden" name="item_id" value="<?php echo $deal['id']; ?>" />
			    	<input type="hidden" name="layout" value="deal" />
			    	<input type="hidden" name="model" value="deal" />
					<li><a onclick="printItems(this)"><?php echo CRMText::_('COM_CRMERY_PRINT'); ?></a></li>
				</form>			

			</ul>
		</div>
	</span>
</div>

<h1><?php echo $deal['name']; ?></h1>

<div class="container">
	<div class="columncontainer">
		<div class="threecolumn">
			<div class="small_info first">
				<?php echo CRMText::_('COM_CRMERY_EDIT_AMOUNT'); ?> of Year <?php echo date('Y')?>:
				<span id="amount_total_top"> </span>
				<span class="editable amount parent" id="editable_amount_container" style="display:none;">
					<?php echo CrmeryHelperConfig::getCurrency(); ?>
					<div class="inline" id="editable_amount"><?php echo CrmeryHelperUsers::formatAmount($deal['amount']); ?></div>
					<div class="filters editable_info">
						<form id="amount_form">
							<input type="text" class="inputbox" name="amount" value="<?php echo CrmeryHelperUsers::formatAmount($deal['amount']); ?>" />
							<input type="button" class="button" onclick="saveEditableModal('amount_form');" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" />
						</form>
					</div>
				</span>
			</div>
			<div class="crmeryRow top">
				<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_EDIT_COMPANY')); ?></div>
				<div class="crmeryValue"><span class="company"><!--<a href="<?php echo JRoute::_('index.php?option=com_crmery&view=companies&layout=company&id='.$deal['company_id']); ?>"><?php echo $deal['company_name']; ?></a>--><?php echo $deal['company_name']; ?></div>
			</div>
			<div class="crmeryRow" style="display:none;">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_EDIT_OWNER'); ?></div>
				<div class="crmeryValue">
					<span class='dropdown' id='deal_owner_link'><?php echo $deal['owner_first_name']." ".$deal['owner_last_name']; ?></span>
					<?php 
						echo '<div class="filters" data-item="deal" data-field="owner_id" data-item-id="'.$deal['id'].'" id="deal_owner">';
		                    echo '<ul>';
		                    	$me = array(array('label'=>CRMText::_('COM_CRMERY_ME'),'value'=>CrmeryHelperUsers::getLoggedInUser()->id));
		                        $users = CrmeryHelperUsers::getUsers(null,TRUE);
		                        $users = array_merge($me,$users);
		                        if ( count($users) ){ foreach ( $users as $key => $user ){
		                            echo '<li><a href="javascript:void(0)" class="owner_select dropdown_item" data-value="'.$user['value'].'">'.$user['label'].'</a></li>';
		                        }}
		                    echo '</ul>';
		                echo '</div>';
	                ?>
				</div>
			</div>
		</div>	
		<div class="threecolumn">
			<div class="small_info middle" style="visibility:hidden;">
				<?php echo CRMText::_('COM_CRMERY_EDIT_AGE'); ?>: 
					<?php
						echo CrmeryHelperDate::getElapsedTime($deal['created']);
					?>
			</div>
				<div class="crmeryRow top" >
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_EDIT_OWNER'); ?></div>
				<div class="crmeryValue">
					<span   id='deal_owner_link'><?php echo $deal['owner_first_name']." ".$deal['owner_last_name']; ?></span>
					<?php 
						echo '<div class="filters" data-item="deal" data-field="owner_id" data-item-id="'.$deal['id'].'" id="deal_owner">';
		                    echo '<ul>';
		                    	$me = array(array('label'=>CRMText::_('COM_CRMERY_ME'),'value'=>CrmeryHelperUsers::getLoggedInUser()->id));
		                        $users = CrmeryHelperUsers::getUsers(null,TRUE);
		                        $users = array_merge($me,$users);
		                        if ( count($users) ){ foreach ( $users as $key => $user ){
		                            echo '<li><a href="javascript:void(0)" class="owner_select dropdown_item" data-value="'.$user['value'].'">'.$user['label'].'</a></li>';
		                        }}
		                    echo '</ul>';
		                echo '</div>';
	                ?>
				</div>
			</div>
	
		
			<div class="crmeryRow top" style="display:none;">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_EDIT_STAGE'); ?></div>
				<div class="crmeryValue">
					<span class='dropdown' id='deal_stage_link'>
						<?php $stages = CrmeryHelperDeal::getStages(null,TRUE,FALSE); ?>
						<?php $stage_color = CRMText::_('COM_CRMERY_DEFAULT_DEAL_STAGE_COLOR'); ?>
						<?php if ( count($stages) > 0 ){ foreach ( $stages as $stage ){ ?>
						<?php if ( $stage['id'] == $deal['stage_id'] ){
								$stage_color =  $stage['color'];
						} ?>
						<?php }} ?>
						<?php $stage_name = ( array_key_exists('stage_id',$deal) && $deal['stage_id'] != 0 ) ? $deal['stage_name'] : CRMText::_('COM_CRMERY_CLICK_TO_EDIT'); 
						?>
						<div class="status-dot" style="background-color: #<?php echo $stage_color; ?>"></div><?php echo $stage_name; ?>
					</span>
					<?php 
						echo '<div class="filters" data-item="deal" data-field="stage_id" data-item-id="'.$deal['id'].'" id="deal_stage">';
		                    echo '<ul>';
		                        if ( count($stages) ){ foreach($stages as $key => $stage ) {
		                            echo '<li><a href="javascript:void(0)" class="stage_select dropdown_item" data-value="'.$stage['id'].'">'.$stage['name'].'<div class="status-dot" style="background-color: #'.$stage['color'].'"></div></a></li>';
		                        }}
		                    echo '</ul>';
		                echo '</div>';
	                ?>
				</div>
			</div>
			<div class="crmeryRow" style="display:none;">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_EDIT_PROBABILITY'); ?></div>
				<div class="crmeryValue">
					<span class="editable parent" id="editable_probability_container">
					<div class="inline" id="editable_probability">
						<?php if ( array_key_exists('probability',$deal) && $deal['probability'] != 0 ){ 
								echo $deal['probability'];
							}else{
								echo CRMText::_('COM_CRMERY_CLICK_TO_EDIT');
							} ?>
					</div>%
					<div class="filters editable_info">
						<form id="probability_form">
							<div id="probability_slider"></div>
							<input type="button" class="button" onclick="saveEditableModal('probability_form');" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" />
							<input type="hidden" name="probability" id="probability_input_hidden" value="<?php if(array_key_exists('probability', $deal))echo $deal['probability']; ?>" />
							<span id="probability_label"><?php if ( array_key_exists('probability',$deal) && $deal['probability'] != 0 ) { echo $deal['probability']."%"; } ?></span>
						</form>
					</div>
					</span>
				</div>
			</div>
		</div>
		<div class="threecolumn">
			<div class="small_info last" style="visibility:hidden;">
				<?php $style = "style='display:none;'"; ?>
				<?php if (in_array($deal['stage_id'],$this->closed_stages) ){
					$actual_close = true;
				} else {
					$actual_close = false;
				} ?>
				<div id="actual_close_container"<?php if ( !$actual_close ){ echo $style; } ?>>
					<?php echo CRMText::_('COM_CRMERY_ACT_CLOSE'); ?>:
					<form class="inline_form" name="actual_close_form">
						<input type="text" class="inputbox-hidden date_input" name="actual_close_hidden" id="actual_close" value="<?php echo CrmeryHelperDate::formatDate($deal['actual_close']); ?>" />
						<input type="hidden" name="actual_close" id="actual_close_hidden" value="<?php echo $deal['actual_close']; ?>" />
					</form>
				</div>
				<div  id="expected_close_container"<?php if ( $actual_close ){ echo $style; } ?>>
					<?php echo CRMText::_('COM_CRMERY_EXP_CLOSE'); ?>: 
					<form class="inline_form" name="expected_close_form">
						<input type="text" class="inputbox-hidden date_input" name="expected_close_hidden" id="expected_close" value="<?php echo CrmeryHelperDate::formatDate($deal['expected_close']); ?>" />
						<input type="hidden" name="expected_close" id="expected_close_hidden" value="<?php echo $deal['expected_close']; ?>" />
					</form>
				</div>
			</div>
			<div class="crmeryRow top">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_EDIT_STATUS'); ?></div>
				<div class="crmeryValue">
					<?php if ( array_key_exists('status_id',$deal) && $deal['status_id'] != 0 ){ 
							echo "<a href='javascript:void(0);' class='dropdown' id='deal_status_link'>".$deal['status_name'].'<div class="status-dot" style="background-color:#'.$deal['status_color'].' !important;"></div></a>';
						}else{
							echo "<a href='javascript:void(0);' class='dropdown' id='deal_status_link'>".CRMText::_('COM_CRMERY_CLICK_TO_EDIT')."</a>";
						} ?>
					<?php
						echo '<div class="filters" data-item="deal" data-field="status_id" data-item-id="'.$deal['id'].'" id="deal_status">';
		                    echo '<ul>';
		                        $statuses = CrmeryHelperDeal::getStatuses(null,true);
		                        if (count($statuses)) { foreach($statuses as $id => $status ){
		                            echo '<li><a href="javascript:void(0)" class="status_select dropdown_item" data-value="'.$id.'">'.$status['label'].'<div class="status-dot" style="background-color:#'.$status['color'].' !important;"></div></a></li>';
		                        }}
		                    echo '</ul>';
	                    echo '</div>';
	                ?>
				</div>
			</div>
			<div class="crmeryRow" style="display:none;">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_EDIT_SOURCE'); ?></div>
				<div class="crmeryValue">
					<?php if ( array_key_exists('source_id',$deal) && $deal['source_id'] != 0 ){ 
							echo "<span class='dropdown' id='deal_source_".$deal['id']."_link'>".$deal['source_name']."</span>";
						}else{
							echo "<span class='dropdown' id='deal_source_".$deal['id']."_link'>".CRMText::_('COM_CRMERY_CLICK_TO_EDIT')."</span>";
						} ?>
					<?php
						echo '<div class="filters" data-item="deal" data-field="source_id" data-item-id="'.$deal['id'].'" id="deal_source_'.$deal['id'].'">';
		                    echo '<ul>';
		                        echo '<li><a href="javascript:void(0)" class="source_select dropdown_item" data-value="0">'.CRMText::_('COM_CRMERY_NONE').'</a></li>';
		                        $sources = CrmeryHelperDeal::getSources();
		                        if (count($sources)) { foreach($sources as $id => $name ){
		                            echo '<li><a href="javascript:void(0)" class="source_select dropdown_item" data-value="'.$id.'">'.$name.'</a></li>';
		                        }}
		                    echo '</ul>';
	                    echo '</div>';
	                ?>
				</diV>
			</div>
	</div>
	
	<div class="edit-summary-container">
	<h2><?php echo CRMText::_('COM_CRMERY_EDIT_SUMMARY'); ?></h2>

	<div class="large_info">
		<?php $summary = ( array_key_exists('summary',$deal) && strlen(trim($deal['summary'])) > 0 ) ? $deal['summary'] : CRMText::_('COM_CRMERY_CLICK_TO_EDIT'); ?>
		<div class="inline"><span id="editable_summary"><?php echo nl2br($summary); ?></span></div>
		<div id="editable_summary_area" style="display:none;">
			<form id="summary_form">
				<textarea class="inputbox" name="summary"><?php echo $summary; ?></textarea>
			</form>
			<span class="actions"><input class="button" onclick="saveEditableModal('summary_form');" type="button" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" /></span>
		</div>
	</div>
	</div>
	
	<h2><?php echo CRMText::_('COM_CRMERY_EDIT_NOTES'); ?></h2>
	<?php echo $deal['notes']->display(); ?>

	<h2><?php echo CRMText::_('COM_CRMERY_EDIT_CUSTOM'); ?></h2>

			<div class="columncontainer">
				<?php $this->custom_fields_view->display(); ?>
			</div>
			
	
	<h2><?php echo CRMText::_('COM_CRMERY_EDIT_CONVERSATIONS'); ?></h2>
	<div class="large_info">
			<div id="convo_message" class="message">
				<div id="show_convo_area_button" class="info"><span class="message"><?php echo CRMText::_('COM_CRMERY_ADD_CONVERSATION_MESSAGE'); ?></span></div>
			</div>
	</div>
	<span class="actions"><a class="button" href="javascript:void(0);" id="add_conversation_entry_button" ><?php echo CRMText::_('COM_CRMERY_ADD_CONVO_BUTTON'); ?></a></span>
	
	<div id="conversation_entries">
	<?php
		$c = count($deal['conversations']);
			for ( $i=0; $i<$c; $i++ ) {
				$convo = $deal['conversations'][$i];
				$convo_view = CrmeryHelperView::getView('deals','conversation_entry',array(array('ref'=>'conversation','data'=>$convo)));
				$convo_view->display();	
			} 
	?>
	</div>

	
	<h2><?php echo CRMText::_('COM_CRMERY_EDIT_DOCUMENTS'); ?></h2>
	
    <div class="large_info">
        <table class="com_crmery_table" id="documents_table">
        <thead>
            <th><?php echo CRMText::_('COM_CRMERY_TYPE'); ?></th> 
            <th><?php echo CRMText::_('COM_CRMERY_FILE_NAME'); ?></th>
            <th><?php echo CRMText::_('COM_CRMERY_OWNER'); ?></th>
            <th><?php echo CRMText::_('COM_CRMERY_SIZE'); ?></th>
            <th><?php echo CRMText::_('COM_CRMERY_UPLOADED'); ?></th>
        </thead>
        <tbody id="documents">
           <?php echo $this->document_list->display(); ?>
        </tbody>
        </table>
    </div>
    <span class="actions">
        <form id="upload_form" target="hidden" action="index.php?option=com_crmery&controller=documents&task=uploadDocument&format=raw&tmpl=component" method="POST" enctype="multipart/form-data">
	        <div class="input_upload_button fltrt" >
	            <a href="javascript:void(0);" type="button" class="button" id="upload_button" ><?php echo CRMText::_('COM_CRMERY_UPLOAD_FILE'); ?></a>
	            <input type="file" id="upload_input_invisible" name="document" />
	        </div>
        </form>
    </span>

    <?php if ( isset($this->quote_dock) ){ 
		$this->quote_dock->display();
	}?>


	<?php 
			
		$db = JFactory::getDbo();
		$query_deal = $db->getQuery(true);
	//$query_deal = "SELECT cf.person_id, cp.id FROM #__crmery_people_cf as cf where cf.association_id=".$deal['id']." and cf.association_type='deal'";
	$query_deal = "SELECT cf.person_id, cp.id FROM #__crmery_people_cf as cf, #__crmery_people as cp where cf.association_id=".$deal['id']." and cf.association_type='deal' and cp.id=cf.person_id and cp.published>0 ";
	

		$db->setQuery($query_deal);
		$referrals = $db->loadObjectlist();
		
		$where_qry  = '';
		
		foreach($referrals as $referral)
		{
			$where_qry .= " referral_ID=".$referral->person_id." or";
		}
	
		$year_qry = $db->getQuery(true);
	    $year_qry = "SELECT MIN(YEAR(STR_TO_DATE(BillingYear, '%d/%m/%Y'))) as billdate FROM #__crmery_referral_payments WHERE FeePaid=1 and (".substr($where_qry,0,-2) ." )";
		$db->setQuery($year_qry);
		$start_year = $db->loadResult();
		
	
		 
?>
			
<!-- referral billing history -->

	<div class="container">
	   <h2><?php echo CRMText::_('COM_CRMERY_CLEINT_REFERRAL_PAYMENT_HISTORY'); ?></h2>
	    <div class="large_info">
		   <table id="latest_activity" class="com_crmery_table">
			<tbody>
			   <tr>
			   
				<th style="text-align:left;"><?php echo CRMText::_('COM_CRMERY_BILING_YEAR'); ?></th>
				<th style="text-align:left;"><?php echo CRMText::_('COM_CRMERY_AMOUNT_BILL'); ?> in GBP</th>
			
			</tr>
		    </tbody>
	<?php 
	
	
	if($start_year>0){
	
	
	
		for($start=$start_year;$start<=date('Y');$start++)
		{
			$total_amount = 0;
			$total_amount_year = 0;
			
			foreach($referrals as $referral)
			{
				$total_amount = get_amount_billed_by_referral_id($referral->person_id,$start);
				$total_amount_year = ($total_amount_year+$total_amount);
				//$referral->person_id.'<br><br>';
				 
			}
				echo '<tr class="crmery_row">';
				echo '<td>31/10/'.$start.'</td>';
				echo '<td>&pound; '.number_format($total_amount_year,2,'.',',').'</td>';
				echo '</tr>';	
				//break;
				
	    }
		echo '<script type="text/javascript"> $("#amount_total_top").html(\'&pound; '.$total_amount_year.'\');</script>';
	}
	else{
				echo '<tr class="crmery_row">';
				echo '<td colspan="2" align="center"> No Billing History of this client</td>';
				echo '</tr>';	
	}
	?>		
</table>
	</div>
	</div>
	

<!-- receiving firms -->
	
	<div class="container">
	   <h2><?php echo CRMText::_('COM_CRMERY_RECEIVING_FIRMS'); ?></h2>
	    <div class="large_info">
	    	
	  <table id="latest_activity" class="com_crmery_table">
        <tbody><tr>
            <th style="text-align:left;"><?php echo CRMText::_('COM_CRMERY_RECEIVING_FIRM'); ?></th>
            <th style="text-align:left;"><?php echo CRMText::_('COM_CRMERY_RECEIVING_PARTNER'); ?></th>
            <th style="text-align:left;"><?php echo CRMText::_('COM_CRMERY_PEOPLE_REFERRING_PARTNER'); ?> </th>
            <th style="text-align:left;"><?php echo CRMText::_('COM_CRMERY_AMOUNT_BILL'); ?><br> in GBP(<?php echo date('Y')?>)</th>
            <th style="text-align:left;"><?php echo CRMText::_('COM_CRMERY_CREATED_ON')?></th>
        </tr>
        </tbody>
		<tbody id="latest_activities">
			<?php 
		
				foreach($referrals as $referral)
				{
					$ref_details = $db->getQuery(true);
					$ref_details = "SELECT * FROM #__crmery_people WHERE id=".$referral->person_id." and published>0";

					$db->setQuery($ref_details);
					$refferalls = $db->loadObjectlist();
					$refferall = $refferalls[0];
					$ref_user = get_user_name_by_id($refferall->owner_id);
					$rec_user = get_user_name_by_id($refferall->assignee_id);
					
					$firm_name = get_firm_name_by_id($refferall->company_id);
					$amount = get_amount_billed_by_referral_id($referral->person_id,date('Y'));
					
					echo '<tr class="crmery_row_">';
					echo '<td>'.$firm_name.'</td>';
					echo '<td>'.$rec_user.'</td>';
					echo '<td>'.$ref_user.'</td>';
					echo '<td>&pound; '.number_format($amount,2,'.',',').'</td>';
					echo '<td>'.date('m/d/Y',strtotime($refferall->created)).'</td>';
					echo '</tr>';
					
				}
				
			?>	
        </tbody>    
		</table>

		
		</div>

	</div>
	
	
	
    <div class="container">
	    <h2><?php echo CRMText::_('COM_CRMERY_LATEST_ACTIVITIES'); ?></h2>
	    <div class="large_info">
	    	<?php echo $this->latest_activities->display(); ?>
	    </div>
	</div>

</div>

	<div id="ajax_search_person_dialog" style="display:none;">
			<input class="inputbox" type="text" name="person_name" id="add_person_name" placeholder="<?php echo CRMText::_('COM_CRMERY_BEGIN_TYPING_TO_SEARCH'); ?>" value="" />
			<div class="actions"><input class="button" type="button" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" onclick="addPersonToDeal();"/> <?php echo CRMText::_('COM_CRMERY_OR'); ?> <a href="javascript:void(0);" onclick="closeDialog('person')"><?php echo CRMText::_('COM_CRMERY_CANCEL'); ?></a></div>
	</div>
</div>
</div>

<?php echo CrmeryHelperCrmery::showShareDialog(); ?>
