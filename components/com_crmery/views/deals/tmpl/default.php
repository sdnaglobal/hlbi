<?php



/*------------------------------------------------------------------------

# CRMery

# ------------------------------------------------------------------------

# @author CRMery

# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.

# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Website: http://www.crmery.com

-------------------------------------------------------------------------*/

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' ); ?>



<ul class="entry_buttons">

    <li><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=deals&layout=edit'); ?>"><?php echo ucwords(CRMText::_('COM_CRMERY_ADD_DEALS')); ?></a></li>

    <li style='display:none'><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=import&import_type=deals&layout=import'); ?>"><?php echo ucwords(CRMText::_('COM_CRMERY_IMPORT_DEALS')); ?></a></li>

    </ul>

<h1><?php echo ucwords(CRMText::_('COM_CRMERY_DEALS_HEADER')); ?></h1>

<ul class="filter_lists" >

<li class="filter_sentence">

	<?php echo CRMText::_('COM_CRMERY_SHOW'); ?>

	<span class="filters"><a class="dropdown" id="deal_type_link" ><?php echo $this->deal_type_name; ?></a></span>

	<div class="filters" id="deal_type">

		<ul>

		    <?php foreach ( $this->deal_types as $title => $text ){

			     echo "<li><a class='filter_".$title."' onclick=\"dealType('".$title."')\">".$text."</a></li>";

            }?>							

		</ul>

	</div>

	<?php echo CRMText::_('COM_CRMERY_FOR'); ?>

	<span class="filters"><a class="dropdown" id="deal_user_link"><?php echo $this->user_name; ?></a></span>

	<div class="filters" id="deal_user">

		<ul>

			<li><a class="filter_user_<?php echo $this->user_id ?>" onclick="dealUser(<?php echo $this->user_id ?>,0)"><?php echo CRMText::_('COM_CRMERY_ME'); ?></a></li>

			<?php  if ( $this->member_role != 'basic' ){ ?>

			     <li><a class="filter_user_all" onclick="dealUser('all',0)"><?php echo CRMText::_('COM_CRMERY_ALL_USERS'); ?></a></li>

            <?php } ?>

			<?php

	           if ( $this->member_role == 'exec' ){

                    if ( count($this->teams) > 0 ){

                        foreach($this->teams as $team){

                             echo "<li><a class='filter_team_".$team['team_id']."' onclick='dealUser(0,".$team['team_id'].")'>".$team['team_name'].CRMText::_('COM_CRMERY_TEAM_APPEND')."</a></li>";

                         }

                    }

                }

                if ( count($this->users) > 0 ){

                    foreach($this->users as $user){

                        echo "<li><a class='filter_user_".$user['id']."' onclick='dealUser(".$user['id'].",0)'>".$user['first_name']."  ".$user['last_name']."</a></li>";

                    }

                }

            ?>

		</ul>

	</div>

	<?php echo CRMText::_('COM_CRMERY_IN'); ?>

	<span class="filters"><a class="dropdown" id="deal_stages_link"><?php echo $this->stage_name; ?></a></span>

	<div class="filters" id="deal_stages">

		<ul>

			<?php foreach ( $this->stages as $title => $text ){

			   echo "<li><a class='filter_".$title."' onclick=\"dealStage('".$title."')\">".$text.'</a></li>'; 

			}?>

		</ul>

	</div>

	<?php echo CRMText::_('COM_CRMERY_CLOSING'); ?>

	<span class="filters"><a class="dropdown" id="deal_closing_link"><?php echo $this->closing_name; ?></a></span>

	<div class="filters" id="deal_closing">

		<ul>

			<?php foreach ( $this->closing_names as $title => $text ){

			   echo "<li><a class='filter_".$title."' onclick=\"dealClose('".$title."')\">".$text."</a></li>";

			}?>

		</ul>

	</div>

	<?php echo CRMText::_('COM_CRMERY_NAMED'); ?>

    <td><input class="inputbox filter_input" name="deal_name" type="text" placeholder="<?php echo CRMText::_('COM_CRMERY_ANYTHING'); ?>" value="<?php echo $this->deal_filter; ?>"></td>



</li>

<li class="filter_sentence">

    <div class="ajax_loader"></div>

</li>

</ul>

<div class="subline">

<ul class="matched_results">

    <li><span id="deals_matched"></span> <?php  echo CRMText::_('COM_CRMERY_DEALS_MATCHED'); ?> <?php //echo CRMText::_("COM_CRMERY_THERE_ARE"); ?> <?php //echo $this->totalDeals; ?> <?php //echo CRMText::_("COM_CRMERY_DEALS_IN_ACCOUNT"); ?></li>

</ul>

<span class="actions">

    <span class="filters"><a class="dropdown" id="column_filter_link"><?php echo CRMText::_('COM_CRMERY_SELECT_COLUMNS'); ?></a></span>

        <div class="filters" id="column_filter">

            <ul>

                <?php foreach ( $this->column_filters as $key => $text ){ 

				

					if($key == 'amount' || $key == 'expected_close' || $key == 'actual_close' || $key == 'contacts' )

					  continue;

				?>

                    <?php $selected = ( in_array($key,$this->selected_columns) ) ? 'checked' : ''; ?>

                    <li><input type="checkbox" class="column_filter" id="show_<?php echo $key; ?>" <?php echo $selected; ?> > <?php echo $text; ?></li>    

                <?php } ?> 

            </ul>

        </div>

        <?php if ( CrmeryHelperUsers::canExport() ){ ?>    

            <span class="filters"><a href="javascript:void(0)" onclick="exportCsv()"><?php echo CRMText::_('COM_CRMERY_EXPORT_CSV'); ?></a>

        <?php } ?>

</div>

<?php echo CrmeryHelperTemplate::getListEditActions(); ?>

<form method="post" id="list_form" action="<?php echo JRoute::_('index.php?option=com_crmery&view=deals'); ?>">

<table class="com_crmery_table" id="deals">

    <?php 

	echo $this->deal_list->display(); 

	

	?>

</table>

<input type="hidden" name="list_type" value="deals" />

</form>

<div id="templates" style="display:none;">

    <div id="note_modal" style="display:none;"></div>

    <div id="edit_button"><a class="edit_button_link" id="edit_button_link" href="javascript:void(0)"></a></div>

    <div id="edit_list_modal" style="display:none;" ></div>

    <div id="deal_contacts_modal_dialog" style="display:none;"></div>

</div>