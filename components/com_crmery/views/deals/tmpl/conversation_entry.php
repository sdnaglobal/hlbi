<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$convo = $this->conversation;
echo '<div class="conversation" id="convo_entry_'.$convo['id'].'">';
	if ( CrmeryHelperUsers::getRole() == "exec" || $convo['author'] == CrmeryHelperUsers::getUserId() ){
		echo '<div class="header"><b>'.CrmeryHelperDate::formatDate($convo['created']).'</b>';
			echo '<div class="convo_edit_functions"><a href="javascript:void(0);" onclick="editConvoEntry('.$convo['id'].')">'.CRMText::_('COM_CRMERY_EDIT').'</a> - <a href="javascript:void(0);" onclick="trashConvoEntry('.$convo['id'].')">'.CRMText::_('COM_CRMERY_REMOVE').'</a></div>';
		echo '</div>';
	}
	echo '<div class="convo_info"><b>'.CRMText::_('COM_CRMERY_USER').' '.$convo['owner_first_name'].' '.$convo['owner_last_name'].'</b> '.CRMText::_('COM_CRMERY_WROTE').':</div>';
	echo '<div class="convo">'.nl2br($convo['conversation']).'</div>';											
echo '</div>';
?>