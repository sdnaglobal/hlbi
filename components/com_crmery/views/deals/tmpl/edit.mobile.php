<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$deal = $this->deal;
?>
<h1><?php echo ucwords($deal['header']); ?></h1>
<form id="deal_form" method="POST" name="new_deal" action="<?php echo JRoute::_('index.php?option=com_crmery&controller=main&model=deal&return=deals&task=save'); ?>" target="hidden" onsubmit="save()">
	<div id="editForm">
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DEAL_NAME'); ?></div>
			<div class="crmeryValue wide"><input class="inputbox" type="text" name="name" value="<?php if(count($deal)>0) echo $deal['name']; ?>" /></div>
		</div>
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DEAL_SUMMARY'); ?></div>
			<div class="crmeryValue wide"><textarea class="inputbox" name="summary" cols="50" rows="5"><?php if(count($deal)>0) echo $deal['summary']; ?></textarea></div>
		</div>
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DEAL_COMPANY'); ?></div>
			<div class="crmeryValue">
					<?php
						if ( JRequest::getVar('company_id') ) {
							echo $deal['company_name'];
						}else{ 
							echo CrmeryHelperDropdown::generateDropdown('company',$deal['company_id']);
						} 
					?>
			</div>
		</div>
			<?php if ( array_key_exists('person_id',$deal) && !is_null($deal['person_id']) ) { ?>
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_PERSON')); ?></div>
				<div class="crmeryValue"><?php echo $deal['person_name']; ?></div>
			</div>
			<?php } ?>
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DEAL_AMOUNT'); ?></div>
				<div class="crmeryValue"><input class="inputbox" type="text" name="amount" value="<?php if(count($deal)>0) echo $deal['amount']; ?>" /></div>
			</div>
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DEAL_STAGE'); ?></div>
				<div class="crmeryValue">
					<?php echo CrmeryHelperDropdown::generateDropdown('stage',$deal['stage_id']); ?>
				</div>
			</div>
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DEAL_SOURCE'); ?></div>
				<div class="crmeryValue">
					<?php echo CrmeryHelperDropdown::generateDropdown('source',$deal['source_id']); ?>
				</div>
			</div>
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DEAL_PROBABILITY'); ?></div>
				<div class="crmeryValue">
					<input type="text" id="probability" class="inputbox" name="probability" />
					<div id="slider"></div>
				</div>
			</div>
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DEAL_STATUS'); ?></div>
				<div class="crmeryValue">
					<?php echo CrmeryHelperDropdown::generateDropdown('deal_status',$deal['status_id']); ?>
				</div>
			</div>
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DEAL_CLOSE'); ?></div>
				<div class="crmeryValue"><input class="inputbox" type="text" name="expected_close" value="<?php if(count($deal)>0) echo $deal['expected_close']; ?>"></div>
			</div>
			<?php 
				$custom = CrmeryHelperCustom::generateCustom('deal',$deal['id']);
                $custom_data = ( array_key_exists('id',$deal) ) ? CrmeryHelperCustom::getCustomData($deal['id'],"deal") : array();
                foreach ( $custom as $field => $value ) {
                    if ( $value['type'] != 'forecast' ){
                        $custom_field_filter = ( count($custom_data) != 0 ) ? $custom_data[$value['id']] : '';
                        echo '<div class="crmeryRow">';
                        echo '<div class="crmeryField">'.$value['name'].'</div>';
                        echo '<div class="crmeryValue">';
                            //determine type of input
                            switch ( $value['type'] ){
                                case "text": ?>
                                <input class="inputbox" name="custom_<?php echo $value['id']; ?>" value="<?php echo $custom_field_filter; ?>" />
                                <?php break;
                                case "picklist": ?>
                                        <select id="custom_<?php echo $value['id']; ?>" class="inputbox" name="custom_<?php echo $value['id']; ?>">
                                            <?php echo JHtml::_('select.options', $value['values'], 'value', 'text', $custom_field_filter, true); ?>
                                        </select>
                                <?php break;
                                case "number": ?>
                                <input class="inputbox" name="custom_<?php echo $value['id']; ?>" value="<?php echo $custom_field_filter; ?>" />
                                <?php break;
                                case "currency": ?>
                                <input class="inputbox" name="custom_<?php echo $value['id']; ?>" value="<?php echo $custom_field_filter; ?>" />
                                <?php break;
                                case "date": ?>
                                <!-- make this a custom date picker -->
                                    <input class="inputbox" name="custom_<?php echo $value['id']; ?>" class="filter_input date_input" name="" type="text" value="<?php echo $custom_field_filter; ?>"  />
                                <?php break; ?>
                            <?php } 
                        echo '</div>';
                        echo '</div>';
                } }
			?>
			<span class="actions"><input class="button" type="submit" value="<?php echo CRMText::_('COM_CRMERY_SAVE_BUTTON'); ?>"> <a href="javascript:void(0);" onclick="window.history.back()"><?php echo CRMText::_('COM_CRMERY_CANCEL_BUTTON'); ?></a></span>
	<?php
		if ( array_key_exists('id',$deal) ){
			echo '<input class="inputbox" type="hidden" name="id" value="'.$deal['id'].'" />';
		}
		if ( array_key_exists('person_id',$deal) AND JRequest::getVar('person_id') ) { 
			echo '<input class="inputbox" type="hidden" name="person_id" value="'.$deal['person_id'].'" />';
		}
		if ( array_key_exists('company_id',$deal) AND JRequest::getVar('company_id') ) {
			echo '<input class="inputbox" type="hidden" name="company_id" value="'.$deal['company_id'].'" />';
		}
	?>
	</div>
</form>