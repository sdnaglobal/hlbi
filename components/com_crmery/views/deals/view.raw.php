<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewDeals extends JView
{
	function display($tpl = null)
	{

        	$id = JRequest::getVar('id') ? JRequest::getVar('id') : null;
                $company_id = JRequest::getVar('company_id');
                $person_id = JRequest::getVar('person_id');

                //get deals
                $model =& JModel::getInstance('deal','CrmeryModel');

                $pagination = $model->getPagination();
                $this->assignRef('pagination',$pagination);

                if ( $company_id ){
                        $model->set('user_override','all');
                        $model->set('company_id',$company_id);
                } else if ( $person_id ){
                        $model->set('person_id',$person_id);
                } else if ( $id ){
                        $model->set('_id',$id);
                }

                $deals = $model->getDeals();
                $layout = $this->getLayout();

                $total = $model->getTotal();
                $this->assignRef('total',$total);
                
                //assign references
                switch ( $layout ){
                        case "deal_dock_list":
                                $this->assignRef('deals',$deals);
                        break;
                        case "edit":
                                $this->assignRef('deal',$deals[0]);
                        break;
                        case "edit_conversation":
                                $model =& JModel::getInstance('conversation','CrmeryModel');
                                $conversation = $model->getConversation($id);
                                $this->assignRef('conversation',$conversation[0]);
                        break;
                        case "conversation_entry":
                                $model =& JModel::getInstance('conversation','CrmeryModel');
                                $conversation = $model->getConversation($id);
                                $this->assignRef('conversation',$conversation[0]);
                        break;
                        default:
                                $this->assignRef('dealList',$deals);
                                $state = $model->getState();
                                $this->assignRef('state',$state);
                        break;
                }
                
                //display view
		parent::display($tpl);		
	}
	
}
		
		