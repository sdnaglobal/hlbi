<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="dash_float" id="latest_container">
    <div class="dash_float_header">
       <a class="minify"></a><h2><?php echo CRMText::_('COM_CRMERY_LATEST_HEADER'); ?></h2>
    </div>
    <?php echo $this->latest_activities->display(); ?>
</div>