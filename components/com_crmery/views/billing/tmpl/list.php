<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<?php if ( !(JRequest::getVar('id')) ){ ?>
  <thead>
    <th class="checkbox_column"><input type="checkbox" onclick="selectAll(this);" /></th>
    <th class="avatar" ></th>
    <th class="first_name" ><div class="sort_order"><a class="p.first_name" onclick="sortTable('p.first_name',this)"><?php echo CRMText::_('COM_CRMERY_PEOPLE_FIRST_NAME'); ?></a></div></th>
    <th class="last_name" ><div class="sort_order"><a class="p.last_name" onclick="sortTable('p.last_name',this)"><?php echo CRMText::_('COM_CRMERY_PEOPLE_LAST_NAME'); ?></a></div></th>
    <th class="company"><div class="sort_order"><a class="c.name" onclick="sortTable('c.name',this)"><?php echo CRMText::_('COM_CRMERY_PEOPLE_COMPANY'); ?></a></div></th>
    <th class="owner" ><div class="sort_order"><a class="u.last_name" onclick="sortTable('u.last_name',this)"><?php echo CRMText::_('COM_CRMERY_PEOPLE_OWNER'); ?></a></div></th>
    <th class="assigned" ><div class="sort_order"><a class="u2.last_name" onclick="sortTable('u2.last_name',this)"><?php echo CRMText::_('COM_CRMERY_ASSIGNED_TO'); ?></a></div></th>
    <th class="email" ><div class="sort_order"><a class="p.email" onclick="sortTable('p.email',this)"><?php echo CRMText::_('COM_CRMERY_PEOPLE_EMAIL'); ?></a></div></th>
    <th class="phone" ><div class="sort_order"><a class="p.phone" onclick="sortTable('p.phone',this)"><?php echo CRMText::_('COM_CRMERY_PEOPLE_PHONE'); ?></a></div></th>
    <th class="mobile_phone" ><div class="sort_order"><a class="p.mobile_phone" onclick="sortTable('p.mobile_phone',this)"><?php echo CRMText::_('COM_CRMERY_MOBILE_PHONE'); ?></a></div></th>
    <th class="position" ><div class="sort_order"><a class="p.position" onclick="sortTable('p.position',this)"><?php echo CRMText::_('COM_CRMERY_TITLE'); ?></a></div></th>
    <th class="status" ><div class="sort_order"><a class="stat.ordering" onclick="sortTable('stat.ordering',this)"><?php echo CRMText::_('COM_CRMERY_PEOPLE_STATUS'); ?></a></div></th>
    <th class="source" ><div class="sort_order"><a class="source.ordering" onclick="sortTable('source.ordering',this)"><?php echo CRMText::_('COM_CRMERY_PEOPLE_SOURCE'); ?></a></div></th>
    <th class="type" ><div class="sort_order"><a class="p.type" onclick="sortTable('p.type',this)"><?php echo CRMText::_('COM_CRMERY_PEOPLE_TYPE'); ?></a></div></th>
    <th class="next_task" ><?php echo CRMText::_('COM_CRMERY_PEOPLE_TASK'); ?></th>
    <th class="next_task" ><?php echo CRMText::_('COM_CRMERY_PEOPLE_DUE'); ?></th>
    <th class="notes" ><?php echo CRMText::_('COM_CRMERY_PEOPLE_NOTES'); ?></th>
    <th class="city" ><?php echo CRMText::_('COM_CRMERY_PEOPLE_CITY'); ?></th>
    <th class="state" ><?php echo CRMText::_('COM_CRMERY_PEOPLE_STATE'); ?></th>
    <th class="country" ><?php echo CRMText::_('COM_CRMERY_PEOPLE_COUNTRY'); ?></th>
    <th class="postal_code" ><?php echo CRMText::_('COM_CRMERY_PEOPLE_POSTAL_CODE'); ?></th>
    <th class="added" ><div class="sort_order"><a class="p.created" onclick="sortTable('p.created',this)"><?php echo CRMText::_('COM_CRMERY_PEOPLE_ADDED'); ?></a></div></th>
    <th class="updated" ><div class="sort_order"><a class="p.modified" onclick="sortTable('p.modified',this)"><?php echo CRMText::_('COM_CRMERY_PEOPLE_UPDATED'); ?></a></div></th>
	<th class="billing" ><?php echo CRMText::_('COM_CRMERY_PEOPLE_BILLINGINFO'); ?></th>
</thead>
<tbody>
<?php } ?>
<?php

        // count rows
        $n = count($this->people);
        $k = 0;

        // generate user dropdown
        $me = array(array('label'=>CRMText::_('COM_CRMERY_ME'),'value'=>CrmeryHelperUsers::getLoggedInUser()->id));
        $users = CrmeryHelperUsers::getUsers(null,TRUE);
        $users = array_merge($me,$users);

        $user_html  = "";
        $user_html .= '<ul>';
            if ( count($users) ){ foreach ( $users as $key => $user ){
                $user_html .= '<li><a href="javascript:void(0)" class="owner_select dropdown_item" data-value="'.$user['value'].'">'.$user['label'].'</a></li>';
            }}
        $user_html .= '</ul>';

        //generate status dropdown
        $statuses = CrmeryHelperPeople::getStatusList();

        $status_html  = "";
        $status_html .= '<ul>';
            $status_html .= '<li><a href="javascript:void(0)" class="status_select dropdown_item" data-value="0"><div class="person-status-none"></div></a></li>';
            if (count($statuses)) { foreach($statuses as $key => $status ){
                $status_html .= '<li><a href="javascript:void(0)" class="status_select dropdown_item" data-value="'.$status['id'].'"><div class="person-status-color" style="background-color:#'.$status['color'].'"></div><div class="person-status">'.$status['name'].'</div></a></li>';
            }}
        $status_html .= '</ul>';

        //generate source dropdown
        $sources = CrmeryHelperDeal::getSources();

        $source_html = "";
        $source_html .= '<ul>';  
            if ( count($sources) ){ foreach($sources as $id => $name ) {
                $source_html .= '<li><a href="javascript:void(0)" class="source_select dropdown_item" data-value="'.$id.'">'.$name.'</a></li>';
            }}
        $source_html .= '</ul>';

        //generate type dropdown
        $types = CrmeryHelperPeople::getPeopleTypes(FALSE);

        $type_html = "";
        $type_html .= '<ul>';
            if ( count($types) ){ foreach($types as $id => $name ) {
                $type_html .= '<li><a href="javascript:void(0)" class="type_select dropdown_item" data-value="'.$id.'">'.ucwords($name).'</a></li>';
            }}
        $type_html .= '</ul>';

        //base url
        $base = JURI::base();

            if ( count($this->people) > 0 ){foreach($this->people as $key => $person){ 

                $k = $key%2;

                //assign null data
                $person['company_name'] = ( $person['company_name'] == '' ) ? CRMText::_('COM_CRMERY_NO_COMPANY') : $person['company_name'];
                $person['status_name'] = ( $person['status_name'] == '' ) ? CRMText::_('COM_CRMERY_NO_STATUS') : $person['status_name'];
                $person['source_name'] = ( $person['source_name'] == '' ) ? CRMText::_('COM_CRMERY_NO_SOURCE') : $person['source_name'];

                if ( array_key_exists('avatar',$person) && $person['avatar'] != "" && $person['avatar'] != null ){
                    $avatar_html = '<td class="avatar" ><img id="avatar_img_'.$person['id'].'" data-item-type="people" data-item-id="'.$person['id'].'" class="avatar" src="'.$base.'components/com_crmery/media/avatars/'.$person['avatar'].'"/></td>';
                }else{
                    $avatar_html = '<td class="avatar" ><img id="avatar_img_'.$person['id'].'" data-item-type="people" data-item-id="'.$person['id'].'" class="avatar" src="'.$base.'components/com_crmery/media/images/person.png'.'"/></td>';
                }

                $person['mobile_phone'] = $person['mobile_phone'] > 0 ? $person['mobile_phone'] : "";
                
                $company_link = $person['company_id'] > 0 ? "<a href='".JRoute::_('index.php?option=com_crmery&view=companies&layout=company&id='.$person['company_id'])."'>".$person['company_name']."</a>" : $person['company_name'];
                echo "<tr id='list_row_".$person['id']."' class='crmery_row_".$k."'>",
                     '<td><input type="checkbox" name="ids[]" value="'.$person['id'].'" /></td>',
                        $avatar_html,
                     '<td class="first_name list_edit_button" id="list_'.$person['id'].'" ><div class="title_holder"><a href="'.JRoute::_('index.php?option=com_crmery&view=people&layout=person&id='.$person['id']).'">'.$person['first_name'].'</a></div></td>',
                     '<td class="last_name list_edit_button" id="list_'.$person['id'].'" ><div class="title_holder"><a href="'.JRoute::_('index.php?option=com_crmery&view=people&layout=person&id='.$person['id']).'">'.$person['last_name'].'</a></div></td>',
                     "<td class='company' >",$company_link,"</td>",
                     "<td class='owner' ><a href='javascript:void(0)' class='dropdown' id='person_owner_".$person['id']."_link'>".$person['owner_first_name']." ".$person['owner_last_name']."</a></td>",
                     '<div class="filters" data-item="people" data-field="owner_id" data-item-id="'.$person['id'].'" id="person_owner_'.$person['id'].'">',
                        $user_html,
                     '</div>',
                     "<td class='assigned'>","<a href='javascript:void(0)' class='dropdown' id='person_assigned_".$person['id']."_link'>".$person['assignee_name']."</a></td>",
                     '<div class="filters" data-item="people" data-field="assignee_id" data-item-id="'.$person['id'].'" id="person_assigned_'.$person['id'].'">',
                        $user_html,
                     '</div>',
                     '<td class="email">'.$person['email'].'</td>',
                     '<td class="phone">'.$person['phone'].'</td>',
                     '<td class="mobile_phone">'.$person['mobile_phone'].'</td>',
                     '<td class="position">'.$person['position'].'</td>',
                     "<td class='status' ><a href='javascript:void(0);' class='dropdown' id='person_status_".$person['id']."_link'><div class='person-status-color' style='background-color:#".$person['status_color']."'></div><div class='person-status'>".$person['status_name']."</div></a></td>",
                     '<div class="filters" data-item="people" data-field="status_id" data-item-id="'.$person['id'].'" id="person_status_'.$person['id'].'">',
                         $status_html,
                     '</div>',
                     "<td class='source' ><a href='javascript:void(0);' class='dropdown' id='person_source_".$person['id']."_link'>".$person['source_name']."</a></td>",
                     '<div class="filters" data-item="people" data-field="source_id" data-item-id="'.$person['id'].'" id="person_source_'.$person['id'].'">',
                         $source_html,
                     '</div>',
                     "<td class='type' ><a href='javascript:void(0);' class='dropdown' id='person_type_".$person['id']."_link'>".ucwords($person['type'])."</a></td>",
                     '<div class="filters" data-item="people" data-field="type" data-item-id="'.$person['id'].'" id="person_type_'.$person['id'].'">',
                         $type_html,
                     '</div>',
                     '<td class="next_task" ><a onclick="editEvent('.$person['event_id'].',\''.$person['event_type'].'\')">'.$person['event_name']."</a></td>",
                     '<td class="next_task" >'.CrmeryHelperDate::formatDate($person["event_due_date"]).'</td>',
                     '<td class="notes" ><a href="javascript:void(0);" onclick="openNoteModal(\''.$person['id'].'\',\'person\')"><img src="'.$base.'components/com_crmery/media/images/notes.png'.'"/></a></td>',
                     '<td class="city">'.$person['work_city'].'</td>',
                     '<td class="state">'.$person['work_state'].'</td>',
                     '<td class="postal_code">'.$person['work_zip'].'</td>',
                     '<td class="country">'.$person['work_country'].'</td>',
                     '<td class="added">'.CrmeryHelperDate::formatDate($person['created']).'</td>',
                     '<td class="updated">'.CrmeryHelperDate::formatDate($person['modified']).'</td>',
                     '<td class="billing"><a href="'.JRoute::_('index.php?option=com_crmery&view=billing&layout=person&id='.$person['id']).'">'.CRMText::_('COM_CRMERY_PEOPLE_BILLINGINFO').'</a></td>',
                 "</tr>";

                //free up php memory
                unset($person);
                unset($this->people[$key]);
            } }
        ?>
<?php if ( !(JRequest::getVar('id')) ){ ?>
    </tbody>
<tfoot>
    <tr>
       <td colspan="20"><?php echo $this->pagination->getListFooter(); ?></td>
    </tr>
 </tfoot>
<script type="text/javascript">
    //update total people count
    jQuery("#people_matched").empty().html("<?php echo $this->total; ?>");
    window.top.window.assignFilterOrder();
</script>
<?php } ?>