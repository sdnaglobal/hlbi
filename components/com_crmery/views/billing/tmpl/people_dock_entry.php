<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$person = $this->person;
echo '<tr class="crmery_row_'.$this->k.'">';
		if ( array_key_exists('avatar',$person) && $person['avatar'] != "" ){
			echo '<td><img src="'.JURI::base().'components/com_crmery/media/avatars/'.$person['avatar'].'"/></td>';
		}else{
			echo '<td><img src="'.JURI::base().'components/com_crmery/media/images/person.png'.'"/></td>';
		}
		echo '<td><a href="'.JRoute::_('index.php?option=com_crmery&view=people&layout=person&id='.$person['id']).'">'.$person['first_name'] . ' ' . $person['last_name'] . '</a></td>';
		echo '<td>'.$person['position'].'</td>';
		echo '<td>'.$person['phone'].'</td>';
		echo '<td>'.$person['owner_first_name'].' '.$person['owner_last_name'].'</td>';
		echo '<td>'.ucwords($person['type']).'</td>';
		echo '<td>'.CrmeryHelperDate::formatDate($person['modified']).'</td>';
	echo '</tr>';
?>