<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<table class="com_crmery_table">
	<th></th>
	<th><?php echo CRMText::_('COM_CRMERY_PEOPLE_NAME'); ?></th>
	<th><?php echo CRMText::_('COM_CRMERY_TITLE'); ?></th>
	<th><?php echo CRMText::_('COM_CRMERY_PEOPLE_PHONE'); ?></th>
	<th><?php echo CRMText::_('COM_CRMERY_PEOPLE_OWNER'); ?></th>
	<th><?php echo CRMText::_('COM_CRMERY_PEOPLE_TYPE'); ?></th>
	<th><?php echo CRMText::_('COM_CRMERY_PEOPLE_CONTACTED'); ?></th>
	<tbody id="people_list">
	<?php
		$deal_dock_list = CrmeryHelperView::getView('people','people_dock_list',array(array('ref'=>'people','data'=>$this->people)));
		$deal_dock_list->display();
	?>
	</tbody>
</table>