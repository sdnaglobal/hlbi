<?php

/*------------------------------------------------------------------------

# CRMery

# ------------------------------------------------------------------------

# @author CRMery

# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.

# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Website: http://www.crmery.com

-------------------------------------------------------------------------*/

// no direct access



defined( '_JEXEC' ) or die( 'Restricted access' ); 



jimport( 'joomla.filesystem.file' );



$db =& JFactory::getDbo();



$id = JRequest::getVar('id');

$lastid = 0;



$BillingYear = '';

$LocalAmount = '';

$LocalCurrencyCode = '';

$HLBFeeRate = 10;

$FeePaid = 1;

$ReferralFee = 10;



$editbilling = false;


$current_financial_year_pure = CrmeryHelperCrmery::getFinancialYear();
$current_financial_date = explode("-",$current_financial_year_pure);
$current_financial_year = $current_financial_date[0];


if(isset($_GET['deleteAtt']))

{	

	$db =& JFactory::getDbo();

	$db->getQuery(true);

	$del = "UPDATE #__crmery_referral_payments set `attachment`=''";

	$deleteAtt = (int)$_GET['deleteAtt'];	



	$del .= " WHERE `id`='$deleteAtt'";

	 

	$db->setQuery($del); 

	if ($db->query()){  

		$msg = 'Selected Attachment deleted successfully.';

	    $allDone =& JFactory::getApplication();

	    $allDone->redirect(JRoute::_('index.php?option=com_crmery&view=billing&id='.$id),$msg);

	}

			

}





if(isset($_GET['edit']))

{

	$editid = $_GET['edit'];

	$editqry = $db->getQuery(true);

	$editqry->select("b.*")

	->from("#__crmery_referral_payments AS b");

	$editqry->where("b.id='".$editid."'");						



	$db->setQuery($editqry);

	$edit_rs = $db->loadObjectList();

	$rs = $edit_rs[0];

	

	

	$BillingYear = $rs->BillingYear;

	$LocalAmount = $rs->LocalAmount;

	$LocalCurrencyCode = $rs->LocalCurrencyCode;

	$HLBFeeRate = $rs->HLBFeeRate;

	$FeePaid = $rs->FeePaid;

	$ReferralFee = $rs->ReferralFee;

	$comments = $rs->comments;

	$currdate = date('Y-m-d H:i:s');

	$last_updatedby = CrmeryHelperUsers::getUserId();

	

	$editbilling = true;

	

}



$user_id = CrmeryHelperUsers::getUserId();

$member_role = CrmeryHelperUsers::getRole();



 

if(isset($_POST['savebilling']))

{

	$BillingYear = $_POST['BillingYear'];

	$LocalAmount = $_POST['LocalAmount'];
	
	$servicetype = $_POST['servicetype'];

	$LocalCurrencyCode = $_POST['LocalCurrencyCode'];

	$HLBFeeRate = $_POST['HLBFeeRate'];

	$FeePaid = $_POST['FeePaid'];

	$ReferralFee = $_POST['ReferralFee'];

	$comments = $_POST['comments'];

	$filename = '';

	

	if(isset($_FILES['billingattachment']) && $_FILES['billingattachment']['tmp_name'] != '')

	{

		$attachment_src = $_FILES['billingattachment']['tmp_name'];

		$filename = time().'-'.$_FILES['billingattachment']['name'];

		$attachment_dest = JPATH_BASE.DS.'media'.DS.$filename;

		

		JFile::upload($attachment_src, $attachment_dest, $use_streams=false);

	}

	 

	$currdate = CrmeryHelperDate::formatDBDate(date('Y-m-d H:i:s'));

	 

	$ccode = $db->getQuery(true);

	$ccode->select('value_in_GBP');

	$ccode->from('#__crmery_currencies');

	$ccode->where("currency_code='$LocalCurrencyCode'");

	$db->setQuery($ccode);

	//echo '<br>'.$ccode;

	$ccodevalue = $db->loadObjectList();

	

	$ccode_value = $ccodevalue[0];

	$gbp = $ccode_value->value_in_GBP;

	//$bill->LocalAmount;

	$SterlingAmount = ($gbp*$LocalAmount); 

	$HLBFee_Amount = ($LocalAmount*$HLBFeeRate/100); 

	$HLBFeeAmount = ($gbp*$HLBFee_Amount); 

	$ReferralFee_Amount = ($LocalAmount*$ReferralFee/100); 

	$ReferralFeeAmount = ($gbp*$ReferralFee_Amount); 

	

	if(isset($_GET['edit'])){

		$insert = "UPDATE #__crmery_referral_payments set ";

		$set = " `LastUpdatedBy`='$user_id',

				 `UpdatedOn`='$currdate',";

		$edit = (int)$_GET['edit'];		 

		$where  = " WHERE `id`='$edit'";		 

	}else{

		$insert = "INSERT INTO #__crmery_referral_payments set ";

		$set = " `BillingAddedBy`='$user_id',

				 `AddedDate`='$currdate',	";

		$where  = " ";		 

	}

	$set .="	`referral_ID`='$id',

				`BillingYear`='$BillingYear',

				`LocalAmount`='$LocalAmount',
				
				`servicetype`='$servicetype',

				`LocalCurrencyCode`='$LocalCurrencyCode',

				`SterlingAmount`='$SterlingAmount',

				`HLBFeeRate`='$HLBFeeRate',

				`HLBFeeAmount`='$HLBFeeAmount',

				`FeePaid`='$FeePaid',	

				`comments`='$comments',	

				`ReferralFeeAmount`='$ReferralFeeAmount',

				`ReferralFee`='$ReferralFee'";

	

	if($filename != '')

		$set .= ",`attachment`='$filename'";

		

	$newqry = $insert.$set.$where;  

	

	$db->setQuery($newqry);  

		

		if ($db->query()){  

			if(isset($_GET['edit'])){

				$lastid = $_GET['edit'];

				

				$insert_history = "INSERT INTO #__crmery_history set 

							`type`='person',

							`type_id`='$id',

							`user_id`='$user_id',

							`date`='$currdate',

							`action_type`='updated',

							`old_value`='Billing details',

							`new_value`='Updated',

							`field`='Billing'";

				$db->setQuery($insert_history);  

				$db->query();

				

				

				$msg = 'Billing details updated successfully.';

			}	

			else

			{

				$lastid = $db->insertid();

				

				 // insert into history table 

				$insert_history = "INSERT INTO #__crmery_history set 

							`type`='person',

							`type_id`='$id',

							`user_id`='$user_id',

							`date`='NOW()',

							`action_type`='created',

							`field`='id'";

				$db->setQuery($insert_history);  

				$db->query();

				

				$msg = 'Billing details added successfully.';

			}			

			$allDone =& JFactory::getApplication();

			$allDone->redirect(JRoute::_('index.php?option=com_crmery&view=billing&id='.$id),$msg);

		}

		

}

 

if($id >0 ){

	$query = $db->getQuery(true);

	$query->select("r.first_name,r.assignee_id,r.company_id")

	->from("#__crmery_people AS r");

	$query->where("r.id='".$id."'");						



	$db->setQuery($query);

	$refer_rs = $db->loadObjectList();



	$refername   = $refer_rs[0]->first_name;

	$assignee_ID = $refer_rs[0]->assignee_id;

	$company_ID  = $refer_rs[0]->company_id;

	

	$usercanaddbilling = false;

	$inp_company_id = 0;

	

	if($member_role == 'basic')

	{

		$db =& JFactory::getDbo();

		$qry = $db->getQuery(true);

		$qry->select("inp_company_id")

		->from("#__crmery_users ");

		$qry->where("id='".$user_id."'");						

 

		$db->setQuery($qry);

		$basic_rs = $db->loadObjectList();

 

		$inp_company_id = $basic_rs[0]->inp_company_id;

		 

	}

	

	if($assignee_ID == $user_id || $company_ID == $inp_company_id)

		$usercanaddbilling = true;

	

	$querys = $db->getQuery(true);

	$querys->select("b.*")

	->from("#__crmery_referral_payments AS b");

	$querys->where("b.referral_ID='".$id."'");						



	$db->setQuery($querys);

	$billing_rs = $db->loadObjectList();

}



if(!isset($_GET['edit']) && !isset($_POST['savebilling']))

{

	$dbus =& JFactory::getDbo();

	$queryus = $dbus->getQuery(true);

	$queryus->select("u.country_id")

	->from("#__crmery_users AS u");

	$queryus->where("u.id='".$user_id."'");						



	$dbus->setQuery($queryus);

	$user_rs = $dbus->loadObjectList();

 

    $country_ids = $user_rs[0]->country_id;



	if(strpos(',',$country_ids) !== false){

		$cnt_ids =  @explode(',',$country_ids);

		$user_country_id = $cnt_ids[0];

	}else{

		$user_country_id = $country_ids;

	}

	

	if($user_country_id > 0)

	{

		$query = $db->getQuery(true);

		$query->select("u.currency_code")

		->from("#__crmery_currencies AS u");

		$query->where("u.country_id='".$user_country_id."'");						



		$db->setQuery($query);

		$refer_rs = $db->loadObjectList();



		$LocalCurrencyCode = $refer_rs[0]->currency_code;

		 

	}

}	

//echo '---->>'.$LocalCurrencyCode;

	

// echo '<pre>';

// print_r($billing_rs);

// echo '</pre>';

  

   

	$codequery = $db->getQuery(true);

	$codequery->select("cc.currency_code")

	->from("#__crmery_currencies AS cc");

							



	$db->setQuery($codequery);

	$codequery_rs = $db->loadObjectList();

	

   $codeArr = array();

   foreach($codequery_rs as $ccode)

   {

		$codeArr[$ccode->currency_code] = $ccode->currency_code;

   }

   

?>



<h1> View Billing Details For "<?php //echo $refername?> <?php echo CRMText::_('COM_CRMERY_REFERRAL_CODE')?>:

<?php echo CRMText::_('COM_CRMERY_REFERRAL_CODE_PREFIX').$id; ?>"</h1>

  

<form method="post" id="list_form" action="<?php //echo JRoute::_('index.php?option=com_crmery&view=billing&task=savebilling&id='.$id); ?>" enctype="multipart/form-data">

<div id="editForm">

<?php if($editbilling === false) { ?>

<table class="com_crmery_table" id="people">

  <thead>

    <tr>

	<!--	<th>Referal Name </th> -->

		<th>Billing Added By </th>
		
		<th>Reporting Year</th>

		<th>Service Type</th>
		
		<th>Amount</th>

		<th>Currency</th>

		<th>Amount in GBP</th>

	<!--<th>HLB Fee</th> 

		<th>HLB Fee Amount in GBP</th>-->

		<th>Referral Fee Amount in GBP</th>

		<th>Billings</th>

		<th>added date</th>

		<th>Attachment</th>

		<th>Comments</th>

	<!--<th>Updated by </th>

		<th>Updated on</th> -->

	<?php	if(	$member_role == ''){ ?>

		<th>Action</th>

	<?php } ?>	

	</tr>

 </thead>  

 <?php 

 if(count($billing_rs)>0){



	foreach($billing_rs as $billing){

		if($billing->FeePaid == 1)

			$fee = 'Yes';

		else

			$fee = 'No';

			

		if($billing->BillingAddedBy > 0)

			$BillingAddedBy = CrmeryHelperUsers::getFirstName($billing->BillingAddedBy);

		else 

			$BillingAddedBy = '';

			

		if($billing->LastUpdatedBy > 0)

			$LastUpdatedBy = CrmeryHelperUsers::getFirstName($billing->LastUpdatedBy);

		else

			$LastUpdatedBy = 'Not Updated';

		

		$attachment = 'N/A';

		if($billing->attachment != '' && file_exists(JPATH_BASE.DS.'media'.DS.$billing->attachment))

		{

			$attach = explode('-',$billing->attachment,2);

			$attachmentLink = JURI::base().'media/'.$billing->attachment;

			$attachment = '<a target="_blank" href="'.$attachmentLink.'" >'.$attach[1].'</a>';

			$attachment .= ' | <a href="'.JRoute::_('index.php?option=com_crmery&view=billing&id='.$id.'&deleteAtt='.$billing->id).'" ><span style="color:red;">(Delete)</a></a>';

			

		}	

		

		echo '<tr>';

	//	echo '<td> '.$refername.' </td>';

		echo '<td> '.$BillingAddedBy.' </td>';

		echo '<td>'.$billing->BillingYear.'</td>';
		
		echo '<td>'.$billing->servicetype.'</td>';

		echo '<td>'.number_format($billing->LocalAmount,2,'.',',').'</td>';

		echo '<td>'.$billing->LocalCurrencyCode.'</td>';

		echo '<td>'.$billing->SterlingAmount.'</td>';

		//echo '<td>'.$billing->HLBFeeAmount.' ('.$billing->HLBFeeRate.'%)</td>';

		echo '<td>'.$billing->ReferralFeeAmount.' ('.$billing->ReferralFee.'%)</td>';

		echo '<td>'.$fee.'</td>';

		echo '<td>'.date('Y-m-d',strtotime($billing->AddedDate)).'</td>';

		echo '<td>'.$attachment.'</td>';

		echo '<td>'.$billing->comments.'</td>';

		//echo '<td>'.$LastUpdatedBy.'</td>';

		//echo '<td>'.substr($billing->UpdatedOn,0,11).'</td>';

		

		if($member_role == '')
		{

			echo '<td><a href="'.JRoute::_('index.php?option=com_crmery&view=billing&id='.$id.'&edit='.$billing->id).'" >Update</a></td>';

		}
		

		echo '</tr>';

	 }

 

 }

 else

 {

	echo '<tr><td colspan="12" align="center">No Billings added yet!</td> </tr>';

 }

 ?>

</table>

<br />

<br />

<br />

<br />



<?php } ?>



<?php //if( ( $member_role == 'basic' || $member_role == 'manager' ) && $usercanaddbilling == true){ ?> 

<div class="crmeryRow" id="other_button">

<?php if($editbilling) {

 echo '<h4>Edit Billing Details </h4>';

}else{

echo '<h4>Add Billing Details</h4>';

}

 ?>

</div>

<?php if($lastid =0){ ?>

<div style="background:#D6EDF2;padding:10px;" >

<h4><?php echo $submitmsg ?>Billing Details have been added. </h4>

</div>

<?php } ?>





		<div class="crmeryRow" id="other_button">

				<div class="crmeryField"> Reporting Year<?php
							
				  //echo CRMText::_('COM_CRMERY_PERSON_BILLINGYEAR'); ?></div>

				<div class="crmeryValue">
					<?php //echo $BillingYear ; ?>
					<select name="BillingYear" class="inputbox" id="BillingYear">

						<?php

							$yyyy = $current_financial_year;

							if($editbilling)
							{
								$ye = '2013';
							}
							else
							{
								$ye = $current_financial_year;
							}
							
							
							for($y=$yyyy;$y>=$ye;$y--){

							if('31/10/'.$y == $BillingYear)

								echo '<option selected="selected" value="31/10/'.$y.'">31/10/'.$y.'</option>';

							else	

								echo '<option value="31/10/'.$y.'">31/10/'.$y.'</option>';

							$yy--;

						} ?>

					</select>

		 		</div> 

				

				<!-- <div class="crmeryValue">

					<input class="inputbox date_input" type="text" id="expected_close" name="BillingYear" value="<?php //echo $BillingYear; ?>">

					<input type="hidden" id="expected_close_hidden" name="expected_close" value="" />

				</div> -->

				

		</div>

		<div class="crmeryRow" id="other_button">

				<div class="crmeryField"> <?php echo CRMText::_('COM_CRMERY_PERSON_SERVICETYPE'); ?></div>

				<div class="crmeryValue">
				<?php
				$custom = CrmeryHelperCustom::generateCustom('people','');
				$all = array();
				$all = $custom[1]['values'];
				?>
       		 	<select name="servicetype" required>
				<option value="">Select Service Type</option>
				
				<?php
				foreach ( $all as $value ) 
				{
				?>		 
					 <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
				<?php
				}
				
				?>
				</select>	
				</div>

		</div>

		
		

		<div class="crmeryRow" id="other_button">

				<div class="crmeryField"> <?php echo CRMText::_('COM_CRMERY_PERSON_LOCALAMOUNT'); ?></div>

				<div class="crmeryValue"><input class="inputbox" required type="text" name="LocalAmount" value="<?php echo $LocalAmount?>" /> (Enter whole values only,do not enter any comma or dot separators)</div>

		</div>

		

		<div class="crmeryRow" id="other_button">

				<div class="crmeryField"> <?php echo CRMText::_('COM_CRMERY_PERSON_LOCALCURRENCYCODE'); ?></div>

				<div class="crmeryValue">

				 <select name="LocalCurrencyCode" >

				 <?php foreach($codeArr as $code) {

					if($code == $LocalCurrencyCode)

						echo '<option selected="selected" value="'.$code.'">'.$code.' </option>';

					else

						echo '<option value="'.$code.'">'.$code.' </option>';

					

				  } ?>

				  </select>

				

			 

				</div>

		</div>

	<?php if($member_role == ' ' || $member_role == '') {?>	

		<div class="crmeryRow" id="other_button">

				<div class="crmeryField"> <?php echo CRMText::_('COM_CRMERY_PERSON_HLBFEERATE'); ?></div>

				<div class="crmeryValue"><input class="inputbox" type="text" name="HLBFeeRate" value="<?php echo $HLBFeeRate?>" /> %(do not add % symbol)</div>

		</div>



		<div class="crmeryRow" id="other_button">

				<div class="crmeryField"> Referral Fee</div>

				<div class="crmeryValue">

				

				<input class="inputbox" type="text" name="ReferralFee" value="<?php if($ReferralFee > 0) echo $ReferralFee; else echo 10; ?>" />  %(do not add % symbol)

				

				</div>

		</div>





		<?php }else { ?>

 <input class="inputbox" type="hidden" name="HLBFeeRate" value="<?php if($HLBFeeRate > 0) echo $HLBFeeRate; else echo 10; ?>" />

 

 		<input class="inputbox" type="hidden" name="ReferralFee" value="<?php if($ReferralFee > 0) echo $ReferralFee; else echo 10; ?>" />

				

				

 <?php } ?>

		

		

		

		<div class="crmeryRow" id="other_button">

				<div class="crmeryField"> <?php echo CRMText::_('COM_CRMERY_FIRM_BILLINGS'); ?></div>

				<div class="crmeryValue">

				<select name="FeePaid" ><option <?php if($FeePaid == 1) echo 'selected="selected"'; ?> value="1">Yes</option> <option value="0" <?php  if($FeePaid == 0) echo 'selected="selected"'; ?> >No</option> </select>

				

				</div>

		</div> 

		

		<div class="crmeryRow" id="other_button">

				<div class="crmeryField"> <?php echo CRMText::_('Attachment(optional)'); ?></div>

				<div class="crmeryValue">

				<input type="file" name="billingattachment" />

				

				</div>

		</div> 

		

		<div class="crmeryRow" id="other_button">

				<div class="crmeryField">Comments </div>

				<div class="crmeryValue"><textarea name="comments" ><?php echo $comments?> </textarea> </div>

		</div>

		

		<span class="actions"><input type="submit" name="savebilling" value="Save" class="button"> <a onclick="window.history.back()" href="javascript:void(0);">Cancel</a>

				

	</span>

	

<?php //} // end if role ?>	

	

	

	</div> 



</form>



 



