<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<table class="com_crmery_table">
	<thead>
		<tr>
			<th><?php echo CRMText::_('COM_CRMERY_NAME'); ?></th>
			<th><?php echo CRMText::_('COM_CRMERY_URL'); ?></th>
			<th><?php echo CRMText::_('COM_CRMERY_CLICKED'); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		    if ( count($this->links) > 0 ){
		        foreach($this->links as $link){
		        	$text = $link->click ? CRMText::_('COM_CRMERY_YES') : CRMText::_('COM_CRMERY_NO');
		        	echo '<tr>';
		        		echo "<td>".$link->name."</td>";
		            	echo "<td><a target='_blank' href='$link->url'>".$link->url."</a></td>";
		            	echo "<td>".$text."</td>";
		            echo '</tr>';
		        }
		    }else{
		    	echo '<tr>';
		    		echo '<td colspan="3">';
		    		echo '<div class="notice">'.CRMText::_('COM_CRMERY_NO_NEWSLETTER_LINKS').'</div>';
		    		echo '</td>';
	    		echo '</tr>';
		    } 
		?>
	</tbody>
</table>