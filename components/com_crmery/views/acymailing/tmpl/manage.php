<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<table class="com_crmery_table">
	<thead>
		<tr>
			<th></th>
			<th><?php echo CRMText::_('COM_CRMERY_NAME'); ?></th>
			<th><?php echo CRMText::_('COM_CRMERY_DESCRIPTION'); ?></th>
			<th><?php echo CRMText::_('COM_CRMERY_ACTIONS'); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		    if ( count($this->lists) > 0 /* && !$this->lists->error */ ){
		        foreach($this->lists as $list){
		        	$jsaction = $list->isSubscribed > 0 ? 1 : 0;
		        	$action = $jsaction ? CRMText::_('COM_CRMERY_REMOVE') : CRMText::_('COM_CRMERY_ADD');
		        	echo '<tr>';
		        		echo "<td><div class='status-dot' style='background-color:".$list->color.";'></div></td>";
		            	echo "<td>".$list->name."</td>";
		            	echo "<td>".$list->description."</td>";
		            	echo "<td id='mailing_list_".$list->listid."'><a href='javascript:void(0);' onclick=\"toggleMailingList('".$list->listid."','".$jsaction."')\">".$action."</a></td>";
		            echo '</tr>';
		        }
		    }
		?>
	</tbody>
</table>