<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewAcymailing extends JView
{
	function display($tpl = null)
	{
		$layout = $this->getLayout();

		switch ( $layout ){
			case "manage":
				$this->lists = CrmeryHelperMailinglists::getMailingLists(TRUE);
			break;
			case "links":
				$this->links = CrmeryHelperMailinglists::getLinks();
			break;
			case "list":
				$this->newsletters = CrmeryHelperMailinglists::getNewsletters();
			break; 
			default:
				$this->mailing_lists = CrmeryHelperMailinglists::getMailingLists();
				$key = array_key_exists(0,$this->mailing_lists) ? $this->mailing_lists[0]->listid : null;
				$this->newsletters = CrmeryHelperMailingLists::getNewsLetters($key);
			break;
		}
	
		//display
		parent::display($tpl);
	}
	
}
?>
