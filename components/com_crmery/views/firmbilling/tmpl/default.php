<?php

/*------------------------------------------------------------------------

# CRMery

# ------------------------------------------------------------------------

# @author CRMery

# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.

# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Website: http://www.crmery.com

-------------------------------------------------------------------------*/

// no direct access



defined( '_JEXEC' ) or die( 'Restricted access' ); 



jimport( 'joomla.filesystem.file' );



$db =& JFactory::getDbo();



$id = JRequest::getVar('id');

$lastid = 0;



$BillingYear = '';

$feeincome = '';

$LocalCurrencyCode = '';

$additionalexpenses = '';

$memberincome = '';

$otherincome = '';



$editbilling = false;

$current_user_id = CrmeryHelperUsers::getUserId();

	

if(isset($_GET['edit']))

{

	$editid = $_GET['edit'];

	$editqry = $db->getQuery(true);

	$editqry->select("b.*")

	->from("#__crmery_companies_billing AS b");

	$editqry->where("b.id='".$editid."'");						

	$db->setQuery($editqry);

	$edit_rs = $db->loadObjectList();

	$rs = $edit_rs[0];

	
	$billingtype = $rs->billingtype;

	$BillingYear = $rs->BillingYear;

	$feeincome = $rs->gross_fee_income;

	$LocalCurrencyCode  = $rs->LocalCurrencyCode;

	$additionalexpenses = $rs->additionalexpenses;

	$memberincome = $rs->memberincome;

	$otherincome  = $rs->otherincome;

	$currdate = date('Y-m-d H:i:s');

	$last_updatedby = CrmeryHelperUsers::getUserId();

	

	$editbilling = true;

	

}



$user_id = CrmeryHelperUsers::getUserId();

$member_role = CrmeryHelperUsers::getRole();



 

if(isset($_POST['savebilling']))
{

	$BillingYear = $_POST['BillingYear'];

	$feeincome   = $_POST['feeincome'];

	$LocalCurrencyCode  = $_POST['LocalCurrencyCode'];

	$gross_fee_income = $_POST['gross_fee_income'];

	$additionalexpenses = $_POST['additionalexpenses'];

	$memberincome = $_POST['memberincome'];

	$otherincome  = $_POST['otherincome'];

	$firm_id   = $_POST['firm_id'];

	$firm_name = $_POST['firm_name'];

	$billingtype = $_POST['billingtype'];

	$billing_added_by = $current_user_id;
	

	$promotional_income = ($otherincome+$memberincome+$additionalexpenses+$gross_fee_income);

	

	$currdate = CrmeryHelperDate::formatDBDate(date('Y-m-d H:i:s'));



	$ccode = $db->getQuery(true);

	$ccode->select('value_in_GBP');

	$ccode->from('#__crmery_currencies');

	$ccode->where("currency_code='$LocalCurrencyCode'");

	$db->setQuery($ccode);

	//echo '<br>'.$ccode;

	$ccodevalue = $db->loadObjectList();

	

	$ccode_value = $ccodevalue[0];

	$gbp = $ccode_value->value_in_GBP;
	
	//	old firm billing
	
	$edit_id = (int)$_GET['edit'];
	
	$firm_billing = $db->getQuery(true);

	$firm_billing->select('gross_fee_income');

	$firm_billing->from('#__crmery_companies_billing');

	$firm_billing->where("id='$edit_id'");

	$db->setQuery($firm_billing);

	$old_firm_billings = $db->loadResult();
	
	
	//$bill->LocalAmount;

	//$feeincome_gbp = ($gbp*$feeincome); 

	$feeincome_gbp = ($gbp*$gross_fee_income); 

	$promotional_income_gbp = ($gbp*$promotional_income); 


	if(isset($_GET['edit'])){

		$insert = "UPDATE #__crmery_companies_billing set ";

		$edit = (int)$_GET['edit'];		 

		$where  = " WHERE `id`='$edit'";		 

	}else{

		$insert = "INSERT INTO #__crmery_companies_billing set ";

		$where  = " ";		 

	}

	$set .="	`BillingYear`='$BillingYear',

				`feeincome`='$feeincome',

				`gross_fee_income`='$gross_fee_income',

				`LocalCurrencyCode`='$LocalCurrencyCode',

				`additionalexpenses`='$additionalexpenses',

				`memberincome`='$memberincome',

				`otherincome`='$otherincome',	

				`firm_id`='$firm_id',	

				`firm_name`='$firm_name',	

				`billingtype`='$billingtype',	

				`billing_added_by`='$billing_added_by',	

				`feeincome_gbp`='$feeincome_gbp',

				`fee_income_promotional`='$promotional_income',

				`fee_income_promotional_gbp`='$promotional_income_gbp'";

	

	$newqry = $insert.$set.$where;  

	 

	$db->setQuery($newqry);  

		

		if ($db->query()){
		

			if(isset($_GET['edit'])){
			
				
				/*$insert_history = "INSERT INTO #__crmery_history set 

							`type`='company',

							`type_id`='$id',

							`user_id`='$user_id',

							`date`='$currdate',

							`action_type`='updated',

							`old_value`='$old_firm_billings',

							`new_value`='$gross_fee_income',

							`field`='Billing'";*/

				$msg = 'Billing details updated successfully.';

			}	

			else

			{
			
				/*$insert_history = "INSERT INTO #__crmery_history set 

							`type`='company',

							`type_id`='$id',

							`user_id`='$user_id',

							`date`='$currdate',

							`action_type`='created',

							`old_value`='New Billing details',

							`new_value`='Created',

							`field`='Billing'";*/

				$lastid = $db->insertid();

				$msg = 'Billing details added successfully.';

			}	
			
			//	TO UPDATE THE DATE WHILE ADDING OR UPDATING THE FIRM BILLING.
			$edit = (int)$_GET['id'];	
				
			$date = CrmeryHelperDate::formatDBDate(date('Y-m-d H:i:s'));	
		
			$update_date = "UPDATE #__crmery_companies set modified = '$date' WHERE `id`='$edit' ";
			
			$db->setQuery($update_date);  

			$db->query();		

			$allDone =& JFactory::getApplication();

			$allDone->redirect(JRoute::_('index.php?option=com_crmery&view=firmbilling&layout=companies&id='.$id),$msg);

		}

		

}

 

if($id >0 ){

 

	$query = $db->getQuery(true);

	$query->select("*")

	->from("#__crmery_companies ");

	$query->where("id='".$id."'");						

	
	$db->setQuery($query);

	$refer_rs = $db->loadObjectList();



	$refername   = $refer_rs[0]->name;

	
	$usercanaddbilling = false;

	$inp_company_id = 0;

	

	$querys = $db->getQuery(true);

	$querys->select("b.*")

	->from("#__crmery_companies_billing AS b");

	$querys->where("b.firm_id='".$id."'");						



	$db->setQuery($querys);

	$billing_rs = $db->loadObjectList();
	

}



if(!isset($_GET['edit']) && !isset($_POST['savebilling']))

{

	$dbus =& JFactory::getDbo();

	$queryus = $dbus->getQuery(true);

	$queryus->select("u.country_id")

	->from("#__crmery_users AS u");

	$queryus->where("u.id='".$user_id."'");						



	$dbus->setQuery($queryus);

	$user_rs = $dbus->loadObjectList();

 

    $country_ids = $user_rs[0]->country_id;



	if(strpos(',',$country_ids) !== false){

		$cnt_ids =  @explode(',',$country_ids);

		$user_country_id = $cnt_ids[0];

	}else{

		$user_country_id = $country_ids;

	}

	

	if($user_country_id > 0)

	{

		$query = $db->getQuery(true);

		$query->select("u.currency_code")

		->from("#__crmery_currencies AS u");

		$query->where("u.country_id='".$user_country_id."'");						



		$db->setQuery($query);

		$refer_rs = $db->loadObjectList();



		$LocalCurrencyCode = $refer_rs[0]->currency_code;

		 

	}

}	

   

	$codequery = $db->getQuery(true);

	$codequery->select("cc.currency_code")

	->from("#__crmery_currencies AS cc");

							



	$db->setQuery($codequery);

	$codequery_rs = $db->loadObjectList();

	

   $codeArr = array();

   foreach($codequery_rs as $ccode)

   {

		$codeArr[$ccode->currency_code] = $ccode->currency_code;

   }

   

?>



<h1> Add Billing for firm  "<?php echo $refername?>"</h1>

  

<form method="post" id="list_form" action="<?php //echo JRoute::_('index.php?option=com_crmery&view=billing&task=savebilling&id='.$id); ?>" enctype="multipart/form-data">

<div id="editForm">

<?php if($editbilling === false) { ?>

<table class="com_crmery_table" id="people" >

  <thead>

    <tr>

		<th><?php echo CRMText::_('COM_CRMERY_FIRM_BILLING_TYPE'); ?></th>

		<th><?php echo CRMText::_('COM_CRMERY_FIRM_BILLINGYEAR'); ?></th>

		<th> <?php echo CRMText::_('COM_CRMERY_PERSON_LOCALCURRENCYCODE'); ?></th>

	<!--	<th><?php echo CRMText::_('COM_CRMERY_GROSS_LOCAL_INCOME'); ?></th> -->

		<th><?php echo CRMText::_('COM_CRMERY_GROSS_LOCAL_INCOME'); ?> in GBP</th>

		<th><?php echo CRMText::_('COM_CRMERY_GROSS_FEE_INCOME'); ?></th>

		<th><?php echo CRMText::_('COM_CRMERY_ADDITIONAL_EXPENSES'); ?></th>

		<th><?php echo CRMText::_('COM_CRMERY_ADDITIONAL_MEMBER_INCOME')?></th>

		<th><?php echo CRMText::_('COM_CRMERY_OTHER_INCOME'); ?></th>

		<th><?php echo CRMText::_('COM_CRMERY_FEE_INCOME_PORMOTIONAL'); ?></th>

		<th><?php echo CRMText::_('COM_CRMERY_FEE_INCOME_PORMOTIONAL'); ?> in GBP</th>
		
		<th>Action</th>

	 

	<!--<th>Updated by </th>

		<th>Updated on</th> -->

	</tr>

 </thead>

 <?php 

 if(count($billing_rs)>0){



	foreach($billing_rs as $billing){

		 

		echo '<tr>';

		echo '<td>'.$billing->billingtype.'</td>';

		echo '<td>'.$billing->BillingYear.'</td>';

		echo '<td>'.$billing->LocalCurrencyCode.'</td>';

		//echo '<td>'.number_format($billing->feeincome,2,'.',',').'</td>';

		echo '<td>'.number_format($billing->feeincome_gbp,2,'.',',').'</td>';

		echo '<td>'.number_format($billing->gross_fee_income,2,'.',',').'</td>';		

		echo '<td>'.number_format($billing->additionalexpenses,2,'.',',').'</td>';

		echo '<td>'.number_format($billing->memberincome,2,'.',',').'</td>';

		echo '<td>'.number_format($billing->otherincome,2,'.',',').'</td>';

		echo '<td>'.number_format($billing->fee_income_promotional,2,'.',',').'</td>';

		echo '<td>'.number_format($billing->fee_income_promotional_gbp,2,'.',',').'</td>';
		
		echo '<td><a href="'.JRoute::_('index.php?option=com_crmery&view=firmbilling&id='.$id.'&edit='.$billing->id).'" >Update</a></td>';



	

		//echo '<td>'.$LastUpdatedBy.'</td>';

		//echo '<td>'.substr($billing->UpdatedOn,0,11).'</td>';

		

		if(($member_role == 'basic' || $member_role == 'manager') && $usercanaddbilling == true)

			//echo '<td><a href="'.JRoute::_('index.php?option=com_crmery&view=billing&id='.$id.'&edit='.$billing->id).'" >Update</a></td>';

		

		echo '</tr>';

	 }

 

 }

 else

 {

	echo '<tr><td colspan="12" align="center">No Billings added yet!</td> </tr>';

 }

 ?>

</table>

<br />

<br />

<br />

<br />



<?php } ?>



<?php //if( ( $member_role == 'basic' || $member_role == 'manager' )){ ?> 

<div class="crmeryRow" id="other_button">

<?php 



     if($editbilling) {

		echo '<h4>Edit Billing Details </h4>';

	 }else{

		echo '<h4>Add Billing Details</h4>';

	 }

?>

</div>





		<div class="crmeryRow" id="other_button">

				<div class="crmeryField"> <?php echo CRMText::_('COM_CRMERY_FIRM_BILLING_TYPE'); ?></div>

				<div class="crmeryValue">

				<?php
				if($billingtype == "Projected Billing")
				{
					$check = "checked";
				}
				else if($billingtype == "Current Billing")
				{
					$check = "checked";
				}
				else
				{
					$check = "";
				}
				
				?>

				  Actual Billing for current year <input type="radio" name="billingtype" value="Current Billing" <?php if($billingtype == "Current Billing") echo "checked"; ?>  /> <br />

				  Projected Billing for next year <input type="radio" name="billingtype" value="Projected Billing" <?php if($billingtype == "Projected Billing") echo "checked"; ?>  /> <br />



				 </div>

		</div>

		

		<div class="crmeryRow" id="other_button">

				<div class="crmeryField"> <?php echo CRMText::_('COM_CRMERY_FIRM_BILLINGYEAR'); ?></div>

			<!--	<div class="crmeryValue">

				<input class="inputbox" required type="text" name="BillingYear" value="<?php// echo $BillingYear?>" /> 		</div> -->

				

	

		
			
				<div class="crmeryValue">

					<input class="inputbox date_input" type="text" id="expected_close" name="BillingYear" value="<?php echo $BillingYear; ?>">

					<input type="hidden" id="expected_close_hidden" name="expected_close" value="" />

				</div>

				

		</div>

		

		

			<div class="crmeryRow" id="other_button">

				<div class="crmeryField"> <?php echo CRMText::_('COM_CRMERY_PERSON_LOCALCURRENCYCODE'); ?></div>

				<div class="crmeryValue">

				 <select name="LocalCurrencyCode" >

				 <?php foreach($codeArr as $code) {

					if($code == $LocalCurrencyCode)

						echo '<option selected="selected" value="'.$code.'">'.$code.' </option>';

					else

						echo '<option value="'.$code.'">'.$code.' </option>';

					

				  } ?>

				  </select>

				</div>

		</div>

	

	<div style="display:none;" class="crmeryRow" id="other_button">

				<div class="crmeryField"> <?php echo CRMText::_('COM_CRMERY_GROSS_LOCAL_INCOME'); ?></div>

				<div class="crmeryValue">

				<input class="inputbox"  type="text" name="feeincome" value="<?php echo $feeincome?>" /></div>

		</div>

		

		

		<div class="crmeryRow" id="other_button">

				<div class="crmeryField"> <?php echo CRMText::_('COM_CRMERY_GROSS_FEE_INCOME'); ?></div>

				<div class="crmeryValue">

				<input class="inputbox" required type="text" name="gross_fee_income" value="<?php echo $feeincome?>" /></div>

		</div>

		

	

		

		<div class="crmeryRow" id="other_button">

				<div class="crmeryField"> <?php echo CRMText::_('COM_CRMERY_ADDITIONAL_EXPENSES'); ?></div>

				<div class="crmeryValue"><input class="inputbox" type="text" name="additionalexpenses" value="<?php echo $additionalexpenses?>" /> </div>

		</div>

		

		<div class="crmeryRow" id="other_button">

				<div class="crmeryField"> <?php echo CRMText::_('COM_CRMERY_ADDITIONAL_MEMBER_INCOME')?></div>

				<div class="crmeryValue"><input class="inputbox" type="text" name="memberincome" value="<?php echo $memberincome?>" /> </div>

		</div>

		

		

		<div class="crmeryRow" id="other_button">

				<div class="crmeryField"> <?php echo CRMText::_('COM_CRMERY_OTHER_INCOME'); ?></div>

				<div class="crmeryValue">

					<input class="inputbox" type="text" name="otherincome" value="<?php echo $otherincome?>" />

				</div>

		</div> 

		

	<input class="firm_name" type="hidden" name="firm_name" value="<?php echo $refername?>" />

		<input class="firm_id" type="hidden" name="firm_id" value="<?php echo $id?>" />

		

		<span class="actions"><input type="submit" name="savebilling" value="Save" class="button"> <a onclick="window.history.back()" href="javascript:void(0);">Cancel</a>

				

	</span>

	

<?php // } // end if role ?>	

	

	

	</div> 



</form>



 



