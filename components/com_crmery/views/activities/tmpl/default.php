<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<div id="latest" class="container">
    <table class="com_crmery_table" id="latest_activity">
        <tr>
            <th><?php echo CRMText::_('COM_CRMERY_ITEM'); ?></th>
            <th><?php echo CRMText::_('COM_CRMERY_ACTION'); ?></th>
            <th><?php echo CRMText::_('COM_CRMERY_BY'); ?></th>
            <th><?php echo CRMText::_('COM_CRMERY_OCCURRED'); ?></th>
        </tr>
        <?php echo $this->latest_activities->display(); ?>
    </table>
</div>