<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

//define deal
$contacts = $this->contacts;
$view = JRequest::getVar('view');
$loc = JRequest::getVar('loc');
?>
<div id="contacts_container">
<?php if ( array_key_exists('print',$this) && $this->print ) { ?>
<div class="scrollable_column" >
<div id="person-details-overflow">
<div class="" id="">
<div id="people_container" class="people_container">
<?php } else { ?>
<div class="scrollable_column">
<div id="person-details-overflow">
<div class="scrollable" id="person_details">
<div id="people_container" class="items people_container">
<?php } ?>
	<?php if ( is_array($contacts) && count($contacts) > 0 ){ foreach ( $contacts as $person ){ ?>
		<div class="person_container item_container" id="person_container_<?php echo $person['id']; ?>">
			<?php if ( $view == "deals" || $loc == "deal" ){ ?>
				<div class="person_actions" style="display:none;">
					<?php if ( $this->primary_contact_id == $person['id'] ){ ?>
						<a class="star" id="primary_contact" onclick="unassignDealPrimaryContact(<?php echo $person['id']; ?>)" data-id="<?php echo $person['id']; ?>" href="javascript:void(0);" ></a>
					<?php } else { ?>
						<a class="white_star" id="star_<?php echo $person['id']; ?>" data-id="<?php echo $person['id']; ?>" onclick="assignDealPrimaryContact(<?php echo $person['id']; ?>);" href="javascript:void(0);" ></a>	
					<?php } ?>
					<a class="remove" href="javascript:void(0);" onclick="removePersonFromDeal(<?php echo $person['id']; ?>);"></a>
				</div>
			<?php } ?>
			<div class="infoBlock">
				<div class="infoLabel">
					<?php if ( array_key_exists('avatar',$person) && $person['avatar'] != "" && $person['avatar'] != null ){
                        	 echo '<td class="avatar" ><img id="avatar_img_'.$person['id'].'" data-item-type="people" data-item-id="'.$person['id'].'" class="avatar" src="'.JURI::base().'components/com_crmery/media/avatars/'.$person['avatar'].'"/></td>';
                    	}else{
                        	echo '<td class="avatar" ><img id="avatar_img_'.$person['id'].'" data-item-type="people" data-item-id="'.$person['id'].'" class="avatar" src="'.JURI::base().'components/com_crmery/media/images/person.png'.'"/></td>';
                    	} ?>
				</div>
				<div class="infoDetails">
					<span class="largeDetails"><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=people&layout=person&id='.$person['id']);?>"><?php echo $person['first_name'] . ' ' . $person['last_name']; ?></a></span><br />
					<?php echo $person['company_name'];?>
					<?php /*<span class="smallDetails"><?php echo $person['owner_first_name'] . ' ' . $person['owner_last_name']; ?></span><br />
					if(array_key_exists('company_id',$person)){ ?>
						<a href="<?php echo JRoute::_("index.php?option=com_crmery&view=companies&layout=company&company_id=".$person['company_id']); ?>"><?php echo $person['company_name']; ?></a>
					<?php } */ ?>
				</div>
			</div>
			<?php if ( array_key_exists("position",$person) && $person['position'] != "" ){ ?>
				<div class="infoBlock">
					<div class="infoLabel right"><?php echo CRMText::_('COM_CRMERY_TITLE'); ?></div>
					<div class="infoDetails"><?php echo $person['position']; ?></div>
				</div>
			<?php } ?>
			<div class="infoBlock">
				<div class="infoLabel right"><?php echo CRMText::_('COM_CRMERY_WORK_PHONE_SHORT'); ?></div>
				<div class="infoDetails"><?php echo $person['phone']; ?></div>
			</div>
			
			<div class="infoBlock">
				<div class="infoLabel right"><?php echo CRMText::_('COM_CRMERY_EMAIL_SHORT'); ?></div>
				<div class="infoDetails">
					<?php if(array_key_exists('email',$person)){ ?>
						<a target='_blank' href="mailto:<?php echo $person['email']; ?>"><?php echo $person['email']; ?></a>
					<?php } ?>
				</div>
			</div>

			<div class="infoBlock">
				<div class="infoLabel">&nbsp;</div>
				<div class="socialIcons infoDetails">

					<?php if(array_key_exists('twitter_user',$person) && $person['twitter_user'] != ""){ ?>
						<a href="http://www.twitter.com/#!/<?php echo $person['twitter_user']; ?>" target="_blank"><div class="twitter_light"></div></a>
					<?php } else { ?>
					<span class="editable parent" id="editable_twitter_container_<?php echo $person['id']; ?>">
					<div class="inline">
						<a href="javascript:void(0);"><div class="twitter_dark"></div></a>
					</div>
					<div class="filters editable_info">
						<form id="twitter_form_<?php echo $person['id']; ?>">
							<input type="hidden" name="item_id" value="<?php echo $person['id']; ?>" />
							<input type="hidden" name="item_type" value="people" />
							<input type="text" class="inputbox" name="twitter_user" value="<?php if ( array_key_exists('twitter_user',$person) ) echo $person['twitter_user']; ?>" />
							<input type="button" class="button" onclick="saveEditableModal('twitter_form_<?php echo $person['id']; ?>');" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" />
						</form>
					</div>
					</span>
					<?php } ?>

					<?php if(array_key_exists('facebook_url',$person) && $person['facebook_url'] != ""){ ?>
						<a href="<?php echo $person['facebook_url']; ?>" target="_blank"><div class="facebook_light"></div></a>
					<?php } else { ?>
					<span class="editable parent" id="editable_facebook_container_<?php echo $person['id']; ?>">
					<div class="inline">
						<a href="javascript:void(0);"><div class="facebook_dark"></div></a>
					</div>
					<div class="filters editable_info">
						<form id="facebook_form_<?php echo $person['id']; ?>">
							<input type="hidden" name="item_id" value="<?php echo $person['id']; ?>" />
							<input type="hidden" name="item_type" value="people" />
							<input type="text" class="inputbox" name="facebook_url" value="<?php if ( array_key_exists('facebook_url',$person) ) echo $person['facebook_url']; ?>" />
							<input type="button" class="button" onclick="saveEditableModal('facebook_form_<?php echo $person['id']; ?>');" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" />
						</form>
					</div>
					</span>
					<?php } ?>

					<?php if(array_key_exists('linkedin_url',$person) && $person['linkedin_url'] != "" ){ ?>
						<a href="<?php echo $person['linkedin_url']; ?>" target="_blank"><div class="linkedin_light"></div></a>
					<?php } else { ?>
					<span class="editable parent" id="editable_linkedin_container_<?php echo $person['id']; ?>">
					<div class="inline">
						<a href="javascript:void(0);"><div class="linkedin_dark"></div></a>
					</div>
					<div class="filters editable_info">
						<form id="linkedin_form_<?php echo $person['id']; ?>">
							<input type="hidden" name="item_id" value="<?php echo $person['id']; ?>" />
							<input type="hidden" name="item_type" value="people" />
							<input type="text" class="inputbox" name="linkedin_url" value="<?php if ( array_key_exists('linkedin_url',$person) ) echo $person['linkedin_url']; ?>" />
							<input type="button" class="button" onclick="saveEditableModal('linkedin_form_<?php echo $person['id']; ?>');" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" />
						</form>
					</div>
					</span>
					<?php } ?>

					<span class="editable parent" id="editable_aim_container_<?php echo $person['id']; ?>">
					<div class="inline">
						<?php if(array_key_exists('aim',$person) && $person['aim'] != "" ){ ?>
							<a href="javascript:void(0);"><div class="aim_light"></div></a>
						<?php } else { ?>
							<a href="javascript:void(0);"><div id="aim_button_<?php echo $person['id']; ?>" class="aim_dark"></div></a>
						<?php } ?>
					</div>
					<div class="filters editable_info">
						<form id="aim_form_<?php echo $person['id']; ?>">
							<input type="hidden" name="item_id" value="<?php echo $person['id']; ?>" />
							<input type="hidden" name="item_type" value="people" />
							<input type="text" class="inputbox" name="aim" value="<?php if ( array_key_exists('aim',$person) )  echo $person['aim']; ?>" />
							<input type="button" class="button" onclick="saveEditableModal('aim_form_<?php echo $person['id']; ?>');" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" />
						</form>
					</div>
					</span>

				</div>
			</div>
		</div>
	<?php } }else{ ?>
		<div class="item_container person_container">
			<div class="infoBlock">
				<div class="infoLabel"><img src="<?php echo JURI::base().'components/com_crmery/media/images/person.png'; ?>"/></div>
				<div class="infoDetails">
				</div>
			</div>
			<div class="infoBlock">
				<div class="infoLabel"><?php echo CRMText::_('COM_CRMERY_WORK_PHONE'); ?></div>
			</div>
			
			<div class="infoBlock">
				<div class="infoLabel"><?php echo CRMText::_('COM_CRMERY_EMAIL'); ?></div>
				<div class="infoDetails">
				</div>
			</div>

			<div class="infoBlock">
				<div class="infoLabel"><?php echo CRMText::_('COM_CRMERY_SOCIAL'); ?></div>
				<div class="socialIcons infoDetails">
					<a href="javascript:void(0);"><div class="twitter_dark"></div></a>
					<a href="javascript:void(0);"><div class="facebook_dark"></div></a>
					<a href="javascript:void(0);"><div class="linkedin_dark"></div></a>
					<a href="javascript:void(0);"><div class="aim_dark"></div></a>
				</div>
			</div>
		</div>
	<?php } ?>
	</div>
</div>
</div>
<?php if ( !array_key_exists('print',$this) ){ ?>
	<div class="contact_info_controls_area_modal">
		<?php $raw = JRequest::getVar('format'); ?>
		<?php if ( $view == "deals" || $loc == "deal" ){ ?>
			<!-- <a class="button" href="javascript:void(0);" onclick="addPerson()" ><?php echo CRMText::_('COM_CRMERY_ADD_PERSON_TO_THIS_DEAL'); ?></a> -->
			 <a class="button" href="<?php echo JRoute::_('index.php?option=com_crmery&view=people&layout=edit&deal_get_id='.$_GET['id']); ?>"  ><?php echo CRMText::_('COM_CRMERY_ADD_PERSON_TO_THIS_DEAL'); ?></a>  
		<?php } ?>
		<?php if(count($contacts)>0 ){ ?>
			<!-- <a class="prev"><<</a> <a class="next">>></a> -->
		<?php } ?>
	
	 
		
	</div>
	<?php } ?>
</div>
</div>