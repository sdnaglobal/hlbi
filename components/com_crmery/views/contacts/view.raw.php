<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewContacts extends JView
{
	function display($tpl = null)
	{
		$deal_id = JRequest::getVar('deal_id');
		$event_id = JRequest::getVar('event_id');

		$model =& JModel::getInstance('contacts','CrmeryModel');
		$model->set('deal_id',$deal_id);
		$model->set('event_id',$event_id);

		$contacts = $model->getContacts();
		$this->assignRef('contacts',$contacts);

		if ( $deal_id ){
			$primary_contact_id = CrmeryHelperDeal::getPrimaryContact($deal_id);
			$this->assignRef('primary_contact_id',$primary_contact_id);
		}
        
        //display view
		parent::display($tpl);		
	}
	
}
		
		