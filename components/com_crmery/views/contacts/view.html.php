<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewContacts extends CrmeryHelperView
{
	function display($tpl = null)
	{
		$deal_id = JRequest::getVar('deal_id');
		if ( $deal_id ){
			$primary_contact_id = CrmeryHelperDeal::getPrimaryContact($deal_id);
			$this->assignRef('primary_contact_id',$primary_contact_id);
		}
				
        //display
		parent::display($tpl);
	}
	
}
?>
