<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$peopleeditid = JRequest::getVar('id');

if($peopleeditid>0)
	$style = "style='display:block;'";
else	
	$style = "style='display:none;'";


$person = $this->person;
$member_id = CrmeryHelperUsers::getUserId();

//echo '<pre>';
//print_r($person);
//echo '</pre>';

/*
$db = JFactory::getDbo();
				
$insert_history = $db->getQuery(true);
//$deal_id_history = $_POST['deal_id'] = 26;

$deal_id_history =  26;
  $date = CrmeryHelperDate::formatDBDate(date('Y-m-d 00:00:00'));
$insert_history = "INSERT INTO #__crmery_history set 
			`type`='deal',
			`type_id`='$deal_id_history',
			`user_id`='8',
			`date`='$date',
			`new_value`='Referred',
			`action_type`='updated',
			`field`='referred'";
$db->setQuery($insert_history);  
$db->query();
*/

$deal_id_for_client = 0;
if($person['id'] > 0){
		$db = JFactory::getDbo();
		$deal_name = ''; 
		$query_deal = $db->getQuery(true);
		$query_deal = "SELECT d.id FROM #__crmery_deals as d, #__crmery_people_cf as cf where d.id=cf.association_id and cf.person_id=".$person['id']." and cf.association_type='deal'";

		$db->setQuery($query_deal);
		$deal_id_for_client = $db->loadResult();

}		

if ( array_key_exists('company_id',$person) ) { $company_id = $person['company_id']; } else if (JRequest::getVar('company_id')) { $company_id = JRequest::getVar('company_id');} else {$company_id = "";}
?>
<?php $format = JRequest::getVar('format'); ?>
<?php if ( $format != "raw" ) { ?>
	<h1><?php echo $person['header']; ?></h1>
<?php } ?>
<form id="edit_form" method="POST" action="<?php echo 'index.php?option=com_crmery&controller=people&task=save'; ?>" onsubmit="return save(this)" >
		<div id="editForm" onclick="">

		<?php if ( !array_key_exists('id',$person) || ( array_key_exists('id',$person) && $person['id'] <= 0 ) ) { ?>
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_PERSON_DEAL')); ?><span class="required">*</span> </div>
			<div class="crmeryValue">
				<?php 
					
					if(isset($_GET['deal_get_id']))
						$deal_get_id = $_GET['deal_get_id'];
						
					echo CrmeryHelperDropdown::generateDropdown('deal',$deal_get_id);
				?>
				<div id="showleadinfo" style="display:none;margin:10px 0px;padding:5px 10px;"> </div>
			</div>
		</div>
		<?php } ?>
		
		 <div class="crmeryRow">
				<div class="crmeryField"> <?php echo ucwords(CRMText::_('COM_CRMERY_COMPANIES')); ?> <span class="required">*</span></div>
				<div class="crmeryValue">
				
				<?php //echo CrmeryHelperCompany::getCountryDropdown($person['country_id']); ?>
				
				<?php if ( !array_key_exists('id',$person) || ( array_key_exists('id',$person) && $person['id'] <= 0 ) ) {   
				
					$db =& JFactory::getDbo();
					$query = $db->getQuery(true);

					$query->select("country_id,country_name")
						->from("#__crmery_currencies");
						$query->order("country_name");

					$db->setQuery($query);
					$countriesRs = $db->loadObjectList();
					
					if($countriesRs)
					{
						//echo '<select name="countryArr" id="countryArr" onchange="getState(this.value)" >
						echo '<select name="countryArr" id="countryArr" onchange="getfirmdropdownbycountry(this.value)" >
								<option value="">Select Country</option>';
						foreach($countriesRs as $country)
						{
							echo '<option value="'.$country->country_id.'">'.$country->country_name.'</option>';
						}
						echo '</select>';
					}
					
		}	
		       
	 ?>
			   
		<div id="state" style="display:none;margin:5px 0px;"><select id="state_id" name="state_id"> </select> </div>
				
				<!-- this is used for firm drop down when selecting country -->
					<div style="margin: 10px 0;" id="firmdropdownbycntry">
					
					</div> 
				<!-- end firm drop down -->	
					
					
									<div style="margin:5px 0px;"> <input class="required inputbox" type="text" id="companyname" name="companyname" placeholder="<?php echo CRMText::_('Start typing'); ?>" onkeyup="showResults(this.value)"  value="<?php echo $person['companyname']; ?>" autocomplete="off"/><div id="livesearch" style="max-height: 185px;overflow: auto;float: right;width:auto; position: absolute;background:#fff;display:none;"></div>	

					</div>
				
					<?php //echo CrmeryHelperDropdown::getPartnerDropdown($person['partner_id']);
                             // echo CrmeryHelperDropdown::generateDropdown('company',$person['company_id']);

					?>
				</div>
			</div>	
			
		 <div class="crmeryRow" style="display:none;">
				<div class="crmeryField"> <?php echo ucwords(CRMText::_('COM_CRMERY_COMPANIES')); ?><span class="required">*</span></div>
				<div class="crmeryValue">  <!-- <input type="hidden" id="company_id" name="company_id" value="<?php //echo $person['company_id'] ?>" /> -->					
					<?php //echo CrmeryHelperDropdown::getPartnerDropdown($person['partner_id']);
                            //  echo CrmeryHelperDropdown::generateDropdown('company',$person['company_id']);

					?>
				</div>
			</div>	
	
	
		   <div class="crmeryRow" <?php echo $style ?> id="firmadminid">
				<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_PARTNER_LIST')); ?><span class="required">*</span></div>
				<div class="crmeryValue">					
					<div id="nofirmadmin" >
					 <?php echo CrmeryHelperDropdown::generateDropdown('firmadmin',$person['company_id'],$person['assignee_id'],'name="assignee_id"'); ?>
					</div>
					<div id="firmadmin" style="display:none;"></div>
				</div>
			</div>	
		
		
	<div <?php echo $style ?> id="hideshowfirstdata" >
	
		<div class="crmeryRow" >
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_FIRST'); ?><span class="required">*</span></div>
			<div class="crmeryValue wide"><input class="required inputbox" type="text" readonly="true" name="first_name" placeholder="<?php echo CRMText::_('COM_CRMERY_PERSON_FIRST_NULL'); ?>" value="<?php echo $person['first_name']; ?>" /></div>
		</div>
		
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_LAST'); ?><span class="required">*</span></div>
			<div class="crmeryValue wide"><input class="required inputbox" type="text" readonly="true" name="last_name" placeholder="<?php echo CRMText::_('COM_CRMERY_PERSON_LAST_NULL'); ?>" value="<?php echo $person['last_name']; ?>"/></div>
		</div>
		
		<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_EMAIL'); ?></div>
				<div class="crmeryValue"><input class="inputbox" type="text" readonly="true" name="email" placeholder="<?php echo CRMText::_('COM_CRMERY_PERSON_EMAIL_NULL'); ?>" value="<?php echo $person['email']; ?>"/></div>
		</div>
		
		
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_PERSON_COMPANY')); ?></div>
			<div class="crmeryValue">
				<input type="text" onblur="checkCompanyName(this);" class="inputbox" readonly="true" name="company" id="company_name" value="<?php if ( array_key_exists('company_name',$person) ) echo $person['company_name']; ?>" />
				<input type="hidden" name="company_id" id="company_id" value="<?php echo $company_id; ?>" />
				<div id="company_message"></div>
			</div>
		</div>		

</div>		
		
		<div class="crmeryRow" style="display:none;">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_POSITION'); ?></div>
			<div class="crmeryValue"><input class="inputbox" type="text" name="position" placeholder="<?php echo CRMText::_('COM_CRMERY_PERSON_POSITION_NULL'); ?>" value="<?php echo $person['position']; ?>"/></div>
		</div>
		
		<div class="crmeryRow" style="display:none;">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_PHONE'); ?></div>
			<div class="crmeryValue"><input class="inputbox" type="text" name="phone" placeholder="<?php echo CRMText::_('COM_CRMERY_PERSON_PHONE_NULL'); ?>" value="<?php echo $person['phone']; ?>"/></div>
		</div>	
		
		<div class="crmeryRow" style="display:none;">
			<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_PERSON_LEAD')); ?></div>
			<div class="crmeryValue"><input class="inputbox" value="lead" type="checkbox" name="type" checked="checked" <?php $checked = ( $person['type'] == "lead") ? "checked" : ""; echo $checked; ?> /><?php echo CRMText::_('COM_CRMERY_THIS_PERSON_IS_A_LEAD'); ?></div>
		</div>
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_SOURCE'); ?></div>
			<div class="crmeryValue">
				<?php echo CrmeryHelperDropdown::generateDropdown('source',$person['source_id']); ?>
			</div>
		</div>
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_STATUS'); ?></div>
			<div class="crmeryValue">
				<?php echo CrmeryHelperDropdown::generateDropdown('people_status',$person['status_id']); ?>
			</div>
		</div>
		
		<?php  if ( $format != "raw" ){ ?>
		<!--<div class="crmeryRow" id="work_button">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_WORK'); ?></div>
			<div class="crmeryValue work">
				<a href="javascript:void(0)" onclick="bind_area('work')"><span class="work_message"><?php echo CRMText::_('COM_CRMERY_PERSON_WORK_ADDRESS'); ?></span></a>
			</div>
		</div> 
		<div style="display:none;" id="work_info" >
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_WORK'); ?></div>
				<div class="crmeryValue">
					<input class="inputbox address_one" placeholder="<?php echo CRMText::_("COM_CRMERY_ADDRESS_1_NULL"); ?>" type="text" name="work_address_1" value="<?php if ( array_key_exists('work_address_1',$person)) echo $person['work_address_1']; ?>" />
					<br />
					<input class="inputbox address_two" placeholder="<?php echo CRMText::_("COM_CRMERY_ADDRESS_2_NULL"); ?>" type="text" name="work_address_2" value="<?php if ( array_key_exists('work_address',$person)) echo $person['work_address_2']; ?>" />
					<br />
					<input class="inputbox address_city" placeholder="<?php echo CRMText::_("COM_CRMERY_CITY_NULL"); ?>" type="text" name="work_city" value="<?php if ( array_key_exists('work_city',$person)) echo $person['work_city']; ?>" />
					<input class="inputbox address_state" placeholder="<?php echo CRMText::_("COM_CRMERY_STATE_NULL"); ?>" type="text" name="work_state" value="<?php if ( array_key_exists('work_state',$person)) echo $person['work_state']; ?>" />
					<input class="inputbox address_zip" placeholder="<?php echo CRMText::_("COM_CRMERY_ZIP_NULL"); ?>" type="text" name="work_zip" value="<?php if ( array_key_exists('work_zip',$person)) echo $person['work_zip']; ?>" />
					<br />
					<input class="inputbox address_country" placeholder="<?php echo CRMText::_("COM_CRMERY_COUNTRY_NULL"); ?>" type="text" name="work_country" value="<?php if ( array_key_exists('work_country',$person)) echo $person['work_country']; ?>" />
				</div>
			</div>
		</div> 

		<div class="crmeryRow" id="home_button">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_HOME'); ?></div>
			<div class="crmeryValue home">
				<a href="javascript:void(0)" onclick="bind_area('home')"><span class="home_message"><?php echo CRMText::_('COM_CRMERY_PERSON_HOME_ADDRESS'); ?></span></a>	
			</div>
		</div>
		<div style="display:none;" id="home_info" >
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_HOME'); ?></div>
				<div class="crmeryValue">
					<input class="inputbox address_one" placeholder="<?php echo CRMText::_("COM_CRMERY_ADDRESS_1_NULL"); ?>" type="text" name="home_address_1" value="<?php if ( array_key_exists('home_address_1',$person)) echo $person['home_address_1']; ?>" />
					<br />
					<input class="inputbox address_two" placeholder="<?php echo CRMText::_("COM_CRMERY_ADDRESS_2_NULL"); ?>" type="text" name="home_address_2" value="<?php if ( array_key_exists('home_address_2',$person)) echo $person['home_address_2']; ?>" />
					<br />
					<input class="inputbox address_city" placeholder="<?php echo CRMText::_("COM_CRMERY_CITY_NULL"); ?>" type="text" name="home_city" value="<?php if ( array_key_exists('home_city',$person)) echo $person['home_city']; ?>" />
					<input class="inputbox address_state" placeholder="<?php echo CRMText::_("COM_CRMERY_STATE_NULL"); ?>" type="text" name="home_state" value="<?php if ( array_key_exists('home_state',$person)) echo $person['home_state']; ?>" />
					<input class="inputbox address_zip" placeholder="<?php echo CRMText::_("COM_CRMERY_ZIP_NULL"); ?>" type="text" name="home_zip" value="<?php if ( array_key_exists('home_zip',$person)) echo $person['home_zip']; ?>" />
					<br />
					<input class="inputbox address_country" placeholder="<?php echo CRMText::_("COM_CRMERY_COUNTRY_NULL"); ?>" type="text" name="home_country" value="<?php if ( array_key_exists('home_country',$person)) echo $person['home_country']; ?>" />
				</div>
			</div>
		</div> -->

		<?php $style = ( array_key_exists('assignee_id',$person) && $person['assignee_id'] > 0 ) ? "" : "style='display:none;'"; ?>
		<?php if ( strlen($style) > 0 ){ ?>
		
		<!--<div class="crmeryRow" id="assignment_button">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_ASSIGNMENT'); ?></div>
			<div class="crmeryValue assignment">
				<a href="javascript:void(0)" onclick="bind_area('assignment')"><span class="assignment_message"><?php echo CRMText::_('COM_CRMERY_PERSON_ASSIGNMENT_MESSAGE'); ?></span></a>
			</div>
		</div> -->
		<?php } ?>
		<!--<div <?php echo $style; ?> id="assignment_info" >
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_PERSON_ASSIGNMENT')); ?></div>
				<div class="crmeryValue">
					<input class="inputbox" type="text" id="assignee_name" onkeyup="checkUserName(this);" name="assignee_name" value="<?php if(array_key_exists('assignee_id',$person) && $person['assignee_id'] > 0) echo $person['assignee_name']; ?>"  />
					<input type="hidden" name="assignee_id" id="assignee_id" value="<?php if(array_key_exists('assignee_id',$person) && $person['assignee_id'] != 0) echo $person['assignee_id']; ?>" />
					<?php if ( $style == "" ){  ?>
						- <a href="javascript:void(0);" onclick="clearAssignment();"><?php echo CRMText::_('COM_CRMERY_CLEAR_ASSIGNMENT'); ?></a>
					<?php } ?>
				</div>
			</div>
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_ASSIGNMENT_NOTE'); ?></div>
				<div class="crmeryValue">
					<textarea class="inputbox" id="assignment_note"  name="assignment_note"><?php if(array_key_exists('assignment_note',$person)) echo $person['assignment_note']; ?></textarea>
				</div>
			</div>
		</div>  

		<div class="crmeryRow" id="other_button">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_OTHER'); ?></div>
			<div class="crmeryValue other"><a href="javascript:void(0)" onclick="bind_area('other')">
				<span class="other_message"><?php echo CRMText::_('COM_CRMERY_PERSON_OTHER_MESSAGE'); ?></span></a>
			</div>
		</div>

		<div style="display:none;" id="other_info" >
			<div class="crmeryRow" id="other_button">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_MOBILE_PHONE'); ?></div>
				<div class="crmeryValue">
					<input class="inputbox" type="text" name="mobile_phone" value="<?php if(array_key_exists('mobile_phone',$person) && $person['mobile_phone'] != 0 ) echo $person['mobile_phone']; ?>" />
				</div>
			</div>
			<div class="crmeryRow" id="other_button">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_HOME_EMAIL'); ?></div>
				<div class="crmeryValue"><input class="inputbox" type="text" name="home_email" value="<?php if(array_key_exists('home_email',$person)) echo $person['home_email']; ?>" /></div>
			</div>
			<div class="crmeryRow" id="other_button">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_OTHER_EMAIL'); ?></div>
				<div class="crmeryValue"><input class="inputbox" type="text" name="other_email" value="<?php if(array_key_exists('other_email',$person)) echo $person['other_email']; ?>" /></div>
			</div>
			<div class="crmeryRow" id="other_button">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_HOME_PHONE'); ?></div>
				<div class="crmeryValue"><input class="inputbox" type="text" name="home_phone" value="<?php if(array_key_exists('home_phone',$person)) echo $person['home_phone']; ?>" /></div>
			</div>
			<div class="crmeryRow" id="other_button">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_FAX'); ?></div>
				<div class="crmeryValue"><input class="inputbox" type="text" name="fax" value="<?php if(array_key_exists('fax',$person)) echo $person['fax']; ?>" /></div>
			</div>
			<div class="crmeryRow" id="other_button">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_WEBSITE'); ?></div>
				<div class="crmeryValue"><input class="inputbox" type="text" name="website" value="<?php if(array_key_exists('website',$person)) echo $person['website']; ?>" /></div>
			</div>
			<div class="crmeryRow" id="other_button">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_FACEBOOK_URL'); ?></div>
				<div class="crmeryValue"><input class="inputbox" type="text" name="facebook_url" value="<?php if(array_key_exists('facebook_url',$person)) echo $person['facebook_url']; ?>" /></div>
			</div>
			<div class="crmeryRow" id="other_button">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_TWITTER_USER'); ?></div>
				<div class="crmeryValue"><input class="inputbox" data-minlength="4" type="text" name="twitter_user" value="<?php if(array_key_exists('twitter_user',$person)) echo $person['twitter_user']; ?>" /></div>
			</div>
			<div class="crmeryRow" id="other_button">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_LINKEDIN_URL'); ?></div>
				<div class="crmeryValue"><input class="inputbox" type="text" name="linkedin_url" value="<?php if(array_key_exists('linkedin_url',$person)) echo $person['linkedin_url']; ?>" /></div>
			</div>
		</div> -->
		<?php echo $this->edit_custom_fields_view->display(); ?>
		<?php }  ?>
		
<!--  IS CODE START MISSING FIELDS -->		

	<!--	<div class="crmeryRow" id="other_button">
				<div class="crmeryField"> <?php //echo CRMText::_('COM_CRMERY_PERSON_BILLINGYEAR'); ?></div>
				<div class="crmeryValue"><input class="inputbox" type="text" name="BillingYear" value="<?php //if(array_key_exists('BillingYear',$person)) echo $person['BillingYear']; ?>" />(yyyy-mm-dd, <?php echo date('Y-m-d');?>)</div>
		</div> 

		<div class="crmeryRow" id="other_button">
				<div class="crmeryField"> <?php echo CRMText::_('COM_CRMERY_PERSON_TYPEOFWORK'); ?></div>
				<div class="crmeryValue"><input class="inputbox" type="text" name="TypeOfWork" value="<?php if(array_key_exists('TypeOfWork',$person)) echo $person['TypeOfWork']; ?>" /></div>
		</div>  -->
		
	<!--	<div class="crmeryRow" id="other_button">
				<div class="crmeryField"> <?php echo CRMText::_('COM_CRMERY_PERSON_REFERRALSCORE'); ?></div>
				<div class="crmeryValue"><input class="inputbox" type="text" name="ReferralScore" value="<?php if(array_key_exists('ReferralScore',$person)) echo $person['ReferralScore']; ?>" /></div>
		</div>
		
		<div class="crmeryRow" id="other_button">
				<div class="crmeryField"> <?php echo CRMText::_('COM_CRMERY_PERSON_REFERALREPORT'); ?></div>
				<div class="crmeryValue"><textarea name="ReferalReport" rows="5" cols="50"> <?php if(array_key_exists('ReferalReport',$person)) echo $person['ReferalReport']; ?> </textarea></div>
		</div> -->
		
		<div class="crmeryRow" id="other_button">
				<div class="crmeryField"> <?php echo CRMText::_('COM_CRMERY_PERSON_ONGOINGWORK'); ?></div>
				<div class="crmeryValue"><select name="OnGoingWork" ><option <?php if(array_key_exists('OnGoingWork',$person)){  if($person['OnGoingWork'] == 'Yes') echo 'selected="selected"'; }?> value="Yes">Yes</option> <option value="No" <?php if(array_key_exists('OnGoingWork',$person)){  if($person['OnGoingWork'] == 'No') echo 'selected="selected"'; }?> >No</option> </select>
				</div>
		</div>

	   <div class="crmeryRow">
			<div class="crmeryField">  <?php echo CRMText::_('COM_CRMERY_REFERRING_FIRM'); ?> </div>
			<div class="crmeryValue">					
				<?php  //echo CrmeryHelperDropdown::getPartnerDropdown($deal['partner_id']);
						  echo CrmeryHelperDropdown::generateDropdown('usercompanyedit',$person['referring_firm_id'],'name="referring_firm_id"');

				?>
			</div>
		</div>	
		
		
		
	<!--	<div class="crmeryRow" id="other_button">
				<div class="crmeryField"> <?php //echo CRMText::_('COM_CRMERY_PERSON_REFERRED'); ?></div>
				<div class="crmeryValue"><input class="inputbox" type="checkbox" name="Referred" value="1"
				<?php //if(array_key_exists('Referred',$person)){ if($person['Referred'] == 1) echo 'checked="checked"'; } ?> /> </div>
		</div>
		
		<div class="crmeryRow" id="other_button">
				<div class="crmeryField"> <?php //echo CRMText::_('COM_CRMERY_PERSON_RECEIVED'); ?></div>
				<div class="crmeryValue"><input class="inputbox" type="checkbox" name="Received" value="1" <?php //if(array_key_exists('Received',$person)){  if($person['Received'] == 1) echo 'checked="checked"'; } ?> /></div>
		</div> 
		
		<div class="crmeryRow" id="other_button">
				<div class="crmeryField"> <?php //echo CRMText::_('COM_CRMERY_PERSON_LOCALAMOUNT'); ?></div>
				<div class="crmeryValue"><input class="inputbox" type="text" name="LocalAmount" value="<?php //if(array_key_exists('LocalAmount',$person)) echo $person['LocalAmount']; ?>" /></div>
		</div>
		
		<div class="crmeryRow" id="other_button">
				<div class="crmeryField"> <?php //echo CRMText::_('COM_CRMERY_PERSON_LOCALCURRENCYCODE'); ?></div>
				<div class="crmeryValue"><input class="inputbox" type="text" name="LocalCurrencyCode" value="<?php //if(array_key_exists('LocalCurrencyCode',$person)) echo $person['LocalCurrencyCode']; ?>" />(eg USD)</div>
		</div>
		
		<div class="crmeryRow" id="other_button">
				<div class="crmeryField"> <?php //echo CRMText::_('COM_CRMERY_PERSON_HLBFEERATE'); ?></div>
				<div class="crmeryValue"><input class="inputbox" type="text" name="HLBFeeRate" value="<?php //if(array_key_exists('HLBFeeRate',$person)) echo $person['HLBFeeRate']; ?>" />%(do not add % symbol)</div>
		</div>
		
		<div class="crmeryRow" id="other_button">
				<div class="crmeryField"> <?php// echo CRMText::_('COM_CRMERY_PERSON_FEEPAID'); ?></div>
				<div class="crmeryValue">
				<select name="FeePaid" ><option <?php //if(array_key_exists('FeePaid',$person)){  if($person['FeePaid'] == 1) echo 'selected="selected"'; }?> value="1">Yes</option> <option value="0" <?php //if(array_key_exists('FeePaid',$person)){  if($person['FeePaid'] == 0) echo 'selected="selected"'; }?> >No</option> </select>
				
				</div>
		</div> 
		-->
		

<!--  IS CODE END MISSING FIELDS -->		
		
		
		<?php if ( $format != "raw" ) { ?>
			<span class="actions"><input class="button" type="submit" value="<?php echo CRMText::_('COM_CRMERY_SAVE_BUTTON'); ?>"> <a href="javascript:void(0);" onclick="window.history.back()"><?php echo CRMText::_('COM_CRMERY_CANCEL_BUTTON'); ?></a>
		<?php } else { ?>	
			<div class="actions"><a href="javascript:void(0);" onclick="saveListItem(<?php echo JRequest::getVar('id'); ?>);" class="button"><?php echo CRMText::_('COM_CRMERY_SAVE_BUTTON'); ?></a><a href="javascript:void(0);" onclick="window.top.window.jQuery('.ui-dialog-content').dialog('close');"><?php echo CRMText::_('COM_CRMERY_CANCEL_BUTTON'); ?></a></div>
		<?php } ?>
	<?php
		if ( JRequest::getVar('deal_id') ) {
			echo '<input type="hidden" name="deal_id" value="'.$person['deal_id'].'" />';
		}
		if ( array_key_exists('id',$person) && $person['id'] ) {
			echo '<input type="hidden" name="id" value="'.$person['id'].'" />';
		}
	?>
	<input type="hidden" name="edit_screen" value="1" />
	</div>
	
 <?php if($deal_id_for_client>0)
 {?>
	<div style="visibility:hidden;height:5px;overflow:hidden;">
	<select onchange="showleadinfo(this.value)" id="deal_id" name="deal_id" class="inputbox required"><option name="deal_id" value="<?php echo $deal_id_for_client?>">Lead for Asia</option></select>
	</div>
 <?php
 }
 ?>
	
	
</form>