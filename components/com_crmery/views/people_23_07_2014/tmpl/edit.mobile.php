<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

?>

<div data-role='header' data-theme='b'>
	<h1><?php echo CRMText::_('COM_CRMERY_ADD_PERSON'); ?></h1>
		<a href="<?php echo JRoute::_('index.php?option=com_crmery&view=people&task=save'); ?>" data-icon="back" class="ui-btn-left">
			<?php echo CRMText::_('COM_CRMERY_BACK'); ?>
		</a>
</div>

<div data-role="content">

	<form id="edit_form" method="POST" action="<?php echo 'index.php?option=com_crmery&controller=main&model=people&return=people&task=save'; ?>" onsubmit="return save(this)" >
			<div id="editForm">
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_FIRST'); ?><span class="required">*</span></div>
				<div class="crmeryValue wide"><input class="required inputbox" type="text" name="first_name" value="" /></div>
			</div>
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_LAST'); ?><span class="required">*</span></div>
				<div class="crmeryValue wide"><input class="required inputbox" type="text" name="last_name" value=""/></div>
			</div>
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_COMPANY'); ?></div>
				<div class="crmeryValue">
					<input class="inputbox" type="text" name="company" value=""/>
				</div>
			</div>
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_POSITION'); ?></div>
				<div class="crmeryValue"><input class="inputbox" type="text" name="position" value=""/></div>
			</div>

			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_PHONE'); ?></div>
				<div class="crmeryValue"><input class="inputbox ui-input-text ui-body-b ui-corner-all ui-shadow-inset" type="phone" name="phone" value=""/></div>
			</div>
			<div class="crmeryRow">
					<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_EMAIL'); ?></div>
					<div class="crmeryValue"><input class="inputbox" type="email" name="email" value=""/></div>
			</div>
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PERSON_SOURCE'); ?></div>
				<div class="crmeryValue">
					<select data-native-menu="false" data-overlay-theme="a" data-theme="c" name="source_id" tabindex="-1">
						<?php $options = CrmeryHelperDropdown::generateDropdown('source','','',true);
							 if(count($options) > 0) { foreach($options as $option) { 
							 	echo "<option value='".$option['id']."''>".$option['name']."</option>";
							 } } ?>
					</select>
				</div>
			</div>
		<?php
			if ( JRequest::getVar('lead') ) {
				echo '<input type="hidden" name="type" value="lead" />';
			}
		?>
		<input type="submit" name="submit"  value="<?php echo CRMText::_('COM_CRMERY_SUBMIT'); ?>" />
	</form>
</div>