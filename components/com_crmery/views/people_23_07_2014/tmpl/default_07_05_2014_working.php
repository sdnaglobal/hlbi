
<?php



/*------------------------------------------------------------------------

# CRMery

# ------------------------------------------------------------------------

# @author CRMery

# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.

# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Website: http://www.crmery.com

-------------------------------------------------------------------------*/

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' ); ?>



<ul class="entry_buttons">

    <li><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=deals&layout=edit'); ?>"><?php echo ucwords(CRMText::_('COM_CRMERY_ADD_DEALS')); ?></a></li>

    <li style='display:none'><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=import&import_type=deals&layout=import'); ?>"><?php echo ucwords(CRMText::_('COM_CRMERY_IMPORT_DEALS')); ?></a></li>

    </ul>

<h1><?php echo ucwords(CRMText::_('COM_CRMERY_PEOPLE_HEADER')); ?></h1>

<ul class="filter_lists" >

<li class="filter_sentence">

	
	<?php echo CRMText::_('COM_CRMERY_SHOW'); ?>
	
	<span class="filters"><a class="dropdown" id="people_type_link" ><?php echo $this->referral_type_name; ?></a></span>

	<div class="filters" id="people_type">
	
		<ul>
			
		    <?php foreach ( $this->referral_types as $title => $text ){

			     echo "<li><a class='filter_".$title."' onclick=\"newfilter('".$title."')\">".$text."</a></li>";

            }?>							

		</ul>

	</div>

	

    OR <?php echo CRMText::_('COM_CRMERY_NAMED'); ?>

    <td><input class="inputbox filter_input" name="name" type="text" placeholder="<?php echo CRMText::_('COM_CRMERY_ANYTHING'); ?>" value="<?php echo $this->people_filter; ?>"></td>

</li>

<li class="filter_sentence">

    <div class="ajax_loader"></div>

</li>

</ul>

<div class="subline">

<ul class="matched_results">

    <li><span id="people_matched"></span> <?php echo CRMText::_('COM_CRMERY_PEOPLE_MATCHED'); ?> <?php //echo CRMText::_('COM_CRMERY_THERE_ARE'); ?> <?php //echo $this->totalPeople; ?> <?php //echo CRMText::_('COM_CRMERY_PEOPLE_IN_ACCOUNT'); ?></li>

</ul>

<span class="actions">

    <span class="filters"><a class="dropdown" id="column_filter_link"><?php echo CRMText::_('COM_CRMERY_SELECT_COLUMNS'); ?></a></span>

        <div class="filters" id="column_filter">

            <ul>

                 <?php $temp_field_arr = array('company','owner','country','assigned','source','added');
				 
				 
					foreach ( $this->column_filters as $key => $text ){ ?>

                    <?php //$selected = ( in_array($key,$this->selected_columns) ) ? 'checked' : ''; ?>

                    <?php $selected = ( in_array($key,$temp_field_arr) ) ? 'checked' : ''; ?>

                    <li><input type="checkbox" class="column_filter" id="show_<?php echo $key; ?>" <?php echo $selected; ?> > <?php echo $text; ?></li>    

                <?php } ?> 

            </ul>

        </div>

        <?php if ( CrmeryHelperUsers::canExport() ){ ?>    

            <span class="filters"><a href="javascript:void(0)" onclick="exportCsv()"><?php echo CRMText::_('COM_CRMERY_EXPORT_CSV'); ?></a>

        <?php } ?>

</div>

<?php echo CrmeryHelperTemplate::getListEditActions(); ?>

<form method="post" id="list_form" action="<?php echo JRoute::_('index.php?option=com_crmery&view=people'); ?>">

<table class="com_crmery_table" id="people">

		
		  <?php echo $this->people_list->display(); ?>

</table>

<input type="hidden" name="list_type" value="people" />

</form>

<div id="templates" style="display:none;">

    <div id="note_modal" style="display:none;"></div>

    <div id="edit_button"><a class="edit_button_link" id="edit_button_link" href="javascript:void(0)"></a></div>

    <div id="edit_list_modal" style="display:none;" ></div>

</div>
