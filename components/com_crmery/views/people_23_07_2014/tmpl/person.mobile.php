<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

//define person
$person = $this->people[0];

?>

<script type="text/javascript">
	var id  = <?php echo $person['id']; ?>;
	var loc = 'person';
	var model = 'people';
	var person_id  = <?php echo $person['id']; ?>;
	var association_type = 'person';
</script>

<div data-role='header' data-theme='b'>
	<h1><?php echo $person['first_name'].' '.$person['last_name']; ?></h1>
		<a href="<?php echo JRoute::_('index.php?option=com_crmery&view=dashboard'); ?>" data-icon="back" class="ui-btn-left">
		<?php echo CRMText::_('COM_CRMERY_BACK'); ?>
	</a>
</div>

<div data-role='content' data-theme='b'>
	<p><?php if ( array_key_exists('position',$person) ) { echo $person['position'] ? $person['position'].', '.CRMText::_('COM_CRMERY_AT'): '' ; } ?> 
		<strong><?php if ( array_key_exists('company_name',$person ) ) echo $person['company_name']; ?></strong>
	</p>
	<ul data-inset='true' data-role='listview' data-theme="c">
		<?php if ( array_key_exists('phone',$person) && $person['phone']!="") { ?>
		<li>
			<a href="tel://<?php echo $person['phone']; ?>" rel="external"><?php echo $person['phone']; ?></a>
		</li>
		<?php } ?>
		<?php if ( array_key_exists('email',$person) && $person['email']!="") { ?>
		<li>
			<a target="_blank" href="mailto:<?php echo $person['email']; ?>" rel="external"><?php echo $person['email']; ?></a>
		</li>
		<?php } ?>
		<?php if ( array_key_exists('address_1',$person) && $person['address_1']!="") { ?>
		<li>
			<a href="<?php echo JRoute::_('index.php?option=com_crmery&view=people&layout=directions&id='.$person['id']); ?>">
				<?php if ( array_key_exists('home_address_1',$person) ) echo $person['home_address_1'].' '.$person['home_city'].', '.$person['home_state'].' '.$person['home_zip']; ?>
			</a>
		</li>
		<?php } ?>
	</ul>

	<div data-role="collapsible" data-collapsed="false">
		<h3><?php echo CRMText::_('COM_CRMERY_EDIT_NOTES'); ?></h3>
			<ul data-inset='true' data-role='listview' data-theme="c"  id="notes">
				<?php $person['notes']->display(); ?>
			</ul>
	</div>

	<div data-role="collapsible">
			<h3><?php echo CRMText::_('COM_CRMERY_TASKS_AND_EVENTS'); ?></h3>
			<ul data-inset='true' data-role='listview' data-theme="c">
				<?php $this->event_dock->display(); ?>
			</ul>
	</div>
<?php // TODO: Format currency to local ?>
	<div data-role="collapsible">
			<h3><?php echo CRMText::_('COM_CRMERY_EDIT_DEALS'); ?></h3>
			<ul data-inset='true' data-role='listview' data-theme="c">
				<?php
				$n = count($person['deals']);
				for ( $i=0; $i<$n; $i++ ) {
					$deal = $person['deals'][$i];
					$k = $i%2;
				
					echo '<li><a href="'.JRoute::_('index.php?option=com_crmery&view=deals&layout=deal&id='.$deal['id']).'">';
					echo '<span class="ui-li-count">'.$deal['amount'].'</span>';
					echo $deal['name'];
					echo '</a></li>';
				}
				?>
			</ul>
	</div>

	<div data-role="collapsible">
		<h3><?php echo CRMText::_('COM_CRMERY_ADD_NOTE'); ?></h3>
		<?php echo $this->add_note->display(); ?>
	</div>
		<div data-role="collapsible" data-collapsed="false">
		<h3><?php echo CRMText::_('COM_CRMERY_CREATE_TASK'); ?></h3>
			<?php $this->add_task->display(); ?>
		</div>
</div>

