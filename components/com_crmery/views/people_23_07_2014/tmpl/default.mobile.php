<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

	$lead = JRequest::getVar('type')=='leads' ? '&lead=true' : '';
?>

<script type="text/javascript">
	var loc = 'people';
</script>

	<div data-role='header' data-theme='b'>
		<h1><?php echo CRMText::_('COM_CRMERY_LEADS_HEADER'); ?></h1>
			<a href="<?php echo JRoute::_('index.php?option=com_crmery&view=dashboard'); ?>" data-icon="back" class="ui-btn-left">
				<?php echo CRMText::_('COM_CRMERY_BACK'); ?>
			</a>
			<a href="<?php echo JRoute::_('index.php?option=com_crmery&view=people&layout=edit&'.$lead); ?>" data-icon="plus" class="ui-btn-right">
				<?php echo CRMText::_('COM_CRMERY_NEW'); ?>
			</a>
	</div>

	<div data-role="content">	
		<ul class="ui-listview" data-role="listview" data-filter="true" data-autodividers="true" data-theme="c">
			<?php
			$n = count($this->people);
			$k = 0;
				for($i=0;$i<$n;$i++) {
					$person = $this->people[$i];
					if ( $i>0 ){
						$currName = array_key_exists('last_name',$person) && strlen($person['last_name']) > 0 ? "last_name" : "first_name";
						$compName = array_key_exists('last_name',$this->people[$i-1]) && strlen($this->people[$i-1]['last_name']) > 0 ? "last_name" : "first_name";
					}
					if($i==0 || ( $i > 0 && substr_compare(strtolower($person[$currName]),strtolower($this->people[$i-1][$compName]),0,1) ) ) { 
						echo "<li data-role='list-divider'>".ucfirst(substr($person['last_name'],0,1))."</li>";
					}
			?>
				<li data-filtertext="<?php echo $person['first_name'].' '.$person['last_name']; ?>">
					<a href="<?php echo JRoute::_('index.php?option=com_crmery&view=people&layout=person&id='.$person['id']); ?>">
						<h3 class="ui-li-heading"><?php echo $person['first_name'].' '.$person['last_name']; ?></h3>
						<p class="ui-li-desc"><?php echo JText::sprintf('COM_CRMERY_MOBILE_PERSON_DESC',$person['position'],$person['company_name']); ?></p>
					</a>
				</li>
			<?php } ?>
		</ul>
	</div>