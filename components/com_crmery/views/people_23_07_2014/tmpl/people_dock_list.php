<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$c = count($this->people);
	for( $i=0; $i<$c; $i++ ) {
		
		$person = $this->people[$i];
		$k=$i%2;
		$data = array(
			array('ref'=>'k','data'=>$k),
			array('ref'=>'person','data'=>$person)
			);
		$person_entry = CrmeryHelperView::getView('people','people_dock_entry',$data);
		$person_entry->display();
	} ?>