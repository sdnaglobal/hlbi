<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
 
//$person_id=123;
  //$ref_temp = substr(JURI::base(),0,-1);
  // echo '==>'.$referralurl = $ref_temp.JRoute::_('index.php?option=com_crmery&view=people&layout=person&id='.$person_id);
  
  
if(isset($_GET['referralid']))
{
	
	$action = $_GET['action'];
	$updateid = (int)$_GET['referralid'];
	
	if($action == 'accepted')
		$value = 1;
	else
		$value = 2;
		
 	$db = JFactory::getDbo();
	
	$pquery = $db->getQuery(true);
	$pquery = "UPDATE #__crmery_people SET Accepted='$value' WHERE id=$updateid";
		  
	$db->setQuery($pquery);
	$db->Query();
	
	
	// temporary stop the emails comment start here  
	
	$aquery = $db->getQuery(true);
	$aquery = "SELECT owner_id,assignee_id FROM #__crmery_people WHERE id = ".$updateid;
		  
	$db->setQuery($aquery);
	$aresults = $db->loadAssocList();
	$aresult = $aresults[0];
	
	$equery = $db->getQuery(true);
	$equery = "SELECT cu.uid, u.email FROM #__crmery_users as cu, #__users as u WHERE cu.id=".$aresult['assignee_id']."  and u.id=cu.uid";
		  
	$db->setQuery($equery);
	$eresults = $db->loadAssocList();
	$eresult = $eresults[0];
	
	$assigne_email = $eresult['email'];
	
	$oquery = $db->getQuery(true);
	$oquery = "SELECT cu.uid, u.email, cu.first_name, cu.last_name FROM #__crmery_users as cu, #__users as u WHERE cu.id=".$aresult['owner_id']." and u.id=cu.uid";

		
	$db->setQuery($oquery);
	$oresults = $db->loadAssocList();
	$oresult = $oresults[0];
	
	$ownername = $oresult['first_name'] .' '.$oresult['last_name'];
	
	$owner_email = $oresult['email'];
	
	$referralurl =  JURI::base().'index.php?option=com_crmery&view=people&layout=person&id='.$updateid;
	//.JRoute::_('index.php?option=com_crmery&view=people&layout=person&id='.$updateid);
	//echo '<br>--->'.JRoute::_($referralurl);
	$config =& JFactory::getConfig();
	$sitename = $config->getValue( 'config.sitename' );

	$mainframe = JFactory::getApplication();
	$fromEmail = $mainframe->getCfg('mailfrom');
				
	// send email to owner about referral accept/decline 
	
	$mySubject = 'Your Referral is '.$action;
	$recipient = $owner_email;
	
	$mail =& JFactory::getMailer();	
	$mail->setSubject($mySubject);
	$mail->addRecipient($recipient);
	$myMessage = "Dear User". "<br />";
	$myMessage .= "Your referral on $sitename is $action by assigned user."."<br />";
	$myMessage .= "<br><a href='$referralurl' >Click and View Details of Referral</a> ";
	$myMessage .= "<br />";
	$myMessage .= "<br />";
	$myMessage .= "Regards,"."<br />";
	$myMessage .= "$sitename "."<br />";
 

	$mail->setBody($myMessage);
	$mail->IsHTML(true);
	$send =& $mail->Send(); 
	
	
	
	// send email to assignee about his action of accept/decline of referral
	
	$mySubject1 = "You have $action Referral on ".$sitename;
	$recipient1 = $assigne_email;

	$mail1 =& JFactory::getMailer();		

	$mail1->setSubject($mySubject1);
	$mail1->addRecipient($recipient1);
	$myMessage1 = "Dear User". "<br />";
	$myMessage1 .= "You have $action referral on $sitename, which is assigned to you by $ownername"."<br />";
	$myMessage1 .= "We have send an email confirmation to $ownername about your action."."<br />";
	$myMessage1 .= "<a href='$referralurl' >Click and View Details of Referral</a> ";
	$myMessage1 .= "<br />";
	$myMessage1 .= "<br />";
	$myMessage1 .= "Regards,"."<br />";
	$myMessage1 .= "$sitename "."<br />";

	$mail1->setBody($myMessage1);
	$mail1->IsHTML(true);
	$send =& $mail1->Send(); 
	
	// send email to hlb admin about accept/decline action on referral 
	
	$msg = 'Referral has been '.$action;
	
	if(isset($_GET['return']))
		$returnurl = 'index.php?option=com_crmery&view=people&layout=person&id='.$updateid;
	else
	    $returnurl = 'index.php?option=com_crmery&view=people&layout=person';
	
	$allDone =& JFactory::getApplication();
	$allDone->redirect(JRoute::_($returnurl),$msg);
	
	 
	
	// temporary stop the emails comment end here 
	
}

function get_receiving_country_by_user_id($userid)
{
	
	$db = JFactory::getDbo();
	$query_cntry = $db->getQuery(true);
	$query_cntry = "SELECT country_id FROM #__crmery_users where uid=".$userid;

	$db->setQuery($query_cntry);
	$cntry_id = @explode(',',$db->loadResult());
	return $cntryid = $cntry_id[0];
}

function get_country_by_user_id($userid)
{
	
	$db = JFactory::getDbo();
	$query_cntry = $db->getQuery(true);
	$query_cntry = "SELECT country_id FROM #__crmery_users where id=".$userid;

	$db->setQuery($query_cntry);
	$cntry_id = @explode(',',$db->loadResult());
	return $cntryid = $cntry_id[0];
}

function get_country_name_by_id($cntryid)
{
	$db = JFactory::getDbo();
	$query_cntry = $db->getQuery(true);
	$query_cntry = $db->getQuery(true);
	$query_cntry = "SELECT country_name FROM #__crmery_currencies where country_id=".$cntryid;

	$db->setQuery($query_cntry);
	return $cntry_name = $db->loadResult();
	
}

$current_financial_year_pure = CrmeryHelperCrmery::getFinancialYear();
$current_financial_date = explode("-",$current_financial_year_pure);
$current_financial_year = $current_financial_date[0];

?>

<?php if ( !(JRequest::getVar('id')) ){ ?>
  <thead>
    <th class="checkbox_column"><input type="checkbox" onclick="selectAll(this);" /></th>
    <th class="" > REFERRAL CODE<?php //echo CRMText::_('COM_CRMERY_REFERRAL_CODE'); ?></th>
    <th class="" ><div class="sort_order">
	<?php echo CRMText::_('COM_CRMERY_PEOPLE_CLIENT_NAME'); ?>
	</div></th>
	
   

	
	
    <!-- <th class="last_name" ><div class="sort_order"><a class="p.last_name" onclick="sortTable('p.last_name',this)"><?php //echo CRMText::_('COM_CRMERY_PEOPLE_LAST_NAME'); ?></a></div></th> -->
 
    <th class="owner" ><div class="sort_order"> <!-- <a class="u.last_name" onclick="sortTable('u.last_name',this)"> -->
	<?php echo CRMText::_('COM_CRMERY_PEOPLE_REFERRING_PARTNER'); ?>
	</div></th>
	
	<th class="" > 
	<?php echo CRMText::_('COM_CRMERY_REFERRING_FIRM'); ?>
 
	</th>
	
	<th class="country" >
		<?php echo CRMText::_('COM_CRMERY_PEOPLE_COUNTRY_REFRRING'); ?>
	</th>
	<th class="company"><div class="sort_order"> <!--<a class="c.name" onclick="sortTable('c.name',this)"> -->
	<?php echo CRMText::_('COM_CRMERY_RECEIVING_FIRM'); ?>
	</div></th>
	
    <th class="assigned" ><div class="sort_order"> <!-- <a class="u2.last_name" onclick="sortTable('u2.last_name',this)">-->
	<?php echo CRMText::_('COM_CRMERY_RECEIVING_PARTNER'); ?>
	</div></th>
	<th class="" >
		<?php echo CRMText::_('COM_CRMERY_PEOPLE_COUNTRY_RECEIVING'); ?>
	</th>
	<th class="" >
		<?php echo CRMText::_('COM_CRMERY_DEALS_AMOUNT'); ?> in GBP(<?php echo $current_financial_year; ?>)
	</th>
	<th class="amount_last_year" ><div class="sort_order"><?php echo CRMText::_('COM_CRMERY_DEALS_AMOUNT'); ?> in GBP(<?php echo ($current_financial_year-1) ; ?>) </div></th>
    <th class="email" ><div class="sort_order"><!-- <a class="p.email" onclick="sortTable('p.email',this)"> --> <?php echo CRMText::_('COM_CRMERY_PEOPLE_EMAIL'); ?> </div></th>
    <th class="phone" ><div class="sort_order"> <!-- <a class="p.phone" onclick="sortTable('p.phone',this)"> --> <?php echo CRMText::_('COM_CRMERY_PEOPLE_PHONE'); ?></div></th>
    <th class="mobile_phone" ><div class="sort_order"> <!--<a class="p.mobile_phone" onclick="sortTable('p.mobile_phone',this)"> --><?php echo CRMText::_('COM_CRMERY_MOBILE_PHONE'); ?></div></th>
    <th class="position" ><div class="sort_order"> <!-- <a class="p.position" onclick="sortTable('p.position',this)">--><?php echo CRMText::_('COM_CRMERY_TITLE'); ?></div></th>
    <th class="status" ><div class="sort_order"> <!--<a class="stat.ordering" onclick="sortTable('stat.ordering',this)"> --> <?php echo CRMText::_('COM_CRMERY_PEOPLE_STATUS'); ?></div></th>
    <th class="source" ><div class="sort_order"> <!-- <a class="source.ordering" onclick="sortTable('source.ordering',this)"> --><?php echo CRMText::_('COM_CRMERY_PEOPLE_SOURCE'); ?></div></th>
    <th class="type" ><div class="sort_order"> <!-- <a class="p.type" onclick="sortTable('p.type',this)">--><?php echo CRMText::_('COM_CRMERY_PEOPLE_TYPE'); ?> </div></th>
    <th class="next_task" ><?php echo CRMText::_('COM_CRMERY_PEOPLE_TASK'); ?></th>
    <th class="next_task" ><?php echo CRMText::_('COM_CRMERY_PEOPLE_DUE'); ?></th>
    <th class="notes" ><?php echo CRMText::_('COM_CRMERY_PEOPLE_NOTES'); ?></th>
    <th class="city" ><?php echo CRMText::_('COM_CRMERY_PEOPLE_CITY'); ?></th>
    <th class="state" ><?php echo CRMText::_('COM_CRMERY_PEOPLE_STATE'); ?></th>

    <th class="postal_code" ><?php echo CRMText::_('COM_CRMERY_PEOPLE_POSTAL_CODE'); ?></th>
    <th class="added" ><div class="sort_order"> <a class="p.created" onclick="sortTable('p.created',this)">  <?php echo CRMText::_('COM_CRMERY_PEOPLE_ADDED'); ?> </div></th>
    <th class="updated" ><div class="sort_order">  <a class="p.modified" onclick="sortTable('p.modified',this)"> <?php echo CRMText::_('COM_CRMERY_PEOPLE_UPDATED'); ?></div></th>
	<th class="" ><?php echo CRMText::_('Referral Status'); ?></th>
	<th class="billing" ><?php echo CRMText::_('COM_CRMERY_PEOPLE_BILLINGINFO'); ?></th>
</thead>
<tbody>
<?php } ?>
<?php

        // count rows
        $n = count($this->people);
        $k = 0;
		
        //generate status dropdown
        $statuses = CrmeryHelperPeople::getStatusList();

        $status_html  = "";
        $status_html .= '<ul>';
            $status_html .= '<li><a href="javascript:void(0)" class="status_select dropdown_item" data-value="0"><div class="person-status-none"></div></a></li>';
            if (count($statuses)) { foreach($statuses as $key => $status ){
                $status_html .= '<li><a href="javascript:void(0)" class="status_select dropdown_item" data-value="'.$status['id'].'"><div class="person-status-color" style="background-color:#'.$status['color'].'"></div><div class="person-status">'.$status['name'].'</div></a></li>';
            }}
        $status_html .= '</ul>';

        //generate source dropdown
        $sources = CrmeryHelperDeal::getSources();

        $source_html = "";
        $source_html .= '<ul>';  
            if ( count($sources) ){ foreach($sources as $id => $name ) {
                $source_html .= '<li><a href="javascript:void(0)" class="source_select dropdown_item" data-value="'.$id.'">'.$name.'</a></li>';
            }}
        $source_html .= '</ul>';

        //generate type dropdown
        $types = CrmeryHelperPeople::getPeopleTypes(FALSE);

        $type_html = "";
        $type_html .= '<ul>';
            if ( count($types) ){ foreach($types as $id => $name ) {
                $type_html .= '<li><a href="javascript:void(0)" class="type_select dropdown_item" data-value="'.$id.'">'.ucwords($name).'</a></li>';
            }}
        $type_html .= '</ul>';

        //base url
        $base = JURI::base();
		
		

            if ( count($this->people) > 0 ){foreach($this->people as $key => $person){ 
			

                $k = $key%2;

                //assign null data
                $person['company_name'] = ( $person['company_name'] == '' ) ? CRMText::_('COM_CRMERY_NO_COMPANY') : $person['company_name'];
                $person['status_name'] = ( $person['status_name'] == '' ) ? CRMText::_('COM_CRMERY_NO_STATUS') : $person['status_name'];
                $person['source_name'] = ( $person['source_name'] == '' ) ? CRMText::_('COM_CRMERY_NO_SOURCE') : $person['source_name'];

                if ( array_key_exists('avatar',$person) && $person['avatar'] != "" && $person['avatar'] != null ){
                    $avatar_html = '<td class="" ><img id="avatar_img_'.$person['id'].'" data-item-type="people" data-item-id="'.$person['id'].'" class="avatar" src="'.$base.'components/com_crmery/media/avatars/'.$person['avatar'].'"/></td>';
                }else{
                    $avatar_html = '<td class="" ><img id="avatar_img_'.$person['id'].'" data-item-type="people" data-item-id="'.$person['id'].'" class="avatar" src="'.$base.'components/com_crmery/media/images/person.png'.'"/></td>';
                }

                $person['mobile_phone'] = $person['mobile_phone'] > 0 ? $person['mobile_phone'] : "";
                $db = JFactory::getDbo();
				$deal_name = ''; 
			    $query_deal = $db->getQuery(true);
			    $query_deal = "SELECT d.name FROM #__crmery_deals as d, #__crmery_people_cf as cf where d.id=cf.association_id and cf.person_id=".$person['id']." and cf.association_type='deal'";

				$db->setQuery($query_deal);
				$deal_name = $db->loadResult();
				
				
				$rec_firm_name = '';
				$rec_firms = CrmeryHelperCompany::getCompany($person['referring_firm_id']);
                $rec_firm_name = $rec_firms[0]['name'];
				
				
				$ref_cntryid = get_country_by_user_id($person['owner_id']);
				
				if($ref_cntryid >0)
				{
					$ref_country_name = get_country_name_by_id($ref_cntryid) ;
				}
				else
				{
					$ref_country_name = "N/A";
				}
				//echo $person['uid']."<br>";
				$rec_cntryid = get_receiving_country_by_user_id($person['uid']);
				
				if($rec_cntryid >0)
				{
					$rec_country_name = get_country_name_by_id($rec_cntryid) ;
				}
				else
				{
					$rec_country_name = "N/A";
				}
				$refvalue = '';
				
				$refvalue = get_amount_billed_by_referral_id($person['id'],$current_financial_year);
				
				$last_year_amount = '';
				$last_year_amount = get_amount_billed_by_referral_id($person['id'],($current_financial_year-1));
				
	
				
                $company_link = $person['company_id'] > 0 ? "<a href='".JRoute::_('index.php?option=com_crmery&view=companies&layout=company&id='.$person['company_id'])."'>".$person['company_name']."</a>" : $person['company_name'];
				$company_link =  $person['company_name'];
                echo "<tr id='list_row_".$person['id']."' class='crmery_row_".$k."'>",
                     '<td><input type="checkbox" name="ids[]" value="'.$person['id'].'" /></td>',
                       // $avatar_html,
					  '<td><a href="'.JRoute::_('index.php?option=com_crmery&view=people&layout=person&id='.$person['id']).'">'.CRMText::_('COM_CRMERY_REFERRAL_CODE_PREFIX').$person['id'].'</a></td>' ,
                     '<td class=" list_edit_button" id="list_'.$person['id'].'" ><div class="title_holder">'.$deal_name.'</div></td>',
				
					// '.$person['first_name'].' '.$person['last_name'].

                     //'<td class="last_name list_edit_button" id="list_'.$person['id'].'" ><div class="title_holder"><a href="'.JRoute::_('index.php?option=com_crmery&view=people&layout=person&id='.$person['id']).'">'.$person['last_name'].'</a></div></td>',
                     "<td class='owner' ><!--<a href='javascript:void(0)' class='dropdown' id='person_owner_".$person['id']."_link'> -->".$person['owner_first_name']." ".$person['owner_last_name']."<!--</a>--></td>",
					 
                     '<div class="filters" data-item="people" data-field="owner_id" data-item-id="'.$person['id'].'" id="person_owner_'.$person['id'].'">',
                        $user_html,
                     '</div>',
					 
					   '<td>'.$rec_firm_name.'</td>' ,
						
					   '<td class="country">'.$ref_country_name.'</td>',
					  "<td class='company' >",$company_link,"</td>",
                     "<td class='assigned'>","<!--<a href='javascript:void(0)' class='dropdown' id='person_assigned_".$person['id']."_link'>-->".$person['first_name'].' '.$person['last_name']."<!--</a>--></td>",
                     '<div class="filters" data-item="people" data-field="assignee_id" data-item-id="'.$person['id'].'" id="person_assigned_'.$person['id'].'">',
                        $user_html,
                     '</div>',
                     '<td >'.$rec_country_name.'</td>',
					 '<td >'.number_format($refvalue,2,'.',',').'</td>',
					 '<td class="amount_last_year">'.number_format($last_year_amount,2,'.',',').'</td>',
                     '<td class="email">'.$person['email'].'</td>',
                     '<td class="phone">'.$person['phone'].'</td>',
                     '<td class="mobile_phone">'.$person['mobile_phone'].'</td>',
                     '<td class="position">'.$person['position'].'</td>',
                     "<td class='status' ><!--<a href='javascript:void(0);' class='dropdown' id='person_status_".$person['id']."_link'>--><div class='person-status-color' style='background-color:#".$person['status_color']."'></div><div class='person-status'>".$person['status_name']."</div><!--</a>--></td>",
                     '<div class="filters" data-item="people" data-field="status_id" data-item-id="'.$person['id'].'" id="person_status_'.$person['id'].'">',
                         $status_html,
                     '</div>',
                     "<td class='source' ><!--<a href='javascript:void(0);' class='dropdown' id='person_source_".$person['id']."_link'>-->".$person['source_name']."<!--</a>--></td>",
                     '<div class="filters" data-item="people" data-field="source_id" data-item-id="'.$person['id'].'" id="person_source_'.$person['id'].'">',
                         $source_html,
                     '</div>',
                     "<td class='type' ><!--<a href='javascript:void(0);' class='dropdown' id='person_type_".$person['id']."_link'> -->".ucwords($person['type'])."<!--</a>--></td>",
                     '<div class="filters" data-item="people" data-field="type" data-item-id="'.$person['id'].'" id="person_type_'.$person['id'].'">',
                         $type_html,
                     '</div>',
                     '<td class="next_task" ><a onclick="editEvent('.$person['event_id'].',\''.$person['event_type'].'\')">'.$person['event_name']."</a></td>",
                     '<td class="next_task" >'.CrmeryHelperDate::formatDate($person["event_due_date"]).'</td>',
                     '<td class="notes" ><a href="javascript:void(0);" onclick="openNoteModal(\''.$person['id'].'\',\'person\')"><img src="'.$base.'components/com_crmery/media/images/notes.png'.'"/></a></td>',
                     '<td class="city">'.$person['work_city'].'</td>',
                     '<td class="state">'.$person['work_state'].'</td>',
                     '<td class="postal_code">'.$person['work_zip'].'</td>',
                    // '<td class="country">'.$person['work_country'].'</td>',
                   
                     '<td class="added">'.CrmeryHelperDate::formatDate($person['created']).'</td>',
                     '<td class="updated">'.CrmeryHelperDate::formatDate($person['modified']).'</td>',				 
                     '<td class="billing">';
					 
				//	 $person['id']
				
				if($person['Accepted'] == 0){
						 echo '<a href="'.JRoute::_('index.php?option=com_crmery&view=people&layout=person&referralid='.$person['id']).'&action=accepted"> Accept Referral </a> ';	 
						 }elseif($person['Accepted'] == 1){  
						  echo 'Accepted';
						 }
						 else
						 {
							echo 'Declined';
						 }
						 
				
					echo '</td>',
					 '<td class="billing">';
					 
					 if($person['Accepted'] == 1){
						echo '<a href="'.JRoute::_('index.php?option=com_crmery&view=billing&layout=person&id='.$person['id']).'">'.CRMText::_('COM_CRMERY_PEOPLE_BILLINGINFO').'</a>';
					 }
					 else 
						echo 'No Billing';
					 
					 echo '</td>',
                 "</tr>";

                //free up php memory
                unset($person);
                unset($this->people[$key]);
            } 
		}
        ?>

<?php 

function get_amount_billed_by_referral_id($ref_id,$year)
{

	$db = JFactory::getDbo();
	$billing = $db->getQuery(true);
	
	$billing = "select sum(SterlingAmount) as total from #__crmery_referral_payments where FeePaid=1 and referral_ID=".$ref_id." and YEAR(STR_TO_DATE(BillingYear, '%d/%m/%Y'))=".$year." group by referral_ID  ";
	$db->setQuery($billing);
 	$billingdetails = $db->loadObjectList();
 
	$GBPsum = $billingdetails[0]->total;

	return $GBPsum;
	
}
?>		
		
<?php if ( !(JRequest::getVar('id')) ){ ?>
    </tbody>
<tfoot>
    <tr>
       <td colspan="20"><?php echo $this->pagination->getListFooter(); ?></td>
    </tr>
 </tfoot>
<script type="text/javascript">
    //update total people count
    jQuery("#people_matched").empty().html("<?php echo $this->total; ?>");
    window.top.window.assignFilterOrder();
</script>
<?php } ?>