<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewPeople extends JView
{
    function display($tpl = null)
    {

        $id = JRequest::getVar('id') ? JRequest::getVar('id') : null;

        $company_id = JRequest::getVar('company_id');

        //retrieve people from model
        $model = JModel::getInstance('people','CrmeryModel');
        $model->set('company_id',$company_id);
        

        $layout = $this->getLayout();

        $total = $model->getTotal();
        $pagination = $model->getPagination();
        $this->assignRef("total",$total);
        $this->assignRef('pagination',$pagination);
        
		
        //assign refs
        switch ( $layout ){

            case "edit":
				$people = $model->getPeople($id);
                $this->assignRef('person',$people[0]);
            break;
            case "people_dock_list":
				$people = $model->getPeople($id);
                $this->assignRef('people',$people);
            break;
            default:
				$people = $model->getPeople($id);
				$this->assignRef('people',$people);
				$state = $model->getState();
                $this->assignRef('state',$state);
            break;
        }
        
        //display view
        parent::display($tpl);      
    }
    
}
        
        