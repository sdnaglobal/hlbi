<?php

/*------------------------------------------------------------------------

# CRMery

# ------------------------------------------------------------------------

# @author CRMery

# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.

# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Website: http://www.crmery.com

-------------------------------------------------------------------------*/

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' ); 



jimport( 'joomla.application.component.view');



class CrmeryViewPeople extends CrmeryHelperView

{

    function display($tpl = null)

    {

        ///retrieve task list from model

        $model = &$this->getModel();

        $state = $model->getState();



        //Pagination

        $pagination = $model->getPagination();

        $this->assignRef('pagination', $pagination);

        

        //session data

        $session = JFactory::getSession();

        

        $user_id = CrmeryHelperUsers::getUserId();

        $team_id = CrmeryHelperUsers::getTeamId();

        $member_role = CrmeryHelperUsers::getRole();

        

        $people_type_name = $session->get('people_type_filter');

        $user = $session->get('people_user_filter');

        $team = $session->get('people_team_filter');

        $stage = $session->get('people_stage_filter');

        $tag = $session->get('people_tag_filter');

        $status = $session->get('people_status_filter');

        

        //load java

        $document = & JFactory::getDocument();

        $document->addScript( JURI::base().'components/com_crmery/media/js/people_manager.js' );

        

        //get list of people

        if ( CrmeryHelperTemplate::isMobile()) {

            $model->set("limit",false);

            $model->set("order",'p.last_name ASC');

        }

        $people = $model->getPeople();

        $person = array();

        

        //determine if we are editing an existing person entry

        if( JRequest::getVar('id') ){

            //grab deal object

            $person = $people[0];

            if (is_null($person['id'])){

                $app = JFactory::getApplication();

                $app->redirect(JRoute::_('index.php?option=com_crmery&view=people'),CRMText::_('COM_CRMERY_NOT_AUTHORIZED'));

            }

            $person['header'] = CRMText::_('COM_CRMERY_EDIT').' '.$person['first_name'] . ' ' . $person['last_name']; 

        }else{

            //else we are creating a new entry

            $person = array();

            $person['id'] = '';

            $person['first_name'] = "";

            $person['last_name'] = "";

            $person['company_id'] = ( JRequest::getVar('company_id') ) ? JRequest::getVar('company_id') : null;

            $person['deal_id'] = ( JRequest::getVar('deal_id') ) ? JRequest::getVar('deal_id') : null;

            

            //get company name to prefill data on page and hidden fields

            if ( $person['company_id'] ) {

                $company = CrmeryHelperCompany::getCompany($person['company_id']);

                $person['company_name'] = $company[0]['name'];

                $person['company_id'] = $company[0]['id'];

            }

            

            //get deal name to prefill data on page and hidden fields

            if ( $person['deal_id'] ) {

                $deal = CrmeryHelperDeal::getDeal($person['deal_id']);

                $person['deal_name'] = $deal[0]['name'];

                $person['deal_id'] = $deal[0]['id'];

            }

            

            //set null fields

            $person['position'] = "";

            $person['phone'] = "";

            $person['email'] = "";

            $person['type'] = '';

            $person['source_id'] = null;

            $person['status_id'] = null;

            $person['header'] = CRMText::_('COM_CRMERY_PERSON_ADD'); 


        }



        //get total people associated with users account

        $total_people = CrmeryHelperUsers::getPeopleCount($user_id,$team_id,$member_role);
		
		//get referral work filter

        $referral_types = CrmeryHelperPeople::getReferralWork();
		
		$referral_type_name = $session->get('referral_type_filter');

        $referral_type_name = ( $referral_type_name ) ? $referral_types[$referral_type_name] : $referral_types['all_referrals'];
		
        //get filter types

        $people_types = CrmeryHelperPeople::getPeopleTypes();

        $people_type_name = ( $people_type_name && array_key_exists($people_type_name,$people_types) ) ? $people_types[$people_type_name] : $people_types['all'];

        

        //get column filters

        $column_filters = CrmeryHelperPeople::getColumnFilters();

        $selected_columns = CrmeryHelperPeople::getSelectedColumnFilters();

        

        //get user filter

        //get associated users//teams

        $teams = CrmeryHelperUsers::getTeams();

        $users = CrmeryHelperUsers::getUsers();

        

        if ( $user AND $user != $user_id AND $user != 'all' ){

            $user_info = CrmeryHelperUsers::getUsers($user);

            $user_info = $user_info[0];

            $user_name = $user_info['first_name'] . " " . $user_info['last_name'];

        }else if ( $team ){

            $team_info = CrmeryHelperUsers::getTeams($team);

            $team_info = $team_info[0];

            $user_name = $team_info['team_name'].CRMText::_('COM_CRMERY_TEAM_APPEND');

        }else if ( $user == 'all' ) {

            $user_name = CRMText::_('COM_CRMERY_ALL_USERS');

        }else{

            $user_name = CRMText::_('COM_CRMERY_ME');            

        }

        

        //get stage filter

        $stages = CrmeryHelperPeople::getStages();

        $stages_name = ( $stage ) ? $stages[$stage] : $stages['past_thirty'];

        

        //get status filter

        $status_list = CrmeryHelperPeople::getStatusList();

        for ( $i=0; $i<count($status_list); $i++ ){

            if ( $status_list[$i]['id'] == $status AND $status != 'any' ){

                $status_name = $status_list[$i]['name'];

                break;

            }

        }

        $status_name = ( $status AND $status != 'any' ) ? $status_name : 'any status';



        $dropdowns = $model->getDropdowns();



        //Load Events & Tasks for person

        $layout = $this->getLayout();

        if ( $layout == "person" ){
				

                $model = & JModel::getInstance('event','CrmeryModel');

                $events = $model->getEvents("person",null,JRequest::getVar('id'));

                $ref = array ( array('ref'=>'events','data'=>$events) );

                $eventDock = CrmeryHelperView::getView('events','event_dock',$ref);

                $this->assignRef('event_dock',$eventDock);



                $deal_dock = CrmeryHelperView::getView('deals','deal_dock',array(array('ref'=>'deals','data'=>$person['deals'])));

                $this->assignRef('deal_dock',$deal_dock);



                $document_list = CrmeryHelperView::getView('documents','document_row',array(array('ref'=>'documents','data'=>$person['documents'])));

                $this->assignRef('document_list',$document_list);



                $custom_fields_view = CrmeryHelperView::getView('custom','default');

                $type = "people";

                $custom_fields_view->assignRef('type',$type);

                $custom_fields_view->assignRef('item',$person);

                $this->assignRef('custom_fields_view',$custom_fields_view);



                $acymailing = CrmeryHelperConfig::checkAcymailing() && CrmeryHelperConfig::getConfigValue('acy_person') == 1;

                $this->assignRef('acymailing',$acymailing);

                if ( $acymailing ){

                    $mailing_list = new CrmeryHelperMailinglists();

                    $mailing_lists = $mailing_list->getMailingLists();

                    $newsletters = array();

                    if ( is_array($mailing_lists) && array_key_exists(0,$mailing_lists) ) { 

                        $newsletters = $mailing_list->getNewsletters($mailing_lists[0]->listid);

                    }

                    $acymailing_dock = CrmeryHelperView::getView('acymailing','default');

                    $acymailing_dock->assignRef('newsletters',$newsletters);

                    $acymailing_dock->assignRef('mailing_lists',$mailing_lists);

                    $this->assignRef('acymailing_dock',$acymailing_dock);

                }



                if ( CrmeryHelperBanter::hasBanter() ){

                    $room_list = new CrmeryHelperTranscriptlists();

                    $room_lists = $room_list->getRooms();

                    $transcripts = array();

                    if ( is_array($room_lists) && count($room_lists) > 0 ) { 

                        $transcripts = $room_list->getTranscripts($room_lists[0]->id);

                    }

                    $banter_dock = CrmeryHelperView::getView('banter','default');

                    $banter_dock->assignRef('rooms',$room_lists);

                    $banter_dock->assignRef('transcripts',$transcripts);

                    $this->assignRef('banter_dock',$banter_dock);

                }



                 /** get latest activities **/

                $activityHelper = new CRMeryHelperActivity;

                $activity = $activityHelper->getActivity();

                $latest_activities = CrmeryHelperView::getView('activities','default');

                $latest_activities_view = CrmeryHelperView::getView('activities','latest_activities');

                $latest_activities_view->assignRef('activity',$activity);

                $latest_activities->assignRef('latest_activities',$latest_activities_view);

                $this->assignRef('latest_activities',$latest_activities);

        }



        if ( $layout == "default" ){
		
			//die("here");

            $people_list = CrmeryHelperView::getView('people','list',array(array('ref'=>'people','data'=>$people)));

            $total = $model->getTotal();

            $people_list->assignRef("total",$total);

            $people_list->assignRef('pagination',$pagination);

            $this->assignRef('people_list',$people_list);
			
			$document->addScriptDeclaration("

            loc = 'people';

            order_url = 'index.php?option=com_crmery&view=people&layout=list&format=raw&tmpl=component';

            order_dir = '".$state->get('People.filter_order_Dir')."';

            order_col = '".$state->get('People.filter_order')."';");



            $people_filter = $state->get('Deal.people_name');

            $this->assignRef('people_filter',$people_filter);

        }

 

        if ( $layout == "edit" ){

            $item = JRequest::getVar('id') && array_key_exists(0,$people) ? $people[0] : array('id'=>'');

            $edit_custom_fields_view = CrmeryHelperView::getView('custom','edit');

            $type = "people";

            $edit_custom_fields_view->assignRef('type',$type);

            $edit_custom_fields_view->assignRef('item',$item);

            $this->assignRef('edit_custom_fields_view',$edit_custom_fields_view);



            $companyModel =& JModel::getInstance('company','CrmeryModel');

            $json = TRUE;

            $companyNames = $companyModel->getCompanyNames($json);

            $document->addScriptDeclaration("var company_names=".$companyNames.";");

        }



        if ( CrmeryHelperTemplate::isMobile() && JRequest::getVar('id')){

            $add_note = CrmeryHelperView::getView('note','edit');

            $this->assignRef('add_note',$add_note);



            $add_task = CrmeryHelperView::getView('events','edit_task');

            $association_type = 'person';

            $association_id = JRequest::getVar('id');

            $add_task->assignRef('association_type',$association_type);

            $add_task->assignRef('association_id', $association_id);



            $this->assignRef('add_task', $add_task);

        }



      //  $userNames = CrmeryHelperUsers::getUsers(null,true);

     //   $document->addScriptDeclaration("var user_names=".json_encode($userNames).";");



        //assign results to view

        $this->assignRef('people',$people);

        $this->assignRef('person',$person);

        $this->assignRef('totalPeople',$total_people);

        $this->assignRef('people_type_name',$people_type_name);

        $this->assignRef('people_types',$people_types);

        $this->assignRef('user_id',$user_id);

        $this->assignRef('team_id',$team_id);

        $this->assignRef('member_role',$member_role);

        $this->assignRef('user_name',$user_name);

        $this->assignRef('teams',$teams);

        $this->assignRef('users',$users);

        $this->assignRef('stages',$stages);

        $this->assignRef('stages_name',$stages_name);

        $this->assignRef('status_list',$status_list);

        $this->assignRef('status_name',$status_name);

        $this->assignRef('state',$state);

        $this->assignRef('column_filters',$column_filters);

        $this->assignRef('selected_columns',$selected_columns);

        $this->assignRef('dropdowns',$dropdowns);
		
		$this->assignRef('referral_type_name',$referral_type_name);
		
		$this->assignRef('referral_types',$referral_types);

            

        //display

        parent::display($tpl);

    }

    

}

?>

