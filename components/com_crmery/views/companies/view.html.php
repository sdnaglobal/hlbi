<?php

/*------------------------------------------------------------------------

# CRMery

# ------------------------------------------------------------------------

# @author CRMery

# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.

# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Website: http://www.crmery.com

-------------------------------------------------------------------------*/

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' ); 



jimport( 'joomla.application.component.view');



class CrmeryViewCompanies extends CRMeryHelperView

{

    function display($tpl = null)

    {
		unset($_SESSION['yearSess']); 
        $app = JFactory::getApplication();



        //get model

        $model = & JModel::getInstance('company','CrmeryModel');

        if ( CrmeryHelperTemplate::isMobile() ){

            $model->set("_user","all");

        }



        //assign pagination

        $pagination = $model->getPagination();

        $this->assignRef('pagination',$pagination);



        $state = $model->getState();

        

        //session data

        $session = JFactory::getSession();

        $member_role = CrmeryHelperUsers::getRole();

        $user_id = CrmeryHelperUsers::getUserId();

        $team_id = CrmeryHelperUsers::getTeamId();

        $company = $session->get('company_type_filter');

        $category = $session->get("company_category_filter");

        $user = $session->get('company_user_filter');

        $team = $session->get('company_team_filter');

        

        //load java libs

        $doc = JFactory::getDocument();

        $doc->addScript( JURI::base().'components/com_crmery/media/js/company_manager.js' );

        

        //determine if we are requesting a specific company or all companies

        //if id requested

        if( JRequest::getVar('id') ) {


            $companies = $model->getCompanies(JRequest::getVar('id'));  
			
			//

            if ( is_null($companies[0]['id']) ){

                $app = JFactory::getApplication();

                $app->redirect(JRoute::_('index.php?option=com_crmery&view=companies'),CRMText::_('COM_CRMERY_NOT_AUTHORIZED'));

            }

        }else{

            //else load all companies

            if ( JRequest::getVar('layout') != 'edit' ){ 

                $companies = $model->getCompanies();
				
				//echo "<pre>"; print_r($companies); echo "</pre>";

            }

        }

        

        //get column filters

        $column_filters = CrmeryHelperCompany::getColumnFilters();

        $selected_columns = CrmeryHelperCompany::getSelectedColumnFilters();

        

        //get company type filters

        $company_types = CrmeryHelperCompany::getTypes();

        $company_type = ( $company ) ? $company_types[$company] : $company_types['all'];



        //get company category filters

        $company_categories = CrmeryHelperCompany::getCategories();

        $company_category = ( $category ) ? $company_categories[$category] : $company_categories['any'];

        

        //get user filter

        if ( $user AND $user != $user_id AND $user != 'all' ){

            $user_info = CrmeryHelperUsers::getUsers($user);

            $user_info = $user_info[0];

            $user_name = $user_info['first_name'] . " " . $user_info['last_name'];

        }else if ( $team ){

            $team_info = CrmeryHelperUsers::getTeams($team);

            $team_info = $team_info[0];

            $user_name = $team_info['team_name'].CRMText::_('COM_CRMERY_TEAM_APPEND');

        }else if ( $user == 'all' ) {

            $user_name = CRMText::_('COM_CRMERY_ALL_USERS');

        }else{

            $user_name = CRMText::_('COM_CRMERY_ME');            

        }



        //get associated members and teams

        $teams = CrmeryHelperUsers::getTeams();

        $users = CrmeryHelperUsers::getUsers();

        

        //get total associated companies for count display

        $company_count = CrmeryHelperUsers::getCompanyCount($user_id,$team_id,$member_role);



        //Load Events & Tasks for person

        $layout = $this->getLayout();

        if ( $layout == "company" ){

                $model = & JModel::getInstance('event','CrmeryModel');

                $events = $model->getEvents("company",null,JRequest::getVar('id'));



                $ref = array ( array('ref'=>'events','data'=>$events) );

                $eventDock = CrmeryHelperView::getView('events','event_dock',$ref);

                $this->assignRef('event_dock',$eventDock);



                $deal_dock = CrmeryHelperView::getView('deals','deal_dock',array(array('ref'=>'deals','data'=>$companies[0]['deals'])));

                $this->assignRef('deal_dock',$deal_dock);



                $document_list = CrmeryHelperView::getView('documents','document_row',array(array('ref'=>'documents','data'=>$companies[0]['documents'])));

                $this->assignRef('document_list',$document_list);



                $people_dock = CrmeryHelperView::getView('people','people_dock',array(array('ref'=>'people','data'=>$companies[0]['people'])));

                $this->assignRef('people_dock',$people_dock);



                $custom_fields_view = CrmeryHelperView::getView('custom','default');

                $type = "company";

                $custom_fields_view->assignRef('type',$type);

                $custom_fields_view->assignRef('item',$companies[0]);

                $this->assignRef('custom_fields_view',$custom_fields_view);



                $acymailing = CrmeryHelperConfig::checkAcymailing() && CrmeryHelperConfig::getConfigValue('acy_company') == 1;

                $this->assignRef('acymailing',$acymailing);

                if ( $acymailing ){

                    $mailing_list = new CrmeryHelperMailinglists();

                    $mailing_lists = $mailing_list->getMailingLists();

                    $newsletters = array();

                    if ( is_array($mailing_lists) && array_key_exists(0,$mailing_lists) ) { 

                        $newsletters = $mailing_list->getNewsletters($mailing_lists[0]->listid);

                    }

                    $acymailing_dock = CrmeryHelperView::getView('acymailing','default');

                    $acymailing_dock->assignRef('newsletters',$newsletters);

                    $acymailing_dock->assignRef('mailing_lists',$mailing_lists);

                    $this->assignRef('acymailing_dock',$acymailing_dock);

                }



                if ( CrmeryHelperBanter::hasBanter() ){

                    $room_list = new CrmeryHelperTranscriptlists();

                    $room_lists = $room_list->getRooms();

                    $transcripts = array();

                    if ( is_array($room_lists) && count($room_lists) > 0 ) { 

                        $transcripts = $room_list->getTranscripts($room_lists[0]->id);

                    }

                    $banter_dock = CrmeryHelperView::getView('banter','default');

                    $banter_dock->assignRef('rooms',$room_lists);

                    $banter_dock->assignRef('transcripts',$transcripts);

                    $this->assignRef('banter_dock',$banter_dock);

                }



                 /** get latest activities **/

                $activityHelper = new CRMeryHelperActivity;

                $activity = $activityHelper->getActivity();

                $latest_activities = CrmeryHelperView::getView('activities','default');

                $latest_activities_view = CrmeryHelperView::getView('activities','latest_activities');

                $latest_activities_view->assignRef('activity',$activity);

                $latest_activities->assignRef('latest_activities',$latest_activities_view);

                $this->assignRef('latest_activities',$latest_activities);

        }



        if ( $layout == "default" ){

            $company_list = CrmeryHelperView::getView('companies','list',array(array('ref'=>'companies','data'=>$companies)));

            $company_list->assignRef('show_filters',$model->show_filters);

            $company_list->assignRef('state',$state);

            $company_list->assignRef('app',$app);



            $company_list->assignRef('member_role',$member_role);

            $company_list->assignRef('user_name',$user_name);

            $company_list->assignRef('users',$users);

            $company_list->assignRef('teams',$teams);



            
			$total = $model->getTotal();

			
			$company_list->assignRef("total",$total);

            $company_list->assignRef("pagination",$pagination);

            $this->assignRef('company_list',$company_list);



            $company_name = $app->getUserState('Company.companies_name');

            $this->assignRef('company_filter',$company_name);



        }



        if ( $layout == "edit" ){

            if ( $session->get("Company.error") ){

                $item = $session->get("Company.editing");

                $item['id'] = '';

                $companies[0] = $item;

                $session->set("Company.error",false);

                $session->set("Company.error_message","");

                $session->set("Company.editing",null);

            }else{

                $item = JRequest::getVar('id') && array_key_exists(0,$companies) ? $companies[0] : array('id'=>'');

            }

            $edit_custom_fields_view = CrmeryHelperView::getView('custom','edit');

            $type = "company";

            $edit_custom_fields_view->assignRef('type',$type);

            $edit_custom_fields_view->assignRef('item',$item);

            $this->assignRef('edit_custom_fields_view',$edit_custom_fields_view);

            $doc->addScriptDeclaration("var loc='company_edit';");

            $companyId = ( isset($companies) && is_array($companies) && array_key_exists(0,$companies) ) ? $companies[0]['id'] : 0;

            $doc->addScriptDeclaration("var company_id=".$companyId.";");

        }



        if ( CrmeryHelperTemplate::isMobile() ){

            $add_note = CrmeryHelperView::getView('note','edit');

            $this->assignRef('add_note',$add_note);

        }

                    

        //ref assignments

        $this->assignRef('companies',$companies);

        $this->assignRef('user_id',$user_id);

        $this->assignRef('member_role',$member_role);

        $this->assignRef('teams',$teams);

        $this->assignRef('users',$users);

        $this->assignRef('company_types',$company_types);

        $this->assignRef('company_type',$company_type);

        $this->assignRef('company_categories',$company_categories);

        $this->assignRef("company_category",$company_category);

        $this->assignRef('user_name',$user_name);

        $this->assignRef('company_count',$company_count);

        $this->assignRef('state',$state);

        $this->assignRef('app',$app);

        $this->assignRef('column_filters',$column_filters);

        $this->assignRef('selected_columns',$selected_columns);

        $this->assignRef('show_filters',$model->show_filters);

        

        //display

        parent::display($tpl);

    }

    

}

?>

