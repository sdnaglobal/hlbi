<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewCompanies extends JView
{
    function display($tpl = null)
    {

        $app = JFactory::getApplication();
        $session = JFactory::getSession();
        $id = JRequest::getVar('id') ? JRequest::getVar('id') : null;

        //get model
        $model = & JModel::getInstance('company','CrmeryModel');
        $state = $model->getState();
        
        //get companies
        $companies = $model->getCompanies($id);
        $total = $model->getTotal();
        $pagination = $model->getPagination();
        $this->assignRef("pagination",$pagination);
        $this->assignRef("total",$total);

        //user information
        $member_role = CrmeryHelperUsers::getRole();
        $user_id = CrmeryHelperUsers::getUserId();

        $user = $session->get('company_user_filter');
        $team = $session->get('company_team_filter');

        if ( $user AND $user != $user_id AND $user != 'all' ){
            $user_info = CrmeryHelperUsers::getUsers($user);
            $user_info = $user_info[0];
            $user_name = $user_info['first_name'] . " " . $user_info['last_name'];
        }else if ( $team ){
            $team_info = CrmeryHelperUsers::getTeams($team);
            $team_info = $team_info[0];
            $user_name = $team_info['team_name'].CRMText::_('COM_CRMERY_TEAM_APPEND');
        }else if ( $user == 'all' ) {
            $user_name = CRMText::_('COM_CRMERY_ALL_USERS');
        }else{
            $user_name = CRMText::_('COM_CRMERY_ME');            
        }

        //get associated members and teams
        $teams = CrmeryHelperUsers::getTeams();
        $users = CrmeryHelperUsers::getUsers();
        
        //assign view refs
        $this->assignRef('companies',$companies);
        $this->assignRef('state',$state);
        $this->assignRef('show_filters',$model->show_filters);
        $this->assignRef('app',$app);
        $this->assignRef('member_role',$member_role);
        $this->assignRef('user_id',$user_id);
        $this->assignRef('user_name',$user_name);
        $this->assignRef('users',$users);
        $this->assignRef('teams',$teams);
        
        //display view
        parent::display($tpl);      
    }
    
}
        
        