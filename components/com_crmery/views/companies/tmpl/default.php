<?php

/*------------------------------------------------------------------------

# CRMery

# ------------------------------------------------------------------------

# @author CRMery

# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.

# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Website: http://www.crmery.com

-------------------------------------------------------------------------*/

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' ); ?>



<script type="text/javascript">

    var loc = 'companies';

    order_url = "<?php echo 'index.php?option=com_crmery&view=companies&layout=list&format=raw&tmpl=component'; ?>";

    order_dir = "<?php echo $this->state->get('Company.filter_order_Dir'); ?>";

    order_col = "<?php echo $this->state->get('Company.filter_order'); ?>";

</script> 

<ul class="entry_buttons">

     

	<?php



	$member_role = CrmeryHelperUsers::getRole(); if($member_role!="manager" && $member_role!="basic") {

	

	?>



    <li><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=companies&layout=edit'); ?>"><?php echo ucwords(CRMText::_('COM_CRMERY_ADD_COMPANY')); ?></a></li>

    <li style='display:none'><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=import&import_type=companies&layout=import'); ?>"><?php echo ucwords(CRMText::_('COM_CRMERY_IMPORT_COMPANIES')); ?></a></li>

	<?php }

	

	//if($member_role =="manager" || $member_role =="basic")

		//echo '<style>#edit_button{display:none !important;} </style>';

		

	?>

	

</ul>

<h1><?php echo ucwords(CRMText::_('COM_CRMERY_COMPANIES')); ?></h1>

<ul class="filter_lists" style="display:none;">

	<li class="filter_sentence">

        <?php echo CRMText::_('COM_CRMERY_SHOW'); ?> 

        <span class="filters"><a class="dropdown" id="company_type_link" href="javascript:void(0);"><?php echo $this->company_type; ?></a></span>

        <div class="filters" id="company_type">

            <ul>

                <?php foreach ( $this->company_types as $title => $text ){

                echo "<li><a class='filter_".$title."' onclick=\"companyType('".$title."')\">".$text."</a></li>";

                }?>

            </ul>

        </div> 

         <?php echo CRMText::_('COM_CRMERY_OWNED_BY'); ?>

        <span class="filters"><a class="dropdown" id="company_user_sentence_link"><?php echo $this->user_name; ?></a></span>

        <div class="filters" id="company_user_sentence">

            <ul>

            <li><a class="filter_user_<?php echo $this->user_id; ?>" onclick="companyUser(<?php echo $this->user_id; ?>,0)"><?php echo CRMText::_('COM_CRMERY_ME'); ?></a></li>

            <li><a class="filter_user_all" onclick="companyUser('all',0)"><?php echo CRMText::_('COM_CRMERY_ALL_USERS'); ?></a></li>

            <?php  

                if ( $this->member_role == 'exec' ){

                    if ( count($this->teams) > 0 ){

                        foreach($this->teams as $team){

                             echo "<li><a class='filter_team_".$team['team_id']."' onclick='companyUser(0,".$team['team_id'].")'>".$team['team_name'].CRMText::_('COM_CRMERY_TEAM_APPEND')."</a></li>";

                         }

                    }

                }            

                if ( count($this->users) > 0 ){

                    foreach($this->users as $user){

                        echo "<li><a class='filter_user_".$user['id']."' onclick='companyUser(".$user['id'].",0)'>".$user['first_name']."  ".$user['last_name']."</a></li>";

                    }

                }

            ?>

        </ul>

        </div>

        <?php echo CRMText::_('COM_CRMERY_IN_CATEGORY'); ?> 

        <span class="filters"><a class="dropdown" id="company_category_link" href="javascript:void(0);"><?php echo $this->company_category; ?></a></span>

        <div class="filters" id="company_category">

            <ul>

                <?php foreach ( $this->company_categories as $id => $text ){

                echo "<li><a class='filter_".$id."' onclick=\"companyCategory('".$id."')\">".$text."</a></li>";

                }?>

            </ul>

        </div> 

        <?php echo CRMText::_('COM_CRMERY_NAMED'); ?>

    <td><input class="inputbox filter_input" name="company_name" type="text" placeholder="<?php echo CRMText::_('COM_CRMERY_ANYTHING'); ?>" value="<?php echo $this->company_filter; ?>"></td>

    </li>

    <li class="filter_sentence">

        <div class="ajax_loader"></div>

    </li>

</ul>

<div class="subline">

<ul class="matched_results">

    <li><span id="companies_matched"></span> <?php echo CRMText::_('COM_CRMERY_COMPANIES_MATCHED'); ?> <?php //echo CRMText::_('COM_CRMERY_THERE_ARE'); ?> <?php //echo $this->company_count; ?> <?php //echo CRMText::_('COM_CRMERY_COMPANIES_IN_ACCOUNT'); ?></li>

</ul>

<span class="actions">

    <span class="filters"><a href="javascript:void(0);" class="dropdown" id="column_filter_link"><?php echo CRMText::_('COM_CRMERY_SELECT_COLUMNS'); ?></a></span>

        <div class="filters" id="column_filter">

            <ul>

                <?php foreach ( $this->column_filters as $key => $text ){ ?>

                    <?php $selected = ( in_array($key,$this->selected_columns) ) ? 'checked' : ''; ?>

                    <li><input type="checkbox" class="column_filter" id="show_<?php echo $key; ?>" <?php echo $selected; ?> > <?php echo $text; ?></li>    

                <?php } ?> 

            </ul>

        </div>

        <?php if ( CrmeryHelperUsers::canExport() ){ ?>

            - <a href="javascript:void(0)" onclick="exportCsv()"><?php echo CRMText::_('COM_CRMERY_EXPORT_CSV'); ?></a>

        <?php } ?>

        <?php $filterText = $this->show_filters ? CRMText::_('COM_CRMERY_RESET_FILTERS') : CRMText::_('COM_CRMERY_SHOW_FILTERS'); ?>

        - <a href="javascript:void(0)" id="filter-link" onclick="toggleFilters()"><?php echo $filterText; ?></a>

</span>

</div>

<?php echo CrmeryHelperTemplate::getListEditActions(); ?>

<form method="post" id="list_form" action="<?php echo JRoute::_('index.php?option=com_crmery&view=companies'); ?>" >

<table class="com_crmery_table" id="companies">

		  <?php echo $this->company_list->display(); ?>

</table>

<input type="hidden" name="list_type" value="companies" />

</form>

<div id="templates" style="display:none;">

    <div id="note_modal" style="display:none;"></div>

    <div id="edit_button"><a class="edit_button_link" id="edit_button_link" href="javascript:void(0)"></a></div>

    <div id="edit_list_modal" style="display:none;" ></div>

</div>