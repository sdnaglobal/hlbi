<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

//determine if specific company is being edited or new company is being added

if ( count($this->companies) == 1 ){
	$company = $this->companies[0];
	$header = ucwords(CRMText::_('COM_CRMERY_COMPANY_EDIT')) . $company['name'];
}else{
	$company['category_id'] = null;
	$company['region_id'] = null;
	$company['country_id'] = null;
	$company['state_id'] = null;
	$company['partner_id'] = null;
	$company['id'] = -1;
	$company['name'] = "";
	$company['phone'] = "";
	$company['website'] = '';
	$company['description'] = '';
	$header = ucwords(CRMText::_('COM_CRMERY_COMPANY_ADD'));
}


?>

<?php $format = JRequest::getVar('format'); ?>
<?php if ( $format != "raw" ){ ?>
<h1><?php echo $header; ?></h1>
<?php } ?>
<form id="edit_form" method="POST" action="<?php echo 'index.php?option=com_crmery&controller=companies&task=save'; ?>" onsubmit="return save(this)">
	<?php
		if ( $company['id'] != -1 ) {
			echo '<input type="hidden" name="id" value="'.$company['id'].'" />';
		}
	?>
	<div id="editForm">
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_NAME')); ?><span class="required">*</span></div>
			<div class="crmeryValue">
				<input type="text" onblur="checkCompanyName(this);" class="inputbox required" name="name" id="company_name" placeholder="<?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_NAME_NULL')); ?>" value="<?php if(array_key_exists('name',$company)) echo $company['name']; ?>" />
				<input type="hidden" name="company_id" id="company_id" value="" />
				<div id="company_message"></div>
			</div>
		</div>
		
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_COMPANY_DESCRIPTION'); ?></div>
			<div class="crmeryValue">
				<textarea class="inputbox" name="description"><?php if(array_key_exists('description',$company)) echo $company['description']; ?></textarea>
			</div>
		</div>
		
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_EMAIL')); ?></div>
			<div class="crmeryValue"><input class="inputbox" type="text" name="email" placeholder="<?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_EMAIL_NULL')); ?>" value="<?php if(array_key_exists('email',$company)) echo $company['email']; ?>"/></div>
		</div>			
		
		 <div class="crmeryRow">
				<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_PARTNER_LIST')); ?></div>
				<div class="crmeryValue">
					<?php //$ownerId = array_key_exists('owner_id',$company) ? $company['owner_id'] : CrmeryHelperUsers::getUserId(); ?>
					<?php echo CrmeryHelperCompany::getFirmAdminDropdown($company['partner_id']); ?>
				</div>
			</div>	
			
			<div class="crmeryRow">
			<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_CATEGORY')); ?></div>
			<div class="crmeryValue">
				<?php echo CrmeryHelperCompany::getCategoryDropdown($company['category_id']); ?>
			</div>
		</div>
			
			
		
		<div class="crmeryRow" style="display:none;"id="address_button">
			<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_ADDRESS')); ?></div>
			<div class="crmeryValue address"><a href="javascript:void(0)" onclick="bind_area('address');" ><?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_ADDRESS_MESSAGE')); ?></a></div>	
		</div>		
		
		<div style="display:block;" id="address_info" >
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_ADDRESS')); ?></div>
				<div class="crmeryValue">
				<input class="inputbox address_city" type="text" placeholder="<?php echo CRMText::_('COM_CRMERY_CITY_NULL'); ?>" name="address_city" value="<?php if(array_key_exists('address_city',$company)) echo $company['address_city']; ?>" />
				<input class="inputbox address_zip" type="text" placeholder="<?php echo CRMText::_('COM_CRMERY_ZIP_NULL'); ?>" name="address_zip" value="<?php if(array_key_exists('address_zip',$company)) echo $company['address_zip']; ?>" />
				<!--
					<input class="inputbox address_one" type="text" placeholder="<?php echo CRMText::_('COM_CRMERY_ADDRESS_1_NULL'); ?>" name="address_1" value="<?php if(array_key_exists('address_1',$company)) echo $company['address_1']; ?>" />
					<br />
					<input class="inputbox address_two" type="text" placeholder="<?php echo CRMText::_('COM_CRMERY_ADDRESS_2_NULL'); ?>" name="address_2" value="<?php if(array_key_exists('address_2',$company)) echo $company['address_2']; ?>" /> -->
					<!--<br />
					<input class="inputbox address_city" type="text" placeholder="<?php echo CRMText::_('COM_CRMERY_CITY_NULL'); ?>" name="address_city" value="<?php if(array_key_exists('address_city',$company)) echo $company['address_city']; ?>" />
					<input class="inputbox address_state" type="text" placeholder="<?php echo CRMText::_('COM_CRMERY_STATE_NULL'); ?>" name="address_state" value="<?php if(array_key_exists('address_state',$company)) echo $company['address_state']; ?>" />
					<input class="inputbox address_zip" type="text" placeholder="<?php echo CRMText::_('COM_CRMERY_ZIP_NULL'); ?>" name="address_zip" value="<?php if(array_key_exists('address_zip',$company)) echo $company['address_zip']; ?>" />-->
					<!--<br />
					<input class="inputbox address_country" type="text" placeholder="<?php echo CRMText::_('COM_CRMERY_COUNTRY_NULL'); ?>" name="address_country" value="<?php if(array_key_exists('address_country',$company)) echo $company['address_country']; ?>" />-->
				
				<br />	
               					
				
				<div class="crmeryValue">
					<?php //$regionId = array_key_exists('region_id',$company) ? $company['region_id'] : CrmeryHelperUsers::getUserId(); ?>
					<?php echo CrmeryHelperCompany::getRegionDropdown($company['region_id']); ?>
				</div>	
				
				<div class="crmeryValue">
					<?php //$ownerId = array_key_exists('owner_id',$company) ? $company['owner_id'] : CrmeryHelperUsers::getUserId(); ?>
					<?php //echo CrmeryHelperCompany::getCountryDropdown($company['country_id']);  ?>					<div id="country" style="display:block;">
		
		<?php if ( $company['id'] != -1 ) {  
		
		    $ownerId =  CrmeryHelperUsers::getUserId(); 
			$db =& JFactory::getDbo();
			$query = $db->getQuery(true);
			
			$query->select("country_id")
				->from("#__crmery_users");
				$query->where('id='.$ownerId);
			$db->setQuery($query);
			$countryIds = $db->loadObjectList();		
			
			if ( count($countryIds) > 0 ){
				foreach ( $countryIds as $countryId ){
				   $countryId= $countryId->country_id;
				}
			} 
		
		             $region_id= $company['region_id'];
		   
		   $db = & JFactory::getDBO();	   
		   
		   $query = $db->getQuery(true);
			
		   $query->select('* FROM #__crmery_currencies');
           $query->where('region_id='.$region_id.' and country_id IN ('.$countryId.')');			  
			
			$results = $db->setQuery($query);
			$results = $db->loadAssocList();			
			
			echo "<select name='country_id' id='country_id' class='inputbox' onChange='getState(this.value)'>";
			echo "<option value=''>Select Country</option>";
			
			foreach($results as $value)
			{		
				if($company['country_id'] == $value['country_id'])
					echo "<option selected value='".$value['country_id']."'>".$value['country_name']."</option>";
				else 	
					echo "<option value='".$value['country_id']."'>".$value['country_name']."</option>";
			}
			echo "</select>"; 	
		
				
					
					} 
					
					?>
			</div> 
					
					<div id="nocountry" style="display:none;"><select class="inputbox"><option>Select Country</option></select></div> 
					
				</div>
				
				<div class="crmeryValue">
					<?php //$ownerId = array_key_exists('owner_id',$company) ? $company['owner_id'] : CrmeryHelperUsers::getUserId(); ?>
					<?php //echo CrmeryHelperCompany::getStateDropdown($company['state_id']); ?>
				
<input class="inputbox address_state" id="statetextbox" type="text" placeholder="<?php echo CRMText::_('COM_CRMERY_STATE_NULL'); ?>" name="address_state" value="<?php if(array_key_exists('address_state',$company)) echo $company['address_state']; ?>" style="display:block;" />
				
				 	<div id="nostate" style="display:block;"><?php // echo CrmeryHelperCompany::getStateDropdown($company['state_id']); ?></div>
					<div id="state" style="display:none;"></div> 
					
				</div>
		    <br />
					<!-- <input class="inputbox address_city" type="text" placeholder="<?php echo CRMText::_('COM_CRMERY_CITY_NULL'); ?>" name="address_city" value="<?php if(array_key_exists('address_city',$company)) echo $company['address_city']; ?>" />
				<input class="inputbox address_zip" type="text" placeholder="<?php echo CRMText::_('COM_CRMERY_ZIP_NULL'); ?>" name="address_zip" value="<?php if(array_key_exists('address_zip',$company)) echo $company['address_zip']; ?>" /> -->
				
				</div>			
				
				
			</div>
		</div>
		
		
		
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_PHONE')); ?></div>
			<div class="crmeryValue"><input class="inputbox" type="text" name="phone" placeholder="<?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_PHONE_NULL')); ?>" value="<?php if(array_key_exists('phone',$company)) echo $company['phone']; ?>"/></div>
		</div>		
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_FAX')); ?></div>
			<div class="crmeryValue"><input class="inputbox" type="text" name="fax" placeholder="<?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_FAX_NULL')); ?>" value="<?php if(array_key_exists('fax',$company)) echo $company['fax']; ?>"/></div>
		</div>
		
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_COMPANY_WEB'); ?></div>
			<div class="crmeryValue"><input class="inputbox" type="text" placeholder="<?php echo CRMText::_('COM_CRMERY_WEBSITE_NULL'); ?>" name="website" value="<?php if(array_key_exists('website',$company)) echo $company['website']; ?>" /></div>
		</div>
		
	<!--	<div class="crmeryRow" id="other_button">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_FACEBOOK_URL'); ?></div>
			<div class="crmeryValue"><input class="inputbox" type="text" name="facebook_url" value="<?php if(array_key_exists('facebook_url',$company)) echo $company['facebook_url']; ?>" /></div>
		</div>
		<div class="crmeryRow" id="other_button">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_TWITTER_USER'); ?></div>
			<div class="crmeryValue"><input class="inputbox" data-minlength="4" type="text" name="twitter_user" value="<?php if(array_key_exists('twitter_user',$company)) echo $company['twitter_user']; ?>" /></div>
		</div>
		<div class="crmeryRow" id="other_button">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_FLICKR_URL'); ?></div>
			<div class="crmeryValue"><input class="inputbox" type="text" name="flickr_url" value="<?php if(array_key_exists('flickr_url',$company)) echo $company['flickr_url']; ?>" /></div>
		</div>
		<div class="crmeryRow" id="other_button">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_YOUTUBE_URL'); ?></div>
			<div class="crmeryValue"><input class="inputbox" type="text" name="youtube_url" value="<?php if(array_key_exists('youtube_url',$company)) echo $company['youtube_url']; ?>" /></div>
		</div>
		-->
		<input type='hidden' name='owner_id' id='owner_id' value='5<?php //echo ;?>'>
		<?php /*if ( CrmeryHelperUsers::getRole() == 'exec' || CrmeryHelperUsers::getRole() == "manager" || !($company['id']>0) || ( array_key_exists('owner_id',$company) && CrmeryHelperUsers::getUserId() == $company['owner_id'] ) || CrmeryHelperUsers::isAdmin() ){ ?>
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_OWNER')); ?></div>
				<div class="crmeryValue">
					<?php $ownerId = array_key_exists('owner_id',$company) ? $company['owner_id'] : CrmeryHelperUsers::getUserId(); ?>
					<?php ///echo CrmeryHelperDropdown::getOwnerDropdown($ownerId); 
					 
					        echo CrmeryHelperCompany::getFirmOwnerDropdown($company['owner_id']);
					
					?>
				</div>
			</div>
		<?php }*/ ?>
		
		      		
		
		
		<?php if ( $format != "raw" ) { ?>
		<?php echo $this->edit_custom_fields_view->display(); ?>
			<span class="actions"><input class="button" type="submit" value="<?php echo CRMText::_('COM_CRMERY_SAVE_BUTTON'); ?>"><a href="javascript:void(0);" onclick="window.history.back()"><?php echo CRMText::_('COM_CRMERY_CANCEL_BUTTON'); ?></a></span>
		<?php } else { ?>
			<div class="actions"><a href="javascript:void(0);" onclick="saveListItem(<?php echo JRequest::getVar('id'); ?>);" class="button"><?php echo CRMText::_('COM_CRMERY_SAVE_BUTTON'); ?></a><a href="javascript:void(0);" onclick="window.top.window.jQuery('.ui-dialog-content').dialog('close');"><?php echo CRMText::_('COM_CRMERY_CANCEL_BUTTON'); ?></a></div>
		<?php } ?>
			
		

	</div>
</form>