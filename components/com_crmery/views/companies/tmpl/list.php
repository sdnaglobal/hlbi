<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );?>
<?php if ( !(JRequest::getVar('id')) ){ ?>
<thead>
    <th class="checkbox_column"><input type="checkbox" onclick="selectAll(this);" /></th>
    <th class="avatar" ></th>
    <th class="name" ><div class="sort_order"><a class="c.name" onclick="sortTable('c.name',this)"><?php echo CRMText::_('COM_CRMERY_COMPANIES_NAME'); ?></a></div></th>
    <th class="owner" ><div class="sort_order"><a class="c.owner_id" onclick="sortTable('c.owner_id',this)"><?php echo CRMText::_('COM_CRMERY_COMPANIES_OWNER'); ?></a></div></th>
    <th class="category" ><div class="sort_order"><a class="c.category_id" onclick="sortTable('c.category_id',this)"><?php echo CRMText::_('COM_CRMERY_COMPANIES_CATEGORY'); ?></a></div></th>
    <th class="phone" ><div class="sort_order"><a class="c.phone" onclick="sortTable('c.phone',this)"><?php echo CRMText::_('COM_CRMERY_COMPANIES_PHONE'); ?></a></div></th>
    <th class="fax" ><?php echo CRMText::_('COM_CRMERY_COMPANIES_FAX'); ?></th>
    <th class="email" ><?php echo CRMText::_('COM_CRMERY_COMPANIES_EMAIL'); ?></th>
    <th class="address" ><?php echo CRMText::_('COM_CRMERY_COMPANIES_ADDRESS'); ?></th>
    <th class="country"><?php echo CRMText::_('COM_CRMERY_COMPANIES_COUNTRY'); ?></th>
    <th class="next_task" ><?php echo CRMText::_('COM_CRMERY_COMPANIES_TASK'); ?></th>
    <th class="next_task" ><?php echo CRMText::_('COM_CRMERY_COMPANIES_DUE_DATE'); ?></th>
    <th class="description"><?php echo CRMText::_('COM_CRMERY_COMPANIES_DESCRIPTION'); ?></th>
    <th class="notes" ><?php echo CRMText::_('COM_CRMERY_COMPANIES_NOTES'); ?></th>
    <th class="added" ><div class="sort_order"><a class="c.created" onclick="sortTable('c.created',this)"><?php echo CRMText::_('COM_CRMERY_COMPANIES_ADDED'); ?></a></div></th>
    <th class="updated" ><div class="sort_order"><a class="c.modified" onclick="sortTable('c.modified',this)"><?php echo CRMText::_('COM_CRMERY_COMPANIES_UPDATED'); ?></a></div></th>
    <th ><?php echo CRMText::_('COM_CRMERY_COMPANIES_FIRM_BILLING'); ?> </th> 
</thead>
<tbody>
<?php $style = $this->show_filters ? "" : "style='display:none;'"; ?>
<tr <?php echo $style; ?> id="list-filters">
    <td class="checkbox_column"></td>
    <td class="avatar"></td>
    <td class="name"><input class="input input-small filter_input" name="company_name" type="text" value="<?php echo $this->app->getUserState("Company.companies_name"); ?>"/></td>
    <td class="owner"></td>
    <td class="category"></td>
    <td class="phone"><input class="input input-small filter_input" name="company_phone" type="text" value="<?php echo $this->app->getUserState("Company.companies_phone"); ?>" /></td>
    <td class="fax"><input class="input input-small filter_input" type="text" name="fax" type="text" value="<?php echo $this->app->getUserState("Company.companies_fax"); ?>" /></td>
    <td class="email"><input class="input input-small filter_input" type="text" name="email" type="text" value="<?php echo $this->app->getUserState("Company.companies_email"); ?>" /></td>
    <td class="address"><input class="input input-small filter_input" type="text" name="address" type="text" value="<?php echo $this->app->getUserState("Company.companies_address"); ?>" /></td>
    <td class="country"><input class="input input-small filter_input" type="text" name="country" type="text" value="<?php echo $this->app->getUserState("Company.companies_country"); ?>" /></td>
    <td class="next_task"></td>
    <td class="next_task"></td>
    <td class="description"><input class="input input-small filter_input" type="text" name="description" type="text" value="<?php echo $this->app->getUserState("Company.companies_description"); ?>" /></td>
    <td class="notes"></td>
    <td class="added"></td>
    <td class="updated"></td>
</tr>
<?php } ?>
<?php
    $n = count($this->companies);
    for ( $i=0; $i<$n; $i++ ) {
        $company = $this->companies[$i];
		
		$c_id = $company['country_id'];
	$firm_cntry_name = '';	
	$db =& JFactory::getDBO();
	$cntryname = $db->getQuery(true);
	$cntryname->select('*');
	$cntryname->from('#__crmery_currencies');
	$cntryname->where("country_id='$c_id'");
	$db->setQuery($cntryname);
	$cntryname_rs_name = $db->loadObjectList();
	$firm_cntry_name = $cntryname_rs_name[0]->country_name;
	
	
        $k = $i%2;
        echo "<tr id='list_row_".$company['id']."' class='crmery_row_".$k."'>";
            echo '<td><input type="checkbox" name="ids[]" value="'.$company['id'].'" /></td>';
            if ( array_key_exists('avatar',$company) && $company['avatar'] != "" && $company['avatar'] != null ){
                 echo '<td class="avatar" ><img id="avatar_img_'.$company['id'].'" data-item-type="companies" data-item-id="'.$company['id'].'" class="avatar" src="'.JURI::base().'components/com_crmery/media/avatars/'.$company['avatar'].'"/></td>';
            }else{
                echo '<td class="avatar" ><img id="avatar_img_'.$company['id'].'" data-item-type="companies" data-item-id="'.$company['id'].'" class="avatar" src="'.JURI::base().'components/com_crmery/media/images/company.png'.'"/></td>';
            }
            echo '<td class="list_edit_button" id="list_'.$company['id'].'" ><div class="title_holder"><a href="'.JRoute::_('index.php?option=com_crmery&view=companies&layout=company&id='.$company['id']).'">'.$company['name'].'</a></div></td>';
            echo '<td class="owner">'.$company['owner_first_name'].' '.$company['owner_last_name'].'</td>';
            echo '<td class="category">'.$company['category_name']."</td>";
            echo '<td class="phone" >'.$company['phone'].'</td>';
            echo '<td class="fax" >'.$company['fax'].'</td>';
            echo '<td class="email" >'.$company['email'].'</td>';
            echo '<td class="address" >'.$company['address_formatted'].'</td>';
           // echo '<td class="country" >'.$company['address_country'].'</td>'; 
            echo '<td class="country" >'.$firm_cntry_name.'</td>'; 
            echo '<td class="next_task" ><a onclick="editEvent('.$company['event_id'].',\''.$company['event_type'].'\')">'.$company['event_name'].'</a></td>';
            echo '<td class="next_task" >'.CrmeryHelperDate::formatDate($company['event_due_date']).'</td>';
            echo '<td class="description" >'.$company['description'].'</td>';
            echo '<td class="notes" ><a href="javascript:void(0);" onclick="openNoteModal(\''.$company['id'].'\',\'company\');"><img src="'.JURI::base().'components/com_crmery/media/images/notes.png'.'"/></a></td>';
            echo '<td class="added" >'.CrmeryHelperDate::formatDate($company['created']).'</td>';
            echo '<td class="updated" >'.CrmeryHelperDate::formatDate($company['modified']).'</td>';
            echo '<td ><a href="'.JRoute::_('index.php?option=com_crmery&view=firmbilling&layout=companies&id='.$company['id']).'"> Add Fee Income </a></td>';
        echo '</tr>';
    }
?>
<?php if ( !(JRequest::getVar('id')) ){ ?>
</tbody>
<tfoot>
    <tr>
       <td colspan="20"><?php echo $this->pagination->getListFooter(); ?></td>
    </tr>
 </tfoot>
<script type="text/javascript">
    //update company count
    jQuery("#companies_matched").empty().html("<?php echo $this->total; ?>");
    window.top.window.assignFilterOrder();
</script>
<?php } ?>