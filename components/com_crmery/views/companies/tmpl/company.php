<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

//define company
$company = $this->companies[0];

$member_role = CrmeryHelperUsers::getRole(); 

?>

<script type="text/javascript">
	var loc = "company";
	var id = <?php echo $company['id']; ?>;
	var company_id = <?php echo $company['id']; ?>;
	var association_type = 'company';
</script>

<iframe id="hidden" name="hidden" style="display:none;width:0px;height:0px;border:0px;"></iframe>

<div class="rightColumn">
	<div class="infoContainer" id="details">
		<h2><?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_DETAILS')); ?></h2>
		<div class="infoBlock">
			<div class="infoLabel">
				<?php if ( array_key_exists('avatar',$company) && $company['avatar'] != "" && $company['avatar'] != null ){
                         echo '<td class="avatar" ><img id="avatar_img_'.$company['id'].'" data-item-type="companies" data-item-id="'.$company['id'].'" class="avatar" src="'.JURI::base().'components/com_crmery/media/avatars/'.$company['avatar'].'"/></td>';
                    }else{
                        echo '<td class="avatar" ><img id="avatar_img_'.$company['id'].'" data-item-type="companies" data-item-id="'.$company['id'].'" class="avatar" src="'.JURI::base().'components/com_crmery/media/images/company.png'.'"/></td>';
                    } ?>
			</div>
			<div class="infoDetails">
				<?php if ( array_key_exists('address_1',$company) && $company['address_1'] != "" ) { ?>
					<?php $urlString = "http://maps.googleapis.com/maps/api/staticmap?&zoom=13&zoom=2&size=600x400&sensor=false&center=".str_replace(" ","+",$company['address_1'].' '.$company['address_2'].' '.$company['address_city'].' '.$company['address_state'].' '.$company['address_zip'].' '.$company['address_country']); ?>
					<a href="javascript:void(0);" class="google-map" id="work_address"></a>
					<div id="work_address_modal" style="display:none;">
						<div class="google_map_center"></div>
						<img class="google-image-modal"  style="background-image:url(<?php echo $urlString; ?>);" />
					</div>
					<?php echo $company['address_1']; ?><br />
					<?php if ( array_key_exists('address_2',$company) && $company['address_2'] != "" ){ 
						echo $company['address_2'].'<br />';
					} ?>
					<?php echo $company['address_city'].', '.$company['address_state']." ".$company['address_zip']; ?><br />
					<?php echo $company['address_country']; ?>
				<?php } ?>
			</div>
		</div>
		<?php if ( array_key_exists('category_id',$company) && $company['category_id'] > 0 ) { ?>
			<div class="infoBlock">
				<div class="infoLabel"><?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_CATEGORY')); ?></div>
				<div class="infoDetails">
					<?php echo $company['category_name']; ?>
				</div>
			</div>
		<?php } ?>
		<?php if ( array_key_exists('website',$company) && !is_null($company['website']) && $company['website'] != "" ) { ?>
			<div class="infoBlock">
				<div class="infoLabel"></div>
				<div class="infoDetails">
					<a target="_blank" href="<?php echo $company['website']; ?>"><?php echo $company['website']; ?></a>
				</div>
			</div>
		<?php } ?>	
		<?php if ( array_key_exists('phone',$company) ) { ?>
			<div class="infoBlock">
				<div class="infoLabel"><?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_PHONE')); ?></div>
				<div class="infoDetails">
					<?php echo $company['phone']; ?>
				</div>
			</div>
		<?php } ?>
		<?php if ( array_key_exists('fax',$company) ) { ?>
			<div class="infoBlock">
				<div class="infoLabel"><?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_FAX')); ?></div>
				<div class="infoDetails">
					<?php echo $company['fax']; ?>
				</div>
			</div>
		<?php } ?>	
		<?php if ( array_key_exists('email',$company) ) { ?>
			<div class="infoBlock">
				<div class="infoLabel"><?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_EMAIL')); ?></div>
				<div class="infoDetails">
					<a target='_blank' href="mailto:<?php echo $company['email']; ?>"><?php echo $company['email']; ?></a>
				</div>
			</div>
		<?php } ?>		
		<?php if ( array_key_exists('description',$company) ) { ?>
			<div class="infoBlock">
				<div class="infoLabel"></div>
				<div class="infoDetails">
					<?php echo nl2br($company['description']); ?>
				</div>
			</div>
		<?php } ?>	

		<div class="infoBlock">
			<div class="infoLabel">&nbsp;</div>
			<div class="infoDetails">

					

					<?php if(array_key_exists('facebook_url',$company) && $company['facebook_url'] != ""){ ?>
						<a href="<?php echo $company['facebook_url']; ?>" target="_blank"><div class="facebook_light"></div></a>
					<?php } else { ?>
					<span class="editable parent" id="editable_facebook_container_<?php echo $company['id']; ?>">
					<div class="inline">
						<a href="javascript:void(0);"><div class="facebook_dark"></div></a>
					</div>
					<div class="filters editable_info">
						<form id="facebook_form_<?php echo $company['id']; ?>">
							<input type="hidden" name="item_id" value="<?php echo $company['id']; ?>" />
							<input type="hidden" name="item_type" value="company" />
							<input type="text" class="inputbox" name="facebook_url" value="<?php if ( array_key_exists('facebook_url',$company) ) echo $company['facebook_url']; ?>" />
							<input type="button" class="button" onclick="saveEditableModal('facebook_form_<?php echo $company['id']; ?>');" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" />
						</form>
					</div>
					</span>
					<?php } ?>

					<?php if(array_key_exists('twitter_user',$company) && $company['twitter_user'] != ""){ ?>
						<a href="http://www.twitter.com/#!/<?php echo $company['twitter_user']; ?>" target="_blank"><div class="twitter_light"></div></a>
					<?php } else { ?>
					<span class="editable parent" id="editable_twitter_container_<?php echo $company['id']; ?>">
					<div class="inline">
						<a href="javascript:void(0);"><div class="twitter_dark"></div></a>
					</div>
					<div class="filters editable_info">
						<form id="twitter_form_<?php echo $company['id']; ?>">
							<input type="hidden" name="item_id" value="<?php echo $company['id']; ?>" />
							<input type="hidden" name="item_type" value="company" />
							<input type="text" class="inputbox" name="twitter_user" value="<?php if ( array_key_exists('twitter_user',$company) ) echo $company['twitter_user']; ?>" />
							<input type="button" class="button" onclick="saveEditableModal('twitter_form_<?php echo $company['id']; ?>');" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" />
						</form>
					</div>
					</span>
					<?php } ?>

					<span class="editable parent" id="editable_youtube_container_<?php echo $company['id']; ?>">
					<div class="inline">
						<?php if(array_key_exists('youtube_url',$company) && $company['youtube_url'] != "" ){ ?>
							<a href="<?php echo $company['youtube_url']; ?>" target="_blank"><div class="youtube_light"></div></a>
						<?php } else { ?>
							<a href="javascript:void(0);"><div id="youtube_button_<?php echo $company['id']; ?>" class="youtube_dark"></div></a>
						<?php } ?>
					</div>
					<div class="filters editable_info">
						<form id="youtube_form_<?php echo $company['id']; ?>">
							<input type="hidden" name="item_id" value="<?php echo $company['id']; ?>" />
							<input type="hidden" name="item_type" value="company" />
							<input type="text" class="inputbox" name="youtube_url" value="<?php if ( array_key_exists('youtube_url',$company) )  echo $company['youtube_url']; ?>" />
							<input type="button" class="button" onclick="saveEditableModal('youtube_form_<?php echo $company['id']; ?>');" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" />
						</form>
					</div>
					</span>

					<?php if(array_key_exists('flickr_url',$company) && $company['flickr_url'] != "" ){ ?>
						<a href="<?php echo $company['flickr_url']; ?>" target="_blank"><div class="flickr_light"></div></a>
					<?php } else { ?>
					<span class="editable parent" id="editable_flickr_container_<?php echo $company['id']; ?>">
					<div class="inline">
						<a href="javascript:void(0);"><div class="flickr_dark"></div></a>
					</div>
					<div class="filters editable_info">
						<form id="flickr_form_<?php echo $company['id']; ?>">
							<input type="hidden" name="item_id" value="<?php echo $company['id']; ?>" />
							<input type="hidden" name="item_type" value="company" />
							<input type="text" class="inputbox" name="flickr_url" value="<?php if ( array_key_exists('flickr_url',$company) ) echo $company['flickr_url']; ?>" />
							<input type="button" class="button" onclick="saveEditableModal('flickr_form_<?php echo $company['id']; ?>');" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" />
						</form>
					</div>
					</span>
					<?php } ?>

			</div>
		</div>

		

	</div>

	<?php if($company['twitter_user']) { ?>
		<div class="infoContainer">
			<h2><?php echo CRMText::_('COM_CRMERY_LATEST_TWEETS'); ?></h2>
			<?php if ( array_key_exists('tweets',$company) ){ for($i=0; $i<count($company['tweets']); $i++) { 
				$tweet = $company['tweets'][$i];
			?>
			<div class="tweet">
				<span class="tweet_date"><?php echo $tweet['date']; ?></span>
				<?php echo $tweet['tweet']; ?>
			</div>
			<?php } } ?>
		</div>
	<?php } ?>

	<?php if ( $this->acymailing ){ ?>
		<div class="infoContainer" id='acymailing'>
			<h2><?php echo ucwords(CRMText::_('COM_CRMERY_ACYMAILING_HEADER')); ?></h2>
			<?php $this->acymailing_dock->display(); ?>
		</div>
	<?php } ?>

	<?php if ( isset($this->banter_dock) ){ 
		$this->banter_dock->display();
	}?>
	<div class="infoContainer" id='event_dock'>
		<h2><?php echo ucwords(CRMText::_('COM_CRMERY_TASKS_AND_EVENTS')); ?></h2>
		<?php $this->event_dock->display(); ?>
	</div>
</div>	

<div class="leftColumn">

<?php if($member_role!="basic" ) { ?>
<ul class="entry_buttons">
	<li><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=companies&layout=edit&id='.$company['id']); ?>" class=""><?php echo CRMText::_('COM_CRMERY_EDIT_BUTTON'); ?></a></li>
</ul>
<?php }  // IS?>


<div class="actions_container">
<span class="actions textMiddle">
    <a href="javascript:void(0);" id="actions_button" class="dropdown_arrow"><?php echo CRMText::_('COM_CRMERY_ACTION_BUTTON'); ?></a>
    <div id="actions">
    	
	    	<ul>
	    		<?php if ( $company['owner_id'] == CrmeryHelperUsers::getUserId() ){ ?>
					<li><a id="company_privacy" href="javascript:void(0)" onclick="toggleCompanyPrivacy();"><?php if($company['public']==0) echo CRMText::_('COM_CRMERY_MAKE_PUBLIC'); if($company['public']==1) echo CRMText::_('COM_CRMERY_MAKE_PRIVATE'); ?></a></li>
					<li><a onclick="shareItemDialog();" ><?php echo CRMText::_('COM_CRMERY_SHARE'); ?></a></li>
				<?php } ?>
				<form id="delete_form" method="POST" action="<?php echo JRoute::_('index.php?option=com_crmery&controller=main&task=trash'); ?>" >
				<input type="hidden" name="item_id" value="<?php echo $company['id']; ?>" />
				<input type="hidden" name="item_type" value="companies" />
				<input type="hidden" name="page_redirect" value="companies" />
				<?php if ( CrmeryHelperUsers::isAdmin() ) { ?>
				<li><a onclick="deleteItem(this)"><?php echo CRMText::_('COM_CRMERY_DELETE'); ?></a></li>
				<?php } ?>
				</form>
				<form class="print_form" method="POST" target="_blank" action="<?php echo JRoute::_('index.php?option=com_crmery&view=print'); ?>">
				<input type="hidden" name="item_id" value="<?php echo $company['id']; ?>" />
		    	<input type="hidden" name="layout" value="company" />
		    	<input type="hidden" name="model" value="company" />
	            <li><a onclick="printItems(this)"><?php echo CRMText::_('COM_CRMERY_PRINT'); ?></a></li>
	        	</form>
	        </ul>
        </form>
    </div>
</span>
</div>
<h1><?php echo $company['name']; ?></h1>

<div class="container">
	<div class="columncontainer">	
		<div class="threecolumn">
			<div class="small_info first">
				<?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_TOTAL_PIPELINE')); ?>:
				<span class="amount"><?php echo CrmeryHelperConfig::getCurrency(); ?><?php echo $company['pipeline']; ?></span></td>
			</div>
		</div>	
		<div class="threecolumn">
			<div class="small_info middle">
				<?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_DEALS')); ?>:
				<span class="amount"><?php echo CrmeryHelperConfig::getCurrency(); ?><?php echo $company['won_deals']; ?></span>
			</div>
		</div>

		<div class="threecolumn">
			<div class="small_info">
				<?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY_CONTACTED')); ?>:
				<?php echo CrmeryHelperDate::formatDate($company['modified']); ?>
			</div>
		</div>
	</div>	
<h2><?php echo ucwords(CRMText::_('COM_CRMERY_EDIT_NOTES')); ?></h2>
<?php echo $company['notes']->display(); ?>

<h2><?php echo CRMText::_('COM_CRMERY_EDIT_CUSTOM'); ?></h2>
<div class="columncontainer">
	<?php $this->custom_fields_view->display(); ?>
</div>

<h2><?php echo ucwords(CRMText::_('COM_CRMERY_EDIT_DEALS')); ?></h2>

	<div class="large_info">
		<?php $this->deal_dock->display(); ?>	
	<span class="actions"><a class="button" onclick="addDeal('company_id=<?php echo $company['id']; ?>')" href="javascript:void(0);"><?php echo ucwords(CRMText::_('COM_CRMERY_ADD_DEAL')); ?></a></span>

	</div>

<h2><?php echo ucwords(CRMText::_('COM_CRMERY_EDIT_PEOPLE')); ?></h2>

	<div class="large_info">
		
		<?php echo $this->people_dock->display(); ?>
	<span class="actions"><a class="button" href="javascript:void(0);" onclick="addPerson('company_id=<?php echo $company['id']; ?>');"><?php echo ucwords(CRMText::_('COM_CRMERY_ADD_PERSON')); ?></a></span>		
	</div>
</div>

<h2><?php echo ucwords(CRMText::_('COM_CRMERY_EDIT_DOCUMENTS')); ?></h2>

	   
    <div class="large_info">
        <table class="com_crmery_table" id="documents_table">
        <thead>
            <th><?php echo CRMText::_('COM_CRMERY_TYPE'); ?></th> 
            <th><?php echo CRMText::_('COM_CRMERY_FILENAME'); ?></th>
            <th><?php echo CRMText::_('COM_CRMERY_OWNER'); ?></th>
            <th><?php echo CRMText::_('COM_CRMERY_SIZE'); ?></th>
            <th><?php echo CRMText::_('COM_CRMERY_UPLOADED'); ?></th>
        </thead>
        <tbody id="documents">
          <?php echo $this->document_list->display(); ?>
        </tbody>
        </table>
        <span class="actions">
	        <form id="upload_form" target="hidden" action="index.php?option=com_crmery&controller=documents&task=uploadDocument&format=raw&tmpl=component" method="POST" enctype="multipart/form-data">
		        <div class="input_upload_button fltrt" >
		            <a href="javascript:void(0);" type="button" class="button" id="upload_button" ><?php echo CRMText::_('COM_CRMERY_UPLOAD_FILE'); ?></a>
		            <input type="file" id="upload_input_invisible" name="document" />
		        </div>
	        </form>
	    </span>

    </div>    
<?php 

$user_id = CrmeryHelperUsers::getUserId();
$member_role = CrmeryHelperUsers::getRole();
$db =& JFactory::getDbo();
	
$can_view_billing = false;	
	
if($member_role == 'basic' || $member_role == 'manager')
{ 
	$ccode = $db->getQuery(true);
	$ccode->select('id');
	$ccode->from('#__crmery_users');
	$ccode->where("(company_id=".$company['id']." or inp_company_id=".$company['id']." )");
	$db->setQuery($ccode);
	//echo '<br>'.$ccode;
	$selected_id = $db->loadResult();
	if($selected_id > 0){
		$can_view_billing = true;
	}else{  // if user is firm partner
	
	    $f_part = $db->getQuery(true);
		$f_part->select('id');
		$f_part->from('#__crmery_companies');
		$f_part->where("partner_id=".$user_id." and id=".$company['id']."");
		$db->setQuery($f_part);
		//$user_id;
	 
		$selected_id = $db->loadResult();
		if($selected_id > 0)
			$can_view_billing = true;
	}	
}else
{
	$ccode = $db->getQuery(true);
	$ccode->select('region_id');
	$ccode->from('#__crmery_users');
	$ccode->where("id=".$user_id."");
	$db->setQuery($ccode);
	//echo '<br>'.$ccode;
	$user_region_id = $db->loadResult();
	$usr_regionArr = @explode(',',$user_region_id);
	
	if(in_array($company['region_id'], $usr_regionArr))
		$can_view_billing = true;
		
}


if($can_view_billing == true)
{
	$querys = $db->getQuery(true);
	$querys->select("b.*")
	->from("#__crmery_companies_billing AS b");
	$querys->where("b.firm_id='".$company['id']."'");						

	$db->setQuery($querys);
	$billing_rs = $db->loadObjectList();

	
?>	
	
  <div class="container">
	    <h2><?php echo CRMText::_('COM_CRMERY_FIRM_FEE_INCOME'); ?></h2>
	    <div class="large_info"> 
	    	<table class="com_crmery_table" id="people" >
			  <thead>
				<tr>

					<th><?php echo CRMText::_('COM_CRMERY_FIRM_BILLINGYEAR'); ?></th>
					<th><?php echo CRMText::_('COM_CRMERY_GROSS_LOCAL_INCOME'); ?></th>
					<th><?php echo CRMText::_('COM_CRMERY_GROSS_LOCAL_INCOME'); ?> in GBP</th>
					<th><?php echo CRMText::_('COM_CRMERY_FEE_INCOME_PORMOTIONAL'); ?></th>
					<th><?php echo CRMText::_('COM_CRMERY_FEE_INCOME_PORMOTIONAL'); ?> in GBP</th>
					<th><?php echo CRMText::_('COM_CRMERY_FIRM_BILLING_TYPE'); ?></th>
				 
				
				</tr>
			 </thead>
 <?php 
 if(count($billing_rs)>0){

	foreach($billing_rs as $billing){
		 
		echo '<tr>';

		echo '<td>'.$billing->BillingYear.'</td>';
		echo '<td>'.number_format($billing->gross_fee_income,2,'.',',').'</td>';
		echo '<td>'.number_format($billing->feeincome_gbp,2,'.',',').'</td>';
		echo '<td>'.number_format($billing->fee_income_promotional,2,'.',',').'</td>';
		echo '<td>'.number_format($billing->fee_income_promotional_gbp,2,'.',',').'</td>';
		echo '<td>'.$billing->billingtype.'</td>';
	
		echo '</tr>';
	 }
 
 }
 else
 {
	echo '<tr><td colspan="12" align="center">No Billings added yet!</td> </tr>';
 }
 ?>
</table>
	    </div>
	</div>
<?php } // end if of $can_view_billing ?>	
	
	<br />
	<br />
	
    <div class="container">
	    <h2><?php echo CRMText::_('COM_CRMERY_LATEST_ACTIVITIES'); ?></h2>
	    <div class="large_info">
	    	<?php echo $this->latest_activities->display(); ?>
	    </div>
	</div>
    
</div>
</div>

<div id="message" style="display:none;"><?php echo CRMText::_('COM_CRMERY_SUCCESS_MESSAGE'); ?></div>

<div id="ajax_search_person_dialog" style="display:none;">
	<input class="inputbox" type="text" name="person_name" placeholder="<?php echo CRMText::_('COM_CRMERY_BEGIN_TYPING_TO_SEARCH'); ?>" value="" />
	<div class="actions"><input class="button" type="button" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" onclick="addPersonToCompany();closeDialog('person')"/> <?php echo CRMText::_('COM_CRMERY_OR'); ?> <a href="javascript:void(0);" onclick="closeDialog('person')"><?php echo CRMText::_('COM_CRMERY_CANCEL'); ?></a></div>
</div>

<div id="ajax_search_deal_dialog" style="display:none;">
	<form id="deal">
	<h2><?php echo ucwords(CRMText::_('COM_CRMERY_ASSOCIATE_TO_DEAL')); ?></h2>
		<input type="text" name="deal_name" placeholder="Begin typing to search..." />
		<input type="hidden" name="company_id" value="<?php echo $company['id']; ?>">
		<div class="actions"><input type="button" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" onclick="saveAjax('deal','deal');closeDialog('deal')"/> or <a onclick="closeDialog('deal')">Cancel</a></div>
	</form>
</div>

<?php echo CrmeryHelperCrmery::showShareDialog(); ?>