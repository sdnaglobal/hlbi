<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

//define deal
$company = $this->companies[0];

?>

<script type="text/javascript">
	var loc = "company";
	var id = <?php echo $company['id']; ?>;
	var company_id = <?php echo $company['id']; ?>;
	var association_type = 'company';
</script>

<div data-role='header' data-theme='b'>
	<h1><?php echo $company['name']; ?></h1>
		<a href="<?php echo JRoute::_('index.php?option=com_crmery&view=companies'); ?>" data-icon="back" class="ui-btn-left">
		<?php echo CRMText::_('COM_CRMERY_BACK'); ?>
	</a>
</div>

<div data-role='content' data-theme='b'>
	<ul data-inset='true' data-role='listview' data-theme="c">
		<?php if($company['phone']!="") { ?>
			<li>
				<a href="tel://<?php echo $company['phone']; ?>" rel="external"><?php echo $company['phone']; ?></a>
			</li>
		<?php } ?>

		<?php if($company['address_1']!="") { ?>
		<li>
			<a href="<?php echo JRoute::_('index.php?option=com_crmery&view=companies&layout=directions&id='.$company['id']); ?>"><?php echo $company['address_1'].' '.$company['address_city'].' '.$company['address_state'].' '.$company['address_zip']; ?></a>
		</li>
		<?php } ?>

		<?php if($company['website']!="") { ?>
		<li>
			<a href="<?php echo $company['website']; ?>" target="_blank"><?php echo $company['website']; ?></a>
		</li>
		<?php } ?>
	</ul>

	<div data-role="collapsible" data-collapsed="false">
		<h3><?php echo CRMText::_('COM_CRMERY_EDIT_NOTES'); ?></h3>
			<ul data-inset='true' data-role='listview' data-theme="c" id="notes">
				<?php $company['notes']->display(); ?>
			</ul>
	</div>

	<div data-role="collapsible">
		<h3><?php echo CRMText::_('COM_CRMERY_ADD_NOTE'); ?></h3>
		<?php echo $this->add_note->display(); ?>
	</div>
</div>