<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="dash_float">
    <div class="dash_float_header">
        <a class="minify"></a><h2><?php echo ucwords(CRMText::_('COM_CRMERY_LEADERBOARD')); ?></h2>
    </div>
    <div class="container">
        <div class="goal_filter_container">
        <?php if ( count($this->leaderboard_list) > 0 ) { ?>
            <span class="filters"><?php echo CRMText::_("COM_CRMERY_SHOWING_LEADERBOARD"); ?> - <a class="dropdown" id="leaderboard_filter_link" ><?php $keys = array_keys($this->leaderboard_list); echo $this->leaderboard_list[$keys[0]]; ?></a></span>
            <div class="filters" id="leaderboard_filter">
                <ul>
                    <?php foreach ( $this->leaderboard_list as $key=>$name ){
                         echo "<li><a class='filter_".$key." dropdown_item' onclick=\"changeLeaderBoard('".$key."')\">".$name."</a></li>";
                    }?>                         
                </ul>
            </div>
        <?php } ?>
        </div>
        <div id="leaderboards" class="goal_list">
        <?php
            if ( count($this->goals->leaderboards) > 0 ) {
            //show first leaderboard
                $first_leaderboard = $this->goals->leaderboards[0];
                foreach ( $first_leaderboard['members'] as $member ) { 
                    echo '<div class="leader">';
                    echo '<div class="leader_name">'.$member['first_name'].' '.$member['last_name'].'</div>';
                    echo '<div class="leader_progress_container">';
                    //determine bar progress
                    //win_cash
                    if ( $first_leaderboard['goal_type'] == 'win_cash' ){
                        $width = $member['cash_won'] / $first_leaderboard['amount'] * 100;
                    }
                    //win_deals
                    if ( $first_leaderboard['goal_type'] == 'win_deals' ){
                        $width = $member['deals_won'] / $first_leaderboard['amount'] * 100;
                    }
                    //move_deals
                    if ( $first_leaderboard['goal_type'] == 'move_deals' ){
                        $width = $member['deals_moved'] / $first_leaderboard['amount'] * 100;
                    }
                    //complete_tasks
                    if ( $first_leaderboard['goal_type'] == 'complete_tasks' ){
                        $width = $member['tasks_completed'] / $first_leaderboard['amount'] * 100;
                    }
                    //write_notes
                    if ( $first_leaderboard['goal_type'] == 'write_notes' ){
                        $width = $member['notes_written'] / $first_leaderboard['amount'] * 100;
                    } 
                    //create_deals
                    if ( $first_leaderboard['goal_type'] == 'create_deals' ){
                        $width = $member['deals_created'] / $first_leaderboard['amount'] * 100;
                    }
                    echo '<div class="leader_progress" style="width:'.$width.'%;"></div>';
                    echo '</div>';
                    echo '<div class="leader_info">';
                    //output info
                    //win_cash
                    if ( $first_leaderboard['goal_type'] == 'win_cash' ){
                        echo CrmeryHelperConfig::getConfigValue('currency').(int)$member['cash_won'].' '.CRMText::_('COM_CRMERY_CASH_WON');
                    }
                    //win_deals
                    if ( $first_leaderboard['goal_type'] == 'win_deals' ){
                        echo (int)$member['deals_won'].' '.CRMText::_('COM_CRMERY_DEALS_WON');
                    }
                    //move_deals
                    if ( $first_leaderboard['goal_type'] == 'move_deals' ){
                        echo (int)$member['deals_moved'].' '.CRMText::_('COM_CRMERY_DEALS_MOVED');
                    }
                    //complete_tasks
                    if ( $first_leaderboard['goal_type'] == 'complete_tasks' ){
                        echo (int)$member['tasks_completed'].' '.CRMText::_('COM_CRMERY_TASKS_COMPLETED');
                    }
                    //write_notes
                    if ( $first_leaderboard['goal_type'] == 'write_notes' ){
                        echo (int)$member['notes_written'].' '.CRMText::_('COM_CRMERY_NOTES_WRITTEN_MESSAGE');
                    } 
                    //create_deals
                    if ( $first_leaderboard['goal_type'] == 'create_deals' ){
                        echo (int)$member['deals_created'].' '.CRMText::_('COM_CRMERY_DEALS_CREATED_MESSAGE');
                    }
                    echo '</div>';
                    echo '</div>';
                    
                }
            }
        ?>
        </div>
    </div>
</div>