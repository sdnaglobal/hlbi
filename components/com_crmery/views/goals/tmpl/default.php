<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<h1><?php echo ucwords(CRMText::_('COM_CRMERY_GOALS')); ?></h1>
<div class="goal_floats_left">
    <ul class="goal_float_list" id="goal_floats_left">
        <?php if ( isset($this->goal_floats_left) && count($this->goal_floats_left) > 0 ){
                foreach ( $this->goal_floats_left as $float ){
                    $view = CrmeryHelperView::getView('goals',$float);
                    if ( $view && $float != "" ){
                        if ( $float == "leaderboard_float" ){
                            $view->assignRef('leaderboard_list',$this->leaderboard_list);
                            $view->assignRef('goals',$this->goals);
                        }
                        if ( $float == "individual_goals_float" ){
                            $view->assignRef('users',$this->users);
                            $view->assignRef('member_role',$this->member_role);
                            $view->assignRef('user_id',$this->user_id);
                            $view->assignRef('goals',$this->goals);
                        }
                        if ( $float == "team_goals_float" ){
                            $view->assignRef('teams',$this->teams);
                            $view->assignRef('member_role',$this->member_role);
                            $view->assignRef('team_id',$this->team_id);
                            $view->assignRef('goals',$this->goals);
                        }
                        if ( $float == "company_goals_float" ){
                            $view->assignRef('member_role',$this->member_role);
                            $view->assignRef('goals',$this->goals);
                        }
                        echo '<li id="'.$float.'">';
                            echo $view->display();
                        echo '</li>';
                    }
                }
            } ?>
    </ul>
</div>
<div class="goal_floats_right">
    <ul class="goal_float_list" id="goal_floats_right">
        <?php if ( isset($this->goal_floats_right) && count($this->goal_floats_right) > 0 ){
                foreach ( $this->goal_floats_right as $float ){
                    $view = CrmeryHelperView::getView('goals',$float);
                    if ( $view && $float != "" ){
                        if ( $float == "leaderboard_float" ){
                            $view->assignRef('leaderboard_list',$this->leaderboard_list);
                            $view->assignRef('goals',$this->goals);
                        }
                        if ( $float == "individual_goals_float" ){
                            $view->assignRef('users',$this->users);
                            $view->assignRef('member_role',$this->member_role);
                            $view->assignRef('user_id',$this->user_id);
                            $view->assignRef('goals',$this->goals);
                        }
                        if ( $float == "team_goals_float" ){
                            $view->assignRef('teams',$this->teams);
                            $view->assignRef('member_role',$this->member_role);
                            $view->assignRef('team_id',$this->team_id);
                            $view->assignRef('goals',$this->goals);
                        }
                        if ( $float == "company_goals_float" ){
                            $view->assignRef('member_role',$this->member_role);
                            $view->assignRef('goals',$this->goals);
                        }
                        echo '<li id="'.$float.'">';
                            echo $view->display();
                        echo '</li>';
                    }
                }
            } ?>
    </ul>
</div>
<div id="delete_goals"></div>