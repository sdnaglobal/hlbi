<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>


<h1><?php echo ucwords($this->header); ?></h1>
<form action="<?php echo JRoute::_('index.php?option=com_crmery&controller=goals&task=edit'); ?>" method="post" onsubmit="return save(this);">
    <div id="goal_edit">
        <ul>
             <li>
                <fieldset class="goal_one">
                    <legend><b><?php echo ucwords(CRMText::_('COM_CRMERY_GOAL_STEP_ONE')); ?></b></legend>
                    <?php
                        //generate user dropdown
                        $user_html = '<select class="inputbox" id="assigned_id">';
                        $selectedId = isset($this->goal) ? $this->goal['assigned_id'] : 0;
                        $selectedType = isset($this->goal) ? $this->goal['assigned_type'] : "";
                            //if an executive is creating a goal
                            if ( $this->member_role == 'exec' ){
                                $selected = $selectedType == "company" ? "selected='selected'" : '';
                                $user_html .= "<option value='company_0' ".$selected.">".ucwords(CRMText::_('COM_CRMERY_THE_COMPANY'))."</option>";
                            }
                            //individual assignment
                            $selected = $selectedType == "member" && $selectedId == CrmeryHelperUsers::getUserId() ? "selected='selected'" : '';
                            $user_html .= "<option ".$selected." value='member_".$this->user_id."'>".CRMText::_('COM_CRMERY_ME')."</option>";
                            //if a manager is creating a goal
                            if ( $this->member_role == 'manager' ){
                                if ( $this->team_id != null ){
                                    $selected = $selectedType == "team" && $selectedId == CrmeryHelperUsers::getTeamId() ? "selected='selected'" : '';
                                    $user_html .= "<option ".$selected." value='team_".$this->team_id."'>".CRMText::_('COM_CRMERY_MY_TEAM')."</option>";
                                }
                                if ( count($this->users) > 0 ){
                                    foreach ( $this->users as $user ){
                                        $selected = $selectedType == "member" && $selectedId == $user['id'] ? "selected='selected'" : '';
                                        $user_html .= "<option ".$selected." value='member_".$user['id']."'>".$user['first_name']." ".$user['last_name']."</option>";
                                    }
                                }
                            }
                            //an executive can also see all of the teams and individual users
                            if ( $this->member_role == 'exec' ){
                                if ( count($this->teams) > 0 ){
                                    foreach ( $this->teams as $team ){
                                        $selected = $selectedType == "team" && $selectedId == $team['team_id'] ? "selected='selected'" : '';
                                        $user_html .= "<option ".$selected." value='team_".$team['team_id']."'>".$team['team_name'].CRMText::_('COM_CRMERY_TEAM_APPEND')."</option>";
                                    }   
                                }
                                if ( count($this->users) > 0 ){
                                    foreach ( $this->users as $user ){
                                        $selected = $selectedType == "member" && $selectedId == $user['id'] ? "selected='selected'" : '';
                                        $user_html .= "<option ".$selected." value='member_".$user['id']."'>".$user['first_name']." ".$user['last_name']."</option>";
                                    }
                                }
                            }
                            //if a basic member is creating a goal we show no more options
                        $user_html .= "</select>";
                        
                        //generate date html
                        $date_html = "<select class='inputbox' name='goal_date' id='date_picker'>";
                        $dates = CrmeryHelperDate::getGoalDates();
                        $value = isset($this->goal) ? $this->goal['goal_date'] : '';
                        $date_html .= JHtml::_('select.options', $dates, 'value', 'text', $value, true); 
                        $date_html .= "</select>";
                    ?>
                    <?php $goalType = $this->type == "edit" ? $this->goal['goal_type'] : $this->type; ?>
                    <?php switch($goalType){
                        case "win_cash": ?>
                                <?php echo CRMText::_('COM_CRMERY_I_WANT'); ?>
                                    <?php echo $user_html; ?>
                                    <?php echo CRMText::_('COM_CRMERY_TO_SELL'); ?>
                                    <input class="required inputbox" type="text" value="<?php if ( isset($this->goal) ){ echo $this->goal['amount']; } ?>" placeholder="0" name="amount" />
                                    <?php echo CRMText::_('COM_CRMERY_OF_NEW_BUSINESS'); ?>
                                   <?php echo $date_html; ?>
                                <?php
                            break;
                        case "win_deals": ?>
                                <?php echo CRMText::_('COM_CRMERY_I_WANT'); ?>
                                    <?php echo $user_html; ?>
                                    <?php echo CRMText::_('COM_CRMERY_TO_WIN'); ?>
                                    <input class="inputbox required" type="text" value="<?php if ( isset($this->goal) ){ echo $this->goal['amount']; } ?>" placeholder="0" name="amount" />
                                    <?php echo CRMText::_('COM_CRMERY_NEW_DEALS'); ?>
                                    <?php echo $date_html; ?>
                                <?php
                            break;
                        case "move_deals": ?>
                                <?php echo CRMText::_('COM_CRMERY_I_WANT'); ?>
                                    <?php echo $user_html; ?>
                                    <?php echo CRMText::_('COM_CRMERY_TO_MOVE'); ?>
                                    <input class="inputbox required" type="text" placeholder="0" value="<?php if ( isset($this->goal) ){ echo $this->goal['amount']; } ?>" name="amount" />
                                    <?php echo CRMText::_('COM_CRMERY_DEALS_FORWARD_TO_THE'); ?>
                                    <select class="inputbox" name="stage_id">
                                        <?php 
                                            $stages = CrmeryHelperDeal::getGoalStages();
                                            $value = isset($this->goal) ? $this->goal['stage_id'] : '';
                                            echo JHtml::_('select.options', $stages, 'value', 'text', $value, true);
                                        ?>
                                    </select>
                                    <?php echo CRMText::_('COM_CRMERY_STAGE'); ?>
                                    <?php echo $date_html; ?>
                                <?php
                            break;
                        case "complete_tasks": ?>
                                 <?php echo CRMText::_('COM_CRMERY_I_WANT'); ?>
                                    <?php echo $user_html; ?>
                                    <?php echo CRMText::_('COM_CRMERY_TO_COMPLETE'); ?>
                                    <input class="inputbox required" type="text" value="<?php if ( isset($this->goal) ){ echo $this->goal['amount']; } ?>" placeholder="0" name="amount" />
                                    <?php echo CRMText::_('COM_CRMERY_TASKS_OF_TYPE'); ?>
                                    <select class="inputbox" name="category_id">
                                        <option value="">Any</option>
                                        <?php 
                                            $categories = CrmeryHelperEvent::getCategories();
                                            $value = isset($this->goal) ? $this->goal['category_id'] : '';
                                            echo JHtml::_('select.options', $categories, 'value', 'text', $value, true);
                                        ?>
                                    </select>
                                    <?php echo $date_html; ?>
                            <?php
                            break;
                        case "write_notes": ?>
                             <?php echo CRMText::_('COM_CRMERY_I_WANT'); ?>
                                    <?php echo $user_html; ?>
                                    <?php echo CRMText::_('COM_CRMERY_TO_WRITE'); ?>
                                    <input class="inputbox required" type="text" value="<?php if ( isset($this->goal) ){ echo $this->goal['amount']; } ?>" placeholder="0" name="amount" />
                                    <?php echo CRMText::_('COM_CRMERY_NEW_NOTES_OF_TYPE'); ?>
                                    <select class="inputbox" name="category_id">
                                        <option value="">Any</option>
                                        <?php 
                                            $categories = CrmeryHelperNote::getCategories();
                                            $value = isset($this->goal) ? $this->goal['category_id'] : '';
                                            echo JHtml::_('select.options', $categories, 'value', 'text', $value, true);
                                        ?>
                                    </select>
                                    <?php echo $date_html; ?>
                            <?php
                            break;
                        case "create_deals": ?>
                             <?php echo CRMText::_('COM_CRMERY_I_WANT'); ?>
                                    <?php echo $user_html; ?>
                                    <?php echo CRMText::_('COM_CRMERY_TO_CREATE'); ?>
                                    <input class="inputbox required" type="text" value="<?php if ( isset($this->goal) ){ echo $this->goal['amount']; } ?>" placeholder="0" name="amount" />
                                    <?php echo CRMText::_('COM_CRMERY_DEALS'); ?>
                                    <?php echo $date_html; ?>
                            <?php
                            break;
                    }?>
                </fieldset>
            </li>
            <li id="date_selection_area" style="display:none;">
                <div id="date_selection_area_template">
                    <fieldset class="custom_date">
                        <legend><b><?php echo CRMText::_('COM_CRMERY_SET_YOUR_DATE'); ?></b></legend>
                        <ul>
                            <li>
                                <label><?php echo CRMText::_('COM_CRMERY_START_DATE'); ?></label>
                                <input class="date_input inputbox required" type="text" name="start_date_hidden" id="start_date">
                                <input type="hidden" id="start_date_hidden" value="" name="start_date"/>
                            </li>
                            <li>
                                <label><?php echo CRMText::_('COM_CRMERY_END_DATE'); ?></label>
                                <input class="date_input inputbox required" type="text" id="end_date" name="end_date_hidden">
                                <input type="hidden" id="end_date_hidden" value="" name="end_date"/>
                            </li>
                        </ul>
                    </fieldset>
                </div>
            </li>
            <li>
                <fieldset class="goal_two">
                    <legend><b><?php echo ucwords(CRMText::_('COM_CRMERY_GOAL_STEP_TWO')); ?></b></legend>
                    <input type="text" name="name" class="inputbox required" value="<?php if ( isset($this->goal) ) echo $this->goal['name']; ?>" />
                </fieldset>
            </li>
            <li>
                <fieldset class="goal_three">
                    <legend><b><?php echo ucwords(CRMText::_('COM_CRMERY_GOAL_STEP_THREE')); ?></b></legend>
                        <ul>
                            <li><label><?php echo CRMText::_('COM_CRMERY_GOAL_STEP_THREE_DESC'); ?></label></li>
                            <li><label class="small"><input <?php if ( isset($this->goal) && $this->goal['leaderboard'] == 1 ) echo "checked='checked'"; ?> type="checkbox" name="leaderboard"></label><span class="faux_input"><?php echo CRMText::_('COM_CRMERY_GOAL_CREATE_LEADERBOARD'); ?></span></li>
                        </ul> 
                </fieldset>
            </li>
            <li>
                <div class="actions">
                    <?php $saveText = isset($this->goal) ? CRMText::_('COM_CRMERY_SAVE') : CRMText::_('COM_CRMERY_ADD'); ?>
                    <input type="submit" class="button" value="<?php echo $saveText; ?>"> <a onclick="window.history.back()"><?php echo CRMText::_('COM_CRMERY_CANCEL_BUTTON'); ?></a>
                </div>
            </li>
        </ul>
    </div>
    <div id="hidden">
        <?php if ( $this->member_role == 'exec' ){ ?>
            <input type="hidden" name="assigned_id" value="0">
            <input type="hidden" name="assigned_type" value="company">
        <?php } else { ?>
            <input type="hidden" name="assigned_id" value="<?php echo $this->user_id; ?>">
            <input type="hidden" name="assigned_type" value="member">
        <?php } ?>
        <input type="hidden" name="goal_type" value="<?php if ( isset($this->goal) ) { echo $this->goal['goal_type']; } else { echo $this->type; } ?>">
        <?php if ( isset($this->goal) ){ ?>
            <input type="hidden" name="id" value="<?php echo $this->goal['id']; ?>" />
        <?php } ?>
    </div>
</form>
