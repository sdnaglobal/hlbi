<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

if ( count($this->goals > 0 ) ){
        foreach ( $this->goals as $goal ){ ?>
           <div id="goal_<?php echo $goal['id']; ?>" class="goal_info">
                <div class="goal_info_name">
                    <?php if ( $goal['owner_id'] == CrmeryHelperUsers::getUserId() || CrmeryHelperUsers::isAdmin() ){ ?>
                        <a href="<?php echo JRoute::_('index.php?option=com_crmery&view=goals&layout=edit&id='.$goal['id']); ?>">
                    <?php } ?>
                    <?php echo $goal['name']; ?>
                    <?php if ( $goal['owner_id'] == CrmeryHelperUsers::getUserId() || CrmeryHelperUsers::isAdmin() ){ ?>
                        </a>
                    <?php } ?>
                </div>
                <div class="goal_info_due_date">by <?php echo CrmeryHelperDate::formatDate($goal['end_date']); ?></div>
                <div class="goal_info_progress">
                    <?php $bgcolor=CrmeryHelperCrmery::percent2Color($goal['goal_info']/$goal['amount']*100); ?>
                    <div class="goal_info_progress_total" style="background-color:#<?php echo $bgcolor; ?>;width:<?php echo number_format($goal['goal_info']/$goal['amount']*100); ?>%;"></div>
                </div>
                <div class="goal_info_out_of">
                    <?php
                        if ( $goal['goal_type'] == 'win_cash' ){
                            echo "$".(int)$goal['goal_info'] ?> out of $<?php echo $goal['amount']." won.";
                        }
                        if ( $goal['goal_type'] == 'win_deals' ){
                            echo (int)$goal['goal_info'] ?> out of <?php echo $goal['amount'] . " deals won.";
                        } 
                        if ( $goal['goal_type'] == 'move_deals' ){
                            echo (int)$goal['goal_info'] ?> out of <?php echo $goal['amount'] . " deals moved.";
                        }
                        if ( $goal['goal_type'] == 'complete_tasks' ){
                            echo (int)$goal['goal_info'] ?> out of <?php echo $goal['amount'] . " tasks completed.";
                        }
                        if ( $goal['goal_type'] == 'write_notes' ){
                            echo (int)$goal['goal_info'] ?> out of <?php echo $goal['amount'] . " notes written.";
                        }
                        if ( $goal['goal_type'] == 'create_deals' ){
                            echo (int)$goal['goal_info'] ?> out of <?php echo $goal['amount'] . " deals created.";
                        }
                    ?>
                </div>
                <div class="goal_info_progress_percentage"><?php echo number_format(($goal['goal_info']/$goal['amount']*100)); ?>% completed</div>
            </div>
<?php } }
?>



