<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="dash_float">
    <div class="dash_float_header">
        <a class="minify"></a><h2><?php echo CRMText::_("COM_CRMERY_TEAM_GOALS"); ?></h2>
    </div>
    <div class="container">
        <div class="goal_filter_container">

        <?php if ( ( count($this->teams) > 0 ) && $this->member_role == 'exec' ){ ?>

            <span class="filters"><?php echo CRMText::_('COM_CRMERY_GOALS_FOR'); ?> - <a class="dropdown" id="team_goal_select_link" ><?php echo CRMText::_("COM_CRMERY_MY_TEAM"); ?></a></span>
            <div class="filters" id="team_goal_select">
                <ul>
                    <li><a class='filter_<?php echo $this->team_id; ?> dropdown_item'  onclick="changeTeam(<?php echo $this->team_id; ?>)"><?php echo CRMText::_("COM_CRMERY_MY_TEAM"); ?></a></li>
                    <?php foreach( $this->teams as $team ){ ?>
                        <li><a class='filter_<?php echo $team['team_id']; ?> dropdown_item'  onclick="changeTeam(<?php echo $team['team_id']; ?>)"><?php echo $team['team_name'].CRMText::_('COM_CRMERY_TEAM_APPEND'); ?></a></li>
                    <?php } ?>
                </ul>
            </div>

        <?php } else { ?>

        <?php echo CRMText::_("COM_CRMERY_SHOWING_GOALS_FOR"); ?> <b><?php echo CRMText::_("COM_CRMERY_MY_TEAM"); ?></b>

        <?php } ?>

        </div>
    <div id="team_goals" class="goal_list">
        <?php
            if ( count($this->goals->team_goals) > 0 ){
                foreach ( $this->goals->team_goals as $goal ){ ?>
                    <div id="goal_<?php echo $goal['id']; ?>" class="goal_info">
                        <div class="goal_info_name">
                            <?php if ( $goal['owner_id'] == CrmeryHelperUsers::getUserId() || CrmeryHelperUsers::isAdmin() || CrmeryHelperUsers::getRole() == 'manager' ){ ?>
                                <a href="<?php echo JRoute::_('index.php?option=com_crmery&view=goals&layout=edit&id='.$goal['id']); ?>">
                            <?php } ?>
                            <?php echo $goal['name']; ?>
                            <?php if ( $goal['owner_id'] == CrmeryHelperUsers::getUserId() || CrmeryHelperUsers::isAdmin() || CrmeryHelperUsers::getRole() == 'manager' ){ ?>
                                </a>
                            <?php } ?>
                        </div>
                        <div class="goal_info_due_date"><?php echo CRMText::_('COM_CRMERY_BY'); ?> <?php echo CrmeryHelperDate::formatDate($goal['end_date']); ?></div>
                        <div class="goal_info_progress">
                            <?php $bgcolor=CrmeryHelperCrmery::percent2Color($goal['goal_info']/$goal['amount']*100); ?>
                            <div class="goal_info_progress_total" style="background-color:#<?php echo $bgcolor; ?>;width:<?php echo number_format($goal['goal_info']/$goal['amount']*100); ?>%;"></div>
                        </div>
                        <div class="goal_info_out_of">
                            <?php
                                if ( $goal['goal_type'] == 'win_cash' ){
                                    echo CrmeryHelperConfig::getCurrency().(int)$goal['goal_info'] ?> <?php echo CRMText::_("COM_CRMERY_OUT_OF"); ?> <?php echo CrmeryHelperConfig::getConfigValue('currency'); ?><?php echo $goal['amount']." ".CRMText::_('COM_CRMERY_WON');
                                }
                                if ( $goal['goal_type'] == 'win_deals' ){
                                    echo (int)$goal['goal_info'] ?> <?php echo CRMText::_("COM_CRMERY_OUT_OF"); ?> <?php echo $goal['amount'] . " ".CRMText::_('COM_CRMERY_DEALS_WON');
                                } 
                                if ( $goal['goal_type'] == 'move_deals' ){
                                    echo (int)$goal['goal_info'] ?> <?php echo CRMText::_("COM_CRMERY_OUT_OF"); ?> <?php echo $goal['amount'] . " ".CRMText::_('COM_CRMERY_DEALS_MOVED');
                                }
                                if ( $goal['goal_type'] == 'complete_tasks' ){
                                    echo (int)$goal['goal_info'] ?> <?php echo CRMText::_("COM_CRMERY_OUT_OF"); ?> <?php echo $goal['amount'] . " ".CRMText::_('COM_CRMERY_TASKS_COMPLETED');
                                }
                                if ( $goal['goal_type'] == 'write_notes' ){
                                    echo (int)$goal['goal_info'] ?> <?php echo CRMText::_("COM_CRMERY_OUT_OF"); ?> <?php echo $goal['amount'] . " ".CRMText::_('COM_CRMERY_NOTES_WRITTEN_MESSAGE');
                                }
                                if ( $goal['goal_type'] == 'create_deals' ){
                                    echo (int)$goal['goal_info'] ?> <?php echo CRMText::_("COM_CRMERY_OUT_OF"); ?> <?php echo $goal['amount'] . " ".CRMText::_('COM_CRMERY_DEALS_CREATED_MESSAGE');
                                }
                            ?>
                        </div>
                        <div class="goal_info_progress_percentage"><?php echo number_format(($goal['goal_info']/$goal['amount']*100)); ?>% <?php echo CRMText::_("COM_CRMERY_COMPLETED"); ?></div>
                    </div>
        <?php } }
        ?>
    </div>
    <?php if ( $this->member_role != 'basic' ){ ?>
    <div class="goal_actions">
        <ul>
            <li><a class="button" href="<?php echo JRoute::_('index.php?option=com_crmery&view=goals&layout=add'); ?>"><?php echo CRMText::_("COM_CRMERY_ADD_GOAL"); ?></a></li>
            <li><a href="javascript:void(0);" class="delete_goals" id="goal_type_team"><?php echo CRMText::_("COM_CRMERY_DELETE_GOALS"); ?></a></li>
        </ul>
    </div>
    <?php } ?>
    </div>
</div>