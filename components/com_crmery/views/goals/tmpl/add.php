<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<h1><?php echo ucwords(CRMText::_('COM_CRMERY_GOALS_HEADER')); ?></h1>
<div class="goals_columns">
    <div class="goals_left_column">
        <ul>
        	<li>
        	    <div class="goal_container">
            	    <div class="goal_img"><img src="<?php echo JURI::base(); ?>components/com_crmery/media/images/win_more_cash.png" /></div>
            	    <div class="goal_info_container">
            	        <h2><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=goals&layout=edit&type=win_cash'); ?>"><?php echo ucwords(CRMText::_('COM_CRMERY_WIN_MORE_CASH').' '.CrmeryHelperConfig::getConfigValue('currency')); ?></a></h2>
            	        <div class="goal_info"><?php echo JText::sprintf('COM_CRMERY_CREATE_GOAL_TRACK_CASH',CrmeryHelperConfig::getConfigValue('currency')); ?></div>
            	    </div>
        	    </div>
    	    </li>
        	<li>
        	    <div class="goal_container">
            	    <div class="goal_img"><img src="<?php echo JURI::base(); ?>components/com_crmery/media/images/win_more_deals.png" /></div>
            	    <div class="goal_info_container">
            	        <h2><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=goals&layout=edit&type=win_deals'); ?>"><?php echo ucwords(CRMText::_('COM_CRMERY_WIN_MORE_DEALS')); ?></a></h2>
            	        <div class="goal_info"><?php echo CRMText::_('COM_CRMERY_CREATE_GOAL_TRACK_DEALS'); ?></div>
        	        </div>
    	        </div>
	        </li>
        	<li>
        	    <div class="goal_container">
            	    <div class="goal_img"><img src="<?php echo JURI::base(); ?>components/com_crmery/media/images/move_deals_forward.png" /></div>
            	    <div class="goal_info_container">
                        <h2><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=goals&layout=edit&type=move_deals'); ?>"><?php echo ucwords(CRMText::_('COM_CRMERY_MOVE_DEALS_FORWARD')); ?></a></h2>
        	            <div class="goal_info"><?php echo CRMText::_('COM_CRMERY_CREATE_GOAL_TRACK_DEAL_STAGES'); ?></div>
        	        </div>
    	        </div>
    	    </li>
        </ul>
    </div>
    <div class="goals_right_column">
        <ul>
            <li>
                <div class="goal_container">
                    <div class="goal_img"><img src="<?php echo JURI::base(); ?>components/com_crmery/media/images/complete_more_tasks.png" /></div>
                    <div class="goal_info_container">
                        <h2><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=goals&layout=edit&type=complete_tasks'); ?>"><?php echo ucwords(CRMText::_('COM_CRMERY_COMPLETE_TASKS')); ?></a></h2>
                        <div class="goal_info"><?php echo CRMText::_('COM_CRMERY_CREATE_GOAL_TRACK_TASKS'); ?></div>
                    </div>
                </div>
            </li>
            <li>
                <div class="goal_container">
                    <div class="goal_img"><img src="<?php echo JURI::base(); ?>components/com_crmery/media/images/write_more_notes.png" /></div>
                    <div class="goal_info_container">
                        <h2><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=goals&layout=edit&type=write_notes'); ?>"><?php echo ucwords(CRMText::_('COM_CRMERY_WRITE_NOTES')); ?></a></h2>
                        <div class="goal_info"><?php echo CRMText::_('COM_CRMERY_CREATE_GOAL_TRACK_NOTES'); ?></div>
                    </div>
                </div>
            </li>
            <li>
                <div class="goal_container">
                    <div class="goal_img"><img src="<?php echo JURI::base(); ?>components/com_crmery/media/images/create_deals.png" /></div>
                    <div class="goal_info_container">
                        <h2><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=goals&layout=edit&type=create_deals'); ?>"><?php echo ucwords(CRMText::_('COM_CRMERY_CREATE_DEALS')); ?></a></h2>
                        <div class="goal_info"><?php echo CRMText::_('COM_CRMERY_CREATE_GOAL_TRACK_DEALS_CREATED'); ?></div>
                    </div>
                </div>
            </li>
        </ul>    
    </div>
</div>
