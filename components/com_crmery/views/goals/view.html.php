<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewGoals extends JView
{
	function display($tpl = null)
	{

	    //edit layout
	    if ( $this->getLayout() == 'edit' ){
	        
            //determine the type of goal we are creating//editing
            $type = JRequest::getVar('type');
            switch($type){
                case "win_cash":
                    $header = ucwords(CRMText::_('COM_CRMERY_WIN_MORE_CASH'));
                    break;
                case "win_deals":
                    $header = ucwords(CRMText::_('COM_CRMERY_WIN_MORE_DEALS'));
                    break;
                case "move_deals";
                    $header = ucwords(CRMText::_('COM_CRMERY_MOVE_DEALS_FORWARD'));
                    break;
                case "complete_tasks";
                    $header = ucwords(CRMText::_('COM_CRMERY_COMPLETE_TASKS'));
                    break;
                case "write_notes";
                    $header = ucwords(CRMText::_('COM_CRMERY_WRITE_NOTES'));
                    break;
                case "create_deals";
                    $header = ucwords(CRMText::_('COM_CRMERY_CREATE_DEALS'));
                    break;
                default:
                    if ( JRequest::getVar('id') ){
                        $model = JModel::getInstance('Goal','CrmeryModel');
                        $this->goal = $model->getGoal();
                        $this->type = $this->goal['goal_type'];
                        $header = ucwords(CRMText::_('COM_CRMERY_'.strtoupper($this->goal['goal_type'])));
                    }else{
                        JApplication::redirect('index.php?option=com_crmery&view=goals');
                    }
                break;   
            }

	    } else if ( $this->getLayout() != 'add' ) {
	        
            //load model
            $model =& JModel::getInstance('goal','CrmeryModel');
            
            //get all goals from model depending on user type
            $member_role = CrmeryHelperUsers::getRole();
            
            //basic members
            if ( $member_role == 'basic' ){
                $individual_goals = $model->getIndividualGoals();
                $team_goals = $model->getTeamGoals();
                $company_goals = $model->getCompanyGoals();
                $leaderboards = $model->getLeaderBoards();
            }
            
            //managers
            if ( $member_role == 'manager' ){
                // $individual_goals = $model->getManagerIndividualGoals();
                $individual_goals = $model->getIndividualGoals();
                $team_goals = $model->getTeamGoals();
                $company_goals = $model->getCompanyGoals();
                $leaderboards = $model->getLeaderBoards();
            }
        
            //executives
            if ( $member_role == 'exec' ){
                // $individual_goals = $model->getExecIndividualGoals();
                $individual_goals = $model->getIndividualGoals();
                // $team_goals = $model->getExecTeamGoals();
                $team_goals = $model->getTeamGoals();
                $company_goals = $model->getCompanyGoals();
                $leaderboards = $model->getLeaderBoards();
            }
            
            //assign goals to global goal object to pass through to view   
            $goals = new stdClass();
            $goals->individual_goals = $individual_goals;
            $goals->team_goals = $team_goals;
            $goals->company_goals = $company_goals;
            $goals->leaderboards = $leaderboards;
            
            //if we get results then load the default goals page else show the add goals page
            $goal_count = false;
            foreach ( $goals as $goal_list ){
                if ( count($goal_list) > 0 ){
                    $goal_count = true;
                }
            }
            if ( $goal_count ){
                //set layout    
                $this->setLayout('default');
                //assign view refs
                $this->assignRef('goals',$goals);
            }else{
                //add goal layout
                $this->setLayout('add');
            }
	    }

        //load java libs
        $doc = JFactory::getDocument();
        $doc->addScript( JURI::base().'components/com_crmery/media/js/goal_manager.js' );

        //get associated members and teams
        $teams = CrmeryHelperUsers::getTeams();
        $users = CrmeryHelperUsers::getUsers();
        $member_role = CrmeryHelperUsers::getRole();
        $user_id = CrmeryHelperUsers::getUserId();
        $team_id = CrmeryHelperUsers::getTeamId();
        
        //assign view refs
        $this->assignRef('type',$type);
        $this->assignRef('header',$header);
        $this->assignRef('teams',$teams);
        $this->assignRef('users',$users);
        $this->assignRef('user_id',$user_id);
        $this->assignRef('team_id',$team_id);
        $this->assignRef('member_role',$member_role);
        $this->assignRef('leaderboard_list',CrmeryHelperDropdown::getLeaderBoards());

        $goalFloats = CrmeryHelperUsers::getFloats('goal');
        $goal_floats_left = $goalFloats['left'];
        $goal_floats_right = $goalFloats['right'];
        $this->assignRef('goal_floats_left',$goal_floats_left);
        $this->assignRef('goal_floats_right',$goal_floats_right);

	    //display
		parent::display($tpl);
	}
	
}
?>
