<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewImport extends JView
{
	function display($tpl = null)
	{

		//Load java libs
		$document =& JFactory::getDocument();
		$document->addScript(JURI::base().'components/com_crmery/media/js/import_manager.js');

		if ( count($_FILES) > 0 ){
			$model =& JModel::getInstance("import",'CrmeryModel');
			foreach ( $_FILES as $file ){
				$import_data = $model->readCSVFile($file['tmp_name']);
			}
			$this->assignRef('headers',$import_data['headers']);
			unset($import_data['headers']);
			$this->assignRef('import_data',$import_data);

			if ( count($import_data) > 0 ){
				switch ( JRequest::getVar('import_type') ){
					case "companies":
						$view = "companies";
						$import_model = "company";
					break;
					case "people":
						$view = "people";
						$import_model = "people";
					break;
					case "deals":
						$view = "deals";
						$import_model = "deal";
					break;
				}

				$success = $model->importCSVData($import_data,$import_model);

				if ( $success == "redirect" ){
					$app = JFactory::getApplication();
					$app->redirect("index.php?option=com_crmery&controller=import&task=batch");
				}
				
				if ( $success == true ){
					$success = "SUCCESSFULLY";
				}else{
					$success = "UNSUCCESSFULLY";
					$view = "import&import_type=".JRequest::getVar('import_type');
				}
				
				$app =& JFactory::getApplication();
				$msg = CRMText::_('COM_CRMERY_'.$success.'_IMPORTED_ITEMS');
				$app->redirect(JRoute::_('index.php?option=com_crmery&view='.$view),$msg);
			}

			$doc =& JFactory::getDocument();
         	$doc->addScriptDeclaration('import_length='.count($import_data).';');

		}

		$import_type = JRequest::getVar('import_type');
		$import_header = ucwords(CRMText::_('COM_CRMERY_IMPORT_'.$import_type));
		$this->assignRef('import_type',$import_type);
		$this->assignRef('import_header',$import_header);

	    //display
		parent::display($tpl);
	}
	
}
?>
