<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<h1><?php echo ucwords($this->import_header); ?></h1>
<div class="step">
	<div class="title">
		<?php echo CRMText::_('COM_CRMERY_STEP_ONE'); ?>
	</div>
	<div class="text">
		<h2><?php echo CRMText::_('COM_CRMERY_EXPORT_YOUR_FILE'); ?></h2>
		<p><?php echo CRMText::_('COM_CRMERY_EXPORT_YOUR_FILE_INSTRUCTIONS'); ?></p>
	</div>
</div>
<div class="step">
	<div class="title">
		<?php echo CRMText::_('COM_CRMERY_STEP_TWO'); ?>
	</div>
	<div class="text">
		<h2><?php echo CRMText::_('COM_CRMERY_ENSURE_YOUR_FILE_IS_FORMATTED'); ?></h2>
			<p><?php echo CRMText::_('COM_CRMERY_ENSURE_YOUR_FILE_IS_FORMATTED_INSTRUCTIONS'); ?>
			<form id="download_import_template" method="POST">
				<p><a href="javascript:void(0);" class="button" onclick="downloadImportTemplate()"><?php echo CRMText::_('COM_CRMERY_DOWNLOAD_TEMPLATE'); ?></a></p>
				<input type="hidden" name="template_type" value="<?php echo $this->import_type; ?>" />
			</form>
	</div>
</div>
<div class="step">
	<div class="title">
		<?php echo CRMText::_('COM_CRMERY_STEP_THREE'); ?>
	</div>
	<div class="text">
		<h2><?php echo CRMText::_('COM_CRMERY_UPLOAD_YOUR_FILE'); ?></h2>
		<p><?php echo CRMText::_('COM_CRMERY_SELECT_YOUR_CSV'); ?>
			<form id="upload_form" action="index.php?option=com_crmery&view=import&layout=review" method="POST" enctype="multipart/form-data">
	        <div class="input_upload_button" >
	        	<input type="hidden" name="type" value="people" />
	            <a href="javascript:void(0);" type="button" class="button" id="upload_button" ><?php echo CRMText::_('COM_CRMERY_UPLOAD_FILE'); ?></a>
	            <input type="hidden" name="import_type" value="<?php echo $this->import_type; ?>" />
	            <input type="file" id="upload_input_invisible" name="document" />
	        </div>
	        </form>
    	</p>
	</div>
</div>
