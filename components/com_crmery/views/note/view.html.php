<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewNote extends CrmeryHelperView
{
	function display($tpl = null)
	{
		
		$type = JRequest::getVar('type');
		$id = JRequest::getVar('id');
		$view = JRequest::getVar('view');

        //retrieve task list from model
        $model = &$this->getModel();

        switch ( $view ){
          case "companies":
              $view = "company";
          break;
          case "deals":
              $view = "deal";
          break;
          case "events":
              $view = "event";
          break;
        }
    
       	$notes = $model->getNotes($id,$view,FALSE);
       	$this->assignRef('notes',$notes);
         
      	//display
       	parent::display($tpl);
	}
	
}
?>
