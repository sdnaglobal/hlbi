<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

?>
<form id="note_edit" method="POST" action="<?php echo 'index.php?option=com_crmery&controller=main&model=company&return=companies&task=save'; ?>" onsubmit="return save(this)" >
	<div id="editForm">
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_CATEGORY'); ?></div>
			<div class="crmeryValue">
				<select data-native-menu="false" data-overlay-theme="a" data-theme="c" name="category_id" tabindex="-1">
					<?php 
                        $categories = CrmeryHelperEvent::getCategories();
                        echo JHtml::_('select.options', $categories, 'value', 'text', "", true);
                    ?>
				</select>
			</div>	
		</div>
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_CONTENT'); ?></div>
			<div class="crmeryValue">
				<textarea class="inputbox" name="note"></textarea>
			</div>	
		</div>
		<input data-theme="c" type="button" name="submit" onclick="addNoteEntry('note_edit');"  value="<?php echo CRMText::_('COM_CRMERY_SUBMIT'); ?>" />
			
	</div>
</form>