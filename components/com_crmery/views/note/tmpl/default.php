<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<?php $view = JRequest::getVar('view'); ?>
<?php if ( $view!="print" ){ ?>
<form id="note" name="note">
	<input type="hidden" name="<?php echo JRequest::getVar('type'); ?>_id" value="<?php echo JRequest::getVar('id'); ?>" />
	<div id="note_entry_area" class="note_entry_area">
		<div class="large_info">
			<div class="message">
				<div class="info">
					<span class="message" id="edit_note_message"><?php echo CRMText::_('COM_CRMERY_EDIT_NOTES_MESSAGE'); ?></span>
				</div>
				<textarea class="hidden" id="deal_note" name="note"></textarea>
			</div>
		</div>
	</div>
	<div id="note_details_area"></div>
</form>
<div class="actions_container">
	<span class="actions"><a class="button" id="add_note_entry_button"><?php echo CRMText::_('COM_CRMERY_ADD_NOTE_BUTTON'); ?></a>
</div>
<?php } ?>
<div id="note_entries">
<?php
	$raw = JRequest::getVar('format');
	$c = count($this->notes);
		$limit = ( $c > 3 && $raw ) ? 3 : $c;
		for ( $i=0; $i<$limit; $i++ ) {
			$note = $this->notes[$i];
			$view = CrmeryHelperView::getView('note','entry',array(array('ref'=>'note','data'=>$note)));
			$view->display();
		} 
?>
</div>