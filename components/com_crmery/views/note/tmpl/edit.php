<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$note = $this->notes[0];
?>
<form id="note_edit" method="POST" action="<?php echo 'index.php?option=com_crmery&controller=main&model=company&return=companies&task=save'; ?>" onsubmit="return save(this)" >
	<input type="hidden" name="id" value="<?php echo $note['id']; ?>" />
	<div id="editForm">
		<?php if ( array_key_exists('person_id', $note) && $note['person_id'] != 0 ){ ?>
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_PERSON')); ?></div>
			<div class="crmeryValue">
				<select class="inputbox" name="person_id">
				<?php 
                    $people = CrmeryHelperDropdown::getPeopleList();
                    echo JHtml::_('select.options', $people, 'value', 'text', $note['person_id'], true);
                ?>
				</select>
			</div>
		</div>
		<?php } ?>
		<?php if ( array_key_exists('deal_id', $note) && $note['deal_id'] != 0 ){ ?>
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_DEAL')); ?></div>
			<div class="crmeryValue">
				<?php echo CrmeryHelperDropdown::generateDropdown('deal',$note['deal_id']); ?>
			</div>
		</div>
		<?php } ?>
		<?php if ( array_key_exists('company_id', $note) && $note['company_id'] != 0 ){ ?>
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY')); ?></div>
			<div class="crmeryValue">
				<?php echo CrmeryHelperDropdown::generateDropdown('company',$note['deal_id']); ?>
			</div>
		</div>
		<?php } ?>
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_CATEGORY')); ?></div>
			<div class="crmeryValue">
				<select class="inputbox" name="category_id">
					<?php 
                        $categories = CrmeryHelperNote::getCategories();
                        echo JHtml::_('select.options', $categories, 'value', 'text', $note['category_id'], true);
                    ?>
				</select>
			</div>	
		</div>
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_CREATED_ON'); ?></div>
			<div class="crmeryValue">
				<span class="date"><?php echo CrmeryHelperDate::formatDate($note['created']). ' '.CrmeryHelperDate::formatTime($note['created']); ?></span>
			</div>	
		</div>
		<div class="crmeryRow">
			<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_CONTENT'); ?></div>
			<div class="crmeryValue">
				<textarea class="inputbox" name="note"><?php echo $note['note']; ?></textarea>
			</div>	
		</div>
		<div class="actions"><a href="javascript:void(0);" onclick="addNoteEntry('note_edit');" class="button"><?php echo CRMText::_('COM_CRMERY_SAVE_BUTTON'); ?></a><a href="javascript:void(0);" onclick="window.top.window.jQuery('.ui-dialog-content').dialog('close');"><?php echo CRMText::_('COM_CRMERY_CANCEL_BUTTON'); ?></a></div>
	</div>
</form>