<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$note = $this->note;
echo '<div id="note_entry_'.$note['id'].'" class="note_container">';
	echo '<div class="header"><span class="dateDisplay"><b>'.CrmeryHelperDate::formatDate($note['created']).'</b></span>';
	echo '<b>'.$note['owner_first_name'].' '.$note['owner_last_name'].'</b> ';
	echo CRMText::_('COM_CRMERY_WROTE_NOTE');
	if ( array_key_exists('category_name',$note) && $note['category_name'] !=  "" ){
		echo CRMText::_('COM_CRMERY_IN').'<b> '.$note['category_name'].'</b> ';
	}
	if ( array_key_exists('person_first_name',$note) && $note['person_first_name'] !=  "" ){
		echo CRMText::_('COM_CRMERY_FOR').' <b>'.$note['person_first_name'].' '.$note['person_last_name'].'</b> ';
	}
	if ( array_key_exists('deal_name',$note) && $note['deal_name'] !=  ""  ){
		echo CRMText::_('COM_CRMERY_ON_THE_DEAL').' <b>'.$note['deal_name'].'</b>';
	}
	if ( array_key_exists('event_name',$note) && $note['event_name'] !=  "" ){
		echo CRMText::_('COM_CRMERY_ON_THE_EVENT').' <b>'.$note['event_name'].'</b>';
	}
	echo '</div>';
	if ( CrmeryHelperUsers::isAdmin() || CrmeryHelperUsers::getRole() == 'exec' || $note['owner_id'] == CrmeryHelperUsers::getLoggedInUser()->id ){
		echo '<div class="note_edit_functions"><a href="javascript:void(0);" onclick="editNoteEntry('.$note['id'].')">'.CRMText::_('COM_CRMERY_EDIT').'</a> - <a href="javascript:void(0);" onclick="trashNoteEntry('.$note['id'].')">'.CRMText::_('COM_CRMERY_REMOVE').'</a></div>';
	}
	echo '<div class="note">';
	if ( array_key_exists("name",$note) && $note['name'] != "" ){
		echo "<b>".$note['name'].'</b><br />';
	}
	echo nl2br($note['note']);
	echo '</div>';
echo '</div>';		
?>