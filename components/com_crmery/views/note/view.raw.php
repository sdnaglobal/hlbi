<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewNote extends JView
{
	function display($tpl = null)
	{
		
		$type = JRequest::getVar('type');
		$id = JRequest::getVar('id');
		$layout = $this->getLayout();

		//retrieve task list from model
       	$model = &$this->getModel();

    	if ( $layout == "edit" ){
       		$notes = $model->getNote($id);
       	}else{
       		$notes = $model->getNotes($id,$type, false);
       	}

       	$this->assignRef('notes',$notes);
         
      	//display
       	parent::display($tpl);
	}
	
}
?>
