<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewDocuments extends JView
{
    function display($tpl = null)
    {
         //get model
        $model =& JModel::getInstance('document','CrmeryModel');
        $pagination = $model->getPagination();
        $documents = $model->getDocuments(JRequest::getVar('document_id'));
        $state = $model->getState();
        $layout = $this->getLayout();

        if ( JRequest::getVar('document_id') && $layout != "document_row" ){
            $documents = $documents[0];
        }

        //assign refs
        $this->assignRef('pagination',$pagination);
        $this->assignRef('documents',$documents);
        $this->assignRef('state',$state);
        
        //display view
        parent::display($tpl);      
    }
    
}
        
        