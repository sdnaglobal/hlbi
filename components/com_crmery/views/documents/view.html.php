<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewDocuments extends JView
{
	function display($tpl = null)
	{
	       
                $layout = $this->getLayout();

                //get model
                $model =& JModel::getInstance('document','CrmeryModel');
                $pagination = $model->getPagination();
                $documents = $model->getDocuments();
                $state = $model->getState();
                
                //add js
                $document = & JFactory::getDocument();
                $document->addScript( JURI::base().'components/com_crmery/media/js/document_manager.js' );
                
                //session data
                $session = JFactory::getSession();
                $member_role = CrmeryHelperUsers::getRole();
                $user_id = CrmeryHelperUsers::getUserId(); 
                
                //associations
                $assoc = $session->get('document_assoc_filter');
                $assoc_names = CrmeryHelperDocument::getAssocTypes();
                $assoc_name = ( $assoc ) ? $assoc_names[$assoc] : $assoc_names['all'];
                
                //users
                $user_id = CrmeryHelperUsers::getUserId();
                $user = $session->get('document_user_filter');
                $team = $session->get('document_team_filter');

                if ( $user == "all" ){
                    $user_name = CRMText::_('COM_CRMERY_ALL_USERS');
                }else if ( $user && $user != $user_id ){
                    $user_info = CrmeryHelperUsers::getUser($user);
                    $user_name = $user_info->first_name . " " . $user_info->last_name;
                }else if ( $team ){
                    $team_info = CrmeryHelperUsers::getTeams($team);
                    $team_info = $team_info[0];
                    $user_name = $team_info['team_name'] . CRMText::_('COM_CRMERY_TEAM_APPEND');
                }else{
                    $user_name = CRMText::_('COM_CRMERY_ME');            
                }
                
                //type
                $type = $session->get('document_type_filter');
                $type_names = CrmeryHelperDocument::getDocTypes();
                $type_name = ( $type && array_key_exists($type,$type_names) ) ? $type_names[$type] : $type_names['all'];
                
                //teams
                $teams = CrmeryHelperUsers::getTeams();
                //users
                $users = CrmeryHelperUsers::getUsers();

                //list view
                $data = array(
                        array('ref'=>'documents','data'=>$documents),
                        array('ref'=>'state','data'=>$state),
                        array('ref'=>'pagination','data'=>$pagination)
                    );
                $document_list = CrmeryHelperView::getView('documents','list',$data);
                
                //assign ref
                $this->assignRef('state',$state);
                $this->assignRef('document_list',$document_list);
                $this->assignRef('assoc_names',$assoc_names);
                $this->assignRef('assoc_name',$assoc_name);
                $this->assignRef('user_name',$user_name);
                $this->assignRef('type_names',$type_names);
                $this->assignRef('type_name',$type_name);
                $this->assignRef('member_role',$member_role);
                $this->assignRef('user_id',$user_id);
                $this->assignRef('teams',$teams);
                $this->assignRef('users',$users);
        
		//display
		parent::display($tpl);
	}
	
}
?>
