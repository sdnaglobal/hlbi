<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<h1><?php echo CRMText::_('COM_CRMERY_EDIT_DOCUMENTS'); ?></h1>
<ul class="filter_lists">
    <li class="filter_sentence">
        <?php echo CRMText::_('COM_CRMERY_SHOW'); ?>
        <span class="filters"><a href="javascript:void(0);" class="dropdown" id="document_assoc_link"><?php echo $this->assoc_name; ?></a></span>
        <div class="filters" id="document_assoc">
            <ul>
                <?php foreach( $this->assoc_names as $title => $text ){
                    echo "<li><a href='javascript:void(0);' class='filter_".$title."' onclick=\"documentAssoc('".$title."')\">".$text."</a></li>";
                }?>
            </ul>
        </div>
        <?php echo CRMText::_('COM_CRMERY_OWNED_BY'); ?>
        <span class="filters"><a href="javascript:void(0);" class="dropdown" id="document_user_link"><?php echo $this->user_name; ?></a></span>
        <div class="filters" id="document_user">
            <ul>
                <li><a href="javascript:void(0);" class="filter_user_<?php echo $this->user_id; ?>" onclick="documentUser(<?php echo $this->user_id; ?>)">Me</a></li>
                <?php
                    if ( $this->member_role == 'exec' ){
                        if ( count($this->teams) > 0 ){
                            foreach($this->teams as $team){
                                 echo "<li><a href='javascript:void(0);' class='filter_team_".$team['team_id']."' onclick='documentUser(0,".$team['team_id'].")'>".$team['team_name'].CRMText::_('COM_CRMERY_TEAM_APPEND')."</a></li>";
                             }
                        }
                    }
                    if ( $this->member_role != 'basic' ){
                ?>
                <li><a href="javascript:void(0);" class="filter_user_all" onclick="documentUser('all')"><?php echo CRMText::_('COM_CRMERY_ALL_USERS'); ?></a></li>
                <?php } ?>
                <?php
                    foreach($this->users as $key => $user){
                        echo "<li><a href='javascript:void(0);' class='filter_user_".$user['id']."' onclick='documentUser(".$user['id'].")' >".$user['first_name'].' '.$user['last_name']."</a></li>";
                    }
                ?>
            </ul>
        </div> 
        <?php echo CRMText::_('COM_CRMERY_THAT_ARE'); ?>
        <span class="filters"><a href="javascript:void(0);" class="dropdown" id="document_type_link"><?php echo $this->type_name; ?></a></span>
        <div class="filters" id="document_type">
            <ul>
                <?php
                    foreach ( $this->type_names as $title => $text ){
                        echo "<li><a href='javascript:void(0);' class='filter_".$title." dropdown_item' onclick=\"documentType('".$title."')\">".$text."</a></li>";                     
                    }?>
            </ul>
        </div>
        <?php echo CRMText::_('COM_CRMERY_NAMED'); ?>
        <input type="text" class="inputbox" placeholder="<?php echo CRMText::_('COM_CRMERY_ANYTHING'); ?>" value="" name="document_name_search" />
    </li>
    <li class="filter_sentence">
        <div class="ajax_loader"></div>
    </li>
</ul>
<div class="subline">
<ul class="matched_results">
<li><span id="documents_matched"></span> <?php echo CRMText::_('COM_CRMERY_DOCUMENT_MATCHES'); ?>. <?php echo CRMText::_('COM_CRMERY_DOCUMENT_THERE_ARE'); ?> <span id="documents_total"><?php echo CrmeryHelperDocument::getTotalDocuments(); ?></span> <?php echo CRMText::_('COM_CRMERY_DOCUMENT_IN_YOUR_ACCOUNT'); ?>.</li>
</ul>
</div>
<form method="post" id="list_form" action="<?php echo JRoute::_('index.php?option=com_crmery&view=documents'); ?>">
    <table class="com_crmery_table" id="documents">
       <?php echo $this->document_list->display(); ?>
    </table>
    <input type="hidden" name="list_type" value="documents" />
</form>