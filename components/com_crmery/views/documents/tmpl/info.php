<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="validate" id="edit_document_<?php echo $this->document['id']; ?>_form">
	<input type="hidden" name="task" value="save" />
	<input type="hidden" name="type" value="document" />
	<input type="hidden" name="id" value="<?php echo $this->document['id']; ?>" />
	<div class="crmeryRow">
		<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DOCUMENT_NAME'); ?><span class="required">*</span></div>
		<div class="crmeryValue"><input class="required inputbox" type="text" name="name" value="<?php if ( array_key_exists('name',$this->document) ) echo $this->document['name']; ?>"/></div>
	</div>
	<div class="crmeryRow">
		<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DOCUMENT_UPLOADED'); ?></div>
		<div class="crmeryValue"><?php if ( array_key_exists('created',$this->document) ) echo CrmeryHelperDate::formatDate($this->document['created']); ?></div>
	</div>
	<div class="crmeryRow">
		<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DOCUMENT_SIZE'); ?></div>
		<div class="crmeryValue"><?php if ( array_key_exists('size',$this->document) ) echo $this->document['size']."Kb"; ?></div>
	</div>
	<div class="crmeryRow">
		<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_DOCUMENT_DESCRIPTION'); ?></div>
		<div class="crmeryValue"><textarea class="inputbox" name="description" ><?php if ( array_key_exists('description',$this->document) ) echo $this->document['description']; ?></textarea>
	</div>
</div>

<div class="actions">
	<a href="javascript:void(0);" class="button" onclick="saveAjax('edit_document_<?php echo $this->document['id']; ?>','document');" ><?php echo CRMText::_('COM_CRMERY_SAVE_BUTTON'); ?></a>
	<?php echo CRMText::_('COM_CRMERY_OR'); ?>
	<a href="javascript:void(0);" onclick="closeDocumentInfo(<?php echo $this->document['id']; ?>)"><?php echo CRMText::_('COM_CRMERY_CLOSE'); ?></a>
</div>