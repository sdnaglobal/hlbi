<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
    if ( count($this->documents) > 0 ){
        foreach( $this->documents as $key => $document ){
            $k = $key%2;
            echo '<tr class="document_'.$key.'" id="document_row_'.$document['id'].'" class="crmery_row_'.$k.'">';
                $imageFile = JFile::exists(JPATH_BASE.'/components/com_crmery/media/images/'.$document['filetype'].'.png') ? JURI::base().'components/com_crmery/media/images/'.$document['filetype'].'.png' : JURI::base().'components/com_crmery/media/images/generic_file.png';
                echo '<td><img width="30px" height="30px" src="'.$imageFile.'" /><br /><b>'.strtoupper($document['filetype']).'<b></td>';
                echo '<td><a href="javascript:void(0);" class="document_edit" id="'.$document['id'].'">'.$document['name'].'</a></td>';
                echo '<td>'.$document['owner_name'].'</td>';
                echo '<td>'.$document['size'].'Kb</td>';
                echo '<td>'.CrmeryHelperDate::formatDate($document['created']);
                    echo '<div class="document_edit_menu" id="document_'.$document['id'].'">';
                        echo '<input type="hidden" name="document_'.$document['id'].'_hash" id="document_'.$document['id'].'_hash" value="'.$document['filename'].'" />';
                        echo '<ul>';
                            echo '<li>';
                                echo '<a href="javascript:void(0);" class="document_info" id="info_'.$document['id'].'">'.CRMText::_('COM_CRMERY_DOCUMENT_INFO').'</a>';
                                echo '<div style="display:none;" id="document_info_modal_'.$document['id'].'">';
                                    $view = CrmeryHelperView::getView('documents','info');
                                    $view->document = $document;
                                    $view->display();
                                echo '</div>';  
                            echo '</li>';
                            if ( $document['is_image'] ){
                                echo '<li><a href="javascript:void(0);" class="document_preview" id="preview_'.$document['id'].'">'.CRMText::_('COM_CRMERY_PREVIEW').'</a></li>';
                            } else { 
                                echo '<li><a target="_blank" href="index.php?option=com_crmery&controller=ajax&task=previewDocument&format=raw&tmpl=component&document='.$document['filename'].'">'.CRMText::_('COM_CRMERY_PREVIEW').'</a></li>';
                            }
                            echo '<li><a href="javascript:void(0);" class="document_download" id="download_'.$document['id'].'">'.CRMText::_('COM_CRMERY_DOWNLOAD').'</a></li>';
                            echo '<li><a href="javascript:void(0);" class="document_delete" id="delete_'.$document['id'].'">'.CRMText::_('COM_CRMERY_DELETE').'</a></li>';
                        echo '</ul>';
                    echo '</div>';
                echo '</td>';
            echo '</tr>';
        }
    }
?>