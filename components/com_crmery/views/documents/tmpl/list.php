<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<thead>
    <th><div class="sort_order"><a href="javascript:void(0);" class="d.type" onclick="sortTable('d.type',this)"><?php echo CRMText::_('COM_CRMERY_DOCUMENT_TYPE'); ?></a></div></th>
    <th><div class="sort_order"><a href="javascript:void(0);" class="d.filename" onclick="sortTable('d.filename',this)"><?php echo CRMText::_('COM_CRMERY_DOCUMENT_NAME'); ?></a></div></th>
    <th><?php echo CRMText::_('COM_CRMERY_DOCUMENT_ASSOCIATION'); ?></th>
    <th><div class="sort_order"><a href="javascript:void(0);" class="p.last_name" onclick="sortTable('p.last_name',this)"><?php echo CRMText::_('COM_CRMERY_DOCUMENT_OWNER'); ?></a></div></th>
    <th><?php echo CRMText::_('COM_CRMERY_DOCUMENT_SIZE'); ?></th>
    <th><div class="sort_order"><a href="javascript:void(0);" class="d.created" onclick="sortTable('d.created',this)"><?php echo CRMText::_('COM_CRMERY_DOCUMENT_UPLOADED'); ?></a></div></th>
</thead>
<tbody class="results">
<?php
    if ( count($this->documents) > 0 ){
        foreach( $this->documents as $key => $document ){
            $k = $key%2;
            
            //assign association link
            switch($document['association_type']){
                case "deal":
                    $view = 'deals';
                    $association_type = "deal";
                    $document['association_name'] = $document['deal_name'];
                    break;
                case "person":
                    $view = "people";
                    $association_type = "person";
                    $document['association_name'] = $document['first_name']." ".$document['last_name'];
                    break;
                case "company";          
                    $view = "companies";
                    $association_type = "company";
                    $document['association_name'] = $document['company_name'];
                    break;
            }
            if(array_key_exists('association_name',$document) AND $document['association_name'] != null){
                $association_link = '<a href="'.JRoute::_('index.php?option=com_crmery&view='.$view.'&layout='.$association_type.'&id='.$document['association_id']).'" >'.$document['association_name'];
            }else{
                $association_link = "";
            }
            
            echo '<tr class="document_'.$key.'" id="document_row_'.$document['id'].'" class="crmery_row_'.$k.'">';
                $imageFile = JFile::exists(JPATH_BASE.'/components/com_crmery/media/images/'.$document['filetype'].'.png') ? JURI::base().'components/com_crmery/media/images/'.$document['filetype'].'.png' : JURI::base().'components/com_crmery/media/images/generic_file.png';
                echo '<td><img width="30px" height="30px" src="'.$imageFile.'" /><br /><b>'.strtoupper($document['filetype']).'<b></td>';
                echo '<td><a href="javascript:void(0);" class="document_edit" id="'.$document['id'].'">'.$document['name'].'</a></td>';
                echo '<td>'.$association_link.'</a></td>';
                echo '<td>'.$document['owner_name'].'</td>';
                echo '<td>'.$document['size'].'Kb</td>';
                echo '<td>'.CrmeryHelperDate::formatDate($document['created']).'</td>';
            echo '</tr>';
            echo '<div class="document_edit_menu" id="document_'.$document['id'].'">';
                echo '<ul>';
                    echo '<li>';
                        echo '<a href="javascript:void(0);" class="document_info" id="info_'.$document['id'].'">'.CRMText::_('COM_CRMERY_DOCUMENT_INFO').'</a>';
                        echo '<div style="display:none;" id="document_info_modal_'.$document['id'].'">';
                            $view = CrmeryHelperView::getView('documents','info');
                            $view->document = $document;
                            $view->display();
                        echo '</div>';  
                    echo '</li>';
                    echo '<input type="hidden" name="document_'.$document['id'].'_hash" id="document_'.$document['id'].'_hash" value="'.$document['filename'].'" />';
                    if ( $document['is_image'] ){
                        echo '<li><a href="javascript:void(0);" class="document_preview" id="preview_'.$document['id'].'">'.CRMText::_('COM_CRMERY_PREVIEW').'</a></li>';
                    } else { 
                        echo '<li><a target="_blank" href="index.php?option=com_crmery&controller=ajax&task=previewDocument&format=raw&tmpl=component&document='.$document['filename'].'">'.CRMText::_('COM_CRMERY_PREVIEW').'</a></li>';
                    }
                    echo '<li><a href="javascript:void(0);" class="document_download" id="download_'.$document['id'].'">'.CRMText::_('COM_CRMERY_DOWNLOAD').'</a></li>';
                    echo '<li><a href="javascript:void(0);" class="document_delete" id="delete_'.$document['id'].'">'.CRMText::_('COM_CRMERY_DELETE').'</a></li>';
                echo '</ul>';
            echo '</div>';
        }
    }
?>
</tbody>
<tfoot>
    <tr>
       <td colspan="20"><?php echo $this->pagination->getListFooter(); ?></td>
    </tr>
</tfoot>
<script type="text/javascript">
    jQuery("#documents_matched").html('<?php echo count($this->documents); ?>');
    var documents = <?php echo json_encode($this->documents); ?>;
    var document_names = <?php
                                //assign document names for autocomplete dialogs
                                $names = array();

                                if ( count($this->documents)> 0 ){
                                    foreach ( $this->documents as $key => $document ){
                                        $names[] = $document['name'];
                                    }
                                }
    
                                 echo json_encode($names); 
                            ?>;
    order_url = "<?php echo 'index.php?option=com_crmery&view=documents&layout=list&format=raw&tmpl=component'; ?>";
    order_dir = "<?php echo $this->state->get('Document.filter_order_Dir'); ?>";
    order_col = "<?php echo $this->state->get('Document.filter_order'); ?>";
</script>