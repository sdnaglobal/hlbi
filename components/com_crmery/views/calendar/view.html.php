<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewCalendar extends JView
{
	function display($tpl = null)
	{
		//load model and retrieve events to pass to calendar
		$model =& JModel::getInstance('event','CrmeryModel');
		$events = $model->getEvents('calendar');
		
		//load js libs
		$document =& JFactory::getDocument();
		$document->addScript( JURI::base().'components/com_crmery/media/js/fullcalendar.js' );
		$document->addScript( JURI::base().'components/com_crmery/media/js/calendar_manager.js' );
		
		//load required css for calendar
		$document->addStyleSheet( JURI::base().'components/com_crmery/media/css/fullcalendar.css' );
		
		//pass reference vars to view
		$this->assignRef('events',json_encode($events));
		$team_members = CrmeryHelperUsers::getUsers();
		$this->assignRef('team_members',$team_members);
		
		//display
		parent::display($tpl);
	}
	
}
?>
