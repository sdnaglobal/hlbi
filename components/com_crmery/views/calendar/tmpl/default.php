<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<script type="text/javascript">
	var eventsObj = <?php echo $this->events; ?>;
	var loc = 'calendar';	
</script>

<form class="print_form" method="POST" target="_blank" action="<?php echo JRoute::_('index.php?option=com_crmery&view=print'); ?>">
    <div class="actions_container">
    <span class="calendar_actions">
        <input type="hidden" name="layout" value="calendar" />
        <input type="hidden" name="model" value="event" />
        <a id="actions_button" type="button" /><?php echo CRMText::_('COM_CRMERY_SHOW'); ?></a>
        <div id="actions">
            <ul>
                <li><a href="javascript:void(0);" onclick="showCalendarTasks()"><?php echo CRMText::_('COM_CRMERY_SHOW_TASKS'); ?></a></li>
                <li><a href="javascript:void(0);" onclick="showCalendarEvents()"><?php echo CRMText::_('COM_CRMERY_SHOW_EVENTS'); ?></a></li>
                <li><a href="javascript:void(0);" onclick="showAllCalendarEvents()"><?php echo CRMText::_('COM_CRMERY_SHOW_TASKS_EVENTS'); ?></a></li>
            </ul>
        </div>
        <a href="<?php echo JRoute::_('index.php?option=com_crmery&view=events'); ?>" ><?php echo CRMText::_('COM_CRMERY_CALENDAR_SHOW_ALL'); ?></a> 
    </span>
    </div>
</form>

<h1><?php echo CRMText::_('COM_CRMERY_CALENDAR_HEADER'); ?></h1>

<div id="calendar"></div>

<div id="team_members">
    <?php if(count($this->team_members)>0){
        echo '<ul id="team_member_calendar_filter">';
        foreach ( $this->team_members as $team_member){
            echo '<li style="background:#'.$team_member['color'].' !important;"><input type="checkbox" onclick="toggleTeamMemberEvents('.$team_member['id'].');" /><span>'.$team_member['first_name'].' '.$team_member['last_name'].'</span></li>';
        }
        echo '</ul>';
    }?>
</div>

<div id="templates" style="display:none;">


    <div id="addTaskEvent" >
        <ul>
            <li><a href="javascript:void(0);" onclick="addTaskEvent('task');"><?php echo CRMText::_('COM_CRMERY_ADD_TASK'); ?></a></li>
            <li><a href="javascript:void(0);" onclick="addTaskEvent('event');"><?php echo CRMText::_('COM_CRMERY_ADD_EVENT'); ?></a></li>
        </ul>
    </div>

    <div id="edit_menu" class="edit_menu" >
        <ul>
            <li><a href="javascript:void(0);" class="edit_event_button"><?php echo CRMText::_('COM_CRMERY_EDIT'); ?></a></li>
            <li><a href="javascript:void(0);" class="remove_event_button"><?php echo CRMText::_('COM_CRMERY_REMOVE_EVENT'); ?></a></li>
            <li><a href="javascript:void(0);" class="remove_event_series_button"><?php echo CRMText::_('COM_CRMERY_REMOVE_SERIES'); ?></a></li>
            <li><a href="javascript:void(0);" class="complete_event_button"><?php echo CRMText::_('COM_CRMERY_MARK_COMPLETE'); ?></a></li>
            <li><a href="javascript:void(0);" style="display:none;" class="show_event_association"></a></li>
        </ul>
    </div>

</div>
