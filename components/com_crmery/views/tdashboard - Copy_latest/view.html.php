<?php

/*------------------------------------------------------------------------

# CRMery

# ------------------------------------------------------------------------

# @author CRMery

# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.

# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Website: http://www.crmery.com

-------------------------------------------------------------------------*/

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' ); 



jimport( 'joomla.application.component.view');



class CrmeryViewTdashboard extends CrmeryHelperView

{



	function display($tpl = null)
	{
	
		$yyear = date("Y");
		
		$model = & JModel::getInstance('charts','CrmeryModel');
		
		$firm_ids = $model->get_user_companies();
		
		$receiving_firm_ids = $model->get_receiving_companies($firm_ids);
		
		
		
		$member_role = CrmeryHelperUsers::getRole();
	
		$current_financial_year_pure = CrmeryHelperCrmery::getFinancialYear();
		$current_financial_date = explode("-",$current_financial_year_pure);
		$current_financial_year = $current_financial_date[0];
				
		$financial_year = date('d/m/Y', strtotime($current_financial_year_pure));
		
		$billing_year = $current_financial_year_pure;
		$curr_date = date("Y/m/d");
		 
		$billing_year = strtotime($billing_year);
		$curr_date = strtotime($curr_date);
		
		
		$firm_total = 0;
		$ref_total = 0;
		$country_turnover = 0;
		$region_turnover = 0;
		$years = '';
		$reffered_firm_total_amount = 0;
		
		//	GET SERVICE TYPE
		//$serviceType = $model->getServiceType($referrals_send);
		//echo "<pre>"; print_r($referrals_send); die;		
		
		
		$firm_ids_from_region = $model->get_firm_ids_from_region($firm_ids,'');
		
		$total_referred_from_region = $model->get_total_referred_from_region($firm_ids_from_region);
		
		//	TOTAL NUMBER OF REFERRALS REFERRED BY COUNTRY BELONGS TO LOGGED IN FIRM
		$referralsReferredByCountry = $model->getReferralsReferredByCountry($firm_ids);
		
		//	TOTAL NUMBER OF REFERRALS RECEIVED BY COUNTRY BELONGS TO LOGGED IN FIRM
		$referalsReceivedByCountry = $model->getReferralsReceivedByCountry($firm_ids);
		
		
		//	WORK REFERRED BY SOURCE
		$workReferredByReferral = $model->getWorkReferredBySource($firm_ids,'4');
		$workReferredByHlb = $model->getWorkReferredBySource($firm_ids,'8');
		$workReferredByBusiness = $model->getWorkReferredBySource($firm_ids,'7');
		
		//	WORK RECEIVED BY SOURCE
		$workReceivedByReferral = $model->getWorkReceivedBySource($firm_ids,'4');
		$workReceivedByHlb = $model->getWorkReceivedBySource($firm_ids,'8');
		$workReceivedByBusiness = $model->getWorkReceivedBySource($firm_ids,'7');
				
		
		//	FIRM AMOUNT REFERRED BY REFERRALS 
		$referrals_send = $model->get_send_referrals_details_by_firm_ids($firm_ids,$years);
		//echo "<pre>"; print_r($referrals_send); die;
		foreach($referrals_send as $referralid)
		{

				$ref_amount = $model->get_amount_billed_by_referral_id($referralid->id,$year=$current_financial_year);
				$firm_total = ($firm_total+$ref_amount);

		}
		//	BILLED AND UNBILLED REFERRED REFERRALS AMOUNT
		$billed_reffered_amount = $model->get_amount_billed_by_referred_firm($firm_ids);
		$unbilled_referrals =  $firm_total - $billed_reffered_amount ;
		
		
		//	FIRM AMOUNT RECEIVED BY REFERRALS 
		//$referrals_received = $model->get_referrals_details_by_firm_ids($firm_ids,'0');
		foreach($receiving_firm_ids as $received_id)
		{
			$received_referrals = $model->get_amount_billed_by_referral_id($received_id,$year=$current_financial_year);
			$total_received_referrals = ($total_received_referrals + $received_referrals);
	
		}
		
		//	BILLED AND UNBILLED RECEIVED REFERRALS AMOUNT
		$billed_received_referrals = $model->get_amount_billed_by_referred_firm($receiving_firm_ids,$year=$current_financial_year);
		$unbilled_received_referrals =  $total_received_referrals - $billed_received_referrals ;
		
		
		//	RECEIVING FIRM TURNOVER
		$receiving_turnover_fee_income_promotional_gbp = $model->get_fee_income_promotional_gbp($receiving_firm_ids);
		$receiving_turnover_fee_income_promotional_gbp = $receiving_turnover_fee_income_promotional_gbp[0]->total;
		
		
		/*		REFERRED TURN OVER STARTS FROM HERE		*/
		
		//	REFERRED FIRM TURNOVER BILLING AMOUNT
		$turnover_fee_income_promotional_gbp = $model->get_fee_income_promotional_gbp($firm_ids);
		$referred_firm_turnover_fee_income_promotional_gbp = $turnover_fee_income_promotional_gbp[0]->total;
		
		//	firm referred turn over	
		$firm_turnover = $firm_total/$referred_firm_turnover_fee_income_promotional_gbp;
		
		//	country turn over				
		$referral_referred_by_country = $model->get_referral_turnover_by_country($firm_ids);
		foreach($referral_referred_by_country as $referralid)
		{
				$ref_amount = $model->get_amount_billed_by_referral_country($referralid->id);
				$country_turnover = ($country_turnover + $ref_amount);

		}
		//	REFERRED COUNTRY TURNOVER BILLING AMOUNT
		$country_firm_ids = $model->get_referrals_id_referred_by_country($firm_ids);
		$country_turnover_fee_income_promotional_gbp = $model->get_coureg_fee_income_promotional_gbp($country_firm_ids);
		$country_turnover_fee_income_promotional_gbp = $country_turnover_fee_income_promotional_gbp[0]->total;
				
		$country_turnover = $country_turnover/$country_turnover_fee_income_promotional_gbp;
		
		//	region turn over		
		$referral_referred_by_regionids = $model->get_referral_turnover_by_region($firm_ids);
		foreach($referral_referred_by_regionids as $referralid)
		{
				$ref_amount = $model->get_amount_billed_by_referral_region($referralid->id);
				$region_turnover = ($region_turnover + $ref_amount);

		}
		//	REFERRED REGION TURNOVER BILLING AMOUNT
		$region_firm_ids = $model->get_referrals_id_referred_by_region($firm_ids);
		$region_turnover_fee_income_promotional_gbp = $model->get_coureg_fee_income_promotional_gbp($region_firm_ids);
		$region_turnover_fee_income_promotional_gbp = $region_turnover_fee_income_promotional_gbp[0]->total;
		
		$region_turnover = $region_turnover/$region_turnover_fee_income_promotional_gbp;
		
		/*	REFERRED TURN OVER ENDS HERE		*/
		
		
		
		
		/*	RECEIVED TURN OVER STARTS FROM HERE		*/
		
		//	firm received turn over	
		
		if($receiving_turnover_fee_income_promotional_gbp == '')
		{
			$firm_received_turnover = '0';
		}
		else
		{
			$firm_received_turnover = $total_received_referrals/$receiving_turnover_fee_income_promotional_gbp;
		}
		
		
		//	country received turn over				
		$received_referred_by_country = $model->get_received_turnover_by_country($firm_ids);
		foreach($received_referred_by_country as $referralid)
		{
				$ref_amount = $model->get_amount_billed_by_referral_country($referralid->id);
				$country_rec_turnover = ($country_rec_turnover+$ref_amount);

		}
		if($receiving_turnover_fee_income_promotional_gbp == '')
		{
			$country_received_turnover = '0';
		}
		else
		{
			$country_received_turnover = $country_rec_turnover/$receiving_turnover_fee_income_promotional_gbp;
		}
		
		//	region received turn over		
		$received_referred_by_regionids = $model->get_received_turnover_by_region($firm_ids);
		foreach($received_referred_by_regionids as $referralid)
		{
				$ref_amount = $model->get_amount_billed_by_referral_region($referralid->id);
				$region_rec_turnover = ($region_rec_turnover+$ref_amount);

		}
		if($receiving_turnover_fee_income_promotional_gbp == '')
		{
			$region_received_turnover = '0';
		}
		else
		{
			$region_received_turnover = $region_rec_turnover/$receiving_turnover_fee_income_promotional_gbp;
		}
		
		
		
		/*		RECEIVED TURN OVER ENDS HERE		*/
		
		
		
		
		/*		COMPARSION CHARTS FOR REFERRED REFERRALS START FROM HERE		*/
		
		//	firms previous and current data
		$referred_referrals_firmids = $model->get_referred_referrals_firmids($firm_ids);
		foreach($referred_referrals_firmids as $referred_referrals_firmid)
		{
				
				$refferred_firm_previous_year_amount = $model->get_amount_billed_by_referral($referred_referrals_firmid->id,'2013');
				$firm_total_previous_year = ($firm_total_previous_year + $refferred_firm_previous_year_amount);

		}
		//echo "<pre>"; print_r($referred_referrals_firmids); die;
		foreach($referred_referrals_firmids as $referred_referrals_firmid)
		{
				
				$refferred_firm_current_year_amount = $model->get_amount_billed_by_referral($referred_referrals_firmid->id,'2014');
				$firm_total_current_year = ($firm_total_current_year+$refferred_firm_current_year_amount);

		}
		
		//	country data				
		foreach($referral_referred_by_country as $referralid)
		{
				$refferred_country_previous_year_amount = $model->get_amount_billed_by_referral($referralid->id,'2013');
				$country_total_previous_year = ($country_total_previous_year+$refferred_country_previous_year_amount);

		}
		
		
		foreach($referral_referred_by_country as $referralid)
		{
				$refferred_country_current_year_amount = $model->get_amount_billed_by_referral($referralid->id,'2014');
				$country_total_current_year = ($country_total_current_year+$refferred_country_current_year_amount);

		}
		
		
		//	region data				
		foreach($referral_referred_by_regionids as $referralid)
		{
				$refferred_region_previous_year_amount = $model->get_amount_billed_by_referral($referralid->id,'2013');
				$region_total_previous_year = ($region_total_previous_year+$refferred_region_previous_year_amount);

		}
		foreach($referral_referred_by_regionids as $referralid)
		{
				$refferred_region_current_year_amount = $model->get_amount_billed_by_referral($referralid->id,'2014');
				$region_total_current_year = ($region_total_current_year+$refferred_region_current_year_amount);

		}
		
		
		
		/*	COMPARSION CHARTS FOR REFERRED REFERRALS ENDS HERE		*/
		
		
		
		
		/*	RECEIVED COMPARSION CHARTS START FROM HERE		*/
		
		//	firms previous and current data
		
		//$received_referrals_firmids = $model->get_received_referrals_firmids($receiving_firm_ids);
		
		foreach($receiving_firm_ids as $received_referrals_firmid)
		{
				
				$received_firm_previous_year_amount = $model->get_amount_billed_by_referral($received_referrals_firmid,'2013');
				$received_firm_total_previous_year = ($received_firm_total_previous_year + $received_firm_previous_year_amount);

		}
		
		foreach($receiving_firm_ids as $received_referrals_firmid)
		{
				
				$received_firm_current_year_amount = $model->get_amount_billed_by_referral($received_referrals_firmid,'2014');
				$received_firm_total_current_year = ($received_firm_total_current_year + $received_firm_current_year_amount);

		}
				
		//	country data				
		foreach($received_referred_by_country as $referralid)
		{
				$received_country_previous_year_amount = $model->get_amount_billed_by_referral($referralid->id,'2013');
				$received_country_total_previous_year = ($received_country_total_previous_year + $received_country_previous_year_amount);

		}
			
		foreach($received_referred_by_country as $referralid)
		{
				$received_country_current_year_amount = $model->get_amount_billed_by_referral($referralid->id,'2014');
				$received_country_total_current_year = ($received_country_total_current_year + $received_country_current_year_amount);

		}
		
		//	region data				
		foreach($received_referred_by_regionids as $referralid)
		{
				$received_region_previous_year_amount = $model->get_amount_billed_by_referral($referralid->id,'2013');
				$received_region_total_previous_year = ($received_region_total_previous_year + $received_region_previous_year_amount);

		}
		foreach($received_referred_by_regionids as $referralid)
		{
				$received_region_current_year_amount = $model->get_amount_billed_by_referral($referralid->id,'2014');
				$received_region_total_current_year = ($received_region_total_current_year + $received_region_current_year_amount);

		}

		
		/*	COMPARSION CHARTS FOR RECEIVED REFERRALS ENDS HERE		*/
		
		
		
		
				
		$this->assignRef('billed_reffered_amount',$billed_reffered_amount);
		$this->assignRef('unbilled_referrals',$unbilled_referrals);
		
		$this->assignRef('billed_received_referrals',$billed_received_referrals);
		$this->assignRef('unbilled_received_referrals',$unbilled_received_referrals);
		
		
		$this->assignRef('ref_total',$ref_total);
		
		$this->assignRef('workReferredByReferral',$workReferredByReferral);
		$this->assignRef('workReferredByHlb',$workReferredByHlb);
		$this->assignRef('workReferredByBusiness',$workReferredByBusiness);
		
		$this->assignRef('workReceivedByReferral',$workReceivedByReferral);
		$this->assignRef('workReceivedByHlb',$workReceivedByHlb);
		$this->assignRef('workReceivedByBusiness',$workReceivedByBusiness);
		
		
		$this->assignRef('referralsReferredByCountry',$referralsReferredByCountry);
		$this->assignRef('referalsReceivedByCountry',$referalsReceivedByCountry);
		
		$this->assignRef('firm_turnover',$firm_turnover);
		$this->assignRef('country_turnover',$country_turnover);
		$this->assignRef('region_turnover',$region_turnover);
		
		$this->assignRef('firm_received_turnover',$firm_received_turnover);
		$this->assignRef('country_received_turnover',$country_received_turnover);
		$this->assignRef('region_received_turnover',$region_received_turnover);
		
		$this->assignRef('firm_total_previous_year',$firm_total_previous_year);
		$this->assignRef('firm_total_current_year',$firm_total_current_year);
		
		$this->assignRef('country_total_previous_year',$country_total_previous_year);
		$this->assignRef('country_total_current_year',$country_total_current_year);
		
		$this->assignRef('region_total_previous_year',$region_total_previous_year);
		$this->assignRef('region_total_current_year',$region_total_current_year);
		
		
		$this->assignRef('received_firm_total_previous_year',$received_firm_total_previous_year);
		$this->assignRef('received_firm_total_current_year',$received_firm_total_current_year);
		
		$this->assignRef('received_country_total_previous_year',$received_country_total_previous_year);
		$this->assignRef('received_country_total_current_year',$received_country_total_current_year);
		
		$this->assignRef('received_region_total_previous_year',$received_region_total_previous_year);
		$this->assignRef('received_region_total_current_year',$received_region_total_current_year);
		
		
	
		parent::display($tpl);



	}

	

}

?>

