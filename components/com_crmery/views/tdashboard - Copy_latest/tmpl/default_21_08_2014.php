<?php

defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<?php 

	 $ref_total =  $this->ref_total;
	 
	 $billed_reffered_amount = $this->billed_reffered_amount;
	 $unbilled_referrals = $this->unbilled_referrals;
	 
	 $billed_received_referrals = $this->billed_received_referrals;
	 $unbilled_received_referrals = $this->unbilled_received_referrals;
	 
	 $firm_turnover = $this->firm_turnover;
	 $country_turnover = $this->country_turnover;
	 $region_turnover = $this->region_turnover;
	 
	 $firm_received_turnover = $this->firm_received_turnover;
	 $country_received_turnover = $this->country_received_turnover;
	 $region_received_turnover = $this->region_received_turnover;
	 
	 
	 $workReferredByReferral = $this->workReferredByReferral;
	 $workReferredByHlb = $this->workReferredByHlb;
	 $workReferredByBusiness = $this->workReferredByBusiness;
	 
	 $workReceivedByReferral = $this->workReceivedByReferral;
	 $workReceivedByHlb = $this->workReceivedByHlb;
	 if($workReceivedByHlb == '')
	 {
	 	$workReceivedByHlb = '0';
	 }
	 $workReceivedByBusiness = $this->workReceivedByBusiness;
	 
	 
	 
	 $firm_total_previous_year = $this->firm_total_previous_year;
	 $firm_total_current_year = $this->firm_total_current_year;
	  
	 $country_total_previous_year = $this->country_total_previous_year;
	 $country_total_current_year = $this->country_total_current_year;
	  
	 $region_total_previous_year = $this->region_total_previous_year;
	 $region_total_current_year = $this->region_total_current_year;
	 
	 
	 $received_firm_total_previous_year = $this->received_firm_total_previous_year;
	 $received_firm_total_current_year = $this->received_firm_total_current_year;
	  
	 $received_country_total_previous_year = $this->received_country_total_previous_year;
	 $received_country_total_current_year = $this->received_country_total_current_year;
	  
	 $received_region_total_previous_year = $this->received_region_total_previous_year;
	 $received_region_total_current_year = $this->received_region_total_current_year;
	 
	 
	 
	 $referralsReferredByCountry = $this->referralsReferredByCountry;
	 foreach( $referralsReferredByCountry as $referralReferredByCountry)
	 {
	  
		$a .= "['$referralReferredByCountry->country_name',$referralReferredByCountry->Numbers,$referralReferredByCountry->Numbers],";
		
	 }
	  
	 $a = substr($a,0,-1);
	 
	 $referalsReceivedByCountry = $this->referalsReceivedByCountry;
	 foreach( $referalsReceivedByCountry as $referalReceivedByCountry)
	 {
	  
		$b .= "['$referalReceivedByCountry->country_name',$referalReceivedByCountry->Numbers,$referalReceivedByCountry->Numbers],";
		
	 }
	  
	 $b = substr($b,0,-1);
	 
	 
?>
	 

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	
	

 <!-- 		Work Referred By Source					-->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		  ['Referral', <?php echo $workReferredByReferral ; ?>,<?php echo $workReferredByReferral ; ?>],
          ['HLB Magic',<?php echo $workReferredByHlb ; ?>,<?php echo $workReferredByHlb ; ?>],
		  ['Business Channel',<?php echo $workReferredByBusiness ; ?>,<?php echo $workReferredByBusiness ; ?>]
          
        ]);

        var options = {
          //title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('work_referred_by_source'));
        chart.draw(data, options);
      }
    </script>
	
	
	 <!-- 		Work Received By Source					-->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		  ['Referral', <?php echo $workReceivedByReferral ; ?>,<?php echo $workReceivedByReferral ; ?>],
          ['HLB Magic',<?php echo $workReceivedByHlb ; ?>,<?php echo $workReceivedByHlb ; ?>],
		  ['Business Channel',<?php echo $workReceivedByBusiness ; ?>,<?php echo $workReceivedByBusiness ; ?>]
          
        ]);

        var options = {
          //title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('work_received_by_source'));
        chart.draw(data, options);
      }
    </script>
	
	
	<!-- 		SCRIPT FOR WORK REFERRED RESULTING IN BILLING -->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		  ['Billed Amount', <?php echo $billed_reffered_amount ; ?>,<?php echo $billed_reffered_amount ; ?>],
          ['Unbilled Amount', <?php echo $unbilled_referrals ; ?>,<?php echo $unbilled_referrals ; ?>]
          
		  
        ]);

        var options = {
         // title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('referred_billing_amount_by_companies'));
        chart.draw(data, options);
      }
    </script>
	
	
		<!-- 		SCRIPT FOR WORK RECEIVED RESULTING IN BILLING -->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		  ['Billed Amount', <?php echo $billed_received_referrals ; ?>,<?php echo $billed_received_referrals ; ?>],
          ['Unbilled Amount', <?php echo $unbilled_received_referrals ; ?>,<?php echo $unbilled_received_referrals ; ?>]
          
		  
        ]);

        var options = {
         // title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('received_billing_amount_by_companies'));
        chart.draw(data, options);
      }
    </script>
	
	
	<!-- 		SCRIPT FOR REFERRALS REFERRED BY COUNTRY			-->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		  <?php echo $a; ?>
          
        ]);

        var options = {
         // title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('referrals_referred_by_country'));
        chart.draw(data, options);
      }
    </script>
	
	<!-- 		SCRIPT FOR REFERRALS RECEIVED BY COUNTRY			-->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		  <?php echo $b; ?>
          
        ]);

        var options = {
         // title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('referrals_received_by_country'));
        chart.draw(data, options);
      }
    </script>
	
	
	
	<!----------------------SCRIPT FOR REFERRALS REFERRED OUT BY TURNOVER Total Amount Through Regions---------------------->
	
	
	<script type="text/javascript">
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
        ['', 'Percentage', { role: 'style' }],
        ['Firm', <?php echo $firm_turnover ;?>, '#b87333'],            // RGB value
		['UK',  <?php echo $country_turnover ;?>, 'silver'],            // English color name
		['Europe',<?php echo $region_turnover ;?>, 'gold'],
		
      ]);
	 

      var options = {
	  	//title: 'Total Amount From Regions',
		//hAxis: {title: 'Regions', titleTextStyle: {color: 'red'}},
		vAxis: {title: 'Percentage', titleTextStyle: {color: 'red'}},
        width: 600,
        height: 400,
        bar: { groupWidth: '75%' },
        isStacked: true,
      };
        var chart = new google.visualization.ColumnChart(document.getElementById('referrals_referred_by_turnover'));
        chart.draw(data, options);
      }
    </script>
	
	<!----------------------SCRIPT FOR REFERRALS RECEIVED OUT BY TURNOVER Total Amount Through Regions---------------------->
	
	
	<script type="text/javascript">
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
        ['', 'Percentage', { role: 'style' }],
        ['Firm', <?php echo $firm_received_turnover ;?>, '#b87333'],            // RGB value
		['UK',  <?php echo $country_received_turnover ;?>, 'silver'],            // English color name
		['Europe',<?php echo $region_received_turnover ;?>, 'gold'],
		
      ]);
	 

      var options = {
	  	//title: 'Total Amount From Regions',
		//hAxis: {title: 'Regions', titleTextStyle: {color: 'red'}},
		vAxis: {title: 'Percentage', titleTextStyle: {color: 'red'}},
        width: 600,
        height: 400,
        bar: { groupWidth: '75%' },
        isStacked: true,
      };
        var chart = new google.visualization.ColumnChart(document.getElementById('referrals_received_turnover'));
        chart.draw(data, options);
      }
    </script>
	
	
	<!-- 		SCRIPT FOR COMPARSION CHARTS REFERRED		-->
	
	<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {

  var data = google.visualization.arrayToDataTable([
    ['Amount', '2013', '2014'],
    ['Menzies',<?php echo $firm_total_previous_year ; ?>, <?php echo $firm_total_current_year ; ?>],
    ['UK',     <?php echo $country_total_previous_year ; ?>, <?php echo $country_total_current_year ; ?>],
    ['Europe', <?php echo $region_total_previous_year ; ?>, <?php echo $region_total_current_year ; ?>]
  ]);

  var options = {
    //title: 'Company Performance',
    //hAxis: {title: 'Year', titleTextStyle: {color: 'red'}},
	vAxis: {title: 'Amount in GBP', titleTextStyle: {color: 'red'}}
  };

  var chart = new google.visualization.ColumnChart(document.getElementById('comparsion_chart'));

  chart.draw(data, options);

}
    </script>
	
	
	<!-- 		SCRIPT FOR COMPARSION CHARTS RECEIVED			-->
	
	<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {

  var data = google.visualization.arrayToDataTable([
    ['Amount', '2013', '2014'],
    ['Menzies',<?php echo $received_firm_total_previous_year ; ?>, <?php echo $received_firm_total_current_year ; ?>],
    ['UK',     <?php echo $received_country_total_previous_year ; ?>, <?php echo $received_country_total_current_year ; ?>],
    ['Europe', <?php echo $received_region_total_previous_year ; ?>, <?php echo $received_region_total_current_year ; ?>]
  ]);

  var options = {
    //title: 'Company Performance',
    //hAxis: {title: 'Year', titleTextStyle: {color: 'red'}},
	vAxis: {title: 'Amount in GBP', titleTextStyle: {color: 'red'}}
  };

  var chart = new google.visualization.ColumnChart(document.getElementById('received_comparsion_chart'));

  chart.draw(data, options);

}
    </script>



<h1><?php echo CRMText::_('COM_CRMERY_DASHBOARD_HEADER'); ?></h1>



<div style="padding-left:50px;"><b>Work Referred By Source</b></div>
<div id="work_referred_by_source" style="width: 500px; height: 250px;"></div>

<div style="padding-left:50px;"><b>Work Received By Source</b></div>
<div id="work_received_by_source" style="width: 500px; height: 250px;"></div>

<div style="padding-left:50px;"><b>Work Referred Result in billing </b></div>
<div id="referred_billing_amount_by_companies" style="width: 500px; height: 250px;"></div>

<div style="padding-left:50px;"><b>Work Received Result in billing </b></div>
<div id="received_billing_amount_by_companies" style="width: 500px; height: 250px;"></div>

<div style="padding-left:50px;"><b>Number of Referrals Referred by Country</b></div>
<div id="referrals_referred_by_country" style="width: 500px; height: 250px;"></div>

<div style="padding-left:50px;"><b>Number of Referrals Received by Country</b></div>
<div id="referrals_received_by_country" style="width: 500px; height: 250px;"></div>

<div style="padding-left:50px;"><b>Referrals referred out of turnover</b></div>
<div id="referrals_referred_by_turnover" style="width: 500px; height: 550px;"></div>


<div style="padding-left:50px;"><b>Referrals received out of turnover</b></div>
<div id="referrals_received_turnover" style="width: 500px; height: 550px;"></div>

<div style="padding-left:50px;"><b>Comparsion charts for referrals referred</b></div>
<div id="comparsion_chart" style="width: 500px; height: 550px;"></div>

<div style="padding-left:50px;"><b>Comparsion charts for referrals received</b></div>
<div id="received_comparsion_chart" style="width: 500px; height: 550px;"></div>













