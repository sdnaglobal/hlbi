<?php

defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<?php 

	 $ref_total =  $this->ref_total;
	 $reffered_amount_total = $this->reffered_amount_total;
	 
	 if($reffered_amount_total == '0')
	 {
	 	$reffered_amount_total = "11";
	 }
	 
	 $firm_turnover = $this->firm_turnover;
	 $country_turnover = $this->country_turnover;
	 $region_turnover = $this->region_turnover;
	 
	 
	 $workReferredByReferral = $this->workReferredByReferral;
	 $workReferredByHlb = $this->workReferredByHlb;
	 $workReferredByBusiness = $this->workReferredByBusiness;
	 
	 $referralsReferredByCountry = $this->referralsReferredByCountry;
	 
	 
	 $firm_total_previous_year = $this->firm_total_previous_year;
	 $firm_total_current_year = $this->firm_total_current_year;
	  
	 $country_total_previous_year = $this->country_total_previous_year;
	 $country_total_current_year = $this->country_total_current_year;
	  
	 $region_total_previous_year = $this->region_total_previous_year;
	 $region_total_current_year = $this->region_total_current_year;
	 
	 
	 
	 
	 

	  foreach( $referralsReferredByCountry as $referralReferredByCountry)
	  {
	  
		$a .= "['$referralReferredByCountry->country_name',$referralReferredByCountry->Numbers,$referralReferredByCountry->Numbers],";
		
	  }
	  
	  $a = substr($a,0,-1);
?>
	 

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	
	
	
	<!----------------------SCRIPT FOR REFERRALS REFERRED OUT BY TURNOVER Total Amount Through Regions---------------------->
	
	
	<script type="text/javascript">
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
        ['', 'Percentage', { role: 'style' }],
        ['Firm', <?php echo $firm_turnover ;?>, '#b87333'],            // RGB value
		['UK',  <?php echo $country_turnover ;?>, 'silver'],            // English color name
		['Europe',  <?php echo $region_turnover ;?>, 'gold'],
		
      ]);

      var options = {
	  	//title: 'Total Amount From Regions',
		//hAxis: {title: 'Regions', titleTextStyle: {color: 'red'}},
		vAxis: {title: 'Percentage', titleTextStyle: {color: 'red'}},
        width: 600,
        height: 400,
        bar: { groupWidth: '75%' },
        isStacked: true,
      };
        var chart = new google.visualization.ColumnChart(document.getElementById('referrals_referred_by_turnover'));
        chart.draw(data, options);
      }
    </script>
	
	<!-- 		SCRIPT FOR COMPARSION CHARTS			-->
	
	<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {

  var data = google.visualization.arrayToDataTable([
    ['Amount', '2013', '2014'],
    ['Menzies',<?php echo $firm_total_previous_year ; ?>, <?php echo $firm_total_current_year ; ?>],
    ['UK',     <?php echo $country_total_previous_year ; ?>, <?php echo $country_total_current_year ; ?>],
    ['Europe', <?php echo $region_total_previous_year ; ?>, <?php echo $region_total_current_year ; ?>]
  ]);

  var options = {
    //title: 'Company Performance',
    //hAxis: {title: 'Year', titleTextStyle: {color: 'red'}},
	vAxis: {title: 'Amount in GBP', titleTextStyle: {color: 'red'}}
  };

  var chart = new google.visualization.ColumnChart(document.getElementById('comparsion_chart'));

  chart.draw(data, options);

}
    </script>

	
	
	
	
	 <!-- 		SCRIPT FOR REFERRALS REFERRED BY COUNTRY			-->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		  <?php echo $a; ?>
          
        ]);

        var options = {
         // title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('referrals_referred_by_country'));
        chart.draw(data, options);
      }
    </script>
	
	
    <!-- 		SCRIPT FOR Total Amount Referred and Received					-->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		  ['Referred Amount', 378962,378962],
          ['Received Amount',91578,91578]
          
        ]);

        var options = {
         // title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('amt_ref_rec'));
        chart.draw(data, options);
      }
    </script>
	
	  <!-- 		Work Referred By Source					-->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		  ['Referral', <?php echo $workReferredByReferral ; ?>,<?php echo $workReferredByReferral ; ?>],
          ['HLB Magic',<?php echo $workReferredByHlb ; ?>,<?php echo $workReferredByHlb ; ?>],
		  ['Business Channel',<?php echo $workReferredByBusiness ; ?>,<?php echo $workReferredByBusiness ; ?>]
          
        ]);

        var options = {
         // title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('work_referred_by_source'));
        chart.draw(data, options);
      }
    </script>
	
	
	


	<!-- 		SCRIPT FOR WORK REFERRED RESULTING IN BILLING -->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		  ['Total Referred Amount', <?php echo $ref_total ; ?>,<?php echo $ref_total ; ?>],
          ['Billed Amount', <?php echo $reffered_amount_total ; ?>,<?php echo $reffered_amount_total ; ?>]
          
		  
        ]);

        var options = {
         // title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('referred_billing_amount_by_companies'));
        chart.draw(data, options);
      }
    </script>
	
	




<h1><?php echo CRMText::_('COM_CRMERY_DASHBOARD_HEADER'); ?></h1>

<div style="padding-left:50px;"><b>Total Amount Referred and Received for 2014</b></div>
<div id="amt_ref_rec" style="width: 500px; height: 250px;"></div>

<div style="padding-left:50px;"><b>Work Referred By Source</b></div>
<div id="work_referred_by_source" style="width: 500px; height: 250px;"></div>

<div style="padding-left:50px;"><b>Work Referred Result in billing for 2014</b></div>
<div id="referred_billing_amount_by_companies" style="width: 500px; height: 250px;"></div>


<div style="padding-left:50px;"><b>Number of Referrals Referred by Country</b></div>
<div id="referrals_referred_by_country" style="width: 500px; height: 250px;"></div>

<div style="padding-left:50px;"><b>Referrals referred out of turnover</b></div>
<div id="referrals_referred_by_turnover" style="width: 500px; height: 550px;"></div>


<div style="padding-left:50px;"><b>Comparsion charts for referrals referred</b></div>
<div id="comparsion_chart" style="width: 500px; height: 550px;"></div>





