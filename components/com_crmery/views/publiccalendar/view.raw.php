<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewPubliccalendar extends JView
{
	function display($tpl = null)
	{
		//grab model
		$model =& JModelLegacy::getInstance('event','CrmeryModel');

		//detect event id
		$id = null;
		if ( JRequest::getVar('parent_id') && !JRequest::getVar('id') ){
			$id = JRequest::getVar('parent_id');
		}else{
			$id = JRequest::getVar('id');
		}
		
		//grab event
		if ( $id != null ){
			$event = $model->getEvent($id,true);
		}

		$this->assignRef('event',$event);
		
		//display
		parent::display($tpl);
	}
	
}
?>