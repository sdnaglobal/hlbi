<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<script type="text/javascript">
	var loc = 'calendar';	
</script>

<h1><?php echo CRMText::_('COM_CRMERY_CALENDAR_HEADER'); ?></h1>

<div id="calendar"></div>

<div id="templates" style="display:none;">
	<div id="view_task"></div>
	<div id="view_event"></div>
</div>