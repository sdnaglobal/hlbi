<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

//assign event
$event = $this->event;

defined('_JEXEC') or die('Restricted access'); ?>

<div id="edit_form">
	<div class="crmeryRow">
		<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_EDIT_TASK_NAME'); ?></div>
		<div class="crmeryValue"><?php if ( array_key_exists('name',$event) ) echo $event['name']; ?></div>
	</div>
	<div class="crmeryRow">
		<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_EDIT_EVENT_START_TIME'); ?></div>
		<div class="crmeryValue">
			<?php if ( array_key_exists('start_time',$event) ) echo $event['start_time_formatted']; ?>
			<?php if ( array_key_exists('start_time_hour',$event) ){ echo $event['start_time']; } ?>
		</div>
	</div>
	<div class="crmeryRow">
		<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_EDIT_EVENT_END_TIME'); ?></div>
		<div class="crmeryValue">
			<?php if ( array_key_exists('end_time',$event) ) echo $event['end_time_formatted']; ?>
			<?php if ( array_key_exists('end_time_hour',$event) ){ echo $event['end_time_hour']; } ?>
		</div>
	</div>
	<?php if ( $event['all_day'] == 1 ){ ?>
	<div class="crmeryRow">
		<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_EDIT_EVENT_ALL_DAY'); ?></div>
		<div class="crmeryValue">
			<p><?php echo CRMText::_('COM_CRMERY_EDIT_EVENT_ALL_DAY_MESSAGE'); ?></p>
		</div>
	</div>
	<?php } ?>
	<?php if ( strtotime($event['end_date']) > 0 ){ ?>
	<div class="crmeryRow">
		<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_END_DATE'); ?></div>
		<div class="crmeryValue">
			<?php if (array_key_exists('end_date',$event)) echo $event['end_date_formatted']; ?>                                  
		</div>
	</div>
	<?php } ?>
	<div class="crmeryRow">
		<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_EDIT_TASK_DESCRIPTION'); ?></div>
		<div class="crmeryValue">
			<p><?php if(array_key_exists('description',$event)) echo $event['description']; ?></p>
		</div>
	</div>
	<div class="actions">
		<a href="javascript:void(0);" onclick="closeTaskEvent('event')"><?php echo CRMText::_('COM_CRMERY_CLOSE'); ?></a>
	</div>
</div>