<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewPubliccalendar extends JView
{
	function display($tpl = null)
	{	
		//load js libs
		$document =& JFactory::getDocument();
		$document->addScript( JURI::base().'components/com_crmery/media/js/fullcalendar.js' );
		$document->addScript( JURI::base().'components/com_crmery/media/js/public_calendar.js' );
		
		//load required css for calendar
		$document->addStyleSheet( JURI::base().'components/com_crmery/media/css/fullcalendar.css' );
		
		//display
		parent::display($tpl);
	}
	
}
?>
