<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.model');

class CrmeryModelCharts extends JModel
{
    /**
     * 
     *
     * @access  public
     * @return  void
     */
    function __construct()
    {
        parent::__construct();
        
    }
	
	function get_receiving_companies($firm_ids)
	{

		foreach($firm_ids as $firm_id)
		{
			$where .= " company_id=".$firm_id." or";
		}
			$where = "(".substr($where,0,-2).")";
	
			$db =& JFactory::getDbo();
			$querysu = $db->getQuery(true);
	
			$querysu->select("id")
			->from("#__crmery_people");
			$querysu->where("$where and published>0 and Accepted=1");
			
			$db->setQuery($querysu);
			$receivingReferralIds = $db->loadObjectList();
			
			$receivingFirmId = array();		
	
			if($receivingReferralIds){
	
	
	
				foreach($receivingReferralIds as $receivingReferralId)
	
				{
	
					$receivingFirmId[] = $receivingReferralId->id;
	
				}
	
			}
			
			return $receivingFirmId;

	}
	
  
    function get_user_companies()
	{
	
		$member_id = CrmeryHelperUsers::getUserId();
	
		$member_role = CrmeryHelperUsers::getRole();
	
		 if ( $member_role == 'manager' || $member_role == 'basic' ) // CUSTOMIZING HERE  
	
		  {
	
			 if($member_role == 'manager') 
	
			 { 
	
				$field="company_id";
	
			 } 
	
			 if($member_role == 'basic')
	
			 {
	
				$field="inp_company_id";
	
			 }
	
	
			$db =& JFactory::getDbo();
	
			$querysu = $db->getQuery(true);
	
			$querysu->select("u.$field")
	
			->from("#__crmery_users AS u");
	
			$querysu->where("u.id=".$member_id);
			
			//echo $querysu;
	
			$db->setQuery($querysu);
	
			$comId = $db->loadResult();
	
	//		echo "<pre>"; print_r($comId); echo "</pre>";
	
			$p_query = $db->getQuery(true);
	
			$p_query->select("id")
	
			->from("#__crmery_companies ");
	
			$p_query->where("published>0 and partner_id=".$member_id." and id !=".$comId);
	
			 
	
			$db->setQuery($p_query);
	
			$par_Ids = $db->loadObjectList();
	
				
	
			$firmsArr = array();		
	
			if($par_Ids){
	
	
	
				foreach($par_Ids as $parid)
	
				{
	
					$firmsArr[] = $parid->id;
	
				}
	
			}
	
			//$firmsArr[] = $comId;
			
			//echo "<pre>"; print_r($firmsArr); echo  "</pre>";
	
			return $firmsArr;
			
	
		}
		
		
		
	
	}
	
	
	/**
     * Get list of themes
     * @param int $id specific search id
     * @return mixed $results results
     */
	 
	 
	function  get_amount_billed_by_source($referralid)
	{
	
		$db =& JFactory::getDBO();
        $query = $db->getQuery(true);
		
		$query = "SELECT sum(SterlingAmount) as amt FROM `kjz42_crmery_referral_payments` WHERE referral_ID =".$referralid ." group by referral_ID";
        
        $db->setQuery($query);
		$total = $db->loadObjectList();
		
		$val = $total[0]->amt;
		
		return $val;
	
	}
	 
    function getWorkReferredBySource($firm_ids,$source_id){
		
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
       
	   	foreach($firm_ids as $firm_id)
		{
	
			$where .= " referring_firm_id=".$firm_id." or";
	
		}
		
		$where = "(".substr($where,0,-2).")";
		
		$query1 = "SELECT sum(rp.SterlingAmount) as referral_total from #__crmery_referral_payments rp inner join kjz42_crmery_people p on p.id = rp.referral_id where $where and p.source_id = $source_id ";
        
		$db->setQuery($query1);
		$referred_amount = $db->loadResult();
		
		return $referred_amount;
    }
	
	function getWorkReceivedBySource($firm_ids,$source_id){
		
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
       
	   	foreach($firm_ids as $firm_id)
		{
	
			$where .= " company_id=".$firm_id." or";
	
		}
		
		$where = "(".substr($where,0,-2).")";
		
		$query1 = "SELECT sum(rp.SterlingAmount) as referral_total from #__crmery_referral_payments rp inner join kjz42_crmery_people p on p.id = rp.referral_id where $where and p.source_id = $source_id ";
        
		$db->setQuery($query1);
		$referred_amount = $db->loadResult();
		
		return $referred_amount;
    }
		
	
	function get_send_referrals_details_by_firm_ids($firm_ids, $year=0)
	{
	
		foreach($firm_ids as $firm_id)
		{
			$where .= " referring_firm_id=".$firm_id." or";
		}
			$where = "(".substr($where,0,-2).")";
	
			$db =& JFactory::getDbo();
			$querysu = $db->getQuery(true);
	
			$querysu->select("id")
			->from("#__crmery_people");
			$querysu->where("$where and published>0 and Accepted=1");
	
			
			if($year > 0)
	
				$querysu->where("YEAR(created)=$year");
			
			$db->setQuery($querysu);
			$referalId = $db->loadObjectList();
	
			return $referalId;
	
	}
		


	function get_full_referrals_details_by_firm_ids($firm_ids, $year=0)
	{
	
		foreach($firm_ids as $firm_id)
		{
			$where .= " referring_firm_id=".$firm_id." or";
		}
			$where = "(".substr($where,0,-2).")";
	
			$db =& JFactory::getDbo();
			$querysu = $db->getQuery(true);
	
			$querysu->select("company_id")
			->from("#__crmery_people");
			$querysu->where("$where and published>0 and Accepted=1");
	
			if($year > 0)
	
				$querysu->where("YEAR(created)=$year");
			
	
			$db->setQuery($querysu);
			$reffered_company_id = $db->loadObjectList();

			return $reffered_company_id;
	
	}
	
	function get_referrals_details_by_firm_ids($firm_ids, $year=0)
	{
	
		foreach($firm_ids as $firm_id)
		{
			$where .= " company_id=".$firm_id." or";
		}
			$where = "(".substr($where,0,-2).")";
	
			$db =& JFactory::getDbo();
			$querysu = $db->getQuery(true);
	
			$querysu->select("id")
			->from("#__crmery_people");
			$querysu->where("$where and published>0 and Accepted=1");
	
			if($year > 0)
	
				$querysu->where("YEAR(created)=$year");
			
			$db->setQuery($querysu);
	
			$referalId = $db->loadObjectList();
	
			return $referalId;
	
	}
	
	
	
	
	
	
	function get_amount_billed_by_referral_id($ref_id,$year)
	{
		$db = JFactory::getDbo();
		$billing = $db->getQuery(true);
	
		
		$billing->select('LocalAmount,LocalCurrencyCode,SterlingAmount');
		$billing->from('#__crmery_referral_payments');
		$billing->where('FeePaid=1 and referral_ID='.$ref_id);
	
		if($year>0)
	
		{  $year = ($year);
	
			$billing->where(' YEAR(STR_TO_DATE(BillingYear, "%d/%m/%Y"))='.$year);
	
		}
		//echo  $billing."<br>";
		$db->setQuery($billing);
		$billingdetails = $db->loadObjectList();
	
		$GBPsum = 0;
		
		foreach($billingdetails as $bill)
		{ 	
			$GBPsum = ($GBPsum+$bill->SterlingAmount);
	
		}
	
		return $GBPsum;
	}
	
	
	function get_amount_billed_by_referred_firm($firm_ids)
	{
		
		foreach($firm_ids as $firm_id)
		{
			$where .= " firm_id=".$firm_id." or";
		}
			$where = "(".substr($where,0,-2).")";
	
		$db = JFactory::getDbo();
		$billingnew = $db->getQuery(true);
		
		$billingnew = "SELECT COALESCE(sum(gross_fee_income),0) FROM #__crmery_companies_billing WHERE $where";
	
		$db->setQuery($billingnew);
		$billedReferrals = $db->loadResult();
		
		return $billedReferrals;
	}
	
	
	function get_firm_ids_from_region()
	{
	
		$where .= " region_id=1";
		
		$db =& JFactory::getDbo();
		$company_ids = $db->getQuery(true);
		$company_ids->select("id")

					->from("#__crmery_companies")

					->where(" $where ");
		
		$db->setQuery($company_ids);
		$all_company_ids = $db->loadObjectList();
		
		return $all_company_ids ;
	
	
	}
	
	
	function get_total_referred_from_region($firm_ids_from_region)
	{
	
		$db =& JFactory::getDbo();
		
		foreach($firm_ids_from_region as $firm_id_from_region)
		{
		
			$referral_ids = $db->getQuery(true);
			$where = " company_id=".$firm_id_from_region->id;
	
			$referral_ids->select(" * ")
		   				 ->from("#__crmery_people")
						 ->where(" $where ");
		
		}
		
		$db->setQuery($referral_ids);
		$all_referral_ids = $db->loadObjectList();
		
	}
	
	
	function getReferralsReferredByCountry($firm_ids)
	{
	
		foreach($firm_ids as $firm_id)
		{
			$where .= " p.referring_firm_id=".$firm_id." or";
		}
		$where = "(".substr($where,0,-2).")";
		
		$firm_detail = $this->get_firm_details($firm_ids);
		$country_id = $firm_detail['country_id'];

		$db =& JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$query = "select cur.country_id ,cur.country_name FromCountry, cu.country_name ToCountry, Count(p.id) as Numbers from kjz42_crmery_people p left join kjz42_crmery_companies c on p.company_id = c.id left join kjz42_crmery_companies co on p.referring_firm_id = co.id left join kjz42_crmery_currencies cur on cur.country_id = co.country_id left join kjz42_crmery_currencies cu on cu.country_id = c.country_id where cur.country_id = ".$country_id." group by cur.country_id, cu.country_name ,cur.country_name order by cu.country_id ";
		
		
		$db->setQuery($query);
		$referalsReferredByCountry = $db->loadObjectList();
		
		return $referalsReferredByCountry;
	
	}
	
	function getReferralsReceivedByCountry($firm_ids)
	{
	
		/*foreach($firm_ids as $firm_id)
		{
			$where .= " p.company_id=".$firm_id." or";
		}
		$where = "(".substr($where,0,-2).")";
		*/
		$db =& JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$firm_detail = $this->get_firm_details($firm_ids);
		$country_id = $firm_detail['country_id'];
		
		$query = "select cu.country_id , cur.country_name FromCountry, cu.country_name ToCountry, Count(p.id) as Numbers from kjz42_crmery_people p left join kjz42_crmery_companies c on p.referring_firm_id = c.id left join kjz42_crmery_companies co on p.company_id = co.id left join kjz42_crmery_currencies cu on cu.country_id = co.country_id left join kjz42_crmery_currencies cur on cur.country_id = c.country_id where cu.country_id = ".$country_id." group by cu.country_id, cur.country_name ,cu.country_name order by cur.country_id ";
		
		
		$db->setQuery($query);
		$referalsreceivedByCountry = $db->loadObjectList();
		unset($referalsreceivedByCountry[0]);
	
		return $referalsreceivedByCountry;
	
	}
	
	function get_coureg_fee_income_promotional_gbp($firm_ids)
	{
		foreach($firm_ids as $firm_id)
		{
			$where .= " firm_id=".$firm_id->id." or";
		}
		$where = "(".substr($where,0,-2).")";

		$db =& JFactory::getDbo();
		$query = $db->getQuery(true);
		$year  = date("y");
		
		$query = "SELECT COALESCE(sum(fee_income_promotional),0) as total FROM `kjz42_crmery_companies_billing` WHERE $where and BillingYear like '%$year%' and billingtype like '%Current Billing%' ";
		
		$db->setQuery($query);
		$fee_income_promotional_gbp = $db->loadObjectList();
		$fee_income_promotional_gbp = $fee_income_promotional_gbp[0]->total;
		
		return $fee_income_promotional_gbp;
	
	}
	
	
	function get_fee_income_promotional_gbp($firm_ids)
	{
		
		foreach($firm_ids as $firm_id)
		{
			$where .= " firm_id=".$firm_id." or";
		}
		$where = "(".substr($where,0,-2).")";

		$db =& JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$year  = date("y");
		
		$query = "SELECT COALESCE(sum(`fee_income_promotional`),0) as total FROM `kjz42_crmery_companies_billing` WHERE $where and BillingYear like '%$year%' and  billingtype like '%Current Billing%' ";
		
		$db->setQuery($query);
		$fee_income_promotional_gbp = $db->loadObjectList();
		//$fee_income = $fee_income_promotional_gbp[0]->total;
		
		return $fee_income_promotional_gbp;
	
	}
	
	function get_firm_details($firm_ids)
	{
	
		$db =& JFactory::getDbo();
		$query = $db->getQuery(true);
		
		foreach($firm_ids as $firm_id)
		{
			$where .= " c.id=".$firm_id." or";
		}
		$where = "(".substr($where,0,-2).")";
		
		$firm_data = "select c.name,c.country_id,c.region_id ,cu.country_name, r.region_name from kjz42_crmery_companies c left join kjz42_crmery_currencies cu on cu.country_id = c.country_id left join kjz42_crmery_region r on r.region_id = c.region_id where $where  ";
		//$firm_data = "select * from #__crmery_companies where $where ";
		
		$db->setQuery($firm_data);
		$firm_full_data = $db->loadAssoc();
		
		return $firm_full_data;
	
	}
	
	function get_amount_billed_by_referral($ref_id,$year)
	{
		$db = JFactory::getDbo();
		$billing = $db->getQuery(true);
	
		$billing->select('LocalAmount,LocalCurrencyCode,SterlingAmount');
		$billing->from('#__crmery_referral_payments');
		$billing->where('FeePaid=1 and referral_ID='.$ref_id);
	
		if($year>0)
	
		{  $year = ($year);
	
			$billing->where(' YEAR(STR_TO_DATE(BillingYear, "%d/%m/%Y"))='.$year);
	
		}
	
		$db->setQuery($billing);
		$billingdetails = $db->loadObjectList();
	
		$GBPsum = 0;
		
		foreach($billingdetails as $bill)
		{ 	
			$GBPsum = ($GBPsum+$bill->SterlingAmount);
	
		}
	
		return $GBPsum;
	}
	
	function get_amount_billed_by_referral_country($ref_id)
	{
	
		$db = JFactory::getDbo();
		$billing = $db->getQuery(true);
		$year = date("Y");
		
		$billing->select('LocalAmount,LocalCurrencyCode,SterlingAmount');
		$billing->from('#__crmery_referral_payments');
		$billing->where('FeePaid=1 and referral_ID= '.$ref_id.' and BillingYear like "%'.$year.'%"');
		
		$db->setQuery($billing);
		$billingdetails = $db->loadObjectList();
		
	
		$GBPsum = 0;
		
		foreach($billingdetails as $bill)
		{ 	
			$GBPsum = ($GBPsum+$bill->SterlingAmount);
	
		}
		
		return $GBPsum;
	}
	
	
	function get_amount_billed_by_referral_region($ref_id)
	{
		$db = JFactory::getDbo();
		$billing = $db->getQuery(true);
	
		$billing->select('LocalAmount,LocalCurrencyCode,SterlingAmount');
		$billing->from('#__crmery_referral_payments');
		$billing->where('FeePaid=1 and referral_ID='.$ref_id);
		
		$db->setQuery($billing);
		$billingdetails = $db->loadObjectList();
	
		$GBPsum = 0;
		foreach($billingdetails as $bill)
		{ 	
			$GBPsum = ($GBPsum+$bill->SterlingAmount);
	
		}
	
		return $GBPsum;
	}
	
	function get_received_turnover_by_country($firm_ids)
	{
	
		$db =& JFactory::getDbo();
        $query = $db->getQuery(true);
		
		$firm_detail = $this->get_firm_details($firm_ids);
		$country_id = $firm_detail['country_id'];
		
		//$query = "select p.id from kjz42_crmery_people p inner join kjz42_crmery_companies co on p.company_id = co.id inner join kjz42_crmery_currencies cu on co.country_id = cu. country_id where $where and cu.country_id = ".$country_id;
		$query = "select p.id from kjz42_crmery_people p inner join kjz42_crmery_companies c on c.id = p.company_id where c.country_id = ".$country_id;
		
		$db->setQuery($query);
		$referral_turnover_by_country = $db->loadObjectList();

		return $referral_turnover_by_country;

	}
	
	function get_referrals_id_received_by_region($firm_ids)
	{
		$firm_detail = $this->get_firm_details($firm_ids);
		$region_id = $firm_detail['region_id'];
		
		$db =& JFactory::getDbo();
        $query = $db->getQuery(true);
		
		$query = "select p.company_id as id from kjz42_crmery_people p left join kjz42_crmery_companies c on c.id = p.referring_firm_id where c.region_id = ".$region_id." group by p.company_id";
		//echo $query."<br>";
		$db->setQuery($query);
		$received_referral_id_by_region = $db->loadObjectList();
		
		
		return $received_referral_id_by_region;
	
	
	}
	
	function get_referrals_id_referred_by_region($firm_ids)
	{
		$firm_detail = $this->get_firm_details($firm_ids);
		$region_id = $firm_detail['region_id'];
		
		$db =& JFactory::getDbo();
        $query = $db->getQuery(true);
		
		$query = "select p.referring_firm_id as id from kjz42_crmery_people p left join kjz42_crmery_companies c on c.id = p.referring_firm_id where c.region_id = ".$region_id." group by p.referring_firm_id";
		//echo $query."<br>";
		$db->setQuery($query);
		$referred_referral_id_by_region = $db->loadObjectList();
		
		
		return $referred_referral_id_by_region;
	
	
	}
	
	function get_firm_id_referred_by_country($firm_ids)
	{
		$firm_detail = $this->get_firm_details($firm_ids);
		$country_id = $firm_detail['country_id'];
		
		$db =& JFactory::getDbo();
        $query = $db->getQuery(true);
		
		$query = "select p.referring_firm_id as id from kjz42_crmery_people p left join kjz42_crmery_companies c on c.id = p.referring_firm_id where c.country_id = ".$country_id." group by p.referring_firm_id";
		//echo $query."<br>";
		$db->setQuery($query);
		$referred_referral_id_by_country = $db->loadObjectList();
		
		
		return $referred_referral_id_by_country;
	
	
	}
	
	function get_referrals_id_received_by_country($firm_ids)
	{
		$firm_detail = $this->get_firm_details($firm_ids);
		$country_id = $firm_detail['country_id'];
		
		$db =& JFactory::getDbo();
        $query = $db->getQuery(true);
		
		$query = "select p.company_id as id from kjz42_crmery_people p left join kjz42_crmery_companies c on c.id = p.referring_firm_id where c.country_id = ".$country_id." group by p.company_id";
		//echo $query."<br>";
		$db->setQuery($query);
		$received_referral_id_by_country = $db->loadObjectList();
		
		
		return $received_referral_id_by_country;
	
	
	}
	
	
	
	function get_referral_turnover_by_country($firm_ids)
	{
	
		$db =& JFactory::getDbo();
        $query = $db->getQuery(true);
		
		foreach($firm_ids as $firm_id)
		{
			$where .= " p.referring_firm_id=".$firm_id." or";
		}
		
		$where = "(".substr($where,0,-2).")";
		
		$firm_detail = $this->get_firm_details($firm_ids);
		$country_id = $firm_detail['country_id'];
		
		//$query = "select p.id from kjz42_crmery_people p inner join kjz42_crmery_companies co on p.company_id = co.id inner join kjz42_crmery_currencies cu on co.country_id = cu. country_id where $where and cu.country_id = ".$country_id;
		
		$query = "select p.id from kjz42_crmery_people p inner join kjz42_crmery_companies c on c.id = p.referring_firm_id where c.country_id = ".$country_id;
		//echo $query."<br>";
		$db->setQuery($query);
		$referral_turnover_by_country = $db->loadObjectList();
		
		
		return $referral_turnover_by_country;

	}
	
	function get_received_turnover_by_region($firm_ids)
	{
		$db =& JFactory::getDbo();
		$query = $db->getQuery(true);
		
		
		$firm_detail = $this->get_firm_details($firm_ids);
		$region_id = $firm_detail['region_id'];
		
		//$query = "select p.id from kjz42_crmery_people p inner join kjz42_crmery_companies co on p.company_id = co.id inner join kjz42_crmery_currencies cu on co.country_id = cu. country_id where $where and cu.region_id = ".$region_id;
		
		$query = "select p.id from kjz42_crmery_people p inner join kjz42_crmery_companies co on p.company_id = co.id where co.region_id = ".$region_id;
		
		$db->setQuery($query);
		$referral_turnover_id_by_region = $db->loadObjectList();

		return $referral_turnover_id_by_region;
	
	
	}
	
	

	function get_referral_turnover_by_region($firm_ids)
	{
		$db =& JFactory::getDbo();
		$query = $db->getQuery(true);
		
		foreach($firm_ids as $firm_id)
		{
	
			$where .= " p.referring_firm_id=".$firm_id." or";
	
		}
		
		$where = "(".substr($where,0,-2).")";
		
		$firm_detail = $this->get_firm_details($firm_ids);
		$region_id = $firm_detail['region_id'];
		
		$query = "select p.id from kjz42_crmery_people p inner join kjz42_crmery_companies co on p.referring_firm_id = co.id where co.region_id = ".$region_id;
		
		$db->setQuery($query);
		$referral_turnover_id_by_region = $db->loadObjectList();

		return $referral_turnover_id_by_region;
	
	
	}
	
	
	function get_referred_referrals_firmids($firm_ids)
	{
		
		foreach($firm_ids as $firm_id)
		{
			$where .= " referring_firm_id=".$firm_id." or";
		}
			$where = "(".substr($where,0,-2).")";
	
			$db =& JFactory::getDbo();
			$querysu = $db->getQuery(true);
	
			$querysu->select("id")
			->from("#__crmery_people");
			$querysu->where("$where and published>0 and Accepted=1");
	
			
			$db->setQuery($querysu);
			$previous_year_referalIds = $db->loadObjectList();
			
			return $previous_year_referalIds;
	
	}
	
	function get_received_referrals_firmids($firm_ids)
	{
		
		foreach($firm_ids as $firm_id)
		{
			$where .= " company_id=".$firm_id." or";
		}
			$where = "(".substr($where,0,-2).")";
	
			$db =& JFactory::getDbo();
			$querysu = $db->getQuery(true);
	
			$querysu->select("id")
			->from("#__crmery_people");
			$querysu->where("$where and published>0 and Accepted=1");
	
			
			$db->setQuery($querysu);
			$previous_year_referalIds = $db->loadObjectList();
			
			return $previous_year_referalIds;
	
	}
	
	
	function get_referred_countryids_previous_year($firm_ids, $year)
	{
		
		foreach($firm_ids as $firm_id)
		{
			$where .= " referring_firm_id=".$firm_id." or";
		}
		$where = "(".substr($where,0,-2).")";
	
		$db =& JFactory::getDbo();
		$querysu = $db->getQuery(true);
	
		$querysu->select("id")
			->from("#__crmery_people");
			$querysu->where("$where and published>0 and Accepted=1");
			
			if($year > 0)
	
				$querysu->where("YEAR(created) like '%$year%'");
				
			
			$db->setQuery($querysu);
			$previous_year_referalIds = $db->loadObjectList();
			
			return $previous_year_referalIds;
	
	}
	
	function get_referred_countryids_current_year($firm_ids, $year)
	{
		
		foreach($firm_ids as $firm_id)
		{
			$where .= " referring_firm_id=".$firm_id." or";
		}
	
			$where = "(".substr($where,0,-2).")";
	
			$db =& JFactory::getDbo();
			$querysu = $db->getQuery(true);
	
			$querysu->select("id")
			->from("#__crmery_people");
			$querysu->where("$where and published>0 and Accepted=1");
			
			if($year > 0)
	
				$querysu->where("YEAR(created) like '%$year%'");
				
			
			$db->setQuery($querysu);
			$current_year_referalIds = $db->loadObjectList();
			
			return $current_year_referalIds;
	
	}
	
	function get_referred_regionids_previous_year($firm_ids, $year)
	{
		
		foreach($firm_ids as $firm_id)
		{
			$where .= " referring_firm_id=".$firm_id." or";
		}
			$where = "(".substr($where,0,-2).")";
	
			$db =& JFactory::getDbo();
			$querysu = $db->getQuery(true);
	
			$querysu->select("id")
			->from("#__crmery_people");
	
			$querysu->where("$where and published>0 and Accepted=1");
	
			
			if($year > 0)
	
				$querysu->where("YEAR(created) like '%$year%'");
				
			
			$db->setQuery($querysu);
			$previous_year_referalIds = $db->loadObjectList();
			
			return $previous_year_referalIds;
	
	}
	
	function get_referred_regionids_current_year($firm_ids, $year)
	{
		
		foreach($firm_ids as $firm_id)
		{
			$where .= " referring_firm_id=".$firm_id." or";
		}
			$where = "(".substr($where,0,-2).")";
	
			$db =& JFactory::getDbo();
			$querysu = $db->getQuery(true);
	
			$querysu->select("id")
	
			->from("#__crmery_people");
	
			$querysu->where("$where and published>0 and Accepted=1");
			
			if($year > 0)
	
				$querysu->where("YEAR(created) like '%$year%'");
				
			
			$db->setQuery($querysu);
			$current_year_referalIds = $db->loadObjectList();
			
			return $current_year_referalIds;
	
	}
	
	
	function getworkReferredByServiceType($firm_ids)
	{
	
		foreach($firm_ids as $firm_id)
		{
			$where .= " p.referring_firm_id=".$firm_id." or";
		}
			$where = "(".substr($where,0,-2).")";
	
			$db =& JFactory::getDbo();
			$querysu = $db->getQuery(true);
	
			$querysu = "SELECT rp.servicetype, sum(rp.SterlingAmount) as Total FROM `#__crmery_referral_payments` rp left join #__crmery_people p on p.id = rp.referral_id where $where group by rp.servicetype ";
			$db->setQuery($querysu);
			
			$servicetype = $db->loadObjectList();
			unset($servicetype[0]);
		
			return $servicetype;
	
	}
	
	
	
	function getworkReceivedByServiceType($firm_ids)
	{
	
		foreach($firm_ids as $firm_id)
		{
			$where .= " p.company_id=".$firm_id." or";
		}
			$where = "(".substr($where,0,-2).")";
	
			$db =& JFactory::getDbo();
			$querysu = $db->getQuery(true);
	
			$querysu = "SELECT rp.servicetype, sum(rp.SterlingAmount) as Total FROM `#__crmery_referral_payments` rp left join #__crmery_people p on p.id = rp.referral_id where $where group by rp.servicetype ";
			$db->setQuery($querysu);
			
			$servicetype = $db->loadObjectList();
			unset($servicetype[0]);
			
			return $servicetype;
	
	}
	
	function getworkReferredByIndustrySector($firm_ids)
	{
	
		foreach($firm_ids as $firm_id)
		{
			$where .= " p.referring_firm_id=".$firm_id." or";
		}
			$where = "(".substr($where,0,-2).")";
	
			$db =& JFactory::getDbo();
			$querysu = $db->getQuery(true);
	
			$querysu = "SELECT d.industry, COALESCE(sum(rp.SterlingAmount),0) as Total FROM `kjz42_crmery_deals` d left join kjz42_crmery_people_cf pd on d.id = pd.association_id left join kjz42_crmery_people p on p.id = pd.person_id left join kjz42_crmery_referral_payments rp on rp.referral_id = p.id where $where group by d.industry  ";
			$db->setQuery($querysu);
			
			$referredIndustrySector = $db->loadObjectList();
			unset($referredIndustrySector[0]);
			
			return $referredIndustrySector;
	
	
	}
	
	
	function getworkReceivedByIndustrySector($firm_ids)
	{
	
		foreach($firm_ids as $firm_id)
		{
			$where .= " p.company_id=".$firm_id." or";
		}
			$where = "(".substr($where,0,-2).")";
	
			$db =& JFactory::getDbo();
			$querysu = $db->getQuery(true);
	
			$querysu = "SELECT d.industry, COALESCE(sum(rp.SterlingAmount),0) as Total FROM `kjz42_crmery_deals` d left join kjz42_crmery_people_cf pd on d.id = pd.association_id left join kjz42_crmery_people p on p.id = pd.person_id left join kjz42_crmery_referral_payments rp on rp.referral_id = p.id where $where group by d.industry  ";
			$db->setQuery($querysu);
			
			$receivedIndustrySector = $db->loadObjectList();
			unset($receivedIndustrySector[0]);
			
			return $receivedIndustrySector;
	
	
	}
	
	
	
   
}

?>