<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="dash_float" id="sales_container">
    <div class="dash_float_header">
        <div class="chart_selectors">
            <?php echo CRMText::_('COM_CRMERY_FROM'); ?> <a href="javascript:void(0);" id="dashboard_graph_date_start_label" onclick="changeGraphDate('dashboard_graph_date_start');"><?php echo CrmeryHelperUsers::getGraphDate('dashboard_graph_date_start',true); ?></a><input type="hidden" name="dashboard_graph_date_start" id="dashboard_graph_date_start" value="" /> <?php echo CRMText::_('COM_CRMERY_TO'); ?> <a href="javascript:void(0);" id="dashboard_graph_date_end_label" onclick="changeGraphDate('dashboard_graph_date_end');"><?php echo CrmeryHelperUsers::getGraphDate('dashboard_graph_date_end',true); ?></a><input type="hidden" name="dashboard_graph_date_end" id="dashboard_graph_date_end" value="" />   -   
            <a id="chart_select_prev"><?php echo CRMText::_('COM_CRMERY_PREVIOUS'); ?></a> | <a id="chart_select_next"><?php echo CRMText::_('COM_CRMERY_NEXT'); ?></a>
        </div>
        <a class="minify"></a><h2><?php echo CRMText::_('COM_CRMERY_SALES_HEADER'); ?></h2>
    </div>
    <div id="sales_graphs_container" class="container">
        <div id="sales_graphs"></div>
    </div>
</div>