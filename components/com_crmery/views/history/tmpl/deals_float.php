<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="dash_float" id="deals_container">   
    <div class="dash_float_header">
        <a class="minify"></a><h2><?php echo ucwords(CRMText::_('COM_CRMERY_RECENT_DEALS')); ?></h2>
    </div>
    <div id="deals" class="container">
        <table class="com_crmery_table" id="deal_list">
            <tr>
                <th><?php echo CRMText::_('COM_CRMERY_DEAL_NAME'); ?></th>
                <th><?php echo CRMText::_('COM_CRMERY_DEAL_STATUS'); ?></th>
                <th class="right"><?php echo CRMText::_('COM_CRMERY_DEAL_AMOUNT'); ?></th>
            </tr>
            <?php
                $n = count($this->recentDeals);
                for ( $i=0; $i<$n && $i<10; $i++ ) {
                    $deal = $this->recentDeals[$i];
                    $k = $i%2;
                    echo '<tr class="crmery_row_'.$k.'">';
                        echo '<td><a href="'.JRoute::_('index.php?option=com_crmery&view=deals&layout=deal&id='.$deal['id']).'">'.$deal['name'].'</a></td>';
                        echo '<td>'.$deal['status_name'].'<div class="status-dot" style="background-color:#'.$deal['status_color'].' !important;"></div></td>';
                        echo '<td><span class="amount">'.CrmeryHelperConfig::getConfigValue('currency').CrmeryHelperUsers::formatAmount($deal['amount']).'</span></td>';
                    echo '</tr>';
                }
            ?>
        </table>
    </div>
</div>