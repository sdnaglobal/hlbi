<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="dash_float" id="inbox_container">
    <div class="dash_float_header">
        <a id="email_loader" class="ajax_loader_static fltrt" onclick="getEmails();"></a>
        <a class="minify"></a><h2><?php echo CRMText::_('COM_CRMERY_INBOX_HEADER'); ?></h2>
    </div>
    <div id="inbox" class="container">
        <table class="com_crmery_table" id="mail_table">
            <thead>
                <tr>
                    <th><?php echo CRMText::_("COM_CRMERY_SUBJECT"); ?></th>
                    <th><?php echo CRMText::_('COM_CRMERY_FROM'); ?></th>
                    <th><?php echo CRMText::_('COM_CRMERY_TO'); ?></th>
                    <th><?php echo CRMText::_("COM_CRMERY_RECEIVED"); ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody id="mail_table_entries">
            </tbody>
        </table>
    </div>
</div>