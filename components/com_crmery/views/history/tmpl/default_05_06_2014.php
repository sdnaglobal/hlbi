<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#piechart_3d").click(function(){
    
  });
});
</script>

    <!-- 		SCRIPT FOR Total Amount Referred and Received					-->
	
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		  ['Referred Amount', 378962,378962],
          ['Received Amount', 91578,91578]
          
        ]);

        var options = {
          title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>
	
	  <!------------SCRIPT FOR TABLE FOR REFERRALS MADE BY REGIONS--------------------->
	
	<script type='text/javascript'>
      google.load('visualization', '1', {packages:['table']});
      google.setOnLoadCallback(drawTable);
      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Region');
        data.addColumn('number', 'Total Referrals Made By Region');
        data.addRows([
          ['Asia',  {v: 1400, f: '1400'}],
          ['Europe',   {v:1630,   f: '1630'}],
          ['Latin America & Caribbean', {v: 980, f: '980'}],
          ['North America',   {v: 660,  f: '660'}],
		  ['Oceania',   {v: 900,  f: '900'}],
		  ['Mexico',   {v: 580,  f: '580'}],
		  ['Middle East & Africa',   {v: 1570,  f: '1570'}]
		 
        ]);

        var table = new google.visualization.Table(document.getElementById('referral_by'));
        table.draw(data, {showRowNumber: true});
      }
    </script>
	
	<!----------------------SCRIPT FOR Number Of Referrals Made By Region---------------------->
    
    <script type="text/javascript">
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Region', 'Accepted', { role: 'annotation' }, 'Unaccepted', { role: 'annotation' }],
          ['Asia',  1000, 1000,  400,400],
          ['Europe',  1170,1170,  460,460],
          ['Latin America & Caribbean',  660,660, 320,320],
		  ['North America',  460,460, 200,200],
		  ['Oceania',  780,780, 120,120],
		  ['Mexico',  560,560, 20,20],
          ['Middle East & Africa',  1030,1030,540,540]
        ]);

        var options = {
          //title: 'Number Of Referrals Made By Region',
          hAxis: {title: 'Regions', titleTextStyle: {color: 'red'}},
		  vAxis: {title: 'Referrals', titleTextStyle: {color: 'red'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('referral_by_region'));
        chart.draw(data, options);
      }
    </script>
	
	
	 <!------------SCRIPT FOR TABLE FOR REFERRALS TO REGIONS--------------------->
	
	<script type='text/javascript'>
      google.load('visualization', '1', {packages:['table']});
      google.setOnLoadCallback(drawTable);
      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Region');
        data.addColumn('number', 'Total Referrals To Region');
        data.addRows([
          ['Asia',  {v: 1200, f: '1200'}],
          ['Europe',   {v:1830,   f: '1830'}],
          ['Latin America & Caribbean', {v: 1280, f: '1280'}],
          ['North America',   {v: 360,  f: '360'}],
		  ['Oceania',   {v: 700,  f: '700'}],
		  ['Mexico',   {v: 280,  f: '280'}],
		  ['Middle East & Africa',   {v: 570,  f: '570'}]
		 
        ]);

        var table = new google.visualization.Table(document.getElementById('referral_to'));
        table.draw(data, {showRowNumber: true});
      }
    </script>
	
	
	<!----------------------SCRIPT FOR Number Of Referrals TO Region---------------------->
    
    <script type="text/javascript">
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Region', 'Accepted', { role: 'annotation' }, 'Unaccepted', { role: 'annotation' }],
          ['Asia',  1000, 1000,  200,200],
          ['Europe',  1600,1600,  230,230],
          ['Latin America & Caribbean',  1000,1000, 280,280],
		  ['North America',  250,250, 160,160],
		  ['Oceania',  500,500, 200,200],
		  ['Mexico',  180,180, 100,100],
          ['Middle East & Africa',  500,500,70,70]
        ]);

        var options = {
          //title: 'Number Of Referrals Made By Region',
          hAxis: {title: 'Regions', titleTextStyle: {color: 'red'}},
		  vAxis: {title: 'Referrals', titleTextStyle: {color: 'red'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('referral_to_region'));
        chart.draw(data, options);
      }
    </script>
	
	
	<!------------SCRIPT FOR MONEY TABLE FOR REGIONS--------------------->
	
	<script type='text/javascript'>
      google.load('visualization', '1', {packages:['table']});
      google.setOnLoadCallback(drawTable);
      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Region');
        data.addColumn('number', 'Total Amount( in GBP )');
        data.addRows([
          ['Asia',  {v: 15789.85, f: '15789.85'}],
          ['Europe',   {v:10001.66,   f: '10001.66'}],
          ['Oceania',   {v: 6089.316,  f: '6089.316'}],
		  ['Mexico',   {v: 40567.0791,  f: '40567.0791'}]
		  ]);

        var table = new google.visualization.Table(document.getElementById('table_div1'));
        table.draw(data, {showRowNumber: true});
      }
    </script>
	
	
      
	
	<!----------------------SCRIPT FOR Total Amount Through Regions---------------------->
	
	
	<script type="text/javascript">
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
        ['Regions', 'Recceived Amount', { role: 'annotation' },'Pending Amount', { role: 'annotation' } ],
        ['Asia', 2400, 2400, 1500,1500],
        ['Europe', 1600, 1600, 2200,2200],
        ['Oceania', 2800, 2800, 1800,1800],
		['Mexico', 2800, 2800, 3900,3900],
      ]);

      var options = {
	  	title: 'Total Amount From Regions',
		hAxis: {title: 'Regions', titleTextStyle: {color: 'red'}},
		vAxis: {title: 'Amount in GBP', titleTextStyle: {color: 'red'}},
        width: 800,
        height: 500,
        bar: { groupWidth: '75%' },
        isStacked: true,
      };
        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div1'));
        chart.draw(data, options);
      }
    </script>
	
	<!----------------------SCRIPT FOR Money RECEIVED BY HLB ADMIN BY Regions---------------------->
	
	
	<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Regions', 'Amount (in GBP)'],
          ['Asia',  15789.85],
          ['Europe',  10001.66],
          ['Oceania', 6089.316],
          ['Mexico', 40567.0791],
        ]);

      var options = {
        legend: 'none',
        pieSliceText: 'label',
        //title: 'Money Received By HLB By Regions',
        pieStartAngle: 100,
      };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
      }
    </script>


  
     
 

<?php

/*------------------------------------------------------------------------

# CRMery

# ------------------------------------------------------------------------

# @author CRMery

# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.

# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Website: http://www.crmery.com

-------------------------------------------------------------------------*/

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<?php

function get_user_companies()

{

	$member_id = CrmeryHelperUsers::getUserId();

	$member_role = CrmeryHelperUsers::getRole();


	 if ( $member_role == 'manager' || $member_role == 'basic' ) // CUSTOMIZING HERE  

	  {

		 if($member_role == 'manager') 

		 { 

		    $field="company_id";

		 } 

		 if($member_role == 'basic')

		 {

		    $field="inp_company_id";

		 }


		$db =& JFactory::getDbo();

		$querysu = $db->getQuery(true);

		$querysu->select("u.$field")

		->from("#__crmery_users AS u");

		$querysu->where("u.id=".$member_id);



		$db->setQuery($querysu);

	    $comId = $db->loadResult();

		

		$p_query = $db->getQuery(true);

		$p_query->select("id")

		->from("#__crmery_companies ");

		$p_query->where("published>0 and partner_id=".$member_id." and id !=".$comId);

		 

		$db->setQuery($p_query);

		$par_Ids = $db->loadObjectList();

		

		$firmsArr = array();		

		if($par_Ids){



			foreach($par_Ids as $parid)

			{

				$firmsArr[] = $parid->id;

			}

		}

		

		$firmsArr[] = $comId;
		
		return $firmsArr;

	}


	if ( $member_role == 'exec' ){         // code for federation admin 


		$db =& JFactory::getDbo();

		$querys = $db->getQuery(true);

		$querys->select("r.region_id,r.country_id")

		->from("#__crmery_users AS r");

		$querys->where("r.id='".$member_id."'");						

		$db->setQuery($querys);

		$regIds = $db->loadObjectList();



		if ( count($regIds) > 0 ){

		foreach ( $regIds as $regId ){						

			$regionId=$regId->region_id;						

		  }

		}

		 

		$reg_Arr = @explode(',',$regionId);

		

		foreach($reg_Arr as $reg_id)

		{

			$reg_where .= " region_id=".$reg_id." or";

		}

		

		$where_reg = "(".substr($reg_where,0,-2).")";

		

		$p_query = $db->getQuery(true);

		$p_query->select("id")

		->from("#__crmery_companies ");

		$p_query->where("published>0 and $where_reg");

		 

		$db->setQuery($p_query);

		//echo $p_query;

		$par_Ids = $db->loadObjectList();

		

		$firmsArr = array();		

		if($par_Ids){



			foreach($par_Ids as $parid)

			{

				$firmsArr[] = $parid->id;

			}

		}

		
		return $firmsArr;
		

	}

	if($member_role == '' || $member_role == ' ')

	{

		 $db =& JFactory::getDbo();

		$p_query = $db->getQuery(true);

		$p_query->select("id")

		->from("#__crmery_companies ");

		$p_query->where("published>0 ");

		 

		$db->setQuery($p_query);

		//echo $p_query;

		$par_Ids = $db->loadObjectList();

		

		$firmsArr = array();		

		if($par_Ids){



			foreach($par_Ids as $parid)

			{

				$firmsArr[] = $parid->id;

			}

		}


		return $firmsArr;


	}

}



function get_send_referrals_details_by_firm_ids($firm_ids, $year=0)

{

	foreach($firm_ids as $firm_id)

	{

		$where .= " referring_firm_id=".$firm_id." or";

	}

		$where = "(".substr($where,0,-2).")";



		$db =& JFactory::getDbo();

		$querysu = $db->getQuery(true);

		$querysu->select("id")

		->from("#__crmery_people");

		$querysu->where("$where and published>0 and Accepted=1");
		
		//echo $querysu;

		if($year > 0)

			$querysu->where("YEAR(created)=$year");

		

		$db->setQuery($querysu);

	    $referalId = $db->loadObjectList();

		return $referalId;

}



function get_referrals_details_by_firm_ids($firm_ids, $year=0)

{

	foreach($firm_ids as $firm_id)

	{

		$where .= " company_id=".$firm_id." or";

	}

		$where = "(".substr($where,0,-2).")";



		$db =& JFactory::getDbo();

		$querysu = $db->getQuery(true);

		$querysu->select("id")

		->from("#__crmery_people");

		$querysu->where("$where and published>0 and Accepted=1");

		

		if($year > 0)

			$querysu->where("YEAR(created)=$year");

		

		$db->setQuery($querysu);

	    $referalId = $db->loadObjectList();

		return $referalId;

}



function get_amount_billed_by_referral_id($ref_id,$year)

{

	$db = JFactory::getDbo();

	$billing = $db->getQuery(true);

	$billing->select('LocalAmount,LocalCurrencyCode,SterlingAmount');

	$billing->from('#__crmery_referral_payments');

	$billing->where('FeePaid=1 and referral_ID='.$ref_id);

	//$billing->where('FeePaid=1');

	

	if($year>0)

	{  $year = ($year);

		$billing->where(' YEAR(STR_TO_DATE(BillingYear, "%d/%m/%Y"))='.$year);

	}
	
	
	$db->setQuery($billing);

    // die($billing);

	$billingdetails = $db->loadObjectList();

	$GBPsum = 0;

	foreach($billingdetails as $bill)

	{ 	
	
		$GBPsum = ($GBPsum+$bill->SterlingAmount);

	}

	return $GBPsum;

}



?>



<script type="text/javascript">

    var loc = "dashboard";

    var graphData = <?php echo json_encode($this->graph_data); ?>;

</script>

<h1><?php echo CRMText::_('COM_CRMERY_DASHBOARD_HEADER'); ?></h1>


<a href="javascript:void();">Total Amount Referred and Received for 2013</a>
<a href="javascript:void();">Number Of Referrals Made By Region</a>
<a href="javascript:void();">Number Of Referrals To Region</a>
<a href="javascript:void();">Total Amount From Regions</a>
<a href="javascript:void();">Money Received By HLB FROM Regions</a>


<div id="piechart_3d" style="width: 500px; height: 250px;"></div>

<span style="padding-left:50px;"><b>Number Of Referrals Made By Region</b></span>
<div id='referral_by' style="width: 450px; padding-left:100px; margin-top:30px; "></div>
<div id="referral_by_region" style="width: 800px; height: 300px;"></div>

<span style="padding-left:50px;"><b>Number Of Referrals To Region</b></span>
<div id='referral_to' style="width: 450px; padding-left:100px; margin-top:30px; "></div>
<div id="referral_to_region" style="width: 800px; height: 300px;"></div>

<div id="chart_div1" style="width: 650px; height: 550px;"></div>

<span style="padding-left:50px;"><b>Money Received By HLB FROM Regions</b></span><div id='table_div1' style="width: 450px; padding-left:100px; margin-top:30px; "></div>
<div id="piechart" style="width: 600px; height: 300px;"></div>

	






