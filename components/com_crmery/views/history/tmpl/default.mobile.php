<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<script type="text/javascript">
	var loc = "dashboard";
</script>

	<div data-role='header' data-theme='b'>
		<h1><?php echo CRMText::_('COM_CRMERY_DASHBOARD_HEADER'); ?></h1>
		<a data-icon='delete' data-role='button' href='<?php echo JRoute::_('index.php?option=com_users&task=logout'); ?>' rel='external'>
			<?php echo CRMText::_('COM_CRMERY_LOGOUT'); ?>
		</a>
	</div>

	<div data-role='content' data-theme='b'><h2><?php echo CRMText::_('COM_CRMERY_WELCOME').' '.ucwords($this->first_name); ?>!</h2>
		<ul data-inset='true' data-role='listview'>
			<li data-theme="c">
				<a href="<?php echo JRoute::_('index.php?option=com_crmery&view=events'); ?>">
				<img class='ui-li-icon' src='<?php echo JURI::root(); ?>/components/com_crmery/media/images/mobile/agenda.png' />
				<?php echo ucwords(CRMText::_('COM_CRMERY_AGENDA')); ?>
				<span class='ui-li-count'><?php echo $this->numEvents; ?></span>
				</a>
			</li>
			<li data-theme="c">
				<a href="<?php echo JRoute::_('index.php?option=com_crmery&view=deals'); ?>">
					<img class='ui-li-icon' src='<?php echo JURI::root(); ?>/components/com_crmery/media/images/mobile/deals.png' />
					<?php echo ucwords(CRMText::_('COM_CRMERY_DEALS_HEADER')); ?>
					<span class='ui-li-count'><?php echo $this->numDeals; ?></span>
				</a>
			</li>
			<li data-theme="c">
				<a href="<?php echo JRoute::_('index.php?option=com_crmery&view=people&type=leads'); ?>">
					<img class='ui-li-icon' src='<?php echo JURI::root(); ?>/components/com_crmery/media/images/mobile/leads.png' />
					<?php echo ucwords(CRMText::_('COM_CRMERY_LEADS')); ?>
					<span class='ui-li-count'><?php echo $this->numLeads; ?></span>
				</a>
			</li>
			<li data-theme="c">
				<a href="<?php echo JRoute::_('index.php?option=com_crmery&view=people&type=not_leads'); ?>">
					<img class='ui-li-icon' src='<?php echo JURI::root(); ?>/components/com_crmery/media/images/mobile/contacts.png' />
					<?php echo ucwords(CRMText::_('COM_CRMERY_CONTACTS')); ?>
					<span class='ui-li-count'><?php echo $this->numContacts; ?></span>
				</a>
			</li>
			<li data-theme="c">
				<a href="<?php echo JRoute::_('index.php?option=com_crmery&view=companies'); ?>">
					<img class='ui-li-icon' src='<?php echo JURI::root(); ?>/components/com_crmery/media/images/mobile/companies.png' />
					<?php echo ucwords(CRMText::_('COM_CRMERY_COMPANIES')); ?>
					<span class='ui-li-count'><?php echo $this->numCompanies; ?></span>
				</a>
			</li>
		</ul>
		<div style="float:right;font-size:8px;"><a href="javascript:void(0);" onclick="window.location='<?php echo JRoute::_('index.php?option=com_crmery&view=dashboard&mobile=no'); ?>';"><?php echo CRMText::_('COM_CRMERY_TOGGLE_DESKTOP_VIEW'); ?></a></div>
	</div>