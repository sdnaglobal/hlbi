<?php

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' ); ?>


<?php


$member_role = CrmeryHelperUsers::getRole();
function get_user_companies()

{

	$member_id = CrmeryHelperUsers::getUserId();

	$member_role = CrmeryHelperUsers::getRole();

	

	 if ( $member_role == 'manager' || $member_role == 'basic' ) // CUSTOMIZING HERE  

	  {

		 if($member_role == 'manager') 

		 { 

		    $field="company_id";

		 } 

		 if($member_role == 'basic')

		 {

		    $field="inp_company_id";

		 }


		$db =& JFactory::getDbo();

		$querysu = $db->getQuery(true);

		$querysu->select("u.$field")

		->from("#__crmery_users AS u");

		$querysu->where("u.id=".$member_id);



		$db->setQuery($querysu);

	    $comId = $db->loadResult();

		

		$p_query = $db->getQuery(true);

		$p_query->select("id")

		->from("#__crmery_companies ");

		$p_query->where("published>0 and partner_id=".$member_id." and id !=".$comId);

		 

		$db->setQuery($p_query);

		$par_Ids = $db->loadObjectList();

		$firmsArr = array();		

		if($par_Ids){

			foreach($par_Ids as $parid)

			{

				$firmsArr[] = $parid->id;

			}

		}

		$firmsArr[] = $comId;

		return $firmsArr;

	}

	if ( $member_role == 'exec' ){         // code for federation admin 
								

		$db =& JFactory::getDbo();

		$querys = $db->getQuery(true);

		$querys->select("r.region_id,r.country_id")

		->from("#__crmery_users AS r");

		$querys->where("r.id='".$member_id."'");						

		//$querys->where("r.id=4");						



		$db->setQuery($querys);

		$regIds = $db->loadObjectList();



		if ( count($regIds) > 0 ){

		foreach ( $regIds as $regId ){						

			$regionId=$regId->region_id;						

		  }

		}

		 

		$reg_Arr = @explode(',',$regionId);

		

		foreach($reg_Arr as $reg_id)

		{

			$reg_where .= " region_id=".$reg_id." or";

		}

		

		$where_reg = "(".substr($reg_where,0,-2).")";

		

		$p_query = $db->getQuery(true);

		$p_query->select("id")

		->from("#__crmery_companies ");

		$p_query->where("published>0 and $where_reg");

		 

		$db->setQuery($p_query);

		//echo $p_query;

		$par_Ids = $db->loadObjectList();

		

		$firmsArr = array();		

		if($par_Ids){



			foreach($par_Ids as $parid)

			{

				$firmsArr[] = $parid->id;

			}

		}

	
		return $firmsArr;

	}

	if($member_role == '' || $member_role == ' ')

	{

		 $db =& JFactory::getDbo();

		$p_query = $db->getQuery(true);

		$p_query->select("id")

		->from("#__crmery_companies ");

		$p_query->where("published>0 ");

		 

		$db->setQuery($p_query);

		//echo $p_query;

		$par_Ids = $db->loadObjectList();

		

		$firmsArr = array();		

		if($par_Ids){



			foreach($par_Ids as $parid)

			{

				$firmsArr[] = $parid->id;

			}

		}

	
		return $firmsArr;

		

	}

	
}



function get_send_referrals_details_by_firm_ids($firm_ids, $year=0)

{

	foreach($firm_ids as $firm_id)

	{

		$where .= " referring_firm_id=".$firm_id." or";

	}

		$where = "(".substr($where,0,-2).")";



		$db =& JFactory::getDbo();

		$querysu = $db->getQuery(true);

		$querysu->select("id")

		->from("#__crmery_people");

		$querysu->where("$where and published>0 and Accepted=1");

		

		if($year > 0)

			$querysu->where("YEAR(created)=$year");

		

		$db->setQuery($querysu);

	    $referalId = $db->loadObjectList();

		return $referalId;

}

function referrals_start_year($firm_ids, $year=0)

{

	foreach($firm_ids as $firm_id)

	{

		$where .= " referring_firm_id=".$firm_id." or";

	}

		$where = "(".substr($where,0,-2).")";



		$db =& JFactory::getDbo();

		$querysu = $db->getQuery(true);

		$querysu->select("min(Year(created)) as start_year")

		->from("#__crmery_people");

		$querysu->where("$where and published>0 and Accepted=1");

		$db->setQuery($querysu);

	    $referral_start = $db->loadObject();
		
		return $referral_start;
}




function get_referrals_details_by_firm_ids($firm_ids, $year=0)

{

	foreach($firm_ids as $firm_id)

	{

		$where .= " company_id=".$firm_id." or";

	}

		$where = "(".substr($where,0,-2).")";



		$db =& JFactory::getDbo();

		$querysu = $db->getQuery(true);

		$querysu->select("id")

		->from("#__crmery_people");

		$querysu->where("$where and published>0 and Accepted=1");

		

		if($year > 0)

			$querysu->where("YEAR(created)=$year");

		//echo $querysu;

		$db->setQuery($querysu);

	    $referalId = $db->loadObjectList();

		return $referalId;

}



function get_amount_billed_by_referral_id($ref_id,$year)

{

	$db = JFactory::getDbo();

	$billing = $db->getQuery(true);

	$billing->select('LocalAmount,LocalCurrencyCode,SterlingAmount');

	$billing->from('#__crmery_referral_payments');

	$billing->where('FeePaid=1 and referral_ID='.$ref_id);

	//$billing->where('FeePaid=1');

	

	if($year>0)
	{  
		$billing->where(' YEAR(STR_TO_DATE(BillingYear, "%d/%m/%Y"))='.$year);

	}

	$db->setQuery($billing);

    // die($billing);

	$billingdetails = $db->loadObjectList();

	$GBPsum = 0;

	foreach($billingdetails as $bill)
	{ 	

		$GBPsum = ($GBPsum+$bill->SterlingAmount);

	}

	return $GBPsum;

}


$db = JFactory::getDbo();
$logged_user = CrmeryHelperUsers::getUserId();

function get_referral_start_year()
{
	$db = JFactory::getDbo();
	$logged_user = CrmeryHelperUsers::getUserId();
	$year_qry = $db->getQuery(true);
	$year_qry = "SELECT Year(min(created)) FROM #__crmery_people WHERE owner_id=".$logged_user;
	$db->setQuery($year_qry);
	$created_year = $db->loadResult();
	return $created_year;
	
}

$financial_year = "SELECT currentfinancialyear FROM #__crmery_people";
$db->setQuery($financial_year);
$current_financial_year = $db->loadResult();

?>



<script type="text/javascript">

    var loc = "dashboard";

    var graphData = <?php echo json_encode($this->graph_data); ?>;

</script>

<h1><?php echo CRMText::_('COM_CRMERY_DASHBOARD_HEADER'); ?></h1>



<div class="newdashboard" > 


	<div class="inner_newdashboard_history" >

		<div class="clear"> </div>
		
			<div class="inner_left_dashboard_history">
	
				Billing Year
	
			</div>
		
			<div class="inner_left_dashboard_center">
	
				Total amount Referred
	
			</div>
			
			<div class="inner_left_dashboard_center_right">
	
				Total Amount Received
	
			</div>
			
			<div class="clear"> </div>
			
			<?php
			
			$firm_ids = get_user_companies();
		
			$referrals_send = get_send_referrals_details_by_firm_ids($firm_ids,0);
			
			$referrals_accepted = get_referrals_details_by_firm_ids($firm_ids,0);
			
			if($member_role == '')
			{
				$start = referrals_start_year($firm_ids,0);
			
				$start_year = $start->start_year;
			
			}
			else
			{
				$start_year = get_referral_start_year();
				
			}
			
			
			for($year = $current_financial_year; $year >= $start_year; $year--)
			{
			?>
			
				<div class="inner_right_dashboard_history">
				
					<?php echo $year; ?>
				
				</div>
				
				<div class="inner_right_dashboard_center">
		
					<?php
					
					$ref_total = 0;
		
					foreach($referrals_send as $referralid)
					{
		
							$ref_amount = get_amount_billed_by_referral_id($referralid->id,$year=$year);
		
							$ref_total = ($ref_total+$ref_amount);
				
					}
					 
					echo CRMText::_('COM_CRMERY_CURRENCY').' '. number_format($ref_total,2,'.',',');
						 
					
					?>
		
				</div>
				
				<div class="inner_right_dashboard_center_right">
					
					<?php
					
					$total = 0;
	
					foreach($referrals_accepted as $referralid)
					{
		
							$amount = get_amount_billed_by_referral_id($referralid->id,$year=$year);
							$total = ($total+$amount);
		
					}
					
					echo CRMText::_('COM_CRMERY_CURRENCY').' '. number_format($total,2,'.',',');
					?>
				
				</div>
			
			<?php
			}
			?>
			
			
		
	</div>
	<div class="clear"> </div>

</div>
