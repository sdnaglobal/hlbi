<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	
    <!-- 		SCRIPT FOR Total Amount Referred and Received					-->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		  ['Referred Amount', 378962,378962],
          ['Received Amount', 91578,91578]
          
        ]);

        var options = {
         // title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('amt_ref_rec'));
        chart.draw(data, options);
      }
    </script>
	
	  <!------------SCRIPT FOR TABLE FOR REFERRALS MADE BY REGIONS--------------------->
	
	<script type='text/javascript'>
      google.load('visualization', '1', {packages:['table']});
      google.setOnLoadCallback(drawTable);
      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Region');
        data.addColumn('number', 'Total Referrals Made By Region');
        data.addRows([
          ['Asia',  {v: 1400, f: '1400'}],
          ['Europe',   {v:1630,   f: '1630'}],
          ['Latin America & Caribbean', {v: 980, f: '980'}],
          ['North America',   {v: 660,  f: '660'}],
		  ['Oceania',   {v: 900,  f: '900'}],
		  ['Mexico',   {v: 580,  f: '580'}],
		  ['Middle East & Africa',   {v: 1570,  f: '1570'}]
		 
        ]);

        var table = new google.visualization.Table(document.getElementById('referral_by'));
        table.draw(data, {showRowNumber: true});
      }
    </script>
	
	<!----------------------SCRIPT FOR Number Of Referrals Made By Region---------------------->
    
    <script type="text/javascript">
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Region', 'Accepted', { role: 'annotation' }, 'Unaccepted', { role: 'annotation' }],
          ['Asia',  1000, 1000,  400,400],
          ['Europe',  1170,1170,  460,460],
          ['Latin America & Caribbean',  660,660, 320,320],
		  ['North America',  460,460, 200,200],
		  ['Oceania',  780,780, 120,120],
		  ['Mexico',  560,560, 20,20],
          ['Middle East & Africa',  1030,1030,540,540]
        ]);

        var options = {
          //title: 'Number Of Referrals Made By Region',
          hAxis: {title: 'Regions', titleTextStyle: {color: 'red'}},
		  vAxis: {title: 'Referrals', titleTextStyle: {color: 'red'}},
		  chartArea: {width:'800px;'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('referral_by_region'));
        chart.draw(data, options);
		
      }
    </script>
	
	
	 <!------------SCRIPT FOR TABLE FOR REFERRALS TO REGIONS--------------------->
	
	<script type='text/javascript'>
      google.load('visualization', '1', {packages:['table']});
      google.setOnLoadCallback(drawTable);
      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Region');
        data.addColumn('number', 'Total Referrals To Region');
        data.addRows([
          ['Asia',  {v: 1200, f: '1200'}],
          ['Europe',   {v:1830,   f: '1830'}],
          ['Latin America & Caribbean', {v: 1280, f: '1280'}],
          ['North America',   {v: 360,  f: '360'}],
		  ['Oceania',   {v: 700,  f: '700'}],
		  ['Mexico',   {v: 280,  f: '280'}],
		  ['Middle East & Africa',   {v: 570,  f: '570'}]
		 
        ]);

        var table = new google.visualization.Table(document.getElementById('referral_to'));
        table.draw(data, {showRowNumber: true});
      }
    </script>
	
	
	<!----------------------SCRIPT FOR Number Of Referrals TO Region---------------------->
    
    <script type="text/javascript">
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Region', 'Accepted', { role: 'annotation' }, 'Unaccepted', { role: 'annotation' }],
          ['Asia',  1000, 1000,  200,200],
          ['Europe',  1600,1600,  230,230],
          ['Latin America & Caribbean',  1000,1000, 280,280],
		  ['North America',  250,250, 160,160],
		  ['Oceania',  500,500, 200,200],
		  ['Mexico',  180,180, 100,100],
          ['Middle East & Africa',  500,500,70,70]
        ]);

        var options = {
          //title: 'Number Of Referrals Made By Region',
          hAxis: {title: 'Regions', titleTextStyle: {color: 'red'}},
		  vAxis: {title: 'Referrals', titleTextStyle: {color: 'red'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('referral_to_region'));
        chart.draw(data, options);
      }
    </script>
	
	
	<!------------SCRIPT FOR MONEY TABLE FOR REGIONS--------------------->
	
	<script type='text/javascript'>
      google.load('visualization', '1', {packages:['table']});
      google.setOnLoadCallback(drawTable);
      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Region');
        data.addColumn('number', 'Total Amount( in GBP )');
        data.addRows([
          ['Asia',  {v: 15789.85, f: '15789.85'}],
          ['Europe',   {v:10001.66,   f: '10001.66'}],
          ['Oceania',   {v: 6089.316,  f: '6089.316'}],
		  ['Mexico',   {v: 40567.0791,  f: '40567.0791'}]
		  ]);

        var table = new google.visualization.Table(document.getElementById('table_div1'));
        table.draw(data, {showRowNumber: true});
      }
    </script>
	
	
      
	
	<!----------------------SCRIPT FOR Total Amount Through Regions---------------------->
	
	
	<script type="text/javascript">
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
        ['Regions', 'Recceived Amount', { role: 'annotation' },'Pending Amount', { role: 'annotation' } ],
        ['Asia', 2400, 2400, 1500,1500],
        ['Europe', 1600, 1600, 2200,2200],
        ['Oceania', 2800, 2800, 1800,1800],
		['Mexico', 2800, 2800, 3900,3900],
      ]);

      var options = {
	  	//title: 'Total Amount From Regions',
		hAxis: {title: 'Regions', titleTextStyle: {color: 'red'}},
		vAxis: {title: 'Amount in GBP', titleTextStyle: {color: 'red'}},
        width: 800,
        height: 500,
        bar: { groupWidth: '75%' },
        isStacked: true,
      };
        var chart = new google.visualization.ColumnChart(document.getElementById('total_amt'));
        chart.draw(data, options);
      }
    </script>
	
	<!----------------------SCRIPT FOR Money RECEIVED BY HLB ADMIN BY Regions---------------------->
	
	
	<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Regions', 'Amount (in GBP)'],
          ['Asia',  15789.85],
          ['Europe',  10001.66],
          ['Oceania', 6089.316],
          ['Mexico', 40567.0791],
        ]);

      var options = {
        legend: 'none',
        pieSliceText: 'label',
        //title: 'Money Received By HLB By Regions',
        pieStartAngle: 100,
      };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
      }
    </script>


  
     
 

<?php

defined( '_JEXEC' ) or die( 'Restricted access' ); ?>



<h1><?php echo CRMText::_('COM_CRMERY_DASHBOARD_HEADER'); ?></h1>

<div style="padding-left:50px;"><b>Total Amount Referred and Received for 2013</b></div>
<div id="amt_ref_rec" style="width: 500px; height: 250px;"></div>


<div style="padding-left:50px;"><b>Number Of Referrals Made By Region</b></div>
<div id='referral_by' style="width: 450px; padding-left:100px; margin-top:30px; "></div>
<div id="referral_by_region" style="width: 800px; height: 300px; "></div>

<div style="padding-left:50px;"><b>Number Of Referrals To Region</b></div></br>
<div id='referral_to' style="width: 450px; padding-left:100px; margin-top:30px; "></div>
<div id="referral_to_region" style="width: 800px; height: 300px;"></div>

<div style="padding-left:50px;"><b>Total Amount From Regions</b></div>
<div id="total_amt" style="width: 650px; height: 550px;"></div>

<div style="padding-left:50px;"><b>Regions with Highest Amount</b></div>
<div id='table_div1' style="width: 450px; padding-left:100px; margin-top:30px; "></div>
<div id="piechart" style="width: 600px; height: 300px;"></div>

	






