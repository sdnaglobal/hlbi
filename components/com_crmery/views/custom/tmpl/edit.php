<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
	
	$model = & JModel::getInstance('deal','CrmeryModel');
	$dealDetail = $model->getDealDetail($this->item['id']);
	
	$custom = CrmeryHelperCustom::generateCustom($this->type,$this->item['id']);
	
	//echo "<pre>"; print_r($dealDetail); echo "</pre>";

	foreach ( $custom as $field => $value ) {
        if ( $value['type'] != 'forecast' ){
            switch ( $value['type'] ){
                case "picklist":
				    if ( $value['multiple_selections'] == 1 ){
						$custom_field_filter = CRMText::_('COM_CRMERY_CLICK_TO_EDIT');
                        $checked = array_key_exists('selected',$value) && !is_null($value['selected']) && $value['selected'] != "" && $value['selected'] != CRMText::_('COM_CRMERY_CLICK_TO_EDIT') ? @unserialize($value['selected']) : array();
                    }else{
					    $custom_field_filter = ( is_array($value) && array_key_exists('values',$value) && is_array($value['values']) && array_key_exists('selected',$value) && in_array($value['selected'],$value['values']) ) ? array_search($value['selected'],$value['values']) : CRMText::_('COM_CRMERY_CLICK_TO_EDIT');
                    }
                break;
                default:
                    $custom_field_filter = array_key_exists('selected',$value) ? $value['selected'] : "";
                break;
            }
            echo '<div class="crmeryRow">';
            echo '<div class="crmeryField">'.$value['name'].'</div>';
            echo '<div class="crmeryValue">';
                //determine type of input
                switch ( $value['type'] ){
                    case "text": ?>
                    <input class="inputbox" name="custom_<?php echo $value['id']; ?>" value="<?php echo $custom_field_filter; ?>" />
                    <?php break;
                    case "picklist": ?>
                            <?php if ( $value['multiple_selections'] == 1 ){ 
							?>
                                <ul class="unstyled">
                                    <input type="hidden" name="custom_<?php echo $value['id']; ?>[-1]" value="1" />
                                   <?php { foreach($value['values'] as $id => $name ) {?>
                                       
									   	 <?php 
										 $industryValue = $dealDetail->industry;
										 if($name == $industryValue)
										 { 
										 	$cha = "checked"; 
										 }
										 else
										 { 
										 	$cha = " "; 
										 }
										 ?>
										<li><input type="radio" name="industry" <?php echo $cha; ?> value="<?php echo $name; ?>" /><?php echo $name; ?></li>
										
                                    <?php }} ?>
                                </ul>
                            <?php } else {
							
							$ref_id = JRequest::getVar('id');
							
							if( $ref_id == '' )
							{
								$years = array("31/12/2010","31/12/2011","31/10/2012","31/10/2013");
								
								$current_financial_year_pure = CrmeryHelperCrmery::getFinancialYear();
								$current_financial_date = explode(" ",$current_financial_year_pure);
								$current_financial_year = $current_financial_date[0];
								
								$lastyear = strtotime("-1 year", strtotime($current_financial_year));
								
								$new_date = date('d/m/Y',$lastyear );
								
								$yearsToDelete = array_merge($years, array($new_date));
								
								$value['values'] = array_diff($value['values'],$yearsToDelete);
								
							}
							
							 ?>   
                            <select id="custom_<?php echo $value['id']; ?>" class="inputbox" name="custom_<?php echo $value['id']; ?>">
								
                                <?php echo JHtml::_('select.options', $value['values'], 'value', 'text', $custom_field_filter, true); ?>
                            </select>
							
                            <?php } ?>
                    <?php break;
                    case "number": ?>
                    <input class="inputbox" name="custom_<?php echo $value['id']; ?>" value="<?php echo $custom_field_filter; ?>" />
                    <?php break;
                    case "currency": ?>
                    <input class="inputbox" name="custom_<?php echo $value['id']; ?>" value="<?php echo $custom_field_filter; ?>" />
                    <?php break;
                    case "date": ?>
                    <!-- make this a custom date picker -->
                        <input id="custom_<?php echo $value['id']; ?>" name="custom_<?php echo $value['id']; ?>_input" class="inputbox filter_input date_input" type="text" value="<?php echo CrmeryHelperDate::formatDate($custom_field_filter); ?>"  />
                        <input id="custom_<?php echo $value['id']; ?>_hidden" name="custom_<?php echo $value['id']; ?>" type="hidden" value="<?php echo $custom_field_filter; ?>" />
                    <?php break; ?>
                <?php } 
            echo '</div>';
            echo '</div>';
    } }
?>