<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
	
	$custom = CrmeryHelperCustom::generateCustom($this->type,$this->item['id']);
    $count = 0;
    echo '<div class="twocolumn">';
    echo '<table class="com_crmery_table">';
    if ( count($custom) > 0 ){
    foreach ( $custom as $field => $value ) {
        $count++;
        $k=$count%3;
		
		
		
        switch ( $value['type'] ){
            case "forecast":
                $custom_field_filter = array_key_exists('forecast',$this->item) ? $this->item['forecast'] : 0;
            break;
            case "text":
                $custom_field_filter = array_key_exists('selected',$value) && strlen(trim($value['selected'])) > 0 ? $value['selected'] : CRMText::_('COM_CRMERY_CLICK_TO_EDIT'); 
            break;
            case "number":
                $custom_field_filter = array_key_exists('selected',$value) && strlen(trim($value['selected'])) > 0 ? $value['selected'] : CRMText::_('COM_CRMERY_CLICK_TO_EDIT');
            break;
            case "currency":
                $custom_field_filter = array_key_exists('selected',$value) && strlen(trim($value['selected'])) > 0 ? $value['selected'] : CRMText::_('COM_CRMERY_CLICK_TO_EDIT');
            break;
            case "date":
                $custom_field_filter = array_key_exists('selected',$value) && $value['selected'] != 0 && $value['selected'] != "0000-00-00 00:00:00" ? CrmeryHelperDate::formatDate($value['selected']) : CRMText::_('COM_CRMERY_CLICK_TO_EDIT');
            break;
            case "picklist":
                if ( $value['multiple_selections'] == 1 ){
                    $custom_field_filter = CRMText::_('COM_CRMERY_CLICK_TO_EDIT');
                    $checked = array_key_exists('selected',$value) && !is_null($value['selected']) && $value['selected'] != "" && $value['selected'] != CRMText::_('COM_CRMERY_CLICK_TO_EDIT') ? @unserialize($value['selected']) : array();
                }else{
                    $custom_field_filter = ( is_array($value) && array_key_exists('values',$value) && is_array($value['values']) && array_key_exists('selected',$value) && in_array($value['selected'],$value['values']) ) ? $value['selected'] : CRMText::_('COM_CRMERY_CLICK_TO_EDIT');
                }
            break;
        }
		
            echo '<tr>';
            echo '<th class="customFieldHead">'.$value['name'].'</th>';
            echo '<td>';
                //determine type of input
                switch ( $value['type'] ){

                    case "text":
                    case "number":
                    case "currency": ?>
                    <span class="editable parent" id="editable_custom_<?php echo $value['id']; ?>_container">
                        <div class="inline" id="editable_custom_<?php echo $value['id']; ?>">
                            <?php echo $custom_field_filter; ?>
                        </div>
                        <div class="filters editable_info">
                            <form id="<?php echo $value['id']; ?>_form">
                                <input placeholder="<?php echo CRMText::_('COM_CRMERY_CLICK_TO_EDIT'); ?>" type="text" class="inputbox" name="custom_<?php echo $value['id']; ?>" value="<?php echo $value['selected']; ?>" />
                                <input type="button" class="button" onclick="saveEditableModal('<?php echo $value['id']; ?>_form');" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" />
                            </form>
                        </div>
                    </span>
                    <?php break;
                    case "picklist":
					//echo "<pre>"; print_r($value); echo "</pre>";
					?>
					
					
                    <?php if ( $value['multiple_selections'] == 1 ){
                        echo '<div data-item="'.$this->type.'" data-field="custom_'.$value['id'].'" data-item-id="'.$this->item['id'].'" id="custom_'.$value['id'].'_field">';
                            echo '<form name="custom_'.$value['id'].'_form" id="'.$value['id'].'_form">';
                                echo '<ul class="unstyled">';
                                    if ( is_array($value) && array_key_exists('values',$value) && count($value['values']) > 0 ){ foreach($value['values'] as $id => $name ) {
                                        $isChecked = is_array($value['values']) && array_key_exists($id,$value['values']) &&  is_array($value['selected']) && array_key_exists($id,$value['selected']) && $value['selected'][$id] == 1 ? "checked='checked'" : "";
                                        echo '<li><input '.$isChecked.' name="custom_'.$value['id'].'['.$id.']" onclick="saveEditableModal(\''.$value['id'].'_form\');" type="checkbox" value="1" /> - '.$name.'</li>';
                                    }}
                                echo '</ul>';
                            echo '</form>';
                        echo '</div>'; 
                    } else { ?>
                        <a href='javascript:void(0)' class='dropdown' id='custom_<?php echo $value['id']; ?>_field_link'><?php echo $custom_field_filter; ?></a>
                    <?php 
                        echo '<div class="filters" data-item="'.$this->type.'" data-field="custom_'.$value['id'].'" data-item-id="'.$this->item['id'].'" id="custom_'.$value['id'].'_field">';
                            echo '<ul>';
                                if ( is_array($value) && array_key_exists('values',$value) && count($value['values']) > 0 ){ foreach($value['values'] as $id => $name ) {
                                    echo '<li><a href="javascript:void(0)" class="dropdown_item" data-value="'.$id.'">'.$name.'</a></li>';
                                }}
                            echo '</ul>';
                        echo '</div>'; 
                    } ?>
                    <?php break;

                    case "forecast" ?>
                    <span id="custom_<?php echo $value['id']; ?>" value="<?php echo $custom_field_filter; ?>" class="forecast">
                        <?php echo CrmeryHelperConfig::getCurrency().$custom_field_filter; ?>
                    </span>
                    <?php break;

                    case "date": ?>
                    <!-- make this a custom date picker -->
                        <form name="custom_<?php echo $value['id'];?>_form" id="custom_<?php echo $value['id']; ?>_form">
                            <input class="inputbox-hidden date_input" id="custom_<?php echo $value['id']; ?>" name="custom_<?php echo $value['id']; ?>_hidden" type="text" placeholder="<?php echo CRMText::_('COM_CRMERY_CLICK_TO_EDIT'); ?>"  value="<?php echo $custom_field_filter; ?>"  />
                            <input type="hidden" id="custom_<?php echo $value['id']; ?>_hidden" name="custom_<?php echo $value['id']; ?>" value="<?php echo $custom_field_filter; ?>"  />
                        </form>
                    <?php break; ?>

                <?php } 
            echo '</td>';
            echo '</tr>';
        }
    }
    echo '</table>';                
    echo '</div>';
?>