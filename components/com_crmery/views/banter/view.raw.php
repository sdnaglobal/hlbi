<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewBanter extends JView
{
    function display($tpl = null)
    {

    	$layout = $this->getLayout();

		switch ( $layout ){
			case "transcripts":
				$this->transcripts = CrmeryHelperTranscriptlists::getTranscripts();
			break;
		}
	
		//display
		parent::display($tpl);

    }

}