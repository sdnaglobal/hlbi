<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewMail extends JView
{
	function display($tpl = null)
	{
		$model =& JModel::getInstance('mail','CrmeryModel');
        $this->mail = $model->getMail();
	
		//display
		parent::display($tpl);
	}
	
}
?>
