<?php

/*------------------------------------------------------------------------

# CRMery

# ------------------------------------------------------------------------

# @author CRMery

# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.

# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Website: http://www.crmery.com

-------------------------------------------------------------------------*/

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' ); 



jimport( 'joomla.application.component.view');



class CrmeryViewDashboard extends CrmeryHelperView

{

	function display($tpl = null)

	{



		//get model and retrieve info

		$model = & JModel::getInstance('event','CrmeryModel');



		if(CrmeryHelperTemplate::isMobile()) {

			$model->set('current_events',true);

		}



		$model->set("limit","20");

		$model->set('filter_order_Dir','asc');

		$events = $model->getEvents();

		$ref = array( array('ref'=>'events','data'=>$events) );

		$eventDock = CrmeryHelperView::getView('events','dashboard_event_dock',$ref);



		$dealModel =& JModel::getInstance('Deal','CrmeryModel');

		$dealModel->set('recent',true);

		$dealModel->set("ordering","d.last_viewed DESC");

		$dealModel->set('archived',0);

		$dealModel->set("limitOverride",true);

		$dealModel->set("limit",20);

		$recentDeals = $dealModel->getDeals();

        

		// load java libs

		$doc = JFactory::getDocument();

        $doc->addScript( JURI::base().'components/com_crmery/media/js/highcharts.js' );

        $doc->addScript( JURI::base().'components/com_crmery/media/js/dashboard.js' );

        $doc->addScriptDeclaration('var graphType="dashboard";');

        $doc->addScriptDeclaration('var homePageChart="'.CrmeryHelperUsers::getHomePageChart().'";');

		

        //get data for sales graphs

        $model =& JModel::getInstance('graphs','CrmeryModel');

        $model->set("graphType","dashboard");

        $graph_data = $model->getGraphData();



		//assign results to view

		$this->assignRef('eventDock',$eventDock);

        $this->assignRef('graph_data',$graph_data);

        $this->assignRef('recentDeals', $recentDeals);



        if(CrmeryHelperTemplate::isMobile()) {



        	$dealModel->set('recent',false);

	        $totalDeals = $dealModel->getTotal();



	        $personModel = JModel::getInstance('People','CRmeryModel');

	        $personModel->set('type','leads');

	        $totalLeads = $personModel->getTotal();



	        $personModel->set('type','not_leads');

	        $totalContacts = $personModel->getTotal();



	        $companyModel = JModel::getInstance('Company','CRMeryModel');

	        $totalCompanies = $companyModel->getTotal();



	        $user = CRMeryHelperUsers::getLoggedInUser();

	        $this->assignRef('first_name',$user->first_name);



	        $user_id = $user->id;

	        $team_id = CrmeryHelperUsers::getTeamId();

	        $member_role = CrmeryHelperUsers::getRole();



	        $this->assignRef('numEvents', count($events));

	        $this->assignRef('numDeals', CrmeryHelperUsers::getDealCount($user_id,$team_id,$member_role));

	        $this->assignRef('numLeads', CrmeryHelperUsers::getLeadCount($user_id,$team_id,$member_role));

	        $this->assignRef('numContacts', CrmeryHelperUsers::getPeopleCount($user_id,$team_id,$member_role));

	        $this->assignRef('numCompanies', CrmeryHelperUsers::getCompanyCount($user_id,$team_id,$member_role));



        }



        $peopleModel =& JModel::getInstance('people','CrmeryModel');

        $json = TRUE;

        $peopleNames = $peopleModel->getPeopleNames($json);

        $doc->addScriptDeclaration("var people_names=".$peopleNames.";");



        $dealModel =& JModel::getInstance('deal','CrmeryModel');

        $json = TRUE;

        $dealNames = $dealModel->getDealNames($json);

        $doc->addScriptDeclaration("var deal_names=".$dealNames.";");



        /** get latest activities **/

        $activityHelper = new CRMeryHelperActivity;

        $activity = $activityHelper->getActivity();

        $latest_activities = CrmeryHelperView::getView('activities','default');

        $latest_activities_view = CrmeryHelperView::getView('activities','latest_activities');

        $latest_activities_view->assignRef('activity',$activity);

        $latest_activities->assignRef('latest_activities',$latest_activities_view);

        $this->assignRef('latest_activities',$latest_activities);



        $dashFloats = CrmeryHelperUsers::getFloats('dashboard');

        $dash_floats_left = $dashFloats['left'];

        $dash_floats_right = $dashFloats['right'];

        $this->assignRef('dash_floats_left',$dash_floats_left);

        $this->assignRef('dash_floats_right',$dash_floats_right);

		

		//display

		parent::display($tpl);



	}

	

}

?>

