<?php

/*------------------------------------------------------------------------

# CRMery

# ------------------------------------------------------------------------

# @author CRMery

# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.

# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Website: http://www.crmery.com

-------------------------------------------------------------------------*/

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<?php

$db = JFactory::getDBO();
$query = $db->getQuery(true);

/*
$db1 = JFactory::getDBO();
$query1 = $db1->getQuery(true);


$query1->select("`values`")
	->from("`kjz42_crmery_people_custom`");
	$query1->where("`name` like '%YearTo%'");
$db1->setQuery($query1);
$countryIds = $db1->loadObjectList();	

$da = $countryIds[0]->values;

$dat = explode(",",$da);

echo $query1;

echo "<pre>";
print_r($dat);
die;
*/

function get_user_companies()

{



	$member_id = CrmeryHelperUsers::getUserId();

	$member_role = CrmeryHelperUsers::getRole();

	

	 if ( $member_role == 'manager' || $member_role == 'basic' ) // CUSTOMIZING HERE  

	  {

		 if($member_role == 'manager') 

		 { 

		    $field="company_id";

		 } 

		 if($member_role == 'basic')

		 {

		    $field="inp_company_id";

		 }

							

	

		$db =& JFactory::getDbo();

		$querysu = $db->getQuery(true);

		$querysu->select("u.$field")

		->from("#__crmery_users AS u");

		$querysu->where("u.id=".$member_id);



		$db->setQuery($querysu);

	    $comId = $db->loadResult();

		

		$p_query = $db->getQuery(true);

		$p_query->select("id")

		->from("#__crmery_companies ");

		$p_query->where("published>0 and partner_id=".$member_id." and id !=".$comId);

		 

		$db->setQuery($p_query);

		$par_Ids = $db->loadObjectList();

		

		$firmsArr = array();		

		if($par_Ids){



			foreach($par_Ids as $parid)

			{

				$firmsArr[] = $parid->id;

			}

		}

		$firmsArr[] = $comId;

		return $firmsArr;
		

	}

	

	if ( $member_role == 'exec' ){         // code for federation admin 

	

									

		$db =& JFactory::getDbo();

		$querys = $db->getQuery(true);

		$querys->select("r.region_id,r.country_id")

		->from("#__crmery_users AS r");

		$querys->where("r.id='".$member_id."'");						

		//$querys->where("r.id=4");						



		$db->setQuery($querys);

		$regIds = $db->loadObjectList();



		if ( count($regIds) > 0 ){

		foreach ( $regIds as $regId ){						

			$regionId=$regId->region_id;						

		  }

		}

		 

		$reg_Arr = @explode(',',$regionId);

		

		foreach($reg_Arr as $reg_id)

		{

			$reg_where .= " region_id=".$reg_id." or";

		}

		

		$where_reg = "(".substr($reg_where,0,-2).")";

		

		$p_query = $db->getQuery(true);

		$p_query->select("id")

		->from("#__crmery_companies ");

		$p_query->where("published>0 and $where_reg");

		 

		$db->setQuery($p_query);

		//echo $p_query;

		$par_Ids = $db->loadObjectList();

		

		$firmsArr = array();		

		if($par_Ids){



			foreach($par_Ids as $parid)

			{

				$firmsArr[] = $parid->id;

			}

		}

		return $firmsArr;

	}

	if($member_role == '' || $member_role == ' ')

	{

		 $db =& JFactory::getDbo();

		$p_query = $db->getQuery(true);

		$p_query->select("id")

		->from("#__crmery_companies ");

		$p_query->where("published>0 ");

		 

		$db->setQuery($p_query);

		//echo $p_query;

		$par_Ids = $db->loadObjectList();

		

		$firmsArr = array();		

		if($par_Ids){



			foreach($par_Ids as $parid)

			{

				$firmsArr[] = $parid->id;

			}

		}


		return $firmsArr;

	}

}



function get_send_referrals_details_by_firm_ids($firm_ids, $year=0)

{

	foreach($firm_ids as $firm_id)

	{

		$where .= " referring_firm_id=".$firm_id." or";

	}

		$where = "(".substr($where,0,-2).")";



		$db =& JFactory::getDbo();

		$querysu = $db->getQuery(true);

		$querysu->select("id")

		->from("#__crmery_people");

		$querysu->where("$where and published>0 and Accepted=1");

		

		if($year > 0)

			$querysu->where("YEAR(created)=$year");

		

		$db->setQuery($querysu);

	    $referalId = $db->loadObjectList();

		return $referalId;

}





function get_referrals_details_by_firm_ids($firm_ids, $year=0)

{

	foreach($firm_ids as $firm_id)

	{

		$where .= " company_id=".$firm_id." or";

	}

		$where = "(".substr($where,0,-2).")";



		$db =& JFactory::getDbo();

		$querysu = $db->getQuery(true);

		$querysu->select("id")

		->from("#__crmery_people");

		$querysu->where("$where and published>0 and Accepted=1");

		if($year > 0)

			$querysu->where("YEAR(created)=$year");
		

		$db->setQuery($querysu);

	    $referalId = $db->loadObjectList();

		return $referalId;

}



function get_amount_billed_by_referral_id($ref_id,$year)

{

	$db = JFactory::getDbo();

	$billing = $db->getQuery(true);

	$billing->select('LocalAmount,LocalCurrencyCode,SterlingAmount');

	$billing->from('#__crmery_referral_payments');

	$billing->where('FeePaid=1 and referral_ID='.$ref_id);

	//$billing->where('FeePaid=1');

	

	if($year>0)

	{  $year = ($year);

		$billing->where(' YEAR(STR_TO_DATE(BillingYear, "%d/%m/%Y"))='.$year);

	}

	$db->setQuery($billing);

    // die($billing);

	$billingdetails = $db->loadObjectList();

	$GBPsum = 0;

	
	foreach($billingdetails as $bill)

	{ 	
		$GBPsum = ($GBPsum+$bill->SterlingAmount);

	}

	return $GBPsum;
}



?>


<div class="newdashboard" >
<h1><?php echo CRMText::_('COM_CRMERY_DASHBOARD_HEADER'); ?></h1>
 
 	<div class="ajax_loader"></div>
	<div style=" float:right;">
			<?php
			$member_role = CrmeryHelperUsers::getRole();
			
			$current_financial_year = CrmeryHelperCrmery::getFinancialYear();

			if($member_role == '')
			{
			?>
				<div class="current_billing"><label id="current_billing_year" value = "31/10/<?php echo $current_financial_year; ?>" >Current Billing Year Is <?php echo $current_financial_year; ?> </label>
				<label id="next_billing_year" value = "<?php echo ($current_financial_year+2); ?>" ></label>
				</div>
				<!--
				<select name="changeBillingYear" class="inputbox" id="changeBillingYear">
					<option value="31/10/2013">Click to change billing year</option>
					<option value="2012">31/10/2012</option>
					<option value="2013">31/10/2013</option>
					<option value="2014">31/10/2014</option>
				</select>
				-->
				<div ><a class="current_billing_button"  href="javascript:void(0);" >Click To Change Billing For Next Year</a></div>
			<?php
			}
			?>
	 </div>
	
	<div class="inner_newdashboard" >
		
		<div class="clear"> </div>

	    <div class="inner_left_dashboard">

			Total amount referred so far in <?php echo $current_financial_year; ?> year

		</div>

		<div class="inner_right_dashboard">

			<?php 

			$ref_total = 0;

			$firm_ids = get_user_companies();

			 $referrals_send = get_send_referrals_details_by_firm_ids($firm_ids,0);
			 
			 

			 foreach($referrals_send as $referralid)
			 {

					$ref_amount = get_amount_billed_by_referral_id($referralid->id,$year=$current_financial_year);
					$ref_total = ($ref_total+$ref_amount);

			 }

			 echo CRMText::_('COM_CRMERY_CURRENCY').' '. number_format($ref_total,2,'.',',');

			?>	

		</div>
		<div class="clear"> </div>
        <div class="inner_left_dashboard">

			Total amount received so far in <?php echo $current_financial_year; ?> year

		</div>

		<div class="inner_right_dashboard">

			<?php 

			$total = 0;

			$referrals_accepted = get_referrals_details_by_firm_ids($firm_ids,0);

			foreach($referrals_accepted as $referralid)
			{
				$amount = get_amount_billed_by_referral_id($referralid->id,$year=$current_financial_year);
				$total = ($total+$amount);

			 }

			 echo CRMText::_('COM_CRMERY_CURRENCY').' '. number_format($total,2,'.',',');

			?>		

		</div>

	</div>

</div>
