<?php
error_reporting(0);
/*------------------------------------------------------------------------

# CRMery

# ------------------------------------------------------------------------

# @author CRMery

# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.

# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Website: http://www.crmery.com

-------------------------------------------------------------------------*/

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' ); 



jimport( 'joomla.application.component.view');



class CrmeryViewCharts extends CrmeryHelperView

{



	function display($tpl = null)
	{
		$chartYear = JRequest::getVar('chartYear');
		
		
		$yyear = date("Y");
		
		$model = & JModel::getInstance('charts','CrmeryModel');
		
		
		$firm_ids = $model->get_user_companies();

		//echo "<pre>"; print_r($firm_ids); echo "</pre>";
		//die;
		$firm_detail = $model->get_firm_details($firm_ids);
		$firm_name = $firm_detail['name'];
		$firm_country_name = $firm_detail['country_name'];
		$firm_region_name = $firm_detail['region_name'];
		
		
		$receiving_firm_ids = $model->get_receiving_companies($firm_ids);
					
		$member_role = CrmeryHelperUsers::getRole();
		
		// 	GET BILLING YEAR DROP DOWN
		
		$startingYear = $model->getBillingYear($firm_ids); 
		
		$current_financial_year_pure = CrmeryHelperCrmery::getFinancialYear();
		$current_financial_date = explode("-",$current_financial_year_pure);
		$current_financial_year = $current_financial_date[0];
		
		//echo "<pre>"; print_r($billYear); die;
				
		$financial_year = date('d/m/Y', strtotime($current_financial_year_pure));
		
		if($chartYear == "")
		{
			$sYear = explode("/",$financial_year);
			$billYear  = $sYear['2'];
		}
		else
		{
			$billYear  = $chartYear;
		}
		
		$cYear = $billYear;
		
		$billing_year = $current_financial_year_pure;

		$curr_date = date("Y/m/d");
		 
		$billing_year = strtotime($billing_year);
		$curr_date = strtotime($curr_date);
		
		$firm_total = 0;
		$ref_total = 0;
		$country_turnover = 0;
		$region_turnover = 0;
		$years = '';
		$reffered_firm_total_amount = 0;
		
		$firm_ids_from_region = $model->get_firm_ids_from_region($firm_ids,'');
		
		$total_referred_from_region = $model->get_total_referred_from_region($firm_ids_from_region);
		
		//	TOTAL NUMBER OF REFERRALS REFERRED BY COUNTRY BELONGS TO LOGGED IN FIRM
		
		$selectedYear = $billYear;
		$yearLastWords = substr($billYear,-2);
		
		$referralsReferredByCountry = $model->getReferralsReferredByCountry($firm_ids,$selectedYear);
		

		
		//	TOTAL NUMBER OF REFERRALS RECEIVED BY COUNTRY BELONGS TO LOGGED IN FIRM
		 $referalsReceivedByCountry = $model->getReferralsReceivedByCountry($firm_ids,$selectedYear);
		 
		 //	AMOUNT REFERRED
		 $totalAmountReferred = $model->getAmountReferred($firm_ids,$selectedYear);
		 
		 //	AMOUNT RECEIVED
		 $totalAmountReceived = $model->getAmountReceived($firm_ids,$selectedYear);

		//	WORK REFERRED BY SOURCE
		$workReferredBySource = $model->getWorkReferredReceivedBySource($firm_ids,$yearLastWords,"p.referring_firm_id= ");


		//	WORK RECEIVED BY SOURCE
		$workReceivedBySource = $model->getWorkReferredReceivedBySource($firm_ids,$yearLastWords,"p.company_id= ");

		//	WORK REFERRED BY SERVICE TYPE
		$workReferredByServiceType = $model->getworkReferredReceivedByServiceType($firm_ids,$yearLastWords,"p.referring_firm_id= ");	
		
		//	WORK RECEIVED BY SERVICE TYPE
		$workReceivedByServiceType = $model->getworkReferredReceivedByServiceType($firm_ids,$yearLastWords,"p.company_id= ");		
		
		//	WORK REFERRED BY INDUSTRY SECTOR
		$workReferredByIndustrySector = $model->getworkReferredReceivedByIndustrySector($firm_ids,$yearLastWords,"p.referring_firm_id= ");
		
		//	WORK RECEIVED BY INDUSTRY SECTOR
		$workReceivedByIndustrySector = $model->getworkReferredReceivedByIndustrySector($firm_ids,$yearLastWords,"p.company_id= ");
		
		//	 TOTAL AMOUNT REFERRED BY FIRM
		$firmAmountReferred = $model->getAmountReferredByFirm($firm_ids,$yearLastWords,"p.referring_firm_id= ");

		//	BILLED AND UNBILLED REFERRED REFERRALS AMOUNT
		$totalReferredReferrals = $model->getTotalReferredReferrals($firm_ids,$yearLastWords);
		$totalReferredMadePayments = $model->getTotalReferredMadePayments($firm_ids,$yearLastWords);
		
		$leftReferralPayments = $totalReferredReferrals - $totalReferredMadePayments;
		
		//$billed_reffered_amount = $model->get_amount_billed_by_referred_firm($firm_ids,$yearLastWords);
		//$unbilled_referrals =  $firmAmountReferred - $billed_reffered_amount ;
		
		//	 TOTAL AMOUNT RECEIVED BY FIRM
		$firmAmountReceived = $model->getAmountReferredByFirm($firm_ids,$yearLastWords,"p.company_id= ");
		
		//	BILLED AND UNBILLED RECEIVED REFERRALS AMOUNT
		$totalReceivedReferrals = $model->getTotalReceivedReferrals($firm_ids,$yearLastWords);
		$totalReceivedMadePayments = $model->getTotalReceivedMadePayments($firm_ids,$yearLastWords);
		
		$leftReceivedPayments = $totalReceivedReferrals - $totalReceivedMadePayments;
		
		
		//$billed_received_referrals = $model->get_amount_billed_by_referred_firm($receiving_firm_ids,$yearLastWords);
		//$unbilled_received_referrals =  $firmAmountReceived - $billed_received_referrals ;
		
		
		
		/*		REFERRED TURN OVER STARTS FROM HERE		*/
		
		//	REFERRED FIRM TURNOVER BILLING AMOUNT
		
		$referred_firm_turnover_fee_income_promotional_gbp = $model->get_fee_income_promotional_gbp($firm_ids,$yearLastWords);
		if($referred_firm_turnover_fee_income_promotional_gbp == 0)
		{
			$firm_turnover = '0';
		}
		else
		{
			$firm_turnover = ($firmAmountReferred/$referred_firm_turnover_fee_income_promotional_gbp);
		}	

		
		//	country turn over
		$country_turnover = $model->getAmountReferredByCountry($firm_ids,$yearLastWords,"p.referring_firm_id");
		
		//	FIRST GETTING THE FIRMS WHICH ARE REFERRED BY COUNTRY AND THEN TAKING TURNOVER BILLING AMOUNT FROM COMPANY BILLING TABLE
		$country_firm_ids = $model->get_firm_id_of_country($firm_ids);
		$country_turnover_fee_income_promotional_gbp = $model->get_coureg_fee_income_promotional_gbp($country_firm_ids,$yearLastWords);
		
		if($country_turnover_fee_income_promotional_gbp == 0)
		{
			$country_turnover = '0';
		}
		else
		{
			$country_turnover = $country_turnover/$country_turnover_fee_income_promotional_gbp;
		}
		
		
		//	region turn over	
		$region_turnover = $model->getAmountRefRecByRegion($firm_ids,$yearLastWords,"p.referring_firm_id");
		
		//	REFERRED REGION TURNOVER BILLING AMOUNT
		$region_firm_ids = $model->get_firm_id_of_region($firm_ids);
		$region_turnover_fee_income_promotional_gbp = $model->get_coureg_fee_income_promotional_gbp($region_firm_ids,$yearLastWords);
		if($region_turnover_fee_income_promotional_gbp == 0)
		{
			$region_turnover = '0';
		}
		else
		{
			$region_turnover = $region_turnover/$region_turnover_fee_income_promotional_gbp;
		}
		
		
		
		/*	REFERRED TURN OVER ENDS HERE		*/
		
		
		
		
		/*	RECEIVED TURN OVER STARTS FROM HERE		*/
		
		//	RECEIVING FIRM TURNOVER
		$firm_receiving_turnover_fee_income_promotional_gbp = $model->get_fee_income_promotional_gbp($receiving_firm_ids,$yearLastWords);
		
		//	firm received turn over	
		if($firm_receiving_turnover_fee_income_promotional_gbp == 0)
		{
			$firm_received_turnover = '0';
		}
		else
		{
			$firm_received_turnover = $firmAmountReceived/$firm_receiving_turnover_fee_income_promotional_gbp;
		}
		

		
		//	country received turn over	
		$country_rec_turnover = $model->getAmountReferredByCountry($firm_ids,$yearLastWords,"p.company_id");
					
		//	RECEIVED COUNTRY TURNOVER BILLING AMOUNT
		$receiving_country_firm_ids = $model->get_referrals_id_received_by_country($firm_ids);
		//$receiving_country_turnover_fee_income_promotional_gbp = $model->get_coureg_fee_income_promotional_gbp($receiving_country_firm_ids,$yearLastWords);
		if($receiving_country_turnover_fee_income_promotional_gbp == '')
		{
			$country_received_turnover = '0';
		}
		else
		{
			$country_received_turnover = $country_rec_turnover/$receiving_country_turnover_fee_income_promotional_gbp;
		}
		
		
		
		//	region received turn over
		$region_rec_turnover = $model->getAmountRefRecByRegion($firm_ids,$yearLastWords,"p.company_id");
		
		//	RECEIVED COUNTRY TURNOVER BILLING AMOUNT
		$receiving_region_firm_ids = $model->get_referrals_id_received_by_region($firm_ids);
		//$receiving_region_turnover_fee_income_promotional_gbp = $model->get_coureg_fee_income_promotional_gbp($receiving_region_firm_ids,$yearLastWords);
		if($receiving_region_turnover_fee_income_promotional_gbp == '')
		{
			$region_received_turnover = '0';
		}
		else
		{
			$region_received_turnover = $region_rec_turnover/$receiving_region_turnover_fee_income_promotional_gbp;
		}
		//echo "<pre>"; print_r($region_received_turnover); die;	
		
		
		/*		RECEIVED TURN OVER ENDS HERE		*/




/*		COMPARSION CHARTS FOR REFERRED REFERRALS START FROM HERE		*/
		
		//	firms previous and current data
		
		
		$str = str_replace("/", "-", $billYear, $billYears);
		$newdate = strtotime ( '-1 year' , strtotime ( $str ) ) ;
		$previousYear = date ( 'j/m/Y' , $newdate );
		
		$prevyearLastWords = $yearLastWords-1;
		
		$firm_total_current_year = $model->getAmountReferredByFirm($firm_ids,$yearLastWords,"p.referring_firm_id= ");
		$firm_total_previous_year = $model->getAmountReferredByFirm($firm_ids,$prevyearLastWords,"p.referring_firm_id= ");
		
		//	country data	
		
		$country_total_current_year = $model->getAmountReferredByCountry($firm_ids,$yearLastWords,"p.referring_firm_id");
		$country_total_previous_year = $model->getAmountReferredByCountry($firm_ids,$prevyearLastWords,"p.referring_firm_id");
					
		
		//	region data	
		$region_total_current_year = $model->getAmountRefRecByRegion($firm_ids,$yearLastWords,"p.referring_firm_id");
		$region_total_previous_year = $model->getAmountRefRecByRegion($firm_ids,$prevyearLastWords,"p.referring_firm_id");
		
					
		
		
		/*	REFERRED REFERRALS COMPARSION CHARTS ENDS HERE		*/
		
		
				
		/*	RECEIVED COMPARSION CHARTS START FROM HERE		*/
		
		//	firms previous and current data
		
		$received_firm_total_current_year = $model->getAmountReferredByFirm($firm_ids,$yearLastWords,"p.company_id= ");
		$received_firm_total_previous_year = $model->getAmountReferredByFirm($firm_ids,$prevyearLastWords,"p.company_id= ");
		
				
		//	country data
		$received_country_total_current_year = $model->getAmountReferredByCountry($firm_ids,$yearLastWords,"p.company_id");
		$received_country_total_previous_year = $model->getAmountReferredByCountry($firm_ids,$prevyearLastWords,"p.company_id");
						
		
		//	region data	
		
		$received_region_total_current_year = $model->getAmountRefRecByRegion($firm_ids,$yearLastWords,"p.company_id");
		$received_region_total_previous_year = $model->getAmountRefRecByRegion($firm_ids,$prevyearLastWords,"p.company_id");
//		echo "<pre>"; print_r($received_region_total_previous_year); die;							
		
		
		/*	COMPARSION CHARTS FOR RECEIVED REFERRALS ENDS HERE		*/

		
		
		
		$this->assignRef('financial_year',$financial_year);
		$this->assignRef('startingYear',$startingYear);
		$this->assignRef('billYear',$billYear);
		
		$this->assignRef('selectedYear',$selectedYear);
		$this->assignRef('cYear',$cYear);
		
		$this->assignRef('firmAmountReferred',$firmAmountReferred);
		$this->assignRef('firmAmountReceived',$firmAmountReceived);
		
		$this->assignRef('totalAmountReferred',$totalAmountReferred);
		$this->assignRef('totalAmountReceived',$totalAmountReceived);
		
		$this->assignRef('firm_name',$firm_name);
		$this->assignRef('firm_country_name',$firm_country_name);
		$this->assignRef('firm_region_name',$firm_region_name);
		
		$this->assignRef('totalReferredMadePayments',$totalReferredMadePayments);
		$this->assignRef('leftReferralPayments',$leftReferralPayments);
		
		
		$this->assignRef('totalReceivedMadePayments',$totalReceivedMadePayments);
		$this->assignRef('leftReceivedPayments',$leftReceivedPayments);
		
				
		$this->assignRef('billed_reffered_amount',$billed_reffered_amount);
		$this->assignRef('unbilled_referrals',$unbilled_referrals);
		
		$this->assignRef('billed_received_referrals',$billed_received_referrals);
		$this->assignRef('unbilled_received_referrals',$unbilled_received_referrals);
		
		
		$this->assignRef('ref_total',$ref_total);
		
		$this->assignRef('workReferredBySource',$workReferredBySource);
		$this->assignRef('workReceivedBySource',$workReceivedBySource);
		
		
		$this->assignRef('workReferredByServiceType',$workReferredByServiceType);
		$this->assignRef('workReceivedByServiceType',$workReceivedByServiceType);
		
		$this->assignRef('workReferredByIndustrySector',$workReferredByIndustrySector);
		
		
		$this->assignRef('referralsReferredByCountry',$referralsReferredByCountry);
		$this->assignRef('referalsReceivedByCountry',$referalsReceivedByCountry);
		
		$this->assignRef('firm_turnover',$firm_turnover);
		$this->assignRef('country_turnover',$country_turnover);
		$this->assignRef('region_turnover',$region_turnover);
		
		$this->assignRef('firm_received_turnover',$firm_received_turnover);
		$this->assignRef('country_received_turnover',$country_received_turnover);
		$this->assignRef('region_received_turnover',$region_received_turnover);
		
		$this->assignRef('firm_total_previous_year',$firm_total_previous_year);
		$this->assignRef('firm_total_current_year',$firm_total_current_year);
		
		$this->assignRef('country_total_previous_year',$country_total_previous_year);
		$this->assignRef('country_total_current_year',$country_total_current_year);
		
		$this->assignRef('region_total_previous_year',$region_total_previous_year);
		$this->assignRef('region_total_current_year',$region_total_current_year);
		
		
		$this->assignRef('received_firm_total_previous_year',$received_firm_total_previous_year);
		$this->assignRef('received_firm_total_current_year',$received_firm_total_current_year);
		
		$this->assignRef('received_country_total_previous_year',$received_country_total_previous_year);
		$this->assignRef('received_country_total_current_year',$received_country_total_current_year);
		
		$this->assignRef('received_region_total_previous_year',$received_region_total_previous_year);
		$this->assignRef('received_region_total_current_year',$received_region_total_current_year);
		
		
	
		parent::display($tpl);



	}

	

}

?>

