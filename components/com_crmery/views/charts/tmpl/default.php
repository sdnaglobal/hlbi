<?php

defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<?php 
	 error_reporting(0);
	 
	 $billYear =  $this->billYear;
	 
	 $selectedYear =  $this->selectedYear;
	 $cYear =  $this->cYear;
	 $previous = $cYear-1;
	 
	 
	 $startingYear =  $this->startingYear;
	 //echo "<pre>"; print_r($startingYear); die;
	 $sYear = explode("-",$startingYear);
	 $startYear = $sYear[0];
	 
	 $financial_year =  $this->financial_year;
	 $f_year = explode("/",$financial_year);
	 $fin_year = $f_year[2];
	 
	 $ref_total =  $this->ref_total;
	 $firmAmountReferred = $this->firmAmountReferred;
	 $firmAmountReceived = $this->firmAmountReceived;
	 
	 $firm_name = $this->firm_name;
	 $firm_country_name = $this->firm_country_name;
	 $firm_region_name = $this->firm_region_name;
	 
	 // 	WORK REFERRED RESULT IN BILLING
	 $totalReferredMadePayments = $this->totalReferredMadePayments;
	 $leftReferralPayments = $this->leftReferralPayments;
	 
	 
	 if($totalReferredMadePayments == "")
	 {
		$totalReferredMadePayments = "0";
	 }
	 
	 $referredBilling = "['Task', 'Amount', { role: 'annotation' }],
						 ['Billed Referrals', $totalReferredMadePayments ,$totalReferredMadePayments ],
						 ['Unbilled Referrals', $leftReferralPayments ,$leftReferralPayments ]";
	 
	 // 	WORK RECEIVED RESULT IN BILLING
	 
	 
	 $totalReceivedMadePayments = $this->totalReceivedMadePayments;
	 if($totalReceivedMadePayments == "")
	 {
	 	
		$totalReceivedMadePayments = "0";
	 }

	 $leftReceivedPayments = $this->leftReceivedPayments;
	 $receivedBilling = "['Task', 'Amount', { role: 'annotation' }],
						 ['Billed Referrals', $totalReceivedMadePayments ,$totalReceivedMadePayments ],
						 ['Unbilled Referrals', $leftReceivedPayments ,$leftReceivedPayments ]";
	 
	 
	 // 	KPI REFERRALS REFERRED
	 $firm_turnover = round($this->firm_turnover,2);
	 $country_turnover = round($this->country_turnover,2);
	 $region_turnover = round($this->region_turnover,2);
	 
	 // 	KPI REFERRALS RECEIVED
	 $firm_received_turnover = round($this->firm_received_turnover,2);
	 $country_received_turnover = round($this->country_received_turnover,2);
	 $region_received_turnover = round($this->region_received_turnover,2);
	 
	 //	WORK REFERRED BY SOURCE
	 $workReferredBySource = $this->workReferredBySource;
	 foreach($workReferredBySource as $source)
	 {
	 	$sourceReferred .= "['$source->name',$source->total,$source->total],";
	 }
	 $sourceReferred = substr($sourceReferred,0,-1);
	 //echo "<pre>"; print_r($sourceReferred); die;
	 
	 	 
	 
	 //	WORK RECEIVED BY SOURCE
	 $workReceivedBySource = $this->workReceivedBySource;
	 foreach($workReceivedBySource as $sourcerec)
	 {
	 	$sourceReceived .= "['$sourcerec->name',$sourcerec->total,$sourcerec->total],";
	 }
	 $sourceReceived = substr($sourceReceived,0,-1);
	 
	 
	 
	 // 	WORK REFERRED BY SERVICE TYPE
	 $workReferredByServiceType = $this->workReferredByServiceType;
	 foreach($workReferredByServiceType as $type)
	 {
	 	$servicetypereferred .= "['$type->servicetype',$type->Total,$type->Total],";
	 }
	 $servicetypereferred = substr($servicetypereferred,0,-1);
	 
	 
	 // 	WORK RECEIVED BY SERVICE TYPE
	 $workReceivedByServiceType = $this->workReceivedByServiceType;
	 foreach($workReceivedByServiceType as $type)
	 {
	 	$servicetypereceived .= "['$type->servicetype',$type->Total,$type->Total],";
	 }
	 $servicetypereceived = substr($servicetypereceived,0,-1);
	 
	 // 	WORK REFERRED BY INDUSTRY SECTOR
	 $workReferredByIndustrySector = $this->workReferredByIndustrySector;
	 foreach($workReferredByIndustrySector as $industryReferred)
	 {
	 	$industrySectorReferred .= "['$industryReferred->industry',$industryReferred->Total,$industryReferred->Total],";
	 }
	 $industrySectorReferred = substr($industrySectorReferred,0,-1);
	 
	 // 	WORK RECEIVED BY INDUSTRY SECTOR
	 $workReceivedByIndustrySector = $this->workReceivedByIndustrySector;
	 foreach($workReceivedByIndustrySector as $industryReceived)
	 {
	 	$industrySectorReceived .= "['$industryReceived->industry',$industryReceived->Total,$industryReceived->Total],";
	 }
	 $industrySectorReceived = substr($industrySectorReceived,0,-1);
	 
	 
	 
	 $firm_total_previous_year = round($this->firm_total_previous_year,2);
	 $firm_total_current_year = round($this->firm_total_current_year,2);
	 
	 $country_total_previous_year = round($this->country_total_previous_year,2);
	 $country_total_current_year = round($this->country_total_current_year,2);
	  
	 $region_total_previous_year = round($this->region_total_previous_year,2);
	 $region_total_current_year = round($this->region_total_current_year,2);
	 
	 
	 $received_firm_total_previous_year = round($this->received_firm_total_previous_year,2);
	 $received_firm_total_current_year = round($this->received_firm_total_current_year,2);
	  
	 $received_country_total_previous_year = round($this->received_country_total_previous_year,2);
	 $received_country_total_current_year = round($this->received_country_total_current_year,2);
	  
	 $received_region_total_previous_year = round($this->received_region_total_previous_year,2);
	 $received_region_total_current_year = round($this->received_region_total_current_year,2);
	 
	 
	 // 	NUMBER OF REFERRALS REFERRED BY COUNTRY
	 $referralsReferredByCountry = $this->referralsReferredByCountry;
	 foreach( $referralsReferredByCountry as $referralReferredByCountry)
	 {
	  
		$a .= "['$referralReferredByCountry->country_name',$referralReferredByCountry->number,$referralReferredByCountry->number],";
		
	 }
	  
	 $a = substr($a,0,-1);
	 
	 
	 // 	NUMBER OF REFERRALS RECEIVED BY COUNTRY
	 $referalsReceivedByCountry = $this->referalsReceivedByCountry;
	 foreach( $referalsReceivedByCountry as $referalReceivedByCountry)
	 {
	  
		$b .= "['$referalReceivedByCountry->country_name',$referalReceivedByCountry->number,$referalReceivedByCountry->number],";
		
	 }
	  
	 $b = substr($b,0,-1);
	 
	 
	 // 	AMOUNT REFERRED
	 $totalAmountReferred = $this->totalAmountReferred;
	 foreach( $totalAmountReferred as $amountReferred)
	 {
	  
		$amount_referred .= "['$amountReferred->country_name',$amountReferred->amount,$amountReferred->amount],";
		
	 }
	  
	 $amount_referred = substr($amount_referred,0,-1);
	 
	 
	 // 	AMOUNT RECEIVED
	 $totalAmountReceived = $this->totalAmountReceived;
	 foreach( $totalAmountReceived as $amountReceived)
	 {
	  
		$amount_received .= "['$amountReceived->country_name',$amountReceived->amount,$amountReceived->amount],";
		
	 }
	  
	 $amount_received = substr($amount_received,0,-1);
	 
	 
?>
	 

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	
	

 <!-- 		Work Referred By Source					-->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
         ['Source', 'Amount', { role: 'annotation' }],
		   <?php echo $sourceReferred; ?>
          
        ]);

        var options = {
          //title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('work_referred_by_source'));
        chart.draw(data, options);
      }
    </script>
	
	
	 <!-- 		Work Received By Source					-->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Source', 'Amount', { role: 'annotation' }],
		   <?php echo $sourceReceived; ?>
          
        ]);

        var options = {
          //title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('work_received_by_source'));
        chart.draw(data, options);
      }
    </script>
	
	
	<!-- 		Work Referred By Service Type					-->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		   <?php echo $servicetypereferred; ?>
		  
        ]);

        var options = {
          //title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('work_referred_by_servicetype'));
        chart.draw(data, options);
      }
    </script>
	
	
	
	<!-- 		Work Received By Service Type					-->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		   <?php echo $servicetypereceived; ?>
		  
        ]);

        var options = {
          //title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('work_received_by_servicetype'));
        chart.draw(data, options);
      }
    </script>
	
	
	
	<!-- 		Work Referred By Industry Sector					-->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		   <?php echo $industrySectorReferred; ?>
		  
        ]);

        var options = {
          //title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('work_referred_by_industrysector'));
        chart.draw(data, options);
      }
    </script>
	
	
	<!-- 		Work Referred By Industry Sector					-->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		   <?php echo $industrySectorReceived; ?>
		  
        ]);

        var options = {
          //title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('work_received_by_industrysector'));
        chart.draw(data, options);
      }
    </script>
	
	
	
	
	<!-- 		SCRIPT FOR WORK REFERRED RESULTING IN BILLING -->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
         <?php echo $referredBilling ; ?>
          
		  
        ]);

        var options = {
         // title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('referred_billing_amount_by_companies'));
        chart.draw(data, options);
      }
    </script>
	
	
		<!-- 		SCRIPT FOR WORK RECEIVED RESULTING IN BILLING -->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          <?php echo $receivedBilling ; ?>
          
		  
        ]);

        var options = {
         // title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('received_billing_amount_by_companies'));
        chart.draw(data, options);
      }
    </script>
	
	
	<!-- 		SCRIPT FOR REFERRALS REFERRED BY COUNTRY			-->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		  <?php echo $a; ?>
          
        ]);

        var options = {
         // title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('referrals_referred_by_country'));
        chart.draw(data, options);
      }
    </script>
	
	<!-- 		SCRIPT FOR REFERRALS RECEIVED BY COUNTRY			-->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		  <?php echo $b; ?>
          
        ]);

        var options = {
         // title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('referrals_received_by_country'));
        chart.draw(data, options);
      }
    </script>
	
	<!-- 		SCRIPT FOR AMOUNT REFERRED			-->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		  <?php echo $amount_referred; ?>
          
        ]);

        var options = {
         // title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('amount_referred'));
        chart.draw(data, options);
      }
    </script>
	
	
	<!-- 		SCRIPT FOR AMOUNT RECEIVED			-->
		
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Amount', { role: 'annotation' }],
		  <?php echo $amount_received; ?>
          
        ]);

        var options = {
         // title: 'Total Amount Referred and Received for 2013',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('amount_received'));
        chart.draw(data, options);
      }
    </script>
	
	
	
	
	<!----------------------SCRIPT FOR REFERRALS REFERRED OUT BY TURNOVER Total Amount Through Regions---------------------->
	
	
	<script type="text/javascript">
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
        ['', 'Percentage', { role: 'style' }],
        ['<?php echo $firm_name ;?>', <?php echo $firm_turnover ;?>, '#b87333'],            // RGB value
		['<?php echo $firm_country_name; ?>',  <?php echo $country_turnover ;?>, 'silver'],            // English color name
		['<?php echo $firm_region_name; ?>',<?php echo $region_turnover ;?>, 'gold'],
		
      ]);
	 

      var options = {
	  	//title: 'Total Amount From Regions',
		//hAxis: {title: 'Regions', titleTextStyle: {color: 'red'}},
		vAxis: {title: 'Percentage (%)',maxValue:'100' ,titleTextStyle: {color: 'red'}},
        width: 600,
        height: 400,
		legend: 'none',
		bar: { groupWidth: '60%' },
        isStacked: true,
      };
        var chart = new google.visualization.ColumnChart(document.getElementById('referrals_referred_by_turnover'));
        chart.draw(data, options);
      }
    </script>
	
	<!----------------------SCRIPT FOR REFERRALS RECEIVED OUT BY TURNOVER Total Amount Through Regions---------------------->
	
	
	<script type="text/javascript">
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
        ['', 'Percentage', { role: 'style' }],
        ['<?php echo $firm_name ;?>', <?php echo $firm_received_turnover ;?>, '#b87333'],            // RGB value
		['<?php echo $firm_country_name; ?>',  <?php echo $country_received_turnover ;?>, 'silver'],            // English color name
		['<?php echo $firm_region_name; ?>',<?php echo $region_received_turnover ;?>, 'gold'],
		
      ]);
	 

      var options = {
	  	//title: 'Total Amount From Regions',
		//hAxis: {title: 'Regions', titleTextStyle: {color: 'red'}},
		vAxis: {title: 'Percentage (%)',maxValue:'100' ,titleTextStyle: {color: 'red'}},
        width: 600,
        height: 400,
		legend: 'none',
        bar: { groupWidth: '60%' },
        isStacked: true,
      };
        var chart = new google.visualization.ColumnChart(document.getElementById('referrals_received_turnover'));
        chart.draw(data, options);
      }
    </script>
	
	
	<!-- 		SCRIPT FOR COMPARSION CHARTS REFERRED		-->
	
	<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {

  var data = google.visualization.arrayToDataTable([
    ['Amount', '<?php echo $previous; ?>', '<?php echo $cYear; ?>'],
    ['<?php echo $firm_name ;?>',<?php echo $firm_total_previous_year ; ?>, <?php echo $firm_total_current_year ; ?>],
    ['<?php echo $firm_country_name; ?>',     <?php echo $country_total_previous_year ; ?>, <?php echo $country_total_current_year ; ?>],
    ['<?php echo $firm_region_name; ?>', <?php echo $region_total_previous_year ; ?>, <?php echo $region_total_current_year ; ?>]
  ]);

  var options = {
    //title: 'Company Performance',
    //hAxis: {title: 'Year', titleTextStyle: {color: 'red'}},
	vAxis: {title: 'Amount in GBP', titleTextStyle: {color: 'red'}},
	bar: { groupWidth: '60%' },
  };

  var chart = new google.visualization.ColumnChart(document.getElementById('comparsion_chart'));

  chart.draw(data, options);

}
    </script>
	
	
	<!-- 		SCRIPT FOR COMPARSION CHARTS RECEIVED			-->
	
	<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {

  var data = google.visualization.arrayToDataTable([
    ['Amount', '<?php echo $previous; ?>', '<?php echo $cYear; ?>'],
    ['<?php echo $firm_name ;?>',<?php echo $received_firm_total_previous_year ; ?>, <?php echo $received_firm_total_current_year ; ?>],
    ['<?php echo $firm_country_name; ?>',     <?php echo $received_country_total_previous_year ; ?>, <?php echo $received_country_total_current_year ; ?>],
    ['<?php echo $firm_region_name; ?>', <?php echo $received_region_total_previous_year ; ?>, <?php echo $received_region_total_current_year ; ?>]
  ]);

  var options = {
    //title: 'Company Performance',
    //hAxis: {title: 'Year', titleTextStyle: {color: 'red'}},
	vAxis: {title: 'Amount in GBP', titleTextStyle: {color: 'red'}}
  };

  var chart = new google.visualization.ColumnChart(document.getElementById('received_comparsion_chart'));

  chart.draw(data, options);

}
    </script>
	
	
	
	
<div class="countryreports-div mtop25">
		<form name="test" method="post" action="index.php?option=com_crmery&view=charts" >
			<label style="font-weight:bold ; font-size:14px;" >Select Billing Year</label>
			<?php //$dm = "31/10/"; ?>
			<select name="chartYear" style="width:20%;  height:22px; border:1px solid #ccc" >
			<?php for($yr = $fin_year;$yr>=2013;$yr--){ 
			
			if($yr == $billYear)
			{
				
				$yearSelected = "selected" ;
			}
			else
			{
				$yearSelected = "" ;
			}
			
			?>
				<option <?php echo $yearSelected; ?>  value="<?php echo $yr ; ?>">&nbsp;<?php echo $yr ; ?></option>
				
			<?php }?>
				
			</select>
		
		<input type="submit" name="Go" value="Go" />
		</form>
		
</div>
<br />


<style>
.aside_left{width:550px; float:left;}
.aside_right{width:550px; float:right;}

</style>
<div class="aside_left">

<div style="padding-left:20px;" class="font_size mtop15"><b>Work referred by source</b></div>
<div id="work_referred_by_source" style="width: 600px; height: 350px;" ></div>

<div style="padding-left:20px;" class="font_size"><b>Work referred by service type</b></div>
<div id="work_referred_by_servicetype" style="width: 600px; height: 350px;"></div>

<div style="padding-left:20px;" class="font_size"><b>Work referred by industry sector</b></div>
<div id="work_referred_by_industrysector" style="width: 600px; height: 350px;"></div>

<div style="padding-left:20px;"class="font_size"><b>Work referred result in billing </b></div>
<div id="referred_billing_amount_by_companies" style="width: 600px; height: 350px;"></div>

<div style="padding-left:20px;" class="font_size"><b>Number of referrals referred (To countries)</b></div>
<div id="referrals_referred_by_country" style="width: 600px; height: 350px;"></div>

<div style="padding-left:20px;" class="font_size"><b>Amount referred (To countries)</b></div>
<div id="amount_referred" style="width: 600px; height: 350px;"></div>

<div style="padding-left:20px;" class="font_size"><b>KPI referrals referred out of turnover</b></div>
<div id="referrals_referred_by_turnover" style="width: 580px; height: 500px;"></div>


<div style="padding-left:20px;" class="font_size"><b>Comparsion charts for referrals referred</b></div>
<div id="comparsion_chart" style="width: 570px; height: 500px;"></div>




</div>



<div class="aside_right">
<div style="padding-left:20px;" class="font_size mtop15"><b>Work received by source</b></div>
<div id="work_received_by_source" style="width: 600px; height: 350px;" ></div>

<div style="padding-left:20px;" class="font_size"><b>Work received by service type</b></div>
<div id="work_received_by_servicetype" style="width: 600px; height: 350px;"></div>

<div style="padding-left:20px;" class="font_size"><b>Work received by industry sector</b></div>
<div id="work_received_by_industrysector" style="width: 600px; height: 350px;"></div>

<div style="padding-left:20px;" class="font_size"><b>Work received result in billing </b></div>
<div id="received_billing_amount_by_companies" style="width: 600px; height: 350px;"></div>

<div style="padding-left:20px;" class="font_size"><b>Number of referrals received (Through countries )</b></div>
<div id="referrals_received_by_country" style="width: 600px; height: 350px;"></div>

<div style="padding-left:20px;" class="font_size"><b>Amount received (Through countries )</b></div>
<div id="amount_received" style="width: 600px; height: 350px;"></div>

<div style="padding-left:20px;" class="font_size"><b>KPI referrals received out of turnover</b></div>
<div id="referrals_received_turnover" style="width: 580px; height: 500px;"></div>

<div style="padding-left:20px;" class="font_size"><b>Comparsion charts for referrals received</b></div>
<div  id="received_comparsion_chart" style="width: 600px; height: 500px; "></div>




</div>

