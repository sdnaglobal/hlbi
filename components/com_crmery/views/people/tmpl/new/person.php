<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

//define person
$person = $this->people[0];
$current_financial_year_pure = CrmeryHelperCrmery::getFinancialYear();
$current_financial_date = explode("-",$current_financial_year_pure);
$current_financial_year = $current_financial_date[0];


/*
if($_REQUEST['return'] == 1)
{
	
	$action = $_REQUEST['action'];
	$updateid = (int)$_REQUEST['referralid'];
	
	if($action == 'accepted')
		$value = 1;
	else
		$value = 2;
		
 	$db = JFactory::getDbo();
	
	$pquery = $db->getQuery(true);
	$pquery = "UPDATE #__crmery_people SET Accepted='$value' WHERE id=$updateid";
		  
	$db->setQuery($pquery);
	$db->Query();
	
	
	// temporary stop the emails comment start here  
	
	$aquery = $db->getQuery(true);
	$aquery = "SELECT owner_id,assignee_id FROM #__crmery_people WHERE id = ".$updateid;
		  
	$db->setQuery($aquery);
	$aresults = $db->loadAssocList();
	$aresult = $aresults[0];
	
	$equery = $db->getQuery(true);
	$equery = "SELECT cu.uid, u.email FROM #__crmery_users as cu, #__users as u WHERE cu.id=".$aresult['assignee_id']."  and u.id=cu.uid";
		  
	$db->setQuery($equery);
	$eresults = $db->loadAssocList();
	$eresult = $eresults[0];
	
	$assigne_email = $eresult['email'];
	
	$oquery = $db->getQuery(true);
	$oquery = "SELECT cu.uid, u.email, cu.first_name, cu.last_name FROM #__crmery_users as cu, #__users as u WHERE cu.id=".$aresult['owner_id']." and u.id=cu.uid";

		
	$db->setQuery($oquery);
	$oresults = $db->loadAssocList();
	$oresult = $oresults[0];
	
	$ownername = $oresult['first_name'] .' '.$oresult['last_name'];
	
	$owner_email = $oresult['email'];
	
	//$referralurl =  JURI::base().'index.php?option=com_crmery&view=people&layout=person&id='.$updateid;
	$referralurl =  JURI::base().'index.php/crmerry-dashboard/people/'.$updateid;
	
	//.JRoute::_('index.php?option=com_crmery&view=people&layout=person&id='.$updateid);
	//echo '<br>--->'.JRoute::_($referralurl);
	$config =& JFactory::getConfig();
	$sitename = $config->getValue( 'config.sitename' );

	$mainframe = JFactory::getApplication();
	$fromEmail = $mainframe->getCfg('mailfrom');
				
	// send email to owner about referral accept/decline 
	
	$mySubject = 'Your Referral is '.$action;
	$recipient = $owner_email;
	
	$mail =& JFactory::getMailer();	
	$mail->setSubject($mySubject);
	$mail->addRecipient($recipient);
	$myMessage = "Dear User". "<br />";
	$myMessage .= "Your referral on $sitename is $action by assigned user."."<br />";
	$myMessage .= "<br><a href='$referralurl' >Click and View Details of Referral</a> ";
	$myMessage .= "<br />";
	$myMessage .= "<br />";
	$myMessage .= "Regards,"."<br />";
	$myMessage .= "$sitename "."<br />";
 

	$mail->setBody($myMessage);
	$mail->IsHTML(true);
	$send =& $mail->Send(); 
	
	
	
	// send email to assignee about his action of accept/decline of referral
	
	$mySubject1 = "You have $action Referral on ".$sitename;
	$recipient1 = $assigne_email;
	$mail1 =& JFactory::getMailer();		

	$mail1->setSubject($mySubject1);
	$mail1->addRecipient($recipient1);
	$myMessage1 = "Dear User". "<br />";
	$myMessage1 .= "You have $action referral on $sitename, which is assigned to you by $ownername"."<br />";
	$myMessage1 .= "We have send an email confirmation to $ownername about your action."."<br />";
	$myMessage1 .= "<a href='$referralurl' >Click and View Details of Referral</a> ";
	$myMessage1 .= "<br />";
	$myMessage1 .= "<br />";
	$myMessage1 .= "Regards,"."<br />";
	$myMessage1 .= "$sitename "."<br />";

	$mail1->setBody($myMessage1);
	$mail1->IsHTML(true);
	$send =& $mail1->Send(); 
	
	// send email to hlb admin about accept/decline action on referral 
	
	$msg = 'Referral has been '.$action;
	
	if(isset($_REQUEST['return']))
		$returnurl = 'index.php?option=com_crmery&view=people&layout=person&id='.$updateid;
	else
	    $returnurl = 'index.php?option=com_crmery&view=people&layout=person';
	
	$allDone =& JFactory::getApplication();
	$allDone->redirect(JRoute::_($returnurl),$msg);
	
	 
	
	// temporary stop the emails comment end here 
	
}
*/


/*
if(isset($_GET['referralid']))
{
	$action = $_GET['action'];
	$updateid = (int)$_GET['referralid'];
	
	if($action == 'accepted')
		$value = 1;
	else
		$value = 2;
		
 	$db = JFactory::getDbo();
	
	$pquery = $db->getQuery(true);
	$pquery = "UPDATE #__crmery_people SET Accepted='$value' WHERE id=$updateid";
		  
	$db->setQuery($pquery);
	$db->Query();
	
}	*/
?>

<script type="text/javascript">
	var id  = <?php echo $person['id']; ?>;
	var loc = 'person';
	var model = 'people';
	var person_id  = <?php echo $person['id']; ?>;
	var association_type = 'person';
</script>
 
<iframe id="hidden" name="hidden" style="display:none;width:0px;height:0px;border:0px;"></iframe>

<div class="rightColumn">   
	<div class="infoContainer" id="details">
		<h2><?php echo ucwords(CRMText::_('COM_CRMERY_CONTACT_INFO')); ?></h2>
		<div class="infoBlock">
			<div class="infoLabel">
				<?php
				if ( array_key_exists('avatar',$person) && $person['avatar'] != "" && $person['avatar'] != null ){
			             echo '<td class="avatar" ><img id="avatar_img_'.$person['id'].'" data-item-type="people" data-item-id="'.$person['id'].'" class="avatar" src="'.JURI::base().'components/com_crmery/media/avatars/'.$person['avatar'].'"/></td>';
                    }else{
                        echo '<td class="avatar" ><img id="avatar_img_'.$person['id'].'" data-item-type="people" data-item-id="'.$person['id'].'" class="avatar" src="'.JURI::base().'components/com_crmery/media/images/person.png'.'"/></td>';
                    } ?>
			</div>
			<div class="infoDetails">
				<span class="largeDetails"><?php echo $person['first_name'] . ' ' . $person['last_name']; ?></span><br />
				<!-- <span class="smallDetails"><?php //echo $person['owner_first_name'] . ' ' . $person['owner_last_name']; ?></span><br /> -->
				<?php if(array_key_exists('company_id',$person)){ ?>
					<!-- <a href="<?php //echo JRoute::_("index.php?option=com_crmery&view=companies&layout=company&company_id=".$person['company_id']); ?>"><?php echo $person['company_name']; ?></a> --> <?php echo $person['company_name']; ?>
				<?php } ?>
			</div>
		</div>

		<?php if ( array_key_exists('assignee_id',$person) && $person['assignee_id'] > 0 ){ ?>
		<div class="infoBlock">
			<div class="infoLabel">
				<?php echo ucwords(CRMText::_('COM_CRMERY_PERSON_ASSIGNMENT')); ?>
			</div>
			<div class="infoDetails">
				<?php echo $person['first_name'] . ' ' . $person['last_name']; ?>
			</div>
		</div>
		<?php } ?>

		<?php if ( array_key_exists("position",$person) && $person['position'] != "" ){ ?>
			<div class="infoBlock">
				<div class="infoLabel right"><?php echo CRMText::_('COM_CRMERY_TITLE'); ?></div>
				<div class="infoDetails"><?php echo $person['position']; ?></div>
			</div>
		<?php } ?>
		
		<div class="infoBlock">
			<div class="infoLabel"><?php echo CRMText::_('COM_CRMERY_WORK_PHONE_SHORT'); ?></div>
			<div class="infoDetails"><?php echo $person['phone']; ?></div>
		</div>
		
		<div class="infoBlock">
			<div class="infoLabel"><?php echo CRMText::_('COM_CRMERY_EMAIL_SHORT'); ?></div>
			<div class="infoDetails">
				<?php if(array_key_exists('email',$person)){ ?>
					<a target="_blank" href="mailto:<?php echo $person['email']; ?>?bcc=<?php echo CRMeryHelperConfig::getConfigValue('imap_user'); ?>"><?php echo $person['email']; ?></a>
				<?php } ?>
			</div>
		</div>

		<?php if ( array_key_exists('work_address_1',$person) && $person['work_address_1'] != "" ) { ?>
			<div class="infoBlock">
				<div class="infoLabel"><?php echo CRMText::_('COM_CRMERY_WORK_ADDRESS'); ?></div>
				<div class="infoDetails">
					<?php $urlString = "http://maps.googleapis.com/maps/api/staticmap?&zoom=13&zoom=2&size=600x400&sensor=false&center=".str_replace(" ","+",$person['work_address_1'].' '.$person['work_address_2'].' '.$person['work_city'].' '.$person['work_state'].' '.$person['work_zip'].' '.$person['work_country']); ?>
					<a href="javascript:void(0);" class="google-map" id="work_address"></a>
					<div id="work_address_modal" style="display:none;">
						<div class="google_map_center"></div>
						<img class="google-image-modal"  style="background-image:url(<?php echo $urlString; ?>);" />
					</div>
					<?php echo $person['work_address_1']; ?><br />
					<?php if ( array_key_exists('work_address_2',$person) && $person['work_address_2'] != "" ){ 
						echo $person['work_address_2'].'<br />';
					} ?>
					<?php echo $person['work_city'].', '.$person['work_state']." ".$person['work_zip']; ?><br />
					<?php echo $person['work_country']; ?>
				</div>
			</div>	
		<?php } ?>	

		<?php if ( array_key_exists('home_address_1',$person) && $person['home_address_1'] != "" ) { ?>
			<div class="infoBlock">
				<div class="infoLabel"><?php echo CRMText::_('COM_CRMERY_HOME_ADDRESS'); ?></div>
				<div class="infoDetails">
					<?php $urlString = "http://maps.googleapis.com/maps/api/staticmap?&zoom=13&zoom=2&size=600x400&sensor=false&center=".str_replace(" ","+",$person['home_address_1'].' '.$person['home_address_2'].' '.$person['home_city'].' '.$person['home_state'].' '.$person['home_zip'].' '.$person['home_country']); ?>
					<a href="javascript:void(0);" class="google-map" id="home_address"></a>
					<div id="home_address_modal" style="display:none;">
						<div class="google_map_center"></div>
						<img class="google-image-modal" style="background-image:url(<?php echo $urlString; ?>);" />
					</div>
					<?php echo $person['home_address_1']; ?><br />
					<?php if ( array_key_exists('home_address_2',$person) && $person['home_address_2'] != "" ){ 
						echo $person['home_address_2'].'<br />';
						}	?>
					<?php echo $person['home_city'].', '.$person['home_state']." ".$person['home_zip']; ?><br />
					<?php echo $person['home_country']; ?>
				</div>
			</div>
		<?php } ?>

		<div class="infoBlock">
			<div class="infoLabel">&nbsp;</div>
			<div class="infoDetails">

				
					<?php if(array_key_exists('facebook_url',$person) && $person['facebook_url'] != ""){ ?>
						<a href="<?php echo $person['facebook_url']; ?>" target="_blank"><div class="facebook_light"></div></a>
					<?php } else { ?>
					<span class="editable parent" id="editable_facebook_container_<?php echo $person['id']; ?>">
					<div class="inline">
						<a href="javascript:void(0);"><div class="facebook_dark"></div></a>
					</div>
					<div class="filters editable_info">
						<form id="facebook_form_<?php echo $person['id']; ?>">
							<input type="hidden" name="item_id" value="<?php echo $person['id']; ?>" />
							<input type="hidden" name="item_type" value="people" />
							<input type="text" class="inputbox" name="facebook_url" value="<?php if ( array_key_exists('facebook_url',$person) ) echo $person['facebook_url']; ?>" />
							<input type="button" class="button" onclick="saveEditableModal('facebook_form_<?php echo $person['id']; ?>');" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" />
						</form>
					</div>
					</span>
					<?php } ?>

					<?php if(array_key_exists('twitter_user',$person) && $person['twitter_user'] != ""){ ?>
						<a href="http://www.twitter.com/#!/<?php echo $person['twitter_user']; ?>" target="_blank"><div class="twitter_light"></div></a>
					<?php } else { ?>
					<span class="editable parent" id="editable_twitter_container_<?php echo $person['id']; ?>">
					<div class="inline">
						<a href="javascript:void(0);"><div class="twitter_dark"></div></a>
					</div>
					<div class="filters editable_info">
						<form id="twitter_form_<?php echo $person['id']; ?>">
							<input type="hidden" name="item_id" value="<?php echo $person['id']; ?>" />
							<input type="hidden" name="item_type" value="people" />
							<input type="text" class="inputbox" name="twitter_user" value="<?php if ( array_key_exists('twitter_user',$person) ) echo $person['twitter_user']; ?>" />
							<input type="button" class="button" onclick="saveEditableModal('twitter_form_<?php echo $person['id']; ?>');" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" />
						</form>
					</div>
					</span>
					<?php } ?>


					<?php if(array_key_exists('linkedin_url',$person) && $person['linkedin_url'] != "" ){ ?>
						<a href="<?php echo $person['linkedin_url']; ?>" target="_blank"><div class="linkedin_light"></div></a>
					<?php } else { ?>
					<span class="editable parent" id="editable_linkedin_container_<?php echo $person['id']; ?>">
					<div class="inline">
						<a href="javascript:void(0);"><div class="linkedin_dark"></div></a>
					</div>
					<div class="filters editable_info">
						<form id="linkedin_form_<?php echo $person['id']; ?>">
							<input type="hidden" name="item_id" value="<?php echo $person['id']; ?>" />
							<input type="hidden" name="item_type" value="people" />
							<input type="text" class="inputbox" name="linkedin_url" value="<?php if ( array_key_exists('linkedin_url',$person) ) echo $person['linkedin_url']; ?>" />
							<input type="button" class="button" onclick="saveEditableModal('linkedin_form_<?php echo $person['id']; ?>');" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" />
						</form>
					</div>
					</span>
					<?php } ?>

					<span class="editable parent" id="editable_aim_container_<?php echo $person['id']; ?>">
					<div class="inline">
						<?php if(array_key_exists('aim',$person) && $person['aim'] != "" ){ ?>
							<a href="javascript:void(0);"><div class="aim_light"></div></a>
						<?php } else { ?>
							<a href="javascript:void(0);"><div id="aim_button_<?php echo $person['id']; ?>" class="aim_dark"></div></a>
						<?php } ?>
					</div>
					<div class="filters editable_info">
						<form id="aim_form_<?php echo $person['id']; ?>">
							<input type="hidden" name="item_id" value="<?php echo $person['id']; ?>" />
							<input type="hidden" name="item_type" value="people" />
							<input type="text" class="inputbox" name="aim" value="<?php if ( array_key_exists('aim',$person) )  echo $person['aim']; ?>" />
							<input type="button" class="button" onclick="saveEditableModal('aim_form_<?php echo $person['id']; ?>');" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" />
						</form>
					</div>
					</span>
			</div>
		</div>
	</div>

	<?php if ( $this->acymailing ){ ?>
		<div class="infoContainer" id='acymailing'>
			<h2><?php echo ucwords(CRMText::_('COM_CRMERY_ACYMAILING_HEADER')); ?></h2>
			<?php $this->acymailing_dock->display(); ?>
		</div>
	<?php } ?>

	<?php if ( isset($this->banter_dock) ){ 
		$this->banter_dock->display();
	}?>

	<?php if($person['twitter_user']) { ?>
		<div class="infoContainer">
			<h2><?php echo CRMText::_('COM_CRMERY_LATEST_TWEETS'); ?></h2>
			<?php if ( array_key_exists('tweets',$person) ){ for($i=0; $i<count($person['tweets']); $i++) { 
				$tweet = $person['tweets'][$i];
			?>
			<div class="tweet">
				<span class="tweet_date"><?php echo $tweet['date']; ?></span>
				<?php echo $tweet['tweet']; ?>
			</div>
			<?php } } ?>
		</div>
	<?php } ?>


	<!--div class="infoContainer" id='event_dock'>
		<h2><?php echo CRMText::_('COM_CRMERY_CLEINT_REFERRAL_PAYMENT_HISTORY'); ?></h2>
		<?php echo "We are going to show the payment history here";//$this->event_dock->display(); ?>
	</div-->
	
	<div >
	   <h2><?php echo "Payment History"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="index.php?option=com_crmery&view=billing&layout=person&id=<?php echo $person['id']; ?>">Add Billing</a></h2>
	    <div class="large_info">
		   <table id="latest_activity" class="com_crmery_table">
			<tbody>
			   <tr>
			   
				<th style="text-align:left;"><?php echo CRMText::_('COM_CRMERY_BILING_YEAR'); ?></th>
				<th style="text-align:left;"><?php echo CRMText::_('COM_CRMERY_AMOUNT_BILL'); ?> in GBP</th>
			
			</tr>
		    </tbody>
	<?php 

		$db = JFactory::getDbo();
		$query_deal = $db->getQuery(true);
		//$query_deal = "SELECT cf.person_id, cp.id FROM #__crmery_people_cf as cf where cf.association_id=".$deal['id']." and cf.association_type='deal'";
		$query_deal = "SELECT cf.person_id, cp.id FROM #__crmery_people_cf as cf, #__crmery_people as cp where cf.association_id=".$person['id']." and cf.association_type='deal' and cp.id=cf.person_id and cp.published>0 ";
		
		$db->setQuery($query_deal);
		$referrals = $db->loadObjectlist();
		
		//echo "<pre>"; print_r($referrals);echo "</pre>";
		
		$where_qry  = '';
		
		foreach($referrals as $referral)
		{
			$where_qry .= " referral_ID=".$referral->person_id." or";
		}
	
		$year_qry = $db->getQuery(true);
	    $year_qry = "SELECT MIN(YEAR(STR_TO_DATE(BillingYear, '%d/%m/%Y'))) as billdate FROM #__crmery_referral_payments WHERE FeePaid=1 and (".substr($where_qry,0,-2) ." )";
		$db->setQuery($year_qry);
		//$start_year = $db->loadResult();
		$start_year = '2013';
	
		if($start_year>0)
		{
		
			for($start=$start_year;$start<=$current_financial_year;$start++)
			{
				$total_amount = 0;
				$total_amount_year = 0;
				
				
					
					$total_amount = get_amount_billed_by_referral_id($person['id'],$start);
					$total_amount_year = ($total_amount_year+$total_amount);
					//$referral->person_id.'<br><br>';
					 
					echo '<tr class="crmery_row">';
					echo '<td>31/10/'.$start.'</td>';
					echo '<td>&pound; '.number_format($total_amount_year,2,'.',',').'</td>';
					echo '</tr>';	
					//break;
					
			}
			echo '<script type="text/javascript"> $("#amount_total_top").html(\'&pound; '.$total_amount_year.'\');</script>';
		}
		else{
					echo '<tr class="crmery_row">';
					echo '<td colspan="2" align="center"> No Billing History of this client</td>';
					echo '</tr>';	
		}
	?>		
</table>
	</div>
	</div>
	
	
	
	

	<!--div class="infoContainer" id='event_dock'>
		<h2><?php echo ucwords(CRMText::_('COM_CRMERY_TASKS_AND_EVENTS')); ?></h2>
		<?php $this->event_dock->display(); ?>
	</div-->

</div>	


<div class="leftColumn">
 
<ul class="entry_buttons">
	<li><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=people&layout=edit&id='.$person['id']); ?>" class=""><?php echo CRMText::_('COM_CRMERY_EDIT_BUTTON'); ?></a></li>
</ul>
<div class="actions_container">
 
<span class="actions textMiddle">

<div style="width:100px;float:left;text-align:center;">
		 <?php 
	 		        
        $member_id = CrmeryHelperUsers::getUserId();
        $member_role = CrmeryHelperUsers::getRole();
$firmId = array();
if($member_role == 'manager' || $member_role == 'basic')
{	
    if($member_role == 'manager') 
		 {$field="company_id"; } 
		 if($member_role == 'basic')
		 {
		   $field="inp_company_id";
		 }
							
	
	   $db =& JFactory::getDbo();
	   $querysu = $db->getQuery(true);
	   $querysu->select("u.$field")
	   ->from("#__crmery_users AS u");
	   $querysu->where("u.id=".CrmeryHelperUsers::getLoggedInUser()->id);
	  
	    $db->setQuery($querysu);
		$comId = $db->loadObjectList();
		 
		if ( count($comId) > 0 ){
		foreach ( $comId as $comid ){						
			$firmId[] =$comid->$field;						
		  }
		}

}else if($member_role == '' || $member_role == 'exec')
{
	$db =& JFactory::getDbo();
	$querys = $db->getQuery(true);
	$querys->select("r.region_id,r.country_id")
	->from("#__crmery_users AS r");
	$querys->where("r.id='".$member_id."'");						

	$db->setQuery($querys);
	$regIds = $db->loadObjectList();
	
	if ( count($regIds) > 0 ){
		foreach ( $regIds as $regId ){						
			$regionId=$regId->region_id;						
		  }
	 }	
	 
	//$query->where("c.region_id IN(".$regionId.")");  

	$region_query = $db->getQuery(true);
	  $region_query->select("r.id")
		->from("#__crmery_companies AS r");
	$region_query->where("r.region_id IN(".$regionId.")");						

	$db->setQuery($region_query);
	$firm_Ids = $db->loadObjectList();
	
	if ( count($firm_Ids) > 0 ){
		 
		foreach ( $firm_Ids as $firm_Id ){						
			$firmId[]=$firm_Id->id;						
		  }
		  //$firm_Ids = implode(',',$firmId);
     }
}		
 
 /*echo '<pre>';
 print_r($firmId);
 echo '==>'.$person['company_id'];
 echo '</pre>';
 */
		        if(in_array($person['company_id'],$firmId) || $member_id == $person['assignee_id']  )
					{	 
						 if($person['Accepted'] == 0){ echo '<a href="'.JRoute::_('index.php?option=com_crmery&view=people&layout=person&referralid='.$person['id']).'&action=accepted&return=1"> Accept Referral</a> ';
						 }elseif($person['Accepted'] == 1){  
						    echo 'Accepted';
						 }
						 else
						 {
							echo 'Declined';
						 }
					 }
					  
		?>	
	</div>	
	
    <a href="javascript:void(0);" id="actions_button" class="dropdown_arrow"><?php echo CRMText::_('COM_CRMERY_ACTION_BUTTON'); ?></a>
    <div id="actions">
    	
        <ul>
            <li><a onclick="addDeal('person_id=<?php echo $person['id']; ?>')"><?php echo CRMText::_('COM_CRMERY_ASSOCIATE_TO_DEAL'); ?></a></li>
            <?php if ( $person['owner_id'] == CrmeryHelperUsers::getUserId() ){ ?>
				<li><a onclick="shareItemDialog();" ><?php echo CRMText::_('COM_CRMERY_SHARE'); ?></a></li>
			<?php } ?>
            <form id="delete_form" method="POST" action="<?php echo JRoute::_('index.php?option=com_crmery&controller=main&task=trash'); ?>" >
			<input type="hidden" name="item_id" value="<?php echo $person['id']; ?>" />
			<input type="hidden" name="item_type" value="people" />
			<input type="hidden" name="page_redirect" value="people" />
			<?php if ( CrmeryHelperUsers::canDelete() || $person['owner_id'] == CrmeryHelperUsers::getUserId() ) { ?>
				<li><a onclick="deleteItem(this)"><?php echo CRMText::_('COM_CRMERY_DELETE_CONTACT'); ?></a></li>
			<?php } ?>
			</form>
			<form class="print_form" method="POST" target="_blank" action="<?php echo JRoute::_('index.php?option=com_crmery&view=print'); ?>">
			<input type="hidden" name="item_id" value="<?php echo $person['id']; ?>" />
	    	<input type="hidden" name="layout" value="person" />
	    	<input type="hidden" name="model" value="people" />		
            <li><a onclick="printItems(this)"><?php echo CRMText::_('COM_CRMERY_PRINT'); ?></a></li>
        	</form>
            <li>
            	<form id="vcard_form" action="" method="POST">
        		<input type="hidden" name="person_id" value="<?php echo $person['id']; ?>" />
        			<a onclick="exportVcard()">
        				<?php echo CRMText::_('COM_CRMERY_VCARD'); ?>
        			</a>
        		</form>
        	</li>  
			
        </ul>
    	</form>
    </div>
</span>
</div>
<h1> <?php echo CRMText::_('COM_CRMERY_REFERRAL_CODE')?>:
<?php echo CRMText::_('COM_CRMERY_REFERRAL_CODE_PREFIX').$person['id']; ?>
</h1>

<div class="container">
	<div class="columncontainer">
		<div class="threecolumn">
			<div class="small_info first">
				<?php echo CRMText::_('COM_CRMERY_PERSON_TOTAL'); ?>: 
				<span class="amount"><?php echo CrmeryHelperConfig::getCurrency(); ?>
				<?php $refvalue = get_amount_billed_by_referral_id($person['id'],$current_financial_year);?>
				<?php echo number_format($refvalue,2,'.',',')?>
				<?php //echo (float)$person['total_pipeline']; ?></span>
			</div>
			<div class="crmeryRow top">
				<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_COMPANY')); ?>:</div>
				<div class="crmeryValue">
					<?php if ( array_key_exists('company_id',$person) ) { ?>
						<!-- <a href="<?php echo JRoute::_("index.php?option=com_crmery&view=companies&layout=company&company_id=".$person['company_id']); ?>"><?php echo $person['company_name']; ?></a>--><?php echo $person['company_name']; ?>
					<?php } else {
						echo CRMText::_('COM_CRMERY_NO_COMPANY');
					} ?>
				</div>
			</div>
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_EDIT_OWNER')); ?></div>
				<div class="crmeryValue">
					<span   id='person_owner_link'><?php echo $person['owner_first_name']." ".$person['owner_last_name']; ?></span>
					<?php /*
						echo '<div class="filters" data-item="people" data-field="owner_id" data-item-id="'.$person['id'].'" id="person_owner">';
		                    echo '<ul>';
		                        $me = array(array('label'=>CRMText::_('COM_CRMERY_ME'),'value'=>CrmeryHelperUsers::getLoggedInUser()->id));
		                        $users = CrmeryHelperUsers::getUsers(null,TRUE);
		                        $users = array_merge($me,$users);
		                        if ( count($users) ){ foreach ( $users as $key => $user ){
		                            echo '<li><a href="javascript:void(0)" class="owner_select dropdown_item" data-value="'.$user['value'].'">'.$user['label'].'</a></li>';
		                        }}
		                    echo '</ul>';
		                echo '</div>';
	              */  ?>
				</div>
			</div>
		</div>

		<div class="threecolumn">	
			<div class="small_info middle" style="visibility:hidden;">
				<?php //echo ucwords(CRMText::_('COM_CRMERY_PERSON_DEALS')); ?>: 
				<span class="amount"><?php echo CrmeryHelperConfig::getCurrency(); ?><?php echo (float)$person['won_deal_amount']; ?></span>
			</div>
			<div class="crmeryRow top">
				<div class="crmeryField">Assigned To<?php //echo CRMText::_('COM_CRMERY_TITLE'); ?>:</div>
				<div class="crmeryValue"><?php echo $person['first_name'].' '.$person['last_name']; ?>
					<span class="editable parent" id="editable_position_container">
					<div class="inline" id="editable_position">
					<?php //echo $person['first_name'].' '.$person['last_name']; ?>
						  <?php //if ( array_key_exists('position',$person) && $person['position'] != "" ){ 
								// echo $person['position'];
							// }else{
								// echo CRMText::_('COM_CRMERY_CLICK_TO_EDIT');
							// } ?>
					</div>
					<div class="filters editable_info">
						<form id="position_form">
							<input type="text" class="inputbox" name="position" value="<?php if(array_key_exists('position',$person)) echo $person['position']; ?>" />
							<input type="button" class="button" onclick="saveEditableModal('position_form');" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" />
						</form>
					</div>
					</span>
		    	</div>
			</div>
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_TYPE'); ?>:</div>
				<div class="crmeryValue">
					<?php $person_type = ( array_key_exists('type',$person) && $person['type'] != "" ) ? $person['type'] : CRMText::_('COM_CRMERY_NOT_SET'); ?>
					<span class='dropdown' id="person_type_link"><?php echo ucwords($person_type); ?></span>
					<?php 
						echo '<div class="filters" data-item="people" data-field="type" data-item-id="'.$person['id'].'" id="person_type">';
		                    echo '<ul>';
		                        $types = CrmeryHelperPeople::getPeopleTypes(FALSE);
		                        if ( count($types) ){ foreach ( $types as $key => $type ){
		                            echo '<li><a href="javascript:void(0)" class="dropdown_item" data-value="'.$key.'">'.ucwords($type).'</a></li>';
		                        }}
		                    echo '</ul>';
		                echo '</div>';
	                ?>
				</div>
			</div>
		</div>

		<div class="threecolumn">
			<div class="small_info last">
				<?php echo CRMText::_('COM_CRMERY_PERSON_CONTACTED'); ?>: 
				<?php 
					echo CrmeryHelperDate::formatDate($person['modified']);
				?>
			</div>
			<div class="crmeryRow top">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_STATUS'); ?>:</div>
				<div class="crmeryValue">
					<?php $person['status_name'] = ( $person['status_name'] == '' ) ? CRMText::_('COM_CRMERY_NO_STATUS') : $person['status_name']; ?>
					<?php echo "<a href='javascript:void(0);' class='dropdown' id='person_status_".$person['id']."_link'><div class='person-status-color' style='background-color:#".$person['status_color']."'></div><div class='person-status'>".$person['status_name']."</div></a>";
						echo '<div class="filters" data-item="people" data-field="status_id" data-item-id="'.$person['id'].'" id="person_status_'.$person['id'].'">';  ?>
                        <ul>
                            <li><a href="javascript:void(0)" class="status_select dropdown_item" data-value="0"><div class="person-status-none"></div></a></li>
                            <?php $statuses = CrmeryHelperPeople::getStatusList();
                            if (count($statuses)) { foreach($statuses as $key => $status ){
                                echo '<li><a href="javascript:void(0)" class="status_select dropdown_item" data-value="'.$status['id'].'"><div class="person-status-color" style="background-color:#'.$status['color'].'"></div><div class="person-status">'.$status['name'].'</div></a></li>';
                            }} ?>
                        </ul>
                    </div>
				</div>
			</div>
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_SOURCE'); ?>:</div>
				<div class="crmeryValue">
					<?php $person['source_name'] = ( $person['source_name'] == '' ) ? CRMText::_('COM_CRMERY_NO_SOURCE') : $person['source_name']; ?>
					<?php 	echo "<a href='javascript:void(0);' class='dropdown' id='person_source_".$person['id']."_link'>".$person['source_name']."</a>";
                    	 	echo '<div class="filters" data-item="people" data-field="source_id" data-item-id="'.$person['id'].'" id="person_source_'.$person['id'].'">'; ?>
                        <ul>
                            <?php $sources = CrmeryHelperDeal::getSources();
                            if ( count($sources) ){ foreach($sources as $id => $name ) {
                                echo '<li><a href="javascript:void(0)" class="source_select dropdown_item" data-value="'.$id.'">'.$name.'</a></li>';
                            }} ?>
                        </ul>
                    </div>
                    </div>
			</div>
		</div>
	</div>
<h2 class="dotted"><?php echo ucwords(CRMText::_('COM_CRMERY_EDIT_DEALS')); ?></h2>

	<div class="large_info">
		<?php $this->deal_dock->display(); ?>
	</div>
	
<h2 class="dotted"><?php echo CRMText::_('COM_CRMERY_EDIT_NOTES'); ?></h2>
<?php 

echo $person['notes']->display(); ?>


<!--		MESSAGE	STARTS FROM HERE -->
	
		
	    <div class="large_info">
		   <table id="latest_activity" class="com_crmery_table">
			<tbody>
			   <tr>
			   
				<th style="text-align:left;"><?php echo CRMText::_('COM_CRMERY_PEOPLE_MESSAGE'); ?></th>
				
			</tr>
		    </tbody>
			
				<tr class="crmery_row">
				<td colspan="2" align="center"><?php echo $person['message']; ?></td>
				</tr>
			
		</table>
	</div>
	<br /><br />




<!--		MESSAGE ENDS HERE		-->


<!-- referral billing history -->

	<!--div class="container">
	   <h2><?php echo CRMText::_('COM_CRMERY_CLEINT_REFERRAL_PAYMENT_HISTORY'); ?></h2>
	    <div class="large_info">
		   <table id="latest_activity" class="com_crmery_table">
			<tbody>
			   <tr>
			   
				<th style="text-align:left;"><?php echo CRMText::_('COM_CRMERY_BILING_YEAR'); ?></th>
				<th style="text-align:left;"><?php echo CRMText::_('COM_CRMERY_AMOUNT_BILL'); ?> in GBP</th>
			
			</tr>
		    </tbody>
	<?php 

		$db = JFactory::getDbo();
		$query_deal = $db->getQuery(true);
		//$query_deal = "SELECT cf.person_id, cp.id FROM #__crmery_people_cf as cf where cf.association_id=".$deal['id']." and cf.association_type='deal'";
		$query_deal = "SELECT cf.person_id, cp.id FROM #__crmery_people_cf as cf, #__crmery_people as cp where cf.association_id=".$person['id']." and cf.association_type='deal' and cp.id=cf.person_id and cp.published>0 ";
		
		$db->setQuery($query_deal);
		$referrals = $db->loadObjectlist();
		
		//echo "<pre>"; print_r($referrals);echo "</pre>";
		
		$where_qry  = '';
		
		foreach($referrals as $referral)
		{
			$where_qry .= " referral_ID=".$referral->person_id." or";
		}
	
		$year_qry = $db->getQuery(true);
	    $year_qry = "SELECT MIN(YEAR(STR_TO_DATE(BillingYear, '%d/%m/%Y'))) as billdate FROM #__crmery_referral_payments WHERE FeePaid=1 and (".substr($where_qry,0,-2) ." )";
		$db->setQuery($year_qry);
		//$start_year = $db->loadResult();
		$start_year = '2013';
	
		if($start_year>0)
		{
		
			for($start=$start_year;$start<=$current_financial_year;$start++)
			{
				$total_amount = 0;
				$total_amount_year = 0;
				
				
					
					$total_amount = get_amount_billed_by_referral_id($person['id'],$start);
					$total_amount_year = ($total_amount_year+$total_amount);
					//$referral->person_id.'<br><br>';
					 
					echo '<tr class="crmery_row">';
					echo '<td>31/10/'.$start.'</td>';
					echo '<td>&pound; '.number_format($total_amount_year,2,'.',',').'</td>';
					echo '</tr>';	
					//break;
					
			}
			echo '<script type="text/javascript"> $("#amount_total_top").html(\'&pound; '.$total_amount_year.'\');</script>';
		}
		else{
					echo '<tr class="crmery_row">';
					echo '<td colspan="2" align="center"> No Billing History of this client</td>';
					echo '</tr>';	
		}
	?>		
</table>
	</div>
	</div-->



<h2><?php echo CRMText::_('COM_CRMERY_EDIT_CUSTOM'); ?></h2>

<div class="columncontainer">
	<?php $this->custom_fields_view->display(); ?>
</div>

<h2 class="dotted"><?php echo CRMText::_('COM_CRMERY_EDIT_DOCUMENTS'); ?></h2>

	<div class="large_info">
        <table id="documents_table" class="com_crmery_table">
        <thead>
        	<tr>
	            <th><?php echo CRMText::_('COM_CRMERY_TYPE'); ?></th> 
	            <th><?php echo CRMText::_('COM_CRMERY_FILE_NAME'); ?></th>
	            <th><?php echo CRMText::_('COM_CRMERY_OWNER'); ?></th>
	            <th><?php echo CRMText::_('COM_CRMERY_SIZE'); ?></th>
	            <th><?php echo CRMText::_('COM_CRMERY_UPLOADED'); ?></th>
	        </tr>
        </thead>
        <tbody id="documents">
           <?php echo $this->document_list->display(); ?>
        </tbody>
        </table>
    </div>
	<span class="actions">
	    <form id="upload_form" target="hidden" action="index.php?option=com_crmery&controller=documents&task=uploadDocument&format=raw&tmpl=component" method="POST" enctype="multipart/form-data">
	    <div class="input_upload_button fltrt" >
            <a href="javascript:void(0);" class="button" id="upload_button" ><?php echo CRMText::_("COM_CRMERY_UPLOAD_FILE"); ?></a>
            <input type="file" id="upload_input_invisible" name="document" />
        </div>
        </form>
    </span>

<div class="container">
    <h2><?php echo CRMText::_('COM_CRMERY_LATEST_ACTIVITIES'); ?></h2>
    <div class="large_info">
    	<?php echo $this->latest_activities->display(); ?>
    </div>
</div>

</div>
</div>

<div id="ajax_search_deal_dialog" style="display:none;">
	<h2><?php echo CRMText::_("COM_CRMERY_ASSOCIATE_TO_DEAL"); ?></h2>
	<input class="inputbox" type="text" name="deal_name" value="" placeholder="<?php echo CRMText::_('COM_CRMERY_BEGIN_TYPING_TO_SEARCH'); ?>" />
	<div class="actions">
		<input type="button" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" onclick="saveCf('people');closeDialog('deal')"/> 
		<?php echo CRMText::_('COM_CRMERY_OR'); ?> 
		<a onclick="closeDialog('deal')"><?php echo CRMText::_('COM_CRMERY_CANCEL'); ?></a>
	</div>
</div>

<?php 

function get_amount_billed_by_referral_id($ref_id,$year)
{

	$db = JFactory::getDbo();
	$billing = $db->getQuery(true);
	
	$billing = "select sum(SterlingAmount) as total from #__crmery_referral_payments where FeePaid=1 and referral_ID=".$ref_id." and YEAR(STR_TO_DATE(BillingYear, '%d/%m/%Y'))=".$year." group by referral_ID  ";
	$db->setQuery($billing);
 	$billingdetails = $db->loadObjectList();
 
	$GBPsum = $billingdetails[0]->total;

	return $GBPsum;
	
}
/*
function get_amount_billed_by_referral_id($ref_id,$year)
{

	$db = JFactory::getDbo();
	$billing = $db->getQuery(true);
	$billing->select('LocalAmount,LocalCurrencyCode');
	$billing->from('#__crmery_referral_payments');
	$billing->where('FeePaid=1 and referral_ID='.$ref_id);
	$billing->where(' YEAR(STR_TO_DATE(BillingYear, "%d/%m/%Y"))='.$year);
	
	$db->setQuery($billing);
	$billingdetails = $db->loadObjectList();

	$total_gbp = 0;

	foreach($billingdetails as $bill)
	{ 	
	
		$sum = ($sum+$bill->LocalAmount);
	
		$currencycode = $bill->LocalCurrencyCode;
	
		$ccode = $db->getQuery(true);
		$ccode->select('value_in_GBP');
		$ccode->from('#__crmery_currencies');
		$ccode->where("currency_code='$currencycode'");
		$db->setQuery($ccode);
		//echo '<br>'.$ccode;
		$ccodevalue = $db->loadObjectList();
		
		$ccode_value = $ccodevalue[0];
		$gbp = $ccode_value->value_in_GBP;
		//$bill->LocalAmount;
		$gbp_price = ($gbp*$bill->LocalAmount); 
		
		$total_gbp = ($total_gbp+$gbp_price); 
	}
	return $total_gbp;
	
}*/

?>




<?php echo CrmeryHelperCrmery::showShareDialog(); ?>