<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 $member_role = CrmeryHelperUsers::getRole();
 			
				
?>

<script type="text/javascript">
    var loc = "people";
    order_url = "<?php echo 'index.php?option=com_crmery&view=people&layout=list&format=raw&tmpl=component'; ?>";
    order_dir = "<?php echo $this->state->get('People.filter_order_Dir'); ?>";
    order_col = "<?php echo $this->state->get('People.filter_order'); ?>";
	

<?php if(isset($_GET['val'])) { ?>

	jQuery(document).ready(function(){
		jQuery('.list-footer').find("li").each(function(){
			var h = (jQuery(this).find("a").attr("href"));

			if(h == '4' || h == 'unset' ){
				jQuery('.limit').show();
			}
			else{
				jQuery('.limit').hide();
			}	
				
			if(typeof h !== 'undefined'){
				h = h + '&val=<?php echo $_GET['val']?>';
				jQuery(this).find("a").attr("href",h);
				jQuery('.limit').hide();
			}
		});
	});

<?php } ?>	
	
</script> 
<ul class="entry_buttons">
<?php if($member_role == 'manager' || $member_role == 'basic') { ?>
    <li><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=people&layout=edit'); ?>"><?php echo ucwords(CRMText::_('COM_CRMERY_PEOPLE_ADD')); ?></a></li>
<?php } ?>	
    <li style='display:none'><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=import&import_type=people&layout=import'); ?>"><?php echo ucwords(CRMText::_('COM_CRMERY_IMPORT_PEOPLE')); ?></a></li>
</ul>
<h1><?php echo ucwords(CRMText::_('COM_CRMERY_PEOPLE_HEADER')); ?></h1>
<ul class="filter_lists">
<li class="filter_sentence">
    <?php echo CRMText::_('COM_CRMERY_SHOW'); ?>
    <span class="filters">
	<a id="people_type_link" class="dropdown" href="javascript:void(0);">
	<?php //echo $this->people_type_name; ?> 
	
	<?php 
	
	$filter_txt = 'All Referrals';
	
	if(isset($_GET['val']))
		{
			switch($_GET['val']){
				case 'assigned':
					$filter_txt='Work Received by me';
				break;
				case 'firm':
					$filter_txt = 'Work Received by my firm';
				break;
				case 'owner':
					$filter_txt = 'Work Referred by me';
				break;
				case 'firmowner':
					$filter_txt = 'Work Referred by my firm';
				break;
				case "3":
					$filter_txt='Pending Referrals';
				break;
				case "1":
					 $filter_txt='Work Accepted';
				break;
				case "2":
					 $filter_txt='Work Declined';
				break;
				case "unset":
				case "4":
					$filter_txt = 'All Referrals';
				break;
			}
	
		}
	
	?>
	<?php echo $filter_txt; ?>
	 
	</a>
	</span>
    <div class="filters" id="people_type">
	  <ul>
            <li><a class='filter_assigned' onclick="newfilter('assigned')">Work Received by me</a></li>   
	
	<?php if($member_role == 'manager' || $member_role == 'basic') { ?>		   
         <li><a class='filter_firm' onclick="newfilter('firm')">Work Received by my firm</a></li>   
	<?php } ?>	
	
               <li><a class='filter_owner' onclick="newfilter('owner')">Work Referred by me</a></li>   
	   
	   <?php if($member_role == 'manager' || $member_role == 'basic') { ?>				   
               <li><a class='filter_firmowner' onclick="newfilter('firmowner')">Work Referred by my firm</a></li>   
		<?php } ?>	
		
               <li><a class='filter_1' onclick="newfilter('1')">Work Accepted</a></li>   
               <li><a class='filter_2' onclick="newfilter('2')">Work Declined</a></li>   
               <li><a class='filter_3' onclick="newfilter('3')">Pending Referrals</a></li>   
               <li><a class='filter_4' onclick="newfilter('4')">All Referrals</a></li>   
           
        </ul>
		
        <!-- <ul>
            <?php //foreach ( $this->people_types as $title => $text){
               // echo "<li><a class='filter_".$title."' onclick=\"peopleType('$title')\">".$text."</a></li>";    
            //}?>
        </ul> -->
    </div>
   <!-- <?php //echo CRMText::_('COM_CRMERY_OWNED_BY'); ?>
    <span class="filters"><a class="dropdown" id="people_user_link"><?php echo $this->user_name; ?></a></span>
    <div class="filters" id="people_user">
        <ul>
            <li><a class="filter_user_<?php echo $this->user_id; ?>" onclick="peopleUser(<?php echo $this->user_id; ?>,0)"><?php echo CRMText::_('COM_CRMERY_ME'); ?></a></li>
            <?php if ( $this->member_role != 'basic' ){ ?>
                 <li><a class="filter_user_all" onclick="peopleUser('all',0)"><?php echo CRMText::_('COM_CRMERY_ALL_USERS'); ?></a></li>
            <?php } ?>
            <?php
               /*  if ( $this->member_role == 'exec' ){
                    if ( count($this->teams) > 0 ){
                        foreach($this->teams as $team){
                             echo "<li><a class='filter_team_".$team['team_id']."' onclick='peopleUser(0,".$team['team_id'].")'>".$team['team_name'].CRMText::_('COM_CRMERY_TEAM_APPEND')."</a></li>";
                         }
                    }
                }
                if ( count($this->users) > 0 ){
                    foreach($this->users as $user){
                        echo "<li><a class='filter_user_".$user['id']."' onclick='peopleUser(".$user['id'].")'>".$user['first_name']."  ".$user['last_name']."</a></li>";
                    }
                } */
            ?>
        </ul>
    </div> --> 
  <!--  <?php //echo CRMText::_('COM_CRMERY_WHO'); ?>
    <span class="filters"><a class="dropdown" id="people_stages_link"><?php //echo $this->stages_name; ?></a></span>
    <div class="filters" id="people_stages">
        <ul>
            <?php //foreach ( $this->stages as $title => $text ){
                //echo "<li><a class='filter_".$title."' onclick=\"peopleUpdated('".$title."')\">".$text."</a></li>";
           // }?>
        </ul>
    </div>  
    <?php //echo CRMText::_('COM_CRMERY_AND_WITH_STATUS'); ?>
    <span class="filters"><a class="dropdown" id="people_status_link"><?php //echo $this->status_name; ?></a></span>
    <div class="filters" id="people_status">
        <ul>
            <li><a class="filter_any" onclick="peopleStatus('any')"><?php //echo CRMText::_('COM_CRMERY_ANY_STATUS'); ?></a></li>
            <?php
                //foreach ( $this->status_list as $key => $status ){
                  //  echo "<li><a class='filter_".$status['id']."' onclick='peopleStatus(".$status['id'].")'>".$status['name']."</a></li>";
               // }
            ?>            
        </ul>
    </div> -->
	
    OR <?php echo CRMText::_('COM_CRMERY_NAMED'); ?>
    <td><input class="inputbox filter_input" name="name" type="text" placeholder="<?php echo CRMText::_('COM_CRMERY_ANYTHING'); ?>" value="<?php echo $this->people_filter; ?>"></td>
</li>
<li class="filter_sentence">
    <div class="ajax_loader"></div>
</li>
<!-- <li>
 <?php// echo CRMText::_('COM_CRMERY_SHOW'); ?>
    <span class="filters"><a id="people_val_link" class="dropdown" href="javascript:void(0);">Referrals Filter </a></span>
    <div class="filters" id="people_val">
        <ul>
               <li><a class='filter_assigned' onclick="newfilter('assigned')"> Work Received</a></li>   
               <li><a class='filter_owner' onclick="newfilter('owner')"> Work Referred</a></li>   
               <li><a class='filter_1' onclick="newfilter('1')"> Work Accepted</a></li>   
               <li><a class='filter_2' onclick="newfilter('2')"> Work Declined</a></li>   
               <li><a class='filter_0' onclick="newfilter('0')"> Pending Referrals</a></li>   
           
        </ul>
    </div>
</li> -->

</ul>
<div class="subline">
<ul class="matched_results">
    <li><span id="people_matched"></span> <?php echo CRMText::_('COM_CRMERY_PEOPLE_MATCHED'); ?> <?php //echo CRMText::_('COM_CRMERY_THERE_ARE'); ?> <?php //echo $this->totalPeople; ?> <?php //echo CRMText::_('COM_CRMERY_PEOPLE_IN_ACCOUNT'); ?></li>
</ul>
<span class="actions">
    <span class="filters"><a id="column_filter_link" class="dropdown"><?php echo CRMText::_('COM_CRMERY_SELECT_COLUMNS'); ?></a></span>
        <div class="filters" id="column_filter">
            <ul>
                 <?php $temp_field_arr = array('company','owner','country','assigned','source','added','status');
				
					//echo '<pre>';
					//print_r($this->selected_columns);
					//echo '</pre>';
					foreach ( $this->column_filters as $key => $text ){ ?>
                    <?php //$selected = ( in_array($key,$this->selected_columns) ) ? 'checked' : ''; ?>
                    <?php $selected = ( in_array($key,$temp_field_arr) ) ? 'checked' : ''; ?>
                    <li><input type="checkbox" class="column_filter" id="show_<?php echo $key; ?>" <?php echo $selected; ?> > <?php echo $text; ?></li>    
                <?php } ?> 
            </ul>
        </div>
        <?php if ( CrmeryHelperUsers::canExport() ){ ?>    
            <span class="filters"><a href="javascript:void(0)" onclick="exportCsv()"><?php echo CRMText::_('COM_CRMERY_EXPORT_CSV'); ?></a>
        <?php } ?>
</span>
</div>
<?php echo CrmeryHelperTemplate::getListEditActions(); ?>
<form method="post" id="list_form" action="<?php echo JRoute::_('index.php?option=com_crmery&view=people'); ?>">
<table class="com_crmery_table" id="people">
		  <?php echo $this->people_list->display(); ?>
</table>
<input type="hidden" name="list_type" value="people" />
</form>
<div id="templates" style="display:none;">
    <div id="note_modal" style="display:none;"></div>
    <div id="edit_button"><a class="edit_button_link" id="edit_button_link" href="javascript:void(0)"></a></div>
    <div id="edit_list_modal" style="display:none;" ></div>
</div>