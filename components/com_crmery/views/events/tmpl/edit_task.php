<?php error_reporting(0);
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

//assign event
 $event = $this->event;
?>
<div class="validate" id="edit_task_form" method="post">
	<?php
		if ( array_key_exists('id',$event) ) {
			echo '<input type="hidden" value="'.$event['id'].'" name="id" />';
		}
        $data = JRequest::get('get');
        if ( array_key_exists('parent_id',$data)) {
        	echo '<input type="hidden" name="parent_id" value="'.$data['parent_id'].'" />';
    	}
	?>
	<input type="hidden" name="task" value="save" />
	<input type="hidden" name="type" value="task" />
	<input type="hidden" name="edit_screen" value="1" />
	
	<div class="crmeryRow">
		<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_EDIT_TASK_NAME')); ?><span class="required">*</span></div>
		<div class="crmeryValue"><input class="inputbox required" type="text" name="name" value="<?php if(array_key_exists('name',$event)) echo $event['name']; ?>" /></div>
	</div>

	<div class="crmeryRow">
	<div class="crmeryField">
		<?php echo CRMText::_('COM_CRMERY_ASSOCIATE'); ?>
	</div>
	<div class="crmeryValue">
        <div id="associate_to_container">
            <?php if ( array_key_exists('association_name',$event) ) { ?>
                <div id="associate_to">
                    <input class="inputbox" type="text" name="associate_name" value="<?php echo $event['association_name']; ?>" />
                </div>                                                               
            <?php } else { ?>
            	<?php if ( JRequest::getVar('association_id') ) { $association_name = $this->association_name;  } else { $association_name = ucwords(CRMText::_('COM_CRMERY_COMPANY_DEAL_OR_PERSON'));} ?>
                <span class="associate_to"><?php echo $association_name; ?></span>
                <div style="display:none;" id="associate_to">
                    <input class="inputbox" type="text" name="associate_name" value="" />
                </div>
            <?php } ?>
            <?php $association_id = JRequest::getVar('association_id'); ?>
			<?php $association_type = JRequest::getVar('association_type'); ?>
			<?php if ( $association_id ){ ?>
				<input type="hidden" name="association_id" value="<?php echo $association_id; ?>" />
				<input type="hidden" name="association_type" value="<?php echo $association_type; ?>" />
			<?php } ?>
        </div>
    </div>
</div>
    
	<div class="crmeryRow">
		<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_EDIT_TASK_TYPE'); ?></div>
		<div class="crmeryValue">
			<select class="inputbox" name="category_id">
				<?php 
                    $categories = CrmeryHelperEvent::getCategories();
                    echo JHtml::_('select.options', $categories, 'value', 'text', $event['category_id'], true);
                ?>
			</select>
		</div>
	</div>
	<div class="crmeryRow">
		<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_EDIT_TASK_ASSIGN_TO'); ?></div>
		<div class="crmeryValue">
			<select class="inputbox" name="assignee_id">
				<?php 
					$users = array();
					$users[CrmeryHelperUsers::getUserId()] = CRMText::_('COM_CRMERY_ME');
                    $users += CrmeryHelperDropdown::getUserNames();
                    echo JHtml::_('select.options', $users, 'value', 'text', $event['assignee_id'], true);
                ?>
			</select>
		</div>
	</div>
	<?php if ( array_key_exists('id',$event) && $event['id'] > 0 ){ ?>
	<div class="crmeryRow">
		<div class="crmeryField">
			<?php echo CRMText::_('COM_CRMERY_CREATED_BY'); ?>
		</div>
		<div class="crmeryValue">
			<?php echo $event['owner_first_name'].' '.$event['owner_last_name']; ?>
		</div>
	</div>
	<?php } ?>
	<div class="crmeryRow">
		<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_EDIT_TASK_DUE_DATE'); ?></div>
		<div class="crmeryValue">
			<div id="due_date_container">
			    <?php if (array_key_exists('due_date',$event) ) { ?>
					<span style="display:none;" class="due_date"><?php echo CRMText::_('COM_CRMERY_EDIT_TASK_DUE_DATE_MESSAGE'); ?></span>
				<div id="due_date">
					<input id="due_date_input"class="inputbox date_input" type="text" name="due_date_input" value="<?php if (array_key_exists('due_date',$event)) echo $event['due_date_formatted']; ?>" />									
					<input id="due_date_input_hidden" name="due_date" type="hidden" value="<?php if ( array_key_exists('due_date_unformatted',$event) ){ echo $event['due_date_unformatted']; } ?>" />
					<select class="inputbox" name="due_date_hour">
						<?php 
                            $time = CrmeryHelperDate::getTimeIntervals();
                            echo JHtml::_('select.options', $time, 'value', 'text', $event['due_date_hour'], true);
                        ?>
					</select>
				</div>
				<?php } else { ?>
				<span class="due_date"><?php echo CRMText::_('COM_CRMERY_EDIT_TASK_DUE_DATE_MESSAGE'); ?></span>
                <div style="display:none;" id="due_date">
                    <input id="due_date_input" class="inputbox date_input" type="text" name="due_date_input" value="<?php if (array_key_exists('due_date',$event)) echo $event['due_date_formatted']; ?>" />                                    
                    <input id="due_date_input_hidden" name="due_date" type="hidden" value="<?php if ( array_key_exists('due_date_unformatted',$event) ){ echo $event['due_date_unformatted']; } ?>" />
                    <select class="inputbox" name="due_date_hour">
						<?php 
                            $time = CrmeryHelperDate::getTimeIntervals();
                            echo JHtml::_('select.options', $time, 'value', 'text', $event['due_date_hour'], true);
                        ?>
					</select>
                </div>
                <?php } ?>   
			</div>
		</div>
	</div>
    <div class="crmeryRow">
     	<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_EDIT_TASK_REPEAT'); ?></div>
        <div class="crmeryValue">
            <select class="inputbox" name="repeats">
                <?php 
                    $repeat_intervals = CrmeryHelperEvent::getRepeatIntervals();
                    echo JHtml::_('select.options', $repeat_intervals, 'value', 'text', $event['repeats'], true);
                ?>
            </select>
        </div>
    </div>
    <div class="crmeryRow">
		<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_END_DATE'); ?></div>
		<div class="crmeryValue">
				<?php if ( array_key_exists('end_date',$event) && $event['end_date'] != null ){ $hidden = "style='display:none;'"; $show = ""; }else{ $hidden = ""; $show = "style='display:none;'"; } ?>
				<span <?php echo $hidden; ?> class="end_date"><?php echo CRMText::_('COM_CRMERY_END_DATE_MESSAGE'); ?></span>
                <div <?php echo $show; ?> id="end_date">
                    <input id="end_date_input" class="inputbox date_input" type="text" name="end_date_input" value="<?php if (array_key_exists('end_date',$event)) echo $event['end_date_formatted']; ?>" />                                    
                    <input id="end_date_input_hidden" name="end_date" type="hidden" value="<?php if ( array_key_exists('end_date_unformatted',$event) ){ echo $event['end_date_unformatted']; } ?>" />
                </div>
		</div>
	</div>
    <?php if ( array_key_exists('repeats',$event) && $event['repeats'] != "none" ) { ?>
    <div class="crmeryRow">
      <div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_UPDATE_FUTURE_EVENTS'); ?></div>
      <div class="crmeryValue"><input class="inputbox" type="checkbox" name="update_future_events" checked="checked" /></div>    
    </div>
    <?php } ?>
	<div class="crmeryRow">
		<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_EDIT_TASK_DESCRIPTION'); ?></div>
		<div class="crmeryValue">
			<textarea class="inputbox" name="description"><?php if(array_key_exists('description',$event)) echo $event['description']; ?></textarea>
		</div>
	</div>
	<div class="crmeryRow">
		<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_PUBLIC_EVENT'); ?></div>
		<div class="crmeryValue">
			<div class="crmeryValue"><input class="inputbox" value="1" type="checkbox" name="public" <?php $checked = ( $event['public'] == 1) ? "checked" : ""; echo $checked; ?> /></div>
		</div>
	</div>
	<div class="actions">
		<a href="javascript:void(0);" class="button" onclick="saveAjax('edit_task','event');"><?php echo CRMText::_('COM_CRMERY_SAVE_BUTTON'); ?></a>
		<?php echo CRMText::_('COM_CRMERY_OR'); ?>
		<a href="javascript:void(0);" onclick="closeTaskEvent('task')"><?php echo CRMText::_('COM_CRMERY_CANCEL_BUTTON'); ?></a>
	</div>
</div>