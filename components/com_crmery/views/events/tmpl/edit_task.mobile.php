<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

if(!isset($this->association_id)) { ?>
	<div data-role='header' data-theme='b'>
		<h1><?php echo ucwords(CRMText::_('COM_CRMERY_ADD_TASK')); ?></h1>
			<a href="<?php echo JRoute::_('index.php?option=com_crmery&view=dashboard'); ?>" data-icon="back" class="ui-btn-left">
				<?php echo CRMText::_('COM_CRMERY_BACK'); ?>
			</a>
	</div>

	<div data-role="content">
<?php } ?>

		<form id="task_edit" method="POST" action="<?php echo JRoute::_('index.php?option=com_crmery&controller=main&model=event&return=events&task=save'); ?>" >
		<div id="editForm">
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_NAME'); ?></div>
				<div class="crmeryValue">
					<input type="text" class="inputbox" name="name" value="" />
				</div>	
			</div>
			<div class="crmeryRow">
				<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_CATEGORY'); ?></div>
				<div class="crmeryValue">
					<select data-native-menu="false" data-overlay-theme="a" data-theme="c" name="category_id" tabindex="-1">
						<?php 
	                        $categories = CrmeryHelperEvent::getCategories();
	                        echo JHtml::_('select.options', $categories, 'value', 'text', "", true);
	                    ?>
					</select>
				</div>	
			</div>
			<div class="crmeryRow">
				<div class="crmeryField"></div>
				<div class="crmeryValue">
					<label for="due_date"><?php echo CRMText::_('COM_CRMERY_DUE_DATE'); ?>:</label>
					<input type="date" name="due_date_hidden" class="inputbox" id="due_date" value="" />
					<input type="hidden" name="due_date" id="due_date_hidden" value="" />
				</div>	
			</div>
			<input data-theme="c" type="button" onclick="addTaskEntry();" name="submit"  value="<?php echo CRMText::_('COM_CRMERY_SUBMIT'); ?>" />
			<?php if(isset($this->association_id)) { ?>
				<input type="hidden" name="association_id" id="association_id" value="<?php echo $this->association_id; ?>" />
				<input type="hidden" name="association_type" id="association_type" value="<?php echo $this->association_type; ?>" />
			<?php } ?>
		</div>
		</form>
	</div>