<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

if ( JRequest::getVar('loc') ){
    $model =& JModel::getInstance('event','CrmeryModel');
    $events = $model->getEvents(JRequest::getVar('loc'),null,JRequest::getVar(JRequest::getVar('loc').'_id'));
    $this->events = $events;
}

if ( count($this->events) > 0 ){
    foreach ( $this->events as $event ){
        echo '<li>';
        echo "<a href='".JRoute::_('index.php?option=com_crmery&view=events&id='.$event['id'])."'>";
            echo '<span class="ui-li-count">'.CrmeryHelperDate::formatDate($event['due_date']).'</span>';
            if ( $event['completed'] == 1 ){
                $completed = "line-through";
            }else{
                $completed = "";
            }
            echo "<h3 class='ui-li-heading'>".$event['name']."</h3>";
           switch($event['association_type']){
               case "company":
                   echo "<div class='ui-li-desc'>(".$event['company_name'].")</div>";
                   break;
               case "deal":
                   echo "<div class='ui-li-desc'>(".$event['deal_name'].")</div>";
                   break;
               case "person":
                   echo "<div class='ui-li-desc'>(".$event['person_first_name']." ".$event['person_last_name'].")</div>";
                   break;
           }
        echo '</a></li>';
    }
}