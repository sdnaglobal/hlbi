<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<form class="print_form">
    <?php if(JRequest::getVar('view')!="print") { ?>
        <div id="controls_area">
            <a href="javascript:void(0);" onclick="addTaskEvent('task');" ><?php echo ucwords(CRMText::_('COM_CRMERY_ADD_TASK')); ?></a> 
            <a href="javascript:void(0);" onclick="addTaskEvent('event');" ><?php echo ucwords(CRMText::_('COM_CRMERY_ADD_EVENT')); ?></a> 
            <a href="javascript:void(0);" id="templates_link" class="dropdown"><?php echo ucwords(CRMText::_('COM_CRMERY_TEMPLATES')); ?></a>
            <div class="filters" id="templates">
                <ul>
                    <?php $templates = CrmeryHelperCrmery::getTaskTemplates(JRequest::getVar('layout'));
                        if ( count($templates) > 0 ) { foreach ( $templates as $template) { ?>
                            <li><a href="javascript:void(0)" onclick="createTemplate(<?php echo $template['id']; ?>)"><?php echo $template['name']; ?></a>
                        <?php } } else { ?>
                            <li><?php echo CRMText::_('COM_CRMERY_NO_TEMPLATES_HAVE_BEEN_CREATED'); ?></li>
                        <?php } ?>
                </ul>
            </div>
            <a href="javascript:void(0);" onclick="printItems(this)"><?php echo CRMText::_('COM_CRMERY_PRINT'); ?></a>
        </div>
    <?php } ?>
<div id="event_list">
<div class="container">
    <?php if(JRequest::getVar('view')!="print") { ?>
        <div class="filter_container">
        <?php echo CRMText::_('COM_CRMERY_SHOW_TASKS_FOR'); ?>:
            <span class="filters" ><a class="dropdown event_user" id="event_user_link" ><?php echo CRMText::_('COM_CRMERY_ME'); ?></a></span>
            <div class="filters" id="event_user">
                <ul>
                    <?php
                        $user_role = CrmeryHelperUsers::getRole();
                        $user_id = CrmeryHelperUsers::getUserId();
                    ?>
                    <li><a class="filter_user_<?php echo $user_id; ?>" onclick="updateEventList(<?php echo $user_id; ?>,0)" ><?php echo CRMText::_('COM_CRMERY_ME'); ?></a></li>
                    <?php if ( $user_role != 'basic' ) { ?>
                        <li><a class="filter_user_all" onclick="updateEventList('all',0)" >all users</a></li>
                    <?php } ?>
                    <?php
                        if ( $user_role == 'exec' ){
                            $teams = CrmeryHelperUsers::getTeams();
                            if ( count($teams) > 0 ){
                                foreach($teams as $team){
                                     echo "<li><a class='filter_team_".$team['team_id']."' onclick='updateEventList(0,".$team['team_id'].")'>".$team['team_name'].CRMText::_('COM_CRMERY_TEAM_APPEND')."</a></li>";
                                 }
                            }
                        }
                        $users = CrmeryHelperUsers::getUsers();
                        if ( count($users) > 0 ){
                            foreach($users as $user){
                                echo "<li><a class='filter_user_".$user['id']."' onclick='updateEventList(".$user['id'].",0)'>".$user['first_name']."  ".$user['last_name']."</a></li>";
                            }
                        }
                        
                    ?>
                </ul>
            </div>
        </div>
    <?php } ?>
<div id="task_container">
		<div id="task_list">
			<?php
			     $task_list = CrmeryHelperView::getView('events','event_listings',array(array('ref'=>'events','data'=>$this->events)));
			     $task_list->display();
			?>
		</div>


</div>
</div>
</div>
<?php if(JRequest::getVar('view')!="print") { ?>
    <div id="controls_area_bottom">
        <a class="button" href="<?php echo JRoute::_('index.php?option=com_crmery&view=events'); ?>"><?php echo ucwords(CRMText::_('COM_CRMERY_SEE_ALL_TASKS')); ?></a>
    </div>
<?php } ?>
</form>