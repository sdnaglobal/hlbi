<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

        $current_heading = "";

        if ( count($this->events) > 0 ){
            foreach ( $this->events as $event ){

              $display_date = ( $event['type'] == "event" ) ? $event['start_time']: $event['due_date'];
              $time = ( $event['type'] == "event" ) ? $event['start_time_hour'] : $event['due_date_hour'];
             
              $display_date = $display_date == "" ? CRMText::_('COM_CRMERY_NA') : CrmeryHelperDate::formatDate($display_date,false,false);

              $relative_date_title = CrmeryHelperDate::getRelativeDate($display_date);

                if($current_heading!=$relative_date_title) {
                  echo '<h4>'.$relative_date_title.'</h4>';
                  $current_heading = $relative_date_title;
                }

                echo '<div class="com_crmery_task_event" id="com_crmery_listing_'.$event['id'].'">';

                    echo '<input type="checkbox" class="event_list_checkbox" name="item_id['.$event['id'].']" />';
                    $display_date = ( $event['type'] == "event" ) ? $event['start_time'] : $event['due_date'];
                    if ( $display_date == "" ) $display_date = CRMText::_('COM_CRMERY_NA');
                    echo '<div class="date">'.CrmeryHelperDate::formatDateString($display_date).' '.CrmeryHelperDate::formatTime($time,"(".CrmeryHelperUsers::getTimeFormat().")").'</div>';

                    if ( $event['completed'] == 1 ){
                        $completed = "line-through";
                    }else{
                        $completed = "";
                    }
                    echo '<a class="dropdown '.$completed.'" id="event_menu_'.$event['id'].'_link" >';
                    echo $event['name'];
                    echo '</a>';
                  
                   switch($event['association_type']){
                       case "company":
                           echo "<div class='task_subheading'><a href='".JRoute::_('index.php?option=com_crmery&view=companies&layout=company&id='.$event['company_id'])."'>".$event['company_name']."</a></div>";
                           break;
                       case "deal":
                           echo "<div class='task_subheading'><a href='".JRoute::_('index.php?option=com_crmery&view=deals&layout=deal&id='.$event['deal_id'])."'>".$event['deal_name']."</a></div>";
                           break;
                       case "person":
                           echo "<div class='task_subheading'><a href='".JRoute::_('index.php?option=com_crmery&view=people&layout=person&id='.$event['person_id'])."'>".$event['person_first_name']." ".$event['person_last_name']."</a></div>";
                           break;
                   }
                   echo '<div id="event_form_'.$event['id'].'">';
                            echo '<input type="hidden" name="event_id" value="'.$event['id'].'" />';
                            echo '<input type="hidden" name="parent_id" value="'.$event['parent_id'].'" />';
                              if ( $event['type'] == "task" ){
                                echo '<input type="hidden" name="due_date" value="'.$event['due_date'].'" />';
                              }else{
                                echo '<input type="hidden" name="start_time" value="'.$event['start_time'].'" />';
                                echo '<input type="hidden" name="end_time" value="'.$event['end_time'].'" />';
                              }
                            echo '<input type="hidden" name="event_type" value="'.$event['type'].'" />';
                            echo '<input type="hidden" name="repeats" value="'.$event['repeats'].'" />';
                            echo '<input type="hidden" name="type" value="single" />';
                   echo '</div>';
                echo '</div>';
                echo '<div class="filters" id="event_menu_'.$event['id'].'">';
                  echo '<ul>';
                   if ( $event['completed'] == 1 ){
                    echo '<li><a href="javascript:void(0);" onclick="markEventIncomplete(this)" >'.CRMText::_('COM_CRMERY_MARK_INCOMPLETE').'</a></li>';
                   }else{
                    echo '<li><a href="javascript:void(0);" onclick="markEventComplete(this)" >'.CRMText::_('COM_CRMERY_MARK_COMPLETE').'</a></li>';
                    echo '<li><a href="javascript:void(0);" onclick="postponeEvent(this,1)" >'.CRMText::_('COM_CRMERY_POSTPONE_1_DAY').'</a></li>';
                    echo '<li><a href="javascript:void(0);" onclick="postponeEvent(this,7)" >'.CRMText::_('COM_CRMERY_POSTPONE_7_DAYS').'</a></li>';
                  }
                    $id = ( array_key_exists('parent_id',$event) && $event['parent_id'] ) != 0 ? $event['parent_id'] : $event['id'];
                    echo '<li><a href="javascript:void(0);" onclick="editEvent('.$id.',\''.$event['type'].'\')" >'.CRMText::_('COM_CRMERY_EDIT').'</a></li>';
                    echo '<li><a href="javascript:void(0);" onclick="deleteEvent(this)" >'.CRMText::_('COM_CRMERY_DELETE').'</a></li>';
                  echo '</ul>';
                echo '</div>';
            }
        }
        
?>