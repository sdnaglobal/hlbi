<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<script type="text/javascript">
</script>
<form class="print_form" method="POST" target="_blank" action="<?php echo JRoute::_('index.php?option=com_crmery&view=print'); ?>">
<input type="hidden" name="layout" value="events" />
<input type="hidden" name="model" value="event" />
<div id="event_list">
    <div class="dash_float_header">
        <a class="minify" onclick=""></a><h2><?php echo ucwords(CRMText::_('COM_CRMERY_TASKS_HEADER')); ?></h2>
    </div>
    <div>
<div id="controls_area">
    <a href="javascript:void(0);" onclick="addTaskEvent('task');" ><?php echo ucwords(CRMText::_('COM_CRMERY_ADD_TASK')); ?></a> 
    <a href="javascript:void(0);" onclick="addTaskEvent('event');" ><?php echo ucwords(CRMText::_('COM_CRMERY_ADD_EVENT')); ?></a> 
    <a href="javascript:void(0);" onclick="printItems(this);"><?php echo CRMText::_('COM_CRMERY_PRINT'); ?></a>
</div>
<div class="container">
<div class="filter_container">
    <?php echo CRMText::_('COM_CRMERY_SHOW_TASKS_FOR'); ?>:
    <span class="filters" ><a class="dropdown event_user" id="event_user_link" ><?php echo CRMText::_('COM_CRMERY_ME'); ?></a></span>
    <div class="filters" id="event_user">
        <ul>
            <?php
                $user_role = CrmeryHelperUsers::getRole();
                $user_id = CrmeryHelperUsers::getUserId();
            ?>
            <li><a class="filter_user_<?php echo $user_id; ?>" onclick="updateEventList(<?php echo $user_id; ?>,0)" ><?php echo CRMText::_('COM_CRMERY_ME'); ?></a></li>
            <?php if ( $user_role != 'basic' ) { ?>
                <?php if ( $user_role == "exec" ){ ?>
                    <li><a class="filter_user_all" onclick="updateEventList('all',0)" ><?php echo ucwords(CRMText::_('COM_CRMERY_ALL_USERS')); ?></a></li>
                <?php } ?>
                <?php if ( $user_role == "manager" ){ ?>
                    <li><a class="filter_user_all" onclick="updateEventList('all',0)" ><?php echo ucwords(CRMText::_('COM_CRMERY_ALL_USERS_ON_MY_TEAM')); ?></a></li>
                <?php } ?>
            <?php } ?>
            <?php
                if ( $user_role == 'exec' ){
                    $teams = CrmeryHelperUsers::getTeams();
                    if ( count($teams) > 0 ){
                        foreach($teams as $team){
                             echo "<li><a class='filter_team_".$team['team_id']."' onclick='updateEventList(0,".$team['team_id'].")'>".$team['team_name'].CRMText::_('COM_CRMERY_TEAM_APPEND')."</a></li>";
                         }
                    }
                }
                $users = CrmeryHelperUsers::getUsers();
                if ( count($users) > 0 ){
                    foreach($users as $user){
                        echo "<li><a class='filter_user_".$user['id']."' onclick='updateEventList(".$user['id'].",0)'>".$user['first_name']."  ".$user['last_name']."</a></li>";
                    }
                }
                
            ?>
        </ul>
    </div>
</div>
<div id="task_container">
		<div id="task_list">
			<?php
			     $task_list = CrmeryHelperView::getView('events','event_listings',array(array('ref'=>'events','data'=>$this->events)));
			     $task_list->display();
			?>
		</div>


<div class="controls_area"><a class="button" href="<?php echo JRoute::_('index.php?option=com_crmery&view=events'); ?>"><?php echo ucwords(CRMText::_('COM_CRMERY_SEE_ALL_TASKS')); ?></a></div>
</div>
</div>
</div>
</div>
</form>