<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewEvents extends CrmeryHelperView
{
	function display($tpl = null)
	{
		//event data
		$model = & JModel::getInstance('event','CrmeryModel');

		$event_id = JRequest::getVar('id');
		$view = JRequest::getVar('view');
		$layout = $this->getLayout();

		$document =& JFactory::getDocument();

		//if id requested
		if( $layout!='default' && isset($event_id)) {
			$events = $model->getEvent($event_id);
		}else{
		//else load all events
			if ( $layout != 'edit_task' || $layout != "edit_event" ){ 
				
				if(CrmeryHelperTemplate::isMobile()) {
					// $model->set('current_events',true);
					$document->addScriptDeclaration('loc="events";');
				}
				if ( JRequest::getVar('loc') ){
		            $events = $model->getEvents(JRequest::getVar('loc'),null,JRequest::getVar(JRequest::getVar('loc').'_id'));
				}else{
					$events = $model->getEvents();
				}
			}
		}

		if ( CrmeryHelperTemplate::isMobile() && isset($event_id)){
			$person_model = JModel::getInstance('people','CrmeryModel');
			$person_model->set('event_id',$event_id);
			$person_model->set('recent',false);
			$person_model->set('_id',null);
			$people = $person_model->getPeople();
			$this->assignRef('people',$people);
		}

		if ( $layout == "default" ){
			$state = $model->getState();

			$event_statuses = CrmeryHelperEvent::getEventStatuses();
			$this->assignRef('event_statuses',$event_statuses);

			$event_types = CrmeryHelperEvent::getEventTypes();
			$this->assignRef('event_types',$event_types);

			$event_categories = CrmeryHelperEvent::getCategories(TRUE);
			$this->assignRef('event_categories',$event_categories);

			$event_due_dates = CrmeryHelperEvent::getEventDueDates();
			$this->assignRef('event_due_dates',$event_due_dates);

			$event_associations = CrmeryHelperEvent::getEventAssociations();
			$this->assignRef('event_associations',$event_associations);

			$event_users = CrmeryHelperUsers::getUsers(NULL,TRUE);
			$this->assignRef('event_users',$event_users);

			$event_teams = CrmeryHelperUsers::getTeams();
			$this->assignRef('event_teams',$event_teams);

			$this->assignRef('state',$state);
			$view = JRequest::getVar('view');
		}

		$layout = JRequest::getVar('layout','list');
		$document->addScriptDeclaration('var layout="'.$layout.'"');
		
		//assign results to view
		$this->assignRef('events',$events);
        $this->assignRef('member_role',CrmeryHelperUsers::getRole());
        $this->assignRef('user_id',CrmeryHelperUsers::getUserId());
        $this->assignRef('team_id',CrmeryHelperUsers::getTeamId());
		
		//display
		parent::display($tpl);
	}
	
}
?>
