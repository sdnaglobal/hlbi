<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
	$custom = CrmeryHelperCustom::generateCustom($this->item_type,$this->item['id']);
    $count = 0;
   
  	if ( count($custom) > 0 ){ ?>

    <h2><?php echo CRMText::_('COM_CRMERY_EDIT_CUSTOM'); ?></h2>

            <div class="columncontainer">
                <div class="twocolumn">
                    <table class="com_crmery_table">

	<?php foreach ( $custom as $field => $value ) {
        $count++;
        $k=$count%3;
        switch ( $value['type'] ){
        	case "forecast":
        		$custom_field_filter = array_key_exists('forecast',$this->item) ? $this->item['forecast'] : 0;
        	break;
        	case "text":
        		$custom_field_filter = array_key_exists('selected',$value) ? $value['selected'] : ""; 
    		break;
        	case "number":
        		$custom_field_filter = array_key_exists('selected',$value) ? $value['selected'] : "";
    		break;
        	case "currency":
        		$custom_field_filter = array_key_exists('selected',$value) ? $value['selected'] : "";
    		break;
    		case "date":
    			$custom_field_filter = array_key_exists('selected',$value) && $value['selected'] != 0 && $value['selected'] != "0000-00-00 00:00:00" ? CrmeryHelperDate::formatDate($value['selected']) : "";
			break;
			case "picklist":
                if ( $value['multiple_selections'] == 1 ){
                    $custom_field_filter = CRMText::_('COM_CRMERY_CLICK_TO_EDIT');
                    $checked = array_key_exists('selected',$value) && !is_null($value['selected']) && $value['selected'] != "" && $value['selected'] != CRMText::_('COM_CRMERY_CLICK_TO_EDIT') ? @unserialize($value['selected']) : array();
                }else{
                    $custom_field_filter = ( is_array($value) && array_key_exists('values',$value) && is_array($value['values']) && array_key_exists('selected',$value) && in_array($value['selected'],$value['values']) ) ? $value['selected'] : CRMText::_('COM_CRMERY_CLICK_TO_EDIT');
                }
			break;
        }
            echo '<tr>';
			echo '<th class="customFieldHead">'.$value['name'].'</th>';
			echo '<td>';
                //determine type of input
                switch ( $value['type'] ){

                    case "text":
                    case "number":
                    case "currency": 
                    case "date":
                        echo '<span>'.$custom_field_filter.'</span>';
                    break;
                    case "picklist":
                        if ( $value['multiple_selections'] == 1 ){
                            echo '<ul class="unstyled">';
                                if ( is_array($value) && array_key_exists('values',$value) && count($value['values']) > 0 ){ foreach($value['values'] as $id => $name ) {
                                    $isChecked = array_key_exists($id,$value['values']) && $value['selected'][$id] == 1 ? "checked='checked'" : "";
                                    echo '<li><input '.$isChecked.' name="custom_'.$value['id'].'['.$id.']" onclick="saveEditableModal(\''.$value['id'].'_form\');" type="checkbox" value="1" /> - '.$name.'</li>';
                                }}
                            echo '</ul>';
                        }else{
                            echo $custom_field_filter;
                        }
                    break;
                    case "forecast":
                    	echo '<span class="forecast">';
                        	echo CrmeryHelperConfig::getCurrency().$custom_field_filter;
                    	echo '</span>';
                    break;
            	}
			echo '</td>';
            echo '</tr>';
	}
    echo '</div>';
	echo '</table>';                
  	echo '</div>';
} ?>