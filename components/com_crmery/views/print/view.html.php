<?php

/*------------------------------------------------------------------------

# CRMery

# ------------------------------------------------------------------------

# @author CRMery

# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.

# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Website: http://www.crmery.com

-------------------------------------------------------------------------*/

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' ); 



jimport( 'joomla.application.component.view');



class CrmeryViewPrint extends CrmeryHelperView

{

	function display($tpl = null)
	{

        /* Model // function info */

        $model_name = JRequest::getVar('model');

        $layout = $this->getLayout();

        $document = JFactory::getDocument();

        if ( !$model_name ){
		
            switch ( $layout ){

                case "person":

                    $model_name = "people";

                break;

                case "company":

                    $model_name = "companies";

                break;

                case "deal":

                    $model_name = "deal";

                break;

				 case "countryregion":

                    $model_name = "countryregion";

                break;

				 case "country_regiontoregion":

                    $model_name = "regiontoregion";

                break;


            }

        }

		
        if ( $layout != "report" ){

            $model = JModel::getInstance($model_name,'CrmeryModel');

            $params = NULL;

            switch (  $model_name ){

                case "company":

                    $func = "companies";

                break;

                case "deal":

                    $func = "deals";

                break;

                case "people":

                    $func = "people";

                break;
				
				case "report":

                    $func = "countryReport";

                break;

                case "event":

                case "events":

                    $func = "events";

                    if ( $layout == "calendar" ){

                        $params = "calendar";



                        //load js libs

                        $document->addScript( JURI::base().'components/com_crmery/media/js/fullcalendar.js' );

                        $document->addScript( JURI::base().'components/com_crmery/media/js/calendar_manager.js' );

                        

                        //load required css for calendar

                        $document->addStyleSheet( JURI::base().'components/com_crmery/media/css/fullcalendar.css' );

                    }

                break;

            }

        }

		
		
        switch ( $layout ){



            case "report":

                $report = JRequest::getVar('report');

                $item_id = ( JRequest::getVar('ids') ) ? JRequest::getVar('ids') : NULL;

                if ( is_array($item_id) )
				{

                    $items = array();

                    foreach ( $item_id as $key => $item ){

                        $items[] = $item;

                    }

                }
				else
				{

                    $items = $item_id;

                }

                switch ( $report ){

                    case "sales_pipeline":

                        $model =& JModel::getInstance('deal','CrmeryModel');

                        $model->set('_id',$items);

                        $state = $model->getState();

                        $reports = $model->getReportDeals();

                        $header = CrmeryHelperView::getView('reports','sales_pipeline_header',array(array('ref'=>'state','data'=>$state),array('ref'=>'reports','data'=>$reports)));

                        $table = CrmeryHelperView::getView('reports','sales_pipeline_filter',array(array('ref'=>'reports','data'=>$reports)));

                        $footer = CrmeryHelperView::getView('reports','sales_pipeline_footer');

                    break;

					

					//----- CUSTOMIZATION PART START HERE -------

					

					case "referred_country_region":

                        $model =& JModel::getInstance('countryregion','CrmeryModel');

                        $model->set('_id',$items);

                        $state = $model->getState();

                        $reports = $model->getReportDeals();

						

						//--------------  CUSTOMIZATION START ----------

			

						$db =& JFactory::getDBO();

						//gen query

						$query = $db->getQuery(true);

						

						$query->select("*");

						$query->from("#__crmery_deals AS l");

						$query->leftJoin("#__crmery_companies AS cm ON l.company_id=cm.id");

						$query->leftJoin("#__crmery_region AS r ON cm.region_id = r.region_id");

						$query->leftJoin("#__crmery_currencies AS c ON cm.country_id = c.country_id");

						//set query

						$db->setQuery($query);

						//load list

						$reports = $db->loadAssocList();		

						

						//--------------  CUSTOMIZATION END ----------						

						

						

                        $header = CrmeryHelperView::getView('reports','country_region_header',array(array('ref'=>'state','data'=>$state),array('ref'=>'reports','data'=>$reports)));

                        $table = CrmeryHelperView::getView('reports','country_region_filter',array(array('ref'=>'reports','data'=>$reports)));

                        $footer = CrmeryHelperView::getView('reports','country_region_footer');

                    break;

					

					case "country_regiontoregion":

                        $model =& JModel::getInstance('regiontoregion','CrmeryModel');

                        $model->set('_id',$items);

                        $state = $model->getState();
						
                        $reports = $model->getReportDeals();

						
						//--------------  CUSTOMIZATION START ----------

			

						$db =& JFactory::getDBO();

						//gen query

						$query = $db->getQuery(true);

						

						$query->select("*");

						$query->from("#__crmery_companies AS cm");

						$query->leftJoin("#__crmery_currencies AS c ON cm.country_id = c.country_id");

						$query->leftJoin("#__crmery_region AS r ON r.region_id = cm.region_id");

						$query->leftJoin("#__crmery_deals AS l ON l.company_id = cm.id");			

						$query->where("cm.region_id ='2'");

						//set query

						$db->setQuery($query);

						//load list

						$reports = $db->loadAssocList();		

						

						//--------------  CUSTOMIZATION END ----------						

						

						

                        $header = CrmeryHelperView::getView('reports','country_regiontoregion_header',array(array('ref'=>'state','data'=>$state),array('ref'=>'reports','data'=>$reports)));

                        $table = CrmeryHelperView::getView('reports','country_regiontoregion_filter',array(array('ref'=>'reports','data'=>$reports)));

                        $footer = CrmeryHelperView::getView('reports','country_regiontoregion_footer');

                    break;	
					
					//	REPORT FOR COUNTRY STARTS FROM HERE
					
					
					case "countryReport":

                        $model =& JModel::getInstance('regiontoregion','CrmeryModel');
						$model->set('_id',$items);

                        //$state = $model->getState();
						//$reports = $model->getReportDeals();

						$db =& JFactory::getDBO();
						$query = $db->getQuery(true);
						
						$query->select("*");
						$query->from("#__crmery_companies AS cm");
						$query->leftJoin("#__crmery_currencies AS c ON cm.country_id = c.country_id");
						$query->leftJoin("#__crmery_region AS r ON r.region_id = cm.region_id");
						$query->leftJoin("#__crmery_deals AS l ON l.company_id = cm.id");			
						$query->where("cm.region_id ='2'");

						$db->setQuery($query);
						$reports = $db->loadAssocList();		

                        $header = CrmeryHelperView::getView('reports','print_country_reports',array(array('ref'=>'state','data'=>$state),array('ref'=>'reports','data'=>$reports)));

                        //$table = CrmeryHelperView::getView('reports','country_regiontoregion_filter',array(array('ref'=>'reports','data'=>$reports)));

                        //$footer = CrmeryHelperView::getView('reports','country_regiontoregion_footer');

                    break;	
					
					//	REPORT FOR COUNTRY ENDS HERER
									

					

					

					//----- CUSTOMIZATION PART START HERE -------

					

                    case "source_report":

                        $model =& JModel::getInstance('deal','CrmeryModel');

                        $model->set('_id',$items);

                        $reports = $model->getDeals();

                        $state = $model->getState();

                        $header = CrmeryHelperView::getView('reports','source_report_header',array(array('ref'=>'state','data'=>$state),array('ref'=>'reports','data'=>$reports)));

                        $table = CrmeryHelperView::getView('reports','source_report_filter',array(array('ref'=>'reports','data'=>$reports)));

                        $footer = CrmeryHelperView::getView('reports','source_report_footer');

                    break;

                    case "roi_report":   

                        $model =& JModel::getInstance('source','CrmeryModel');

                        $model->set('_id',$items);

                        $sources = $model->getRoiSources();

                        $header = CrmeryHelperView::getView('reports','roi_report_header');

                        $table = CrmeryHelperView::getView('reports','roi_report_filter',array(array('ref'=>'sources','data'=>$sources)));

                        $footer = CrmeryHelperView::getView('reports','roi_report_footer');

                    break;

                    case "notes":

                        $model = JModel::getInstance('note','CrmeryModel');

                        $model->set('_id',$items);

                        $note_entries = $model->getNotes(NULL,NULL,FALSE);

                        $state = $model->getState();

                        $header = CrmeryHelperView::getView('reports','notes_header',array(array('ref'=>'state','data'=>$state),array('ref'=>'note_entries','data'=>$note_entries)));

                        $table = CrmeryHelperView::getView('reports','notes_filter',array(array('ref'=>'note_entries','data'=>$note_entries)));

                        $footer = CrmeryHelperView::getView('reports','notes_footer');

                    break;

                    case "custom_report":

                        $model =& JModel::getInstance('report','CrmeryModel');

                        $report = $model->getCustomReports(JRequest::getVar('custom_report'));

                        $report_data = $model->getCustomReportData(JRequest::getVar('custom_report'));

                        $state = $model->getState();

                        $data = array(

                                array('ref'=>'report_data','data'=>$report_data),

                                array('ref'=>'report','data'=>$report),

                                array('ref'=>'state','data'=>$state)

                            );

                        $header = CrmeryHelperView::getView('reports','custom_report_header',$data);

                        $table = CrmeryHelperView::getView('reports','custom_report_filter',$data);

                        $footer = CrmeryHelperView::getView('reports','custom_report_footer');

                    break;

                }

                $this->assignRef('header',$header);

                $this->assignRef('table',$table);

                $this->assignRef('footer',$footer);

            break;

            default:

                /* Item info */

                $function = "get".ucwords($func);
				
                $item_id = ( JRequest::getVar('item_id') ) ? JRequest::getVar('item_id') : NULL;

                $ids = ( JRequest::getVar('ids') ) ? JRequest::getVar('ids') : NULL;

                

                $item_id = $item_id ? $item_id : $ids;



                if ( is_array($item_id) ){

                    $items = array();

                    if ( JRequest::getVar('item_id') ){

                        foreach ( $item_id as $key => $item ){

                            $items[] = $key;

                        }

                    }else{

                        foreach ( $item_id as $key => $item ){

                            $items[] = $item;

                        }

                    }

                }else{

                    $items = $item_id;

                }



                $model->set('_id',$items);

                $model->set("completed",null);

                $info = $model->$function($params);
				
				//echo "<pre>"; print_r($function); die;



                 /* Event list */

                $model = & JModel::getInstance('event','CrmeryModel');

                $events = $model->getEvents("deal",NULL,$item_id);

                if (count($events)>0){

                    $ref = array( array('ref'=>'events','data'=>$events),array('ref'=>'print','data'=>TRUE) );

                    $eventDock = CrmeryHelperView::getView('events','event_dock',$ref);

                    $this->assignRef('event_dock',$eventDock);

                }



                /* Contact info */

                if(is_array($info) && array_key_exists(0,$info) && array_key_exists('people',$info[0]) && count($info[0]['people'])>0){

                    $contactModel =& JModel::getInstance('Contacts','CrmeryModel');

                    $contactModel->set('deal_id',$info[0]['id']);

                    $contacts = $contactModel->getContacts();

                    $ref = array(array('ref'=>'contacts','data'=>$contacts),array('ref'=>'print','data'=>TRUE));

                    $contact_info = CrmeryHelperView::getView('contacts','default',$ref);

                    $this->assignRef('contact_info',$contact_info);

                }





                $this->assignRef('info',$info); 

            break;

        }



        $custom_fields = array('deal','company','person');

        if ( in_array($layout,$custom_fields) ){

            switch($layout){

                case "company":

                    $type = "company";

                break;

                case "deal":

                    $type = "deal";

                break;

                case "person":

                    $type = "people";

                break;

            }

            $this->custom_fields = CrmeryHelperView::getView('print','custom_fields');

            $this->custom_fields->assignRef('item_type',$type);

            $this->custom_fields->assignRef('item',$info[0]);

        }





        $js = "jQuery(document).ready(function(){

                window.print();

        })";



        $document->addScriptDeclaration($js);



        //display

		parent::display($tpl);

	}

	

}

?>

