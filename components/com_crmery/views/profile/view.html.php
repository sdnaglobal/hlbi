<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewProfile extends JView
{
	function display($tpl = null)
	{
	    //javascript
	    $document =& JFactory::getDocument();
        $document->addScript( JURI::base().'components/com_crmery/media/js/profile_manager.js' );
        
        //get user data and pass to view
        $user = CrmeryHelperUsers::getLoggedInUser();
        $user_id = CrmeryHelperUsers::getUserId();
        $this->assignRef('user',$user);
        $this->assignRef('user_id',$user_id);
        
        //display
		parent::display($tpl);
	}
	
}
?>
