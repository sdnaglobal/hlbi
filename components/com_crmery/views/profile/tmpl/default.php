<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<script type="text/javascript">
    var user_id = <?php echo $this->user_id; ?>;
</script>
<h1><?php echo CRMText::_('COM_CRMERY_PROFILE'); ?></h1>
<div id="profile">
    <ul>
        <li><a class="profile_slide_link" id="profile_personal"><?php echo CRMText::_('COM_CRMERY_EDIT_PERSONAL_INFO'); ?></a></li>
        <div class="profile_slide" id="profile_personal_slide">
            <form>
                <fieldset>
                    <ul>
                        <li><label><?php echo CRMText::_('COM_CRMERY_PERSON_FIRST'); ?></label><input class="inputbox" type="text" name="first_name" value="<?php echo $this->user->first_name; ?>"></li>
                        <li><label><?php echo CRMText::_('COM_CRMERY_PERSON_LAST'); ?></label><input class="inputbox" type="text" name="last_name" value="<?php echo $this->user->last_name; ?>"></li>
                    </ul>
                </fieldset>
            </form>
            <input type="button" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" class="button save" ><input type="button" value="<?php echo CRMText::_('COM_CRMERY_CANCEL'); ?>" class="button cancel">
        </div>
        <li><a class="profile_slide_link" id="profile_date"><?php echo CRMText::_('COM_CRMERY_DATE_TIME_SETTINGS'); ?></a></li>
        <div class="profile_slide" id="profile_date_slide">
            <form> 
                <fieldset>
                    <ul>
                        <li><label><?php echo CRMText::_('COM_CRMERY_TIMEZONE'); ?></label>
                            <select class="inputbox" name="time_zone">
                                <?php
                                    $timezone_abbreviations = CrmeryHelperDate::getTimezones();
                                    echo JHtml::_('select.options', $timezone_abbreviations, 'value', 'text', $this->user->time_zone, true);
                                ?>
                            </select>    
                        </li>
                        <li>
                        </li>
                        <li><label><?php echo CRMText::_('COM_CRMERY_DATE_FORMAT'); ?></label>
                            <select class="inputbox" name="date_format">
                                <?php 
                                    $date_formats = CrmeryHelperDate::getDateFormats();
                                    echo JHtml::_('select.options', $date_formats, 'value', 'text', $this->user->date_format, true);
                                ?>
                            </select>
                        </li>
                        <li><label><?php echo CRMText::_('COM_CRMERY_TIME_FORMAT'); ?></label>
                            <select class="inputbox" name="time_format">
                                <?php 
                                    $time_formats = CrmeryHelperDate::getTimeFormats();
                                    echo JHtml::_('select.options', $time_formats, 'value', 'text', $this->user->time_format, true);
                                ?>
                            </select>
                        </li>
                    </ul>
                </fieldset>
            </form>
            <input type="button" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" class="button save" ><input type="button" value="<?php echo CRMText::_('COM_CRMERY_CANCEL'); ?>" class="button cancel">
        </div>
        <li><a class="profile_slide_link" id="profile_inbox"><?php echo CRMText::_('COM_CRMERY_USER_INBOX_MULTIPLE_EMAILS'); ?></a></li>
        <div class="profile_slide" id="profile_inbox_slide">
            <form>
                <fieldset>
                    <ul id="email_input_boxes">
                        <li><?php echo CRMText::_('COM_CRMERY_MULTIPLE_INBOX_DESC_1'); ?></li>
                        <li><?php echo CRMText::_('COM_CRMERY_MULTIPLE_INBOX_DESC_2'); ?></li>
                        <li><label><?php echo CRMText::_('COM_CRMERY_PRIMARY_EMAIL'); ?></label><input class="inputbox" disabled="disabled" value="<?php echo $this->user->email; ?>" /></li>
                        <?php if ( count($this->user->emails) ){
                            foreach ( $this->user->emails as $key=>$email ){  if ( $email['email'] != $this->user->email ){ ?>
                                 <li><label><?php echo CRMText::_('COM_CRMERY_EMAIL'); ?></label><input class="inputbox" type="text" name="email[]" value="<?php echo $email['email']; ?>"><span class="message"></span></li>
                             <?php } }
                                $remaining = 2 - count($this->user->emails);
                                for ( $i=0; $i<$remaining; $i++ ){ ?>
                                     <li><label><?php echo CRMText::_('COM_CRMERY_EMAIL'); ?></label><input class="inputbox" type="text" name="email[]" value=""><span class="message"></span></li>
                                <?php }
                                }else{ ?>
                                    <li><label><?php echo CRMText::_('COM_CRMERY_EMAIL'); ?></label><input class="inputbox" type="text" name="email[]" value=""><span class="message"></span></li>
                                    <li><label><?php echo CRMText::_('COM_CRMERY_EMAIL'); ?></label><input class="inputbox" type="text" name="email[]" value=""><span class="message"></span></li>
                        <?php } ?>
                    </ul>
                    <a class="add_email_link" onclick="addEmailBox();"><?php echo CRMText::_('COM_CRMERY_ADD_ANOTHER_EMAIL'); ?></a>
                </fieldset>
            </form>
            <input type="button" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" class="button save" ><input type="button" value="<?php echo CRMText::_('COM_CRMERY_CANCEL'); ?>" class="button cancel">
        </div>
        <li><a class="profile_slide_link" id="profile_email"><?php echo CRMText::_('COM_CRMERY_EMAIL_PREF_REMINDERS'); ?></a></li>
        <div class="profile_slide" id="profile_email_slide">
            <form>
                <fieldset>
                    <ul>
                        <?php /**
                        <li>
                            <label class="small"><input type="checkbox" name="daily_agenda" <?php if ( $this->user['daily_agenda'] ) echo 'checked'; ?> ></label>
                            <span class="faux_input"><b><?php echo CRMText::_('COM_CRMERY_DAILY_AGENDA'); ?></b></span>
                            <span class="faux_input_details"><?php echo CRMText::_('COM_CRMERY_DAILY_AGENDA_DESC'); ?></span>
                        </li>
                         **/ ?>
                        <li>
                            <label class="small"><input type="checkbox" name="morning_coffee" <?php if ( $this->user->morning_coffee ) echo 'checked'; ?>></label>
                            <span class="faux_input"><b><?php echo CRMText::_('COM_CRMERY_MORNING_COFFEE'); ?></b></span>
                            <span class="faux_input_details"><?php echo CRMText::_('COM_CRMERY_MORNING_COFFEE_DESC'); ?></span>
                        </li>
                        <?php /**
                        <?php if(CrmeryHelperUsers::getRole()!='basic') { ?>
                        <li>
                            <label class="small"><input type="checkbox" name="weekly_team_report" <?php if ( $this->user['weekly_team_report'] ) echo 'checked'; ?>></label>
                            <span class="faux_input"><b><?php echo CRMText::_('COM_CRMERY_TEAM_USE_REPORT'); ?></b></span>
                            <span class="faux_input_details"><?php echo CRMText::_('COM_CRMERY_TEAM_USE_REPORT_DESC'); ?></span>
                        </li>
                        <?php } ?>
                        <li>
                            <label class="small"><input type="checkbox" name="weekly_personal_report" <?php if ( $this->user['weekly_personal_report'] ) echo 'checked'; ?>></label>
                            <span class="faux_input"><b><?php echo CRMText::_('COM_CRMERY_TEAM_USE_REPORT'); ?></b></span>
                            <span class="faux_input_details"><?php echo CRMText::_('COM_CRMERY_PERSONAL_USE_REPORT_DESC'); ?></span>
                        </li>
                        <?php /* TODO: ADD CRON for 15 Min Email Reminder
                        <li>
                            <label class="small"><input type="checkbox" name="reminder_notifications" <?php if ( $this->user['reminder_notifications'] ) echo 'checked'; ?>></label>
                            <span class="faux_input"><b>Email me 15 minutes before a reminder is due</b></span>
                        </li>
                         */ ?>
                    </ul>
                </fieldset>
            </form>
            <input type="button" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" class="button save" ><input type="button" value="<?php echo CRMText::_('COM_CRMERY_CANCEL'); ?>" class="button cancel">
        </div>
        <?php /*
        <li><a class="profile_slide_link" id="profile_sms">Set SMS Mobile Number</a></li>
        <div class="profile_slide" id="profile_sms_slide">
            <form>
                <fieldset>
                    <ul>
                        <li>Set your mobile number in order to receive SMS reminders 15 minutes before they are due.</li>
                        <li>
                            <label><b>Mobile Number</b></label>
                            <input type="text" name="sms_number" value="<?php if ( $this->user['sms_number'] != null ) echo $this->user['sms_number']; ?>">
                        </li>
                        <li>
                            <label class="small"><input type="checkbox" name="text_messages" <?php if ( $this->user['text_messages'] ) echo 'checked'; ?>/></label>
                            <span class="faux_input">Send me text messages</span>
                        </li>
                    </ul>
                </fieldset>
            </form>
            <input type="button" value="Save" class="save" ><input type="button" value="Cancel" class="cancel">
        </div>
         */ ?>
        <li><a class="profile_slide_link" id="profile_charts"><?php echo CRMText::_('COM_CRMERY_SET_HOME_CHART'); ?></a></li>
        <div class="profile_slide" id="profile_charts_slide">
            <form>
                <fieldset>
                    <ul>
                        <li><?php echo CRMText::_('COM_CRMERY_SET_HOME_CHART_DESC'); ?></li>
                        <li>
                            <label>
                                <select class="inputbox" name="home_page_chart">
                                    <?php 
                                        $charts = CrmeryHelperCharts::getDashboardCharts();
                                        echo JHtml::_('select.options', $charts, 'value', 'text', $this->user->home_page_chart, true);
                                    ?>
                                </select>
                            </label>
                        </li>
                    </ul>
                </fieldset>
            </form>
            <input type="button" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" class="button save" ><input type="button" value="<?php echo CRMText::_('COM_CRMERY_CANCEL'); ?>" class="button cancel">
        </div>
        <li><a class="profile_slide_link" id="profile_commission"><?php echo CRMText::_('COM_CRMERY_SET_COMMISSION_RATE'); ?></a></li>
        <div class="profile_slide" id="profile_commission_slide">
            <form>
                <fieldset>
                    <ul>
                        <li><?php echo CRMText::_('COM_CRMERY_SET_COMMISSION_RATE_DESC'); ?></li>
                        <li>
                            <label>
                                <?php echo CRMText::_('COM_CRMERY_COMMISSION_RATE'); ?>
                            </label>
                            <input class="inputbox" type="text" name="commission_rate" value="<?php echo $this->user->commission_rate; ?>">%
                        </li>
                    </ul>
                </fieldset>
            </form>
            <input type="button" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" class="button save" ><input type="button" value="<?php echo CRMText::_('COM_CRMERY_CANCEL'); ?>" class="button cancel">
        </div>
        <li><a class="profile_slide_link" id="documents"><?php echo CRMText::_('COM_CRMERY_DOCUMENT_EMAIL_SETTINGS'); ?></a></li>
        <div class="profile_slide" id="documents_slide">
            <form>
                <fieldset>
                    <ul id="document_input_boxes">
                        <li><?php echo CRMText::_('COM_CRMERY_DOCUMENT_EMAIL_SETTINGS_DESC'); ?></li>
                        <?php if ( count($this->user->document_bypass) && is_array($this->user->document_bypass)){
                            foreach ( $this->user->document_bypass as $key=>$document ){ ?>
                                 <li><label><?php echo CRMText::_('COM_CRMERY_FILENAME'); ?></label><input class="inputbox" type="text" name="document_bypass[]" value="<?php echo $document; ?>"></li>
                             <?php } }else{ ?>
                                <li><label><?php echo CRMText::_('COM_CRMERY_FILENAME'); ?></label><input class="inputbox" type="text" name="document_bypass[]" placeholder="<?php echo CRMText::_('COM_CRMERY_DOCUMENT_EMAIL_BYPASS_NULL'); ?>"></li>
                         <?php } ?>
                    </ul>
                    <a class="add_document_link" onclick="addDocumentBox();"><?php echo CRMText::_('COM_CRMERY_ADD_ANOTHER_FILE'); ?></a>
                </fieldset>
            </form>
            <input type="button" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>" class="button save" ><input type="button" value="<?php echo CRMText::_('COM_CRMERY_CANCEL'); ?>" class="button cancel">
        </div>
    </ul>
</div>