<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<table class="com_crmery_table">
        <thead>
            <th class="checkbox_column"><input type="checkbox" onclick="selectAll(this);" /></th>
            <th><div class="sort_order"><a class="s.name" onclick="sortTable('s.name',this)"><?php echo CRMText::_('COM_CRMERY_REPORTS_SOURCE'); ?></a></div></th>
            <th><div class="sort_order"><a class="count(d.id)" onclick="sortTable('count(d.id)',this)"><?php echo CRMText::_('COM_CRMERY_NUMBER_OF_DEALS'); ?></a></div></th>
            <th><div class="sort_order"><a class="sum(d.amount)" onclick="sortTable('sum(d.amount)',this)"><?php echo JText::sprintf('COM_CRMERY_REVENUE',CrmeryHelperConfig::getConfigValue('currency')); ?></a></div></th>
            <th><div class="sort_order"><a class="s.cost" onclick="sortTable('s.cost',this)"><?php echo JText::sprintf('COM_CRMERY_TOTAL_COSTS',CrmeryHelperconfig::getConfigValue('currency')); ?></a></div></th>
            <th><div class="sort_order"><a class="roi" onclick="sortTable('roi',this)"><?php echo CRMText::_('COM_CRMERY_RETURN_ON_INVESTMENTS'); ?></a></div></th>
        </thead>
        <tbody class="results" id="reports">