<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<h1><?php echo CRMText::_('COM_CRMERY_ROI_REPORT'); ?></h1>
<?php echo $this->menu; ?>
<script type="text/javascript">
    order_url = "<?php echo 'index.php?option=com_crmery&view=reports&layout=roi_report_filter&tmpl=component&format=raw'; ?>";
    order_dir = "<?php echo $this->state->get('Source.filter_order_Dir'); ?>";
    order_col = "<?php echo $this->state->get('Source.filter_order'); ?>";
</script>
<form id="list_form" class="print_form" method="POST" target="_blank" action="<?php echo JRoute::_('index.php?option=com_crmery&view=print'); ?>">
<input type="hidden" name="layout" value="report" />
<input type="hidden" name="model" value="source" />
<input type="hidden" name="report" value="roi_report" />
<span class="actions">
    <a href="javascript:void(0)" onclick="printItems(this)"><?php echo CRMText::_('COM_CRMERY_PRINT'); ?></a>
    <?php if ( CrmeryHelperUsers::canExport() ){?>
    	<a href="javascript:void(0)" onclick="exportCsv()"><?php echo CRMText::_('COM_CRMERY_EXPORT_CSV'); ?></a>
    <?php } ?>
</span>
    <?php echo $this->roi_report_header->display(); ?>
    <?php echo $this->roi_list->display(); ?>
    <?php echo $this->roi_report_footer->display(); ?>
<input type="hidden" name="list_type" value="roi_report" />
</form>