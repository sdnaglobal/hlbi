<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<?php $report = $this->report[0]; ?>
<script type="text/javascript">
    order_url = "<?php echo 'index.php?option=com_crmery&view=reports&layout=custom_report_filter&format=raw&tmpl=component&id='.$report['id']; ?>";
    order_dir = "<?php echo $this->state->get('Report.'.$report['id'].'_'.$this->layout.'_filter_order_Dir'); ?>";
    order_col = "<?php echo $this->state->get('Report.'.$report['id'].'_'.$this->layout.'_filter_order'); ?>";
</script>
<h1><?php echo $report['name']; ?></h1>
<?php echo $this->menu; ?>
<form id="list_form" class="print_form" method="POST" target="_blank" action="<?php echo JRoute::_('index.php?option=com_crmery&view=print'); ?>">
<input type="hidden" id="list_form_layout" name="layout" value="report" />
<input type="hidden" name="model" value="source" />
<input type="hidden" name="report" value="custom_report" />
<input type="hidden" name="custom_report" value="<?php echo $report['id']; ?>" />
<span class="actions">
    <a href="javascript:void(0)" onclick="printItems(this)"><?php echo CRMText::_('COM_CRMERY_PRINT'); ?></a>
    <?php if ( CrmeryHelperUsers::canExport() ){?>
    	<a href="javascript:void(0)" onclick="exportCsv()"><?php echo CRMText::_('COM_CRMERY_EXPORT_CSV'); ?></a>
    <?php } ?>
</span>
<?php echo $this->custom_report_header->display(); ?>
<?php echo $this->custom_report_list->display(); ?>
<?php echo $this->custom_report_footer->display(); ?>
<input type="hidden" name="list_type" value="custom_report" />
<input type="hidden" name="report_id" value="<?php echo $report['id']; ?>" />
</form>