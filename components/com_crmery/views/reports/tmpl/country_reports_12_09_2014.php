<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<!--h1><?php //echo CRMText::_('COM_CRMERY_COUNTRY_REGION_TO_REGION'); ?></h1-->
<br />
<?php 
echo $this->menu;
$countryReferredReceived= $this->countryReferredReceived; 
?>
<!--form id="list_form" class="print_form" method="POST" target="_blank" action="<?php echo JRoute::_('index.php?option=com_crmery&view=print'); ?>">

<!--input type="hidden" name="layout" value="report" />
<input type="hidden" name="model" value="regiontoregion" />
<input type="hidden" name="report" value="country_regiontoregion" /-->

<!--input type="hidden" name="layout" value="report" />
<input type="hidden" name="model" value="report" />
<input type="hidden" name="report" value="countryReport" />

<span class="actions" >
        <a href="javascript:void(0)" onclick="printItems(this);"><?php echo CRMText::_('COM_CRMERY_PRINT'); ?></a>
        
</span>
</form-->

<div class="row"><h3>Report for ALL Countries</h3></div>
<table width="900"> <thead><tr class="odd" ><th width="250">Country</th><th width="250" >Received</th><th width="250">Referred</th> </tr></thead>
<?php 
$total_received_amount = 0;
$total_referred_amount = 0;
$cu = count($countryReferredReceived);
echo "<tfoot>";
foreach($countryReferredReceived as $country)
{ 
	if($cu%2 != 0)
	{
		$cl = "class='column1'";
	}
	else
	{
		$cl = "class='column2'";
	}
?>
	<tr class='odd'>
		<td <?php echo $cl; ?> ><?php echo $country['country_name'] ;?></td>
		<td <?php echo $cl; ?> ><?php echo CRMText::_('COM_CRMERY_CURRENCY').' '. number_format($country['Received_Amount'],2,'.',',');?></td>
		<td <?php echo $cl; ?> ><?php echo CRMText::_('COM_CRMERY_CURRENCY').' '. number_format($country['Referred_Amount'],2,'.',',');?></td>
	</tr>
	<?php
	$total_received_amount = $total_received_amount + $country['Received_Amount'];
	$total_referred_amount = $total_referred_amount + $country['Referred_Amount'];
	$cu++;	
}
?>

<tr>
	<td class="foot"><strong>Total</strong></td><td class="foot"><?php echo CRMText::_('COM_CRMERY_CURRENCY').' '. number_format($total_received_amount,2,'.',',');?></td>
	<td class="foot"><?php echo CRMText::_('COM_CRMERY_CURRENCY').' '. number_format($total_referred_amount,2,'.',',');?></td>  
</tr>
</tfoot>
</table>
