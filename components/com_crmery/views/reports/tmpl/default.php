<?php

/*------------------------------------------------------------------------

# CRMery

# ------------------------------------------------------------------------

# @author CRMery

# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.

# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Website: http://www.crmery.com

-------------------------------------------------------------------------*/

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' ); ?>



<script type="text/javascript">

    var graphData = <?php echo json_encode($this->graph_data); ?>;

</script>



 

<h1 ><?php echo CRMText::_('COM_CRMERY_SALES_DASHBOARD'); ?></h1>

<?php echo $this->menu; ?>

<?php if ( $this->member_role != 'basic' ){ ?>

<div  >

<?php echo CRMText::_("COM_CRMERY_SHOWING_SALES_DASHBOARD_FOR"); ?> 

    <span class="filters"><a class="sales_dashboard_filter dropdown" id="sales_dashboard_filter_link"><?php echo CRMText::_('COM_CRMERY_ME'); ?></a></span>

    <div class="filters" id="sales_dashboard_filter">

        <ul>

            <li><a class="filter_member_<?php echo $this->user_id; ?> dropdown_item" onclick="salesDashboardFilter(<?php echo $this->user_id; ?>,null)"><?php echo CRMText::_('COM_CRMERY_ME'); ?></a></li>

            <?php if ( $this->member_role == 'manager' ){ ?>

                <li><a class="filter_team_<?php echo $this->team_id; ?> dropdown_item" onclick="salesDashboardFilter(null,<?php echo $this->team_id; ?>)"><?php echo CRMText::_('COM_CRMERY_MY_TEAM'); ?></a></li>

            <?php } ?>

            <?php if ( $this->member_role == 'exec' ){ ?>

                <li><a class="filter_company dropdown_item" onclick="salesDashboardFilter(null,null)"><?php echo CRMText::_("COM_CRMERY_THE_COMPANY"); ?></a></li>

            <?php foreach( $this->teams as $title => $text ){ ?>

                <li><a class="filter_team_<?php echo $text['team_id']; ?> dropdown_item" onclick="salesDashboardFilter(null,<?php echo $text['team_id']; ?>)"><?php echo $text['team_name'].CRMText::_('COM_CRMERY_TEAM_APPEND'); ?></a></li>

            <?php }} ?>

            <?php foreach ( $this->users as $title => $text ){

                 echo "<li><a class='filter_member_".$text['id']." dropdown_item' onclick=\"salesDashboardFilter(".$text['id'].",null)\">".$text['first_name'].' '.$text['last_name']."</a></li>";

            }?>

        </ul>

    </div>

<?php } ?>

<?php echo CRMText::_('COM_CRMERY_FROM'); ?> <a href="javascript:void(0);" id="report_graph_date_start_label" onclick="changeGraphDate('report_graph_date_start');"><?php echo CrmeryHelperUsers::getGraphDate('report_graph_date_start',true); ?></a><input type="hidden" name="report_graph_date_start" id="report_graph_date_start" value="" /> <?php echo CRMText::_('COM_CRMERY_TO'); ?> <a href="javascript:void(0);" id="report_graph_date_end_label" onclick="changeGraphDate('report_graph_date_end');"><?php echo CrmeryHelperUsers::getGraphDate('report_graph_date_end',true); ?></a><input type="hidden" name="report_graph_date_end" id="report_graph_date_end" value="" />

<div id="sales_dashboard_graphs">

    <div class="dash_floats_left reports_floats_left">

        <ul class="dash_float_list" id="dash_floats_left">

            <?php if ( isset($this->sales_dash_floats_left) && count($this->sales_dash_floats_left) > 0 ){

                foreach ( $this->sales_dash_floats_left as $float ){

                    $view = CrmeryHelperView::getView('reports',$float);

                    if ( $view && $float != "" ){

                        echo '<li id="'.$float.'">';

                            echo $view->display();

                        echo '</li>';

                    }

                }

            } ?>

        </ul>

    </div>

    <div class="dash_floats_right reports_floats_right">

        <ul class="dash_float_list" id="dash_floats_right">

            <?php if ( isset($this->sales_dash_floats_right) && count($this->sales_dash_floats_right) > 0 ){

                foreach ( $this->sales_dash_floats_right as $float ){

                    $view = CrmeryHelperView::getView('reports',$float);

                    if ( $view && $float != "" ){

                        echo '<li id="'.$float.'">';

                            echo $view->display();

                        echo '</li>';

                    }

                }

            } ?>

        </ul>

    </div>

</div>



 



</div> <!-- display:none; -->