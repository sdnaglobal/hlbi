<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

for($i=0;$i<count($this->deals);$i++){
    $deal = $this->deals[$i]; ?>
    <div class="deal_milestone_report">
        <h2><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=deals&layout=deal&id='.$deal['id']); ?>"><?php echo $deal['name']; ?></a></h2>
        <div class="details">
            <ul>
                <li><?php echo CRMText::_('COM_CRMERY_AMOUNT').": ".CrmeryHelperConfig::getCurrency().CrmeryHelperUsers::formatAmount($deal['amount']); ?></li>
                <li><?php echo CRMText::_('COM_CRMERY_DEAL_STAGE').": ".$deal['stage_name']; ?></li>
                <li><?php echo CRMText::_('COM_CRMERY_OWNER').": ".$deal['owner_first_name']." ".$deal['owner_last_name']; ?></li>
            </ul>
        </div>
        <div class="events">
        <table class="com_crmery_table">
            <thead>
                <th class="logo" ><?php echo CRMText::_('COM_CRMERY_MILESTONE_NAME'); ?></th>
                <th class="logo" ><?php echo CRMText::_('COM_CRMERY_DUE_DATE'); ?></th>
                <th class="logo" ><?php echo CRMText::_('COM_CRMERY_COMPLETED'); ?></th>
            </thead>
            <tbody id="reports">
                <?php
                    if ( is_array($deal['events']) ){ foreach ($deal['events'] as $key=>$event){ ?>
                        <tr>
                            <td>
                                <?php
                                if ( $event['completed'] == 1 ){
                                    $completed = "line-through";
                                }else{
                                    $completed = "";
                                }
                                ?>
                                <a href="javascript:void(0);" class="dropdown <?php echo $completed; ?>" id="event_menu_<?php echo $event['id']; ?>_link"><?php echo $event['name']; ?></a>
                                <?php
                                echo '<div id="event_form_'.$event['id'].'">';
                                        echo '<input type="hidden" name="event_id" value="'.$event['id'].'" />';
                                        echo '<input type="hidden" name="parent_id" value="'.$event['parent_id'].'" />';
                                          if ( $event['type'] == "task" ){
                                            echo '<input type="hidden" name="due_date" value="'.$event['due_date'].'" />';
                                          }else{
                                            echo '<input type="hidden" name="start_time" value="'.$event['start_time'].'" />';
                                            echo '<input type="hidden" name="end_time" value="'.$event['end_time'].'" />';
                                          }
                                        echo '<input type="hidden" name="event_type" value="'.$event['type'].'" />';
                                        echo '<input type="hidden" name="repeats" value="'.$event['repeats'].'" />';
                                        echo '<input type="hidden" name="type" value="single" />';
                               echo '</div>';
                                echo '<div class="filters" id="event_menu_'.$event['id'].'">';
                                  echo '<ul>';
                                   if ( $event['completed'] == 1 ){
                                    echo '<li><a href="javascript:void(0);" onclick="markEventIncomplete(this)" >'.CRMText::_('COM_CRMERY_MARK_INCOMPLETE').'</a></li>';
                                   }else{
                                    echo '<li><a href="javascript:void(0);" onclick="markEventComplete(this)" >'.CRMText::_('COM_CRMERY_MARK_COMPLETE').'</a></li>';
                                    echo '<li><a href="javascript:void(0);" onclick="postponeEvent(this,1)" >'.CRMText::_('COM_CRMERY_POSTPONE_1_DAY').'</a></li>';
                                    echo '<li><a href="javascript:void(0);" onclick="postponeEvent(this,7)" >'.CRMText::_('COM_CRMERY_POSTPONE_7_DAYS').'</a></li>';
                                  }
                                    $id = ( array_key_exists('parent_id',$event) && $event['parent_id'] ) != 0 ? $event['parent_id'] : $event['id'];
                                    echo '<li><a href="javascript:void(0);" onclick="editEvent('.$id.',\''.$event['type'].'\')" >'.CRMText::_('COM_CRMERY_EDIT').'</a></li>';
                                    echo '<li><a href="javascript:void(0);" onclick="deleteEvent(this)" >'.CRMText::_('COM_CRMERY_DELETE').'</a></li>';
                                  echo '</ul>';
                                echo '</div>';
                                ?>
                            </td>
                            <td><?php echo CrmeryHelperDate::formatDate($event['due_date']); ?></td>
                            <td><?php echo CrmeryHelperDate::formatDate($event['actual_close']); ?></td>
                        </tr>
                <?php }} ?>
            </tbody>
        </table>
        </div>
    </div>
<?php }
?>