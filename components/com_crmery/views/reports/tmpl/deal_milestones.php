<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<script type="text/javascript">
    var order_url = "<?php echo 'index.php?option=com_crmery&view=reports&layout=deal_milestones_filter&format=raw'; ?>";
</script>
<h1><?php echo ucwords(CRMText::_('COM_CRMERY_DEAL_MILESTONE_REPORT')); ?></h1>
<?php echo $this->menu; ?>
<table class="com_crmery_table">
    <thead>
        <tr>
            <th><?php echo CRMText::_('COM_CRMERY_DEAL_NAME'); ?></th>
            <th><?php echo CRMText::_('COM_CRMERY_OWNER'); ?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <?php $deal_filter = $this->state->get('Deal.'.str_replace('_filter','',JRequest::getVar('layout')).'_name'); ?>
            <td><input class="inputbox filter_input" name="deal_name" type="text" value="<?php echo $deal_filter; ?>"></td>
            <td>
                <select class="inputbox filter_input" name="owner_id">
                    <?php if ( CrmeryHelperUsers::getRole() != "basic" ) { ?>
                        <option value=""><?php echo CRMText::_('COM_CRMERY_ALL'); ?></option>
                    <?php } ?>
                    <?php if ( CrmeryHelperUsers::getRole() != "basic" ) { ?>
                        <?php $user_filter = $this->state->get('Deal.'.str_replace('_filter','',JRequest::getVar('layout')).'_owner_id'); ?>
                        <?php $team_filter = $this->state->get('Deal.'.str_replace('_filter','',JRequest::getVar('layout')).'_owner_id');; ?>
                        <optgroup label="<?php echo CRMText::_('COM_CRMERY_TEAM'); ?>" class="team">
                            <?php echo JHtml::_('select.options', $this->team_names, 'value', 'text', $user_filter, true); ?>
                        </optgroup>
                        <optgroup label="<?php echo CRMText::_('COM_CRMERY_MEMBERS'); ?>" class="member">
                            <option value="<?php echo CrmeryHelperUsers::getUserId(); ?>"><?php echo CRMText::_('COM_CRMERY_ME'); ?></option>
                            <?php echo JHtml::_('select.options', $this->user_names, 'value', 'text', $team_filter, true); ?>
                        </optgroup>
                    <?php } ?>
                </select>
            </td>
        </tr>
    </tbody>
</table>
<div class="results">
    <?php echo $this->deal_milestone_list->display(); ?>
</div>
<form method="post" action="<?php echo JRoute::_('index.php?option=com_crmery&view=reports&layout=deal_milestones'); ?>">
    <table class="com_crmery_table">
        <tfoot>
        <tr>
           <td colspan="14"><?php echo $this->pagination->getListFooter(); ?></td>
        </tr>
     </tfoot>
    </table>
</form>