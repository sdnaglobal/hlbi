<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<h1><?php echo CRMText::_('COM_CRMERY_REFERRED_COUNTRY_REGION'); ?></h1>
<?php echo $this->menu; ?>
<script type="text/javascript">
    // add to view.html.php
    order_url = "<?php echo 'index.php?option=com_crmery&view=reports&layout=sales_pipeline_filter&tmpl=component&format=raw'; ?>";
    order_dir = "<?php echo $this->state->get('Deal.sales_pipeline_filter_order_Dir'); ?>";
    order_col = "<?php echo $this->state->get('Deal.sales_pipeline_filter_order'); ?>";
</script>

<form id="list_form" class="print_form" method="POST" target="_blank" action="<?php echo JRoute::_('index.php?option=com_crmery&view=print'); ?>">
<input type="hidden" name="layout" value="report" />
<input type="hidden" name="model" value="countryregion" />
<input type="hidden" name="report" value="referred_country_region" />
<span class="actions">
        <a href="javascript:void(0)" onclick="printItems(this);"><?php echo CRMText::_('COM_CRMERY_PRINT'); ?></a>
        <?php if ( CrmeryHelperUsers::canExport() ){?>
	    	<a href="javascript:void(0)" onclick="exportCsv()"><?php echo CRMText::_('COM_CRMERY_EXPORT_CSV'); ?></a>
    	<?php } ?>
</span>
<?php echo $this->sales_pipeline_header->display(); ?>
<?php echo $this->sales_pipeline_list->display(); ?>
<?php echo $this->sales_pipeline_footer->display(); ?>
<input type="hidden" name="list_type" value="sales_pipeline" />
</form>