<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<h1><?php echo CRMText::_('COM_CRMERY_COUNTRY_REGION_TO_REGION'); ?></h1>
<?php echo $this->menu; ?>
<script type="text/javascript">
    // add to view.html.php
    order_url = "<?php //echo 'index.php?option=com_crmery&view=reports&layout=sales_pipeline_filter&tmpl=component&format=raw'; ?>";
    order_dir = "<?php //echo $this->state->get('Deal.sales_pipeline_filter_order_Dir'); ?>";
    order_col = "<?php //echo $this->state->get('Deal.sales_pipeline_filter_order'); ?>";
</script>

<form id="list_form" class="print_form" method="POST" target="_blank" action="<?php echo JRoute::_('index.php?option=com_crmery&view=print'); ?>">
<input type="hidden" name="layout" value="report" />
<input type="hidden" name="model" value="regiontoregion" />
<input type="hidden" name="report" value="country_regiontoregion" />
<span class="actions" style="display:none;">
        <a href="javascript:void(0)" onclick="printItems(this);"><?php echo CRMText::_('COM_CRMERY_PRINT'); ?></a>
        <?php if ( CrmeryHelperUsers::canExport() ){?>
	    	<a href="javascript:void(0)" onclick="exportCsv()"><?php echo CRMText::_('COM_CRMERY_EXPORT_CSV'); ?></a>
    	<?php } ?>
</span>
<?php //echo $this->sales_pipeline_header->display(); ?>
<?php //echo $this->sales_pipeline_list->display(); ?>
<?php //echo $this->sales_pipeline_footer->display(); ?>
<input type="hidden" name="list_type" value="sales_pipeline" />
</form>

<br>
<?php 

$member_role = CrmeryHelperUsers::getRole();
if($member_role == '') {

	$db =& JFactory::getDBO();
	$region = $db->getQuery(true);
	$region->select('*');
	$region->from('#__crmery_region');
	$db->setQuery($region);
	$region_rs = $db->loadObjectList();
	echo '<ul>';
	foreach($region_rs as $reg)
	{ 
	 echo '<li style="float:left;margin-right:10px;list-style:none;"><a href="'.JRoute::_('index.php?option=com_crmery&view=reports&layout=region_based_report').'?getregion='.$reg->region_id.'" >'.$reg->region_name.'</a></li>';
	}
	echo '</ul>';
	

if(isset($_REQUEST['getregion']))
{
	$reg_id = $_REQUEST['getregion'];
	
	$db =& JFactory::getDBO();
	$regionname = $db->getQuery(true);
	$regionname->select('*');
	$regionname->from('#__crmery_region');
	$regionname->where("region_id='$reg_id'");
	$db->setQuery($regionname);
	$region_rs_name = $db->loadObjectList();
	$region_name = $region_rs_name[0]->region_name;
	
?>

<br />
<br />
Report for "<?php echo $region_name?>" Region
<br/>
<br/>

<table width="700"> <tr><th>Firm</th><th>Local</th><th>Sterling</th> </tr>
<tr height="20"><td colspan=""5></td></tr>
<?php 


	$Total_sum = 0;
	$Grand_total_gbp = 0;


	$db =& JFactory::getDBO();
	$region = $db->getQuery(true);
	$region->select('*');
	$region->from('#__crmery_currencies');
	$region->where("region_id='$reg_id'");
	$db->setQuery($region);
	$region_rs = $db->loadObjectList();

	foreach($region_rs as $reg)
	{ 

		$total_gbp = 0;
		$sum = 0;
				
		echo '<tr>';
		echo '<td>'.$reg->country_name.' </td>';
		
		$country_id = $reg->country_id;
		
		$cmpny = $db->getQuery(true);
		$cmpny->select('country_id, id as company_id');
		$cmpny->from('#__crmery_companies as cmp');
		$cmpny->where('cmp.country_id='.$country_id.' and cmp.published>0');
		$db->setQuery($cmpny);

		$cmpids = $db->loadObjectList();
		
		if($cmpids)
		{  
			$cmpidArr = array();
			foreach($cmpids as $cmpid)
			{
				$cmpidArr[] = $cmpid->company_id;
			}
			
			$cmpidstr = implode(',',$cmpidArr);
			
			$referal = $db->getQuery(true);
			$referal->select('id');
			$referal->from('#__crmery_people');
			$referal->where('company_id IN('.$cmpidstr.') and published>0');
			$db->setQuery($referal);
			//echo $referal;
			$referalids = $db->loadObjectList();
			if($referalids)
			{
				$referalidArr = array();
			
				foreach($referalids as $referalid)
				{
					//$referalidArr[] = $referalid->id;
					
					$billing = $db->getQuery(true);
					$billing->select('LocalAmount,LocalCurrencyCode');
					$billing->from('#__crmery_referral_payments');
					$billing->where('FeePaid=1 and referral_ID='.$referalid->id);
					$db->setQuery($billing);
					//echo '<br>$billing->'.$billing;
					$billingdetails = $db->loadObjectList();
					
					foreach($billingdetails as $bill)
					{ 	
						$sum = ($sum+$bill->LocalAmount);
						//echo '<br>sum-->'.$sum = ($bill->sum);
						//echo 'refid-->'.$referalid->id;
						$currencycode = $bill->LocalCurrencyCode;
					
						$ccode = $db->getQuery(true);
						$ccode->select('value_in_GBP');
						$ccode->from('#__crmery_currencies');
						$ccode->where("currency_code='$currencycode'");
						$db->setQuery($ccode);
						//echo '<br>'.$ccode;
						$ccodevalue = $db->loadObjectList();
						
						$ccode_value = $ccodevalue[0];
						$gbp = $ccode_value->value_in_GBP;
						$bill->LocalAmount;
						$gbp_price = ($gbp*$bill->LocalAmount); 
						
						//echo '<br>gbp->'.$gbp = $ccode_value->value_in_GBP;
						//echo '<br>local->'.$bill->LocalAmount;
						//echo '<br>multiply->'.$gbp_price = ($gbp*$bill->LocalAmount); 					
						
						$total_gbp = ($total_gbp+$gbp_price); 
					}
				}	
			
			}	
			
		}

		$Total_sum = ($sum+$Total_sum);
		$Grand_total_gbp = ($total_gbp+$Grand_total_gbp);
		
		echo '<td>'.$sum.' </td>';
		echo '<td>'.$total_gbp.' </td>';
		echo '</tr>';

		//break;
	}
		echo '<tr height="20"><td colspan=""5></td></tr>';
		echo '<tr><td><strong>Total</strong></td> <td>'.$Total_sum.' </td> <td>'.$Grand_total_gbp.' </td>  </tr>';
		echo '</table>';


	} // else close of !isset(getregion)

} // end if $member_role == ''		
?>

<!-- 
<table style="margin-top:40px;" class="com_crmery_table">
<tr>
                                     <th class="checkbox_column"> </th>
                                 <th><div class="sort_order">Firm Name</div></th>
                <th><div class="sort_order">Amount Billed</div></th>
                <th></th>
                <th><div class="sort_order"> Sterling </div></th>
                <th><div class="sort_order"> </div></th>
                <th><div class="sort_order"><a onclick="sortTable('stage.percent',this)" class="stage.percent"></a></div></th>
                <th><div class="sort_order"><a onclick="sortTable('d.status_id',this)" class="d.status_id"></a></div></th>
                <th><div class="sort_order"><a onclick="sortTable('d.expected_close',this)" class="d.expected_close"></a></div></th>
                <th><div class="sort_order"><a onclick="sortTable('d.modified',this)" class="d.modified"></a></div></th>
                <th><div class="sort_order"><a onclick="sortTable('d.created',this)" class="d.created"></a></div></th>
            </tr>
			
      <tbody id="reports" class="results"> 
		 <tr class="crmery_row_0">
			 <td>France</td>
			 <td></td>
			 <td></td>
			 <td>0</td>
			 <td></td>
			 <td></td>
			 <td><div style="background-color:# !important;" class="status-dot"></div></td> 
			 <td></td>
			 <td></td>
			 <td></td>
		</tr> 
	 </tbody>
</table>	
 
SELECT u.region_id, cmp.id as company_id FROM luqnj_crmery_users as u, luqnj_crmery_companies as cmp where u.id=5 and cmp.region_id IN(u.region_id)

SELECT u.region_id, cmp.id as company_id, deals.amount as dealamount FROM luqnj_crmery_users as u, luqnj_crmery_companies as cmp, luqnj_crmery_deals as deals where u.id=3 and cmp.region_id IN(u.region_id) and deals.company_id IN(cmp.id) and deals.published>0



SELECT u.region_id, cmp.id as company_id, ref.id as refid  FROM luqnj_crmery_users as u, luqnj_crmery_companies as cmp, luqnj_crmery_people as ref  where u.id=5 and cmp.region_id IN(u.region_id) and ref.company_id IN(cmp.id) and ref.published > 0


SELECT cmp.region_id, cmp.id as company_id  FROM  luqnj_crmery_companies as cmp WHERE cmp.region_id IN(SELECT region_id FROM luqnj_crmery_users WHERE id=5 ) and cmp.published>0

-->


