<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

//assign event
$event = $this->event;
?>
<div id="edit_form">	
	<div class="crmeryRow">
		<div class="crmeryField"><?php echo ucwords(CRMText::_('COM_CRMERY_EDIT_TASK_NAME')); ?><span class="required">*</span></div>
		<div class="crmeryValue"><?php if(array_key_exists('name',$event)) echo $event['name']; ?></div>
	</div>
	<div class="crmeryRow">
		<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_TASK_DUE_DATE'); ?></div>
		<div class="crmeryValue">
           	<?php if (array_key_exists('due_date',$event)) echo $event['due_date_formatted']; ?>                                   
            <?php if ( array_key_exists('due_date_hour',$event) ){ echo $event['due_date_hour']; } ?>
		</div>
	</div>
	<?php if ( strtotime($event['end_date']) > 0 ){ ?>
    <div class="crmeryRow">
		<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_END_DATE'); ?></div>
		<div class="crmeryValue">
		 	<?php if (array_key_exists('end_date',$event)) echo $event['end_date_formatted']; ?>
		</div>
	</div>
	<?php } ?>
	<div class="crmeryRow">
		<div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_EDIT_TASK_DESCRIPTION'); ?></div>
		<div class="crmeryValue">
			<?php if(array_key_exists('description',$event)) echo $event['description']; ?>
		</div>
	</div>
	<div class="actions">
		<a href="javascript:void(0);" onclick="closeTaskEvent('task')"><?php echo CRMText::_('COM_CRMERY_CLOSE'); ?></a>
	</div>
</div>