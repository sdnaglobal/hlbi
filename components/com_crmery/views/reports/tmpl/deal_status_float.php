<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="graph">
    <a class="pieIcon" href="javascript:void(0);" onclick="showChart('dealStatusPie');"><span><?php echo CRMText::_("COM_CRMERY_GRAPHS_PIE"); ?></span></a>
    <a class="barIcon" href="javascript:void(0);" onclick="showChart('dealStatusBar');"><span><?php echo CRMText::_("COM_CRMERY_GRAPHS_BAR"); ?></span></a>    
    <div id="deal_status"></div>
</div>