<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );  ?>

<script type="text/javascript">
    var order_url = "<?php echo JRoute::_('index.php?option=com_crmery&view=reports&layout=custom_reports_filter&tmpl=component&format=raw'); ?>";
</script>
<h1><?php echo CRMText::_('COM_CRMERY_CUSTOM_REPORTS'); ?></h1>
<?php echo $this->menu; ?>
<h2><?php echo CRMText::_('COM_CRMERY_SELECT_CUSTOM_COLUMNS'); ?></h2>
<p><?php echo CRMText::_('COM_CRMERY_CUSTOM_COLUMNS_MESSAGE'); ?></p>
<div id="custom_field_columns">
    <ul class="columns">
        <?php $this->columns = (array_key_exists('report',$this)) ? array_diff($this->columns,unserialize($this->report[0]['fields'])) : $this->columns; ?>
        <?php foreach ( $this->columns as $key=>$text ){ ?>
            <li class="data" id="<?php echo $key; ?>" name="<?php echo $text; ?>"><span class="field-label"><?php echo $text; ?></span></li>
        <?php } ?>
    </ul>
</div>
<div id="custom_field_holders">
    <ul class="holders">
        <?php $count = ( array_key_exists('report',$this) ) ? count($this->report) : 0; ?>
        <?php $field_count = 0; ?>
        <?php if ( $count > 0 ) { ?>
            <?php $fields = unserialize($this->report[0]['fields']); ?>
            <?php foreach ( $fields as $id => $text ){ ?>
                <?php $field_count++; ?>
                <?php if ( strstr($id,"calculate_field_") ){ ?>
                <li class="added_data" id="<?php echo $id; ?>" name="<?php echo $text['name']; ?>"><span class="field-label"><?php echo $text['name']; ?></span><div class="remove"><a href="javascript:void(0)" onclick="removeBlock(this)" class="remove"></a></div>
                    <div id="calculate_field_modal_<?php echo str_replace("calculate_field_","",$id); ?>" >
                        <div class="calculate_field_actions">
                            <a id="edit_calculation_fields_link_<?php echo str_replace("calculate_field_","",$id); ?>" href="javascript:void(0);" onclick="editCalculationFields(this);"><?php echo CRMText::_('COM_CRMERY_EDIT'); ?></a>
                            <div id="edit_calculation_fields_modal_<?php echo str_replace("calculate_field_","",$id); ?>" class="calculate_field_modal" style="display:none;">
                                <div class="crmeryRow">
                                    <div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_NAME'); ?></div>
                                    <div class="crmeryValue"><input value="<?php echo $text['name']; ?>" data-field-id="<?php echo str_replace("calculate_field_","",$id); ?>" class="input-medium calculation_field_name" type="text" name="calculation_field_name[]" placeholder="<?php echo CRMText::_('COM_CRMERY_NAME_YOUR_FIELD'); ?>" /></div>
                                </div>
                                <p><div class="center btn-group"><a class="button btn btn-mini" onclick="addField(this);"><?php echo CRMText::_('COM_CRMERY_ADD_FIELD'); ?></a>
                                <a class="button btn btn-mini" onclick="addOperator(this);"><?php echo CRMText::_('COM_CRMERY_ADD_OPERATOR'); ?></a>
                                <a class="button btn btn-mini" onclick="addInput(this);"><?php echo CRMText::_('COM_CRMERY_ADD_INPUT'); ?></a>
                                <a class="button btn btn-mini" onclick="editFields(this);"><?php echo CRMText::_('COM_CRMERY_EDIT'); ?></a></div></p>
                                <div class='center dropdown-operations'>
                                    <ul class="sortable dropdown-operations-list">
                                    <?php $ops = CrmeryHelperCrmery::splitExpr($text['operation']); ?>
                                    <?php foreach ( $ops['expression_details'] as $exp ){ ?>
                                    <li><span class="hand"></span><?php
                                        switch ( $exp['type'] ){
                                            case "field":
                                                echo CrmeryHelperDropdown::getCustomCalculationFields($exp['value']);
                                            break;
                                            case "operation":
                                                echo CrmeryHelperDropdown::getCustomOperationFields($exp['value']);
                                            break;
                                            case "parenthesis":
                                            case "input":
                                                ?><input type="text" name="calculation_field_input" class="inputbox calculation-field-input-small" value="<?php echo $exp['value']; ?>" /><?php
                                            break;
                                        } ?>
                                    <a class="delete" onclick="deleteField(this);"></a></li>
                                    <?php } ?>
                                    </ul>
                                </div>
                                <div class="actions">
                                    <a class="button btn" id="hide_calculation_fields_link_<?php echo str_replace("calculate_field_","",$id); ?>" href="javascript:void(0);" onclick="hideCalculationFields(this);"><?php echo CRMText::_('COM_CRMERY_DONE_EDITING'); ?></a> -
                                    <a class="remove_calculation_fields_link" href="javascript:void(0);" onclick="removeCalculationField(this);"><?php echo CRMText::_('COM_CRMERY_REMOVE'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    </li>
                <?php } else { ?>
                    <li class="added_data" id="<?php echo $id; ?>" name="<?php echo $text; ?>"><?php echo $text; ?><div class="remove"><a href="javascript:void(0)" onclick="removeBlock(this)" class="remove"></a></div></li>
                <?php } ?>
            <?php } ?>
        <?php } ?>
        <?php for ( $i=0; $i<10-$field_count; $i++ ){ ?>
            <li class="holder"><?php echo CRMText::_('COM_CRMERY_DRAG_FIELD_HERE'); ?></li>
        <?php } ?>
    </ul>
</div>
<form action="<?php echo JRoute::_('index.php?option=com_crmery&controller=reports&task=edit'); ?>" onsubmit="return validateCustomForm(this);" method="post">
    <div class="custom_report_inputs">
        <p><?php echo CRMText::_('COM_CRMERY_NAME_REPORT'); ?>:<p><input class="inputbox required" type="text" name="name" value="<?php if ( array_key_exists('report',$this) ) echo $this->report[0]['name']; ?>">
    </div>
    <div class="actions_center">
        <input class="button btn" type="submit" value="<?php echo CRMText::_('COM_CRMERY_SAVE'); ?>"> - <?php echo CRMText::_('COM_CRMERY_OR'); ?> - <a onclick="window.history.back()"><?php echo CRMText::_('COM_CRMERY_CANCEL_BUTTON'); ?></a></p>
    </div>
    <div id="post">
        <?php if ( array_key_exists('report',$this) ){ ?>
            <input type="hidden" name="id" value="<?php echo $this->report[0]['id']; ?>">
        <?php } ?>
    </div>
</form>

<div id="calculate_field_modal" style="display:none;">
    <div class="calculate_field_actions">
        <a class="edit_calculation_fields_link" href="javascript:void(0);" onclick="editCalculationFields(this);"><?php echo CRMText::_('COM_CRMERY_EDIT'); ?></a>
        <div class="calculate_field_modal" style="display:none;">
            <div class="crmeryRow">
                <div class="crmeryField"><?php echo CRMText::_('COM_CRMERY_NAME'); ?></div>
                <div class="crmeryValue"><input class="inputbox input-medium calculation_field_name" type="text" name="calculation_field_name[]" placeholder="<?php echo CRMText::_('COM_CRMERY_NAME_YOUR_FIELD'); ?>" /></div>
            </div>
            <div class="center btn-group"><a class="button btn btn-mini" onclick="addField(this);"><?php echo CRMText::_('COM_CRMERY_ADD_FIELD'); ?></a>
            <a class="button btn btn-mini" onclick="addOperator(this);"><?php echo CRMText::_('COM_CRMERY_ADD_OPERATOR'); ?></a>
            <a class="button btn btn-mini" onclick="addInput(this);"><?php echo CRMText::_('COM_CRMERY_ADD_INPUT'); ?></a>
            <a class="button btn btn-mini" onclick="editFields(this);"><?php echo CRMText::_('COM_CRMERY_EDIT'); ?></a></div>
            <div class='center dropdown-operations'><ul class="dropdown-operations-list sortable"></ul></div>
            <div class="actions">
                <a class="button btn hide_calculation_fields_link" href="javascript:void(0);" onclick="hideCalculationFields(this);"><?php echo CRMText::_('COM_CRMERY_DONE_EDITING'); ?></a> - 
                <a class="remove_calculation_fields_link" href="javascript:void(0);" onclick="removeCalculationField(this);"><?php echo CRMText::_('COM_CRMERY_REMOVE'); ?></a>
            </div>
        </div>
    </div>
</div>
<div style="display:none">
 <div style="display:none;" id="dropdown-template-field">
    <li><span class="hand"></span>
    <?php echo CrmeryHelperDropdown::getCustomCalculationFields(); ?>
    <a class="delete" onclick="deleteField(this);"></a></li>
</div>
<div style="display:none;" id="dropdown-template-operator">
    <li><span class="hand"></span>
    <?php echo CrmeryHelperDropdown::getCustomOperationFields(); ?>
    <a class="delete" onclick="deleteField(this);"></a></li>
</div>
<div style="display:none;" id="dropdown-template-input">
    <li><span class="hand"></span>
    <input type="text" name="calculation_field_input" class="inputbox calculation-field-input-small" />
    <a class="delete" onclick="deleteField(this);"></a></li>
</div>
</div>