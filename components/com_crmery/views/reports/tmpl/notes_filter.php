 <?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

  for ( $i=0; $i<count($this->note_entries); $i++ ){
      $note = $this->note_entries[$i]; 
      $k = $i%2; ?>
     <tr class="crmery_row_<?php echo $k; ?>">
        <?php if ( JRequest::getVar('view') != "print" ){ ?>
         <td><input type="checkbox" name="ids[]" value="<?php echo $note['id']; ?>" /></td>
         <?php } ?>
         <td>
             <?php 
                if ( $note['company_id'] != null AND $note['company_id'] != 0 ) {
                    echo '<a href="'.JRoute::_('index.php?option=com_crmery&view=companies&layout=company&id='.$note['company_id']).'">'.$note['company_name'].'</a>';
                } 
             ?>
         </td>
         <td>
             <?php 
                if ( $note['deal_id'] != null AND $note['deal_id'] != 0 ) {
                    echo '<a href="'.JRoute::_('index.php?option=com_crmery&view=deals&layout=deal&id='.$note['deal_id']).'">'.$note['deal_name'].'</a>';
                } 
             ?>
         </td>
         <td>
             <?php 
                if ( $note['person_id'] != null AND $note['person_id'] != 0 ) {
                    echo '<a href="'.JRoute::_('index.php?option=com_crmery&view=people&layout=person&id='.$note['person_id']).'">'.$note['person_first_name'].' '.$note['person_last_name'].'</a>';
                } 
             ?>
         </td>
         <td><?php echo $note['owner_first_name']." ".$note['owner_last_name']; ?></td>
         <td><?php echo CrmeryHelperDate::formatDate(($note['created'])); ?></td>
         <td><?php echo $note['category_name']; ?></td>
         <td>
            <?php if ( array_key_exists("name",$note) && $note['name'] != "" ){
                    echo "<b>".$note['name'].'</b><br />';
                }
            ?>
            <?php echo nl2br($note['note']); ?>
        </td>
     </tr> 
<?php }  ?>