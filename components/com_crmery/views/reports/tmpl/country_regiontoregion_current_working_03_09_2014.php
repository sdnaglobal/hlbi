<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<h1><?php echo CRMText::_('COM_CRMERY_COUNTRY_REGION_TO_REGION'); ?></h1>

<?php 
echo $this->menu;
$regionReferredReceived= $this->regionReferredReceived; 
//echo "<pre>"; print_r($regionReferredAmount); die;

?>

<br />
Report for ALL Regions
<br />
<br />
<br />
<table width="400"> <tr><th>Region</th><th>Received</th><th>Referred</th> </tr>
<?php 
$total_received_amount = 0;
$total_referred_amount = 0;
foreach($regionReferredReceived as $region)
{ 
	
	echo "<tr>";
		echo "<td>".$region['Region_Name']." </td>";
		echo "<td>".number_format($region['Received_Amount'],2,'.',',')." </td>";
		echo "<td>". number_format($region['Referred_Amount'],2,'.',',')." </td>";
	echo "</tr>";
	$total_received_amount = $total_received_amount + $region['Received_Amount'];
	$total_referred_amount = $total_referred_amount + $region['Referred_Amount'];
	
}


echo '<tr><td><strong>Total</strong></td><td>'.number_format($total_received_amount,2,'.',',').' </td><td>'.number_format($total_referred_amount,2,'.',',').' </td>  </tr>';
echo '</table>';
	
?>

