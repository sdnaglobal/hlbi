<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<?php 
$customTotals = $_SESSION['customTotals'];
$quoteTotals = $_SESSION['quoteTotals'];
$rows = $_SESSION['rows'];
?>
 </tbody>
 <tfoot>
 	<tr>
		<?php if ( count($rows) > 0 ){ foreach ( $rows as $row ){ ?>
			<td>
				<?php if ( array_key_exists(str_replace("custom_","",$row),$customTotals) )
					echo  $customTotals[str_replace("custom_","",$row)];
					?>
					<?php if ( array_key_exists(str_replace("quote_","",$row),$quoteTotals) ){
						echo CRMText::_('COM_CRMQUOTE_TOTAL').": ".CrmeryHelperConfig::getCurrency().CrmeryHelperUsers::formatAmount($quoteTotals[str_replace("quote_","",$row)]['total']);
						echo '<br />';
						echo CRMText::_('COM_CRMQUOTE_GP_TOTAL').": ".CrmeryHelperConfig::getCurrency().CrmeryHelperUsers::formatAmount($quoteTotals[str_replace("quote_","",$row)]['gp_total']);
					}
					?>
			</td>
		<?php }} ?>
 	</tr>
<tfoot>
</table>
