<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

for ( $i=0; $i<count($this->reports); $i++ ){
    $report = $this->reports[$i]; 
    $k = $i%2; ?>
    <tr id="custom_report_<?php echo $report['id']; ?>" class="crmery_row_<?php echo $k; ?>">
        <td><a href="<?php echo JRoute::_('index.php?option=com_crmery&view=reports&layout=custom_report&id='.$report['id']); ?>"><?php echo $report['name']; ?></a></td>
        <td><?php echo CrmeryHelperDate::formatDate($report['modified']); ?></td>
        <td><?php echo CrmeryHelperDate::formatDate($report['created']); ?></td>
        <td>
            <a href="<?php echo JRoute::_('index.php?option=com_crmery&view=reports&layout=edit_custom_report&id='.$report['id']); ?>"><?php echo CRMText::_('COM_CRMERY_EDIT_BUTTON'); ?></a>
            |
            <a class="delete delete_custom_report"></a>
        </td>
    </tr>
<?php } ?>