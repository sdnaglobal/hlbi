<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<?php $view = JRequest::getVar("view"); 
if ( $view != "print" ){ ?>
<tfoot>
	<tr>
		<td colspan="8"><?php echo $this->pagination->getListFooter(); ?></td>
	</tr>
</foot>
<?php } ?>
</tbody>
</table>