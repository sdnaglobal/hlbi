<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<h1><?php echo CRMText::_('COM_CRMERY_COUNTRY_REGION_TO_REGION'); ?></h1>

<?php 
echo $this->menu;
$countryReferredReceived= $this->countryReferredReceived; 
?>
<form id="list_form" class="print_form" method="POST" target="_blank" action="<?php echo JRoute::_('index.php?option=com_crmery&view=print'); ?>">

<!--input type="hidden" name="layout" value="report" />
<input type="hidden" name="model" value="regiontoregion" />
<input type="hidden" name="report" value="country_regiontoregion" /-->

<input type="hidden" name="layout" value="report" />
<input type="hidden" name="model" value="report" />
<input type="hidden" name="report" value="countryReport" />

<span class="actions" >
        <a href="javascript:void(0)" onclick="printItems(this);"><?php echo CRMText::_('COM_CRMERY_PRINT'); ?></a>
        
</span>
</form>


<br />
Report for ALL Countries
<br />
<br />
<br />
<table width="500"> <tr><th>Country</th><th>Received</th><th>Referred</th> </tr>
<?php 
$total_received_amount = 0;
$total_referred_amount = 0;
foreach($countryReferredReceived as $country)
{ 
	
	echo "<tr>";
		echo "<td>".$country['country_name']." </td>";
		echo "<td>".number_format($country['Received_Amount'],2,'.',',')." </td>";
		echo "<td>".number_format($country['Referred_Amount'],2,'.',',')." </td>";
	echo "</tr>";
	$total_received_amount = $total_received_amount + $country['Received_Amount'];
	$total_referred_amount = $total_referred_amount + $country['Referred_Amount'];

	
}

echo '<tr><td><strong>Total</strong></td><td>'.number_format($total_received_amount,2,'.',',').' </td><td>'.number_format($total_referred_amount,2,'.',',').' </td>  </tr>';
echo '</table>';
	
?>

