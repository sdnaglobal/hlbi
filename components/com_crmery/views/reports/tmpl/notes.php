<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$view = JRequest::getVar('view');
$layout = JRequest::getVar('layout');

defined('_JEXEC') or die('Restricted access'); ?>
<script type="text/javascript">
    order_url = "<?php echo 'index.php?option=com_crmery&view=reports&layout=notes_filter&tmpl=component&format=raw'; ?>";
    order_dir = "<?php echo $this->state->get('Note.'.$view.'.'.$layout.'.filter_order_Dir'); ?>";
    order_col = "<?php echo $this->state->get('Note.'.$view.'.'.$layout.'.filter_order'); ?>";
</script>
<h1><?php echo CRMText::_('COM_CRMERY_NOTES_REPORT'); ?></h1>
<?php echo $this->menu; ?>
<form id="list_form" class="print_form" method="POST" action="index.php?option=com_crmery&view=reports&layout=notes">
<input type="hidden" name="model" value="note" />
<input type="hidden" name="report" value="notes" />
<span class="actions">
        <a href="javascript:void(0)" onclick="printItems(this,'report');"><?php echo CRMText::_('COM_CRMERY_PRINT'); ?></a>
        <?php if ( CrmeryHelperUsers::canExport() ){?>
        <span class="filters"><a href="javascript:void(0)" onclick="exportCsv()"><?php echo CRMText::_('COM_CRMERY_EXPORT_CSV'); ?></a></span>
        <?php } ?>
</span>
<?php echo $this->notes_header->display(); ?>
<?php echo $this->notes_list->display(); ?>
<?php echo $this->notes_footer->display(); ?>
<input type="hidden" name="list_type" value="notes" />
</form>