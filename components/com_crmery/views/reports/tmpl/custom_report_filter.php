<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 $fields = unserialize($this->report[0]['fields']);
 $hasQuote = CrmeryHelperQuote::hasCRMQuote(); ?>
<?php $rows = array(); ?>
    <?php foreach ($fields as $id => $text){ ?>
        <?php $rows[] = $id; ?>
    <?php } ?>
 <?php $_SESSION['rows'] = $rows;
 $customTotals = array();
 $_SESSION['quoteTotals'] = array();
for ( $i=0; $i<count($this->report_data); $i++ ){
    $report = $this->report_data[$i]; 
    $k = $i%2; ?>
   <tr class="crmery_row_<?php echo $k; ?>">
       <?php foreach ( $rows as $row ){ ?>
           <?php $custom_field = strstr($row,"custom_"); ?>
           <?php $calculation_field = strstr($row,"calculate_field_"); ?>
           <?php $isQuoteField = strstr($row,"quote_"); ?>
           <?php if ( $hasQuote && $isQuoteField ){ ?>
              <?php echo CrmeryHelperQuote::getCustomReportRow($row,$report['id']); ?>
              <?php } else if ( $calculation_field ){ ?>
              <td><?php if ( array_key_exists($row,$report) ){ echo $report[$row]; } ?></td>
           <?php } else if ( !$custom_field ){ ?>
           <?php switch ($row){
                  case "summary": ?>
                      <td><?php echo $report['summary']; ?></td>
                  <?php break; 
                  case "status_id": ?>
                      <td><div class="status-dot" style="background-color:#<?php echo $report['status_color']; ?> !important;"></div><?php echo $report['status_name']; ?></td>
                  <?php break;
                  case "modified": ?>
                   <td><?php echo CrmeryHelperDate::formatDate($report['modified']); ?></td>
                  <?php break;
                  case "expected_close": ?>
                      <td><?php echo CrmeryHelperDate::formatDate($report['expected_close']); ?></td>
                  <?php break;
                  case "actual_close": ?>
                      <td><?php echo CrmeryHelperDate::formatDate($report['actual_close']); ?></td>
                  <?php break;
                  case "source_id": ?>
                      <td><?php echo $report['source_name']; ?></td>
                  <?php break;
                  case "created": ?>
                      <td><?php echo CrmeryHelperDate::formatDate($report['created']); ?></td>
                  <?php break;
                  case "primary_contact_name": ?>
                      <td><?php echo $report['primary_contact_first_name'] . ' ' . $report['primary_contact_last_name']; ?></td>
                  <?php break;
                  case "primary_contact_email": ?>
                      <td><?php echo $report['primary_contact_email']; ?></td>
                  <?php break;
                  case "primary_contact_phone"; ?>
                      <td><?php echo $report['primary_contact_phone']; ?></td>
                  <?php break;
                  case "primary_contact_city"; ?>
                      <td></td>
                  <?php break;
                  case "primary_contact_state"; ?>
                      <td></td>
                  <?php break;
                  case "primary_contact_company_name"; ?>
                       <td><?php echo $report['primary_contact_company_name']; ?></td>
                  <?php break;
                  case "owner_id": ?>
                       <td><?php echo $report['first_name'] . ' ' . $report['last_name']; ?></td>
                  <?php break;
                  case "name": ?>
                      <td><?php echo $report['name']; ?></td>    
                  <?php break;
                  case "stage_id" : ?>
                      <td><?php echo $report['stage_name']; ?></td>
                  <?php break;
                  case "amount" : ?>
                      <td><?php echo CrmeryHelperConfig::getCurrency().$report['amount']; ?></td>
                  <?php break;
                  case "probability" ?>
                      <td><?php echo $report['probability']; ?>%</td>
                  <?php break;
                  case "company_name" ?>
                      <td><?php echo $report['company_name']; ?></td>
                  <?php break;
                  case "company_category" ?>
                      <td><?php echo $report['company_category']; ?></td>
                  <?php break; ?>
           <?php } ?>  
           <?php }else{ ?>
                      <td>
                        <?php $custom = CrmeryHelperCustom::getCustomValue("deal",$custom_field,$report[$custom_field],$report['id'],true);
                        $reported = CrmeryHelperCustom::isReported(str_replace("custom_","",$custom_field));
                        if ( $reported )
                        { 
                          if ( array_key_exists(str_replace("custom_","",$custom_field),$customTotals) )
                          {
                            $customTotals[str_replace("custom_","",$custom_field)] += $custom;
                          }else{
                            $customTotals[str_replace("custom_","",$custom_field)] = $custom;
                          }
                        }
                        if ( is_array($custom) ){
                          foreach ( $custom as $value ){
                            echo $value.'<br />';
                          }
                        }else{
                            echo $custom;
                        }?>
                      </td>
           <?php } ?> 
       <?php } ?>
   </tr> 
<?php }  
  $_SESSION['customTotals'] = $customTotals;
?>