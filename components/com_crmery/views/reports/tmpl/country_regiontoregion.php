<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.model');
 ?>

<?php 

$startYear = $this->startYear;
$tillYear = $this->tillYear;
$billYear = $this->billYear;

 ?>
<br />
<div class="countryreports-div">
		<form name="test" method="post" action="index.php?option=com_crmery&layout=country_regiontoregion&view=reports" >
			<label style="font-weight:bold ; font-size:14px;">Select Billing Year </label>
			<select name="chartYear"  style="width:20%;  height:22px; border:1px solid #ccc">
			<?php for($yr = $tillYear;$yr>=$startYear;$yr--){ 
			
			if($yr == $billYear)
			{
				
				$yearSelected = "selected" ;
			}
			else
			{
				$yearSelected = "" ;
			}
			
			?>
				<option <?php echo $yearSelected; ?>  value="<?php echo $yr ; ?>"><?php echo $yr; ?></option>
				
			<?php }?>
			
				
			</select>
		<input type="hidden" name="reportFor" value="country_regiontoregion" />
		<input type="submit" name="Go" value="View Report" />
		</form>
</div>

<?php 

echo $this->menu;






$regionReferredReceived= $this->regionReferredReceived; 
$billYear= $this->billYear; 
//echo "<pre>"; print_r($regionReferredAmount); die;

?>



<div class="row" style="margin-top:40px; margin-bottom:20px;"><h3>Report from regions for <?php echo $billYear; ?> </h3></div>

<table width="900"> <thead><tr class="odd" ><th width="250">Region</th><th width="250" >Received</th><th width="250">Referred</th> </tr></thead>
<?php 
$total_received_amount = 0;
$total_referred_amount = 0;
$cu = count($regionReferredReceived);
echo "<tfoot>";


foreach($regionReferredReceived as $region)
{
	if($cu%2 != 0)
	{
		$cl = "class='column1'";
	}
	else
	{
		$cl = "class='column2'";
	}
	
 
	?>
	<tr class='odd'>
		<td <?php echo $cl; ?> ><?php echo $region['Region_Name'] ;?></td>
		<td  <?php echo $cl; ?> ><?php echo CRMText::_('COM_CRMERY_CURRENCY').' '. number_format($region['Received_Amount'],2,'.',',');?></td>
		<td  <?php echo $cl; ?> ><?php echo CRMText::_('COM_CRMERY_CURRENCY').' '. number_format($region['Referred_Amount'],2,'.',',');?></td>
	</tr>
	<?php
	
	$total_received_amount = $total_received_amount + $region['Received_Amount'];
	$total_referred_amount = $total_referred_amount + $region['Referred_Amount'];
	$cu++;	
}
	

?>

<tr>
	<td class="foot"><strong>Total</strong></td><td class="foot"><strong><?php echo CRMText::_('COM_CRMERY_CURRENCY').' '. number_format($total_received_amount,2,'.',',');?></strong></td>
	<td class="foot"><strong><?php echo CRMText::_('COM_CRMERY_CURRENCY').' '. number_format($total_referred_amount,2,'.',',');?></strong></td>  
</tr>
</tfoot>
</table>

