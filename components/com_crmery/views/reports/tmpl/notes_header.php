<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$view = JRequest::getVar('view');
$layout = JRequest::getVar('layout');
?>
<table class="com_crmery_table">
        <thead>
            <tr>
                <?php if ( $view != "print" ){ ?>
                <th class="checkbox_column"><input type="checkbox" onclick="selectAll(this);" /></th>
                <?php } ?>
                <th><div class="sort_order"><a class="comp.name" onclick="sortTable('comp.name',this)"><?php echo CRMText::_('COM_CRMERY_COMPANY_REPORT_NAME'); ?></a></div></th>
                <th><div class="sort_order"><a class="deal.name" onclick="sortTable('deal.name',this)"><?php echo CRMText::_('COM_CRMERY_DEAL_REPORT_NAME'); ?></a></div></th>
                <th><div class="sort_order"><a class="person.last_name" onclick="sortTable('person.last_name',this)"><?php echo CRMText::_('COM_CRMERY_PERSON_NAME'); ?></a></div></th>
                <th><div class="sort_order"><a class="user.last_name" onclick="sortTable('user.last_name',this)"><?php echo CRMText::_('COM_CRMERY_OWNER'); ?></a></div></th>
                <th><div class="sort_order"><a class="n.created" onclick="sortTable('n.created',this)"><?php echo CRMText::_('COM_CRMERY_WRITTEN_ON'); ?></a></div></th>
                <th><div class="sort_order"><a class="cat.name" onclick="sortTable('cat.name',this)"><?php echo CRMText::_('COM_CRMERY_CATEGORY'); ?></a></div></th>
                <th><?php echo CRMText::_('COM_CRMERY_DESCRIPTION'); ?></th>
            </tr>
            <?php if ( $view != "print" ){ ?>
            <tr>
                <?php
                    //get user state variables
                    $company_filter = $this->state->get('Note.'.$view.'.'.$layout.'.company_name');
                    $deal_filter = $this->state->get('Note.'.$view.'.'.$layout.'.deal_name');
                    $person_filter = $this->state->get('Note.'.$view.'.'.$layout.'.person_name');
                ?>
                <th></th>
                <th><input class="filter_input" name="company_name" type="text" value="<?php echo $company_filter; ?>"></th>
                <th><input class="filter_input" name="deal_name" type="text" value="<?php echo $deal_filter; ?>"></th>
                <th><input class="filter_input" name="person_name" type="text" value="<?php echo $person_filter; ?>"></th>
                <th>
                   <select class="filter_input" name="owner_id" id="owner_id">
                        <?php $user_filter = $this->state->get('Note.reports.notes.owner_id'); ?>
                        <?php if ( CrmeryHelperUsers::getRole() != 'basic' ){ ?>
                            <?php   $all = array();
                                $all[] = JHTML::_('select.option','all',CRMText::_('COM_CRMERY_ALL')); 
                                echo JHtml::_('select.options',$all,'value','text',$user_filter,true);
                            ?>
                        <?php } ?>
                         <optgroup label="<?php echo CRMText::_('COM_CRMERY_MEMBERS'); ?>" class="member" id="member" >
                            <?php   $member = array();
                                    $member[] = JHTML::_('select.option',CrmeryHelperUsers::getUserId(),CRMText::_('COM_CRMERY_ME')); 
                                    echo JHtml::_('select.options',$member,'value','text',$user_filter,true);
                            ?>
                            <?php echo JHtml::_('select.options', $this->user_names, 'value', 'text', $user_filter, true); ?>
                        </optgroup>
                        <?php if ( CrmeryHelperUsers::getRole() == 'exec' ){ ?>
                        <optgroup label="<?php echo CRMText::_('COM_CRMERY_TEAM'); ?>" class="team" id="team" >
                            <?php echo JHtml::_('select.options', $this->team_names, 'value', 'text', $user_filter, true); ?>
                        </optgroup>
                        <?php } ?>
                    </select>
                </th>
                <th>
                    <select class="filter_input" name="created">
                        <?php $created_filter = $this->state->get('Note.'.$view.'.'.$layout.'.created'); ?>
                        <option value=""><?php echo CRMText::_('COM_CRMERY_ALL'); ?></option>
                        <?php echo JHtml::_('select.options', $this->created_dates, 'value', 'text', $created_filter, true); ?>
                    </select>
                </th>
                <th>
                    <select class="filter_input" name="category_id">
                        <?php $category_filter = $this->state->get('Note.'.$view.'.'.$layout.'.category_id'); ?>
                        <option value=""><?php echo CRMText::_('COM_CRMERY_ALL'); ?></option>
                        <?php echo JHtml::_('select.options',$this->categories, 'id', 'name', $category_filter, true); ?>
                    </select>
                </th>
                <th></th>
            </tr>
            <?php } ?>
        </thead>
        <tfoot>
            <tr>
                <?php if ( $view != "print" ){ ?>
                <td></td>
                <?php } ?>
                <td><?php echo CRMText::_('COM_CRMERY_TOTAL_NOTES_FOUND').'<span class="count">'.count($this->note_entries)."</span>"; ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tfoot>
        <tbody class="results" id="reports">