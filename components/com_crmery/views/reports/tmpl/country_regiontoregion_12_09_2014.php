<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.model');
 ?>

<!--h1><?php //echo CRMText::_('COM_CRMERY_COUNTRY_REGION_TO_REGION'); ?></h1-->
<br />
<?php 

echo $this->menu;
$startYear = $this->startYear;
$tillYear = $this->tillYear;





$regionReferredReceived= $this->regionReferredReceived; 
//echo "<pre>"; print_r($regionReferredAmount); die;

?>



<div class="row"><h3>Report for ALL Regions</h3></div>
<div style="padding-left:50px;"><b>Select Billing Year </b>
		<form name="test" method="post" action="index.php?option=com_crmery&view=reports" >
			
			<select name="chartYear"  >
			<?php for($yr = $tillYear;$yr>=$startYear;$yr--){ 
			
			/*if($billYear == "31/10/2013")
			{
				
				$yearSelected = "selected='selected'" ;
			}*/
			
			?>
				<option <?php echo $yearSelected; ?>  value="31/10/<?php echo $yr ; ?>"><?php echo $yr; ?></option>
				
			<?php }?>
			
				
			</select>
		
		<input type="submit" name="Go" value="Go" />
		</form>
<table width="900"> <thead><tr class="odd" ><th width="250">Region</th><th width="250" >Received</th><th width="250">Referred</th> </tr></thead>
<?php 
$total_received_amount = 0;
$total_referred_amount = 0;
$cu = count($regionReferredReceived);
echo "<tfoot>";


foreach($regionReferredReceived as $region)
{
	if($cu%2 != 0)
	{
		$cl = "class='column1'";
	}
	else
	{
		$cl = "class='column2'";
	}
	
 
	?>
	<tr class='odd'>
		<td <?php echo $cl; ?> ><?php echo $region['Region_Name'] ;?></td>
		<td  <?php echo $cl; ?> ><?php echo CRMText::_('COM_CRMERY_CURRENCY').' '. number_format($region['Received_Amount'],2,'.',',');?></td>
		<td  <?php echo $cl; ?> ><?php echo CRMText::_('COM_CRMERY_CURRENCY').' '. number_format($region['Referred_Amount'],2,'.',',');?></td>
	</tr>
	<?php
	
	$total_received_amount = $total_received_amount + $region['Received_Amount'];
	$total_referred_amount = $total_referred_amount + $region['Referred_Amount'];
	$cu++;	
}
	

?>

<tr>
	<td class="foot"><strong>Total</strong></td><td class="foot"><?php echo CRMText::_('COM_CRMERY_CURRENCY').' '. number_format($total_received_amount,2,'.',',');?></td>
	<td class="foot"><?php echo CRMText::_('COM_CRMERY_CURRENCY').' '. number_format($total_referred_amount,2,'.',',');?></td>  
</tr>
</tfoot>
</table>

