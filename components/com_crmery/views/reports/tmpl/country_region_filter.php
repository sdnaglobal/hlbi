 <?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

  for ( $i=0; $i<count($this->reports); $i++ ){
      $report = $this->reports[$i]; 
      $k = $i%2; ?>
     <tr class="crmery_row_<?php echo $k; ?>">
        <?php if ( JRequest::getVar('view') != "print" ){ ?>
            <td><!--<input type="checkbox" name="ids[]" value="<?php echo $report['id']; ?>" />--></td>
        <?php } ?>
         <td><!--<a href="<?php echo JRoute::_("index.php?option=com_crmery&view=deals&layout=deal&id=".$report['id']); ?>">--><?php echo $report['region_name']; ?><!--</a>--></td>
         <td><?php echo $report['country_name']; ?></td>
         <td><?php echo $report['']; ?></td>
         <td><?php echo $report['amount']; //echo CrmeryHelperConfig::getCurrency().$report['amount']; ?></td>
         <td><?php echo $report['sterling']=$report['value_in_GBP']*$report['amount']; ?></td>
         <td><?php //echo $report['percent']; %?></td>
         <td><?php //echo $report['status_name']; ?><div class="status-dot" style="background-color:#<?php //echo $report['status_color']; ?> !important;"></div></td> 
         <td><?php //echo CrmeryHelperDate::formatDate($report['expected_close']); ?></td>
         <td><?php //echo CrmeryHelperDate::formatDate($report['modified']); ?></td>
         <td><?php //echo CrmeryHelperDate::formatDate($report['created']); ?></td>
     </tr> 
  <?php  $sterling_amount += $report['sterling'];   }  ?>
<?php
    $filtered_amount = 0; 
    if ( count($this->reports) > 0 ){
    foreach ( $this->reports as $key=>$report ){
        $filtered_amount += $report['amount'];
    }
	
}?>
<script type="text/javascript">
    var amount = <?php echo $filtered_amount; ?>;
    jQuery("#filtered_amount").html(amount);	
	
	 var sterling_amount = <?php echo $sterling_amount; ?>;
    jQuery("#sterling_amount").html(sterling_amount);
	
	
</script>