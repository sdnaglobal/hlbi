<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<script type="text/javascript">
    order_url = "<?php echo JRoute::_('index.php?option=com_crmery&view=reports&layout=custom_reports_filter&tmpl=component&format=raw'); ?>";
    order_dir = "<?php echo $this->state->get('Report.filter_order_Dir'); ?>";
    order_col = "<?php echo $this->state->get('Report.filter_order'); ?>";
</script>
<h1><?php echo CRMText::_('COM_CRMERY_CUSTOM_REPORTS'); ?></h1>
<?php echo $this->menu; ?>
<ul class="entry_buttons">
    <li><a class="button" href="<?php echo JRoute::_('index.php?option=com_crmery&view=reports&layout=edit_custom_report'); ?>">New Custom Fields Report</a></li>
</ul>
<table class="com_crmery_table">
    <thead>
        <tr>
            <th><div class="sort_order"><a class="report.name" onclick="sortTable('report.name',this)"><?php echo CRMText::_('COM_CRMERY_CUSTOM_REPORT_NAME'); ?></a></div></th>
            <th><div class="sort_order"><a class="report.modified" onclick="sortTable('report.modified',this)"><?php echo CRMText::_('COM_CRMERY_CUSTOM_REPORT_MODIFIED'); ?></a></div></th>
            <th><div class="sort_order"><a class="report.created" onclick="sortTable('report.created',this)"><?php echo CRMText::_('COM_CRMERY_CUSTOM_REPORT_CREATED'); ?></a></div></th>
            <th><?php echo CRMText::_('COM_CRMERY_CUSTOM_REPORT_ACTIONS'); ?></th>
        </tr>
    </thead>
    <tbody class="results">
        <?php echo $this->custom_reports_list->display(); ?>
    </tbody>
</table>
