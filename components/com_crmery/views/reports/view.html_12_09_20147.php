<?php

/*------------------------------------------------------------------------

# CRMery

# ------------------------------------------------------------------------

# @author CRMery

# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.

# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Website: http://www.crmery.com

-------------------------------------------------------------------------*/

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' ); 



jimport( 'joomla.application.component.view');



class CrmeryViewReports extends JView

{

	function display($tpl = null)

	{



	    //load reports menu bar

	    $this->assignRef('menu',CrmeryHelperTemplate::loadReportMenu());



        $document =& JFactory::getDocument();

        $document->addScriptDeclaration("var graphType='report';");

        

        //determine view layout

        $layout = $this->getLayout();

        if ( $layout == 'default' ){

            

            //get document

            $document =& JFactory::getDocument();

            //load javascripts

            $document->addScript( JURI::base().'components/com_crmery/media/js/highcharts.js' );

            $document->addScript( JURI::base().'components/com_crmery/media/js/sales_dashboard.js' );

            $document->addScriptDeclaration("var loc='sales_dashboard';");

            

            //get data for sales graphs

            $model =& JModel::getInstance('graphs','CrmeryModel');

            $graph_data = $model->getGraphData();

            

            //assign results to view

            $this->assignRef('graph_data',$graph_data);



            $dashFloats = CrmeryHelperUsers::getFloats('sales_dashboard');

            $sales_dash_floats_left = $dashFloats['left'];

            $sales_dash_floats_right = $dashFloats['right'];

            $this->assignRef('sales_dash_floats_left',$sales_dash_floats_left);

            $this->assignRef('sales_dash_floats_right',$sales_dash_floats_right);

            

        }

		 

        //sales pipeline page

        if ( $layout == 'sales_pipeline' ){

            //get deals for reports

            $model =& JModel::getInstance('deal','CrmeryModel');

            $model->set('archived',0);

            $model->set('limit',0);

            $reports = $model->getReportDeals();

            

             // Initialise state variables.

            $state = $model->getState();

            

            //info for dropdowns

            $deal_amounts = CrmeryHelperDeal::getAmounts();

            $deal_stages  = CrmeryHelperDeal::getActiveStages(TRUE);

            $deal_statuses = CrmeryHelperDeal::getStatuses();

            $deal_close_dates = CrmeryHelperDeal::getClosing();

            $modified_dates = CrmeryHelperDeal::getModified();



            //list view

            $sales_pipeline_header = CrmeryHelperView::getView('reports','sales_pipeline_header',array(array('ref'=>'state','data'=>$state),array('ref'=>'reports','data'=>$reports)));

            $sales_pipeline_list = CrmeryHelperView::getView('reports','sales_pipeline_filter',array(array('ref'=>'reports','data'=>$reports)));

            $sales_pipeline_footer = CrmeryHelperView::getView('reports','sales_pipeline_footer');



            $sales_pipeline_header->assignRef('deal_amounts',$deal_amounts);

            $sales_pipeline_header->assignRef('deal_stages',$deal_stages);

            $sales_pipeline_header->assignRef('deal_statuses',$deal_statuses);

            $sales_pipeline_header->assignRef('deal_close_dates',$deal_close_dates);

            $sales_pipeline_header->assignRef('modified_dates',$modified_dates);

            $sales_pipeline_header->assignRef('created_dates',CrmeryHelperDate::getCreatedDates());

            $sales_pipeline_header->assignRef('team_names',CrmeryHelperDropdown::getTeamNames());

            $sales_pipeline_header->assignRef('user_names',CrmeryHelperDropdown::getUserNames());

            $sales_pipeline_header->assignRef('state',$state);



            //assign refs to view

            $this->assignRef('sales_pipeline_header',$sales_pipeline_header);

            $this->assignRef('sales_pipeline_list',$sales_pipeline_list);

            $this->assignRef('sales_pipeline_footer',$sales_pipeline_footer);

            $this->assignRef('state',$state);

            $this->assignRef('reports',$reports);



        }

		

		// ---------- CUSTOMIZATION PART START -----

		

		//referred country region total page

        if ( $layout == 'referred_country_region' ){

            //get deals for reports

            $model =& JModel::getInstance('countryregion','CrmeryModel');

            $model->set('archived',0);

            $model->set('limit',0);

            $reports = $model->getReportDeals();

			

			//--------------  CUSTOMIZATION START ----------

			

			$db =& JFactory::getDBO();

            //gen query

            $query = $db->getQuery(true);

			

			$query->select("*");

            $query->from("#__crmery_deals AS l");

			$query->leftJoin("#__crmery_companies AS cm ON l.company_id=cm.id");

			$query->leftJoin("#__crmery_region AS r ON cm.region_id = r.region_id");

			$query->leftJoin("#__crmery_currencies AS c ON cm.country_id = c.country_id ORDER BY r.region_name");			

			

			//set query

            $db->setQuery($query);

            //load list

            $reports = $db->loadAssocList();		

            

			//--------------  CUSTOMIZATION END ----------

			

             // Initialise state variables.

            $state = $model->getState();

            

            //info for dropdowns

            $deal_amounts = CrmeryHelperDeal::getAmounts();

            $deal_stages  = CrmeryHelperDeal::getActiveStages(TRUE);

            $deal_statuses = CrmeryHelperDeal::getStatuses();

            $deal_close_dates = CrmeryHelperDeal::getClosing();

            $modified_dates = CrmeryHelperDeal::getModified();



            //list view

            $sales_pipeline_header = CrmeryHelperView::getView('reports','country_region_header',array(array('ref'=>'state','data'=>$state),array('ref'=>'reports','data'=>$reports)));

            $sales_pipeline_list = CrmeryHelperView::getView('reports','country_region_filter',array(array('ref'=>'reports','data'=>$reports)));

            $sales_pipeline_footer = CrmeryHelperView::getView('reports','sales_pipeline_footer');



			

			

            $sales_pipeline_header->assignRef('deal_amounts',$deal_amounts);

            $sales_pipeline_header->assignRef('deal_stages',$deal_stages);

            $sales_pipeline_header->assignRef('deal_statuses',$deal_statuses);

            $sales_pipeline_header->assignRef('deal_close_dates',$deal_close_dates);

            $sales_pipeline_header->assignRef('modified_dates',$modified_dates);

            $sales_pipeline_header->assignRef('created_dates',CrmeryHelperDate::getCreatedDates());

            $sales_pipeline_header->assignRef('team_names',CrmeryHelperDropdown::getTeamNames());

            $sales_pipeline_header->assignRef('user_names',CrmeryHelperDropdown::getUserNames());

            $sales_pipeline_header->assignRef('state',$state);



            //assign refs to view

            $this->assignRef('sales_pipeline_header',$sales_pipeline_header);

            $this->assignRef('sales_pipeline_list',$sales_pipeline_list);

            $this->assignRef('sales_pipeline_footer',$sales_pipeline_footer);

            $this->assignRef('state',$state);

            $this->assignRef('reports',$reports);



        }	

		

		//referred country region total page

        if ( $layout == 'country_regiontoregion' ){

            //get deals for reports

            $model =& JModel::getInstance('regiontoregion','CrmeryModel');

            $model->set('archived',0);

            $model->set('limit',0);

            $reports = $model->getReportDeals();

			//echo '<pre>';print_r($reports);

			//echo '</pre>';

			

			//--------------  CUSTOMIZATION START ----------

			

			$db =& JFactory::getDBO();

            //gen query

            $query = $db->getQuery(true);

			

			$query->select("*");

            $query->from("#__crmery_companies AS cm");

			$query->leftJoin("#__crmery_currencies AS c ON cm.country_id = c.country_id");

			$query->leftJoin("#__crmery_region AS r ON r.region_id = cm.region_id");

			$query->leftJoin("#__crmery_deals AS l ON l.company_id = cm.id");			

			$query->where("cm.region_id ='2'");



			/* $query->select("*");

            $query->from("#__luqnj_crmery_people AS ref");

			$query->leftJoin("#__crmery_currencies AS c ON cm.country_id = c.country_id");

			$query->leftJoin("#__crmery_region AS r ON r.region_id = cm.region_id");

			$query->leftJoin("#__crmery_deals AS l ON l.company_id = cm.id");			

			$query->where("cm.region_id ='2'");

 */

			

			//set query

            $db->setQuery($query);

            //load list

			//echo $query;

            $reports = $db->loadAssocList();		

            

			//--------------  CUSTOMIZATION END ----------

			

             // Initialise state variables.

            $state = $model->getState();

            

            //info for dropdowns

            $deal_amounts = CrmeryHelperDeal::getAmounts();

            $deal_stages  = CrmeryHelperDeal::getActiveStages(TRUE);

            $deal_statuses = CrmeryHelperDeal::getStatuses();

            $deal_close_dates = CrmeryHelperDeal::getClosing();

            $modified_dates = CrmeryHelperDeal::getModified();



            //list view

            $sales_pipeline_header = CrmeryHelperView::getView('reports','country_regiontoregion_header',array(array('ref'=>'state','data'=>$state),array('ref'=>'reports','data'=>$reports)));

            $sales_pipeline_list = CrmeryHelperView::getView('reports','country_regiontoregion_filter',array(array('ref'=>'reports','data'=>$reports)));

            $sales_pipeline_footer = CrmeryHelperView::getView('reports','country_regiontoregion_footer');



			

			

            $sales_pipeline_header->assignRef('deal_amounts',$deal_amounts);

            $sales_pipeline_header->assignRef('deal_stages',$deal_stages);

            $sales_pipeline_header->assignRef('deal_statuses',$deal_statuses);

            $sales_pipeline_header->assignRef('deal_close_dates',$deal_close_dates);

            $sales_pipeline_header->assignRef('modified_dates',$modified_dates);

            $sales_pipeline_header->assignRef('created_dates',CrmeryHelperDate::getCreatedDates());

            $sales_pipeline_header->assignRef('team_names',CrmeryHelperDropdown::getTeamNames());

            $sales_pipeline_header->assignRef('user_names',CrmeryHelperDropdown::getUserNames());

            $sales_pipeline_header->assignRef('state',$state);



            //assign refs to view

            $this->assignRef('sales_pipeline_header',$sales_pipeline_header);

            $this->assignRef('sales_pipeline_list',$sales_pipeline_list);

            $this->assignRef('sales_pipeline_footer',$sales_pipeline_footer);

            $this->assignRef('state',$state);

            $this->assignRef('reports',$reports);



        }

		

		//-------------- CUSTOMIZATION PART END HERE --------- 

		

		

		

		

        //source report page

        if ( $layout == 'source_report' ){

            //get deals for reports

            $model =& JModel::getInstance('deal','CrmeryModel');

            $model->set('archived',0);

            $model->set('limit',0);

            $reports = $model->getDeals();

            

            //get model state

            $state = $model->getState();

            

            //info for dropdowns

            $deal_amounts = CrmeryHelperDeal::getAmounts();

            $deal_sources = CrmeryHelperDeal::getSources();

            $deal_stages  = CrmeryHelperDeal::getStages();

            $deal_statuses = CrmeryHelperDeal::getStatuses();

            $deal_close_dates = CrmeryHelperDeal::getClosing();

            $modified_dates = CrmeryHelperDeal::getModified();



            //list view

            $source_report_header = CrmeryHelperView::getView('reports','source_report_header',array(array('ref'=>'state','data'=>$state),array('ref'=>'reports','data'=>$reports)));

            $source_list = CrmeryHelperView::getView('reports','source_report_filter',array(array('ref'=>'reports','data'=>$reports)));

            $source_report_footer = CrmeryHelperView::getView('reports','source_report_footer');

            

            //assign refs to view

            $source_report_header->assignRef('deal_amounts',$deal_amounts);

            $source_report_header->assignRef('deal_sources',$deal_sources);

            $source_report_header->assignRef('deal_stages',$deal_stages);

            $source_report_header->assignRef('deal_statuses',$deal_statuses);

            $source_report_header->assignRef('deal_close_dates',$deal_close_dates);

            $source_report_header->assignRef('modified_dates',$modified_dates);

            $source_report_header->assignRef('created_dates',CrmeryHelperDate::getCreatedDates());

            $source_report_header->assignRef('team_names',CrmeryHelperDropdown::getTeamNames());

            $source_report_header->assignRef('user_names',CrmeryHelperDropdown::getUserNames());

            $source_report_header->assignRef('state',$state);



            $this->assignRef('source_report_header',$source_report_header);

            $this->assignRef('source_report_list',$source_list);

            $this->assignRef('source_report_footer',$source_report_footer);

            $this->assignRef('state',$state);

            $this->assignRef('reports',$reports);

        }

        //roi report page

        if ( $layout == 'roi_report' ){

            //get sources for reports

            $model =& JModel::getInstance('source','CrmeryModel');

            $sources = $model->getRoiSources();

             

             // Initialise state variables.

            $state = $model->getState();



            //list view

            $roi_report_header = CrmeryHelperView::getView('reports','roi_report_header');

            $roi_list = CrmeryHelperView::getView('reports','roi_report_filter',array(array('ref'=>'sources','data'=>$sources)));

            $roi_report_footer = CrmeryHelperView::getView('reports','roi_report_footer');



            $roi_report_header->assignRef('state',$state);

            

            //assign refs to view

            $this->assignRef('roi_report_header',$roi_report_header);

            $this->assignRef('roi_list',$roi_list);

            $this->assignRef('roi_report_footer',$roi_report_footer);

            $this->assignRef('state',$state);

            $this->assignRef('sources',$sources);

        }

        //deal milestones page

        if ( $layout == 'deal_milestones' ){

            //get deals for reports

            $model = JModel::getInstance('deal','CrmeryModel');

            $model->set('archived',0);

            $model->set('completed','false');

            $deals = $model->getDeals();

            

            //pagination

            $pagination = $model->getPagination();

            $this->assignRef('pagination',$pagination);

            

             // Initialise state variables.

            $state = $model->getState();



            //list view

            $deal_milestone_list = CrmeryHelperView::getView('reports','deal_milestones_filter',array(array('ref'=>'deals','data'=>$deals)));

            

            //assign refs to view

            $this->assignRef('deal_milestone_list',$deal_milestone_list);

            $this->assignRef('state',$state);

            $this->assignRef('deals',$deals);

        }

        //notes report page

        if ( $layout == 'notes' ){

            //get model for reports

            $model = JModel::getInstance('note','CrmeryModel');

            $note_entries = $model->getNotes(NULL,NULL,FALSE);

            $notes_list = CrmeryHelperView::getView('reports','notes_filter',array(array('ref'=>'note_entries','data'=>$note_entries)));



            $state = $model->getState();

            $notes_header = CrmeryHelperView::getView('reports','notes_header',array(array('ref'=>'note_entries','data'=>$note_entries),array('ref'=>'state','data'=>$state)));

            $notes_footer = CrmeryHelperView::getView('reports','notes_footer');

            $notes_header->assignRef('team_names',CrmeryHelperDropdown::getTeamNames());

            $notes_header->assignRef('user_names',CrmeryHelperDropdown::getUserNames());

            $notes_header->assignRef('state',$state);



            $categories = $model->getNoteCategories();

            $notes_header->assignRef('categories',$categories);



            $notes_header->assignRef('created_dates',CrmeryHelperDate::getCreatedDates());

            

             // Initialise state variables.

            $state = $model->getState();



            //pagination

            $pagination = $model->getPagination();

            $notes_footer->assignRef('pagination',$pagination);

            

            //assign refs to view

            $this->assignRef('notes_header',$notes_header);

            $this->assignRef('notes_list',$notes_list);

            $this->assignRef('notes_footer',$notes_footer);

            $this->assignRef('state',$state);

            $this->assignRef('note_entries',$note_entries);

        }

        

        //custom reports default

        if ( $layout == 'custom_reports' ){

            

            //load java libs

            $doc = JFactory::getDocument();

            $doc->addScript( JURI::base().'components/com_crmery/media/js/custom_reports.js' );

            

            //get info from model

            $model = JModel::getInstance('report','CrmeryModel');

            $reports = $model->getCustomReports();

            $state = $model->getState();



            //list view

            $custom_reports_list = CrmeryHelperView::getView('reports','custom_reports_filter',array(array('ref'=>'reports','data'=>$reports)));

            

            //assign references

            $this->assignRef('custom_reports_list',$custom_reports_list);

            $this->assignRef('reports',$reports);

            $this->assignRef('state',$state);

            

        }

       

        //individual custom reports

        if ( $layout == "custom_report" ){

            //get model

            $model =& JModel::getInstance('report','CrmeryModel');

            //get report

            $report = $model->getCustomReports(JRequest::getVar('id'));

            $report_data = $model->getCustomReportData(JRequest::getVar('id'));

            $state = $model->getState();

            //assign refs

            $this->assignRef('report',$report);

            $this->assignRef('report_data',$report_data);

            $this->assignRef('state',$state);

            

            //info for dropdowns

            $deal_amounts = CrmeryHelperDeal::getAmounts();

            $deal_sources = CrmeryHelperDeal::getSources();

            $deal_stages  = CrmeryHelperDeal::getSourceStages();

            $deal_statuses = CrmeryHelperDeal::getStatuses();

            $deal_expected_close_dates = CrmeryHelperDeal::getExpectedClosingDates();

            $deal_actual_close_dates = CrmeryHelperDeal::getActualClosingDates();

            $modified_dates = CrmeryHelperDeal::getModified();

            $companyCategories = CrmeryHelperCompany::getCategories();



            //list layout

            $data = array(

                    array('ref'=>'report_data','data'=>$report_data),

                    array('ref'=>'report','data'=>$report),

                    array('ref'=>'state','data'=>$state),

                );



            $custom_report_header = CrmeryHelperView::getView('reports','custom_report_header',$data);

            $custom_report_list = CrmeryHelperView::getView('reports','custom_report_filter',$data);

            $custom_report_footer = CrmeryHelperView::getView('reports','custom_report_footer');



            $custom_report_header->assignRef('deal_amounts',$deal_amounts);

            $custom_report_header->assignRef('deal_sources',$deal_sources);

            $custom_report_header->assignRef('deal_stages',$deal_stages);

            $custom_report_header->assignRef('deal_statuses',$deal_statuses);

            $custom_report_header->assignRef('deal_actual_close_dates',$deal_actual_close_dates);

            $custom_report_header->assignRef('deal_expected_close_dates',$deal_expected_close_dates);

            $custom_report_header->assignRef('modified_dates',$modified_dates);

            $custom_report_header->assignRef('created_dates',CrmeryHelperDate::getCreatedDates());

            $custom_report_header->assignRef('team_names',CrmeryHelperDropdown::getTeamNames());

            $custom_report_header->assignRef('user_names',CrmeryHelperDropdown::getUserNames());

            $custom_report_header->assignRef('state',$state);

            $custom_report_header->assignRef("company_categories",$companyCategories);

                        

            //assign refs to view

            $this->assignRef('custom_report_header',$custom_report_header);

            $this->assignRef('custom_report_list',$custom_report_list);

            $this->assignRef('custom_report_footer',$custom_report_footer);

            $this->assignRef('state',$state);

            $this->assignRef('reports',$reports);

            



        }

        

        //edit custom reports

        if ( $layout == "edit_custom_report" ){

            //load java libs

            $doc = JFactory::getDocument();

            $doc->addScript( JURI::base().'components/com_crmery/media/js/custom_reports.js' );

            

            //if we are editing an existing entry

            $id = JRequest::getVar('id');

            if ( $id != null ){

                $model = JModel::getInstance('report','CrmeryModel');

                $report = $model->getCustomReports($id);

                $this->assignRef('report',$report); 

            }

            

            //assign references

            $columns = CrmeryHelperDeal::getAllCustomFields($id);

            if ( CrmeryHelperQuote::hasCrmQuote() ){

                $columns = $columns + CrmeryHelperQuote::getColumns();

            }

            $this->assignRef('columns',$columns);

        }
		
		
		// Region Reports
		$model = & JModel::getInstance('report','CrmeryModel');
		
		$allYear = $model->getStartingLastYear();
		$start = $allYear['start'];
		$till = $allYear['till'];
		
		$s = explode("-",$start);
		$startYear = $s[0];
		
		$t = explode("-",$till);
		$tillYear = $t[0];
		
		$regionReferredReceived = $model->getRegionReport();
		$countryReferredReceived = $model->getCountryReport();
		$firmReferredReceived = $model->getFirmReport();
		
		
		
		$this->assignRef('startYear',$startYear);
		$this->assignRef('tillYear',$tillYear);
		$this->assignRef('regionReferredReceived',$regionReferredReceived);
		$this->assignRef('countryReferredReceived',$countryReferredReceived);
		$this->assignRef('firmReferredReceived',$firmReferredReceived);
		
		//echo "<pre>"; print_r($regionReport); die;
		

        

        //assign user filter priviliges

        $member_role = CrmeryHelperUsers::getRole();

        $user_id = CrmeryHelperUsers::getUserId();

        $team_id = CrmeryHelperUsers::getTeamId();

        //if the user is not basic then they are able to filter through company/team/user data

        if ( $member_role != 'basic' ){

            

            //exec can see teams

            if ( $member_role == 'exec' ){

                $teams = CrmeryHelperUsers::getTeams();

                $this->assignRef('teams',$teams);

            }

            

            //manager and exec users

            $users = CrmeryHelperUsers::getUsers();

            $this->assignRef('users',$users);

        }

        

        

        //assign team names for drop downs

        $team_names = CrmeryHelperDropdown::getTeamNames();

        $this->assignRef('team_names',$team_names);

       

        //assign user names for drop downs

        $this->assignRef('user_names',CrmeryHelperDropdown::getUserNames());

        

        //assign categories for drop downs

        $this->assignRef('categories',CrmeryHelperNote::getCategories());

        

        //assign refs

        $this->assignRef('member_role',$member_role);

        $this->assignRef('user_id',$user_id);

        $this->assignRef('team_id',$team_id);

        $this->assignRef('layout',$layout);



	    //display

		parent::display($tpl);

	}

	

}

?>

