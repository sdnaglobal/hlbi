<?php

/*------------------------------------------------------------------------

# CRMery

# ------------------------------------------------------------------------

# @author CRMery

# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.

# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Website: http://www.crmery.com

-------------------------------------------------------------------------*/

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' ); 



jimport( 'joomla.application.component.view');



class CrmeryViewReports extends JView
{

	function display($tpl = null)
	{

	    //load reports menu bar

	    $this->assignRef('menu',CrmeryHelperTemplate::loadReportMenu());

		
        $layout = $this->getLayout();
		if( $layout == "default")
		{
			$layout = $_REQUEST['reportFor'];
		
		}
		
		// Region Reports
		$model = & JModel::getInstance('report','CrmeryModel');
		
		
		$chartYear = $_REQUEST['chartYear'];
	
		if($chartYear == '')  
		{
			if($_SESSION['yearSess'] == '')
			{
				$billYear = date("Y");
			
			}
			else
			{
				$billYear = $_SESSION['yearSess'];
			
			}
			
		}
		else
		{
			$billYear = $chartYear;
		}
		$_SESSION['yearSess'] = $billYear ;
		$yearLastWords = substr($billYear,-2);
		//echo "<pre>"; print_r($_SESSION); die;
		$allYear = $model->getStartingLastYear();
		$start = $allYear['start'];
		$till = $allYear['till'];
		
		$s = explode("-",$start);
		$startYear = $s[0];
		
		$t = explode("-",$till);
		$tillYear = $t[0];
		
		
		
		
		$regionReferredReceived = $model->getRegionReport($yearLastWords);
		$countryReferredReceived = $model->getCountryReport($yearLastWords);
		$firmReferredReceived = $model->getFirmReport($yearLastWords);
		
		
		
		$this->assignRef('startYear',$startYear);
		$this->assignRef('tillYear',$tillYear);
		$this->assignRef('billYear',$billYear);
		
		$this->assignRef('regionReferredReceived',$regionReferredReceived);
		$this->assignRef('countryReferredReceived',$countryReferredReceived);
		$this->assignRef('firmReferredReceived',$firmReferredReceived);
		
		
		
		$this->assignRef('startYear',$startYear);
		
		
		
		//echo "<pre>"; print_r($regionReport); die;
		

        $this->assignRef('firm_ids',$firm_ids);

	    //display

		parent::display($tpl);

	}

	

}

?>

