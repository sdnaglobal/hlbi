<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewReports extends JView
{
    function display($tpl = null)
    {
        //get layout requested to load correct data and pass references
        $layout = $this->getLayout();
        
        //notes report page
        if ( $layout == 'notes_filter' ){
            //get model for reports
            $model =& JModel::getInstance('note','CrmeryModel');
            $notes = $model->getNotes(NULL,NULL,FALSE);
            //assign refs to view
            $this->assignRef('note_entries',$notes);
        }
        
        //deal_milestones page
        if ( $layout == "deal_milestones_filter" ){
            //get deals for reports
            $model =& JModel::getInstance('deal','CrmeryModel');
            $model->set('archived',0);
            $deals = $model->getDeals();
            //assign refs to view
            $this->assignRef('deals',$deals);
        }
        
        //roi report page
        if ( $layout == 'roi_report_filter' ){
            //get sources for reports
            $model =& JModel::getInstance('source','CrmeryModel');
            $sources = $model->getRoiSources();
             
            //assign refs to view
            $this->assignRef('sources',$sources);
        }
        
        //sales pipeline page
        if ( $layout == 'sales_pipeline_filter' ){
            //get deals for reports
            $model =& JModel::getInstance('deal','CrmeryModel');
            $model->set('archived',0);
            $model->set('limit',0);
            $reports = $model->getReportDeals();
            
            //assign refs to view
            $this->assignRef('reports',$reports);
        }
		
		 //referred country region total page
        if ( $layout == 'country_region_filter' ){
            //get deals for reports
            $model =& JModel::getInstance('CountryRegion','CrmeryModel');
            $model->set('archived',0);
            $model->set('limit',0);
            $reports = $model->getReportDeals();
            
            //assign refs to view
            $this->assignRef('reports',$reports);
        }
		
		 //referred country within region total page
        if ( $layout == 'country_regiontoregion_filter' ){
            //get deals for reports
            $model =& JModel::getInstance('regiontoregion','CrmeryModel');
            $model->set('archived',0);
            $model->set('limit',0);
            $reports = $model->getReportDeals();
            
            //assign refs to view
            $this->assignRef('reports',$reports);
        }
		
        
        //source report page
        if ( $layout == 'source_report_filter' ){
            //get deals for reports
            $model =& JModel::getInstance('deal','CrmeryModel');
            $model->set('archived',0);
            $model->set('limit',0);
            $reports = $model->getDeals();
            
            //assign refs to view
            $this->assignRef('reports',$reports);
        }
        
        //custom reports default page
        if ( $layout == "custom_reports_filter" ){
            //get model
            $model =& JModel::getInstance('report','CrmeryModel');
            $reports = $model->getCustomReports();
            $state = $model->getState();

            
            //assign refs
            $this->assignRef('state',$state);
            $this->assignRef('reports',$reports);
        }
        
        //individual custom reports
        if ( $layout == "custom_report_filter" ){
            //get model
            $model =& JModel::getInstance('report','CrmeryModel');
            //get report
            $report = $model->getCustomReports(JRequest::getVar('id'));
            $report_data = $model->getCustomReportData(JRequest::getVar('id'));
            $state = $model->getState();
            //assign refs
            $this->assignRef('state',$state);
            $this->assignRef('report',$report);
            $this->assignRef('report_data',$report_data);
        }
        
        //display
        parent::display($tpl);      
    }
    
}
        
        