<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
require_once (JPATH_SITE.'/components/com_crmery/helpers/users.php');
function CrmeryBuildRoute(&$query)
{
	
	$array = array();

	if ( array_key_exists('view',$query) ){
		$array[] = $query['view'];
	}

	if ( array_key_exists('id',$query) ){
		$array[] = $query['id'];
	}

	if ( array_key_exists('company_id',$query) ){
		$array[] = $query['company_id'];
		unset($query['company_id']);
	}

	if ( array_key_exists('layout',$query)){
		if ( $query['layout'] == 'edit' ){
			$array[] = $query['layout'];
		}
		if ( $query['view'] == "reports" ){
			$array[] = $query['layout'];
		}
		if ($query['view'] == "companies") {
			if ( $query['layout'] != "edit" ){
				$array[] = $query['layout'];
			}
		}
		if ( $query['view'] == "goals" ){
			if ( $query['layout'] != "edit" ){
				$array[] = $query['layout'];
			}
			if ( array_key_exists('type',$query) ){
				$array[] = $query['type'];
				unset($query['type']);
			}
		}
		if ( $query['layout'] == "edit_task" ){
			$array[] = $query['layout'];
		}
	}

	if ( array_key_exists('import_type',$query) ){
		$array[] = $query['import_type'];
		unset($query['import_type']);
	}

	unset($query['view']);
	unset($query['layout']);
	unset($query['id']);


	return $array;
	
}

function CrmeryParseRoute($segments)
{
	$vars = array();
	$length = count($segments);
	
	if(isset($segments[0]) && JFile::exists(JPATH_BASE.'/components/com_crmery/views/'.$segments[0].'/view.html.php') ){
		$vars['view'] = $segments[0];
	}

	if ( array_key_exists(1,$segments) ){
		if ( $segments[1] == "edit" ){
			$vars['layout'] = "edit";
		}else if ( $segments[0] == "reports"){
			$vars['layout'] = $segments[1];

		}else if ( $segments[0] == "import" ){
			if ( $segments[1] == "review" ){
				$vars['layout'] = "review";
			}else{
				$vars['layout'] = "import";
				$vars['import_type'] = $segments[1];
			}
		}else if( $segments[0] == "documents" ) {
			if ( array_key_exists(1,$segments) && $segments[1] == "download" ){
				$vars['layout'] = "download";
				$vars['document'] = $segments[2];
				unset($segments);
			}
		}else{
			$vars['id'] = $segments[1];
			switch ( $vars['view'] ){
				case "deals":
					$layout = "deal";
				break;
				case "people":
					$layout = "person";
				break;
				case "companies":
					$layout = "company";
				break;
				case "events":
					$layout = "event";
				break;
			}
			if ( isset($layout) ){
				$vars['layout'] = $layout;
			}
		}
	}


	if ( array_key_exists(2,$segments) ){
		$vars['id'] = $segments[1];
		$vars['layout'] = $segments[2];
	}

	if ( array_key_exists('view',$vars) ){
		if ( $vars['view'] == "goals" ){
			if ( array_key_exists(1,$segments) )
			{
				if ( array_key_exists(2,$segments) ){
					$vars['layout'] = "edit";
					$vars['type'] = $segments[2];
				}else{
					$vars['layout'] = "add";
				}
			}
		}

		if ( $vars['view'] == "print" ){
			$vars['tmpl'] = "component";
			if (array_key_exists(1,$segments)){
				$vars['layout'] = $segments[1];
			}
			if ( array_key_exists(2,$segments) ){
				$vars['item_id'] = $segments[2];
			}
		}

		if ( $vars['view'] == "events" ){
			if ( array_key_exists(1,$segments) ){
				if ( $segments[1]=="edit_task" ){
					$vars['layout'] = "edit_task";
				}
			}
		}
	}

	if(array_key_exists('mobile',$_SESSION)) {
		if ( $_SESSION['mobile'] == "yes"){
        	$vars['mobile'] = "yes";
        }
        if ( $_SESSION['mobile'] == "no" ){
        	$vars['mobile'] = "no";
        }
    }
     
	return $vars;
}