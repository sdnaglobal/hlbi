<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class TableCompanypartners extends JTable
{
    var $id                     = null;
    var $name                   = null;
    var $type                   = null;
    var $required               = null;
    var $values                 = null;
    var $multiple_selections    = null;
    var $created                = null;
    var $modified               = null;
    var $ordering               = null;
    
    /**
     * Constructor
     *
     * @param object Database connector object
     */
    function __construct( &$db ) {
        parent::__construct('#__crmery_company_partners', 'id', $db);
    }
}