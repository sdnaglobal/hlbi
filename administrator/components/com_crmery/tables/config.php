<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class TableConfig extends JTable
{
    var $id                     = null;
    var $timezone               = null;  
    var $imap_user              = null;
    var $imap_pass              = null;
    var $imap_host              = null;              
    var $users                  = null;
    var $settings               = null;
    var $workflows              = null;
    var $menu                   = null;
    var $import                 = null;
    var $launch                 = null;
    var $show_help              = null;
    var $lang_deal              = null;
    var $lang_company           = null;
    var $lang_person            = null;
    var $lang_contact           = null;
    var $lang_lead              = null;
    var $lang_task              = null;
    var $lang_event             = null;
    var $lang_goal              = null;
    var $currency               = null;
    var $sample_data            = null;
    var $welcome_message        = null;
    var $acy_company            = null;
    var $acy_person             = null;
    var $allowed_documents      = null;
    var $fullscreen             = null;
    var $toolbar_help           = null;
    var $login_redirect         = null;
    var $help_url               = null;
    
    /**
     * Constructor
     *
     * @param object Database connector object
     */
    function __construct( &$db ) {
        parent::__construct('#__crmery_config', 'id', $db);
    }
}