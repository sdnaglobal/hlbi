<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class TableFormwizard extends JTable
{
    var $id                     = null;
    var $name					= null;
    var $description			= null;
    var $type					= null;
    var $created 				= null;
    var $created_by				= null;
    var $modified				= null;
    var $modified_by			= null;
    var $fields                 = null;
    var $html                   = null;
    var $return_url             = null;
    var $owner_id               = null;
    
    /**
     * Constructor
     *
     * @param object Database connector object
     */
    function __construct( &$db ) {
        parent::__construct('#__crmery_formwizard', 'id', $db);
    }
}