<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class TableBranding extends JTable
{
    var $id                     = null;
    var $header                 = null;
    var $tabs_hover             = null;
    var $tabs_hover_text        = null;
    var $table_header_row       = null;
    var $table_header_text      = null;
    var $assigned               = null;
    var $modified               = null;                      
    
    /**
     * Constructor
     *
     * @param object Database connector object
     */
    function __construct( &$db ) {
        parent::__construct('#__crmery_branding', 'id', $db);
    }
}