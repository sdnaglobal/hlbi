<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class TableUsers extends JTable
{
    var $id                         = null;
    var $uid                        = null;
    var $role_type                  = null;
    var $admin                      = null;
    var $exports                    = null;
    var $can_delete                 = null;
    var $team_id                    = null;
    var $first_name                 = null;
    var $last_name                  = null;
    var $created                    = null;
    var $modified                   = null;
 
    /**
     * Constructor
     *
     * @param object Database connector object
     */
    function __construct( &$db ) {
        parent::__construct('#__crmery_users', 'id', $db);
    }
}