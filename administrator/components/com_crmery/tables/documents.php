<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class TableDocuments extends JTable
{
    var $id                     = null;
    var $name                   = null;
    var $filename               = null;
    var $association_id         = null;
    var $association_type       = null;
    var $created                = null;
    var $filetype               = null;
    var $size                   = null;
    var $owner_id               = null;
    var $modified               = null;
    var $shared                 = null;
    var $email                  = null;
    
    /**
     * Constructor
     *
     * @param object Database connector object
     */
    function __construct( &$db ) {
        parent::__construct('#__crmery_documents', 'id', $db);
    }
}