<?php error_reporting(0);
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 


jimport('joomla.application.component.controller');

class CrmeryController extends JController
{
    
    /**
     * Constructor
     */
    function __construct(){
        
        parent::__construct();
        
         //set title
        JToolBarHelper::title(JText::_('COM_CRMERY_ADMIN'), 'moo');
        
        //load menu helper
        CrmeryHelperMenu::loadNavi();

        
    }
	/**
	 * Method to display the view
	 *
	 * @access	public
	 */
	function display()
	{
        
        parent::display();  
	}
    
}