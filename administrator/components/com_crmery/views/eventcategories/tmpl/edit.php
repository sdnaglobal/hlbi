<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="container-fluid">
	<?php echo $this->menu['help_menu']->display(); ?>
	<div class="row-fluid">
	<div class="span12" id="content">
	<div id="system-message-container"></div>
		<div class="row-fluid">
				<?php echo $this->menu['menu']->display(); ?>
		<div class="span9">
<form action="<?php echo JRoute::_('index.php?option=com_crmery&view=eventcategories'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate"  >
    <div class="width-100 fltlft">
            <legend><h3><?php echo $this->header; ?></h3></legend>
                    <label><b><?php echo JText::_('COM_CRMERY_NAME'); ?></b></label>
                    <input type="text" class="inputbox" name="name" value="<?php if ( is_array($this->category) ) echo $this->category['name']; ?>" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_NAME_EVENT_TOOLTIP'); ?>" />
                    <label><b><?php echo JText::_('COM_CRMERY_DESCRIPTION'); ?></b></label>
                    <textarea name="description" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_DESCRIBE_EVENT_TOOLTIP'); ?>" ><?php if ( is_array($this->category) ) echo $this->category['description']; ?></textarea>
                    <label><b><?php echo JText::_('COM_CRMERY_MILESTONE'); ?></b></label>
                    <input type="checkbox" value="1" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_EVENT_IS_A_MILESTONE'); ?>" name="milestone" <?php if (is_array($this->category) && array_key_exists('milestone',$this->category) && $this->category['milestone'] == 1 ) echo "checked='checked'"; ?> />
                <div>
                    <?php if ( is_array($this->category) && $this->category['id'] ) { ?>
                        <input type="hidden" name="id" value="<?php echo $this->category['id']; ?>" />
                    <?php } ?>
                    <input type="hidden" name="task" value="" />
                    <?php echo JHtml::_('form.token'); ?>
                </div>
    </div>
</form>
</div>
</div>
</div>
<?php $this->menu['quick_menu']->display(); ?>
					</div>
			</div>