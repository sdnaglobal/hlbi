<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<script type="text/javascript">
    <?php if ( $this->status['color'] != null ) { ?>
        var status_color = "<?php echo $this->status['color']; ?>";
    <?php } else { ?>
        var status_color = "ff0000";
    <?php } ?>
</script>

<div class="container-fluid">
	<?php echo $this->menu['help_menu']->display(); ?>
	<div class="row-fluid">
	<div class="span12" id="content">
	<div id="system-message-container"></div>
		<div class="row-fluid">
				<?php echo $this->menu['menu']->display(); ?>
		<div class="span9">
<form action="<?php echo JRoute::_('index.php?option=com_crmery&view=statuses'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate"  >
    <div class="width-100 fltlft">
            <legend><h3><?php echo $this->header; ?></h3></legend>
                    <label><b><?php echo JText::_("COM_CRMERY_NAME"); ?></b></label>
                    <input type="text" class="inputbox" name="name" value="<?php echo $this->status['name']; ?>" />
                    <label><b><?php echo JText::_('COM_CRMERY_HEADER_STATUS_COLOR'); ?></b></label>
                    <span class="crmeryfield"><input type="text" class="inputbox" name="color" value="<?php echo $this->status['color']; ?>" /><div id="colorwheel" class="colorwheel"></div></span>
                <div>
                    <?php if ( $this->status['id'] ) { ?>
                        <input type="hidden" name="id" value="<?php echo $this->status['id']; ?>" />
                    <?php } ?>
                    <input type="hidden" name="task" value="" />
                    <?php echo JHtml::_('form.token'); ?>
                </div>
    </div>
</form>
</div>
</div>
</div>
<?php $this->menu['quick_menu']->display(); ?>
					</div>
			</div>