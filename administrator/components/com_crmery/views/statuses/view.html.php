<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 


jimport( 'joomla.application.component.view' );


class CrmeryViewStatuses extends JView{

    /**
     * display method
     * @return void
     **/
    function display($tpl = null)
    {
        //display title
        JToolBarHelper::title(JText::_('COM_CRMERY_ADMIN').JText::_('COM_CRMERY_PEOPLE_STATUSES'), 'moo');

         /** Menu Links **/
        $menu = CrmeryHelperMenu::getMenuModules();
        $this->assignRef('menu',$menu);
        
        $layout = $this->getLayout();
        $this->pagination   = $this->get('Pagination');
        
        if ( $layout && $layout == 'edit' ){
            
            //toolbar buttons
            JToolbarHelper::cancel('Statuses.cancel');
            JToolbarHelper::save('Statuses.save');
            //javascripts
            $document =& JFactory::getDocument();
            $document->addScript(JURI::base().'components/com_crmery/media/js/colorpicker.js');
            //stylesheets
            $document->addStylesheet(JURI::base().'components/com_crmery/media/css/colorpicker.css');
            
        }else{
            
            //buttons
            JToolBarHelper::addNew('Statuses.add');
            JToolBarHelper::editList('Statuses.edit');
            JToolBarHelper::deleteList(JText::_('COM_CRMERY_CONFIRMATION'),'Statuses.remove');
                
            //gather information for view
            $model = JModel::getInstance('statuses','CrmeryModel');
            $statuses = $model->getStatuses();
            $this->assignRef('statuses',$statuses);
                
            // Initialise state variables.
            $state = $model->getState();
            $this->assignRef('state',$state);
        }
        
        //display
        parent::display($tpl);
    }
}