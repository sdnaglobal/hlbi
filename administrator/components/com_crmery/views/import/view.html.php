<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class CrmeryViewImport extends JView
{
	function display($tpl = null)
	{

		if(JRequest::getVar('layout')=='sample') {
			$this->_displaySample($tpl);
			return;
		}

		/** Menu Links **/
        $menu = CrmeryHelperMenu::getMenuModules();
        $this->assignRef('menu',$menu);

		$doc =& JFactory::getDocument();

		$import_post = FALSE;

		if ( count($_FILES) > 0 ){
			$import_post = TRUE;
			$import_data = array();
			$import_type = JRequest::getVar('import_type');
			$model =& JModelLegacy::getInstance("import",'CrmeryModel');
			foreach ( $_FILES as $file ){
				$data = $model->readCSVFile($file['tmp_name']);
				$import_data = array_merge($import_data,$data);
			}

			if ( count($import_data) > 0 ){
					switch ( JRequest::getVar('import_type') ){
						case "company":
						case "companies":
							$import_model = "company";
						break;
						case "people":
							$import_model = "people";
						break;
						case "deals":
							$import_model = "deal";
						break;
					}
					if ( $model->importCSVData($import_data,$import_model) ){
						$success = "SUCCESSFULLY";
					}else{
						$success = "UNSUCCESSFULLY";
					}
				$view = "import";
				$app =& JFactory::getApplication();
				$msg = CRMText::_('COM_CRMERY_'.$success.'_IMPORTED_ITEMS');
				$app->redirect(JRoute::_('index.php?option=com_crmery&view='.$view),$msg);
			}

			$this->assignRef('headers',$import_data['headers']);
			unset($import_data['headers']);
			$this->assignRef('import_data',$import_data);
			$this->assignRef('import_type',$import_type);
         	$doc->addScriptDeclaration('import_length='.count($import_data).';');
         	$doc->addScriptDeclaration("show_tab='import_review';");

		}

		$this->assignRef('import_post',$import_post);

	    //display
		parent::display($tpl);
	}

	function _displaySample($tpl=null) {

		/** Menu Links **/
        $menu = CrmeryHelperMenu::getMenuModules();
        $this->assignRef('menu',$menu);

		$doc =& JFactory::getDocument();

		parent::display($tpl);
	}
	
}
?>
