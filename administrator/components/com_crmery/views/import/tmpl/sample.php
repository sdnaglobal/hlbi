<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="container-fluid">
    <?php echo $this->menu['help_menu']->display(); ?>
    <?php $this->menu['quick_menu']->display(); ?>
    <div class="row-fluid">
    <div class="span12" id="content">
    <div id="system-message-container"></div>
        <div class="row-fluid">
                <?php echo $this->menu['menu']->display(); ?>
        <div class="span9">

    <div class="width-100 fltlft">
            <div class="sample_text">
            	<h1><?php echo JText::_('COM_CRMERY_INSTALL_SAMPLE_DATA_TITLE'); ?></h1>
                <p><div class="alert alert-info"><?php echo JText::_('COM_CRMERY_INSTALL_SAMPLE_DATA_DESC'); ?></div></p>
                <form action="<?php echo JRoute::_('index.php?option=com_crmery&view=import'); ?>" method="post" name="adminForm" id="adminForm" class="inline-form"  >
                    <input type="submit" value="<?php echo JText::_('COM_CRMERY_INSTALL_SAMPLE_BUTTON'); ?>" class="btn btn-primary btn-large" />
                    <input type="hidden" name="id" value="1" />
                    <input type="hidden" name="task" value="installSampleData" />
                    <input type="hidden" name="controller" value="import" />
                    <input type="hidden" name="view" value="import" />
                    <input type="hidden" name="layout" value="sample" />
                    <?php echo JHtml::_('form.token'); ?>
                </form>
                <form action="<?php echo JRoute::_('index.php?option=com_crmery&view=import'); ?>" method="post" name="adminForm" id="adminForm" class="inline-form"  >
                    <input type="submit" value="<?php echo JText::_('COM_CRMERY_REMOVE_SAMPLE_BUTTON'); ?>" class="btn btn-danger btn-large" />
                    <input type="hidden" name="id" value="1" />
                    <input type="hidden" name="task" value="removeSampleData" />
                    <input type="hidden" name="controller" value="import" />
                    <input type="hidden" name="view" value="import" />
                    <input type="hidden" name="layout" value="sample" />
                    <?php echo JHtml::_('form.token'); ?>
                </form>
            </div>
    </div>
</form>
</div>
</div>
</div>
</div>
</div>
