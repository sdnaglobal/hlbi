<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="container-fluid">
	<?php echo $this->menu['help_menu']->display(); ?>
	<div class="row-fluid">
	<div class="span12" id="content">
	<div id="system-message-container"></div>
		<div class="row-fluid">
				<?php echo $this->menu['menu']->display(); ?>
		<div class="span9">
<div class="width-100 fltlft">


		<ul class="nav nav-tabs" id="myTab">
                <li class="active"><a data-toggle="tab" href="#import_begin"><?php echo JText::_('COM_CRMERY_IMPORT_BEGIN'); ?></a></li>
                <li><a data-toggle="tab" href="#import_sample_tab"><?php echo JText::_('COM_CRMERY_IMPORT_SAMPLE_DATA'); ?></a></li>
            </ul>
            <?php echo JHtml::_('bootstrap.startPane', 'myTab', array('active' => 'import_begin'));?>
            <?php echo JHtml::_('bootstrap.addPanel', 'myTab', 'import_begin');?>
             
					<div class="step">
						<div class="title">
							<?php echo JText::_('COM_CRMERY_STEP_ONE'); ?>
						</div>
						<div class="text">
							<h2><?php echo JText::_('COM_CRMERY_EXPORT_YOUR_FILE'); ?></h2>
							<p><?php echo JText::_('COM_CRMERY_EXPORT_YOUR_FILE_INSTRUCTIONS'); ?></p>
						</div>
					</div>
					<div class="step">
						<div class="title">
							<?php echo JText::_('COM_CRMERY_STEP_TWO'); ?>
						</div>
						<div class="text">
							<h2><?php echo JText::_('COM_CRMERY_ENSURE_YOUR_FILE_IS_FORMATTED'); ?></h2>
								<p><?php echo JText::_('COM_CRMERY_ENSURE_YOUR_FILE_IS_FORMATTED_INSTRUCTIONS'); ?>
								<form class="inline-form" method="POST">
									<input class="btn" onclick="downloadImportTemplate(this)" type="button" value="<?php echo JText::_('COM_CRMERY_DOWNLOAD_COMPANIES_TEMPLATE'); ?>" />
									<input type="hidden" name="template_type" value="companies" />
								</form>
								<form class="inline-form" method="POST">
									<input class="btn" onclick="downloadImportTemplate(this)" type="button" value="<?php echo JText::_('COM_CRMERY_DOWNLOAD_DEALS_TEMPLATE'); ?>" />
									<input type="hidden" name="template_type" value="deals" />
								</form>
								<form class="inline-form" method="POST">
									<input class="btn" onclick="downloadImportTemplate(this)" type="button" value="<?php echo JText::_('COM_CRMERY_DOWNLOAD_PEOPLE_TEMPLATE'); ?>" />
									<input type="hidden" name="template_type" value="people" />
								</form>
						</div>
					</div>
					<div class="step">
						<div class="title">
							<?php echo JText::_('COM_CRMERY_STEP_THREE'); ?>
						</div>
						<div class="text">
							<h2><?php echo JText::_('COM_CRMERY_UPLOAD_YOUR_FILE'); ?></h2>
							<p><?php echo JText::_('COM_CRMERY_SELECT_YOUR_CSV'); ?>
								<form id="import_form" action="index.php?option=com_crmery&view=import" method="POST" enctype="multipart/form-data">
						        <div class="input_upload_button" >
					        		<label><?php echo JText::_('COM_CRMERY_TYPE'); ?></label>
					        		<?php echo CrmeryHelperDropdown::showImportTypes(); ?>
						        	<input type="hidden" name="type" value="people" />
						            <label><?php echo JText::_('COM_CRMERY_FILE'); ?></label>
						            <input class="input-file" type="file" name="document" />
						            <label></label>
						            <input class="btn btn-primary" type="submit" value="<?php echo JText::_('COM_CRMERY_IMPORT_DATA'); ?>" />
						        </div>
						        </form>
					    	</p>
						</div>
					</div>

            <?php echo JHtml::_('bootstrap.endPanel'); ?>
            	<?php echo JHtml::_('bootstrap.addPanel', 'myTab', 'import_sample_tab');?>

				<div class="sample_text">
            	<h1><?php echo JText::_('COM_CRMERY_INSTALL_SAMPLE_DATA_TITLE'); ?></h1>
                <p><div class="alert alert-info"><?php echo JText::_('COM_CRMERY_INSTALL_SAMPLE_DATA_DESC'); ?></div></p>
	                <form action="<?php echo JRoute::_('index.php?option=com_crmery&view=import'); ?>" method="post" name="adminForm" id="adminForm" class="inline-form"  >
	                    <input type="submit" value="<?php echo JText::_('COM_CRMERY_INSTALL_SAMPLE_BUTTON'); ?>" class="btn btn-primary btn-large" />
	                    <input type="hidden" name="id" value="1" />
	                    <input type="hidden" name="task" value="installSampleData" />
	                    <input type="hidden" name="controller" value="import" />
	                    <input type="hidden" name="layout" value="default" />
	                    <input type="hidden" name="view" value="import" />
	                    <?php echo JHtml::_('form.token'); ?>
	                </form>
	                <form action="<?php echo JRoute::_('index.php?option=com_crmery&view=import'); ?>" method="post" name="adminForm" id="adminForm" class="inline-form"  >
	                    <input type="submit" value="<?php echo JText::_('COM_CRMERY_REMOVE_SAMPLE_BUTTON'); ?>" class="btn btn-danger btn-large" />
	                    <input type="hidden" name="id" value="1" />
	                    <input type="hidden" name="task" value="removeSampleData" />
	                    <input type="hidden" name="controller" value="import" />
	                    <input type="hidden" name="layout" value="default" />
	                    <input type="hidden" name="view" value="import" />
	                    <?php echo JHtml::_('form.token'); ?>
	                </form>
	            </div>
           
            <?php echo JHtml::_('bootstrap.endPanel'); ?>
            <?php echo JHtml::_('bootstrap.endPane', 'myTab'); ?>

</div>
</div>
</div>
</div>
<?php $this->menu['quick_menu']->display(); ?>
					</div>
			</div>