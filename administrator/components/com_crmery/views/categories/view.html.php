<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view' );

class CrmeryViewCategories extends JView
{
    /**
     * display method
     * @return void
     **/
    function display($tpl = null)
    {
        //display title
        JToolBarHelper::title(JText::_('COM_CRMERY_ADMIN').JText::_('COM_CRMERY_NOTE_CATEGORIES'), 'moo');

        /** Menu Links **/
        $menu = CrmeryHelperMenu::getMenuModules();
        $this->assignRef('menu',$menu);
        
        $layout = $this->getLayout();
        
        if ( $layout && $layout == 'edit' ){
            
            JToolbarHelper::cancel('Categories.cancel');
            JToolbarHelper::save('Categories.save');
            
            
        }else{
            
            //buttons
            JToolBarHelper::addNew('Categories.add');
            JToolBarHelper::editList('Categories.edit');
            JToolBarHelper::deleteList(JText::_('COM_CRMERY_CONFIRMATION'),'Categories.remove');
                
            //gather information for view
            $model = JModel::getInstance('categories','CrmeryModel');
            $categories = $model->getCategories();
            $this->assignRef('categories',$categories);
                
            // Initialise state variables.
            $state = $model->getState();
            $this->assignRef('state',$state);
        }

        //pagination
        $this->pagination   = $this->get('Pagination');
        
        //display
        parent::display($tpl);
    }
}
        