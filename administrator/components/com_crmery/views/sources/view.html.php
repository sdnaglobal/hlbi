<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view' );

class CrmeryViewSources extends JView{

    /**
     * display method
     * @return void
     **/
    function display($tpl = null)
    {
        //display title
        JToolBarHelper::title(JText::_('COM_CRMERY_ADMIN').JText::_('COM_CRMERY_SOURCES'), 'moo');

         /** Menu Links **/
        $menu = CrmeryHelperMenu::getMenuModules();
        $this->assignRef('menu',$menu);
        
        $layout = $this->getLayout();
        $this->pagination   = $this->get('Pagination');
        
        if ( $layout && $layout == 'edit' ){
            
            JToolbarHelper::cancel('Sources.cancel');
            JToolbarHelper::save('Sources.save');
            
            //information for view
            $this->source_types = CrmeryHelperDropdown::getSources();
            
        }else{
            
            //buttons
            JToolBarHelper::addNew('Sources.add');
            JToolBarHelper::editList('Sources.edit');
            JToolBarHelper::deleteList(JText::_('COM_CRMERY_CONFIRMATION'),'Sources.remove');
                
            //gather information for view
            $model = JModel::getInstance('sources','CrmeryModel');
            $sources = $model->getSources();
            $this->assignRef('sources',$sources);
                
            // Initialise state variables.
            $state = $model->getState();
            $this->assignRef('state',$state);
        }
        
        //display
        parent::display($tpl);
    }
}