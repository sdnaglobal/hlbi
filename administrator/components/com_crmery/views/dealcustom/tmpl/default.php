<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="container-fluid">
	<?php echo $this->menu['help_menu']->display(); ?>
	<div class="row-fluid">
	<div class="span12" id="content">
	<div id="system-message-container"></div>
		<div class="row-fluid">
				<?php echo $this->menu['menu']->display(); ?>
		<div class="span9">
<div class="width-100 fltlft">
<legend><h3><?php echo JText::_('COM_CRMERY_DEAL_CUSTOM_FIELDS'); ?></h3></legend>
<div class="alert alert-info"><?php echo JText::_('COM_CRMERY_DEAL_CUSTOM_FIELDS_DESC'); ?></div>
<form action="<?php echo JRoute::_('index.php?option=com_crmery&view=dealcustom'); ?>" method="post" name="adminForm" id="adminForm">
    <?php
        $listOrder = $this->state->get('Dealcustom.filter_order');
        $listDirn   = $this->state->get('Dealcustom.filter_order_Dir');
        $saveOrder  = $listOrder == 'c.ordering';
    ?>
 <table class="adminlist">
        <thead>
            <tr>
                <th width="1%">
                    <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
                </th>
                <th>
                    <?php echo JHtml::_('grid.sort',  'COM_CRMERY_HEADER_CUSTOM_NAME', 'c.name', $listDirn, $listOrder); ?>
                </th>
                <th width="10%">
                    <?php echo JHtml::_('grid.sort',  'JGRID_HEADING_ORDERING', 'c.ordering', $listDirn, $listOrder); ?>
                    <?php if ($saveOrder) :?>
                        <?php echo JHtml::_('grid.order',  $this->custom_fields, 'filesave.png', 'dealcustom.saveorder'); ?>
                    <?php endif; ?>
                </th>
                <th>
                    <?php echo JHtml::_('grid.sort',  'COM_CRMERY_HEADER_CUSTOM_TYPE', 'c.type', $listDirn, $listOrder); ?>
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="13">
                    <?php echo $this->pagination->getListFooter(); ?>
                </td>
            </tr>
        </tfoot>
        <tbody>
            <?php if ( count($this->custom_fields) > 0 ) { 
                $ordering   = ($listOrder == 'c.ordering');
                $i=0;
                foreach($this->custom_fields as $key=>$custom){ ?>
                
                <tr class="row<?php echo $i % 2; ?>">
                    <td class="center">
                        <?php echo JHtml::_('grid.id', $key, $custom['id']); ?>
                    </td>
                    <td class="order"><?php echo JHtml::_('link','index.php?option=com_crmery&task=Dealcustom.edit&id='.$custom['id'],$custom['name']); ?></td>
                    <td class="order">
                        <?php if ($saveOrder) :?>
                            <?php if ($listDirn == 'asc') : ?>
                                <span><?php echo $this->pagination->orderUpIcon($i, TRUE, 'dealcustom.orderup', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>
                                <span><?php echo $this->pagination->orderDownIcon($i, $this->pagination->total, TRUE, 'dealcustom.orderdown', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>
                            <?php elseif ($listDirn == 'desc') : ?>
                                <span><?php echo $this->pagination->orderUpIcon($i, TRUE, 'dealcustom.orderdown', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>
                                <span><?php echo $this->pagination->orderDownIcon($i, $this->pagination->total, TRUE, 'dealcustom.orderup', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php $disabled = $saveOrder ?  '' : 'disabled="disabled"'; ?>
                        <input type="text" name="order[]" size="5" value="<?php echo $custom['ordering'];?>" <?php echo $disabled ?> class="text-area-order" />
                    </td>
                    <td class="order"><?php echo ucwords($custom['type']); ?></td>
                </tr>
            
            <?php $i++; } } ?>
        </tbody>
    </table>
    <div>
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>
</div>
</div>
</div>
</div>
<?php $this->menu['quick_menu']->display(); ?>
					</div>
			</div>