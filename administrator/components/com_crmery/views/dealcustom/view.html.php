<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view' );

class CrmeryViewDealcustom extends JView{

    /**
     * display method
     * @return void
     **/
    function display($tpl = null)
    {
        //display title
        JToolBarHelper::title(JText::_('COM_CRMERY_ADMIN').JText::_('COM_CRMERY_DEAL_CUSTOM_FIELDS'), 'moo');

        /** Menu Links **/
        $menu = CrmeryHelperMenu::getMenuModules();
        $this->assignRef('menu',$menu);
        
        $layout = $this->getLayout();
        $this->pagination   = $this->get('Pagination');
        
        if ( $layout && $layout == 'edit' ){
            
            JToolbarHelper::cancel('Dealcustom.cancel');
            JToolbarHelper::save('Dealcustom.save');
            
            //assign view info
            $this->custom_types = CrmeryHelperCustom::getCustomTypes('deal');
            
        }else{
            
            //buttons
            JToolBarHelper::addNew('Dealcustom.add');
            JToolBarHelper::editList('Dealcustom.edit');
            JToolBarHelper::deleteList(JText::_('COM_CRMERY_CONFIRMATION'),'Dealcustom.remove');
                
            //gather information for view
            $model = JModel::getInstance('Dealcustom','CrmeryModel');
            $custom = $model->getCustom();
            $this->assignRef('custom_fields',$custom);
                
            // Initialise state variables.
            $state = $model->getState();
            $this->assignRef('state',$state);
        }
        
        //display
        parent::display($tpl);
    }
}