<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="container-fluid">
    <?php echo $this->side_menu['help_menu']->display(); ?>
    <?php $this->side_menu['quick_menu']->display(); ?>
    <div class="row-fluid">
    <div class="span12" id="content">
    <div id="system-message-container"></div>
        <div class="row-fluid">
                <?php echo $this->side_menu['menu']->display(); ?>
        <div class="span9">
<form action="<?php echo JRoute::_('index.php?option=com_crmery&view=menu'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate"  >
    <div class="width-100 fltlft">
        <!--rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_CUSTOMIZE_CRMERY_MENU_ITEMS'); ?>"-->
        <legend><h3 id="header"><?php echo JText::_('COM_CRMERY_EDIT_MENU'); ?></h3></legend>
        <div class="alert alert-info"><?php echo JText::_('COM_CRMERY_EDIT_MENU_DESC'); ?></div>
            <?php foreach ( $this->menu_template as $menu_item ){ ?>
                <label><b><?php echo JText::_('COM_CRMERY_'.strtoupper($menu_item)); ?></b></label>
                <input type="checkbox" class="inputbox" name="menu_items[]" value="<?php echo $menu_item; ?>" <?php if (is_array($this->menu->menu_items) && in_array($menu_item,$this->menu->menu_items)){ echo 'checked="checked"'; } ?> />
            <?php } ?>    
        <div>
            <input type="hidden" name="id" value="1" />
            <input type="hidden" name="task" value="" />
            <?php echo JHtml::_('form.token'); ?>
        </div>
    </div>
</form>
</div>
</div>
</div>
					</div>
			</div>
			