<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view' );

class CrmeryViewMenu extends JView{

    /**
     * display method
     * @return void
     **/
    function display($tpl = null)
    {
        //display title
        JToolBarHelper::title(JText::_('COM_CRMERY_ADMIN').JText::_('COM_CRMERY_MENU'), 'moo');

        /** Menu Links **/
        $side_menu = CrmeryHelperMenu::getMenuModules();
        
		$this->assignRef('side_menu',$side_menu);
            
        JToolbarHelper::cancel('Menu.cancel');
        JToolbarHelper::save('Menu.save');

        $model = JModel::getInstance('menu','CrmeryModel');
        $menu = $model->getMenu();
        $menu_template = $model->getMenuTemplate();
		
		
        $this->assignRef('menu',$menu);
        $this->assignRef('menu_template',$menu_template);
        
        //display
        parent::display($tpl);
    }
}