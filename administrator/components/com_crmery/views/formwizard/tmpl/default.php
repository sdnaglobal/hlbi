<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="container-fluid">
	<?php echo $this->menu['help_menu']->display(); ?>
	<div class="row-fluid">
	<div class="span12" id="content">
	<div id="system-message-container"></div>
		<div class="row-fluid">
				<?php echo $this->menu['menu']->display(); ?>
		<div class="span9">
            <div class="width-100 fltlft">
            <legend><h3><?php echo JText::_('COM_CRMERY_FORM_WIZARD_HEADER'); ?></h3></legend>
            <form action="<?php echo JRoute::_('index.php?option=com_crmery&view=users'); ?>" method="post" name="adminForm" id="adminForm">
                <?php
                    $listOrder = $this->state->get('Formwizard.filter_order');
                    $listDirn   = $this->state->get('Formwizard.filter_order_Dir');
                ?>
                <table class="adminlist">
                    <thead>
                        <tr>
                            <th width="1%">
                                <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
                            </th>
                            <th>
                                <?php echo JHtml::_('grid.sort',  'COM_CRMERY_FORM_HEADER_NAME', 'f.name', $listDirn, $listOrder); ?>
                            </th>
                            <th><?php echo JText::_('COM_CRMERY_FORM_HEADER_DESCRIPTION'); ?></th>
                            <th><?php echo JText::_('COM_CRMERY_FORM_HEADER_HTML'); ?></th>
                            <th><?php echo JText::_('COM_CRMERY_FORM_HEADER_SHORTCODE'); ?></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <td colspan="13">
                                <!-- pagination -->
                            </td>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php if ( count($this->forms) ) { $i=0; foreach($this->forms as $key=>$form){ ?>
                            
                            <tr class="row<?php echo $i % 2; ?>">
                                <td class="center">
                                    <?php echo JHtml::_('grid.id', $key, $form['id']); ?>
                                </td>
                                <td class="order"><?php echo JHtml::_('link','index.php?option=com_crmery&task=Formwizard.edit&id='.$form['id'],$form['name']); ?></td>
                                <td><?php echo $form['description']; ?></td>
                                <td>
                                    <input onclick="selectTextarea('html_text_<?php echo $form['id']; ?>')" type="button" class="btn-mini btn-primary" data-toggle="modal" href="#form_<?php echo $form['id']; ?>" id="show_fields_button" value="<?php echo JText::_('COM_CRMERY_VIEW_HTML'); ?>" />
                                    <div class="modal hide" id="form_<?php echo $form['id'];?>">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">×</button>
                                            <h3><?php echo JText::_('COM_CRMERY_FORM_HTML'); ?></h3>
                                        </div>
                                        <div class="modal-body">
                                            <textarea rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_FORM_HTML_TOOLTIP'); ?>" wrap="off" cols="20" rows="15" style="width:500px !important;" onclick="selectTextarea(this);" rel="" id="html_text_<?php echo $form['id']; ?>"><?php echo $form['html']; ?></textarea>
                                        </div>
                                        <div class="modal-footer">
                                            <a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>
                                        </div>
                                    </div>
                                </td>
                                <td>[crmeryform<?php echo $form['id']; ?>]</td>
                            </tr>
                        
                        <?php $i++; } } ?>
                    </tbody>
                </table>
                <div>
                    <input type="hidden" name="task" value="" />
                    <input type="hidden" name="boxchecked" value="0" />
                    <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
                    <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
                    <?php echo JHtml::_('form.token'); ?>
                </div>
            </form>
        </div>
</div>
</div>
</div>
<?php $this->menu['quick_menu']->display(); ?>
                    </div>
            </div>