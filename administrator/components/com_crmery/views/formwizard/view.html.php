<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 


jimport( 'joomla.application.component.view' );


class CrmeryViewFormwizard extends JView
{
    
    /**
     * Constructor
     */
    function __construct(){
        parent::__construct();
    }
    
	/**
	 * display method
	 * @return void
	 **/
	function display($tpl = null)
	{
	    //display title
	    JToolBarHelper::title(JText::_('COM_CRMERY_ADMIN').JText::_('COM_CRMERY_FORM_WIZARD_HEADER'), 'moo');
        
        //add toolbar buttons to manage users
        if ( $this->getLayout() == 'default' ){
            //buttons
            JToolBarHelper::addNew('Formwizard.add');
            JToolBarHelper::editList('Formwizard.edit');
            JToolBarHelper::deleteList(JText::_('COM_CRMERY_CONFIRMATION'),'Formwizard.delete');
            
            // Initialise variables.
            $this->state = $this->get('State');
            $model =& JModel::getInstance('formwizard','CrmeryModel');
            $this->forms = $model->getForms();
            
        }
        if ( $this->getLayout() == 'edit' ){
            //buttons
            JToolBarHelper::cancel('Formwizard.cancel');
            JToolBarHelper::save('Formwizard.save');

            $formModel = JModel::getInstance('Formwizard','CRMeryModel');
            $form_id = $formModel->getTempFormId();
            $this->assignRef('form_id',$form_id);
        }

        $document =& JFactory::getDocument();
        $document->addScript(JURI::base().'components/com_crmery/media/js/jquery.base64.js');
        $document->addScript(JURI::base().'components/com_crmery/media/js/formwizard.js');
        
        /** Menu Links **/
        $menu = CrmeryHelperMenu::getMenuModules();
        $this->assignRef('menu',$menu);
	    
	    //display
		parent::display($tpl);
	}
    
}