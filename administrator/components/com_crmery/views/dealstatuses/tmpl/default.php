<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="container-fluid">
	<?php echo $this->menu['help_menu']->display(); ?>
	<div class="row-fluid">
	<div class="span12" id="content">
	<div id="system-message-container"></div>
		<div class="row-fluid">
				<?php echo $this->menu['menu']->display(); ?>
		<div class="span9">
<div class="width-100 fltlft">
<legend><h3><?php echo JText::_('COM_CRMERY_DEAL_STATUSES'); ?></h3></legend>
<div class="alert alert-info"><?php echo JText::_('COM_CRMERY_DEAL_STATUS_DESC_1'); ?><br />
        <?php echo JText::_('COM_CRMERY_PEOPLE_STATUS_DESC_2'); ?></div>
<form action="<?php echo JRoute::_('index.php?option=com_crmery&view=dealstatuses'); ?>" method="post" name="adminForm" id="adminForm">
    <?php
        $listOrder = $this->state->get('Dealstatuses.filter_order');
        $listDirn   = $this->state->get('Dealstatuses.filter_order_Dir');
        $saveOrder  = $listOrder == 's.ordering';
    ?>
 <table class="adminlist">
        <thead>
            <tr>
                <th width="1%">
                    <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
                </th>
                <th style="text-align:left;">
                    <?php echo JHtml::_('grid.sort',  'COM_CRMERY_HEADER_STATUS_NAME', 's.name', $listDirn, $listOrder); ?>
                </th>
                <th width="10%">
                    <?php echo JHtml::_('grid.sort',  'JGRID_HEADING_ORDERING', 's.ordering', $listDirn, $listOrder); ?>
                    <?php if ($saveOrder) :?>
                        <?php echo JHtml::_('grid.order',  $this->statuses, 'filesave.png', 'dealstatuses.saveorder'); ?>
                    <?php endif; ?>
                </th>
                <th>
                    <?php echo JHtml::_('grid.sort',  'COM_CRMERY_HEADER_STATUS_COLOR', 's.color', $listDirn, $listOrder); ?>
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="13">
                    <?php echo $this->pagination->getListFooter(); ?>
                </td>
            </tr>
        </tfoot>
        <tbody>
            <?php if ( count($this->statuses) ) { 
                $ordering   = ($listOrder == 's.ordering');
                $i=0;
                foreach($this->statuses as $key=>$status){ ?>
                
                <tr class="row<?php echo $i % 2; ?>">
                    <td class="center">
                        <?php echo JHtml::_('grid.id', $key, $status['id']); ?>
                    </td>
                    <td style="text-align:left;" class="order"><?php echo JHtml::_('link','index.php?option=com_crmery&task=Dealstatuses.edit&id='.$status['id'],$status['name']); ?></td>
                    <td class="order">
                        <?php if ($saveOrder) :?>
                            <?php if ($listDirn == 'asc') : ?>
                                <span><?php echo $this->pagination->orderUpIcon($i, TRUE, 'dealstatuses.orderup', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>
                                <span><?php echo $this->pagination->orderDownIcon($i, $this->pagination->total, TRUE, 'dealstatuses.orderdown', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>
                            <?php elseif ($listDirn == 'desc') : ?>
                                <span><?php echo $this->pagination->orderUpIcon($i, TRUE, 'dealstatuses.orderdown', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>
                                <span><?php echo $this->pagination->orderDownIcon($i, $this->pagination->total, TRUE, 'dealstatuses.orderup', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php $disabled = $saveOrder ?  '' : 'disabled="disabled"'; ?>
                        <input type="text" name="order[]" size="5" value="<?php echo $status['ordering'];?>" <?php echo $disabled ?> class="text-area-order" />
                    </td>
                    <td class="order"><div class="status_dot" style="background-color: #<?php echo $status['color']; ?>;"></div></td>
                </tr>
            
            <?php $i++; } } ?>
        </tbody>
    </table>
    <div>
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>
</div>
</div>
</div>
</div>
<?php $this->menu['quick_menu']->display(); ?>
					</div>
			</div>