<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 


jimport( 'joomla.application.component.view' );


class CrmeryViewDealstatuses extends JView{

    /**
     * display method
     * @return void
     **/
    function display($tpl = null)
    {
        //display title
        JToolBarHelper::title(JText::_('COM_CRMERY_ADMIN').JText::_('COM_CRMERY_DEAL_STATUSES'), 'moo');

         /** Menu Links **/
        $menu = CrmeryHelperMenu::getMenuModules();
        $this->assignRef('menu',$menu);
        
        $layout = $this->getLayout();
        $this->pagination   = $this->get('Pagination');
        
        if ( $layout && $layout == 'edit' ){
            
            //toolbar buttons
            JToolbarHelper::cancel('Dealstatuses.cancel');
            JToolbarHelper::save('Dealstatuses.save');
            //javascripts
            $document =& JFactory::getDocument();
            $document->addScript(JURI::base().'components/com_crmery/media/js/colorpicker.js');
            //stylesheets
            $document->addStylesheet(JURI::base().'components/com_crmery/media/css/colorpicker.css');
            
        }else{
            
            //buttons
            JToolBarHelper::addNew('Dealstatuses.add');
            JToolBarHelper::editList('Dealstatuses.edit');
            JToolBarHelper::deleteList(JText::_('COM_CRMERY_CONFIRMATION'),'Dealstatuses.remove');
                
            //gather information for view
            $model = JModel::getInstance('dealstatuses','CrmeryModel');
            $statuses = $model->getStatuses();
            $this->assignRef('statuses',$statuses);
                
            // Initialise state variables.
            $state = $model->getState();
            $this->assignRef('state',$state);
        }
        
        //display
        parent::display($tpl);
    }
}