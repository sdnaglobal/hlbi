<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view' );

class CrmeryViewCompanycategories extends JView
{
    /**
     * display method
     * @return void
     **/
    function display($tpl = null)
    {
        //display title
        JToolBarHelper::title(JText::_('COM_CRMERY_ADMIN').JText::_('COM_CRMERY_COMPANY_CATEGORIES'), 'moo');

        /** Menu Links **/
        $menu = CrmeryHelperMenu::getMenuModules();
        $this->assignRef('menu',$menu);
        
        $layout = $this->getLayout();
        $this->pagination = $this->get('Pagination');
        
        if ( $layout && $layout == 'edit' ){
            
            JToolbarHelper::cancel('Companycategories.cancel');
            JToolbarHelper::save('Companycategories.save');
            
            
        }else{
            
            //buttons
            JToolBarHelper::addNew('Companycategories.add');
            JToolBarHelper::editList('Companycategories.edit');
            JToolBarHelper::deleteList(JText::_('COM_CRMERY_CONFIRMATION'),'Companycategories.remove');
                
            //gather information for view
            $model = JModel::getInstance('companycategories','CrmeryModel');
            $categories = $model->getCategories();
            $this->assignRef('categories',$categories);
                
            // Initialise state variables.
            $state = $model->getState();
            $this->assignRef('state',$state);
        }
        
        //display
        parent::display($tpl);
    }
}
        