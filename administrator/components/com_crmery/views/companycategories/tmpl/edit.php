<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="container-fluid">
	<?php echo $this->menu['help_menu']->display(); ?>
	<div class="row-fluid">
	<div class="span12" id="content">
	<div id="system-message-container"></div>
		<div class="row-fluid">
				<?php echo $this->menu['menu']->display(); ?>
		<div class="span9">
<form action="<?php echo JRoute::_('index.php?option=com_crmery&view=companycategories'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate"  >
    <div class="width-100 fltlft">
            <legend><h3><?php echo $this->header; ?></h3></legend>
                    <label><b>Name</b></label>
                    <input type="text" class="inputbox" name="name" value="<?php echo $this->category['name']; ?>" />
                <div>
                    <?php if ( $this->category['id'] ) { ?>
                        <input type="hidden" name="id" value="<?php echo $this->category['id']; ?>" />
                    <?php } ?>
                    <input type="hidden" name="task" value="" />
                    <?php echo JHtml::_('form.token'); ?>
                </div>
    </div>
</form>
</div>
</div>
</div>
<?php $this->menu['quick_menu']->display(); ?>
					</div>
			</div>
			