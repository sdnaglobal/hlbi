<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<script type="text/javascript">
    <?php foreach ( $this->themes as $key=>$row ){
        if ( $row['assigned'] ) echo "var assigned_theme=".$row['id'].";";
    }?>
</script>

<div class="container-fluid">
	<?php echo $this->menu['help_menu']->display(); ?>
	<div class="row-fluid">
	<div class="span12" id="content">
	<div id="system-message-container"></div>
		<div class="row-fluid">
				<?php echo $this->menu['menu']->display(); ?>
		<div class="span9">
<div class="width-100 fltlft">
    <form action="<?php echo JRoute::_('index.php?option=com_crmery&view=branding'); ?>" method="post" name="adminForm" id="adminForm">
    <fieldset class="adminform">
        <legend><h3><?php echo JText::_("COM_CRMERY_PREVIEW"); ?></h3></legend>
            <?php
                //generate html
                $list_html = '<div id="com_crmery"><div id="com_crmery_toolbar"><div id="com_crmery_menu_wrap"><ul class="crmery_menu">';
                foreach ( $this->toolbar_list as $item ) {
                    $list_html .= '<li><a href="#">'.ucwords($item).'</a></li>';
                }
                $list_html .= '</ul></div><div id="user_functions">'.JText::_('COM_CRMERY_CREATE').' <a href="#" />'.JText::_('COM_CRMERY_PROFILE').'</a> Help <a href="#" />Logout</a></div></div></div>';
                //return html
                echo $list_html;
            ?>
            <table cellspacing="0" cellpadding="0" class="com_crmery_table">
                <thead>
                    <th><?php echo JText::_("COM_CRMERY_ADMIN_GENERIC_HEADER"); ?></th>
                    <th><?php echo JText::_("COM_CRMERY_ADMIN_GENERIC_HEADER"); ?></th>
                    <th><?php echo JText::_("COM_CRMERY_ADMIN_GENERIC_HEADER"); ?></th>
                    <th><?php echo JText::_("COM_CRMERY_ADMIN_GENERIC_HEADER"); ?></th>
                </thead>
                <tbody id="deals">
                        <tr class='crmery_row_0'>
                            <td><?php echo JText::_("COM_CRMERY_ADMIN_GENERIC_TEXT"); ?></td>
                            <td><?php echo JText::_("COM_CRMERY_ADMIN_GENERIC_TEXT"); ?></td>
                            <td><?php echo JText::_("COM_CRMERY_ADMIN_GENERIC_TEXT"); ?></td>
                            <td><?php echo JText::_("COM_CRMERY_ADMIN_GENERIC_TEXT"); ?></td>
                        </tr>
                        <tr class='crmery_row_1'>
                            <td><?php echo JText::_("COM_CRMERY_ADMIN_GENERIC_TEXT"); ?></td>
                            <td><?php echo JText::_("COM_CRMERY_ADMIN_GENERIC_TEXT"); ?></td>
                            <td><?php echo JText::_("COM_CRMERY_ADMIN_GENERIC_TEXT"); ?></td>
                            <td><?php echo JText::_("COM_CRMERY_ADMIN_GENERIC_TEXT"); ?></td>
                        </tr>
                </tbody>
            </table>
    </fieldset>
    <fieldset class="adminform">
        <legend><h3><?php echo JText::_("COM_CRMERY_ADMIN_CHOOSE_THEME"); ?></h3></legend>
        <ul class="adminlist crmeryadminlist">
            <li>
                <label><b><?php echo JText::_("COM_CRMERY_ADMIN_STANDARD"); ?></b></label>
                <input type="radio" name="id" value="1" <?php if ( $this->themes[0]['assigned'] ) { echo "checked"; } ?> >
                <label><b><?php echo JText::_("COM_CRMERY_ADMIN_USER_DEFINED"); ?></b></label>
                <input type="radio" name="id" value="2" <?php if ( $this->themes[1]['assigned'] ) { echo "checked"; } ?> >            
            </li>
        </ul>
    </fieldset>
    <fieldset id="customization_area" class="adminform">
        <legend><h3><?php echo JText::_("COM_CRMERY_ADMIN_THEME_CUSTOMIZATION"); ?></h3></legend>
        <div id="customization_content"></div>
    </fieldset>
    <div>
        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
    </form>
    <div id="themes" style="display:none;" >
    <div id="1">
        <ul class="adminlist crmeryadminlist">
            <li>
                <label><b><?php echo JText::_("COM_CRMERY_ADMIN_GENERIC_HEADER"); ?></b></label>
                <input class="inputbox" type="text" name="header" value="<?php echo $this->themes[0]['header']; ?>"><div class="colorwheel"></div>
                <label><b><?php echo JText::_("COM_CRMERY_ADMIN_TABS_HOVER"); ?></b></label>
                <input class="inputbox" type="text" name="tabs_hover" value="<?php echo $this->themes[0]['tabs_hover']; ?>" ><div class="colorwheel"></div>
                <label><b><?php echo JText::_("COM_CRMERY_ADMIN_TABS_HOVER_TEXT"); ?></b></label>
                <input class="inputbox" type="text" name="tabs_hover_text" value="<?php echo $this->themes[0]['tabs_hover_text']; ?>" ><div class="colorwheel"></div>
                <label><b><?php echo JText::_("COM_CRMERY_ADMIN_TABLE_HEADER_ROW"); ?></b></label>
                <input class="inputbox" type="text" name="table_header_row" value="<?php echo $this->themes[0]['table_header_row']; ?>" ><div class="colorwheel"></div>
                <label><b><?php echo JText::_("COM_CRMERY_ADMIN_TABLE_HEADER_TEXT"); ?></b></label>
                <input class="inputbox" type="text" name="table_header_text" value="<?php echo $this->themes[0]['table_header_text']; ?>" ><div class="colorwheel"></div>
                <label><b><?php echo JText::_("COM_CRMERY_ADMIN_LINK_COLOR"); ?></b></label>
                <input class="inputbox" type="text" name="link" value="<?php echo $this->themes[0]['link']; ?>" ><div class="colorwheel"></div>
                <label><b><?php echo JText::_("COM_CRMERY_ADMIN_LINK_HOVER"); ?></b></label>
                <input class="inputbox" type="text" name="link_hover" value="<?php echo $this->themes[0]['link_hover']; ?>" ><div class="colorwheel"></div>
            </li>
        </ul>
    </div>
    <div id="2">
        <ul class="adminlist crmeryadminlist">
           <li>
                <label><b><?php echo JText::_("COM_CRMERY_ADMIN_GENERIC_HEADER"); ?></b></label>
                <input class="inputbox" type="text" name="header" value="<?php echo $this->themes[1]['header']; ?>"><div class="colorwheel"></div>
                <label><b><?php echo JText::_("COM_CRMERY_ADMIN_TABS_HOVER"); ?></b></label>
                <input class="inputbox" type="text" name="tabs_hover" value="<?php echo $this->themes[1]['tabs_hover']; ?>" ><div class="colorwheel"></div>
                <label><b><?php echo JText::_("COM_CRMERY_ADMIN_TABS_HOVER_TEXT"); ?></b></label>
                <input class="inputbox" type="text" name="tabs_hover_text" value="<?php echo $this->themes[1]['tabs_hover_text']; ?>" ><div class="colorwheel"></div>
                <label><b><?php echo JText::_("COM_CRMERY_ADMIN_TABLE_HEADER_ROW"); ?></b></label>
                <input class="inputbox" type="text" name="table_header_row" value="<?php echo $this->themes[1]['table_header_row']; ?>" ><div class="colorwheel"></div>
                <label><b><?php echo JText::_("COM_CRMERY_ADMIN_TABLE_HEADER_TEXT"); ?></b></label>
                <input class="inputbox" type="text" name="table_header_text" value="<?php echo $this->themes[1]['table_header_text']; ?>" ><div class="colorwheel"></div>
                <label><b><?php echo JText::_("COM_CRMERY_ADMIN_LINK_COLOR"); ?></b></label>
                <input class="inputbox" type="text" name="link" value="<?php echo $this->themes[1]['link']; ?>" ><div class="colorwheel"></div>
                <label><b><?php echo JText::_("COM_CRMERY_ADMIN_LINK_HOVER"); ?></b></label>
                <input class="inputbox" type="text" name="link_hover" value="<?php echo $this->themes[1]['link_hover']; ?>" ><div class="colorwheel"></div>
            </li>
        </ul>
        </div>
    </div>
</div>
</div>
</div>
</div>
<?php $this->menu['quick_menu']->display(); ?>
					</div>
			</div>