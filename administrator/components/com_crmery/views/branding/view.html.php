<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view' );

class CrmeryViewBranding extends JView
{
    /**
     * display method
     * @return void
     **/
    function display($tpl = null)
    {
        //display title
        JToolBarHelper::title(JText::_('COM_CRMERY_ADMIN').JText::_('COM_CRMERY_BRANDING'), 'moo');

        /** Menu Links **/
        $menu = CrmeryHelperMenu::getMenuModules();
        $this->assignRef('menu',$menu);
        
         //add javascript
        $document =& JFactory::getDocument();
        $document->addScript(JURI::base().'components/com_crmery/media/js/branding_manager.js');
        $document->addScript(JURI::base().'components/com_crmery/media/js/colorpicker.js');
        //stylesheets
        $document->addStylesheet(JURI::base().'components/com_crmery/media/css/colorpicker.css');
        CrmeryHelperStyles::loadStyleSheets();
        
        
        //view refs
        $model =& $this->getModel('Branding');
        $themes = $model->getThemes();
        //toolbar items
        $list = array( 
            'dashboard',
            'deals',
            'people',
            'companies',
            'calendar',
            'documents',
            'goals',
            'reports'
        );
        $this->assignRef('toolbar_list',$list);
        $this->assignRef('themes',$themes);
        
        //save button
        JToolbarHelper::save('Branding.save','Save');
        
        //display
        parent::display($tpl);
    }
}