<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 


jimport( 'joomla.application.component.view' );


class CrmeryViewUsers extends JView
{
    
    /**
     * Constructor
     */
    function __construct(){
        parent::__construct();
    }
    
	/**
	 * display method
	 * @return void
	 **/
	function display($tpl = null)
	{
	    //display title
	    JToolBarHelper::title(JText::_('COM_CRMERY_ADMIN').JText::_('COM_CRMERY_USERS'), 'moo');
        $document =& JFactory::getDocument();

        
        //add toolbar buttons to manage users
        if ( $this->getLayout() == 'default' ){
            //buttons
            JToolBarHelper::addNew('Users.add');
            JToolBarHelper::editList('Users.edit');
            JToolBarHelper::deleteList(JText::_('COM_CRMERY_CONFIRMATION'),'Users.delete');
            
            // Initialise variables.
            $this->state = $this->get('State');
            
            //get users
            $model = $this->getModel();
            $users = $model->getUsers();
            
            //assign refs
            $this->assignRef('member_roles',$roles);
            $this->assignRef('users',$users);
        }
        
        if ( $this->getLayout() == 'edit' ){
            //buttons
            JToolBarHelper::cancel('Users.cancel');
            JToolBarHelper::save('Users.save');
            
            $id = JRequest::getVar('id');
            $dispatcher =& JDispatcher::getInstance();
            $dispatcher->trigger('onBeforeCRMUserEdit', array(&$id));    

            //view data
            $roles = CrmeryHelperDropdown::getMemberRoles();
            $teamId = CrmeryHelperUsers::getTeamId(JRequest::getVar('id'));
            $teams = CrmeryHelperDropdown::getTeams($teamId);
            $managers = CrmeryHelperDropdown::getManagers(JRequest::getVar('id'));
			$regions = CrmeryHelperDropdown::getRegions();
			$countries = CrmeryHelperDropdown::getCountry();
			$states = CrmeryHelperDropdown::getState();
			
            $this->assignRef('member_roles',$roles);
            $this->assignRef('teams',$teams);
            $this->assignRef('managers',$managers);
			$this->assignRef('regions',$regions);
			$this->assignRef('countries',$countries);
			$this->assignRef('states',$states);
			
        }

        $document->addScript(JURI::base().'components/com_crmery/media/js/colorpicker.js');
        //stylesheets
        $document->addStylesheet(JURI::base().'components/com_crmery/media/css/colorpicker.css');
        
        /** Menu Links **/
        $menu = CrmeryHelperMenu::getMenuModules();
        $this->assignRef('menu',$menu);
	    
	    //display
		parent::display($tpl);
	}
    
}