<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="container-fluid">
	<?php echo $this->menu['help_menu']->display(); ?>
	<div class="row-fluid">
	<div class="span12" id="content">
	<div id="system-message-container"></div>
		<div class="row-fluid">
				<?php echo $this->menu['menu']->display(); ?>
		<div class="span9">
            <div class="width-100 fltlft">
            <legend><h3><?php echo JText::_('COM_CRMERY_ADD_TO_ACCOUNT'); ?></h3></legend>
            <div class="alert alert-info">
            <h3><?php echo JText::_("COM_CRMERY_USER_ROLES_VISIBILITY"); ?></h3>
                <b><?php echo JText::_('COM_CRMERY_FEDRATIONADMIN'); ?></b> - <?php echo JText::_('COM_CRMERY_EXECUTIVE_DESC'); ?><br />
                <b><?php echo JText::_('COM_CRMERY_FIRMADMIN'); ?></b> - <?php echo JText::_('COM_CRMERY_MANAGERS_DESC'); ?><br />
                <b><?php echo JText::_('COM_CRMERY_FIRMINPUTER'); ?></b> - <?php echo JText::_('COM_CRMERY_BASIC_USERS_DESC'); ?>
            </div>
            <form action="<?php echo JRoute::_('index.php?option=com_crmery&view=users'); ?>" method="post" name="adminForm" id="adminForm">
                <?php
                    $listOrder = $this->state->get('Users.filter_order');
                    $listDirn   = $this->state->get('Users.filter_order_Dir');
                ?>
                <table class="adminlist">
                    <thead>
                        <tr>
                            <th width="1%">
                                <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
                            </th>
                            <th>
                                <?php echo JHtml::_('grid.sort',  'COM_CRMERY_USERS_HEADER_NAME', 'u.last_name', $listDirn, $listOrder); ?>
                            </th>
                            <th>
                                <?php echo JHtml::_('grid.sort', 'COM_CRMERY_USERS_HEADER_USERNAME', 'ju.username', $listDirn, $listOrder); ?>
                            </th>
                            <th>
                                <?php echo JHtml::_('grid.sort', 'COM_CRMERY_USERS_HEADER_TEAM', 'u.team_id', $listDirn, $listOrder); ?>
                            </th>
                            <th>
                                <?php echo JHtml::_('grid.sort', 'COM_CRMERY_USERS_HEADER_EMAIL', 'ju.email', $listDirn, $listOrder); ?>
                            </th>
                            <th>
                                <?php echo JHtml::_('grid.sort', 'COM_CRMERY_USERS_HEADER_ROLE', 'u.role_type', $listDirn, $listOrder); ?>
                            </th>
                            <th>
                                <?php echo JHtml::_('grid.sort',  'COM_CRMERY_USERS_HEADER_LOGIN', 'ju.lastvisitDate', $listDirn, $listOrder); ?>
                            </th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <td colspan="13">
                                <!-- pagination -->
                            </td>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php if ( count($this->users) ) { foreach($this->users as $key=>$user){ ?>
                            
                            <tr class="row<?php echo $key % 2; ?>">
                                <td class="center">
                                    <?php echo JHtml::_('grid.id', $key, $user['id']); ?>
                                </td>
                                <td class="order"><?php echo JHtml::_('link','index.php?option=com_crmery&task=Users.edit&id='.$user['id'],$user['first_name'].' '.$user['last_name']); ?></td>
                                <td class="order"><?php echo $user['username']; ?></td>
                                <td class="order">
                                    <?php
                                    if ( array_key_exists('team_id',$user) && $user['team_id'] ){ 
                                        echo $user['team_name'].JText::_("COM_CRMERY_TEAM_APPEND");
                                    }
                                    ?>
                                </td>
                                <td class="order"><?php echo $user['email']; ?></td>
                                <td class="order">
								<?php
									if($user['role_type'] == 'manager')
										echo 'Firm Admin';
									elseif($user['role_type'] == 'basic')
										echo 'Firm Inputer';	
									elseif($user['role_type'] == 'exec')
										echo 'Federation Admin';
									else 
										echo 'HLB Admin';
										
										
									//echo ucwords($user['role_type']); ?>
								
								</td>
                                <td class="order"><?php echo date("F j,Y g:iA",strtotime($user['last_login'])); ?></td>
                            </tr>
                        
                        <?php }} ?>
                    </tbody>
                </table>
                <div>
                    <input type="hidden" name="task" value="" />
                    <input type="hidden" name="boxchecked" value="0" />
                    <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
                    <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
                    <?php echo JHtml::_('form.token'); ?>
                </div>
            </form>
        </div>
</div>
</div>
</div>
<?php $this->menu['quick_menu']->display(); ?>
                    </div>
            </div>