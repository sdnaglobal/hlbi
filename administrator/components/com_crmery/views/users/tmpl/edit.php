<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

JHTML::_( 'behavior.formvalidation' ); ?>

<script type="text/javascript">
    var users = <?php echo json_encode($this->users); ?>;
    <?php if ( $this->user['id'] ){ ?>
    var role_type = '<?php echo $this->user['role_type']; ?>';
    <?php }else{ ?>
    var role_type = null;
    <?php } ?>
</script>

<div class="container-fluid">
    <?php echo $this->menu['help_menu']->display(); ?>
    <div class="row-fluid">
    <div class="span12" id="content">
    <div id="system-message-container"></div>
        <div class="row-fluid">
                <?php echo $this->menu['menu']->display(); ?>
        <div class="span9">
    <form action="<?php echo JRoute::_('index.php?option=com_crmery&view=users'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate" >
        <div class="width-100 fltlft">
            <legend><h2><?php echo $this->header; ?></h2></legend>
            <ul class="adminformlist crmeryadminlist">
                <?php if ( !$this->user['id'] ) { ?>
                    <li>
                        <label><b><?php echo JText::_('COM_CRMERY_SELECT_USER_TO_ADD'); ?></b></label>
                        <input type="text" id="uid_name" name="uid_name" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_START_TYPING_JOOMLA_NAME'); ?>">
                        <input class="required" type="hidden" name="uid" id="uid" />
                    </li>
                <?php } ?>
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_FIRST_NAME'); ?></b></label>
                    <input class="inputbox required" type="text" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_ENTER_FIRST_NAME_HERE'); ?>" name="first_name" value="<?php echo $this->user['first_name']; ?>" />
                </li>
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_LAST_NAME'); ?></b></label>
                    <input class="inputbox" type="text" name="last_name" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_ENTER_LAST_NAME_HERE'); ?>" value="<?php echo $this->user['last_name']; ?>" />
                </li>
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_EMAIL'); ?></b><br /><span class="label_details"><?php echo JText::_('COM_CRMERY_EDIT_USER_EMAIL'); ?></span></label>
                    <input disabled="disabled" type="text" id="email" class="inputbox" name="email" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_EDIT_USER_EMAIL'); ?>" value="<?php echo $this->user['email']; ?>" />
                </li>
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_USERNAME'); ?></b><br /><span class="label_details"><?php echo JText::_('COM_CRMERY_EDIT_USER_USERNAME'); ?></span></label>
                    <input disabled="disabled" type="text" id="username" class="inputbox" name="username" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_EDIT_USER_USERNAME'); ?>" value="<?php echo $this->user['username']; ?>" />
                </li>
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_MEMBER_ROLE'); ?></b></label>
                    <select class="inputbox" name="role_type" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_ASSIGN_USER_ROLE'); ?>" onchange="updateRole(this.value)" >
                        <?php echo JHtml::_('select.options', $this->member_roles, 'value', 'text', $this->user['role_type'], true);?>
                    </select>
                    <div id="team_name" <?php if ( $this->user['role_type'] != "manager"){ ?> style="display:none;" <?php } ?> >
                        <label><b><?php echo JText::_('COM_CRMERY_TEAM_NAME'); ?></b></label>
                        <input type="text" class="inputbox" name="team_name" value="<?php if ( isset($this->user) ) echo $this->user['team_name']; ?>" />							  
						
						
						<?php   //get Firm List customization 
							$db =& JFactory::getDBO();
							$query = $db->getQuery(true);
							//query string
							$query->select('*');
							$query->from("#__crmery_companies");
							//$query->where("u.role_type='manager'");

							 $query->where("published=1");
							$query->order('name ASC');
							//get results
							$db->setQuery($query);
							$results = $db->loadAssocList();
							//generate users object
							$firmss = array();
							foreach ( $results as $key=>$firm ){
							   // if ( $user['id'] == $remove ){
								  //  unset($results[$key]);
								//}else{
									$firmss[$firm['id']] = $firm['name'];
								//}
							}                        
							
                         ?>  
					   
								
						<label><b><?php echo JText::_('COM_CRMERY_USERS_ASSIGN_FIRM'); ?></b></label>
                        <select class="inputbox" id="company_id" name="company_id" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_ASSIGN_FIRM_TOFIRMADMIN'); ?>"  >
                                <option value="0"><?php echo JText::_("COM_CRMERY_SELECT_FIRM"); ?></option>
                               <?php 
							   
								  $reg=$firmss;							
								 // sort($reg);							
                                  $companyId=$this->user['company_id'];  								
								  foreach($reg as $key=>$value)
								  { ?>
								    <option value="<?php echo $key;?>" <?php if($companyId==$key){ echo "selected=Selected";} ?>><?php echo $value ?></option>
								 <?php }							
								
								//echo JHtml::_('select.options', $this->teams, 'value', 'text', $this->user['team_id'], true);?>
                        </select>				
						
                    </div>
                </li>
                <?php if ( $this->user['role_type'] == 'basic' ){  ?>
                <li id="team_assignment">   

				
                <?php } else { ?>
                <li style="display:none;" id="team_assignment">        
                <?php } ?>
                    <label><b><?php echo JText::_('COM_CRMERY_USERS_HEADER_TEAM'); ?></b></label>
                        <select class="inputbox" id="team_id" name="team_id" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_ASSIGN_USER_TEAM'); ?>"  >
                                <option value="0"><?php echo JText::_("COM_CRMERY_NONE"); ?></option>
                                <?php echo JHtml::_('select.options', $this->teams, 'value', 'text', $this->user['team_id'], true);?>
                        </select>							
							
							<?php   //get Firm List customization 
							$db =& JFactory::getDBO();
							$query = $db->getQuery(true);
							//query string
							$query->select('*');
							$query->from("#__crmery_companies");
							$query->where("published=1");
							
							//get results
							$db->setQuery($query);
							$results = $db->loadAssocList();
							//generate users object
							$firms_inputer = array();
							foreach ( $results as $key=>$firmInp ){
							   // if ( $user['id'] == $remove ){
								  //  unset($results[$key]);
								//}else{
									$firms_inputer[$firmInp['id']] = $firmInp['name'];
								//}
							}                        
							
                         ?>  
					   
								
						<label><b><?php echo JText::_('COM_CRMERY_USERS_ASSIGN_FIRM'); ?></b></label>
                        <select class="inputbox" id="inp_company_id" name="inp_company_id" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_ASSIGN_FIRM_TOFIRMADMIN'); ?>"  >
                                <option value="0"><?php echo JText::_("COM_CRMERY_SELECT_FIRM"); ?></option>
                               <?php 
							   
								  $reg=$firms_inputer;							
                                  $compId=$this->user['inp_company_id'];  								
								  foreach($reg as $key=>$valInp)
								  { ?>
								    <option value="<?php echo $key;?>" <?php if($compId==$key){ echo "selected=Selected";} ?>><?php echo $valInp ?></option>
								 <?php }							
								
								//echo JHtml::_('select.options', $this->teams, 'value', 'text', $this->user['team_id'], true);?>
                        </select>							
						
                </li>
                <?php if ( $this->user['role_type'] == 'manager' ){ ?>
                <li style="display:none;" id="manager_assignment">
                    <label><b><?php echo JText::_('COM_CRMERY_ASSIGN_NEW_MANAGER'); ?><span class="required">*</span></b></label>
                        <select class="inputbox" id="manager_id" name="manager_assignment" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_ASSIGN_MANAGER'); ?>"  >
                                <option value=""><?php echo JText::_('COM_CRMERY_NEW_MANAGER'); ?></option>
                                <?php echo JHtml::_('select.options', $this->managers, 'value', 'text', '', true);?>
                        </select>	
						
						
                </li>
                <?php } ?>
				
			
				
                <li>
                    <label><b><?php echo JText::_("COM_CRMERY_ADMNISTRATOR"); ?></b></label>
                    <input type="checkbox" name="admin" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_IF_CHECKED_ADMINISTRATOR'); ?>" <?php echo ($this->user['admin'] ? 'checked' : ''); ?> />
                </li>
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_ALLOWED_TO_DELETE'); ?></b></label>
                    <input type="checkbox" name="can_delete" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_IF_CHECKED_DELETE'); ?>" <?php echo ($this->user['can_delete'] ? 'checked' : ''); ?> />
                </li>
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_ALLOWED_TO_EXPORT'); ?></b></label>
                    <input type="checkbox" name="exports" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_IF_CHECKED_EXPORT'); ?>" <?php echo ($this->user['exports'] ? 'checked' : ''); ?> />
                </li>
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_USER_COLOR'); ?></b></label>
                    <input id="user_color" type="text" class="" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_USER_COLOR'); ?>" name="color" value="<?php echo $this->user['color']; ?>" />
                </li>
				
				<?php if($this->user['role_type']=="exec"){ $newstyle = 'style="display:block;"';}else{  $newstyle = 'style="display:block;"';}?>
				 <?php if (!$this->user['id'] ) { $newstyle = 'style="display:block;"';}?>
				<li  > <div id="regionBlock" <?php echo $newstyle?> >
                    <label><b><?php echo JText::_('COM_CRMERY_REGION'); ?></b></label>
                     <select class="inputbox" id="region_id" name="region_id[]" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_ASSIGN_REGION'); ?>" multiple >
                                <option value=""><?php echo JText::_('COM_CRMERY_SELECT_REGION'); ?></option>
                                <?php
                                  $reg=$this->regions;
								  
								  $regions=explode(",",$this->user['region_id']);
								  $i=0;
								  
								  foreach($reg as $key=>$value)
								  {  ?>
								    <option value="<?php echo $key;?>" <?php for($i=0;$i<count($regions);$i++){ if($regions[$i]==$key){ echo "selected=Selected";} }?>><?php echo $value ?></option>
								 <?php $i++;}
   
								//echo JHtml::_('select.options', $this->regions, 'value', 'text', '', true);?>
                     </select>
					</div> 					 
				</li>
				
				
				<li  > <div id="countryBlock" <?php echo $newstyle?> >
                    <label><b><?php echo JText::_('COM_CRMERY_COUNTRY'); ?></b></label>
                     <select class="inputbox" id="country_id" name="country_id[]" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_ASSIGN_COUNTRY'); ?>" multiple>
                                <option value=""><?php echo JText::_('COM_CRMERY_SELECT_COUNTRY'); ?></option>
                                <?php
                                  $reg=$this->countries;
								  $country=explode(",",$this->user['country_id']);
								  $i=0;
								  
								  foreach($reg as $key=>$value)
								  { 							     
								  ?>
								    <option value="<?php echo $key;?>" <?php for($i=0;$i<count($country);$i++){ if($country[$i]==$key){ echo "selected=Selected";} }?>><?php echo $value ?></option>
								 <?php $i++; }
   
								//echo JHtml::_('select.options', $this->regions, 'value', 'text', '', true);?>
                     </select> 
                      </div>					 
				</li>
               <!-- <li> <div id="stateBlock">
                    <label><b><?php echo JText::_('COM_CRMERY_STATE'); ?></b></label>
                     <select class="inputbox" id="state_id" name="state_id[]" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_ASSIGN_STATE'); ?>" multiple>
                                <option value=""><?php echo JText::_('COM_CRMERY_SELECT_STATE'); ?></option>
                                <?php
                                  $reg=$this->states;
								  $state=explode(",",$this->user['state_id']);
								  $i=0;
								  foreach($reg as $key=>$value)
								  { ?>
								    <option value="<?php echo $key;?>" <?php for($i=0;$i<count($state);$i++){ if($state[$i]==$key){ echo "selected=Selected";} }?>><?php echo $value ?></option>
								 <?php $i++; }
   
								//echo JHtml::_('select.options', $this->regions, 'value', 'text', '', true);?>
                     </select>				 
				</li>-->
				
			
				
				<?php // }  ?>
				 
				
				
            </ul>
			
			<?php  ?>
			
        <div>
            <?php if ( $this->user['id'] ) { ?>
                <input type="hidden" name="id" value="<?php echo $this->user['id']; ?>" />
            <?php } ?>
            <input type="hidden" name="task" value="" />
            <?php echo JHtml::_('form.token'); ?>
        </div>

        </div>
    </form>
</div>
</div>
</div>
<?php $this->menu['quick_menu']->display(); ?>
					</div>
			</div>