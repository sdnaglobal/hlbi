<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="container-fluid">
	<?php echo $this->menu['help_menu']->display(); ?>
	<div class="row-fluid">
	<div class="span12" id="content">
	<div id="system-message-container"></div>
		<div class="row-fluid">
				<?php echo $this->menu['menu']->display(); ?>
		<div class="span9">
<div class="width-100 fltlft">
    <legend><h3><?php echo JText::_('COM_CRMERY_TASK_PEOPLE_TEMPLATES'); ?></h3></legend>
<div class="alert alert-info"><?php echo JText::_('COM_CRMERY_TASK_PEOPLE_TEMPLATES_DESC_1'); ?></div>
<form action="<?php echo JRoute::_('index.php?option=com_crmery&view=templates'); ?>" method="post" name="adminForm" id="adminForm">
    <?php
        $listOrder = $this->state->get('Templates.filter_order');
        $listDirn   = $this->state->get('Templates.filter_order_Dir');
    ?>
 <table class="adminlist">
        <thead>
            <tr>
                <th width="1%">
                    <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
                </th>
                <th>
                    <?php echo JHtml::_('grid.sort',  'COM_CRMERY_HEADER_TEMPLATE_NAME', 't.name', $listDirn, $listOrder); ?>
                </th>
                <th>
                    <?php echo JHtml::_('grid.sort',  'COM_CRMERY_HEADER_TEMPLATE_TYPE', 't.type', $listDirn, $listOrder); ?>
                </th>
                <th>
                    <?php echo JHtml::_('grid.sort',  'COM_CRMERY_HEADER_TEMPLATE_CREATED', 't.created', $listDirn, $listOrder); ?>
                </th>
                <th>
                    <?php echo JHtml::_('grid.sort',  'COM_CRMERY_HEADER_TEMPLATE_MODIFIED', 't.modified', $listDirn, $listOrder); ?>
                </th>
                <th>
                    <?php echo JHtml::_('grid.sort',  'COM_CRMERY_HEADER_TEMPLATE_DEFAULT', 't.default', $listDirn, $listOrder); ?>
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="13">
                    <!-- pagination -->
                </td>
            </tr>
        </tfoot>
        <tbody>
            <?php if ( count($this->templates) ) { foreach($this->templates as $key=>$template){ ?>
                
                <tr class="row<?php echo $key % 2; ?>">
                    <td class="center">
                        <?php echo JHtml::_('grid.id', $key, $template['id']); ?>
                    </td>
                    <td class="order"><?php echo JHtml::_('link','index.php?option=com_crmery&task=Templates.edit&id='.$template['id'],$template['name']); ?></td>
                    <td class="order"><?php echo ucwords($template['type']); ?></td>
                    <td class="order"><?php echo $template['created']; ?></td>
                    <td class="order"><?php echo $template['modified']; ?></td>
                    <td class="order"><?php if($template['default']) echo "Default"; ?></td>
                </tr>
            
            <?php }} ?>
        </tbody>
    </table>
    <div>
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>
</div>
</div>
</div>
</div>
<?php $this->menu['quick_menu']->display(); ?>
					</div>
			</div>