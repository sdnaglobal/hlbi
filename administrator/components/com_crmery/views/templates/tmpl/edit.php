<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="container-fluid">
	<?php echo $this->menu['help_menu']->display(); ?>
	<div class="row-fluid">
	<div class="span12" id="content">
	<div id="system-message-container"></div>
		<div class="row-fluid">
				<?php echo $this->menu['menu']->display(); ?>
		<div class="span9">
<form action="<?php echo JRoute::_('index.php?option=com_crmery&view=templates'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate"  >
    <div class="width-100 fltlft">
        <fieldset class="adminform">
        <legend><h3><?php echo $this->header; ?></h3></legend>
            <ul class="adminformlist crmeryadminlist">
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_NAME'); ?></b></label>
                    <input type="text" class="inputbox" name="name" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_NAME_YOUR_WORKFLOW'); ?>" value="<?php echo $this->template['name']; ?>" />
                </li>
                <li>
                    <span class="crmeryfaux-label"><b><?php echo JText::_('COM_CRMERY_HEADER_SOURCE_TYPE'); ?></b></span>
                    <span class="crmeryfield"><b><?php echo JText::_('COM_CRMERY_DEAL'); ?></b></span>
                    <span class="crmeryfield"><input type="radio" name="type" value="deal" <?php if($this->template['type']=='deal') echo 'checked'; ?> /></span>
                    <span class="crmeryfield"><b><?php echo JText::_('COM_CRMERY_PERSON'); ?></b></span>
                    <span class="crmeryfield"><input type="radio" name="type" value="person" <?php if($this->template['type']=='person') echo 'checked'; ?>/></span>
                    <span class="crmeryfield"><b><?php echo JText::_('COM_CRMERY_COMPANY'); ?></b></span><span class="crmeryfield"><input type="radio" name="type" value="company" <?php if($this->template['type']=='company') echo 'checked'; ?>/></span>    
                </li>
                <li>
                    <span class="crmeryfaux-label"></span>
                    <span class="crmeryfield"></span>
                </li>
                <li>
                    <label><b><?php echo JText::_("COM_CRMERY_MAKE_DEFAULT_TEMPLATE"); ?></b></label>
                    <input type="checkbox" name="default" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_NAME_YOUR_WORKFLOW'); ?>"  <?php if($this->template['default']) echo 'checked'; ?> />
                </li>
                <li>
            </ul>
        </fieldset>
    </div>
    <div class="width-100 fltlft">
        <fieldset class="adminform">
            <legend><h3><?php echo JText::_("COM_CRMERY_ENTER_ITEMS"); ?></h3></legend>
            <ul class="adminformlist crmeryadminlist">
                <li>
                <div id="items">
                <?php if ( count($this->template['data']) ){ foreach($this->template['data'] as $data){ ?>
                    <span class="crmeryfield clrboth">
                    <div class="item">
                        <table>
                            <tr>
                                    <input type="hidden" name="items[]" value ="<?php echo $data['id']; ?>]" />
                                    <td><b><?php echo JText::_('COM_CRMERY_NAME'); ?></b></td>
                                    <td><input class="inputbox" type="text" name="names[]" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_EVENT_NAME_TOOLTIP'); ?>" value="<?php echo $data['name']; ?>" /></td>
                                    <td><b><?php echo JText::_("COM_CRMERY_DAY"); ?></b></td>
                                    <td><input class="inputbox" type="text" name="days[]" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_EVENT_DAY_TOOLTIP'); ?>" value="<?php echo $data['day']; ?>" /></td>
                                    <td><b><?php echo JText::_("COM_CRMERY_HEADER_SOURCE_TYPE"); ?></b></td>
                                    <td>
                                        <select class="inputbox" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_EVENT_TYPE_TOOLTIP'); ?>" name="types[]">
                                            <option value=""><?php echo JText::_('COM_CRMERY_SELECT_EVENT_TYPE'); ?></option>
                                              <?php echo JHtml::_('select.options', $this->template_types, 'value', 'text', $data['type'], true);?>
                                        </select>
                                    </td>
                                    <td><a class="remove_item"><?php echo JText::_("COM_CRMERY_REMOVE"); ?></a></td>
                            </tr>
                        </table>
                    </div>
                    </span>
                <?php } 
                    }else{ ?>
                        <span class="crmeryfield clrboth">
                        <div class="item">
                            <table>
                                <tr>
                                        <input type="hidden" name="items[]" value ="" />
                                        <td><b><?php echo JText::_('COM_CRMERY_NAME'); ?></b></td>
                                        <td><input class="inputbox" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_EVENT_NAME'); ?>" type="text" name="names[]" value="" /></td>
                                        <td><b><?php echo JText::_('COM_CRMERY_DAY'); ?></b></td>
                                        <td><input class="inputbox" type="text" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_EVENT_DAY'); ?>" name="days[]" value="" /></td>
                                        <td><b><?php echo JText::_('COM_CRMERY_HEADER_SOURCE_TYPE'); ?></b></td>
                                        <td>
                                            <select class="inputbox" name="types[]" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_EVENT_TYPE'); ?>" >
                                                <option value=""><?php echo JText::_('COM_CRMERY_SELECT_EVENT_TYPE'); ?></option>
                                                  <?php echo JHtml::_('select.options', $this->template_types, 'value', 'text', '', true);?>
                                            </select>
                                        </td>
                                        <td><a class="remove_item"><?php echo JText::_("COM_CRMERY_REMOVE"); ?></a></td>
                                </tr>
                            </table>
                        </div>
                        </span>  
                    <?php } ?>
                    </div>
                    </li>
                    <li>
                        <label><a id="add_item"><b>Add More Items</a></b></label>
                    </li>
            </ul>
        </fieldset>
    </div>
    <div>
        <?php if ( $this->template['id'] ) { ?>
            <input type="hidden" name="id" value="<?php echo $this->template['id']; ?>" />
        <?php } ?>
        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>
<div style="display:none;" id="item_template">
    <span class="crmeryfield clrboth">
    <div class="item">
        <table>
            <tr>
                    <input type="hidden" name="items[]" value ="" />
                    <td><b><?php echo JText::_('COM_CRMERY_NAME'); ?></b></td>
                    <td><input class="inputbox" type="text" name="names[]" value="" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_EVENT_NAME'); ?>"  /></td>
                    <td><b><?php echo JText::_('COM_CRMERY_DAY'); ?></b></td>
                    <td><input class="inputbox" type="text" name="days[]" value="" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_EVENT_DAY'); ?>"  /></td>
                    <td><b><?php echo JText::_('COM_CRMERY_HEADER_SOURCE_TYPE'); ?></b></td>
                    <td>
                        <select class="inputbox" name="types[]" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_EVENT_TYPE'); ?>" >
                            <option value=""><?php echo JText::_('COM_CRMERY_SELECT_EVENT_TYPE'); ?></option>
                              <?php echo JHtml::_('select.options', $this->template_types, 'value', 'text', '', true);?>
                        </select>
                    </td>
                    <td><a class="remove_item"><?php echo JText::_("COM_CRMERY_REMOVE"); ?></a></td>
            </tr>
        </table>
    </div>
    </span>
</div>
</div>
</div>
</div>
<?php $this->menu['quick_menu']->display(); ?>
					</div>
			</div>