<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 


jimport( 'joomla.application.component.view' );


class CrmeryViewTemplates extends JView
{
    /**
     * display method
     * @return void
     **/
    function display($tpl = null)
    {
        //display title
        JToolBarHelper::title(JText::_('COM_CRMERY_ADMIN').JText::_('COM_CRMERY_TASK_PEOPLE_TEMPLATES'), 'moo');

        /** Menu Links **/
        $menu = CrmeryHelperMenu::getMenuModules();
        $this->assignRef('menu',$menu);
        
        //get layout
        $layout = $this->getLayout();
        
        //filter for layout type
        if ( $layout == "edit" ){
            
             //toolbar buttons
            JToolbarHelper::cancel('Templates.cancel');
            JToolbarHelper::save('Templates.save');
            //javascripts
            $document =& JFactory::getDocument();
            $document->addScript(JURI::base().'components/com_crmery/media/js/template_manager.js');
            //assign view data
            $this->template_types = CrmeryHelperDropdown::getTemplateTypes();
            
        }else{
            
            //buttons
            JToolBarHelper::addNew('Templates.add');
            JToolBarHelper::editList('Templates.edit');
            JToolBarHelper::deleteList(JText::_('COM_CRMERY_CONFIRMATION'),'Templates.remove');
                
            //gather information for view
            $model = JModel::getInstance('templates','CrmeryModel');
            $templates = $model->getTemplates();
            $this->assignRef('templates',$templates);
                
            // Initialise state variables.
            $state = $model->getState();
            $this->assignRef('state',$state); 
            
        }
        
        //display
        parent::display($tpl);
    }
}