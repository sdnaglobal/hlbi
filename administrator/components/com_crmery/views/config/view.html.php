<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view' );

class CrmeryViewConfig extends JView{

    /**
     * display method
     * @return void
     **/
    function display($tpl = null)
    {
        //display title
        JToolBarHelper::title(JText::_('COM_CRMERY_ADMIN').JText::_('COM_CRMERY_CONFIG'), 'moo');
        JToolbarHelper::cancel('Config.cancel');
        JToolbarHelper::save('Config.save');

        /** Menu Links **/
        $menu = CrmeryHelperMenu::getMenuModules();
        $this->assignRef('menu',$menu);

        $model =& JModel::getInstance('config','CrmeryModel');
        $config = $model->getConfig();

        $list = timezone_identifiers_list();
        $timezones =  array();

        foreach( $list as $zone ){
           $timezones[$zone] = $zone;
        }

        $this->imap_found = function_exists('imap_open') ? TRUE : FALSE ;
        $this->imap_services = CrmeryHelperConfig::getImapServices();

        $time_formats = CrmeryHelperDate::getTimeFormats();

        $this->assignRef('config',$config);
        $this->assignRef('timezones',$timezones);
        $this->assignRef('time_formats',$time_formats);
        
        //display
        parent::display($tpl);
    }
}