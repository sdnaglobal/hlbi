<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="container-fluid">
	<?php echo $this->menu['help_menu']->display(); ?>
    <?php $this->menu['quick_menu']->display(); ?>
	<div class="row-fluid">
	<div class="span12" id="content">
	<div id="system-message-container"></div>
		<div class="row-fluid">
				<?php echo $this->menu['menu']->display(); ?>
		<div class="span9">
<form action="index.php?option=com_crmery&view=config" method="post" name="adminForm" id="adminForm" class="form-validate"  >
    <div class="width-100 fltlft">
            <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a data-toggle="tab" href="#locale"><?php echo JText::_('COM_CRMERY_LOCALE'); ?></a></li>
                <li><a data-toggle="tab" href="#display"><?php echo JText::_('COM_CRMERY_DISPLAY'); ?></a></li>
                <li><a data-toggle="tab" href="#currency"><?php echo JText::_('COM_CRMERY_CURRENCY'); ?></a></li>
                <li><a data-toggle="tab" href="#email"><?php echo JText::_('COM_CRMERY_EMAIL'); ?></a></li>
                <li><a data-toggle="tab" href="#language"><?php echo JText::_('COM_CRMERY_LANGUAGE'); ?></a></li>
                <li><a data-toggle="tab" href="#help"><?php echo JText::_('COM_CRMERY_HELP'); ?></a></li>
                <li><a data-toggle="tab" href="#documents"><?php echo JText::_('COM_CRMERY_DOCUMENTS'); ?></a></li>
                <?php if ( CrmeryHelperConfig::checkAcyMailing() == 1 ){ ?>
                    <li><a data-toggle="tab" href="#acy"><?php echo JText::_('COM_CRMERY_ACYMAILING'); ?></a></li>
                <?php } ?>
                <li><a data-toggle="tab" href="#authentication"><?php echo JText::_('COM_CRMERY_AUTHENTICATION'); ?></a></li>
            </ul>
            <?php echo JHtml::_('bootstrap.startPane', 'myTab', array('active' => 'locale'));?>
            <?php echo JHtml::_('bootstrap.addPanel', 'myTab', 'locale');?>
             <ul class="adminlist crmeryadminlist">
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_TIMEZONE'); ?></b></label>
                    <select class="inputbox" name="timezone" rel="tooltip" data-original-title='<?php echo JText::_('COM_CRMERY_SELECT_CRMERY_TIMEZONE'); ?>' >
                        <?php echo JHtml::_('select.options', $this->timezones, 'value', 'text', $this->config->timezone, true);?>
                    </select>
                </li>
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_TIME_FORMAT'); ?></b></label>
                    <select class="inputbox" name="time_format" rel="tooltip" data-original-title='<?php echo JText::_('COM_CRMERY_SELECT_SERVER_TIME_FORMAT'); ?>' >
                        <?php echo JHtml::_('select.options', $this->time_formats, 'value', 'text', $this->config->time_format, true);?>
                    </select>
                </li>
            </ul>
            <?php echo JHtml::_('bootstrap.endPanel'); ?>
            <?php echo JHtml::_('bootstrap.addPanel', 'myTab', 'display');?>
             <ul class="adminlist crmeryadminlist">
                 <li>
                    <label><b><?php echo JText::_('COM_CRMERY_FORCE_FULLSCREEN'); ?></b></label>
                    <input type="checkbox" class="inputbox" name="fullscreen" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_FORCE_FULLSCREEN_TOOLTIP'); ?>" value="1" <?php if ( array_key_exists('fullscreen',$this->config ) && $this->config->fullscreen == 1 )  echo "checked='checked'"; ?> />
                </li>
                 <li>
                    <label><b><?php echo JText::_('COM_CRMERY_SHOW_HELP_LINK'); ?></b></label>
                    <input type="checkbox" class="inputbox" name="toolbar_help" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_SHOW_HELP_LINK_TOOLTIP'); ?>" value="1" <?php if ( array_key_exists('toolbar_help',$this->config ) && $this->config->toolbar_help == 1 )  echo "checked='checked'"; ?> />
                </li>
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_CUSTOMIZE_HELP_LINK'); ?></b></label>
                    <input type="text" class="inputbox" name="help_url" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_CUSTOMIZE_HELP_LINK_TOOLTIP'); ?>" value="<?php if ( array_key_exists('help_url',$this->config ) )  echo $this->config->help_url; ?>" />
                </li>
            </ul>
            <?php echo JHtml::_('bootstrap.endPanel'); ?>
            <?php echo JHtml::_('bootstrap.addPanel', 'myTab', 'currency');?>
             <ul class="adminlist crmeryadminlist">
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_CURRENCY'); ?></b></label>
                    <input type="text" class="inputbox" name="currency" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_CURRENCY_TOOLTIP'); ?>" value="<?php if ( array_key_exists('currency',$this->config ) ) echo $this->config->currency; ?>" />
                </li>
            </ul>
            <?php echo JHtml::_('bootstrap.endPanel'); ?>
            <?php echo JHtml::_('bootstrap.addPanel', 'myTab', 'email'); ?>
            <div class="alert alert-block">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <h4 class="alert-heading"><?php echo JText::_('COM_CRMERY_IMAP_SETTINGS_TITLE'); ?></h4>
                <?php echo JText::_('COM_CRMERY_IMAP_SETTINGS_DESCRIPTION'); ?>           
            </div>
            <?php if ( !$this->imap_found ){ ?>
                <div style="font-weight:bold;" class="alert alert-error">
                    <?php echo JText::_("COM_CRMERY_WARNING_IMAP_NOT_ENABLED"); ?>
                </div>
             <?php } ?>
            <ul class="adminlist crmeryadminlist">
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_IMAP_HOST'); ?></b></label>
                    <input type="text" class="inputbox" name="imap_host" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_INPUT_IMAP_HOST'); ?>" value="<?php if ( array_key_exists('imap_host',$this->config ) ) echo $this->config->imap_host; ?>" />
                </li>
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_IMAP_USER'); ?></b></label>
                    <input type="text" class="inputbox" name="imap_user" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_INPUT_IMAP_USER'); ?>" value="<?php if ( array_key_exists('imap_user',$this->config ) ) echo $this->config->imap_user; ?>" />
                </li>
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_IMAP_PASS'); ?></b></label>
                    <input type="password" class="inputbox" name="imap_pass" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_INPUT_IMAP_PASS'); ?>" value="<?php if ( array_key_exists('imap_pass',$this->config ) )  echo $this->config->imap_pass; ?>" />
                </li>
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_IMAP_PORT'); ?></b></label>
                    <input type="text" class="inputbox" name="imap_port" rel="tooltip" placeholder="993" data-original-title="<?php echo JText::_('COM_CRMERY_IMAP_PORT'); ?>" value="<?php if ( array_key_exists('imap_port',$this->config ) )  echo $this->config->imap_port; ?>" />
                </li>
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_IMAP_SERVICE'); ?></b></label>
                    <select class="inputbox" name="imap_service" rel="tooltip" data-original-title='<?php echo JText::_('COM_CRMERY_SELECT_SERVICE'); ?>' >
                        <?php echo JHtml::_('select.options', $this->imap_services, 'value', 'text', $this->config->imap_service, true);?>
                    </select>
                </li>
                <li>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="testImapConnection();"><?php echo JText::_('COM_CRMERY_TEST_CONNECTION'); ?></a>
                </li>
                <li>
                    <div id="connection_results"></div>
                </li>
            </ul>
             <?php echo JHtml::_('bootstrap.endPanel'); ?>
            <?php echo JHtml::_('bootstrap.addPanel', 'myTab', 'language'); ?>
            <ul class="adminlist crmeryadminlist">
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_WELCOME_MESSAGE'); ?></b></label>
                    <input type="text" class="inputbox" name="welcome_message" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_WELCOME_MESSAGE_TOOLTIP'); ?>" value="<?php if ( array_key_exists('welcome_message',$this->config ) ) echo $this->config->welcome_message; ?>" />
                </li>
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_I_CALL_A_DEAL'); ?></b></label>
                    <input type="text" class="inputbox" name="lang_deal" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_I_CALL_A_DEAL_TOOLTIP'); ?>" value="<?php if ( array_key_exists('lang_deal',$this->config ) ) echo $this->config->lang_deal; ?>" />
                </li>
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_I_CALL_A_PERSON'); ?></b></label>
                    <input type="text" class="inputbox" name="lang_person" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_I_CALL_A_PERSON_TOOLTIP'); ?>" value="<?php if ( array_key_exists('lang_person',$this->config ) ) echo $this->config->lang_person; ?>" />
                </li>
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_I_CALL_A_COMPANY'); ?></b></label>
                    <input type="text" class="inputbox" name="lang_company" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_I_CALL_A_COMPANY_TOOLTIP'); ?>" value="<?php if ( array_key_exists('lang_company',$this->config ) )  echo $this->config->lang_company; ?>" />
                </li>
                 <li>
                    <label><b><?php echo JText::_('COM_CRMERY_I_CALL_A_CONTACT'); ?></b></label>
                    <input type="text" class="inputbox" name="lang_contact" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_I_CALL_A_CONTACT_TOOLTIP'); ?>" value="<?php if ( array_key_exists('lang_contact',$this->config ) )  echo $this->config->lang_contact; ?>" />
                </li>
                 <li>
                    <label><b><?php echo JText::_('COM_CRMERY_I_CALL_A_LEAD'); ?></b></label>
                    <input type="text" class="inputbox" name="lang_lead" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_I_CALL_A_LEAD_TOOLTIP'); ?>" value="<?php if ( array_key_exists('lang_lead',$this->config ) )  echo $this->config->lang_lead; ?>" />
                </li>
                 <li>
                    <label><b><?php echo JText::_('COM_CRMERY_I_CALL_A_TASK'); ?></b></label>
                    <input type="text" class="inputbox" name="lang_task" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_I_CALL_A_TASK_TOOLTIP'); ?>" value="<?php if ( array_key_exists('lang_task',$this->config ) )  echo $this->config->lang_task; ?>" />
                </li>
                 <li>
                    <label><b><?php echo JText::_('COM_CRMERY_I_CALL_AN_EVENT'); ?></b></label>
                    <input type="text" class="inputbox" name="lang_event" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_I_CALL_AN_EVENT_TOOLTIP'); ?>" value="<?php if ( array_key_exists('lang_event',$this->config ) )  echo $this->config->lang_event; ?>" />
                </li>
                 <li>
                    <label><b><?php echo JText::_('COM_CRMERY_I_CALL_A_GOAL'); ?></b></label>
                    <input type="text" class="inputbox" name="lang_goal" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_I_CALL_A_GOAL_TOOLTIP'); ?>" value="<?php if ( array_key_exists('lang_goal',$this->config ) )  echo $this->config->lang_goal; ?>" />
                </li>
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_I_CALL_A_OWNER'); ?></b></label>
                    <input type="text" class="inputbox" name="lang_owner" rel="tooltip" value="<?php if ( array_key_exists('lang_owner',$this->config ) )  echo $this->config->lang_owner; ?>" />
                </li>
            </ul>
            <?php echo JHtml::_('bootstrap.endPanel'); ?>
            <?php echo JHtml::_('bootstrap.addPanel', 'myTab', 'help'); ?>
            <ul class="adminlist crmeryadminlist">
                 <li>
                    <label><b><?php echo JText::_('COM_CRMERY_SHOW_CRMERY_CONFIG_HELP'); ?></b></label>
                    <input type="checkbox" class="inputbox" name="show_help" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_SHOW_HELP_TOOLTIP'); ?>" value="1" <?php if ( array_key_exists('show_help',$this->config ) && $this->config->show_help == 1 )  echo "checked='checked'"; ?> />
                </li>
            </ul>
            <?php echo JHtml::_('bootstrap.endPanel'); ?>
            <?php echo JHtml::_('bootstrap.addPanel', 'myTab', 'documents'); ?>
            <ul class="adminlist crmeryadminlist">
                 <li>
                    <label><b><?php echo JText::_('COM_CRMERY_ALLOWED_DOCUMENTS'); ?></b></label>
                    <input type="text" class="inputbox" name="allowed_documents" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_ALLOWED_DOCUMENTS_TOOLTIP'); ?>" value="<?php if ( array_key_exists('allowed_documents',$this->config ) )  echo $this->config->allowed_documents; ?>" />
                </li>
            </ul>
            <?php echo JHtml::_('bootstrap.endPanel'); ?>
            <?php if ( CrmeryHelperConfig::checkAcyMailing() == 1 ){ ?>
            <?php echo JHtml::_('bootstrap.addPanel', 'myTab', 'acy'); ?>
            <ul class="adminlist crmeryadminlist">
                 <li>
                    <label><b><?php echo JText::_('COM_CRMERY_ENABLE_PEOPLE_ACYMAILING'); ?></b></label>
                    <input type="checkbox" class="inputbox" name="acy_person" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_ENABLE_ACYMAILING'); ?>" value="1" <?php if ( array_key_exists('acy_person',$this->config ) && $this->config->acy_person == 1 )  echo "checked='checked'"; ?> />
                </li>
                <li>
                    <label><b><?php echo JText::_('COM_CRMERY_ENABLE_COMPANY_ACYMAILING'); ?></b></label>
                    <input type="checkbox" class="inputbox" name="acy_company" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_ENABLE_ACYMAILING'); ?>" value="1" <?php if ( array_key_exists('acy_company',$this->config ) && $this->config->acy_company == 1 )  echo "checked='checked'"; ?> />
                </li>
            </ul>
            <?php echo JHtml::_('bootstrap.endPanel'); ?>
            <?php } ?>
             <?php echo JHtml::_('bootstrap.addPanel', 'myTab', 'authentication'); ?>
            <ul class="adminlist crmeryadminlist">
                 <li>
                    <label><b><?php echo JText::_('COM_CRMERY_LOGIN_REDIRECT'); ?></b></label>
                    <input type="text" class="inputbox" name="login_redirect" rel="tooltip" data-original-title="<?php echo JText::_('COM_CRMERY_LOGIN_REDIRECT_TOOLTIP'); ?>" value="<?php if ( array_key_exists('login_redirect',$this->config ) )  echo $this->config->login_redirect; ?>" />
            </ul>
            <?php echo JHtml::_('bootstrap.endPanel'); ?>
            <?php echo JHtml::_('bootstrap.endPane', 'myTab'); ?>
                <div>
                    <input type="hidden" name="id" value="1" />
                    <input type="hidden" name="task" value="" />
                    <?php echo JHtml::_('form.token'); ?>
                </div>
    </div>
</form>
</div>
</div>
</div>
					</div>
			</div>
