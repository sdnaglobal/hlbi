<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<div class="container-fluid">
	<?php if ( !$this->launch_default ){ ?>
		<div class="alert"><?php echo JText::_('COM_CRMERY_YOUR_SETUP_IS').$this->setup_percent.'% '.JText::_('COM_CRMERY_COMPLETED'); ?></div>
	<?php  } ?>
	<?php echo $this->menu['help_menu']->display(); ?>
	<?php $this->menu['quick_menu']->display(); ?>
	<div class="row-fluid">
	<div class="span12" id="content">
	<div id="system-message-container"></div>
		<div class="row-fluid">
				<?php echo $this->menu['menu']->display(); ?>
		<div class="span9">
			<?php if ( !$this->php_version_check ){ ?>
				<div class="alert alert-error">
                    <?php echo JText::sprintf("COM_CRMERY_WARNING_PHP_VERSION_INVALID",$this->php_version); ?>
                </div>
            <?php } ?>
			<div class="well">
				<div class="module-title"><h6><?php echo JText::_('COM_CRMERY_VERSION_STATUS'); ?></h6></div>
				<div class="row-striped">
					<div class="row-fluid">
						<div class="span12">
							<h5>
							<?php if ($this->latestVersion == 'no_curl') { ?>
								<span class="btn btn-danger btn-mini">
									<i class="icon-remove icon-white tip"></i>
								</span>
									<?php echo JText::_('COM_CRMERY_CURL_NOT_INSTALLED'); ?>
								<?php } elseif ( CrmeryHelperVersion::isUpToDate($this->installedVersion, $this->latestVersion) ) { ?>
									<span class="btn btn-success btn-mini">
										<i class="icon-ok icon-white"></i>
									</span>
									<?php echo JText::sprintf('COM_CRMERY_UP_TO_DATE', $this->installedVersion); ?>
								<?php } else {	?>
										<span class="btn btn-danger btn-mini">
											<i class="icon-remove icon-white"></i>
										</span>
										<?php echo JText::sprintf('COM_CRMERY_UPDATE', $this->installedVersion, $this->latestVersion); ?>
										<a href="<?php echo $this->updateUrl; ?>" target="_blank"><?php echo JText::_('COM_CRMERY_UPDATE_LINK'); ?></a>
								<?php } ?>
							</h5>
						</div>
					</div>
				</div>
			</div>
		<div class="module-title"><h6><?php echo JText::_('COM_CRMERY_LATEST_NEWS'); ?></h6></div>
				<div class="accordion" id="news-accordion">
					<?php $i = 0; ?>
						<?php if ( isset($this->updatesFeed) && array_key_exists('doc',$this->updatesFeed) ){ foreach ( $this->updatesFeed['doc']->item as $item){ $i++; ?>
						<div class="accordion-group">
						    <div class="accordion-heading">
						      <a class="accordion-toggle" data-toggle="collapse" data-parent="#news-accordion" href="#collapse<?php echo $i; ?>">
						        <?php echo $item->title; ?>
						      </a>
						    </div>
						    <div id="collapse<?php echo $i; ?>" class="accordion-body collapse">
						      <div class="accordion-inner">
									<?php echo $item->description; ?>						      
								</div>
						    </div>
						  </div>
						<?php } }?>
					</div>
	</div>
</div>
</div>
					</div>
			</div>