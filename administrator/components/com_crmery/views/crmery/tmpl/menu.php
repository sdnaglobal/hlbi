<?php JHtml::_('bootstrap.loadtooltip', 'content', array()); ?>
<div class="span3 crmerySpan3">
		<div class="sidebar-nav">
			<ul class="nav nav-list">
        <li class="nav-header"><?php echo JText::_('CRMery Menu'); ?></li>
        <?php $view = JRequest::getVar('view'); ?>
        <?php if ( count($this->menu_links) > 0 ){ foreach ( $this->menu_links as $menu_link ){ ?>
        	<?php $active = $view == $menu_link['view'] ? "active" : ""; ?>
            <li class="<?php echo $active; ?>" ><?php echo JHtml::_('bootstrap.tooltip', '<i class="'.$menu_link['class'].'"></i><span id="'.$menu_link['id'].'">'.$menu_link['text'].'</span>', $menu_link['tooltip'],$menu_link['link']); ?></li>
        <?php } } ?>
        </ul>
    </div>
</div>