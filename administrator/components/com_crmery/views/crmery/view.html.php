<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view' );

class CrmeryViewCrmery extends JView
{
    /**
     * display method
     * @return void
     **/
    function display($tpl = null)
    {
        /** Menu Links **/
        $menu = CrmeryHelperMenu::getMenuModules();
        $this->assignRef('menu',$menu);

        $configModel = JModel::getInstance('Config','CrmeryModel');

        /** Component version **/
        $installedVersion   = CrmeryHelperConfig::getVersion();
        $latestVersion      = CrmeryHelperVersion::getLatestVersion();
        $updateUrl = "http://www.crmery.com/member-login/my-membership/view-my-memberships";
        $updatesFeed = $configModel->getUpdatesRSS();

        /** Launch completion **/
        $configModel =& JModel::getInstance('Config','CrmeryModel');
        $config = $configModel->getConfig();
        $this->assignRef('launch_default',$config->launch_default);
        $percentage = $menu['percentage'];
        $this->assignRef('setup_percent',$percentage);

        /** php version check **/
        $this->php_version = (float)phpversion();
        $this->php_version_check = $this->php_version >= 5.3 ? TRUE : FALSE;

        /** View Ref **/
        $this->assignRef('installedVersion',$installedVersion);
        $this->assignRef('latestVersion',$latestVersion);
        $this->assignRef('updateUrl',$updateUrl);
        $this->assignRef('updatesFeed',$updatesFeed);
        
        //display
        parent::display($tpl);
    }
}
