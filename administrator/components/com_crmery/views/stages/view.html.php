<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view' );

class CrmeryViewStages extends JView
{
    /**
     * display method
     * @return void
     **/
    function display($tpl = null)
    {
        //display title
        JToolBarHelper::title(JText::_('COM_CRMERY_ADMIN').JText::_('COM_CRMERY_DEAL_STAGES'), 'moo');

        /** Menu Links **/
        $menu = CrmeryHelperMenu::getMenuModules();
        $this->assignRef('menu',$menu);
        
        $layout = $this->getLayout();
        $this->pagination   = $this->get('Pagination');
        
        if ( $layout && $layout == 'edit' ){
            
            JToolbarHelper::cancel('Stages.cancel');
            JToolbarHelper::save('Stages.save');

            $document =& JFactory::getDocument();
            $document->addScript(JURI::base().'components/com_crmery/media/js/stage_manager.js');
            $document->addScript(JURI::base().'components/com_crmery/media/js/colorpicker.js');
            //stylesheets
            $document->addStylesheet(JURI::base().'components/com_crmery/media/css/colorpicker.css');
            
            
        }else{
            
            //buttons
            JToolBarHelper::addNew('Stages.add');
            JToolBarHelper::editList('Stages.edit');
            JToolBarHelper::deleteList(JText::_('COM_CRMERY_CONFIRMATION'),'Stages.remove');
                
            //gather information for view
            $model = JModel::getInstance('stages','CrmeryModel');
            $stages = $model->getStages();
            $this->assignRef('stages',$stages);
                
            // Initialise state variables.
            $state = $model->getState();
            $this->assignRef('state',$state);
        }
        
        //display
        parent::display($tpl);
    }
}
        