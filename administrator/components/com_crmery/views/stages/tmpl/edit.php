<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="container-fluid">
	<?php echo $this->menu['help_menu']->display(); ?>
	<div class="row-fluid">
	<div class="span12" id="content">
	<div id="system-message-container"></div>
		<div class="row-fluid">
				<?php echo $this->menu['menu']->display(); ?>
		<div class="span9">
<form action="<?php echo JRoute::_('index.php?option=com_crmery&view=stages'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate"  >
    <div class="width-100 fltlft">

            <legend><h3><?php echo $this->header; ?></h3></legend>

                    <label><b><?php echo JText::_('COM_CRMERY_NAME'); ?></b></label>
                    <input type="text" class="inputbox" name="name" value="<?php echo $this->stage['name']; ?>" />

                    <label><b><?php echo JText::_('COM_CRMERY_HEADER_PERCENT'); ?></b></label>
                    <span class="crmeryfield">
                        <input type="hidden" name="percent" value="<?php echo $this->stage['percent']; ?>"/>
                        <div class="" id="percent"></div>
                        <div id="percent_value"><?php echo $this->stage['percent']."%"; ?></div>
                    </span>

                    <label><b><?php echo JText::_("COM_CRMERY_COLOR"); ?></b></label>
                    <input class="inputbox color" type="text" name="color" value="<?php echo $this->stage['color']; ?>"><div class="colorwheel"></div>

                    <label><b><?php echo JText::_("COM_CRMERY_WON_STAGE"); ?></b></label>
                    <input <?php if ( isset($this->stage) && array_key_exists('won',$this->stage) && $this->stage['won'] == 1 ) echo "checked='checked'"; ?> type="checkbox" name="won" value="1">

                <div>
                    <?php if ( $this->stage['id'] ) { ?>
                        <input type="hidden" name="id" value="<?php echo $this->stage['id']; ?>" />
                    <?php } ?>
                    <input type="hidden" name="task" value="" />
                    <?php echo JHtml::_('form.token'); ?>
                </div>
            </ul>
    </div>
</form>
</div>
</div>
</div>
<?php $this->menu['quick_menu']->display(); ?>
					</div>
			</div>