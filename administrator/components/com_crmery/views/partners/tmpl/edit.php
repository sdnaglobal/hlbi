<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<script type="text/javascript">
    <?php if ( $this->custom['type'] != null ) { ?>
        var type = "<?php echo $this->custom['type']; ?>";
    <?php } ?> 
</script>

<div class="container-fluid">
	<?php echo $this->menu['help_menu']->display(); ?>
	<div class="row-fluid">
	<div class="span12" id="content">
	<div id="system-message-container"></div>
		<div class="row-fluid">
				<?php echo $this->menu['menu']->display(); ?>
		<div class="span9">
<form action="<?php echo JRoute::_('index.php?option=com_crmery&view=partners'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate"  >
    <div class="width-100 fltlft">
            <legend><h3><?php echo $this->header; ?></h3></legend>
                    <label><b>First Name</b></label>
                    <input type="text" class="inputbox required" name="name" value="<?php echo $this->custom['name']; ?>" />
					<label><b>Last Name</b></label>
                    <input type="text" class="inputbox required" name="lname" value="<?php echo $this->custom['lname']; ?>" />
					<label><b>Position</b></label>
                    <input type="text" class="inputbox required" name="position" value="<?php echo $this->custom['position']; ?>" />
					<label><b>Phone</b></label>
                    <input type="text" class="inputbox required" name="phone" value="<?php echo $this->custom['phone']; ?>" />
					
					<label><b>Email</b></label>
                    <input type="text" class="inputbox required" name="email" value="<?php echo $this->custom['email']; ?>" />
					
                   <!-- <label><b>Type</b></label>
                    <select class="inputbox required" name="type">
                        <option value="">- Select Custom Field Type -</option>
                        <?php echo JHtml::_('select.options', $this->custom_types, 'value', 'text', $this->custom['type'], true);?>
                    </select>
                <legend>Field Information</legend>
                <div id="custom_field_data">
                </div>-->
                <div>
                    <?php if ( $this->custom['id'] ) { ?>
                        <input type="hidden" name="id" value="<?php echo $this->custom['id']; ?>" />
                    <?php } ?>
                    <input type="hidden" name="task" value="" />
                    <?php echo JHtml::_('form.token'); ?>
                </div>
    </div>
</form>
<!--<div style="display:none;" id="custom_field_templates">
    <div id="custom_field_number">
        <ul>
            <li>Numeric Fields can be used to capture items such as product units or other numeric values related to your companies.</li>
        </ul>
        <table>
            <tr>
                <td><input class="inputbox" type="checkbox" name="required" <?php if ( $this->custom['required']) echo 'checked'; ?> /></td>
                <td>Make this field a required entry.</td>
            </tr>
        </table>
    </div>
    <div id="custom_field_text">
        <ul>
            <li>Text Fields are used to capture free form text. Examples: Billing Street Address, Email Address.</li>
        </ul>
        <table>
            <tr>
                <td><input class="inputbox" type="checkbox" name="required" <?php if ( $this->custom['required']) echo 'checked'; ?> /></td>
                <td>Make this field a required entry.</td>
            </tr>
        </table>
    </div>
    <div id="custom_field_currency">
        <ul>
            <li>Currency Fields are used to capture items that have a $ monetary value. Examples: Service Revenue $, Product Sales $</li>
        </ul>
        <table>
            <tr>
                <td><input class="inputbox" type="checkbox" name="required" <?php if ( $this->custom['required']) echo 'checked'; ?> /></td>
                <td>Make this field a required entry.</td>
            </tr>
        </table>
    </div>
    <div id="custom_field_picklist">
        <ul>
            <li>Picklist fields allow you to specify a list of pre-defined values for a user to pick from. Examples: Industry, Competitor, Regions, Product or Service Interest.</li>
        </ul>
        <div id="choices">
            <?php if ( is_array($this->custom) && array_key_exists('values',$this->custom) && $this->custom['values'] != null ){
            $values = $this->custom['values'];
            if ( count($values) > 0 ){
                foreach ( $values as $value ){ ?>
                    <div class="choices">
                        <table>
                            <tr>
                                <td>Enter Choice</td>
                                <td><input class="inputbox required" type="text" name="values[]" value="<?php echo $value; ?>" /></td>
                                <td><a class="btn btn-danger remove_values">Remove</a></td>
                            </tr>
                        </table>
                    </div>
                <?php } 
            }}else{ ?>
                 <div class="choices">
                    <table>
                        <tr>
                            <td>Enter Choice</td>
                            <td><input class="inputbox required" type="text" name="values[]" value="" /></td>
                            <td><a class="btn btn-danger remove_values">Remove</a></td>
                        </tr>
                    </table>
                </div>   
            <?php } ?>
        </div>
        <table>
            <tr>
                <td><a class="btn btn-primary" id="add_values">Add More Choices</a></td> 
            </tr>
       </table>
       <table>
            <tr>
                <td><input type="checkbox" name="multiple_selections" value="1" <?php if ( $this->custom['multiple_selections']) echo 'checked'; ?> /></td>
                <td>Users can select more than one value</td>
            </tr>
            <tr>
                <td><input type="checkbox" name="required" <?php if ( $this->custom['required']) echo 'checked'; ?> /></td>
                <td>Make this field a required entry.</td>
            </tr>
        </table>
    </div>
    <div id="choice_template">
        <div class="choices">
            <table>
                <tr>
                    <td>Enter Choice</td>
                    <td><input class="inputbox required" type="text" name="values[]" value="" /></td>
                    <td><a class="btn btn-danger remove_values">Remove</a></td>
                </tr>
            </table>
        </div>
    </div>
   <div id="custom_field_date">
       <ul>
           <li>Date fields allow you to capture important dates related to your companies.</li>
       </ul>
       <table>
            <tr>
                <td><input type="checkbox" name="required" <?php if ( $this->custom['required']) echo 'checked'; ?> /></td>
                <td>Make this field a required entry.</td>
            </tr>
        </table>
   </div>
</div>-->
</div>
</div>
</div>
<?php $this->menu['quick_menu']->display(); ?>
					</div>
			</div>
			