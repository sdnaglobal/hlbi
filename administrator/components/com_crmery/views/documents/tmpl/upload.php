<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<?php 
    $session =& JFactory::getSession();
    if ( $session->get('upload_success') == true ){
        $session->set('upload_success',false);
        ?>
        <script type="text/javascript">
               window.top.location = 'index.php?option=com_crmery&view=documents'; 
        </script>
        <?php
    }else{
?>
<div class="width-100 fltlft">
<legend><h3><?php echo JText::_('COM_CRMERY_UPLOAD_DOCUMENT'); ?></h3></legend>
<form action="<?php echo JRoute::_('index.php?option=com_crmery&view=documents&layout=upload'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data" >
        <input type="file" class="input-file" name="document" />
        <input type="button" class="btn btn-primary" value="Upload" onclick="uploadDocument();" />
        <input type="hidden" name="task" value="Documents.upload" />
        <?php echo JHtml::_('form.token'); ?>
</form>
</ul>
</div>
<?php } ?>