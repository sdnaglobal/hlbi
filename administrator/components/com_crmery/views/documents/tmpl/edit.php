<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="container-fluid">
	<?php echo $this->menu['help_menu']->display(); ?>
	<div class="row-fluid">
	<div class="span12" id="content">
	<div id="system-message-container"></div>
		<div class="row-fluid">
				<?php echo $this->menu['menu']->display(); ?>
		<div class="span9">
<h3><?php echo $this->header; ?></h3>
<form action="<?php echo JRoute::_('index.php?option=com_crmery&view=documents'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate"  >
    <table>
        <tr>
            <td><b><?php echo JText::_('COM_CRMERY_NAME'); ?></b></td>
            <td><input type="text" class="inputbox" name="filename" value="<?php echo $this->document['filename']; ?>" /></td>
        </tr>
    </table> 
    <div>
        <?php if ( $this->document['id'] ) { ?>
            <input type="hidden" name="id" value="<?php echo $this->document['id']; ?>" />
        <?php } ?>
        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>
</div>
</div>
</div>
<?php $this->menu['quick_menu']->display(); ?>
					</div>
			</div>