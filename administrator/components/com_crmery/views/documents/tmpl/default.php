<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<div class="container-fluid">
	<?php echo $this->menu['help_menu']->display(); ?>
	<div class="row-fluid">
	<div class="span12" id="content">
	<div id="system-message-container"></div>
		<div class="row-fluid">
				<?php echo $this->menu['menu']->display(); ?>
		<div class="span9">
<div class="width-100 fltlft">
<legend><h3><?php echo JText::_('COM_CRMERY_SHARED_DOCUMENTS'); ?></h3></legend>
<div class="alert alert-info"><?php echo JText::_('COM_CRMERY_SHARED_DOCS_DESC'); ?></div>
<form action="<?php echo JRoute::_('index.php?option=com_crmery&view=documents'); ?>" method="post" name="adminForm" id="adminForm">
    <?php
        $listOrder = $this->state->get('Documents.filter_order');
        $listDirn   = $this->state->get('Documents.filter_order_Dir');
    ?>
 <table class="adminlist">
        <thead>
            <tr>
                <th width="1%">
                    <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
                </th>
                <th>
                    <?php echo JHtml::_('grid.sort',  'COM_CRMERY_HEADER_DOCUMENT_TYPE', 'd.filetype', $listDirn, $listOrder); ?>
                </th>
                <th>
                    <?php echo JHtml::_('grid.sort',  'COM_CRMERY_HEADER_DOCUMENT_FILENAME', 'd.name', $listDirn, $listOrder); ?>
                </th>
                <th>
                    <?php echo JHtml::_('grid.sort',  'COM_CRMERY_HEADER_DOCUMENT_SIZE', 'd.size', $listDirn, $listOrder); ?>
                </th>
                <th>
                    <?php echo JHtml::_('grid.sort',  'COM_CRMERY_HEADER_DOCUMENT_UPLOADED', 'd.created', $listDirn, $listOrder); ?>
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="13">
                    <!-- pagination -->
                </td>
            </tr>
        </tfoot>
        <tbody>
            <?php if ( count($this->documents) ) { foreach($this->documents as $key=>$document){ ?>
                
                <tr class="row<?php echo $i % 2; ?>">
                    <td class="center">
                        <?php echo JHtml::_('grid.id', $key, $document['id']); ?>
                    </td>
                    <td class="order"><?php echo '<img width="30px" height="30px" src="'.JURI::base().'components/com_crmery/media/images/'.$document['filetype'].'.png'.'" /><br /><b>'.strtoupper($document['filetype']).'<b></td>'; ?></td>
                    <td class="order"><?php echo JHtml::_('link',JURI::root().'components/com_crmery/documents/'.$document['filename'],$document['name'],array('target'=>'_blank')); ?></td>
                    <td class="order"><?php echo $document['size']; ?>kb</td>
                    <td class="order"><?php echo $document['created']; ?></td>
                </tr>
            
            <?php }} ?>
        </tbody>
    </table>
    <div>
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>
</div>
</div>
</div>
</div>
<?php $this->menu['quick_menu']->display(); ?>
					</div>
			</div>