<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view' );

class CrmeryViewDocuments extends JView{

    /**
     * display method
     * @return void
     **/
    function display($tpl = null)
    {
        //display title
        JToolBarHelper::title(JText::_('COM_CRMERY_ADMIN').JText::_('COM_CRMERY_SHARED_DOCUMENTS'), 'moo');
        
        //get the layout
        $layout = $this->getLayout();
        
        //add javascript
        $document =& JFactory::getDocument();
        $document->addScript(JURI::base().'components/com_crmery/media/js/document_manager.js');

        if ( $layout != "upload" ){
            /** Menu Links **/
            $menu = CrmeryHelperMenu::getMenuModules();
            $this->assignRef('menu',$menu);
        }
        
        //determine layout type
        if ( $layout && $layout == 'edit' ){
            
            JToolbarHelper::cancel('Documents.cancel');
            JToolbarHelper::save('Documents.save');
            
            
        }else{
            
            //buttons
            $bar=& JToolBar::getInstance( 'toolbar' );
            $bar->appendButton( 'Popup', 'upload', "Upload", 'index.php?option=com_crmery&view=documents&layout=upload&tmpl=component', 375, 150 );
            JToolBarHelper::deleteList(JText::_('COM_CRMERY_CONFIRMATION'),'Documents.remove');
            
            //gather information for view
            $model = JModel::getInstance('Documents','CrmeryModel');
            $documents = $model->getDocuments();
            $this->assignRef('documents',$documents);
                
            // Initialise state variables.
            $state = $model->getState();
            $this->assignRef('state',$state);
        }
        
        //display
        parent::display($tpl);
    }
}