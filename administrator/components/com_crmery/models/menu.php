<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.model');

class CrmeryModelMenu extends JModel
{
    /**
     * 
     *
     * @access  public
     * @return  void
     */
    function __construct()
    {
        parent::__construct();
        
    }
    
    function store()
    {
        //Load Tables
        $row =& JTable::getInstance('Menu','Table');
        $data = JRequest::get( 'post' );
        
        //date generation
        $date = date('Y-m-d H:i:s');
        $data['modified'] = $date;

        //serialize menu items for storage
        $data['menu_items'] = serialize($data['menu_items']);
        
        // Bind the form fields to the table
        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
     
        // Make sure the record is valid
        if (!$row->check()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
     
        // Store the web link table to the database
        if (!$row->store()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
        
        return true;
    }

    function getMenu(){

        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);

        $query->select("*")->from("#__crmery_menu")->where("id=1");
        $db->setQuery($query);

        $menu = $db->loadObject();
        $menu->menu_items = unserialize($menu->menu_items);
        return $menu;

    }

    function getMenuTemplate(){
        return array('dashboard','deals','people','companies','calendar','documents','goals','reports','history', 'charts');
    }

}