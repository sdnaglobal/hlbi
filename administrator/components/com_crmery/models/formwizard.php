<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

include_once(JPATH_COMPONENT.'/models/crmery.php');

class CrmeryModelFormwizard extends CrmeryModelCrmery
{
    /**
     * 
     *
     * @access  public
     * @return  void
     */
    function __construct()
    {
        parent::__construct();
        
    }

    public function populateState(){
        //get states
        $app = JFactory::getApplication();
        $filter_order = $app->getUserStateFromRequest('Formwizard.filter_order','filter_order','f.name');
        $filter_order_Dir = $app->getUserStateFromRequest('Formwizard.filter_order_Dir','filter_order_Dir','asc');
        
        //set states
        $this->setState('Formwizard.filter_order', $filter_order);
        $this->setState('Formwizard.filter_order_Dir',$filter_order_Dir);
    }
    
    function store()
    {
        //Load Tables
        $row =& JTable::getInstance('formwizard','Table');
        $data = JRequest::get( 'post' );

        $userId = JFactory::getUser()->id;
        
        //date generation
        $date = date('Y-m-d H:i:s');
        $data['modified'] = $date;
        $data['modified_by'] = $userId;
        if ( !array_key_exists('id',$data) ){
            $data['created'] = $date;
            $data['created_by'] = $userId;
        }

        if ( array_key_exists('fields',$data) ){
            $data['fields'] = serialize($data['fields']);
        }

        if ( array_key_exists('html',$data) ){
            $data['html'] = JRequest::getVar('html', '', 'post', 'string', JREQUEST_ALLOWRAW);
        }

        //TODO: This poses a problem if the user creates a form and copies HTML immediately on new page before saving
        // they could potentially have an existing ID and then have the wrong code in their copied HTML
        // This would be rare and only if multiple users are simultaneously adding custom forms...

        if(array_key_exists('temp_id',$data) ) {
            
            $db = JFactory::getDBO();
            $query = $db->getQuery(true);
            $query->select('COUNT(*) as existing, MAX(id) AS greatest')
                    ->from('#__crmery_formwizard')
                    ->where('id = '.$data['temp_id']);
            $db->setQuery($query);
            $existing = $db->loadAssoc();

            if($existing['existing'] > 0) {
                $nextId = $existing['greatest']+1;
                $data['html'] = preg_replace('/name="form_id" value="(.*?)"/','name="form_id" value="'.$nextId.'"',$data['html']);
            }

        }

        // Bind the form fields to the table
        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
     
        // Make sure the record is valid
        if (!$row->check()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }

        // Store the web link table to the database
        if (!$row->store()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }

        return true;
    }

    function __buildQuery(){
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select("f.*,CONCAT(user.first_name,' ',user.last_name) AS owner_name")->from("#__crmery_formwizard AS f");
        $query->leftJoin("#__crmery_users AS user ON user.id = f.owner_id");
        return $query;
    }

    function getForms(){
        $query = $this->__buildQuery();
        $db =& JFactory::getDBO();
        $query->order($this->getState('Formwizard.filter_order') . ' ' . $this->getState('Formwizard.filter_order_Dir'));
        $db->setQuery($query);
        $results = $db->loadAssocList();
        if ( count($results) > 0 ){
            foreach ( $results as $key => $result ){
                $results[$key]['fields'] = unserialize($result['fields']);
                $results[$key]['html'] = $result['html'];
            }
        }
        return $results;
    }

    function getForm($formId){
        $query = $this->__buildQuery();
        $db =& JFactory::getDBO();
        $query->where("f.id=".$formId);
        $db->setQuery($query);
        $result = $db->loadAssoc();
        $result['fields'] = unserialize($result['fields']);
        $result['html'] = $result['html'];
        return $result;
    }

    function delete($ids){
        //get db
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);

        $query->delete("#__crmery_formwizard");
                if ( is_array($ids) ){
                    $query->where("id IN(".implode(','.$ids).")");
                }else{
                    $query->where("id=".$ids);
                }
        $db->setQuery($query);
        if ( $db->query() ){
            return true;
        }else{
            return false;
        }
    }

    function getTempFormId()
    {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('MAX(id)')
                ->from('#__crmery_formwizard');
        $db->setQuery($query);
        $lastId = $db->loadResult();

        return $lastId+1;
    }

}