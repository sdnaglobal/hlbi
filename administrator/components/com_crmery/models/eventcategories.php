<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

include_once(JPATH_COMPONENT.'/models/crmery.php');

class CrmeryModelEventCategories extends CrmeryModelCrmery
{
    /**
     * 
     *
     * @access  public
     * @return  void
     */
    function __construct()
    {
        parent::__construct();
        
    }
    
    function store()
    {
        //Load Tables
        $row =& JTable::getInstance('EventCategories','Table');
        $data = JRequest::get( 'post' );
        
        //date generation
        $date = date('Y-m-d H:i:s');

        $userId = JFactory::getUser()->id;
        
        if ( !array_key_exists('id',$data) ){
            $data['created'] = $date;
            $data['created_by'] = $userId;
        }
        
        $data['modified'] = $date;
        $data['modified_by'] = $userId;

        $data['milestone'] = array_key_exists('milestone',$data) ? 1 : 0;
        
        // Bind the form fields to the table
        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
     
        // Make sure the record is valid
        if (!$row->check()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
     
        // Store the web link table to the database
        if (!$row->store()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
        
        return true;
    }
    
    
    function __buildQuery()
    {
        //database
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query->select("c.*");
        $query->from("#__crmery_events_categories AS c");
        
        //sort
        $query->order($this->getState('EventCategories.filter_order') . ' ' . $this->getState('EventCategories.filter_order_Dir'));

        return $query;
    }
    
    
    /**
     * Get list of stages
     * @param int $id specific search id
     * @return mixed $results results
     */
    function getCategories($id=null){
        
        //database
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query = $this->__buildQuery();
        if( $id ){
            $query->where("c.id=$id");
        }
        
        //return results
        $db->setQuery($query);
        $categories = $db->loadAssocList();

        return $categories;
        
    }
    
    function populateState(){
        //get states
        $app = JFactory::getApplication();
        $filter_order = $app->getUserStateFromRequest('EventCategories.filter_order','filter_order','c.name');
        $filter_order_Dir = $app->getUserStateFromRequest('EventCategories.filter_order_Dir','filter_order_Dir','asc');
        
        //set states
        $this->setState('EventCategories.filter_order', $filter_order);
        $this->setState('EventCategories.filter_order_Dir',$filter_order_Dir);
    }
    
    function remove($id){
        //get dbo
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //delete id
        $query->delete('#__crmery_events_categories')->where('id = '.$id);
        $db->setQuery($query);
        $db->query();
    }

    

}