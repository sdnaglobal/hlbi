<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.model');

class CrmeryModelBranding extends JModel
{
    /**
     * 
     *
     * @access  public
     * @return  void
     */
    function __construct()
    {
        parent::__construct();
        
    }
    
    function store()
    {
        //Load Tables
        $row =& JTable::getInstance('branding','Table');
        $data = JRequest::get( 'post' );
        
        //date generation
        $date = date('Y-m-d H:i:s');
        $data['modified'] = $date;
        $this->changeDefault($data['id']);
        
        // Bind the form fields to the table
        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
     
        // Make sure the record is valid
        if (!$row->check()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
     
        // Store the web link table to the database
        if (!$row->store()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
        
        return true;
    }
    
    
    /**
     * Get list of themes
     * @param int $id specific search id
     * @return mixed $results results
     */
    function getThemes($id=null){
        
        //database
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query->select("b.*");
        $query->from("#__crmery_branding AS b");
        
        //return results
        $db->setQuery($query);
        return $db->loadAssocList();
        
    }
    
    /**
     * Get list of themes
     * @param int $id specific search id
     * @return mixed $results results
     */
    function getDefaultTheme(){
        
        //database
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query->select("b.*");
        $query->from("#__crmery_branding AS b");
        $query->where("assigned=1");
        
        //return results
        $db->setQuery($query);
        return $db->loadAssocList();
        
    }
    
    /**
     * Change the default template
     * @param int $id id to assign to default
     * @return void
     */
    function changeDefault($id){
        
        //database
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //unassign default
        $queryString = "UPDATE #__crmery_branding SET assigned=0 WHERE id <> $id";
        $db->setQuery($queryString);
        $db->query();
        
        //assign default
        $queryString = "UPDATE #__crmery_branding SET assigned=1 WHERE id=$id";
        $db->setQuery($queryString);
        $db->query();
        
    }
    

}