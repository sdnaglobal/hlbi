<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

include_once(JPATH_COMPONENT.'/models/crmery.php');

class CrmeryModelSources extends CrmeryModelCrmery
{

    var $id = null;

    /**
     * 
     *
     * @access  public
     * @return  void
     */
    function __construct()
    {
        parent::__construct();
        
    }
    
    function store()
    {
        //Load Tables
        $row =& JTable::getInstance('sources','Table');
        $data = JRequest::get( 'post' );
        
        //date generation
        $date = date('Y-m-d H:i:s');
        
        if ( !array_key_exists('id',$data) ){
            $data['created'] = $date;
        }
        
        $data['modified'] = $date;
        
        // Bind the form fields to the table
        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
     
        // Make sure the record is valid
        if (!$row->check()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
     
        // Store the web link table to the database
        if (!$row->store()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
        
        return true;
    }

    function __buildQuery(){

        //database
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query->select("s.*");
        $query->from("#__crmery_sources AS s");
        
        //sort
        $query->order($this->getState('Sources.filter_order') . ' ' . $this->getState('Sources.filter_order_Dir'));

        return $query;

    }
    
    
    /**
     * Get list of stages
     * @param int $id specific search id
     * @return mixed $results results
     */
    function getSources($id=null){
        
        //database
        $db =& JFactory::getDBO();
        $query = $this->__buildQuery();
        
        if( $id ){
            $query->where("s.id=$id");
        }
        
        //return results
        $db->setQuery($query);
        return $db->loadAssocList();
        
    }
    
    function populateState(){
        //get states
        $app = JFactory::getApplication();
        $filter_order = $app->getUserStateFromRequest('Sources.filter_order','filter_order','s.name');
        $filter_order_Dir = $app->getUserStateFromRequest('Sources.filter_order_Dir','filter_order_Dir','asc');
        
        //set states
        $this->setState('Sources.filter_order', $filter_order);
        $this->setState('Sources.filter_order_Dir',$filter_order_Dir);
    }
    
    function remove($id){
        //get dbo
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //delete id
        $query->delete('#__crmery_sources')->where('id = '.$id);
        $db->setQuery($query);
        $db->query();
    }

    

}