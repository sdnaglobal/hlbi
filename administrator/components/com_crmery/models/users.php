<?php

/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.model');

class CrmeryModelUsers extends JModel
{
    /**
     * 
     *
     * @access  public
     * @return  void
     */
    function __construct()
    {
        parent::__construct();
    }
    
    function store()
    {
        //Load Tables
        $row =& JTable::getInstance('users','Table');
        $data = JRequest::get( 'post' );
		
		$region=implode(",",$data['region_id']);
		$country=implode(",",$data['country_id']);
		$state=implode(",",$data['state_id']);
		
		$data['region_id']=$region;
		$data['country_id']=$country;
        $data['state_id']=$state;				
		

        $dispatcher =& JDispatcher::getInstance();
        $dispatcher->trigger('onBeforeCRMUserSave', array(&$data));
        
        //date generation
        $date = date('Y-m-d H:i:s');
        
        if ( !array_key_exists('id',$data) ){
            $data['created'] = $date;
            $data['time_zone'] = CrmeryHelperConfig::getConfigValue('timezone');
            $data['time_format'] = CrmeryHelperConfig::getConfigValue('time_format');
        }

        //generate team data
        $model =& JModel::getInstance('teams','CrmeryModel');
        $teamId = array_key_exists('id',$data) ? $this->getTeamId($data['id']) : 0;
        
        //assign user priviliges
        $data['modified'] = $date;
        $data['admin'] = ( array_key_exists ('admin',$data) && $data['admin'] == 'on' ) ? 1 : 0;
        $data['exports'] = ( array_key_exists ('exports',$data) && $data['exports'] == 'on' ) ? 1 : 0;
        $data['can_delete'] = ( array_key_exists ('can_delete',$data) && $data['can_delete'] == 'on' ) ? 1 : 0;

        //republish / register users
        if ( array_key_exists('uid',$data) && $data['uid'] != "" ){
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            $query->clear()->select("id")->from("#__crmery_users")->where("uid=".$data['uid']);
            $db->setQuery($query);
            $id = $db->loadResult();
            if ( $id ){
                $data['id'] = $id;
                $data['published'] = 1;
            }
        }

        if ( array_key_exists('team_id',$data) && $data['team_id'] == "" ){
            unset($data['team_id']);
        }

        // Bind the form fields to the table
        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
     
        // Make sure the record is valid
        if (!$row->check()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
     
        // Store the web link table to the database
        if (!$row->store()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }

        if ( array_key_exists('role_type',$data) && $data['role_type'] == "manager"  ) {
            $teamModel =& JModel::getInstance('teams','CrmeryModel');
            $teamName = array_key_exists('team_name',$data) ? $data['team_name'] : "";
            $teamModel->createTeam($row->id,$teamName);
        }

        //if we are downgrading a users priviliges
        if ( array_key_exists('manager_assignment',$data) && $data['manager_assignment'] != null && $data['manager_assignment'] != "" ) {
            $newTeamId = $this->getTeamId($data['manager_assignment']);
            $model->updateTeam($teamId,$newTeamId);
        }
        
        $dispatcher =& JDispatcher::getInstance();
        $dispatcher->trigger('onAfterCRMUserSave', array(&$data));

        return true;
    }
    
    public function getUsers($id=null){
        //get dbo
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //select
        $query->select("u.*,ju.username,ju.email,ju.lastvisitDate as last_login,
                        team_leader.first_name as leader_first_name,team_leader.last_name as leader_last_name,
                        team.leader_id as leader_id,
                        IF(team.name!='',team.name,CONCAT(team_leader.first_name,' ',team_leader.last_name)) AS team_name");
        $query->from("#__crmery_users AS u");
        
        //left join essential data
        $query->leftJoin("#__users AS ju ON ju.id = u.uid");
        $query->leftJoin("#__crmery_teams AS team ON team.team_id = u.team_id");
        $query->leftJoin("#__crmery_users AS team_leader ON team_leader.id = team.leader_id");
        
        //sort
        $query->order($this->getState('Users.filter_order') . ' ' . $this->getState('Users.filter_order_Dir'));
        if( $id ){
            $query->where("u.id=$id");
        }

        $query->where("u.published=1");
        
        //return results
        $db->setQuery($query);
        return $db->loadAssocList();
    }
    
    public function populateState(){
        //get states
        $app = JFactory::getApplication();
        $filter_order = $app->getUserStateFromRequest('Users.filter_order','filter_order','u.last_name');
        $filter_order_Dir = $app->getUserStateFromRequest('Users.filter_order_Dir','filter_order_Dir','asc');
        
        //set states
        $this->setState('Users.filter_order', $filter_order);
        $this->setState('Users.filter_order_Dir',$filter_order_Dir);
    }
    
    public function getJoomlaUsersToAdd(){
        //get dbo
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //select
        $query->select("ju.id,ju.name,ju.username,ju.email,cu.id as cid,cu.published");
        $query->from("#__users AS ju");
        
        //left join essential data
        $query->leftJoin("#__crmery_users AS cu ON ju.id = cu.uid");
        
        //return results
        $db->setQuery($query);
        $results = $db->loadAssocList();
        $users = array();
        foreach ( $results as $key => $user ){
            if ( !$user['cid'] || $user['published'] == -1  ){
                $name = explode(" ",$user['name']);
                $user['first_name'] = array_key_exists(0,$name) ? $name[0] : "";
                $temp_last_name =  array_key_exists(1,$name) ? $name[1] : "";
				$temp_last_name1 = array_key_exists(2,$name) ? $name[2] : "";
				$temp_last_name2 = array_key_exists(3,$name) ? $name[3] : "";
				$temp_last_name3 = array_key_exists(4,$name) ? $name[4] : "";
				$user['last_name'] = trim($temp_last_name.' '.$temp_last_name1.' '.$temp_last_name2.' '.$temp_last_name3);
 

                $users[$user['id']] = $user;
            }
        } 
        return $users;
    }

    public function getCrmeryUsers($idsOnly=FALSE){
        //get dbo
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //select
        $query->select("u.id AS value,CONCAT(u.first_name,' ',u.last_name) AS label");
        $query->from("#__crmery_users AS u");
        $query->where("u.published=1");
        
        //return results
        $db->setQuery($query);
        $results = $db->loadAssocList();
        
        return $results;
    }
    
    public function getJoomlaUsersToAddList($namesOnly=FALSE){
        //get dbo
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //select
        $query->select("ju.id,ju.name,ju.username,cu.id as cid,cu.published");
        $query->from("#__users AS ju");
        
        //left join essential data
        $query->leftJoin("#__crmery_users AS cu ON ju.id = cu.uid");
        
        //return results
        $db->setQuery($query);
        $results = $db->loadAssocList();

        $users = array();
        foreach ( $results as $key=>$user){
            if ( !$user['cid'] || $user['published'] == -1 ){
                if ( $namesOnly ){
                    $users[] = $user['name'];
                }else{
                    $users[$user['id']] = $user['name'];
                }
            }
        }
        return $users;
    }
    
    //return user team id
    function getTeamId($user_id){

        if ( $user_id > 0 ){
            //get db
            $db =& JFactory::getDBO();
            $query = $db->getQuery(true);
            
            //get id
            $query->select("team_id");
            $query->from("#__crmery_users");
            $query->where('id='.$user_id);
            
            //return id
            $db->setQuery($query);
            return $db->loadResult();
        }else{
            return 0;
        }
    }

    function delete($ids){
        //get db
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);

        $dispatcher =& JDispatcher::getInstance();
        $dispatcher->trigger('onBeforeCRMUserDelete', array(&$ids));            

        $query->update("#__crmery_users");
                if ( is_array($ids) ){
                    $query->where("id IN(".implode(','.$ids).")");
                }else{
                    $query->where("id=".$ids);
                }
        $query->set("published=-1");
        $db->setQuery($query);
        if ( $db->query() ){
     
            $dispatcher =& JDispatcher::getInstance();
            $dispatcher->trigger('onAfterCRMUserDelete', array(&$ids));            

            return true;
        }else{
            return false;
        }

    }

    

}

