<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 


jimport('joomla.application.component.model');

class CrmeryModelDocuments extends JModel
{
    /**
     * 
     *
     * @access  public
     * @return  void
     */
    function __construct()
    {
        parent::__construct();
        
    }
    
    function store($data=null)
    {
        //Load Tables
        $row =& JTable::getInstance('documents','Table');
        if($data==null){
            $data = JRequest::get( 'post' );
        }
        
        //date generation
        $date = date('Y-m-d H:i:s');
        
        if ( !array_key_exists('id',$data) ){
            $data['created'] = $date;
        }
        
        $data['modified'] = $date;
        $data['shared'] = 1;
        
        // Bind the form fields to the table
        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
     
        // Make sure the record is valid
        if (!$row->check()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
     
        // Store the web link table to the database
        if (!$row->store()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
        
        return true;
    }
    
    
    /**
     * Get list of stages
     * @param int $id specific search id
     * @return mixed $results results
     */
    function getDocuments($id=null){
        
        //database
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query->select("d.*");
        $query->from("#__crmery_documents AS d");
        $query->where("d.shared=1");
        
        //sort
        $query->order($this->getState('Documents.filter_order') . ' ' . $this->getState('Documents.filter_order_Dir'));
        if( $id ){
            $query->where("d.id=$id");
        }
        
        //return results
        $db->setQuery($query);
        $documents = $db->loadAssocList();

        return $documents;
        
    }
    
    function populateState(){
        //get states
        $app = JFactory::getApplication();
        $filter_order = $app->getUserStateFromRequest('Documents.filter_order','filter_order','d.filename');
        $filter_order_Dir = $app->getUserStateFromRequest('Documents.filter_order_Dir','filter_order_Dir','asc');
        
        //set states
        $this->setState('Documents.filter_order', $filter_order);
        $this->setState('Documents.filter_order_Dir',$filter_order_Dir);
    }
    
    function remove($id){
        //get dbo
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //delete id
        $query->delete('#__crmery_documents')->where('id = '.$id);
        $db->setQuery($query);
        $db->query();
    }

    

}