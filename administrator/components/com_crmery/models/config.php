<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.model');

class CrmeryModelConfig extends JModel
{

    /**
     * 
     *
     * @access  public
     * @return  void
     */
    function __construct()
    {
        parent::__construct();

    }

    
    function store($data=null)
    {
        //Load Tables
        $row =& JTable::getInstance('config','Table');
        $data = isset($data) && is_array($data) && count($data) > 0 ? $data : JRequest::get( 'post' );
        
        //date generation
        $date = date('Y-m-d H:i:s');

        $data['id'] = 1;
        
        if ( !array_key_exists('id',$data) ){
            $data['created'] = $date;
        }
        
        $data['modified'] = $date;

        if ( array_key_exists('imap_pass',$data) ){
            $data['imap_pass'] = base64_encode($data['imap_pass']);
        }

        $data['show_help'] = array_key_exists('show_help',$data) ? $data['show_help'] : 0;
        $data['acy_person'] = array_key_exists('acy_person',$data) ? $data['acy_person'] : 0;
        $data['acy_company'] = array_key_exists('acy_company',$data) ? $data['acy_company'] : 0;
        $data['fullscreen'] = array_key_exists('fullscreen',$data) ? $data['fullscreen'] : 0;
        $data['toolbar_help'] = array_key_exists('toolbar_help',$data) ? $data['toolbar_help'] : 0;
        
        // Bind the form fields to the table
        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
     
        // Make sure the record is valid
        if (!$row->check()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
     
        // Store the web link table to the database
        if (!$row->store()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
        
        return true;
    }

    function getConfig($array=FALSE){

        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);

        $query->select("*")->from("#__crmery_config")->where("id=1");
        $db->setQuery($query);

        if ( $array ){
            $config = $db->loadAssoc();
            $config['imap_pass'] = base64_decode($config['imap_pass']);
        }else{
            $config = $db->loadObject();
            $config->imap_pass = base64_decode($config->imap_pass);
        }
        
        return $config;

    }

    /**
     * Get an RSS feed for the Changelog on CRMery.com
     * @return array    The RSS feed and display parameters
     */
    function getUpdatesRSS()
    {
         $feed = array();
        
        // Parameters
        $feed['numItems']   = 2;
        $feed['Desc']       = 0;
        $feed['image']      = 0;
        $feed['itemDesc']   = 1;
        $feed['words']      = 60;
        $feed['title']      = 0;
        $feed['suffix']     = '';
        
        //  get RSS parsed object
        $options = array();
        $options['rssUrl']      = "http://www.crmery.com/changelog.html?format=feed&type=rss";
        $options['cache_time']  = 15 * 60;
        
        $rssDoc =& JFactory::getXml($options['rssUrl'], $options['cache_time']);
        $feed['doc'] = $rssDoc->channel;

        return $feed;
    }

    function populateState(){



    }

    function getNamingConventions(){

        $session =& JFactory::getSession();
        if ( $names = $session->get("Config.naming_conventions") ){

            return $names;

        } else {

            $names = self::getLanguage();
            $session->set("Config.naming_conventions",$names);
            return $names;

        }

    }

    function getLanguage(){
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select("lang_deal,lang_person,lang_company,lang_contact,lang_lead,lang_task,lang_event,lang_goal,lang_owner")
                ->from("#__crmery_config")
                ->where("id=1");
        $db->setQuery($query);
        $result =  $db->loadObject();

        $names = array();
        if ( count($result) > 0 ){
            foreach ( $result as $key => $value ){
                $names[$key] = $value;
                $names[CrmeryHelperConfig::pluralize($key)] = CrmeryHelperConfig::pluralize($value);
            }
        }
        return $names;
    }

}