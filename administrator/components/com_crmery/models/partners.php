<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

include_once(JPATH_COMPONENT.'/models/crmery.php');

class CrmeryModelPartners extends CrmeryModelCrmery
{

    var $id = null;

    /**
     * 
     *
     * @access  public
     * @return  void
     */
    function __construct()
    {
        parent::__construct();
        
    }
    
    function store()
    {
        //Load Tables
        $row =& JTable::getInstance('Companypartners','Table');		
		
        $data = JRequest::get( 'post' );
        
		//print_r($data);
		
		
        //date generation
        $date = date('Y-m-d H:i:s');
        if ( !array_key_exists('id',$data) ){
            $data['created'] = $date;
        }
        $data['modified'] = $date;
        
        //generate custom values
        $data['values'] = array_key_exists('values',$data) ? json_encode(($data['values'])) : "";
        
        //filter checkboxes
        if ( array_key_exists('required',$data) ){
            $data['required'] = ($data['required'] == 'on') ? 1 : 0;
        }else{
            $data['required'] = 0;
        }
        $data['multiple_selections'] = array_key_exists("multiple_selections",$data) ? 1 : 0;
        
        // Bind the form fields to the table
        if (!$row->bind($data)) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
     
        // Make sure the record is valid
        if (!$row->check()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
     
        // Store the web link table to the database
        if (!$row->store()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }
        
        return true;
    }

    function __buildQuery(){
        //database
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //query
        $query->select("c.*");
        $query->from("#__crmery_company_partners AS c");
        
        //sort
        $query->order($this->getState('Partners.filter_order') . ' ' . $this->getState('Partners.filter_order_Dir'));
        return $query;
    }
    
    
    /**
     * Get list of stages
     * @param int $id specific search id
     * @return mixed $results results
     */
    function getCustom($id=null){
        
        //database
        $db =& JFactory::getDBO();
        $query = $this->__buildQuery();
        
        if( $id ){
            $query->where("c.id=$id");
        }
        
        //return results
        $db->setQuery($query);
        $results = $db->loadAssocList();

        if ( count ( $results ) > 0 ){
            foreach ( $results as $key => $result ){
                $results[$key]['values'] = json_decode($result['values']);
            }
        }
        
        return $results;
        
    }
    
    function populateState(){
        //get states
        $app = JFactory::getApplication();
        $filter_order = $app->getUserStateFromRequest('Partners.filter_order','filter_order','c.name');
        $filter_order_Dir = $app->getUserStateFromRequest('Partners.filter_order_Dir','filter_order_Dir','asc');
        
        //set states
        $this->setState('Partners.filter_order', $filter_order);
        $this->setState('Partners.filter_order_Dir',$filter_order_Dir);
    }
    
    function remove($id){
        //get dbo
        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);
        
        //delete id
        $query->delete('#__crmery_company_partners')->where('id = '.$id);
        $db->setQuery($query);
        $db->query();
    }

    

}