var user_id = 0;

jQuery(document).ready(function(){

	jQuery("#type").change(function(){
		updateFields();
	});

	jQuery("#return_url").blur(function(){
		updateFields();
	});

	jQuery('#owner_id').autocomplete({
		source: function(request, response) {
	    var results = jQuery.ui.autocomplete.filter(user_list, request.term);
	    	response(results.slice(0, 10));
		},
		select:function(event,ui){
			user_id = 0;
			user_id = ui.item.value;
			jQuery("#owner_id_hidden").val(user_id);
			jQuery("#owner_id").val(ui.item.label);
			updateFields();
			return false;
		},
		search:function(event,ui){
			user_id = 0;
			jQuery("#owner_id_hidden").val('');
			updateFields();
			return;
		},
		change:function(event,ui){
			if ( user_id == 0 ){
				jQuery("#owner_id_hidden").val('');
				jQuery("#owner_id").val('');
			}
			updateFields();
		},
		close:function(event,ui){
			updateFields();
		}
	});

})

function showFieldCheckboxes(){
	jQuery("div.field_checkbox_container").hide();
	var type = jQuery("#type").val();
	jQuery("#"+type+"_fields").show();
}

function updateFields(){
	var html = "<form class='validate-form' action='"+base_url+"index.php?option=com_crmery&controller=ajax&task=saveWizardForm' method='POST'>\n";
	var type = jQuery("#type").val();
	jQuery.each(fields[type],function(fieldIndex,field){
		if ( jQuery("#"+type+"_field_"+fieldIndex).is(":checked") ){
			switch(field.type){
				case "text":
				case "number":
				case "currency":
					html += "\t<div class='row'>\n\t\t<label>"+field.display+"</label>\n\t\t<input type='text' name='"+field.name+"' />\n\t</div>\n";
				break;
				case "picklist":
					html += "\t<div class='row'>\n\t\t<label>"+field.display+"</label>\n\t\t<select name='"+field.name+"'>\n";
					jQuery.each(field.values,function(valueIndex,value){
						html += "\t\t\t<option value='"+valueIndex+"'>"+value+'</option>\n';
					});
					html += "\t\t</select>\n\t</div>\n";
				break;
			}
		}
	});
	var return_url = jQuery.base64.encode(jQuery("#return_url").val());
	var owner_id = jQuery("#owner_id_hidden").val();
	var form_id = jQuery("#form_id").val();

	html += '<div id="dynamic_recaptcha_1"></div>';
	html += '\t<input type="hidden" name="owner_id" value="'+owner_id+'" />\n';
	html += '\t<input type="submit" value="Submit" />\n';
	html += '\t<input type="hidden" name="save_type" value="'+type+'" />\n';
	if ( type == "contact" || type == "lead" ){
		html += '\t<input type="hidden" name="type" value="'+type+'" />\n';
	}
	html += '\t<input type="hidden" name="return" value="'+return_url+'" />\n';
	html += '\t<input type="hidden" name="form_id" value="'+form_id+'" />\n';
	html += '</form>\n';
	jQuery("#fields").val(html);
}	

