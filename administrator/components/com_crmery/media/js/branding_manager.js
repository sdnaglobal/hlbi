var tabsHoverCss = "";
var tabsHoverTextCss = "";
var old_link_style = "";

jQuery(document).ready(function(){
	
	//define our areas
	var customization_area = jQuery("#themes").children('ul').children('li').children('input:text');
	var customization_area_colorwheel = jQuery("#themes").children('ul').children('li').children('div.colorwheel');
	
	//assign input binds
	jQuery.each(customization_area,function(index,area){
		bindColorInputs(area);
	});
	
	//assign color wheel binds
	jQuery.each(customization_area_colorwheel,function(index,area){
		bindColorWheels(area);
	});
	
	//assign new custom area depending on radio select
	jQuery('input[name=id]').each(function(index){
		jQuery(this).bind('click',function(){changeTheme(jQuery(this).val())});
	});
	
	//assign the default theme
	changeTheme(assigned_theme);
	
});


/**
 * function to change theme
 */
function changeTheme(id){
	//update page html
	jQuery.when(jQuery("#customization_content").html(jQuery("#"+id).html()))
	
	//define new areas
	.then(function(){
		var customization_area = jQuery("#customization_content").children('ul').children('li').children('input:text');
		var customization_area_colorwheel = jQuery("#customization_content").children('ul').children('li').children('div.colorwheel');
		//assign input binds
		jQuery.each(customization_area,function(index,area){
			bindColorInputs(area);
		});
		//assign color wheel binds
		jQuery.each(customization_area_colorwheel,function(index,area){
			bindColorWheels(area);
		});
		
	})
	.then(function(){
		//update page css
		jQuery("#com_crmery_toolbar").css({'backgroundColor':"#"+jQuery("#customization_content input[name=header]").val()});
		jQuery("#com_crmery_toolbar ul.crmery_menu a").hover(
		  function () {
		    jQuery(this).css({'backgroundColor':"#"+jQuery("#customization_content input[name=tabs_hover]").val()});
		  },
		  function () {
		    var cssObj = {
		      'background-color' : 'transparent'
		    };
		    jQuery(this).css(cssObj);
		  }); 
		var old_tabs_hover_text = jQuery("#com_crmery_toolbar ul.crmery_menu a").css('color');
		jQuery("#com_crmery_toolbar ul.crmery_menu a").hover(
		  function () {
		    jQuery(this).css({'color':"#"+jQuery("#customization_content input[name=tabs_hover_text]").val()});
		  },
		  function () {
    	var cssObj = {
	      'color' : old_tabs_hover_text
				    };
		    jQuery(this).css(cssObj);
		  }); 
		jQuery(".com_crmery_table th").css({'backgroundColor':"#"+jQuery("#customization_content input[name=table_header_row]").val()});
		jQuery(".com_crmery_table th").css({'color':"#"+jQuery("#customization_contenet input[name=table_header_text]").val()});
	});
	
	
}

// bind color input events
function bindColorInputs(ele){
	var name = jQuery(ele).attr('name');
	jQuery(ele).css({'backgroundColor':"#"+jQuery(ele).val()});
	jQuery(ele).ColorPicker({
			color	: "#"+jQuery(ele).val(),
			onChange : function(rgb,hex){
				updateCss(name,hex);
				jQuery(ele).val(hex);
				jQuery(ele).css({'backgroundColor':"#"+hex});
			},
			onSubmit : function(rgb,hex){
				updateCss(name,hex);
				jQuery(ele).val(hex);
				jQuery(ele).css({'backgroundColor':"#"+hex});
			}
		});
}

//bind color wheel events
function bindColorWheels(ele){
	var parent_input = jQuery(ele).prev('input:text');
	var name = jQuery(ele).prev('input:text').attr('name');
	jQuery(parent_input).css({'backgroundColor':"#"+jQuery(ele).val()});
		jQuery(ele).ColorPicker({
			color	: "#"+jQuery(parent_input).val(),
			onChange : function(rgb,hex){
				updateCss(name,hex);
				jQuery(parent_input).val(hex);
				jQuery(parent_input).css({'backgroundColor':"#"+hex});
			},
			onSubmit : function(rgb,hex){
				updateCss(name,hex);
				jQuery(parent_input).val(hex);
				jQuery(parent_input).css({'backgroundColor':"#"+hex});
			}
		});
}

/**
 * dynamically updates the css on the page
 */
function updateCss(name,hex){
	
	//change the radio button
	jQuery('input:radio[name=id]')[1].checked = true;
	
	//update page css
	if ( name == 'header' ){
		jQuery("#com_crmery_toolbar").css({'backgroundColor':"#"+hex});
	}
	if ( name == 'tabs_hover' ){
		tabsHoverCss = "background:#"+hex+"!important";
		jQuery("#com_crmery_toolbar ul.crmery_menu a").hover(
		  function () {
		    jQuery(this).attr('style','background:#'+hex+" !important;"+tabsHoverTextCss);
		  },
		  function () {
		    var cssObj = {
		      'background-color' : 'transparent'
		    };
		    jQuery(this).css(cssObj);
		  }); 
	}
	if ( name == 'tabs_hover_text' ){
		var old_tabs_hover_text = jQuery("#com_crmery_toolbar ul.crmery_menu a").css('color');
		tabsHoverTextCss = "color:#"+hex+" !important";
		jQuery("#com_crmery_toolbar ul.crmery_menu li a").hover(
		  function () {
		    jQuery(this).attr('style','color:#'+hex+" !important;"+tabsHoverCss+";");
		  },
		  function () {
    	var cssObj = {
	      'color' : old_tabs_hover_text
				    };
		    jQuery(this).css(cssObj);
		  }); 
	}
	if ( name == 'table_header_row' ){
		jQuery(".com_crmery_table th").css({'backgroundColor':"#"+hex});
	}
	if ( name == 'table_header_text'){
		jQuery(".com_crmery_table th").css({'color':"#"+hex});
	}
	if ( name == "link" ){
		jQuery("#com_crmery a").attr('style','color:#'+hex+' !important;');
	}
	if ( name == "link_hover" ){
		jQuery("#user_functions a").hover(function(){
			jQuery(this).css({'color':'#'+hex});
		},function(){
			jQuery(this).css({'color':old_link_style});
		});
	}
}
