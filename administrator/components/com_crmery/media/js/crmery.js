var current_import = 0;

jQuery(document).ready(function(){
	jQuery(":checkbox").tooltip({
		placement:'right',
		trigger:'hover'
	});
	jQuery(":input[rel=tooltip]").tooltip({
		placement:'right',
		trigger:'focus'
	});
	jQuery("select[rel=tooltip]").tooltip({
		placement:'right',
		trigger:'focus'
	});
	jQuery("a[rel=tooltip]").tooltip({
		placement:'right',
	});

	//make people scrollable
	jQuery("#import_entries").scrollable({
		
	});

	if ( typeof window.show_tab !== 'undefined' ){
		showTab(show_tab);
	}
});

function selectTextarea(ele){
	if ( typeof ele == 'object' ){
		jQuery(ele).select();
	}else{
		if ( jQuery("#"+ele).is(":visible") ){
			jQuery("#"+ele).select();
		}else{
			setTimeout(function(){
				jQuery("#"+ele).select();	
			},1);
		}
	}
}

function showTooltip(idOrName){
	if ( jQuery("#"+idOrName).length > 0 ){
		jQuery("#"+idOrName).tooltip({
			placement:'right',
			trigger:'manual'
		});
		jQuery("#"+idOrName).tooltip('show');
	}else{
		jQuery("[name="+idOrName+"]").tooltip({
			placement:'right',
			trigger:'focus'
		});
		jQuery("[name="+idOrName+"]").tooltip('show');
	}
	jQuery("#"+idOrName).parent('a').tooltip('show');
}

function showTab(name){
		jQuery("a[href=#"+name+"]").tab('show');
}

function updateConfig(config_type,config_value){
	if ( config_value == 1 ){
		jQuery("#"+config_type.replace('.','\\.')).addClass('completed').removeClass('uncompleted');
		jQuery("#"+config_type.replace('.','\\.')).find('span.uncompleted').addClass('completed').removeClass('uncompleted');
		jQuery("#"+config_type.replace('.','\\.')).find('span.uncompleted-inner').addClass('completed-inner').removeClass('uncompleted-inner');
		jQuery.ajax({
			type:'POST',
			url:'index.php?option=com_crmery&controller=config&task=updateConfig&format=raw&tmpl=component',
			data:'show_help=1&'+config_type+"="+config_value,
			dataType:'JSON',
			success:function(data){

			}
		})
	}
	jQuery("#help_actions").fadeOut('fast');
}

function disableHelp(){
	jQuery.ajax({
			type:'POST',
			url:'index.php?option=com_crmery&controller=config&task=updateConfig&format=raw&tmpl=component',
			data:'show_help=0',
			dataType:'JSON',
			success:function(data){
				// jQuery("#disable_help_hidden").modal();
				jQuery("#help_description_action").slideUp('fast');
			}
		})
}

function downloadImportTemplate(ele){

	var form = jQuery(ele).parent('form');
	var old_action = jQuery(form).attr('action');

	jQuery(form).attr('action','index.php?option=com_crmery&controller=import&task=downloadImportTemplate&tmpl=component&format=raw');
	jQuery(form).submit();
	jQuery(form).attr('action',old_action);

}

function seekImport(seek){

	jQuery("#editForm").innerHeight(jQuery("#editForm").css('height').replace('px',''));

	var beginning = ( current_import == 0 && seek == -1 ) ? true : false;
	var end = ( current_import >= import_length-1 && seek == 1 ) ? true : false;

	if ( !beginning && !end ){

		jQuery("#import_entry_"+current_import).fadeOut('fast',function(){
			current_import += seek;
			jQuery("#viewing_entry").fadeOut('fast',function(){
				jQuery("#viewing_entry").html(current_import+1)
				jQuery("#viewing_entry").fadeIn('fast');
			})
			jQuery("#import_entry_"+current_import).fadeIn('fast');
		});

	}

}

function testImapConnection()
{

	jQuery("#connection_results").empty();
	jQuery("#connection_results").append("<div class='imap-connection alert alert-info'><span class='loading'></span></div>");

	var host 		= jQuery("input[name=imap_host]").val();
	var user 		= jQuery("input[name=imap_user]").val();
	var pass 		= jQuery("input[name=imap_pass]").val();
	var port 		= jQuery("input[name=imap_port]").val();
	var service 	= jQuery("input[name=imap_service]").val();

	var dataObj = {host:host,user:user,pass:pass,port:port,service:service};

	jQuery.ajax({
		url:'index.php?option=com_crmery&controller=ajax&task=testImapConnection&format=raw&tmpl=component',
		type:'POST',
		data:dataObj,
		dataType:'JSON',
		success:function(data)
		{
			jQuery("#connection_results").empty();
			if ( data.success ){
				jQuery("#connection_results").append('<div class="imap-connection alert alert-success"><b>'+data.msg+"</b></div>");
			}else{
				var errors = "";
				jQuery.each(data.errors,function(idx,msg){
					errors += '<p>'+msg+'</p>';
				});
				jQuery.each(data.alerts,function(idx,msg){
					errors += '<p>'+msg+'</p>';
				});
				jQuery("#connection_results").append('<div class="imap-connection alert alert-danger"><b>'+data.msg+"</b>"+errors+"</div>");
			}
		}
	})

}


	function getCountry(strURL) 
	{		
	  alert(strURL); return false;
	
		un=strURL
		//strURL="";
		//strURL+="index.php?option=com_crmery&controller=main&task=loadcountry&format=raw&region_id="+un;
		//alert(strURL);
		var req = getXMLHTTP();
		//alert(req);
		if (req) 
		{
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('country').innerHTML=req.responseText;
                        document.getElementById('nocountry').style.display='none';
                        document.getElementById('country').style.display='block';						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			//req.open("GET", strURL, true);
			//req.send(null);
			
			
			var url = "index.php?option=com_crmery&controller=main&task=loadcountry&format=raw&region_id="+un;
			var params = "region_id="+un;
			req.open("post", url, true);
			req.send(params);
			
			
		}		
	}
	
	

