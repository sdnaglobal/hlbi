jQuery(document).ready(function(){
	if ( typeof(status_color) !== 'undefined' ){
		jQuery("input[name=color]").css({'backgroundColor':"#"+status_color});
	}
	//assign color picker
	jQuery('input[name=color]').ColorPicker({
		color	:	status_color,
		onChange : function(rgb,hex){
			jQuery('input[name=color]').val(hex);
			jQuery("input[name=color]").css({'backgroundColor':"#"+hex});
		},
		onSubmit : function(rgb,hex){
			jQuery('input[name=color]').val(hex);
			jQuery("input[name=color]").css({'backgroundColor':"#"+hex});
		}
	});
	jQuery('#colorwheel').ColorPicker({
		color	:	status_color,
		onChange : function(rgb,hex){
			jQuery('input[name=color]').val(hex);
			jQuery("input[name=color]").css({'backgroundColor':"#"+hex});
		},
		onSubmit : function(rgb,hex){
			jQuery('input[name=color]').val(hex);
			jQuery("input[name=color]").css({'backgroundColor':"#"+hex});
		}
	});
});
