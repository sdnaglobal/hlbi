	/**
 * Globals
 */
var new_manager = false;
var user_id = 0;

//onready
jQuery(document).ready(function(){

	jQuery("#user_color").css({'backgroundColor':"#"+jQuery("#user_color").val()});
	jQuery("#user_color").ColorPicker({
		color	: "#"+jQuery(this).val(),
		onChange : function(rgb,hex){
			jQuery("#user_color").val(hex);
			jQuery("#user_color").css({'backgroundColor':"#"+hex});
		},
		onSubmit : function(rgb,hex){
			jQuery("#user_color").val(hex);
			jQuery("#user_color").css({'backgroundColor':"#"+hex});
		}
	});

	Joomla.submitbutton = function(task){
		if ( task == "Users.cancel" ){
			Joomla.submitform(task);
			return;
		}
		if ( save() ){
			Joomla.submitform(task);
		}else{
			if ( user_id == 0 ){
				alert(Joomla.JText._('COM_CRMERY_PLEASE_SELECT_A_USER'));
			}
		}
	}

	jQuery('#uid_name').autocomplete({
		source: function(request, response) {
        var results = jQuery.ui.autocomplete.filter(user_list_names, request.term);
        	response(results.slice(0, 10));
    	},
    	select:function(event,ui){
    		user_id = getUserId(ui.item.value);
    		updateUser(user_id);
    		jQuery("#uid").val(user_id);
    	},
    	search:function(event,ui){
    		user_id = getUserId(event.currentTarget.value);
    		updateUser(user_id);
    		jQuery("#uid").val(user_id);
    	},
    	
		// autoFocus:true,

	});

});

function getUserId(name){
	user_id = 0;
	jQuery.each(user_list,function(id,values){
		if ( values.name == name ){
			user_id = id;
		}
	})
	return user_id;
}

function save(){
	var valid = true;
	jQuery("#adminForm :input").each(function(){
		if ( jQuery(this).hasClass('required') && ( jQuery(this).val() == "" || jQuery(this).val() == 0 ) ){
			jQuery(this).focus();
			valid = false;
		}
	});
	return valid;
}

//prefill user data on page
function updateUser(id){
	jQuery("#uid").val('');
	jQuery("input[name=first_name]").val('');
	jQuery("input[name=last_name]").val('');
	jQuery("#email").val('');
	jQuery("#username").val('');
	if ( id ){
		jQuery("#uid").val(id);
		jQuery("input[name=first_name]").val(users[id].first_name);
		jQuery("input[name=last_name]").val(users[id].last_name);
		jQuery("#email").val(users[id].email);
		jQuery("#username").val(users[id].username);
	}
}

//update member and team data depending on selection
function updateRole(id){

	if ( id == "manager" ){
		jQuery("#team_name").show();
		
	    //jQuery("#regionBlock").hide();
		//jQuery("#countryBlock").hide();
		//jQuery("#stateBlock").hide(); 
		
	}else{
		jQuery("#team_name").hide();
		
		jQuery("#regionBlock").show();
		jQuery("#countryBlock").show();
		jQuery("#stateBlock").show();
	}
	
	//show team assignment
	if ( id == 'basic' ){
		jQuery("#team_assignment").show().removeClass('hidden');
		
		//jQuery("#regionBlock").hide();
		//jQuery("#countryBlock").hide();
		//jQuery("#stateBlock").hide(); 	
		
	}else{
		jQuery("#team_assignment").hide().addClass('hidden');		
		
	}
	
	//if we are downgrading a users access
	if ( role_type == 'manager' && id != 'manager' ){
		new_manager = true;
		jQuery("#manager_id").addClass('required');
		jQuery("#manager_assignment").show();
	}else{
		new_manager = false;
		jQuery("#manager_id").removeClass('required');
		jQuery("#manager_assignment").hide();
	}
	
	//reassign a team if we are changing role
	if ( role_type != id ){
		jQuery("select[name=team_id]").val('');
	}
	
}