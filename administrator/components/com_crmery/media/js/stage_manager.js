jQuery(document).ready(function(){
	
	jQuery("#percent").slider({
		
		max		:	100,
		min		:	0,
		value 	:	jQuery("input[name=percent]").val(),
		slide	:	function(event,ui){
			jQuery("input[name=percent]").val(ui.value);
			jQuery("#percent_value").html(ui.value+"%");
		},
		stop	:	function(event,ui){
			jQuery("input[name=percent]").val(ui.value);
			jQuery("#percent_value").html(ui.value+"%");
		},
		change	:	function(event,ui){
			jQuery("input[name=percent]").val(ui.value);
			jQuery("#percent_value").html(ui.value+"%");
		}
	});

	//define our areas
	var customization_area = jQuery("#adminForm input.color");
	var customization_area_colorwheel = jQuery("#adminForm div.colorwheel");

	//assign input binds
	jQuery.each(customization_area,function(index,area){
		bindColorInputs(area);
	});
	
	//assign color wheel binds
	jQuery.each(customization_area_colorwheel,function(index,area){
		bindColorWheels(area);
	});
	
});

// bind color input events
function bindColorInputs(ele){
	var name = jQuery(ele).attr('name');
	jQuery(ele).css({'backgroundColor':"#"+jQuery(ele).val()});
	jQuery(ele).ColorPicker({
			color	: "#"+jQuery(ele).val(),
			onChange : function(rgb,hex){
				jQuery(ele).val(hex);
				jQuery(ele).css({'backgroundColor':"#"+hex});
			},
			onSubmit : function(rgb,hex){
				updateCss(name,hex);
				jQuery(ele).val(hex);
				jQuery(ele).css({'backgroundColor':"#"+hex});
			}
		});
}

//bind color wheel events
function bindColorWheels(ele){
	var parent_input = jQuery(ele).prev('input:text');
	var name = jQuery(ele).prev('input:text').attr('name');
	jQuery(parent_input).css({'backgroundColor':"#"+jQuery(ele).val()});
		jQuery(ele).ColorPicker({
			color	: "#"+jQuery(parent_input).val(),
			onChange : function(rgb,hex){
				jQuery(parent_input).val(hex);
				jQuery(parent_input).css({'backgroundColor':"#"+hex});
			},
			onSubmit : function(rgb,hex){
				updateCss(name,hex);
				jQuery(parent_input).val(hex);
				jQuery(parent_input).css({'backgroundColor':"#"+hex});
			}
		});
}