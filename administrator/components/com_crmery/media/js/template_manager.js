jQuery(document).ready(function(){
	
	//bind button to add more choices for picklists
	bindAdd();
	//bind remove value button
	bindRemove();
	
});

//bind add to picklist
function bindAdd(){
	jQuery("#add_item").unbind();
	jQuery("#add_item").bind('click',function(){
		addValue();
	});
}

//bind picklist areas
function bindRemove(){
	var ele = jQuery("#items").children('.item:last');
	jQuery('.item').each(function(index){
		jQuery(this).find('.remove_item').unbind();
		jQuery(this).find('.remove_item').bind('click',function(){
			removeValue(jQuery(this).parentsUntil('div').parent('div'));
		})
	});
}

//add choices to the picklist
function addValue(){
	//append template
	jQuery("#items").append(jQuery("#item_template").html());
	//get the new entry
	bindRemove();
}

//remove entry choices
function removeValue(element){
	//remove the element
	element.remove();
}
