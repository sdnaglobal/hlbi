<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 
 class CrmeryHelperMenu extends JObject
 {
     
        //load the navigation menu
        function loadNavi(){
            /**/
            //add home page submenu
            JSubMenuHelper::addEntry(
                JText::_('COM_CRMERY_MAIN'),
                'index.php?option=com_crmery'
            );
            //add users submenu
            JSubMenuHelper::addEntry(
                JText::_('COM_CRMERY_USERS'),
                'index.php?option=com_crmery&view=users'
            );
        }

        function getMenuLinks(){
            return array( 
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=crmery'),
                        'class' => 'icon-home',
                        'text' => JText::_('CRMery Dashboard'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_DASHBOARD_MENU_TOOLTIP'),
                        'id'        => "dashboard_menu_link",
                        'view'      => "crmery"
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=users'),
                        'class' => 'icon-user',
                        'text' => JText::_('Users'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_USERS_MENU_TOOLTIP'),
                        'id'        => "user_menu_link",
                        'view'      => "users"
                    ),
					
					array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=partners'),
                        'class' => 'icon-user',
                        'text' => JText::_('Partners'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_PARTNERS_MENU_TOOLTIP'),
                        'id'        => "partner_menu_link",
                        'view'      => "partners"
                    ),
					
					
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=branding'),
                        'class' => 'icon-tint',
                        'text' => JText::_('Colors and Branding'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_COLORS_MENU_TOOLTIP'),
                        'id'        => "colors_menu_link",
                        'view'      => "branding"
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=stages'),
                        'class' => 'icon-tasks',
                        'text' => JText::_('Lead Stages'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_STAGES_MENU_TOOLTIP'),
                        'id'        => "stages_menu_link",
                        'view'      => "stages"
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=dealstatuses'),
                        'class' => 'icon-tasks',
                        'text' => JText::_('Lead Statuses'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_DEAL_STATUSES_MENU_TOOLTIP'),
                        'id'        => "deal_statuses_menu_link",
                        'view'      => "dealstatuses"
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=companycategories'),
                        'class' => 'icon-th-list',
                        'text' => JText::_('Firm Categories'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_COMPANY_CATEGORIES_MENU_TOOLTIP'),
                        'id'        => "company_categories_menu_link",
                        'view'      => "company_categories"
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=categories'),
                        'class' => 'icon-th-list',
                        'text' => JText::_('Note Categories'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_NOTES_MENU_TOOLTIP'),
                        'id'        => "notes_menu_link",
                        'view'      => "categories"
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=eventcategories'),
                        'class' => 'icon-th-list',
                        'text' => JText::_('Event Categories'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_EVENT_CATEGORIES_MENU_TOOLTIP'),
                        'id'        => "eventcategories_menu_link",
                        'view'      => "eventcategories"
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=sources'),
                        'class' => 'icon-random',
                        'text' => JText::_('Sources'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_SOURCES_MENU_TOOLTIP'),
                        'id'        => "sources_menu_link",
                        'view'      => "sources"
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=companycustom'),
                        'class' => 'icon-edit',
                        'text' => JText::_('Firm Custom Fields'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_CUSTOM_MENU_TOOLTIP'),
                        'id'        => "companycustom_menu_link",
                        'view'      => "companycustom"
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=peoplecustom'),
                        'class' => 'icon-edit',
                        'text' => JText::_('Referral Custom Fields'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_CUSTOM_MENU_TOOLTIP'),
                        'id'        => "peoplecustom_menu_link",
                        'view'      => "peoplecustom"
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=dealcustom'),
                        'class' => 'icon-edit',
                        'text' => JText::_('Lead Custom Fields'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_CUSTOM_MENU_TOOLTIP'),
                        'id'        => "dealcustom_menu_link",
                        'view'      => "dealcustom"
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=statuses'),
                        'class' => 'icon-thumbs-up',
                        'text' => JText::_('Referral Statuses'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_STATUSES_MENU_TOOLTIP'),
                        'id'        => "statuses_menu_link",
                        'view'      => "statuses"
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=templates'),
                        'class' => 'icon-filter',
                        'text' => JText::_('Workflow'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_WORKFLOW_MENU_TOOLTIP'),
                        'id'        => "workflow_menu_link",
                        'view'      => "templates"
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=documents'),
                        'class' => 'icon-folder-open',
                        'text' => JText::_('Shared Documents'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_DOCUMENTS_MENU_TOOLTIP'),
                        'id'        => "documents_menu_link",
                        'view'      => "documents"
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=menu'),
                        'class' => 'icon-align-justify',
                        'text' => JText::_('Menu'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_MENU_MENU_TOOLTIP'),
                        'id'        => "menu_menu_link",
                        'view'      => "menu"
                    ),
                     array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=import'),
                        'class' => 'icon-upload',
                        'text' => JText::_('Import'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_CONFIG_IMPORT_TOOLTIP'),
                        'id'        => "import_menu_link",
                        'view'      => "import"
                    ),
                     array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=formwizard'),
                        'class' => 'icon-star-empty',
                        'text' => JText::_('Form Wizard'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_FORMWIZARD_TOOLTIP'),
                        'id'        => "formwizard_menu_link",
                        'view'      => "formwizard"
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=config'),
                        'class' => 'icon-cog',
                        'text' => JText::_('Settings'),
                        'access' => array( ),
                        'tooltip'   => JText::_('COM_CRMERY_CONFIG_MENU_TOOLTIP'),
                        'id'        => "config_menu_link",
                        'view'      => "config"
                    )
                );
        }

        function getQuickMenuLinks(){
            return array( 
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&controller=users&task=add'),
                        'class' => 'icon-user',
                        'text' => JText::_('COM_CRMERY_ADD_NEW_USER'),
                        'access' => array( )
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&controller=stages&task=add'),
                        'class' => 'icon-tasks',
                        'text' => JText::_('COM_CRMERY_ADD_NEW_DEAL_STAGE'),
                        'access' => array( )
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&controller=categories&task=add'),
                        'class' => 'icon-th-list',
                        'text' => JText::_('COM_CRMERY_ADD_NEW_NOTE_CATEGORY'),
                        'access' => array( )
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&controller=sources&task=add'),
                        'class' => 'icon-random',
                        'text' => JText::_('COM_CRMERY_ADD_NEW_SOURCE'),
                        'access' => array( )
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&controller=companycustom&task=add'),
                        'class' => 'icon-edit',
                        'text' => JText::_('COM_CRMERY_ADD_NEW_COMPANY_CUSTOM_FIELD'),
                        'access' => array( )
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&controller=peoplecustom&task=add'),
                        'class' => 'icon-edit',
                        'text' => JText::_('COM_CRMERY_ADD_NEW_PEOPLE_CUSTOM_FIELD'),
                        'access' => array( )
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&controller=dealcustom&task=add'),
                        'class' => 'icon-edit',
                        'text' => JText::_('COM_CRMERY_ADD_NEW_DEAL_CUSTOM_FIELD'),
                        'access' => array( )
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&controller=statuses&task=add'),
                        'class' => 'icon-thumbs-up',
                        'text' => JText::_('COM_CRMERY_ADD_NEW_PERSON_STATUS'),
                        'access' => array( )
                    ),
                    array(
                        'link' => JRoute::_('index.php?option=com_crmery&controller=templates&task=add'),
                        'class' => 'icon-filter',
                        'text' => JText::_('COM_CRMERY_CREATE_NEW_WORKFLOW'),
                        'access' => array( )
                    )
                );
        }

        function getHelpMenuLinks(){

            $types = array(
                     array(
                        'link' => JRoute::_('index.php?option=com_crmery&controller=users&task=add&show_fields=uid'),
                        'class' => 'icon-user',
                        'text' => JText::_('COM_CRMERY_CREATE_NEW_USERS_HELP'),
                        'access' => array( ),
                        'config' => 'users_add',
                        'completed_status' => CrmeryHelperConfig::getConfigValue('users_add'),
                    ),
                     array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=config&layout=default&show_fields=timezone'),
                        'class' => 'icon-cog',
                        'text' => JText::_('COM_CRMERY_CREATE_LOCALE_HELP'),
                        'access' => array( ),
                        'config' => 'config_default',
                        'completed_status' => CrmeryHelperConfig::getConfigValue('config_default')
                    ),
                     array(
                        'link' => JRoute::_('index.php?option=com_crmery&controller=templates&task=edit&show_fields=name'),
                        'class' => 'icon-filter',
                        'text' => JText::_('COM_CRMERY_CREATE_WORKFLOWS_HELP'),
                        'access' => array( ),
                        'config' => 'templates_edit',
                        'completed_status' => CrmeryHelperConfig::getConfigValue('templates_edit')
                    ),
                     array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=menu&layout=default&show_fields=header'),
                        'class' => 'icon-align-justify',
                        'text' => JText::_('COM_CRMERY_CREATE_MENU_ITEMS_HELP'),
                        'access' => array( ),
                        'config' => 'menu_default',
                        'completed_status' => CrmeryHelperConfig::getConfigValue('menu_default')
                    ),
                     array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=import&layout=sample'),
                        'class' => 'icon-list-alt',
                        'text' => JText::_('COM_CRMERY_CREATE_INSTALL_SAMPLE'),
                        'access' => array( ),
                        'config' => 'import_sample',
                        'completed_status' => is_array(CrmeryHelperConfig::getConfigValue('import_sample',TRUE)) ? 1 : 0
                    ),
                     array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=import&layout=default'),
                        'class' => 'icon-share',
                        'text' => JText::_('COM_CRMERY_CREATE_IMPORT_HELP'),
                        'access' => array( ),
                        'config' => 'import_default',
                        'completed_status' => CrmeryHelperConfig::getConfigValue('import_default')
                    ),
                     array(
                        'link' => JRoute::_('index.php?option=com_crmery&view=launch&layout=default'),
                        'class' => 'icon-arrow-right',
                        'text' => JText::_('COM_CRMERY_CREATE_LAUNCH_HELP'),
                        'access' => array( ),
                        'config' => 'launch_default',
                        'completed_status' => CrmeryHelperConfig::getConfigValue('launch_default')
                    )
                );
        
            return $types;


        }


        function getMenuModules(){

            $modules = array();

            /** Main Menu Links **/
            $menu_links = CrmeryHelperMenu::getMenuLinks();
            $menu = CrmeryHelperView::getView('crmery','menu');
            $menu->assignRef('menu_links',$menu_links);
            $modules['menu'] = $menu;

            /** Quick Menu Links **/
            $quick_menu_links = CrmeryHelperMenu::getQuickMenuLinks();
            $quick_menu = CrmeryHelperview::getView('crmery','quick_menu');
            $quick_menu->assignRef('quick_menu_links',$quick_menu_links);
            $modules['quick_menu'] = $quick_menu;

            /** Determine help type on page **/
            $help_type_1 = JRequest::getVar('view') != '' || !is_null(JRequest::getVar('view')) ? JRequest::getVar('view') : JRequest::getVar('controller');
            $help_type_2 = JRequest::getVar('layout') != '' || !is_null(JRequest::getVar('layout')) ? JRequest::getVar('layout') : JRequest::getVar('task');
            $help_type_1 = ( $help_type_1 == "" || is_null($help_type_1) ) ? "" : $help_type_1;
            $help_type_2 = ( $help_type_2 == "" || is_null($help_type_2) ) ? "" : '_'.$help_type_2;
            $help_type = str_replace(".","_",$help_type_1.$help_type_2);
            $help_types = self::getHelpTypes();
            $show_help = CrmeryHelperConfig::getConfigValue('show_help');
            $launch_default = CrmeryHelperConfig::getConfigValue('launch_default');
            $step_completed = CrmeryHelperConfig::getConfigValue($help_type);
            $show_update_buttons = in_array($help_type,$help_types);

            /** Help Menu Links **/
            $help_menu_links = CrmeryHelperMenu::getHelpMenuLinks();
            $help_menu = CrmeryHelperview::getView('crmery','help_menu');
            $help_menu->assignRef('help_menu_links',$help_menu_links);
            $help_menu->assignRef('help_type',$help_type);
            $help_menu->assignRef('show_help',$show_help);
            $help_menu->assignRef('launch_default',$launch_default);
            $help_menu->assignRef('step_completed',$step_completed);
            $help_menu->assignRef('show_update_buttons',$show_update_buttons);
            $modules['help_menu'] = $help_menu;

            $count = count($help_menu_links)-1;
            $completed = 0;
            foreach ( $help_menu_links as $link ){
                if ( $link['completed_status'] == 1 ){
                    $completed++;
                }
            }
            $modules['percentage'] = number_format(($completed/$count)*100,0) ;

            /** Auto show input tooltips **/
            $input_fields = explode(',',JRequest::getVar('show_fields'));
            if ( count ( $input_fields ) > 0 ){ 
                $document = JFactory::getDocument();
                foreach ( $input_fields as $input_field){
                    $document->addScriptDeclaration(
                            "jQuery(document).ready(function(){showTooltip('".$input_field."');});"
                        );
                }
            }

            return $modules;

        }

        function getHelpTypes(){
            $links = self::getHelpMenuLinks();
            $types = array();
            foreach ( $links as $link ){
                $types[] = $link['config'];
            }
            return $types;
        }
        
 }
    