<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 
 class CrmeryHelperCpanel extends JObject
 {
     
        //load the navigation menu
        function getUsers(){
            return array ( 
                            array(
                                'link' => JRoute::_('index.php?option=com_crmery&view=users'),
                                'image' => JURI::base().'components/com_crmery/media/images/cpanel/users.png',
                                'text' => JText::_('Manage Users'),
                                'access' => array( )
                            )
                        );
        }
        
        function getCustom(){
            return array ( 
                            array(
                                'link' => JRoute::_('index.php?option=com_crmery&view=branding'),
                                'image' => JURI::base().'components/com_crmery/media/images/cpanel/branding.png',
                                'text' => JText::_('Colors and Branding'),
                                'access' => array( )
                            ),
                            array(
                                'link' => JRoute::_('index.php?option=com_crmery&view=stages'),
                                'image' => JURI::base().'components/com_crmery/media/images/cpanel/stages.png',
                                'text' => JText::_('Deal Stages'),
                                'access' => array( )
                            ),
                            array(
                                'link' => JRoute::_('index.php?option=com_crmery&view=categories'),
                                'image' => JURI::base().'components/com_crmery/media/images/cpanel/categories.png',
                                'text' => JText::_('Note Categories'),
                                'access' => array( )
                            ),
                            array(
                                'link' => JRoute::_('index.php?option=com_crmery&view=sources'),
                                'image' => JURI::base().'components/com_crmery/media/images/cpanel/sources.png',
                                'text' => JText::_('Sources'),
                                'access' => array( )
                            ),
                            array(
                                'link' => JRoute::_('index.php?option=com_crmery&view=custom'),
                                'image' => JURI::base().'components/com_crmery/media/images/cpanel/custom.png',
                                'text' => JText::_('Deal Custom Fields'),
                                'access' => array( )
                            ),
                            array(
                                'link' => JRoute::_('index.php?option=com_crmery&view=statuses'),
                                'image' => JURI::base().'components/com_crmery/media/images/cpanel/statuses.png',
                                'text' => JText::_('People Statuses'),
                                'access' => array( )
                            ),
                            array(
                                'link' => JRoute::_('index.php?option=com_crmery&view=templates'),
                                'image' => JURI::base().'components/com_crmery/media/images/cpanel/templates.png',
                                'text' => JText::_('Templates'),
                                'access' => array( )
                            ),
                            array(
                                'link' => JRoute::_('index.php?option=com_crmery&view=documents'),
                                'image' => JURI::base().'components/com_crmery/media/images/cpanel/documents.png',
                                'text' => JText::_('Shared Documents'),
                                'access' => array( )
                            ),
                            array(
                                'link' => JRoute::_('index.php?option=com_crmery&view=menu'),
                                'image' => JURI::base().'components/com_crmery/media/images/cpanel/menu.png',
                                'text' => JText::_('Menu'),
                                'access' => array( )
                            ),
                            array(
                                'link' => JRoute::_('index.php?option=com_crmery&view=config'),
                                'image' => JURI::base().'components/com_crmery/media/images/cpanel/config.png',
                                'text' => JText::_('Config'),
                                'access' => array( )
                            ),
                        );
        }

    function getMenu(){
        return array ( 
                            array(
                                'link' => JRoute::_('index.php?option=com_crmery&view=crmery'),
                                'image' => JURI::base().'components/com_crmery/media/images/cpanel/home.png',
                                'text' => JText::_(''),
                                'access' => array( )
                            ),
                            array(
                                'link' => JRoute::_('index.php?option=com_crmery&view=users'),
                                'image' => JURI::base().'components/com_crmery/media/images/cpanel/users.png',
                                'text' => JText::_(''),
                                'access' => array( )
                            )
                        );
    }

    function button($button){
        $html  = '<div class="icon-wrapper">';
        $html .= '<div class="icon">';
        $html .= '<a href="'.$button['link'].'">';
            $html .= JHtml::_('image', $button['image'], NULL, NULL, true);
            $html .= '<span>'.$button['text'].'</span></a>';
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }
 }
    