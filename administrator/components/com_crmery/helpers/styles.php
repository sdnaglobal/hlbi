<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

 
 class CrmeryHelperStyles extends JObject
 {
     
        //get base stylesheets
        function getBaseStyle(){
            return JURI::base()."../components/com_crmery/media/css/default.css";
        }
        
        //dynamically generate styles
        function getDynamicStyle(){
            //get default theme
            $model = JModel::getInstance('branding','CrmeryModel');
            $theme = $model->getDefaultTheme();
            $theme = $theme[0];
            //assign style declarations
            $style  = "#com_crmery_toolbar{background:#".$theme['header'].";}";
            $style .= "#com_crmery_toolbar ul.crmery_menu a:hover{background:#".$theme['tabs_hover'].";}";
            $style .= "#com_crmery_toolbar ul.crmery_menu a:hover{color:#".$theme['tabs_hover_text'].";}";
            $style .= "table.com_crmery_table th{background:#".$theme['table_header_row'].";}";
            $style .= "table.com_crmery_table th{color:#".$theme['table_header_text'].";}";
            //return
            return $style;
        }
        
        //load all styles
        function loadStyleSheets(){
            
            //base stylesheet
            $base_style = CrmeryHelperStyles::getBaseStyle();
            
            //dynamic stylesheet
            $dyn_style = CrmeryHelperStyles::getDynamicStyle();
            
            //add sheets to document
            $document =& JFactory::getDocument();
            $document->addStyleSheet($base_style);

            $document->addStyleDeclaration($dyn_style);
            
        }
        
 }
    