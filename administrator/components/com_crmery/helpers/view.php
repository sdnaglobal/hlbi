<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
 
 jimport( 'joomla.application.component.view');
 
 class CrmeryHelperView extends JView
 {
    
    var $layout     = null;
    var $view       = null;

    /*
     * Temporary Fix until Joomla! updates their core file
    */

    public function getName()
    {
        //hbd - joomla work around correction
        //$name = $this->_name = $this->name;
        $name = $this->name = $this->_name;

        if (empty($name))
        {
            $r = null;
            if (!preg_match('/View((view)*(.*(view)?.*))$/i', get_class($this), $r)) {
                JError::raiseError (500, JText::_('JLIB_APPLICATION_ERROR_VIEW_GET_NAME'));
            }
            if (strpos($r[3], "view"))
            {
                JError::raiseWarning('SOME_ERROR_CODE', JText::_('JLIB_APPLICATION_ERROR_VIEW_GET_NAME_SUBSTRING'));
            }
            $name = strtolower($r[3]);
        }

        return $name;
    }


    function getView($view,$layout='default',$ref=null){
        
        //set view and layout
        $cfg = array ( 'name' => $view );
        $view = new CrmeryHelperView($cfg);
        $view->setLayout($layout);

        //pass data to view
        $n = count($ref);
        
        for ( $i=0; $i<$n; $i++ ) {
            $assign = $ref[$i];
            $view->assignRef($assign['ref'],$assign['data']);
        }
        //return view
        return $view;       
        
    }

    function display($tpl=null)
    {
        parent::display($tpl);
    }
        
        
 }
    