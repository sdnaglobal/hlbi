<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerUsers extends CrmeryController
{

    var $id = null;

    /**
     * constructor (registers additional tasks to methods)
     * @return void
     */
    function __construct()
    {
        parent::__construct();
        //get post data
        $array = JRequest::getVar('cid',  0, '', 'array');
        if ( !($this->id = JRequest::getVar('id')) ){
            if ( count($array) > 1 ){
                $this->id = $array;
            }else{
                $this->id = (int)$array[0];
            }
        }

        JRequest::setVar('view','users');
    }
    
    function add(){
        $this->id=0;
        $this->edit();
    }
    
    function edit(){
        //set layout
        $view = JController::getView('Users','html');
        $view->setLayout('edit');
        
        //add javascript
        $document =& JFactory::getDocument();
        $document->addScript(JURI::base().'components/com_crmery/media/js/user_manager.js');
        
        //if we get post data generate data for view
        if ( $this->id ){
            //get user info
            $model = $this->getModel('Users');
            $user = $model->getUsers($this->id);
            $user = $user[0];
            $header = JText::_('COM_CRMERY_EDITING_USER') . $user['first_name'] . ' ' . $user['last_name'];
            $document->addScriptDeclaration('var user_id='.$user['id'].';');
        }else{
            //get joomla users to add
            $model = $this->getModel('Users');
            $user_list = $model->getJoomlaUsersToAdd();
            $user_list_names = $model->getJoomlaUsersToAddList(TRUE);
            $document->addScriptDeclaration('var user_list='.json_encode($user_list).';');
            $document->addScriptDeclaration('var user_list_names='.json_encode($user_list_names).';');
            $users = $model->getJoomlaUsersToAdd();
            $header = JText::_('COM_CRMERY_ADDING_USER');
        }
        
        //assign references
        $view->assignRef('header',$header);
        $view->assignRef('user',$user);
        $view->assignRef('users',$users);
        
        //display view
        $view->display();
    }
    
    function cancel(){
        $msg = JText::_('User entry cancelled!');
        $this->setRedirect('index.php?option=com_crmery&view=users',$msg);
    }
    
    function save(){
        $model = $this->getModel('Users');
        if ( $model->store() ) {
            $msg = "Successfully saved!";
        }else{
            $msg = "Error Saving!";
        }
        $this->setRedirect('index.php?option=com_crmery&view=users',$msg);
    }

    function delete(){
        $model = $this->getModel('Users');
        if ( $model->delete($this->id) ){
            $msg = JText::_('COM_CRMERY_DELETED_USER');
        }else{
            $msg = JText::_('COM_CRMERY_ERROR_DELETING_USER');
        }
        $this->setRedirect('index.php?option=com_crmery&view=users',$msg);
    }
    
}