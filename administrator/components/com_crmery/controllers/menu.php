<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerMenu extends CrmeryController
{
    /**
     * constructor (registers additional tasks to methods)
     * @return void
     */
    function __construct()
    {
        parent::__construct();
        //get post data
        $array = JRequest::getVar('cid',  0, '', 'array');
        if ( !($this->id = JRequest::getVar('id')) ){
            if ( count($array) > 1 ){
                $this->id = $array;
            }else{
                $this->id = (int)$array[0];
            }
        }
        JRequest::setVar('view','menu');
    }
    
    function cancel(){
        $msg = JText::_('Menu cancelled!');
        $this->setRedirect('index.php?option=com_crmery',$msg);
    }
    
    function save(){
        $model = $this->getModel('Menu');
        if ( $model->store() ) {
            $msg = "Successfully saved!";
        }else{
            $msg = "Error Saving!";
        }
        $this->setRedirect('index.php?option=com_crmery&view=menu',$msg);
    }
    
}