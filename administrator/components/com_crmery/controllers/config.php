<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerConfig extends CrmeryController
{
    /**
     * constructor (registers additional tasks to methods)
     * @return void
     */
    function __construct()
    {
        parent::__construct();
        //get post data
        $array = JRequest::getVar('cid',  0, '', 'array');
        if ( !($this->id = JRequest::getVar('id')) ){
            if ( count($array) > 1 ){
                $this->id = $array;
            }else{
                $this->id = (int)$array[0];
            }
        }
        $view = JRequest::getVar('view') ? JRequest::getVar('view') : "config";
        JRequest::setVar('view',$view);
    }
    
    function cancel(){
        $msg = JText::_('Config cancelled!');
        $this->setRedirect('index.php?option=com_crmery',$msg);
    }
    
    function save(){
        $model = $this->getModel('Config');
        if ( $model->store() ) {
            $msg = "Successfully saved!";
        }else{
            $msg = "Error Saving!";
        }
        $view = JRequest::getVar('view') ? 'index.php?option=com_crmery&view='.JRequest::getVar('view') : 'index.php?option=com_crmery&view=config';
        if ( JRequest::getVar('show_launch_message') ){
            $view .= '&launch=1';
        }
        $this->setRedirect($view,$msg);
    }

    function updateConfig(){
        $model = $this->getModel('Config');
        if ( $model->store() ) {
            $error = 0;
        }else{
            $error = 1;
        }
        $return = array('error'=>$error);
        echo json_encode($return);
    }
    
}