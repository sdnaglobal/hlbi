<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerDocuments extends CrmeryController
{
    /**
     * constructor (registers additional tasks to methods)
     * @return void
     */
    function __construct()
    {
        parent::__construct();
        //get post data
        $array = JRequest::getVar('cid',  0, '', 'array');
        if ( !($this->id = JRequest::getVar('id')) ){
            if ( count($array) > 1 ){
                $this->id = $array;
            }else{
                $this->id = (int)$array[0];
            }
        }
        JRequest::setVar('view','documents');
    }
    
    function add(){
        $this->id=0;
        $this->edit();
    }
    
    function edit(){
        //set layout
        $view = JController::getView('Documents','html');
        $view->setLayout('edit');
        
        //add javascript
        $document =& JFactory::getDocument();
        $document->addScript(JURI::base().'components/com_crmery/media/js/document_manager.js');
        
        //get stage info
        if ( $this->id ){
            $model = $this->getModel('Documents');
            $document = $model->getDocuments($this->id);
            $document = $document[0];
            $header = JText::_('COM_CRMERY_EDITING_DOCUMENT') . $document['filename'];
        }else{
            $document = null;
            $header = JText::_('COM_CRMERY_ADDING_DOCUMENT');
        }
        
        //assign references
        $view->assignRef('header',$header);
        $view->assignRef('document',$document);
        
        //display view
        $view->display();
    }
    
    function cancel(){
        $msg = JText::_('Document entry cancelled!');
        $this->setRedirect('index.php?option=com_crmery&view=documents',$msg);
    }
    
    function save(){
        $model = $this->getModel('Documents');
        if ( $model->store() ) {
            $msg = JText::_('COM_CRMERY_SUCCESS');
        }else{
            $msg = JText::_('COM_CRMERY_ERROR');
        }
        $this->setRedirect('index.php?option=com_crmery&view=documents',$msg);
    }
    
    function remove(){
        $model = $this->getModel('Documents');
        if ( is_array($this->id) ){
            foreach( $this->id as $id ){
                $model->remove($id);
            }
        }else{
            $model->remove($this->id);
        }
        $msg = JText::_('COM_CRMERY_DOCUMENT_REMOVED');
        $this->setRedirect('index.php?option=com_crmery&view=documents',$msg);
    }
    
    function upload(){
        //import joomlas filesystem functions, we will do all the filewriting with joomlas functions,
            //so if the ftp layer is on, joomla will write with that, not the apache user, which might
            //not have the correct permissions
            jimport('joomla.filesystem.file');
            jimport('joomla.filesystem.folder');
             
            //this is the name of the field in the html form, filedata is the default name for swfupload
            //so we will leave it as that
            $fieldName = 'document';
            
            //any errors the server registered on uploading
            $fileError = $_FILES[$fieldName]['error'];
            if ($fileError > 0) 
            {
                    switch ($fileError) 
                {
                    case 1:
                    echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
                    return;
             
                    case 2:
                    echo JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
                    return;
             
                    case 3:
                    echo JText::_( 'ERROR PARTIAL UPLOAD' );
                    return;
             
                    case 4:
                    echo JText::_( 'ERROR NO FILE' );
                    return;
                    }
            }
             
            //check for filesize
            $fileSize = $_FILES[$fieldName]['size'];
            if($fileSize > 2000000)
            {
                echo JText::_( 'FILE BIGGER THAN 2MB' );
            }
             
            //check the file extension is ok
            $fileName = $_FILES[$fieldName]['name'];
            $uploadedFileNameParts = explode('.',$fileName);
            $uploadedFileExtension = array_pop($uploadedFileNameParts);
             
            $validFileExts = explode(',', 'jpeg,jpg,png,gif,pdf,doc,docx,odt,rtf,ppt,xls,txt');
             
            //assume the extension is false until we know its ok
            $extOk = false;
             
            //go through every ok extension, if the ok extension matches the file extension (case insensitive)
            //then the file extension is ok
            foreach($validFileExts as $key => $value)
            {
                if( preg_match("/$value/i", $uploadedFileExtension ) )
                {
                    $extOk = true;
                }
            }
             
            if ($extOk == false) 
            {
                echo JText::_( 'INVALID EXTENSION' );
                    return;
            }
             
            //the name of the file in PHP's temp directory that we are going to move to our folder
            $fileTemp = $_FILES[$fieldName]['tmp_name'];
             
            //for security purposes, we will also do a getimagesize on the temp file (before we have moved it 
            //to the folder) to check the MIME type of the file, and whether it has a width and height
            $imageinfo = getimagesize($fileTemp);
             
            //we are going to define what file extensions/MIMEs are ok, and only let these ones in (whitelisting), rather than try to scan for bad
            //types, where we might miss one (whitelisting is always better than blacklisting) 
            // $okMIMETypes = 'image/jpeg,image/pjpeg,image/png,image/x-png,image/gif';
            // $validFileTypes = explode(",", $okMIMETypes);       
             
            //if the temp file does not have a width or a height, or it has a non ok MIME, return
            // if( !is_int($imageinfo[0]) || !is_int($imageinfo[1]) ||  !in_array($imageinfo['mime'], $validFileTypes) )
            // {
                // echo JText::_( 'INVALID FILETYPE' );
                    // return;
            // }
             
            //lose any special characters in the filename
            $fileName = ereg_replace("[^A-Za-z0-9.]", "-", $fileName);
            $hash = md5($fileName).".".$uploadedFileExtension;
             
            //always use constants when making file paths, to avoid the possibilty of remote file inclusion
            $uploadPath = JPATH_SITE.'/components/com_crmery/documents/'.$hash;
             
            if(!JFile::upload($fileTemp, $uploadPath)) 
            {
                $msg = JText::_('COM_CRMERY_DOC_UPLOAD_FAIL');
                $this->setRedirect('index.php?option=com_crmery&view=documents',$msg);
            }
            else
            {
               //update the database
               //date generation
               $date = date('Y-m-d H:i:s');
               $data = array (
                            'name'              =>  $fileName,
                            'filename'          =>  $hash,
                            'filetype'          =>  $uploadedFileExtension,
                            'size'              =>  $fileSize/1024,
                            'created'           =>  $date,
                            'shared'            =>  1,
                            'is_image'          =>  is_array(getimagesize($uploadPath)) ? true : false
                            );        
                
               $model = & JModel::getInstance('documents','CrmeryModel');
               $session =& JFactory::getSession();
               
               if($model->store($data)){
                   $msg = JText::_('COM_CRMERY_DOC_UPLOAD_SUCCESS');
                   $this->setRedirect('index.php?option=com_crmery&view=documents&layout=upload&tmpl=component',$msg);
                   $session->set("upload_success", true);
               }else{
                   $msg = JText::_('COM_CRMERY_DOC_UPLOAD_FAIL');
                   $this->setRedirect('index.php?option=com_crmery&view=documents&layout=upload&tmpl=component',$msg);
                   $session->set("upload_success", false);
               }
            }
    }
    
}