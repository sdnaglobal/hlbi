<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerImport extends CrmeryController
{
    /**
     * constructor (registers additional tasks to methods)
     * @return void
     */
    function __construct()
    {
        parent::__construct();
        //get post data
        $array = JRequest::getVar('cid',  0, '', 'array');
        if ( !($this->id = JRequest::getVar('id')) ){
            if ( count($array) > 1 ){
                $this->id = $array;
            }else{
                $this->id = (int)$array[0];
            }
        }
        JRequest::setVar('view','import');
        JRequest::setVar('layout', JRequest::getVar('layout') ? JRequest::getVar('layout')  : "default");
    }


    /**
    * Download example import templates
    * @return [type] [description]
    */
   function downloadImportTemplate(){

        $template_type = JRequest::getVar('template_type');
    
        $path = JPATH_SITE.'/components/com_crmery/media/import_templates/import_'.$template_type.'.csv';

        ob_start();
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="import_'.$template_type.'.csv"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($path));
        ob_clean();
        flush();

        readfile($path);

        exit();

   }

    function installSampleData()
    {
        $sampleIds = array();

        $importModel =& JModel::getInstance('Import','CrmeryModel');
        $sampleCsvFiles = array(
                'sample-company'    => "companies",
                'sample-person'     => "people",
                'sample-deal'       => "deals"
                // 'sample-event'      => "events",
                // 'sample-goal'       => "goals",
            );
        foreach ($sampleCsvFiles as $file => $table ){
            $importData = $importModel->readCSVFile(JPATH_SITE.'/components/com_crmery/sample/'.$file.'.csv',$table,FALSE);
            switch ( $table ){
                case "companies":
                    $model = "company";
                break;
                case "people":
                    $model = "people";
                break;
                case "deals":
                    $model = "deal";
                break;
            }
            unset($importData['headers']);
            $ids = $importModel->importCSVData($importData,$model,TRUE);
            $sampleIds[$table] = $ids;
        }

        $data = array('import_sample'=>serialize($sampleIds));

        $configModel =& JModel::getInstance('Config','CrmeryModel');
        $configModel->store($data);

        $msg = JText::_('COM_CRMERY_SAMPLE_DATA_INSTALLED');
        $this->setRedirect('index.php?option=com_crmery&view=import',$msg);

    }

    function removeSampleData(){

        $sampleIds = unserialize(CrmeryHelperConfig::getConfigValue('import_sample'));

        $db =& JFactory::getDBO();
        $query = $db->getQuery(true);

        foreach ( $sampleIds as $table => $ids ){
                $query->clear()
                    ->delete("#__crmery_".$table)
                    ->where("id IN(".implode(',',$ids).")");
                $db->setQuery($query);
                $db->query();
        }

        $data = array('import_sample'=>"0");

        $configModel =& JModel::getInstance('Config','CrmeryModel');
        $configModel->store($data);

        $msg = JText::_('COM_CRMERY_SAMPLE_DATA_REMOVED');
        $this->setRedirect('index.php?option=com_crmery&view=import',$msg);

    }

        /**
         * Import CSV Files into database
         */
        function import(){
            $success = false;
            $data = JRequest::get('post');
            $session = JFactory::getSession();
            if ( is_array($data) && count($data) > 0 ){
                $import_type = $data['import_type'];
                unset($data['import_type']);
                switch($import_type){
                case "companies":
                    $import_model = "company";
                    break;
                case "deals":
                    $import_model = "deal";
                    break;
                case "people":
                    $import_model = "people";  
                    break;
                }
                if ( isset($import_model) ) {       
                    $model =& JModel::getInstance("Import","CrmeryModel");
                    $success = $model->importCSVData($data['import_id'],$import_model);
                }
            }
            if ( $success == "redirect" ){
                $app = JFactory::getApplication();
                $app->redirect("index.php?option=com_crmery&controller=import&task=batch");
            }

            $type = $session->get("batch_type");
            switch ( $type ){
                case "company":
                    $typeView = "companies";
                break;
                case "deal":
                    $typeView = "deals";
                break;
                case "people":
                    $typeView = "people";
                break;
            }
            $view = $type ? $typeView : "";
            $view = $view == "" ? $view : "&view=".$view;

            if ( $success == true ){
                $msg = CRMText::_('COM_CRMERY_IMPORT_WAS_SUCCESSFUL');
                $this->setRedirect(JRoute::_('index.php?option=com_crmery'.$view),$msg);
            }else{
                $msg = CRMText::_('COM_CRMERY_ERROR_IMPORTING');
                $this->setRedirect(JRoute::_('index.php?option=com_crmery'.$view),$msg);
            }
        }

        function batch(){

            $session = JFactory::getSession();
            $success = true;
            $app = JFactory::getApplication();

            if (JFile::exists(JPATH_BASE."/tmp/crmery_import.php")){
                include_once(JPATH_BASE."/tmp/crmery_import.php");
            }

            if ( isset($batchData) && count($batchData) > 0 ){
                $model =& JModel::getInstance("Import","CrmeryModel");
                $success = $model->importCSVData($batchData,$batchType);
            }

            if ( $success == "redirect" && JFile::exists(JPATH_BASE."/tmp/crmery_import.php") ){
                $app->redirect("index.php?option=com_crmery&controller=import&task=batch");
            }

            $type = $session->get("batch_type");
            switch ( $type ){
                case "company":
                    $typeView = "companies";
                break;
                case "deal":
                    $typeView = "deals";
                break;
                case "people":
                    $typeView = "people";
                break;
            }
            $view = $type ? $typeView : "";

            jimport("joomla.filesystem.file");
            if (JFile::exists(JPATH_BASE."/tmp/crmery_import.php")){
                JFile::delete(JPATH_BASE."/tmp/crmery_import.php");
            }

            if ( $success == true ){
                $msg = CRMText::_('COM_CRMERY_IMPORT_WAS_SUCCESSFUL');
                $app->redirect('index.php?option=com_crmery&view='.$view);
            }else{
                $msg = CRMText::_('COM_CRMERY_ERROR_IMPORTING');
                $app->redirect('index.php?option=com_crmery&view='.$view);
            }

            $view = $view == "" ? $view : "&view=".$view;
            $app->redirect('index.php?option=com_crmery'.$view);

        }

}