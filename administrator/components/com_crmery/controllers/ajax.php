<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerAjax extends JController
{

	function testImapConnection()
	{
		$return = array('success'=>false,'msg'=>JText::_('COM_CRMERY_CONNETION_FAILURE'));

		$host 	 = JRequest::getVar('host');
		$user 	 = JRequest::getVar('user');
		$pass 	 = JRequest::getVar('pass');
		$service = JRequest::getVar('service');
		$port    = JRequest::getVar("port");

		JModel::addIncludePath(JPATH_SITE."/components/com_crmery/models");
		$model = JModel::getInstance("Mail","CrmeryModel");
		if ( $model->testConnection($host,$user,$pass,$service,$port) ){
			$return['success'] = true;
			$return['msg'] = JText::_('COM_CRMERY_CONNECTION_SUCCESS');
		}else{
			$return['success'] = false;
			$return['msg'] = JText::_('COM_CRMERY_CONNETION_FAILURE');
			$return['errors'] = $model->getErrors();
			$return['alerts'] = $model->getAlerts();
		}

		echo json_encode($return);

	}

}
