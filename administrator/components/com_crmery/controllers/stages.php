<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.controlleradmin');

class CrmeryControllerStages extends JControllerAdmin
{
    /**
     * constructor (registers additional tasks to methods)
     * @return void
     */
    function __construct()
    {
        parent::__construct();
        //get post data
        $array = JRequest::getVar('cid',  0, '', 'array');
        if ( !($this->id = JRequest::getVar('id')) ){
            if ( count($array) > 1 ){
                $this->id = $array;
            }else{
                $this->id = (int)$array[0];
            }
        }
        JRequest::setVar('view','stages');
    }
    
    function add(){
        $this->id=0;
        $this->edit();
    }
    
    function edit(){
        //set layout
        $view = JController::getView('Stages','html');
        $view->setLayout('edit');
        
        //add javascript
        $document =& JFactory::getDocument();
        $document->addScript(JURI::base().'components/com_crmery/media/js/stage_manager.js');
        
        //get stage info
        if ( $this->id ){
            $model = $this->getModel('Stages');
            $stage = $model->getStages($this->id);
            $stage = $stage[0];
            $header = JText::_('COM_CRMERY_EDITING_STAGE') . $stage['name'];
        }else{
            $stage = null;
            $header = JText::_('COM_CRMERY_ADDING_STAGE');
        }
        
        //assign references
        $view->assignRef('header',$header);
        $view->assignRef('stage',$stage);
        
        //display view
        $view->display();
    }
    
    function cancel(){
        $msg = JText::_('Stage entry cancelled!');
        $this->setRedirect('index.php?option=com_crmery&view=stages',$msg);
    }
    
    function save(){
        $model = $this->getModel('Stages');
        if ( $model->store() ) {
            $msg = JText::_('COM_CRMERY_SUCCESS');
        }else{
            $msg = JText::_('COM_CRMERY_ERROR');
        }
        $this->setRedirect('index.php?option=com_crmery&view=stages',$msg);
    }
    
    function remove(){
        $model = $this->getModel('Stages');
        if ( is_array($this->id) ){
            foreach( $this->id as $id ){
                $model->remove($id);
            }
        }else{
            $model->remove($this->id);
        }
        $msg = JText::_('COM_CRMERY_STAGES_REMOVED');
        $this->setRedirect('index.php?option=com_crmery&view=stages',$msg);
    }
    
}