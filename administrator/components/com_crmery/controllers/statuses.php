<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.controlleradmin');

class CrmeryControllerStatuses extends JControllerAdmin
{
    /**
     * constructor (registers additional tasks to methods)
     * @return void
     */
    function __construct()
    {
        parent::__construct();
        //get post data
        $array = JRequest::getVar('cid',  0, '', 'array');
        if ( !($this->id = JRequest::getVar('id')) ){
            if ( count($array) > 1 ){
                $this->id = $array;
            }else{
                $this->id = (int)$array[0];
            }
        }
        JRequest::setVar('view','statuses');
    }
    
    function add(){
        $this->id=0;
        $this->edit();
    }
    
    function edit(){
        //set layout
        $view = JController::getView('Statuses','html');
        $view->setLayout('edit');
        
        //add javascript
        $document =& JFactory::getDocument();
        $document->addScript(JURI::base().'components/com_crmery/media/js/status_manager.js');
        
        //get stage info
        if ( $this->id ){
            $model = $this->getModel('Statuses');
            $status = $model->getStatuses($this->id);
            $status = $status[0];
            $header = JText::_('COM_CRMERY_EDITING_STATUS') . $status['name'];
        }else{
            $source = null;
            $header = JText::_('COM_CRMERY_ADDING_STATUS');
        }
        
        //assign references
        $view->assignRef('header',$header);
        $view->assignRef('status',$status);
        
        //display view
        $view->display();
    }
    
    function cancel(){
        $msg = JText::_('Status entry cancelled!');
        $this->setRedirect('index.php?option=com_crmery&view=statuses',$msg);
    }
    
    function save(){
        $model = $this->getModel('Statuses');
        if ( $model->store() ) {
            $msg = JText::_('COM_CRMERY_SUCCESS');
        }else{
            $msg = JText::_('COM_CRMERY_ERROR');
        }
        $this->setRedirect('index.php?option=com_crmery&view=statuses',$msg);
    }
    
    function remove(){
        $model = $this->getModel('Statuses');
        if ( is_array($this->id) ){
            foreach( $this->id as $id ){
                $model->remove($id);
            }
        }else{
            $model->remove($this->id);
        }
        $msg = JText::_('COM_CRMERY_STATUS_REMOVED');
        $this->setRedirect('index.php?option=com_crmery&view=statuses',$msg);
    }
    
}