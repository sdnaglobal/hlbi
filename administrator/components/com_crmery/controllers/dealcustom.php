<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.controlleradmin');

class CrmeryControllerDealcustom extends JControllerAdmin
{
    /**
     * constructor (registers additional tasks to methods)
     * @return void
     */
    function __construct()
    {
        parent::__construct();
        //get post data
        $array = JRequest::getVar('cid',  0, '', 'array');
        if ( !($this->id = JRequest::getVar('id')) ){
            if ( count($array) > 1 ){
                $this->id = $array;
            }else{
                $this->id = (int)$array[0];
            }
        }
        JRequest::setVar('view','dealcustom');
    }
    
    function add(){
        $this->id=0;
        $this->edit();
    }
    
    function edit(){
        //set layout
        $view = JController::getView('Dealcustom','html');
        $view->setLayout('edit');
        
        //add javascript
        $document =& JFactory::getDocument();
        $document->addScript(JURI::base().'components/com_crmery/media/js/custom_manager.js');
        
        //get stage info
        if ( $this->id ){
            $model = $this->getModel('Dealcustom');
            $custom = $model->getCustom($this->id);
            $custom = $custom[0];
            $header = JText::_('COM_CRMERY_EDITING_CUSTOM') . $custom['name'];
        }else{
            $custom = null;
            $header = JText::_('COM_CRMERY_ADDING_CUSTOM');
        }
        
        //assign references
        $view->assignRef('header',$header);
        $view->assignRef('custom',$custom);
        
        //display view
        $view->display();
    }
    
    function cancel(){
        $msg = JText::_('Custom field entry cancelled!');
        $this->setRedirect('index.php?option=com_crmery&view=dealcustom',$msg);
    }
    
    function save(){
        $model = $this->getModel('Dealcustom');
        if ( $model->store() ) {
            $msg = JText::_('COM_CRMERY_SUCCESS');
        }else{
            $msg = JText::_('COM_CRMERY_ERROR');
        }
        $this->setRedirect('index.php?option=com_crmery&view=dealcustom',$msg);
    }
    
    function remove(){
        $model = $this->getModel('Dealcustom');
        if ( is_array($this->id) ){
            foreach( $this->id as $id ){
                $model->remove($id);
            }
        }else{
            $model->remove($this->id);
        }
        $msg = JText::_('COM_CRMERY_CUSTOM_REMOVED');
        $this->setRedirect('index.php?option=com_crmery&view=dealcustom',$msg);
    }
    
}