<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.controlleradmin');

class CrmeryControllerEventCategories extends JControllerAdmin
{
    /**
     * constructor (registers additional tasks to methods)
     * @return void
     */
    function __construct()
    {
        parent::__construct();
        //get post data
        $array = JRequest::getVar('cid',  0, '', 'array');
        if ( !($this->id = JRequest::getVar('id')) ){
            if ( count($array) > 1 ){
                $this->id = $array;
            }else{
                $this->id = (int)$array[0];
            }
        }
        JRequest::setVar('view','eventcategories');
    }
    
    function add(){
        $this->id=0;
        $this->edit();
    }
    
    function edit(){
        //set layout
        $view = JController::getView('EventCategories','html');
        $view->setLayout('edit');
        
        //get stage info
        if ( $this->id ){
            $model = $this->getModel('EventCategories');
            $category = $model->getCategories($this->id);
            $category = $category[0];
            $header = JText::_('COM_CRMERY_EDITING_EVENT_CATEGORY') . $category['name'];
        }else{
            $category = null;
            $header = JText::_('COM_CRMERY_ADDING_EVENT_CATEGORY');
        }
        
        //assign references
        $view->assignRef('header',$header);
        $view->assignRef('category',$category);
        
        //display view
        $view->display();
    }
    
    function cancel(){
        $msg = JText::_('Category entry cancelled!');
        $this->setRedirect('index.php?option=com_crmery&view=eventcategories',$msg);
    }
    
    function save(){
        $model = $this->getModel('EventCategories');
        if ( $model->store() ) {
            $msg = JText::_('COM_CRMERY_SUCCESS');
        }else{
            $msg = JText::_('COM_CRMERY_ERROR');
        }
        $this->setRedirect('index.php?option=com_crmery&view=eventcategories',$msg);
    }
    
    function remove(){
        $model = $this->getModel('EventCategories');
        if ( is_array($this->id) ){
            foreach( $this->id as $id ){
                $model->remove($id);
            }
        }else{
            $model->remove($this->id);
        }
        $msg = JText::_('COM_CRMERY_CATEGORY_REMOVED');
        $this->setRedirect('index.php?option=com_crmery&view=eventcategories',$msg);
    }
    
}