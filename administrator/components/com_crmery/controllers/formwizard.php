<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class CrmeryControllerFormwizard extends CrmeryController
{

    var $id = null;

    /**
     * constructor (registers additional tasks to methods)
     * @return void
     */
    function __construct()
    {
        parent::__construct();
        //get post data
        $array = JRequest::getVar('cid',  0, '', 'array');
        if ( !($this->id = JRequest::getVar('id')) ){
            if ( count($array) > 1 ){
                $this->id = $array;
            }else{
                $this->id = (int)$array[0];
            }
        }

        JRequest::setVar('view','formwizard');
    }
    
    function add(){
        $this->id=0;
        $this->edit();
    }
    
    function edit(){
        //set layout
        $view = JController::getView('Formwizard','html');
        $view->setLayout('edit');
        
        //add javascript
        $document =& JFactory::getDocument();
        $document->addScript(JURI::base().'components/com_crmery/media/js/formwizard.js');
        
        //if we get post data generate data for view
        if ( $this->id ){
            $model = $this->getModel('Formwizard');
            $this->form = $model->getForm($this->id);
            $view->assignRef('form',$this->form);
            $form_type = $this->form['type'];
            $this->header = JText::_('COM_CRMERY_EDITING_FORM') . $this->form['name'];
        }else{
            $form_type = "";
            $this->header = JText::_('COM_CRMERY_CREATING_FORM');
        }

        //get joomla users to add
        $model = $this->getModel('Users');
        $user_list = $model->getCrmeryUsers();
        $document->addScriptDeclaration('var user_list='.json_encode($user_list).';');

        $fields = array(
            'lead'=>CrmeryHelperDropdown::getFormFields('people'),
            'contact'=>CrmeryHelperDropdown::getFormFields('people'),
            'deal'=>CrmeryHelperDropdown::getFormFields('deal'),
            'company'=>CrmeryHelperDropdown::getFormFields('company') );
        $view->assignRef('fields',$fields);
        $document->addScriptDeclaration('var fields='.json_encode($fields));

        $view->assignRef('header',$this->header);
        $this->form_types = CrmeryHelperDropdown::getFormTypes($form_type);
        $view->assignRef('form_types',$this->form_types);
        
        //display view
        $view->display();
    }
    
    function cancel(){
        $msg = JText::_('Form entry cancelled!');
        $this->setRedirect('index.php?option=com_crmery&view=formwizard',$msg);
    }
    
    function save(){
        $model = $this->getModel('Formwizard');
        if ( $model->store() ) {
            $msg = "Successfully saved!";
        }else{
            $msg = "Error Saving!";
        }
        $this->setRedirect('index.php?option=com_crmery&view=formwizard',$msg);
    }

    function delete(){
        $model = $this->getModel('Formwizard');
        if ( $model->delete($this->id) ){
            $msg = JText::_('COM_CRMERY_DELETED_FORM');
        }else{
            $msg = JText::_('COM_CRMERY_ERROR_DELETING_FORM');
        }
        $this->setRedirect('index.php?option=com_crmery&view=formwizard',$msg);
    }
    
}