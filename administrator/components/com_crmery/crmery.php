<?php 

/*------------------------------------------------------------------------

# CRMery

# ------------------------------------------------------------------------

# @author CRMery

# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.

# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Website: http://www.crmery.com

-------------------------------------------------------------------------*/

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' ); 



jimport('joomla.application.component.helper');

jimport('joomla.application.component.model');



//Require helper files

require_once (JPATH_COMPONENT.'/helpers/menu.php');

require_once (JPATH_COMPONENT.'/helpers/config.php');

require_once (JPATH_COMPONENT.'/helpers/view.php');

require_once (JPATH_COMPONENT.'/helpers/cpanel.php');

require_once (JPATH_COMPONENT.'/helpers/styles.php');

require_once (JPATH_COMPONENT.'/helpers/version.php');

require_once (JPATH_COMPONENT.'/libraries/bootstrap.php');

require_once (JPATH_ADMINISTRATOR.'/modules/mod_quickicon/helper.php');

require_once (JPATH_SITE.'/components/com_crmery/tables/company.php');

require_once (JPATH_SITE.'/components/com_crmery/tables/deal.php');

require_once (JPATH_SITE.'/components/com_crmery/tables/people.php');

require_once (JPATH_SITE.'/components/com_crmery/helpers/crmery.php');

require_once (JPATH_SITE.'/components/com_crmery/helpers/custom.php');

require_once (JPATH_SITE.'/components/com_crmery/helpers/date.php');

require_once (JPATH_SITE.'/components/com_crmery/helpers/dropdown.php');

require_once (JPATH_SITE.'/components/com_crmery/helpers/users.php');

require_once (JPATH_SITE.'/components/com_crmery/helpers/deal.php');

require_once (JPATH_SITE.'/components/com_crmery/helpers/activity.php');

require_once (JPATH_SITE.'/components/com_crmery/helpers/template.php');

require_once (JPATH_SITE.'/components/com_crmery/helpers/crmtext.php');

require_once (JPATH_SITE.'/components/com_crmery/models/company.php');

require_once (JPATH_SITE.'/components/com_crmery/models/deal.php');

require_once (JPATH_SITE.'/components/com_crmery/models/people.php');



/** include models **/

JModel::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_crmery/models');



// Require the base controller

require_once( JPATH_COMPONENT.'/controller.php' );



//load stylesheets

$document =& JFactory::getDocument();

$document->addStylesheet(JURI::base().'components/com_crmery/media/css/admin.css');

$document->addStylesheet(JURI::base().'components/com_crmery/media/css/jquery.css');

$document->addStylesheet(JURI::base().'components/com_crmery/media/css/bootstrap.css');

$document->addStylesheet(JURI::base().'components/com_crmery/media/css/bootstrap-responsive.css');



//java libs

$document->addScript(JURI::base().'components/com_crmery/media/js/jquery.min.js');

$document->addScript(JURI::base().'components/com_crmery/media/js/jquery-ui.min.js');

$document->addScript(JURI::base().'components/com_crmery/media/js/jquery-chosen.js');

$document->addScript(JURI::base().'components/com_crmery/media/js/jquery-cookie.js');

$document->addScript(JURI::root().'components/com_crmery/media/js/jquery.tools.min.js');

$document->addScript(JURI::base().'components/com_crmery/media/js/crmery.js');

$document->addScript(JURI::base().'components/com_crmery/media/js/bootstrap.js');

$document->addScript(JURI::base().'components/com_crmery/media/js/bootstrap-tooltip.js');

$document->addScriptDeclaration("var base_url='".JURI::root()."';");



CrmeryHelperTemplate::loadJavascriptLanguage();



//Load plugins

JPluginHelper::importPlugin('crmery');



// Require specific controller if requested

if($controller = JRequest::getWord('controller')) {

	$path = JPATH_COMPONENT.'/controllers/'.$controller.'.php';

	if (file_exists($path)) {

		require_once $path;

	} else {

		$controller = '';

	}

    // Create the controller

    $classname  = 'CrmeryController'.$controller;

    $controller = new $classname( );

}else{

    $controller = JController::getInstance('Crmery');

}



// Perform the Request task

$controller->execute( JRequest::getVar( 'task' ) );



// Redirect if set by the controller

$controller->redirect();