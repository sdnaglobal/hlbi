
<?php
/*------------------------------------------------------------------------
# CRMery
# ------------------------------------------------------------------------
# @author CRMery
# @copyright Copyright (C) 2012 crmery.com All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://www.crmery.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
jimport('joomla.installer.installer');
jimport('joomla.installer.helper');

class com_crmeryInstallerScript
{
    /**
     * Method to install the component
     * 
     * @param  mixed    $parent     The class calling this method
     * @return void
     */
    function install($parent) 
    {
        echo '<p>' . JText::_('COM_CRMERY_INSTALL_SUCCESSFULL') . '</p>';
    }

    /**
	 * Method to update the component
	 * 
	 * @param  mixed	$parent		The class calling this method
	 * @return void
	 */
	function update($parent) 
	{

		$this->addNewFields();
		
		$this->updateVersion();
		
		echo '<p>' . JText::_('COM_CRMERY_UPDATE_SUCCESSFULL') . '</p>';
	}

	/**
	 * Method to uninstall the component
	 * 
	 * @param  mixed	$parent		The class calling this method
	 * @return void
	 */
	function uninstall($parent)
	{
		echo '<p>' . JText::_('COM_CRMERY_UNINSTALL_SUCCESSFULL') . '</p>';
	}

	/**
	 * method to run before an install/update/uninstall method
	 *
	 * @param  mixed	$parent		The class calling this method
	 * @return void
	 */
	function preflight($type, $parent) 
	{	
		/** detect joomla version **/
		if ( version_compare( JVERSION, '3.0', '>=' ) == 1) {
			$version = "30";
			$installerParent = $parent->get('parent');
			$paths = $installerParent->get('paths');
			$source = $paths['source'];
		}else{
			$version = "25";
			$installerParent = $parent->get('parent');
			$paths = $installerParent->get('_paths');
			$source = $paths['source'];
		}

		/** rename admin folder **/
		$oldFile = $source."/admin".$version;
		$newFile = $source.'/admin';
		rename($oldFile,$newFile);

		/** rename site folder **/
		$oldFile = $source."/site".$version;
		$newFile = $source.'/site';
		rename($oldFile,$newFile);

	}
 
	/**
	 * method to run after an install/update/uninstall method
	 * 
	 * @param  mixed	$parent		The class calling this method
	 * @return void
	 */
	function postflight($type, $parent) 
	{

		$this->importSql();
		
	}

	function importSql(){

		
		$sqlfile = file_get_contents(JPATH_ADMINISTRATOR.'/components/com_crmery/install.sql');

		// Create an array of queries from the sql file
		$queries = JInstallerHelper::splitSql($sqlfile);
		
		$db =& JFactory::getDBO();
		if ( count ( $queries ) > 0 ){
			foreach ( $queries as $query ){
				$db->setQuery($query);
				$db->query();
			}
		}

	}

	function addNewFields(){

		/** Adds new fields to company categories **/
		$fields = array(
			'ordering' => "int(11) DEFAULT NULL",
		);

		$this->alterTable("#__crmery_company_categories",$fields);

		/** Adds new fields to note categories **/
		$fields = array(
			'ordering' => "int(11) DEFAULT NULL",
		);

		$this->alterTable("#__crmery_notes_categories",$fields);


		/** Add new fields to users table **/
	  	$fields = array(
		  	'document_bypass' 					=> "text NOT NULL",
		  	'dashboard_graph_date_start' 		=> "date DEFAULT NULL",
  			'dashboard_graph_date_end'		 	=> "date DEFAULT NULL",
  			'report_graph_date_start' 			=> "date DEFAULT NULL",
  			'report_graph_date_end' 			=> "date DEFAULT NULL",
  			'dashboard_floats_left' 			=> "varchar(255) NOT NULL DEFAULT 'tasks_events_float,deals_float'",
			'dashboard_floats_right' 			=> "varchar(255) NOT NULL DEFAULT 'sales_float,inbox_float,latest_float'",
			'goal_floats_left' 					=> "varchar(255) NOT NULL DEFAULT 'leaderboard_float'",
			'goal_floats_right' 				=> "varchar(255) NOT NULL DEFAULT 'individual_goals_float,team_goals_float,company_goals_float'",
			'sales_dashboard_floats_left' 		=> "varchar(255) NOT NULL DEFAULT 'deal_stage_float,deal_status_float'",
			'sales_dashboard_floats_right' 		=> "varchar(255) NOT NULL DEFAULT 'yearly_commission_float,yearly_revenue_float,monthly_commission_float,monthly_revenue_float'"
	  	);

	  	$this->alterTable('#__crmery_users', $fields);

		/** Add new fields to events categories table **/

	  	$fields = array(
		  'goal_date' => "enum('this_week','next_week','this_month','next_month','this_quarter','next_quarter','this_year','custom') DEFAULT NULL"
	  	);

	  	$this->alterTable('#__crmery_goals', $fields);


		/** Add new fields to events categories table **/

	  	$fields = array(
		  'description' 	=> 'text',
		  'created_by' 		=> 'int(11) DEFAULT NULL',
		  'modified_by' 	=> 'int(11) DEFAULT NULL',
		  'milestone' 		=> 'tinyint(2) DEFAULT NULL',
		  'ordering' 		=> "int(11) DEFAULT NULL",
	  	);

	  	$this->alterTable('#__crmery_events_categories', $fields);

		/* Add new fields to events table */
		$fields	= array(
						'end_date' 	=> "date DEFAULT NULL",
						'public'	=> "tinyint(2) DEFAULT '0'"
		);
		
		$this->alterTable('#__crmery_events', $fields);

		/* Add new fields to company table */
		$fields	= array(
						'fax'				=> 'VARCHAR(255) NOT NULL',
						'email'				=> 'VARCHAR(255) NOT NULL',
						'twitter_user' 		=> 'text',
  						'facebook_url' 		=> 'text',
  						'flickr_url' 		=> 'text',
  						'youtube_url' 		=> 'text',
  						'category_id'		=> "int(11) DEFAULT NULL",
  						'public'			=> "tinyint(2) NOT NULL DEFAULT '0'"
		);
		
		$this->alterTable('#__crmery_companies', $fields);

		/* Add new fields to config table */
		$fields = array(
				'imap_service'		=> "varchar(255) NOT NULL DEFAULT 'imap'",
  				'imap_port' 		=> "int(11) NOT NULL DEFAULT '993'",
				'users_add' 		=> "tinyint(2) DEFAULT '0'",
				'config_default' 	=> "tinyint(2) DEFAULT '0'",
				'templates_edit' 	=> "tinyint(2) DEFAULT '0'",
				'menu_default' 		=> "tinyint(2) DEFAULT '0'",
				'import_default' 	=> "tinyint(2) DEFAULT '0'",
				'launch_default' 	=> "tinyint(2) DEFAULT '0'",
				'show_help' 		=> "tinyint(2) DEFAULT '1'",
				'import_sample' 	=> "text NOT NULL",
				'currency' 			=> "varchar(255) DEFAULT '$'",
				'lang_deal' 		=> "varchar(255) DEFAULT 'deal'",
				'lang_person' 		=> "varchar(255) DEFAULT 'person'",
				'lang_company' 		=> "varchar(255) DEFAULT 'company'",
				'lang_contact' 		=> "varchar(255) DEFAULT 'contact'",
				'lang_lead' 		=> "varchar(255) DEFAULT 'lead'",
				'lang_task' 		=> "varchar(255) DEFAULT 'task'",
				'lang_event' 		=> "varchar(255) DEFAULT 'event'",
				'lang_goal' 		=> "varchar(255) DEFAULT 'goal'",
				'lang_owner' 		=> "varchar(255) DEFAULT 'owner'",
				'welcome_message'	=> "varchar(255) DEFAULT 'Hello'",
				'time_format'		=> "varchar(255) DEFAULT 'H:i'",
				'acy_company'		=> "tinyint(2) DEFAULT '1'",
  				'acy_person'		=> "tinyint(2) DEFAULT '1'",
  				'allowed_documents'	=> "text NOT NULL",
  				'fullscreen'		=> "tinyint(2) DEFAULT '0'",
  				'toolbar_help'		=> "tinyint(2) DEFAULT '1'",
  				'login_redirect'    => "varchar(255) NOT NULL DEFAULT 'index.php?option=com_users&view=login'",
  				'help_url'			=> "varchar(255) NOT NULL DEFAULT 'http://www.crmery.com/support/'"
			);

		$this->alterTable('#__crmery_config', $fields);

		/* Add new fields to deal stages table */
		$fields = array(
				'ordering' 			=> "int(11) DEFAULT NULL",
				'won' 				=> "tinyint(2) DEFAULT '0'"
			);

		$this->alterTable('#__crmery_stages', $fields);

		/* Add new fields to deal status table */
		$fields = array(
				'ordering' 			=> "int(11) DEFAULT NULL",
  				'color' 			=> "varchar(255) DEFAULT NULL",
  				'created' 			=> "datetime DEFAULT NULL",
  				'modified' 			=> "datetime DEFAULT NULL",
			);

		$this->alterTable('#__crmery_deal_status', $fields);

		/* Add new fields to sources table */
		$fields = array(
				'ordering' 		=> "int(11) DEFAULT NULL",
			);

		$this->alterTable('#__crmery_sources', $fields);

		/* Add new fields to deals custom table */
		$fields = array(
				'ordering' 		=> "int(11) DEFAULT NULL",
			);

		$this->alterTable('#__crmery_deal_custom', $fields);

		/* Add new fields to company custom table */
		$fields = array(
				'ordering' 		=> "int(11) DEFAULT NULL",
			);

		$this->alterTable('#__crmery_company_custom', $fields);

		/* Add new fields to people custom table */
		$fields = array(
				'ordering' 		=> "int(11) DEFAULT NULL",
			);

		$this->alterTable('#__crmery_people_custom', $fields);

		/* Add new fields to people status table */
		$fields = array(
				'ordering' 		=> "int(11) DEFAULT NULL",
			);

		$this->alterTable('#__crmery_people_status', $fields);

		/* Add new fields to people table */
		$fields = array(
				'uid' 		=> "int(11) DEFAULT NULL",
			);

		$this->alterTable('#__crmery_people', $fields);

		/* Add new fields to team table */
		$fields = array(
				'name' 		=> "varchar(255) DEFAULT ''",
			);

		$this->alterTable('#__crmery_teams', $fields);

		/* Add new fields to team table */
		$fields = array(
				'link' 				=> "varchar(255) DEFAULT ''",
				'link_hover' 		=> "varchar(255) DEFAULT ''",
			);

		$this->alterTable('#__crmery_branding', $fields);

		/* Add new fields to formwizard table */
		$fields = array(
				'id' 			=>	"int(11) unsigned NOT NULL AUTO_INCREMENT",
  				'name'			=> 	"varchar(255) DEFAULT NULL",
  				'description' 	=> 	"text",
				'type'			=> 	"enum('contact','lead','deal','company') DEFAULT NULL",
				'modified' 		=> 	"datetime DEFAULT NULL",
				'created' 		=> 	"datetime DEFAULT NULL",
				'modified_by' 	=> 	"int(11) DEFAULT NULL",
				'created_by' 	=>	"int(11) DEFAULT NULL",
				'fields' 		=> 	"text",
				'html' 			=>  "text",
				'return_url' 	=>	"text",
				'owner_id' 		=>	"int(11) DEFAULT NULL"
				);

		$this->alterTable('#__crmery_formwizard', $fields);

		/** Add new fields to documents table **/
		$fields = array(
				'published' 	=> "tinyint(2) DEFAULT '1'",
				'description' 	=> "text"
				);

		$this->alterTable('#__crmery_documents', $fields);

	}

	function alterTable($table, $fields)
	{
		$db =& JFactory::getDBO();
		
		// Get the existing fields
		$db->setQuery("SHOW FIELDS FROM `$table`");
		$cols = $db->loadObjectList();
		$existingFields = array();
		for ($i=0, $n=count($cols); $i<$n; $i++) {
			$existingFields[] = $cols[$i]->Field;
		}
		
		// Determine which fields are missing from the table
		$toAdd = array();
		foreach ($fields AS $field=>$type) {
			if (! in_array($field, $existingFields)) {
				$toAdd[] = $this->nameQuote($field)." ".$type;
			}
		}
		
		// Add the missing fields to the table
		if (count($toAdd) > 0) {
			$newColumns = "ADD COLUMN ".implode(", ADD COLUMN ", $toAdd);
			$query = "ALTER TABLE $table $newColumns";
			$db->setQuery($query);
			$db->query();
		}

	}

	function nameQuote($name, $as = null){
		if (is_string($name))
		{
			$quotedName = $this->quoteNameStr(explode('.', $name));

			$quotedAs = '';
			if (!is_null($as))
			{
				settype($as, 'array');
				$quotedAs .= ' AS ' . $this->quoteNameStr($as);
			}

			return $quotedName . $quotedAs;
		}
		else
		{
			$fin = array();

			if (is_null($as))
			{
				foreach ($name as $str)
				{
					$fin[] = $this->quoteName($str);
				}
			}
			elseif (is_array($name) && (count($name) == count($as)))
			{
				for ($i = 0; $i < count($name); $i++)
				{
					$fin[] = $this->quoteName($name[$i], $as[$i]);
				}
			}

			return $fin;
		}
	}

	protected function quoteNameStr($strArr)
	{
		$parts = array();
		$q = $this->nameQuote;

		foreach ($strArr as $part)
		{
			if (is_null($part))
			{
				continue;
			}

			if (strlen($q) == 1)
			{
				$parts[] = $q . $part . $q;
			}
			else
			{
				$parts[] = $q{0} . $part . $q{1};
			}
		}

		return implode('.', $parts);
	}

	function updateVersion(){



	}

	function unzip($src_file, $dest_dir=false, $create_zip_name_dir=true, $overwrite=true)
	{
	  if ($zip = zip_open($src_file))
	  {
	    if ($zip)
	    {
	      $splitter = ($create_zip_name_dir === true) ? "." : "/";
	      if ($dest_dir === false) $dest_dir = substr($src_file, 0, strrpos($src_file, $splitter))."/";
	     
	      // Create the directories to the destination dir if they don't already exist
	      self::create_dirs($dest_dir);

	      // For every file in the zip-packet
	      while ($zip_entry = zip_read($zip))
	      {
	        // Now we're going to create the directories in the destination directories
	       
	        // If the file is not in the root dir
	        $pos_last_slash = strrpos(zip_entry_name($zip_entry), "/");
	        if ($pos_last_slash !== false)
	        {
	          // Create the directory where the zip-entry should be saved (with a "/" at the end)
	          self::create_dirs($dest_dir.substr(zip_entry_name($zip_entry), 0, $pos_last_slash+1));
	        }

	        // Open the entry
	        if (zip_entry_open($zip,$zip_entry,"r"))
	        {
	         
	          // The name of the file to save on the disk
	          $file_name = $dest_dir.zip_entry_name($zip_entry);
	         
	          // Check if the files should be overwritten or not
	          if ($overwrite === true || $overwrite === false && !is_file($file_name))
	          {
	            // Get the content of the zip entry
	            $fstream = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

	            file_put_contents($file_name, $fstream );
	            // Set the rights
	            chmod($file_name, 0777);
	            //echo "save: ".$file_name."<br />";
	          }
	         
	          // Close the entry
	          zip_entry_close($zip_entry);
	        }      
	      }
	      // Close the zip-file
	      zip_close($zip);
	    }
	  }
	  else
	  {
	    return false;
	  }
	 
	  return true;
	}

	/**
	 * This function creates recursive directories if it doesn't already exist
	 *
	 * @param String  The path that should be created
	 * 
	 * @return  void
	 */
	function create_dirs($path)
	{
	  if (!is_dir($path))
	  {
	    $directory_path = "";
	    $directories = explode("/",$path);
	    array_pop($directories);
	   
	    foreach($directories as $directory)
	    {
	      $directory_path .= $directory."/";
	      if (!is_dir($directory_path))
	      {
	        mkdir($directory_path);
	        chmod($directory_path, 0777);
	      }
	    }
	  }
	}

      
} 