<?php
 /**
 * Main Component File (Admin)
 *
 * @package			JUserTube 
 * @version			6.1.0
 *
 * @author			Md. Afzal Hossain <afzal.csedu@gmail.com>
 * @link			http://www.srizon.com
 * @copyright		Copyright 2012 Md. Afzal Hossain All Rights Reserved
 * @license			http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

echo "<h3>For setting-up the module/component</h3> 1. Go to Module manager, find the module 'JUserTube' and setup the parameters properly<br />".'<a href="index.php?option=com_modules&filter_module=mod_jusertube">This link</a> will take you directly to module manager showing only the JUserTube modules (filtered)<br />'."Or<br />2. Go to Menu manager and add a menu of JUserTube type (2 layouts available with pagination)";
if(isset($_GET['sync']) && $_GET['sync']=='now'){
	$target = JPATH_BASE.'/../modules/mod_jusertube/savedxml/';
	$xml_files = JFolder::files($target, '.xml');
	foreach($xml_files as $file){
		unlink($target.$file);
	}
	echo '<h2>All instance of the module/component will be synced on next load</h2>';
}
else{
	echo '<h3>For Forcing the sync process with youtube or vimeo click the link below</h3> <a href="index.php?option=com_jusertube&sync=now"><h4>Sync Now</h4></a>';
}