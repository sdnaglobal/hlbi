/**
 * jquery.elastislide.js v1.1.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2012, Codrops
 * http://www.codrops.com
 */

;( function( srizonjq, window, undefined ) {
	
	'use strict';
	
	/*
	* debouncedresize: special jQuery event that happens once after a window resize
	*
	* latest version and complete README available on Github:
	* https://github.com/louisremi/jquery-smartresize/blob/master/jquery.debouncedresize.js
	*
	* Copyright 2011 @louis_remi
	* Licensed under the MIT license.
	*/
	var srizonjqevent = srizonjq.event,
	srizonjqspecial,
	resizeTimeout;

	srizonjqspecial = srizonjqevent.special.debouncedresize = {
		setup: function() {
			srizonjq( this ).on( "resize", srizonjqspecial.handler );
		},
		teardown: function() {
			srizonjq( this ).off( "resize", srizonjqspecial.handler );
		},
		handler: function( event, execAsap ) {
			// Save the context
			var context = this,
				args = arguments,
				dispatch = function() {
					// set correct event type
					event.type = "debouncedresize";
					srizonjqevent.dispatch.apply( context, args );
				};

			if ( resizeTimeout ) {
				clearTimeout( resizeTimeout );
			}

			execAsap ?
				dispatch() :
				resizeTimeout = setTimeout( dispatch, srizonjqspecial.threshold );
		},
		threshold: 150
	};

	// ======================= imagesLoaded Plugin ===============================
	// https://github.com/desandro/imagesloaded

	// srizonjq('#my-container').imagesLoaded(myFunction)
	// execute a callback when all images have loaded.
	// needed because .load() doesn't work on cached images

	// callback function gets image collection as argument
	//  this is the container

	// original: mit license. paul irish. 2010.
	// contributors: Oren Solomianik, David DeSandro, Yiannis Chatzikonstantinou

	// blank image data-uri bypasses webkit log warning (thx doug jones)
	var BLANK = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==';

	srizonjq.fn.imagesLoaded = function( callback ) {
		var srizonjqthis = this,
			deferred = srizonjq.isFunction(srizonjq.Deferred) ? srizonjq.Deferred() : 0,
			hasNotify = srizonjq.isFunction(deferred.notify),
			srizonjqimages = srizonjqthis.find('img').add( srizonjqthis.filter('img') ),
			loaded = [],
			proper = [],
			broken = [];

		// Register deferred callbacks
		if (srizonjq.isPlainObject(callback)) {
			srizonjq.each(callback, function (key, value) {
				if (key === 'callback') {
					callback = value;
				} else if (deferred) {
					deferred[key](value);
				}
			});
		}

		function doneLoading() {
			var srizonjqproper = srizonjq(proper),
				srizonjqbroken = srizonjq(broken);

			if ( deferred ) {
				if ( broken.length ) {
					deferred.reject( srizonjqimages, srizonjqproper, srizonjqbroken );
				} else {
					deferred.resolve( srizonjqimages );
				}
			}

			if ( srizonjq.isFunction( callback ) ) {
				callback.call( srizonjqthis, srizonjqimages, srizonjqproper, srizonjqbroken );
			}
		}

		function imgLoaded( img, isBroken ) {
			// don't proceed if BLANK image, or image is already loaded
			if ( img.src === BLANK || srizonjq.inArray( img, loaded ) !== -1 ) {
				return;
			}

			// store element in loaded images array
			loaded.push( img );

			// keep track of broken and properly loaded images
			if ( isBroken ) {
				broken.push( img );
			} else {
				proper.push( img );
			}

			// cache image and its state for future calls
			srizonjq.data( img, 'imagesLoaded', { isBroken: isBroken, src: img.src } );

			// trigger deferred progress method if present
			if ( hasNotify ) {
				deferred.notifyWith( srizonjq(img), [ isBroken, srizonjqimages, srizonjq(proper), srizonjq(broken) ] );
			}

			// call doneLoading and clean listeners if all images are loaded
			if ( srizonjqimages.length === loaded.length ){
				setTimeout( doneLoading );
				srizonjqimages.unbind( '.imagesLoaded' );
			}
		}

		// if no images, trigger immediately
		if ( !srizonjqimages.length ) {
			doneLoading();
		} else {
			srizonjqimages.bind( 'load.imagesLoaded error.imagesLoaded', function( event ){
				// trigger imgLoaded
				imgLoaded( event.target, event.type === 'error' );
			}).each( function( i, el ) {
				var src = el.src;

				// find out if this image has been already checked for status
				// if it was, and src has not changed, call imgLoaded on it
				var cached = srizonjq.data( el, 'imagesLoaded' );
				if ( cached && cached.src === src ) {
					imgLoaded( el, cached.isBroken );
					return;
				}

				// if complete is true and browser supports natural sizes, try
				// to check for image status manually
				if ( el.complete && el.naturalWidth !== undefined ) {
					imgLoaded( el, el.naturalWidth === 0 || el.naturalHeight === 0 );
					return;
				}

				// cached images don't fire load sometimes, so we reset src, but only when
				// dealing with IE, or image is complete (loaded) and failed manual check
				// webkit hack from http://groups.google.com/group/jquery-dev/browse_thread/thread/eee6ab7b2da50e1f
				if ( el.readyState || el.complete ) {
					el.src = BLANK;
					el.src = src;
				}
			});
		}

		return deferred ? deferred.promise( srizonjqthis ) : srizonjqthis;
	};

	// global
	var srizonjqwindow = srizonjq( window ),
		Modernizr = window.Modernizr;

	srizonjq.Elastislide = function( options, element ) {
		
		this.srizonjqel = srizonjq( element );
		this._init( options );
		
	};

	srizonjq.Elastislide.defaults = {
		// orientation 'horizontal' || 'vertical'
		orientation : 'horizontal',
		// sliding speed
		speed : 500,
		// sliding easing
		easing : 'ease-in-out',
		// the minimum number of items to show. 
		// when we resize the window, this will make sure minItems are always shown 
		// (unless of course minItems is higher than the total number of elements)
		minItems : 3,
		// index of the current item (left most item of the carousel)
		start : 0,
		// scale down image size (for big images big numbers)
		scaledown : 1,
		// click item callback
		onClick : function( el, position, evt ) { return false; },
		onReady : function() { return false; },
		onBeforeSlide : function() { return false; },
		onAfterSlide : function() { return false; }
	};

	srizonjq.Elastislide.prototype = {

		_init : function( options ) {
			
			// options
			this.options = srizonjq.extend( true, {}, srizonjq.Elastislide.defaults, options );

			// https://github.com/twitter/bootstrap/issues/2870
			var self = this,
				transEndEventNames = {
					'WebkitTransition' : 'webkitTransitionEnd',
					'MozTransition' : 'transitionend',
					'OTransition' : 'oTransitionEnd',
					'msTransition' : 'MSTransitionEnd',
					'transition' : 'transitionend'
				};
			
			this.transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ];
			
			// suport for css transforms and css transitions
			this.support = Modernizr.csstransitions && Modernizr.csstransforms;

			// current item's index
			this.current = this.options.start;

			// control if it's sliding
			this.isSliding = false;

			this.srizonjqitems = this.srizonjqel.children( 'li' );
			// total number of items
			this.itemsCount = this.srizonjqitems.length;
			if( this.itemsCount === 0 ) {

				return false;

			}
			this._validate();
			// remove white space
			this.srizonjqitems.detach();
			this.srizonjqel.empty();
			this.srizonjqel.append( this.srizonjqitems );

			// main wrapper
			this.srizonjqel.wrap( '<div class="elastislide-wrapper elastislide-loading elastislide-' + this.options.orientation + '"></div>' );

			// check if we applied a transition to the <ul>
			this.hasTransition = false;
			
			// add transition for the <ul>
			this.hasTransitionTimeout = setTimeout( function() {
				
				self._addTransition();

			}, 100 );

			// preload the images
			
			this.srizonjqel.imagesLoaded( function() {

				self.srizonjqel.show();

				self._layout();
				self._configure();
				
				if( self.hasTransition ) {

					// slide to current's position
					self._removeTransition();
					self._slideToItem( self.current );

					self.srizonjqel.on( self.transEndEventName, function() {

						self.srizonjqel.off( self.transEndEventName );
						self._setWrapperSize();
						// add transition for the <ul>
						self._addTransition();
						self._initEvents();

					} );

				}
				else {

					clearTimeout( self.hasTransitionTimeout );
					self._setWrapperSize();
					self._initEvents();
					// slide to current's position
					self._slideToItem( self.current );
					setTimeout( function() { self._addTransition(); }, 25 );

				}

				self.options.onReady();

			} );

		},
		_validate : function() {

			if( this.options.speed < 0 ) {

				this.options.speed = 500;

			}
			if( this.options.minItems < 1 || this.options.minItems > this.itemsCount ) {

				this.options.minItems = 1;

			}
			if( this.options.start < 0 || this.options.start > this.itemsCount - 1 ) {

				this.options.start = 0;

			}
			if( this.options.orientation != 'horizontal' && this.options.orientation != 'vertical' ) {

				this.options.orientation = 'horizontal';

			}
				
		},
		_layout : function() {

			this.srizonjqel.wrap( '<div class="elastislide-carousel"></div>' );

			this.srizonjqcarousel = this.srizonjqel.parent();
			this.srizonjqwrapper = this.srizonjqcarousel.parent().removeClass( 'elastislide-loading' );

			// save original image sizes
			var srizonjqimg = this.srizonjqitems.find( 'img:first' );
			this.imgSize = { width : srizonjqimg.outerWidth( true )/this.options.scaledown, height : srizonjqimg.outerHeight( true )/this.options.scaledown };

			this._setItemsSize();
			this.options.orientation === 'horizontal' ? this.srizonjqel.css( 'max-height', this.imgSize.height ) : this.srizonjqel.css( 'height', this.options.minItems * this.imgSize.height );

			// add the controls
			this._addControls();

		},
		_addTransition : function() {

			if( this.support ) {

				this.srizonjqel.css( 'transition', 'all ' + this.options.speed + 'ms ' + this.options.easing );
				
			}
			this.hasTransition = true;

		},
		_removeTransition : function() {

			if( this.support ) {

				this.srizonjqel.css( 'transition', 'all 0s' );

			}
			this.hasTransition = false;
			
		},
		_addControls : function() {

			var self = this;

			// add navigation elements
			this.srizonjqnavigation = srizonjq( '<nav><span class="elastislide-prev">Previous</span><span class="elastislide-next">Next</span></nav>' )
				.appendTo( this.srizonjqwrapper );


			this.srizonjqnavPrev = this.srizonjqnavigation.find( 'span.elastislide-prev' ).on( 'mousedown.elastislide', function( event ) {

				self._slide( 'prev' );
				return false;

			} );

			this.srizonjqnavNext = this.srizonjqnavigation.find( 'span.elastislide-next' ).on( 'mousedown.elastislide', function( event ) {

				self._slide( 'next' );
				return false;

			} );

		},
		_setItemsSize : function() {

			// width for the items (%)
			var w = this.options.orientation === 'horizontal' ? ( Math.floor( this.srizonjqcarousel.width() / this.options.minItems ) * 100 ) / this.srizonjqcarousel.width() : 100;
			
			this.srizonjqitems.css( {
				'width' : w + '%',
				'max-width' : this.imgSize.width,
				'max-height' : this.imgSize.height
			} );

			if( this.options.orientation === 'vertical' ) {
			
				this.srizonjqwrapper.css( 'max-width', this.imgSize.width + parseInt( this.srizonjqwrapper.css( 'padding-left' ) ) + parseInt( this.srizonjqwrapper.css( 'padding-right' ) ) );
			
			}

		},
		_setWrapperSize : function() {

			if( this.options.orientation === 'vertical' ) {

				this.srizonjqwrapper.css( {
					'height' : this.options.minItems * this.imgSize.height + parseInt( this.srizonjqwrapper.css( 'padding-top' ) ) + parseInt( this.srizonjqwrapper.css( 'padding-bottom' ) )
				} );

			}

		},
		_configure : function() {

			// check how many items fit in the carousel (visible area -> this.srizonjqcarousel.width() )
			this.fitCount = this.options.orientation === 'horizontal' ? 
								this.srizonjqcarousel.width() < this.options.minItems * this.imgSize.width ? this.options.minItems : Math.floor( this.srizonjqcarousel.width() / this.imgSize.width ) :
								this.srizonjqcarousel.height() < this.options.minItems * this.imgSize.height ? this.options.minItems : Math.floor( this.srizonjqcarousel.height() / this.imgSize.height );

		},
		_initEvents : function() {

			var self = this;

			srizonjqwindow.on( 'debouncedresize.elastislide', function() {

				self._setItemsSize();
				self._configure();
				self._slideToItem( self.current );

			} );

			this.srizonjqel.on( this.transEndEventName, function() {

				self._onEndTransition();

			} );

			if( this.options.orientation === 'horizontal' ) {

				this.srizonjqel.on( {
					swipeleft : function() {

						self._slide( 'next' );
					
					},
					swiperight : function() {

						self._slide( 'prev' );
					
					}
				} );

			}
			else {

				this.srizonjqel.on( {
					swipeup : function() {

						self._slide( 'next' );
					
					},
					swipedown : function() {

						self._slide( 'prev' );
					
					}
				} );

			}

			// item click event
			this.srizonjqel.on( 'click.elastislide', 'li', function( event ) {

				var srizonjqitem = srizonjq( this );

				self.options.onClick( srizonjqitem, srizonjqitem.index(), event );
				
			});

		},
		_destroy : function( callback ) {
			
			this.srizonjqel.off( this.transEndEventName ).off( 'swipeleft swiperight swipeup swipedown .elastislide' );
			srizonjqwindow.off( '.elastislide' );
			
			this.srizonjqel.css( {
				'max-height' : 'none',
				'transition' : 'none'
			} ).unwrap( this.srizonjqcarousel ).unwrap( this.srizonjqwrapper );

			this.srizonjqitems.css( {
				'width' : 'auto',
				'max-width' : 'none',
				'max-height' : 'none'
			} );

			this.srizonjqnavigation.remove();
			this.srizonjqwrapper.remove();

			if( callback ) {

				callback.call();

			}

		},
		_toggleControls : function( dir, display ) {

			if( display ) {

				( dir === 'next' ) ? this.srizonjqnavNext.show() : this.srizonjqnavPrev.show();

			}
			else {

				( dir === 'next' ) ? this.srizonjqnavNext.hide() : this.srizonjqnavPrev.hide();

			}
			
		},
		_slide : function( dir, tvalue ) {

			if( this.isSliding ) {

				return false;

			}
			
			this.options.onBeforeSlide();

			this.isSliding = true;

			var self = this,
				translation = this.translation || 0,
				// width/height of an item ( <li> )
				itemSpace = this.options.orientation === 'horizontal' ? this.srizonjqitems.outerWidth( true ) : this.srizonjqitems.outerHeight( true ),
				// total width/height of the <ul>
				totalSpace = this.itemsCount * itemSpace,
				// visible width/height
				visibleSpace = this.options.orientation === 'horizontal' ? this.srizonjqcarousel.width() : this.srizonjqcarousel.height();
			
			if( tvalue === undefined ) {
				
				var amount = this.fitCount * itemSpace;

				if( amount < 0 ) {

					return false;

				}

				if( dir === 'next' && totalSpace - ( Math.abs( translation ) + amount ) < visibleSpace ) {

					amount = totalSpace - ( Math.abs( translation ) + visibleSpace );

					// show / hide navigation buttons
					this._toggleControls( 'next', false );
					this._toggleControls( 'prev', true );

				}
				else if( dir === 'prev' && Math.abs( translation ) - amount < 0 ) {

					amount = Math.abs( translation );

					// show / hide navigation buttons
					this._toggleControls( 'next', true );
					this._toggleControls( 'prev', false );

				}
				else {
					
					// future translation value
					var ftv = dir === 'next' ? Math.abs( translation ) + Math.abs( amount ) : Math.abs( translation ) - Math.abs( amount );
					
					// show / hide navigation buttons
					ftv > 0 ? this._toggleControls( 'prev', true ) : this._toggleControls( 'prev', false );
					ftv < totalSpace - visibleSpace ? this._toggleControls( 'next', true ) : this._toggleControls( 'next', false );
						
				}
				
				tvalue = dir === 'next' ? translation - amount : translation + amount;

			}
			else {

				var amount = Math.abs( tvalue );

				if( Math.max( totalSpace, visibleSpace ) - amount < visibleSpace ) {

					tvalue	= - ( Math.max( totalSpace, visibleSpace ) - visibleSpace );
				
				}

				// show / hide navigation buttons
				amount > 0 ? this._toggleControls( 'prev', true ) : this._toggleControls( 'prev', false );
				Math.max( totalSpace, visibleSpace ) - visibleSpace > amount ? this._toggleControls( 'next', true ) : this._toggleControls( 'next', false );

			}
			
			this.translation = tvalue;

			if( translation === tvalue ) {
				
				this._onEndTransition();
				return false;

			}

			if( this.support ) {
				
				this.options.orientation === 'horizontal' ? this.srizonjqel.css( 'transform', 'translateX(' + tvalue + 'px)' ) : this.srizonjqel.css( 'transform', 'translateY(' + tvalue + 'px)' );

			}
			else {

				srizonjq.fn.applyStyle = this.hasTransition ? srizonjq.fn.animate : srizonjq.fn.css;
				var styleCSS = this.options.orientation === 'horizontal' ? { left : tvalue } : { top : tvalue };
				
				this.srizonjqel.stop().applyStyle( styleCSS, srizonjq.extend( true, [], { duration : this.options.speed, complete : function() {

					self._onEndTransition();
					
				} } ) );

			}
			
			if( !this.hasTransition ) {

				this._onEndTransition();

			}

		},
		_onEndTransition : function() {

			this.isSliding = false;
			this.options.onAfterSlide();

		},
		_slideTo : function( pos ) {

			var pos = pos || this.current,
				translation = Math.abs( this.translation ) || 0,
				itemSpace = this.options.orientation === 'horizontal' ? this.srizonjqitems.outerWidth( true ) : this.srizonjqitems.outerHeight( true ),
				posR = translation + this.srizonjqcarousel.width(),
				ftv = Math.abs( pos * itemSpace );

			if( ftv + itemSpace > posR || ftv < translation ) {

				this._slideToItem( pos );
			
			}

		},
		_slideToItem : function( pos ) {

			// how much to slide?
			var amount	= this.options.orientation === 'horizontal' ? pos * this.srizonjqitems.outerWidth( true ) : pos * this.srizonjqitems.outerHeight( true );
			this._slide( '', -amount );
			
		},
		// public method: adds new items to the carousel
		/*
		
		how to use:
		var carouselEl = srizonjq( '#carousel' ),
			carousel = carouselEl.elastislide();
		...
		
		// append or prepend new items:
		carouselEl.prepend('<li><a href="#"><img src="images/large/2.jpg" alt="image02" /></a></li>');

		// call the add method:
		es.add();
		
		*/
		add : function( callback ) {
			
			var self = this,
				oldcurrent = this.current,
				srizonjqcurrentItem = this.srizonjqitems.eq( this.current );
			
			// adds new items to the carousel
			this.srizonjqitems = this.srizonjqel.children( 'li' );
			this.itemsCount = this.srizonjqitems.length;
			this.current = srizonjqcurrentItem.index();
			this._setItemsSize();
			this._configure();
			this._removeTransition();
			oldcurrent < this.current ? this._slideToItem( this.current ) : this._slide( 'next', this.translation );
			setTimeout( function() { self._addTransition(); }, 25 );
			
			if ( callback ) {

				callback.call();

			}
			
		},
		// public method: sets a new element as the current. slides to that position
		setCurrent : function( idx, callback ) {
			
			this.current = idx;

			this._slideTo();
			
			if ( callback ) {

				callback.call();

			}
			
		},
		// public method: slides to the next set of items
		next : function() {

			self._slide( 'next' );

		},
		// public method: slides to the previous set of items
		previous : function() {

			self._slide( 'prev' );

		},
		// public method: slides to the first item
		slideStart : function() {

			this._slideTo( 0 );

		},
		// public method: slides to the last item
		slideEnd : function() {

			this._slideTo( this.itemsCount - 1 );

		},
		// public method: destroys the elastislide instance
		destroy : function( callback ) {

			this._destroy( callback );
		
		}

	};
	
	var logError = function( message ) {

		if ( window.console ) {

			window.console.error( message );
		
		}

	};
	
	srizonjq.fn.elastislide = function( options ) {

		var self = srizonjq.data( this, 'elastislide' );
		
		if ( typeof options === 'string' ) {
			
			var args = Array.prototype.slice.call( arguments, 1 );
			
			this.each(function() {
			
				if ( !self ) {

					logError( "cannot call methods on elastislide prior to initialization; " +
					"attempted to call method '" + options + "'" );
					return;
				
				}
				
				if ( !srizonjq.isFunction( self[options] ) || options.charAt(0) === "_" ) {

					logError( "no such method '" + options + "' for elastislide self" );
					return;
				
				}
				
				self[ options ].apply( self, args );
			
			});
		
		} 
		else {
		
			this.each(function() {
				
				if ( self ) {

					self._init();
				
				}
				else {

					self = srizonjq.data( this, 'elastislide', new srizonjq.Elastislide( options, this ) );
				
				}

			});
		
		}
		
		return self;
		
	};
	
} )( srizonjq, window );
