srizonjq(document).ready(function(){   
	srztb_init('a.srzthickbox, area.srzthickbox, input.srzthickbox');//pass where to apply srzthickbox
	imgLoader = new Image();// preload image
	imgLoader.src = srztb_pathToImage;
});

function srztb_init(domChunk){
	srizonjq(domChunk).click(function(){
	var t = this.title || this.name || null;
	var a = this.href || this.alt;
	var g = this.rel || false;
	srztb_show(t,a,g);
	this.blur();
	return false;
	});
}

function srztb_show(caption, url, imageGroup) {//function called when the user clicks on a srzthickbox link

	try {
		if (typeof document.body.style.maxHeight === "undefined") {//if IE 6
			srizonjq("body","html").css({height: "100%", width: "100%"});
			srizonjq("html").css("overflow","hidden");
			if (document.getElementById("srztb_HideSelect") === null) {//iframe to hide select elements in ie6
				srizonjq("body").append("<iframe id='srztb_HideSelect'></iframe><div id='srztb_overlay'></div><div id='srztb_window'></div>");
				srizonjq("#srztb_overlay").click(srztb_remove);
			}
		}else{//all others
			if(document.getElementById("srztb_overlay") === null){
				srizonjq("body").append("<div id='srztb_overlay'></div><div id='srztb_window'></div>");
				srizonjq("#srztb_overlay").click(srztb_remove);
			}
		}
		
		if(srztb_detectMacXFF()){
			srizonjq("#srztb_overlay").addClass("srztb_overlayMacFFBGHack");//use png overlay so hide flash
		}else{
			srizonjq("#srztb_overlay").addClass("srztb_overlayBG");//use background and opacity
		}
		
		if(caption===null){caption="";}
		srizonjq("body").append("<div id='srztb_load'><img src='"+imgLoader.src+"' /></div>");//add loader to the page
		srizonjq('#srztb_load').show();//show loader
		
		var baseURL;
	   if(url.indexOf("?")!==-1){ //ff there is a query string involved
			baseURL = url.substr(0, url.indexOf("?"));
	   }else{ 
	   		baseURL = url;
	   }
	   
	   var urlString = /\.jpg$|\.jpeg$|\.png$|\.gif$|\.bmp$/;
	   var urlType = baseURL.toLowerCase().match(urlString);

		if(urlType == '.jpg' || urlType == '.jpeg' || urlType == '.png' || urlType == '.gif' || urlType == '.bmp'){//code to show images
				
			srztb_PrevCaption = "";
			srztb_PrevURL = "";
			srztb_PrevHTML = "";
			srztb_NextCaption = "";
			srztb_NextURL = "";
			srztb_NextHTML = "";
			srztb_imageCount = "";
			srztb_FoundURL = false;
			if(imageGroup){
				srztb_TempArray = srizonjq("a[@rel="+imageGroup+"]").get();
				for (srztb_Counter = 0; ((srztb_Counter < srztb_TempArray.length) && (srztb_NextHTML === "")); srztb_Counter++) {
					var urlTypeTemp = srztb_TempArray[srztb_Counter].href.toLowerCase().match(urlString);
						if (!(srztb_TempArray[srztb_Counter].href == url)) {						
							if (srztb_FoundURL) {
								srztb_NextCaption = srztb_TempArray[srztb_Counter].title;
								srztb_NextURL = srztb_TempArray[srztb_Counter].href;
								srztb_NextHTML = "<span id='srztb_next'>&nbsp;&nbsp;<a href='#'>Next &gt;</a></span>";
							} else {
								srztb_PrevCaption = srztb_TempArray[srztb_Counter].title;
								srztb_PrevURL = srztb_TempArray[srztb_Counter].href;
								srztb_PrevHTML = "<span id='srztb_prev'>&nbsp;&nbsp;<a href='#'>&lt; Prev</a></span>";
							}
						} else {
							srztb_FoundURL = true;
							srztb_imageCount = "Image " + (srztb_Counter + 1) +" of "+ (srztb_TempArray.length);											
						}
				}
			}

			imgPreloader = new Image();
			imgPreloader.onload = function(){		
			imgPreloader.onload = null;
				
			// Resizing large images - orginal by Christian Montoya edited by me.
			var pagesize = srztb_getPageSize();
			var x = pagesize[0] - 150;
			var y = pagesize[1] - 150;
			var imageWidth = imgPreloader.width;
			var imageHeight = imgPreloader.height;
			if (imageWidth > x) {
				imageHeight = imageHeight * (x / imageWidth); 
				imageWidth = x; 
				if (imageHeight > y) { 
					imageWidth = imageWidth * (y / imageHeight); 
					imageHeight = y; 
				}
			} else if (imageHeight > y) { 
				imageWidth = imageWidth * (y / imageHeight); 
				imageHeight = y; 
				if (imageWidth > x) { 
					imageHeight = imageHeight * (x / imageWidth); 
					imageWidth = x;
				}
			}
			// End Resizing
			
			srztb_WIDTH = imageWidth + 30;
			srztb_HEIGHT = imageHeight + 60;
			srizonjq("#srztb_window").append("<a href='' id='srztb_ImageOff' title='Close'><img id='srztb_Image' src='"+url+"' width='"+imageWidth+"' height='"+imageHeight+"' alt='"+caption+"'/></a>" + "<div id='srztb_caption'>"+caption+"<div id='srztb_secondLine'>" + srztb_imageCount + srztb_PrevHTML + srztb_NextHTML + "</div></div><div id='srztb_closeWindow'><a href='#' id='srztb_closeWindowButton' title='Close'>close</a> or Esc Key</div>"); 		
			
			srizonjq("#srztb_closeWindowButton").click(srztb_remove);
			
			if (!(srztb_PrevHTML === "")) {
				function goPrev(){
					if(srizonjq(document).unbind("click",goPrev)){srizonjq(document).unbind("click",goPrev);}
					srizonjq("#srztb_window").remove();
					srizonjq("body").append("<div id='srztb_window'></div>");
					srztb_show(srztb_PrevCaption, srztb_PrevURL, imageGroup);
					return false;	
				}
				srizonjq("#srztb_prev").click(goPrev);
			}
			
			if (!(srztb_NextHTML === "")) {		
				function goNext(){
					srizonjq("#srztb_window").remove();
					srizonjq("body").append("<div id='srztb_window'></div>");
					srztb_show(srztb_NextCaption, srztb_NextURL, imageGroup);				
					return false;	
				}
				srizonjq("#srztb_next").click(goNext);
				
			}

			document.onkeydown = function(e){ 	
				if (e == null) { // ie
					keycode = event.keyCode;
				} else { // mozilla
					keycode = e.which;
				}
				if(keycode == 27){ // close
					srztb_remove();
				} else if(keycode == 190){ // display previous image
					if(!(srztb_NextHTML == "")){
						document.onkeydown = "";
						goNext();
					}
				} else if(keycode == 188){ // display next image
					if(!(srztb_PrevHTML == "")){
						document.onkeydown = "";
						goPrev();
					}
				}	
			};
			
			srztb_position();
			srizonjq("#srztb_load").remove();
			srizonjq("#srztb_ImageOff").click(srztb_remove);
			srizonjq("#srztb_window").css({display:"block"}); //for safari using css instead of show
			};
			
			imgPreloader.src = url;
		}else{//code to show html
			
			var queryString = url.replace(/^[^\?]+\??/,'');
			var params = srztb_parseQuery( queryString );

			srztb_WIDTH = (params['width']*1) + 30 || 630; //defaults to 630 if no paramaters were added to URL
			srztb_HEIGHT = (params['height']*1) + 40 || 440; //defaults to 440 if no paramaters were added to URL
			ajaxContentW = srztb_WIDTH - 30;
			ajaxContentH = srztb_HEIGHT - 45;
			
			if(url.indexOf('srztb_iframe') != -1){// either iframe or ajax window		
					//urlNoQuery = url.split('srztb_');
					srizonjq("#srztb_iframeContent").remove();
					if(params['modal'] != "true"){//iframe no modal
						srizonjq("#srztb_window").append("<div id='srztb_title'><div id='srztb_ajaxWindowTitle'>"+caption+"</div><div id='srztb_closeAjaxWindow'><a href='#' id='srztb_closeWindowButton' title='Close'>close</a> or Esc Key</div></div><iframe frameborder='0' hspace='0' src='"+url+"' id='srztb_iframeContent' name='srztb_iframeContent"+Math.round(Math.random()*1000)+"' onload='srztb_showIframe()' style='width:"+(ajaxContentW + 29)+"px;height:"+(ajaxContentH + 17)+"px;' > </iframe>");
					}else{//iframe modal
					srizonjq("#srztb_overlay").unbind();
						srizonjq("#srztb_window").append("<iframe frameborder='0' hspace='0' src='"+url+"' id='srztb_iframeContent' name='srztb_iframeContent"+Math.round(Math.random()*1000)+"' onload='srztb_showIframe()' style='width:"+(ajaxContentW + 29)+"px;height:"+(ajaxContentH + 17)+"px;'> </iframe>");
					}
			}else{// not an iframe, ajax
					if(srizonjq("#srztb_window").css("display") != "block"){
						if(params['modal'] != "true"){//ajax no modal
						srizonjq("#srztb_window").append("<div id='srztb_title'><div id='srztb_ajaxWindowTitle'>"+caption+"</div><div id='srztb_closeAjaxWindow'><a href='#' id='srztb_closeWindowButton'>close</a> or Esc Key</div></div><div id='srztb_ajaxContent' style='width:"+ajaxContentW+"px;height:"+ajaxContentH+"px'></div>");
						}else{//ajax modal
						srizonjq("#srztb_overlay").unbind();
						srizonjq("#srztb_window").append("<div id='srztb_ajaxContent' class='srztb_modal' style='width:"+ajaxContentW+"px;height:"+ajaxContentH+"px;'></div>");	
						}
					}else{//this means the window is already up, we are just loading new content via ajax
						srizonjq("#srztb_ajaxContent")[0].style.width = ajaxContentW +"px";
						srizonjq("#srztb_ajaxContent")[0].style.height = ajaxContentH +"px";
						srizonjq("#srztb_ajaxContent")[0].scrollTop = 0;
						srizonjq("#srztb_ajaxWindowTitle").html(caption);
					}
			}
					
			srizonjq("#srztb_closeWindowButton").click(srztb_remove);
			
				if(url.indexOf('srztb_inline') != -1){	
					srizonjq("#srztb_ajaxContent").append(srizonjq('#' + params['inlineId']).children());
					srizonjq("#srztb_window").unload(function () {
						srizonjq('#' + params['inlineId']).append( srizonjq("#srztb_ajaxContent").children() ); // move elements back when you're finished
					});
					srztb_position();
					srizonjq("#srztb_load").remove();
					srizonjq("#srztb_window").css({display:"block"}); 
				}else if(url.indexOf('srztb_iframe') != -1){
					srztb_position();
					if(srizonjq.browser.safari){//safari needs help because it will not fire iframe onload
						srizonjq("#srztb_load").remove();
						srizonjq("#srztb_window").css({display:"block"});
					}
				}else{
					srizonjq("#srztb_ajaxContent").load(url += "&random=" + (new Date().getTime()),function(){//to do a post change this load method
						srztb_position();
						srizonjq("#srztb_load").remove();
						srztb_init("#srztb_ajaxContent a.srzthickbox");
						srizonjq("#srztb_window").css({display:"block"});
					});
				}
			
		}

		if(!params['modal']){
			document.onkeyup = function(e){ 	
				if (e == null) { // ie
					keycode = event.keyCode;
				} else { // mozilla
					keycode = e.which;
				}
				if(keycode == 27){ // close
					srztb_remove();
				}	
			};
		}
		
	} catch(e) {
		//nothing here
	}
}

//helper functions below
function srztb_showIframe(){
	srizonjq("#srztb_load").remove();
	srizonjq("#srztb_window").css({display:"block"});
}

function srztb_remove() {
 	srizonjq("#srztb_imageOff").unbind("click");
	srizonjq("#srztb_closeWindowButton").unbind("click");
	srizonjq("#srztb_window").fadeOut("fast",function(){srizonjq('#srztb_window,#srztb_overlay,#srztb_HideSelect').trigger("unload").unbind().remove();});
	srizonjq("#srztb_load").remove();
	if (typeof document.body.style.maxHeight == "undefined") {//if IE 6
		srizonjq("body","html").css({height: "auto", width: "auto"});
		srizonjq("html").css("overflow","");
	}
	document.onkeydown = "";
	document.onkeyup = "";
	return false;
}

function srztb_position() {
srizonjq("#srztb_window").css({marginLeft: '-' + parseInt((srztb_WIDTH / 2),10) + 'px', width: srztb_WIDTH + 'px'});
	if ( !(srizonjq.browser.msie && srizonjq.browser.version < 7)) { // take away IE6
		srizonjq("#srztb_window").css({marginTop: '-' + parseInt((srztb_HEIGHT / 2),10) + 'px'});
	}
}

function srztb_parseQuery ( query ) {
   var Params = {};
   if ( ! query ) {return Params;}// return empty object
   var Pairs = query.split(/[;&]/);
   for ( var i = 0; i < Pairs.length; i++ ) {
      var KeyVal = Pairs[i].split('=');
      if ( ! KeyVal || KeyVal.length != 2 ) {continue;}
      var key = unescape( KeyVal[0] );
      var val = unescape( KeyVal[1] );
      val = val.replace(/\+/g, ' ');
      Params[key] = val;
   }
   return Params;
}

function srztb_getPageSize(){
	var de = document.documentElement;
	var w = window.innerWidth || self.innerWidth || (de&&de.clientWidth) || document.body.clientWidth;
	var h = window.innerHeight || self.innerHeight || (de&&de.clientHeight) || document.body.clientHeight;
	arrayPageSize = [w,h];
	return arrayPageSize;
}

function srztb_detectMacXFF() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (userAgent.indexOf('mac') != -1 && userAgent.indexOf('firefox')!=-1) {
    return true;
  }
}


