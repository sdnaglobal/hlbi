(function(jQuery) {
	jQuery.fn.juserSlider = function(options) {
		var opts = jQuery.extend({}, jQuery.fn.juserSlider.defaults, options);
		return this.each(function() {
			var $juser_container 	= jQuery(this),
			o = jQuery.meta ? jQuery.extend({}, opts, $juser_container.data()) : opts;
			var $juser_juserslider		= jQuery('.boxsinglethumb',$juser_container),
			$juser_next		= jQuery('.next1',$juser_container),
			$juser_prev		= jQuery('.prev1',$juser_container),
			juserslideshow,
			total_elems = o.total,
			current			= 0;
			
			$juser_next.bind('click',function(){
				if(o.auto != 0){
					clearInterval(juserslideshow);
					juserslideshow	= setInterval(function(){
						$juser_next.trigger('click');
					},o.auto);
				}
				++current;
				if(current >= total_elems) current = 0;
				juserslide(current,$juser_juserslider,o);
			});
			$juser_prev.bind('click',function(){
				if(o.auto != 0){
					clearInterval(juserslideshow);
					juserslideshow	= setInterval(function(){
						$juser_prev.trigger('click');
					},o.auto);
				}
				--current;
				if(current < 0) current = total_elems - 1;
				juserslide(current,$juser_juserslider,o);
			});
			if(o.auto != 0){
				juserslideshow	= setInterval(function(){
					$juser_next.trigger('click');
				},o.auto);
			}
		});	
		
		jQuery.fn.juserSlider.defaults = {
			auto			: 0,	
			speed			: 1000,
			easing			: 'jswing',
		};
	}

	var juserslide			= function(current,$juser_juserslider,o){
		var juserslide_to	= parseInt(-o.srzn_juser_cont_width * current);
		if(o.vertical == true){
			$juser_juserslider.stop().animate({
					top : juserslide_to + 'px'
			},o.speed, o.easing);
		}
		else{
			$juser_juserslider.stop().animate({
					left : juserslide_to + 'px'
			},o.speed, o.easing);
		}
		
	}

})(jQuery);
