<?php
/**
 * @package			JUserTube 
 * @version			6.1.0
 *
 * @author			Md. Afzal Hossain <afzal.csedu@gmail.com>
 * @link			http://www.srizon.com
 * @copyright                   Copyright 2012 Md. Afzal Hossain All Rights Reserved
 * @license			http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
 // no direct access
defined( '_JEXEC' ) or die;
echo '<div class="jusertube" id="'.$scroller_id.'">';
if($autoplay_l == 'yes'){
    $doautoplay = "&amp;auto=1";
}
else{
    $doautoplay = "&amp;auto=0";
}
$i = 0;
$totvid = count($videos);
$ju_start = $_GET['jupage']*$totalvid;
$ju_end = min($totvid,($ju_start+$totalvid));
for($j=$ju_start;$j<$ju_end;$j++){
	$video = $videos[$j];
    $i++;
    if($i!=1) echo '<div class="divider"></div>';
    echo '<table><tr><td class="vtop">';
    if( $showinlightbox == 'yes'){
		$link = '<a class="srzthickbox" href="'.JURI::base().'index.php?option=com_jusertube&amp;view=lightbox&amp;rid='.$video['id'].'&amp;'.$yorvuser .'='.$youtubeuser.$doautoplay.'&amp;eh='.$eheight.'&amp;ew='.$ewidth.'&amp;st='.$showtitle.'&amp;height='.$popupy.'&amp;width='.$popupx.'&amp;srztb_iframe=true">';
    }
    else if( $showinlightbox == 'newp'){
		$link = '<a href="'.JURI::base().'index.php?option=com_jusertube&amp;view=video&amp;rid='.$video['id'].'&amp;'.$yorvuser .'='.$youtubeuser.$doautoplay.'&amp;eh='.$eheight.'&amp;ew='.$ewidth.'&amp;st='.$showtitle.'&amp;height='.$popupy.'&amp;width='.$popupx.'&amp;Itemid='.$juseritem.'">';
	}
	else if( $showinlightbox == 'res'){
		if(strpos($video['link'],'&')) $minlink = substr($video['link'],0,strpos($video['link'],'&'));
		else $minlink = $video['link'];
		$link = '<a class="magpopif" href="'.$minlink.'">';
	}
    else{
        $link = '<a target="_blank" href="'.$video['link'].'">';
    }
    $imgcode = str_replace("<img","<img alt=\"".$video['title']."\"",$video['img']);
    $imgcode = str_replace('alt=""','',$imgcode);
    if($vidicon == 'yes') $imgcode.='<div class="vid_icon"></div>';
    echo '<div class="imgbox">'.$link.$imgcode.'</a></div>';
    if($truncate_len!='' and strlen($video['title'])>$truncate_len){
        $video['title'] = substr($video['title'],0,$truncate_len).'...';
    }
    
    if($truncate_len2!='' and strlen($video['desc'])>$truncate_len2){
        $video['desc'] = substr($video['desc'],0,$truncate_len2).'...';
    }
    
    echo '</td><td class="vtop"><div class="txtbox">';
    echo $link.'<h3 class="videotitle">'.$video['title'].'</h3></a>';
    echo '<div class="videodecs">'.$video['desc'].'</div>';
    echo '</div>';
    echo '</td></tr></table>';
}
$totalpages = ceil($totvid/$totalvid);
$u = JURI::getInstance();
$url = $u->toString();
if($jupos = strpos($url,'jupage')){
	$url=substr($url,0,$jupos-1);
}
$url = str_replace('&', '&amp;', $url);
if(strpos($url,'?')) $url.='&amp;';
else $url.='?';
if($totalpages>1){
?>
<div id="tnt_pagination">
<?php
	if($_GET['jupage']>4){
		$pgstart = $_GET['jupage']-4;
		echo '<a href="'.$url.'jupage=0">First</a>';
	}
	else $pgstart = 0;
	$pgend = min($totalpages,$pgstart+10);
	
	for($k=$pgstart;$k<$pgend;$k++){
		if($k==$_GET['jupage']){
			echo '<span class="active_tnt_link">'.($k+1).'</span>';
		}
		else{
			echo '<a href="'.$url.'jupage='.$k.'">'.($k+1).'</a>';
		}
	}
	if($totalpages>$pgend){
		echo '<a href="'.$url.'jupage='.($totalpages-1).'">Last</a>';
	}
?>
</div>
<?php }?>
<div style="clear:both;"></div> 
<script type="text/javascript">
	<?php
		if($GLOBALS['jqlibraryju'] == 'srizonjq') $fname = 'srizonjq';
		else $fname = 'jQuery';
		echo $fname;?>(document).ready(function() {
		if(<?php echo $fname;?>('.magpopif').length){
		  <?php echo $fname;?>('.magpopif').magnificPopup({type:'iframe'});
	  }
		});
	</script>
</div>
