<?php
/**
 * @package			JUserTube 
 * @version			6.1.0
 *
 * @author			Md. Afzal Hossain <afzal.csedu@gmail.com>
 * @link			http://www.srizon.com
 * @copyright		Copyright 2012 Md. Afzal Hossain All Rights Reserved
 * @license			http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
 // no direct access
defined( '_JEXEC' ) or die;
$hidecode = $hidebutton? ' style="display:none;"':'';
echo '<div class="jusertube"  id="'.$scroller_id.'"><table><tr><td><div class="prev1"'.$hidecode.'></div></td><td><div class="masksinglethumb"><div class="boxsinglethumb">';
if($autoplay_l == 'yes'){
    $doautoplay = "&amp;auto=1";
}
else{
    $doautoplay = "&amp;auto=0";
}

$i=0;
for($j=0;$j<$totcol_int;$j++){
    $width1 = $thumbwidth+$grid_margin_right+$grid_margin_right;
    echo '<div style="width:'.$width1.'px;">';
    for($k=0;$k<$gridrow;$k++){
        if($i>=count($videos)) continue;
        $video = $videos[$i++];
        echo '<div style="padding:'.$grid_margin_top.'px '.$grid_margin_right.'px;">';
        //echo '<div style="padding:'.$grid_margin_top.'px '.$grid_margin_right.'px '.$grid_margin_bottom.'px '.$grid_margin_left.'px;">';
        if( $showinlightbox == 'yes'){
			$link = '<a class="srzthickbox" href="'.JURI::base().'index.php?option=com_jusertube&amp;view=lightbox&amp;rid='.$video['id'].'&amp;'.$yorvuser .'='.$youtubeuser.$doautoplay.'&amp;eh='.$eheight.'&amp;ew='.$ewidth.'&amp;st='.$showtitle.'&amp;height='.$popupy.'&amp;width='.$popupx.'&amp;srztb_iframe=true">';
		}
		else if( $showinlightbox == 'newp'){
			$link = '<a href="'.JURI::base().'index.php?option=com_jusertube&amp;view=video&amp;rid='.$video['id'].'&amp;'.$yorvuser .'='.$youtubeuser.$doautoplay.'&amp;eh='.$eheight.'&amp;ew='.$ewidth.'&amp;st='.$showtitle.'&amp;height='.$popupy.'&amp;width='.$popupx.'">';
		}
	else if( $showinlightbox == 'res'){
		if(strpos($video['link'],'&')) $minlink = substr($video['link'],0,strpos($video['link'],'&'));
		else $minlink = $video['link'];
		$link = '<a class="magpopif" href="'.$minlink.'">';
		}
		else{
			$link = '<a target="_blank" href="'.$video['link'].'">';
		}
        $imgcode = str_replace("<img","<img alt=\"".$video['title']."\"",$video['img']);
        $imgcode = str_replace('alt=""','',$imgcode);
        if($vidicon == 'yes') $imgcode.='<div class="vid_icon"></div>';
        echo '<div class="imgbox">'.$link.$imgcode.'</a></div>';
        if($truncate_len!='' and strlen($video['title'])>$truncate_len){
            $video['title'] = substr($video['title'],0,$truncate_len).'...';
        }
        
        if($showtitlethumb == 'yes'){
            echo '<div class="titlebelow">'.$link.$video['title'].'</a></div>';
        }
        echo '</div>';
    }
    echo '</div>';
}
echo '</div></div></td><td><div class="next1"'.$hidecode.'></div></td></tr></table> </div>';
?>
<script type="text/javascript">
<?php
	if($GLOBALS['jqlibraryju'] == 'srizonjq') $fname = 'srizonjq';
	else $fname = 'jQuery';
	echo $fname;?>(document).ready(function() {
		if(<?php echo $fname;?>('.magpopif').length){
		<?php echo $fname;?>('.magpopif').magnificPopup({type:'iframe'});
	  }
});
</script>
