<?php
/**
 * @package			JUserTube 
 * @version			6.1.0
 *
 * @author			Md. Afzal Hossain <afzal.csedu@gmail.com>
 * @link			http://www.srizon.com
 * @copyright		Copyright 2012 Md. Afzal Hossain All Rights Reserved
 * @license			http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
require_once (dirname(__FILE__) . '/mycurl.php');

class FeedReader
{
    var $cachetime;
	var $filepath;

	
	function FeedReader($updatefeed = 300){
		$this->cachetime = $updatefeed;
		$this->filepath = dirname(__FILE__).'/savedxml/';
	}

	function get_youtube_top($youtubeuser, $totalvid, $userorplaylist, $maxvidp, $paginate, $plsortorder){
        $filename = $this->filepath.'youtube_'.$youtubeuser.'.xml';
        if(is_file($filename)){
            $utime = filemtime($filename);
            $chtime = time() - 60*$this->cachetime;
            if($utime>$chtime) $updated = true;
            else $updated = false;
        }
        else{
            $updated = false;
        }
        if(!$updated){
			$toloop = ceil($maxvidp/50);
			$data = '';
			for($i=0;$i<$toloop;$i++){
				$stind = $i*50+1;
				$maxres = '&start-index='.$stind.'&max-results=50';
				if($userorplaylist == 'user'){
					$link="http://gdata.youtube.com/feeds/api/users/$youtubeuser/uploads?alt=rss".$maxres;
				}
				else if($userorplaylist == 'favourites'){
				   $link = "http://gdata.youtube.com/feeds/api/users/$youtubeuser/favorites?alt=rss".$maxres;
				}
				else{
					$maxres = '&orderby='.$plsortorder.$maxres;
					$link="http://gdata.youtube.com/feeds/api/playlists/$youtubeuser?alt=rss".$maxres;
				}
				if(0){
					$data1 = @file_get_contents( $link );
				}
				else{
					$jcurl = new mycurljusertube($link);
					$jcurl->createCurl();
					$data1 = $jcurl->tostring();
				}
				$data1 = str_replace('media:','media',$data1);
                $data1 = str_replace('app:','app',$data1);
				if($i>0){
					$substart = strpos($data1,'<item>');
					$subend = strpos($data1,'</channel>')+10;
					$data1 = substr($data1,$substart,$subend-$substart);
					if($substart){
						$data = str_replace('</channel>',$data1,$data);
					}
				}
				else $data=$data1;
			}
            if(strlen($data)>500) 
            {
                $retval = file_put_contents($filename,$data);
                if($retval == false){
                    echo "<strong>Can't create file inside <pre>/modules/mod_jusertube/savedxml</pre> Change the permission of that folder so that files can be created</strong>";
                }
            }
            else 
            {
                echo "<strong>Can't get data from youtube.<br />Probable causes listed below:</strong><p>1. Youtube username or Playlist is not valid with your selection. Please set the parameters correctly from module manager</p> <p>2. It might also be a problem with CURL library or your server config</p>";
                echo "<h3>Reply from youtube:</h3>";
                echo "<pre>";
                echo $data;
                echo "</pre>";
            }
        }
        
        if(is_file($filename)){
            $data = file_get_contents($filename);
        }
        else{
            return false;
        }

		$rss = new SimpleXMLElement($data);
		$i = 0;
        $videos = array();
		foreach($rss->channel->item as $item){
            $guid_split = parse_url($item->link);
            parse_str($guid_split['query'],$temp_v);
            $videos[$i]['id'] = $temp_v['v'];
            $videos[$i]['title'] = (string) $item->title;
			$videos[$i]['title'] = htmlspecialchars($videos[$i]['title']);
            $videos[$i]['link'] = (string) $item->link;
            $videos[$i]['desc'] = (string) $item->description;
			$videos[$i]['desc'] = htmlspecialchars($videos[$i]['desc']);
            $videos[$i]['img'] = '<img src="'.$item->mediagroup->mediathumbnail['url'].'" />';
			if($GLOBALS['pageprotocol']=='https'){
				$videos[$i]['img'] = str_replace('http:','https:',$videos[$i]['img']);
				$videos[$i]['link'] = str_replace('http:','https:',$videos[$i]['link']);
			}
			$i++;
			if($i>=$totalvid && $paginate==0) break;
		}
		return $videos;
	}
    function get_vimeo_top($youtubeuser,$totalvid, $userorplaylist, $paginate){
        $filename = $this->filepath.'vimeo_'.$youtubeuser.'.xml';
        if(is_file($filename)){
            $utime = filemtime($filename);
            $chtime = time() - 60*$this->cachetime;
            if($utime>$chtime) $updated = true;
            else $updated = false;
        }
        else{
            $updated = false;
        }
        if(!$updated){
            if($userorplaylist == 'channelv'){
				$link = "http://vimeo.com/api/v2/channel/$youtubeuser/videos.xml";
            }
			else if($userorplaylist == 'albumv'){
				$link = "http://vimeo.com/api/v2/album/$youtubeuser/videos.xml";
            }
            else{
				$link = "http://vimeo.com/api/v2/$youtubeuser/videos.xml";
            }
			for($i=1;$i<4;$i++){
				$link1 = $link.'?page='.$i;
				if(0){
					$data1 = @file_get_contents( $link1 );
				}
				else{
					$jcurl = new mycurljusertube($link1);
					$jcurl->createCurl();
					$data1 = $jcurl->tostring();
				}
				if($i>1){
					$substart = strpos($data1,'<video>');

					$data1 = substr($data1,$substart);
					if($substart){
						$data = str_replace('</videos>',$data1,$data);
					}
				}
				else $data=$data1;
			}
			
            if(strlen($data)>100) 
            {
                $retval = file_put_contents($filename,$data);
                if($retval == false){
                    echo "<strong>Can't create file inside <pre>/modules/mod_jusertube/savedxml</pre> Change the permission of that folder so that files can be created</strong>";
                }
            }
            else 
            {
                echo "<strong>Can't get data from Vimeo.<br />Probable causes listed below:</strong><p>1. Vimeo channel name is not valid.</p> <p>2. It might also be a problem with CURL library or your server config</p>";
                echo "<h3>Reply from vimeo:</h3>";
                echo "<pre>";
                echo $data;
                echo "</pre>";
            }
        }
        
        if(is_file($filename)){
            $data = file_get_contents($filename);
        }
        else{
            return false;
        }

		$rss = new SimpleXMLElement($data);
		$i = 0;
        $videos = array();
		foreach($rss->video as $item){

            $videos[$i]['id'] = (string) $item->id;
            $videos[$i]['title'] = (string) $item->title;
			$videos[$i]['title'] = htmlspecialchars($videos[$i]['title']);
            $videos[$i]['link'] = (string) $item->url;
            $videos[$i]['desc'] = (string) $item->description;
			//$videos[$i]['desc'] = substr($videos[$i]['desc'],0,strpos($videos[$i]['desc'],'Cast:'));
			//$videos[$i]['desc'] = strip_tags($videos[$i]['desc']);
			//$videos[$i]['desc'] = htmlspecialchars($videos[$i]['desc']);
			//$videos[$i]['desc'] = nl2br($videos[$i]['desc']);
            $videos[$i]['img'] = '<img src="'.$item->thumbnail_medium.'" />';
			$i++;
			if($i>=$totalvid && $paginate==0) break;
		}
		return $videos;
	}
    
}
