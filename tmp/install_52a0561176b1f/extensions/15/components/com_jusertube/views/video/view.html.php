<?php
/**
 * @package			JUserTube 
 * @version			6.1.0
 *
 * @author			Md. Afzal Hossain <afzal.csedu@gmail.com>
 * @link			http://www.srizon.com
 * @copyright		Copyright 2012 Md. Afzal Hossain All Rights Reserved
 * @license			http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * Content categories view.
 *
 * @package		Joomla.Site
 * @subpackage	com_weblinks
 * @since 1.5
 */
class JusertubeViewVideo extends JView
{

	function display()
	{
		$filepath = dirname(__FILE__).'/../../../../modules/mod_jusertube/'.'savedxml'.'/';
		if(!isset($_GET['yuser'])) $_GET['yuser'] = 0;
		if(!isset($_GET['auto'])) $_GET['auto'] = 0;
		$rid = $_GET['rid'];
		$auto = $_GET['auto'];
		if(isset($_GET['ttv']))	$ttv = $_GET['ttv'];
		else $ttv = 0;
		if($_GET['yuser']){
			$youtubeuser = $_GET['yuser'];
			$filename = $filepath.'youtube_'.$youtubeuser.'.xml';
		}
		else{
			$youtubeuser = $_GET['vuser'];
			$filename = $filepath.'vimeo_'.$youtubeuser.'.xml';
		}
		if($auto == 1){
			$autotext = '&amp;autoplay=1';
		}
		else{
			$autotext = '&amp;autoplay=0';
		}
		if(is_file($filename)){
			$data = file_get_contents($filename);
		}
		else{
			return JError::raiseWarning(404,'This requested video no longer exists on this site');
		}
		$rss = new SimpleXMLElement($data);
		$videos = array();
		$u =& JURI::getInstance();
		$url = $u->toString();
		$p2 = strpos($url,'&auto=');
		$url = substr($url,0,$p2);
		$url.='&auto=1&eh=385&ew=640&st=yes';
		$url = str_replace('&','&amp;',$url);
		$vidfound = 0;
		if($_GET['yuser']){
			foreach($rss->channel->item as $item){
				$guid_split = parse_url($item->link);
				parse_str($guid_split['query'],$temp_v);
				$tid = $temp_v['v'];
				
				if($tid == $rid)
				{
					$videos['title'] = (string) $item->title;
					$videos['embed'] = '<iframe class="youtube-player" type="text/html" width="'.$_GET['ew'].'" height="'.$_GET['eh'].'" src="http://www.youtube.com/embed/'.$tid.'?fs=1&amp;rel=0'.$autotext.'" frameborder="0"></iframe>';
					$vidfound = 1;
					if($GLOBALS['pageprotocol']=='https'){
						$videos['embed'] = str_replace('http:','https:',$videos['embed']);
					}
					break;
				}
			}
			if($vidfound == 0) return JError::raiseWarning(404,'This requested video no longer exists on this site');
		}
		else{
			foreach($rss->video as $item){
				$tlnk = $item->link;
				$tid = $item->id;

				if($tid == $rid)
				{
					str_replace('&amp;','',$autotext);
					$videos['title'] = (string) $item->title;
					$videos['embed'] = '<iframe src="http://player.vimeo.com/video/'.$tid.'?'.$autotext.'" width="'.$_GET['ew'].'" height="'.$_GET['eh'].'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
					$vidfound = 1;
					break;
				}
			}
			if($vidfound == 0) return JError::raiseWarning(404,'This requested video no longer exists on this site');
		}
?>
<?php if( (isset($GLOBALS['srizon_showfbcomment']) and $GLOBALS['srizon_showfbcomment'] == 'yes') or (isset($GLOBALS['srizon_showfblike']) and $GLOBALS['srizon_showfblike'] == 'yes' ) ){
	if(isset($GLOBALS['srizon_fbappid']) and $GLOBALS['srizon_fbappid']!=''){
		$appIdstr = '&appId='.$GLOBALS['srizon_fbappid'];
	}
	else{
		$appIdstr = '';
	}
?>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1<?php echo $appIdstr;?>";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
<?php }?>
		<div>
			<?php if($_GET['st']=='yes'){?>
			<h2 style="text-align:center; margin: 10px;"><?php echo $videos['title']?></h2>
			<?php }?>
			<table style="margin: 0 auto;">
				<tr>
					<td colspan="2">
					<?php echo $videos['embed'];?>
					</td>
				</tr>
<?php if(isset($GLOBALS['srizon_showgplus']) and $GLOBALS['srizon_showgplus'] == 'yes' and isset($GLOBALS['srizon_showfblike']) and $GLOBALS['srizon_showfblike'] == 'yes' ){?>
				<tr>
					<td style="vertical-align: top;">
						<div class="fb-like" data-send="true" data-width="<?php echo ($_GET['ew']/2);?>" data-href="<?php echo $url;?>" data-show-faces="true"></div>
					</td>
					<td style="vertical-align: top;">
						<div class="g-plusone" data-annotation="inline" data-width="<?php echo ($_GET['ew']/2);?>" data-href="<?php echo $url;?>"></div>
						<script type="text/javascript">
						  (function() {
							var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
							po.src = 'https://apis.google.com/js/plusone.js';
							var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
						  })();
						</script>
					</td>
				</tr>
<?php }				
	else if(isset($GLOBALS['srizon_showgplus']) and $GLOBALS['srizon_showgplus'] == 'yes'){?>
				<tr>
					<td colspan="2">
						<div class="g-plusone" data-annotation="inline" data-width="<?php echo $_GET['ew'];?>" data-href="<?php echo $url;?>"></div>
						<script type="text/javascript">
						  (function() {
							var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
							po.src = 'https://apis.google.com/js/plusone.js';
							var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
						  })();
						</script>
					</td>
				</tr>
<?php }
	else if(isset($GLOBALS['srizon_showfblike']) and $GLOBALS['srizon_showfblike'] == 'yes'){?>
				<tr>
					<td colspan="2">
						<div class="fb-like" data-send="true" data-width="<?php echo $_GET['ew'];?>" data-href="<?php echo $url;?>" data-show-faces="true"></div>
					</td>
				</tr>	
<?php }
	if(isset($GLOBALS['srizon_showfbcomment']) and $GLOBALS['srizon_showfbcomment'] == 'yes'){?>				
				<tr>
					<td colspan="2">
						<div class="fb-comments" data-href="<?php echo $url;?>" data-num-posts="<?php echo $GLOBALS['srizon_fbcmntpage'];?>" data-width="<?php echo $_GET['ew'];?>"></div>
					</td>
				</tr>
<?php }?>				
			</table>
		</div>
<?php
    }

}
