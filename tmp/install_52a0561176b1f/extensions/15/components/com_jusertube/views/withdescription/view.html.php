<?php
/**
 * @package			JUserTube 
 * @version			6.1.0
 *
 * @author			Md. Afzal Hossain <afzal.csedu@gmail.com>
 * @link			http://www.srizon.com
 * @copyright                   Copyright 2012 Md. Afzal Hossain All Rights Reserved
 * @license			http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class JusertubeViewWithdescription extends JView
{

	function display($tpl = null)
	{
		$app		= JFactory::getApplication();
		$params		= $app->getParams();
		// Include the whosonline functions only once
		require_once (dirname(__FILE__).'/../../../../modules/mod_jusertube/helper.php');
		if(!isset($_GET['jupage'])) $_GET['jupage'] = 0;
		$userorplaylist = $params->get('userorplaylist','user');
		$yorvuser = 'yuser';
		if($userorplaylist == 'favourites') $youtubeuser = $params->get('youtubeuserfav','');
		else if($userorplaylist == 'playlist') $youtubeuser = $params->get('youtubeuserpl','');
		else if($userorplaylist == 'channelv') {
			$youtubeuser = $params->get('vimeochannel','');
			$yorvuser = 'vuser';
		}
		else if($userorplaylist == 'albumv') {
			$youtubeuser = $params->get('vimeoalbum','');
			$yorvuser = 'vuser';
		}
		else if($userorplaylist == 'userv') {
			$youtubeuser = $params->get('vimeouser','');
			$yorvuser = 'vuser';
		}
		else $youtubeuser = $params->get('youtubeuser','ClevverMovies');

		/*$starts_with_pl = substr($youtubeuser,0,2);
		if($starts_with_pl == 'PL'){
			$youtubeuser = substr($youtubeuser,2);
		}*/
		$plsortorder = $params->get('plsortorder','published');
		if($plsortorder == 'positionr'){
			$plsortorder = 'position';
			$needtoreverse = true;
		}
		else{
			$needtoreverse = false;
		}
		$updatefeed = $params->get('updatefeed',300);

		$vidicon = $params->get('vidicon','yes');
		$totalvid = $params->get('totalvideo',5);
		$totalvideop = $params->get('totalvideop',50);
		$thumbwidth = $params->get('thumbwidth','200');
		$trimthumb = $params->get('trimthumb','no');
		$tpltheme = $params->get('tpltheme','white');

		$showinlightbox = $params->get('showinlightbox','yes');
		$autoplay_l = $params->get('autoplay_l','yes');

		$showtitle = $params->get('showtitle','yes');
		$truncate_len = $params->get('truncate_len',''); 
		$truncate_len2 = $params->get('truncate_len2','100'); 
		$showtitlethumb = $params->get('showtitlethumb','yes');
		$titlethumb_height = (int) $params->get('titlethumb_height',50);

		$popupx = $params->get('popupx','680');
		$popupy = $params->get('popupy','500');

		$ewidth = $params->get('ewidth','640');
		$eheight = $params->get('eheight','385');
		$juseritem = JRequest::getVar('Itemid');

		$module_base    = JURI::base() . 'modules/mod_jusertube/';
		$rspath = JURI::base() . 'media/jusertube/';
		$document = JFactory::getDocument();

		$vid = new FeedReader($updatefeed);
		if($userorplaylist == 'channelv' or $userorplaylist == 'userv' or $userorplaylist == 'albumv'){
			$videos = $vid->get_vimeo_top($youtubeuser,$totalvid, $userorplaylist, 1);
		}
		else{
			$videos = $vid->get_youtube_top($youtubeuser,$totalvid, $userorplaylist, $totalvideop, 1, $plsortorder);
			if($needtoreverse) $videos = array_reverse($videos);
		}
		if (!isset($GLOBALS['jusermod'])) {
			$GLOBALS['jusermod'] = 1;
		} else {
			$GLOBALS['jusermod']++;
		}
		$scroller_id = 'jusertube-scroller-' . $GLOBALS['jusermod'];
		if( version_compare( JVERSION, '1.6.0', 'ge' ) ){
			if ($params->get('show_page_heading', 1)) echo '<h2 class="contentheading">'.$this->escape($params->get('page_heading')).'</h2>';
		}
		else{
			if ($params->get('show_page_title', 1)) echo '<div class="componentheading">'.$this->escape($params->get('page_title')).'</div>';
		}
		$liststyle = 'withdescription';
		$fromcomponent = 1;
		include (dirname(__FILE__).'/../../../../modules/mod_jusertube/layouts/all.css.php');
		include(dirname(__FILE__).'/withdescription.php');
		parent::display($tpl);
	}

}
