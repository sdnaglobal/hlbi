<?php
/**
 * @package			JUserTube 
 * @version			6.1.0
 *
 * @author			Md. Afzal Hossain <afzal.csedu@gmail.com>
 * @link			http://www.srizon.com
 * @copyright		Copyright 2012 Md. Afzal Hossain All Rights Reserved
 * @license			http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * Content categories view.
 *
 * @package		Joomla.Site
 * @subpackage	com_weblinks
 * @since 1.5
 */
class JusertubeViewVideobox extends JView
{

	function display()
	{
		$filepath = dirname(__FILE__).'/../../../../modules/mod_jusertube/'.'savedxml'.'/';
		if(!isset($_GET['yuser'])) $_GET['yuser'] = 0;
		if(!isset($_GET['auto'])) $_GET['auto'] = 0;
		$rid = $_GET['rid'];
		$auto = $_GET['auto'];
		if(isset($_GET['ttv']))	$ttv = $_GET['ttv'];
		else $ttv = 0;
		if($_GET['yuser']){
			$youtubeuser = $_GET['yuser'];
			$filename = $filepath.'youtube_'.$youtubeuser.'.xml';
		}
		else{
			$youtubeuser = $_GET['vuser'];
			$filename = $filepath.'vimeo_'.$youtubeuser.'.xml';
		}
		if($auto == 1){
			$autotext = '&autoplay=1';
		}
		else{
			$autotext = '&autoplay=0';
		}
		if(is_file($filename)){
			$data = file_get_contents($filename);
		}
		$rss = new SimpleXMLElement($data);
		$videos = array();
		if($_GET['yuser']){
			foreach($rss->channel->item as $item){
				$guid_split = parse_url($item->link);
				parse_str($guid_split['query'],$temp_v);
				$tid = $temp_v['v'];
				
				if($tid == $rid)
				{
					$videos['title'] = (string) $item->title;
					$videos['embed'] = '<iframe class="youtube-player" type="text/html" width="'.$_GET['ew'].'" height="'.$_GET['eh'].'" src="http://www.youtube.com/embed/'.$tid.'?fs=1&rel=0&wmode=transparent'.$autotext.'" frameborder="0"></iframe>';
					if($GLOBALS['pageprotocol']=='https'){
						$videos['embed'] = str_replace('http:','https:',$videos['embed']);
					}
					break;
				}
			}
		}
		else{
			foreach($rss->video as $item){
				$tlnk = $item->link;
				$tid = $item->id;

				if($tid == $rid)
				{
					str_replace('&','',$autotext);
					$videos['title'] = (string) $item->title;
					$videos['embed'] = '<iframe src="http://player.vimeo.com/video/'.$tid.'?'.$autotext.'" width="'.$_GET['ew'].'" height="'.$_GET['eh'].'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
					break;
				}
			}

		}
?>
	<div>
	<style>
	.juser-vid-container {
		position: relative;
		padding-bottom: 56.25%;
		padding-top: 30px; height: 0; overflow: hidden;
		max-width: <?php echo $_GET['ew'];?>px;
		margin:0 auto;
	}
	.juser-vid-container iframe,
	.juser-vid-container object,
	.juser-vid-container embed {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
	}
	</style>
		<?php if($_GET['st']=='yes'){?>
		<h2 style="text-align:center; margin: 10px;"><?php echo $videos['title']?></h2>
		<?php }?>
		<table style="margin: 0 auto; width:100%; height:auto;">		
			<tr>
				<td>
				<div class="juser-vid-container"><?php echo $videos['embed'];?></div>
				</td>
			</tr>
		</table>
	</div>
<?php
		exit(0);
    }

}
