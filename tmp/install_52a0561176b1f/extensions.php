<?php
/**
 * Install File
 * Does the stuff for the specific extensions
 *
 * @package			JUserTube 
 * @version			5.2.0
 *
 * @author			Md. Afzal Hossain <afzal.csedu@gmail.com>
 * @link			http://www.srizon.com
 * @copyright		Copyright � 2012 Md. Afzal Hossain All Rights Reserved
 * @license			http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// No direct access
defined( '_JEXEC' ) or die();

$name = 'JUserTube';
$alias = 'jusertube';
$ext = $name;

// SYSTEM PLUGIN
$states[] = installExtension( $alias, $name.' Resource Loader', 'plugin', array('folder'=>'system'));
//Module
$states[] = installExtension( $alias, $name, 'module');
$states[] = installExtension( $alias, $name, 'component');
