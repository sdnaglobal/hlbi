<?php
/**
 * @package			JUserTube
 * @version			6.1.0
 *
 * @author			Md. Afzal Hossain <afzal.csedu@gmail.com>
 * @link			http://www.srizon.com
 * @copyright                   Copyright 2012 Md. Afzal Hossain All Rights Reserved
 * @license			http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

// Include the whosonline functions only once
require_once (dirname(__FILE__).'/helper.php');

$userorplaylist = $params->get('userorplaylist','user');
$yorvuser = 'yuser';
if($userorplaylist == 'favourites') $youtubeuser = $params->get('youtubeuserfav','');
else if($userorplaylist == 'playlist') $youtubeuser = $params->get('youtubeuserpl','');
else if($userorplaylist == 'channelv') {
	$youtubeuser = $params->get('vimeochannel','');
			$yorvuser = 'vuser';
		}
		else if($userorplaylist == 'albumv') {
			$youtubeuser = $params->get('vimeoalbum','');
	$yorvuser = 'vuser';
}
else if($userorplaylist == 'userv') {
	$youtubeuser = $params->get('vimeouser','');
	$yorvuser = 'vuser';
}
else $youtubeuser = $params->get('youtubeuser','ClevverMovies');

/*$starts_with_pl = substr($youtubeuser,0,2);
if($starts_with_pl == 'PL'){
    $youtubeuser = substr($youtubeuser,2);
}*/
$plsortorder = $params->get('plsortorder','published');
if($plsortorder == 'positionr'){
	$plsortorder = 'position';
	$needtoreverse = true;
}
else{
	$needtoreverse = false;
}
$updatefeed = $params->get('updatefeed',300);
$totalvid = $params->get('totalvideo',18);
$totalvideop = $params->get('totalvideop',50);
$thumbwidth = $params->get('thumbwidth','200');
$trimthumb = $params->get('trimthumb','no');
$vidicon = $params->get('vidicon','yes');
$liststyle = $params->get('liststyle','vertical');
$tpltheme = $params->get('tpltheme','white');

$showinlightbox = $params->get('showinlightbox','yes');
$autoplay_l = $params->get('autoplay_l','yes');
$autoplay = $params->get('autoplay', 1) ? 'true' : 'false';
$hidebutton = $params->get('showbutton',1)? false:true;

$slide_interval = $params->get('slide_interval', 5000);
if($autoplay == 'false') $slide_interval = 0;

$showtitle = $params->get('showtitle','yes');
$truncate_len = $params->get('truncate_len',''); 
$truncate_len2 = $params->get('truncate_len2','100'); 
$showtitlethumb = $params->get('showtitlethumb','yes');
$titlethumb_height = (int) $params->get('titlethumb_height',50);

$popupx = $params->get('popupx','680');
$popupy = $params->get('popupy','500');

$ewidth = $params->get('ewidth','640');
$eheight = $params->get('eheight','385');

/* Grid Related Options*/
$gridrow = $params->get('gridrow','3');
$gridcol = $params->get('gridcol','3');
$grid_margin_top = $params->get('grid_margin_top','3');
$grid_margin_right = $params->get('grid_margin_right','3');

/*Responsive slider related options*/
$respslidespeed = $params->get('respslidespeed','500');
$respslideminitem = $params->get('respslideminitem','2');
$respslidestart = $params->get('respslidestart','0');
$respslidescale = $params->get('respslidescale','3');
if(!($popupx>=100 and $popupx<=1200)) $popupx = 680;
if(!($popupy>=100 and $popupy<=900)) $popupy = 500;

if(!($ewidth>=100 and $ewidth<=1200)) $ewidth = 640;
if(!($eheight>=100 and $eheight<=900)) $eheight = 385;


$module_base    = JURI::base() . 'modules/mod_jusertube/';
$rspath = JURI::base() . 'media/jusertube/';
$document =& JFactory::getDocument();

$vid = new FeedReader($updatefeed);
if($userorplaylist == 'channelv' or $userorplaylist == 'userv' or $userorplaylist == 'albumv'){
	$videos = $vid->get_vimeo_top($youtubeuser,$totalvid, $userorplaylist, 0);
}
else{
	$videos = $vid->get_youtube_top($youtubeuser,$totalvid, $userorplaylist, $totalvideop, 0, $plsortorder);
	if($needtoreverse) $videos = array_reverse($videos);
}

if($liststyle == 'vertical'){
	$inlinestyle = 'style="display:block;clear:both;"';
}
else{
	$inlinestyle = 'style="display:block;float:left;clear:none;margin:10px;text-align:center;"';
}

if (!isset($GLOBALS['jusermod'])) {
	$GLOBALS['jusermod'] = 1;
} else {
	$GLOBALS['jusermod']++;
}

$scroller_id = 'jusertube-scroller-' . $GLOBALS['jusermod'];

include('layouts/all.css.php');

if($liststyle == 'vertical' || $liststyle == 'horizontal')
{
    include('layouts/no_scroller.php');
}
else if($liststyle == 'withdescription'){
    include('layouts/withdescription.php');
}
else if($liststyle == 'respslider'){
    include('layouts/respslider.php');
}
else if($liststyle == 'respsliderbox'){
    include('layouts/respsliderbox.php');
}
else if($liststyle == 'slidergrid' || $liststyle == 'slidergridv' || $liststyle == 'slidergridbox'){
    if($liststyle == 'slidergrid') {
		include('layouts/slidergrid.php');
		$vertical = 'false';
		$slideamount = $maskwidth;
	}
	else if($liststyle == 'slidergridbox') {
		include('layouts/slidergridbox.php');
		$vertical = 'false';
		$slideamount = $maskwidth;
	}
	else{
		include('layouts/slidergridv.php');
		$vertical = 'true';
		$slideamount = $maskheight;
	}
    ?>
	<script type="text/javascript">
	<?php
		if($GLOBALS['jqlibraryju'] == 'srizonjq') $fname = 'srizonjq';
		else $fname = 'jQuery';
		echo $fname;?>(function() {
				<?php echo $fname;?>('#<?php echo $scroller_id?>').juserSlider({
					'speed':<?php echo $params->get('slide_speed',300);?>,
					'auto':<?php echo $slide_interval;?>,
					'easing':'<?php echo $params->get('slide_style','jswing');?>',
					'total':<?php echo ($totcol_int/$gridcol);?>,
					'srzn_juser_cont_width': <?php echo $slideamount;?>,
					'vertical': <?php echo $vertical;?>
				});
		});
	</script>
    <?php
}
