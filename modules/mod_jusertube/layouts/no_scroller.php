<?php
/**
 * @package			JUserTube 
 * @version			6.1.0
 *
 * @author			Md. Afzal Hossain <afzal.csedu@gmail.com>
 * @link			http://www.srizon.com
 * @copyright		Copyright 2012 Md. Afzal Hossain All Rights Reserved
 * @license			http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
 // no direct access
defined( '_JEXEC' ) or die;
echo '<div class="jusertube" id="'.$scroller_id.'">';
if($autoplay_l == 'yes'){
    $doautoplay = "&amp;auto=1";
}
else{
    $doautoplay = "&amp;auto=0";
}
$i = 0;
foreach ($videos as $video){
    $i++;
    echo "<div $inlinestyle>";
    if( $showinlightbox == 'yes'){
		$link = '<a class="srzthickbox" href="'.JURI::base().'index.php?option=com_jusertube&amp;view=lightbox&amp;rid='.$video['id'].'&amp;'.$yorvuser .'='.$youtubeuser.$doautoplay.'&amp;eh='.$eheight.'&amp;ew='.$ewidth.'&amp;st='.$showtitle.'&amp;height='.$popupy.'&amp;width='.$popupx.'&amp;srztb_iframe=true">';
    }
    else if( $showinlightbox == 'newp'){
		$link = '<a href="'.JURI::base().'index.php?option=com_jusertube&amp;view=video&amp;rid='.$video['id'].'&amp;'.$yorvuser .'='.$youtubeuser.$doautoplay.'&amp;eh='.$eheight.'&amp;ew='.$ewidth.'&amp;st='.$showtitle.'&amp;height='.$popupy.'&amp;width='.$popupx.'">';
	}
	else if( $showinlightbox == 'res'){
		if(strpos($video['link'],'&')) $minlink = substr($video['link'],0,strpos($video['link'],'&'));
		else $minlink = $video['link'];
		$link = '<a class="magpopif" href="'.$minlink.'">';
	}
    else{
        $link = '<a target="_blank" href="'.$video['link'].'">';
    }
    $imgcode = str_replace("<img","<img alt=\"".$video['title']."\"",$video['img']);
    $imgcode = str_replace('alt=""','',$imgcode);
        if($vidicon == 'yes') $imgcode.='<div class="vid_icon"></div>';
    echo '<div class="imgbox">'.$link.$imgcode.'</a></div>';
    if($truncate_len!='' and strlen($video['title'])>$truncate_len){
        $video['title'] = substr($video['title'],0,$truncate_len).'...';
    }
    
    if($showtitlethumb == 'yes'){
        echo '<div class="titlebelow">'.$link.$video['title'].'</a></div>';
    }
    echo '</div>';
}
echo '<div style="clear:both;"></div> </div>';
?>
<script type="text/javascript">
<?php
	if($GLOBALS['jqlibraryju'] == 'srizonjq') $fname = 'srizonjq';
	else $fname = 'jQuery';
	echo $fname;?>(document).ready(function() {
		if(<?php echo $fname;?>('.magpopif').length){
	  <?php echo $fname;?>('.magpopif').magnificPopup({type:'iframe'});
	  }
});
</script>