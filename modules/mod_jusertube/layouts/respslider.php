<?php
/**
 * @package			JUserTube 
 * @version			6.1.0
 *
 * @author			Md. Afzal Hossain <afzal.csedu@gmail.com>
 * @link			http://www.srizon.com
 * @copyright		Copyright 2012 Md. Afzal Hossain All Rights Reserved
 * @license			http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
 // no direct access
defined( '_JEXEC' ) or die;

echo '<ul class="elastislide-list"  id="'.$scroller_id.'">';
if($autoplay_l == 'yes'){
    $doautoplay = "&amp;auto=1";
}
else{
    $doautoplay = "&amp;auto=0";
}

$i=0;
for($j=0;$j<count($videos);$j++){
	$video = $videos[$i++];

	if( $showinlightbox == 'yes'){
		$link = '<a class="srzthickbox" title="'.$video['title'].'" href="'.JURI::base().'index.php?option=com_jusertube&amp;view=lightbox&amp;rid='.$video['id'].'&amp;'.$yorvuser .'='.$youtubeuser.$doautoplay.'&amp;eh='.$eheight.'&amp;ew='.$ewidth.'&amp;st='.$showtitle.'&amp;height='.$popupy.'&amp;width='.$popupx.'&amp;srztb_iframe=true">';
	}
	else if( $showinlightbox == 'newp'){
		$link = '<a title="'.$video['title'].'" href="'.JURI::base().'index.php?option=com_jusertube&amp;view=video&amp;rid='.$video['id'].'&amp;'.$yorvuser .'='.$youtubeuser.$doautoplay.'&amp;eh='.$eheight.'&amp;ew='.$ewidth.'&amp;st='.$showtitle.'&amp;height='.$popupy.'&amp;width='.$popupx.'">';
	}
	else if( $showinlightbox == 'res'){
		if(strpos($video['link'],'&')) $minlink = substr($video['link'],0,strpos($video['link'],'&'));
		else $minlink = $video['link'];
		$link = '<a class="magpopif" title="'.$video['title'].'" href="'.$minlink.'">';
	}
	else{
		$link = '<a target="_blank" title="'.$video['title'].'" href="'.$video['link'].'">';
	}
	
	$imgcode = str_replace("<img","<img alt=\"".$video['title']."\"",$video['img']);
	$imgcode = str_replace('alt=""','',$imgcode);

	echo '<li>'.$link.$imgcode.'</a></li>';
}
echo '</ul>';
?>
<script type="text/javascript">
<?php
	if($GLOBALS['jqlibraryju'] == 'srizonjq') $fname = 'srizonjq';
	else $fname = 'jQuery';
	echo $fname;?>(document).ready(function() {
		if(<?php echo $fname;?>('.magpopif').length){
	  <?php echo $fname;?>('.magpopif').magnificPopup({type:'iframe'});
	  }
});
	<?php echo $fname;?>( '#<?php echo $scroller_id;?>' ).elastislide({minItems : <?php echo $respslideminitem;?>, scaledown : <?php echo $respslidescale;?>, speed : <?php echo $respslidespeed;?>, start : <?php echo $respslidestart;?>});
</script>
