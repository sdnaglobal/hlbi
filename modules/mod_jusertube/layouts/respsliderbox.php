<?php
/**
 * @package			JUserTube 
 * @version			6.1.0
 *
 * @author			Md. Afzal Hossain <afzal.csedu@gmail.com>
 * @link			http://www.srizon.com
 * @copyright		Copyright 2012 Md. Afzal Hossain All Rights Reserved
 * @license			http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
 // no direct access
defined( '_JEXEC' ) or die;
echo '<div style="width:100%" id="vid'.$scroller_id.'"></div>';
echo '<ul class="elastislide-list"  id="'.$scroller_id.'">';
if($autoplay_l == 'yes'){
    $doautoplay = "&auto=1";
}
else{
    $doautoplay = "&auto=0";
}
if($GLOBALS['jqlibraryju'] == 'srizonjq') $fname = 'srizonjq';
else $fname = 'jQuery';
$i=0;
for($j=0;$j<count($videos);$j++){
	$video = $videos[$i++];

	$link1 = JURI::base().'index.php?option=com_jusertube&view=videobox&rid='.$video['id'].'&'.$yorvuser .'='.$youtubeuser.$doautoplay.'&eh='.$eheight.'&ew='.$ewidth.'&st='.$showtitle.'&height='.$popupy.'&width='.$popupx;
	$onclickfunc = 'onclick="'.$fname.'(\'#vid'.$scroller_id.'\').load(\''.$link1.'\');"';
	$link = '<a href="javascript:void(0)" title="'.$video['title'].'" '.$onclickfunc.'>';
	if(!isset($initboxjs)){
		$link_n_auto = JURI::base().'index.php?option=com_jusertube&view=videobox&rid='.$video['id'].'&'.$yorvuser .'='.$youtubeuser.'&auto=0&eh='.$eheight.'&ew='.$ewidth.'&st='.$showtitle.'&height='.$popupy.'&width='.$popupx;
		$initboxjs = '<script type="text/javascript">'.$fname.'(\'#vid'.$scroller_id.'\').load(\''.$link_n_auto.'\');</script>';
	}
	
	$imgcode = str_replace("<img","<img alt=\"".$video['title']."\"",$video['img']);
	$imgcode = str_replace('alt=""','',$imgcode);

	echo '<li>'.$link.$imgcode.'</a></li>';
}
echo '</ul>';
echo $initboxjs;
?>
<script type="text/javascript">
	<?php echo $fname;?>( '#<?php echo $scroller_id;?>' ).elastislide({minItems : <?php echo $respslideminitem;?>, scaledown : <?php echo $respslidescale;?>, speed : <?php echo $respslidespeed;?>, start : <?php echo $respslidestart;?>});
</script>
