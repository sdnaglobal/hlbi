<?php
$thumbheight = $thumbwidth * 0.75 ;
$dyncss = '#'.$scroller_id.' div.vid_icon, #'.$scroller_id.' div.imgbox, #'.$scroller_id.' div.imgbox a img{
	width: '.$thumbwidth.'px;
	height: '.$thumbheight.'px;
	border: 0;
	position: relative;
}
#'.$scroller_id.' div.titlebelow{
	width: '.$thumbwidth.'px;
	height: '.$titlethumb_height.'px;
	overflow: hidden;
}
#'.$scroller_id.' img,#'.$scroller_id.' table, #'.$scroller_id.' tr, #'.$scroller_id.' td{
	border: 0;margin: 0;padding: 0;
}
#'.$scroller_id.' table, #'.$scroller_id.' table tr{
	direction: ltr;
}
#'.$scroller_id.' div.imgbox a, div.imgbox a:hover, div.imgbox a:active, div.imgbox a:focus{
	background:none;
	border:none;
	text-decoration:none;
	font-size:0;
}';

if($vidicon == 'yes'){
$dyncss .= '#'.$scroller_id.' div.imgbox a{
	position: absolute;
	left: 0;
	top: 0;
}';
}

if($trimthumb == 'yes'){

$trimwidth = $thumbwidth * 1.5;
$trimheight = $thumbwidth*1.5*0.75;
$trimtopmargin = $thumbwidth*.2;
$trimleftmargin = $thumbwidth*.25;

$dyncss .= '#'.$scroller_id.' div.imgbox, #'.$scroller_id.' div.vid_icon{
	width: '.$thumbwidth.'px;
	height: '.$thumbheight.'px;
	border: 0;
	overflow: hidden;
	position: relative;
	border-collapse: separate;
}
#'.$scroller_id.' div.imgbox a img{
	width: '.$trimwidth.'px !important;
	height: '.$trimheight.'px !important;
	position: relative;
	margin-top: -'.$trimtopmargin.'px;
	margin-left: -'.$trimleftmargin.'px;
	border: 0;
}
#'.$scroller_id.' div.imgbox a{
	width: '.$trimwidth.'px !important;
	height: '.$trimheight.'px !important;
}
';

}

//--------------------------
$navblack = $rspath . 'images/black/nav_s.png';
$navwhite = $rspath . 'images/nav_s.png';
$vidiconsrc =  $rspath . 'images/vid_icon.png';

$dyncss .= '#'.$scroller_id.' div.imgbox{';
if ($tpltheme == 'black'){ 
	$dyncss .= '
	-moz-box-shadow: 0px 0px 5px #111;
	-webkit-box-shadow: 0px 0px 5px #111;
	box-shadow: 0px 0px 5px #111;';
}else{ 
	$dyncss .= '
	-moz-box-shadow: 0px 0px 5px #666;
	-webkit-box-shadow: 0px 0px 5px #666;
	box-shadow: 0px 0px 5px #666;';
} 
	$dyncss .= '
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px;	
}
#'.$scroller_id.' div.imgbox:hover{';
if ($tpltheme == 'black'){ 
	$dyncss .= '
	-moz-box-shadow: 0px 0px 10px #666;
	-webkit-box-shadow: 0px 0px 10px #666;
	box-shadow: 0px 0px 10px #666;';
}else{ 
	$dyncss .= '
	-moz-box-shadow: 0px 0px 10px #111;
	-webkit-box-shadow: 0px 0px 10px #111;
	box-shadow: 0px 0px 10px #111;';
} 
$dyncss .= '}
#'.$scroller_id.' div.imgbox img{
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px;
}
#'.$scroller_id.' div.vid_icon{
	background-position: 50% 50%;
	background-repeat: no-repeat;
	background-image: url("'.$vidiconsrc.'");
	z-index: 5;
	position: absolute;
	overflow: hidden;
	top:0px;
	left:0px;
	opacity: 0.5;
	-moz-opacity: 0.5;
	filter:alpha(opacity=50);
	cursor: pointer;
}
#'.$scroller_id.' div.vid_icon:hover{
	opacity: 0.9;
	-moz-opacity: 0.9;
	filter:alpha(opacity=90);
}';

if($liststyle == 'vertical' || $liststyle == 'horizontal'){
$dyncss .= '#'.$scroller_id.' div.imgbox{
	margin: 10px;
	float: left;
	clear: both;
}
#'.$scroller_id.' div.titlebelow{
	margin: 10px;
	clear: both;
}';

}
else if($liststyle == 'slidergrid' || $liststyle == 'slidergridv' || $liststyle == 'slidergridbox'){

$totcol = (float) count($videos) / (float) $gridrow;
$totcol_int = (int) ceil($totcol);
if($liststyle == 'slidergridv'){
	$boxwidthmult = $gridcol;
}
else{
	$boxwidthmult = $totcol_int;
}
$boxwidth = ($thumbwidth+$grid_margin_right+$grid_margin_right)*$boxwidthmult;
$maskwidth = ($thumbwidth+$grid_margin_right+$grid_margin_right)*$gridcol;
if($showtitlethumb == 'yes') $ttheight = $titlethumb_height;
else $ttheight = 0;
$maskheight = (($thumbwidth * 0.75)+$ttheight+$grid_margin_top+$grid_margin_top)*$gridrow;

$dyncss .='#'.$scroller_id.' div.boxsinglethumb{
	position:absolute;
	width:'.$boxwidth.'px;
}
#'.$scroller_id.' div.boxsinglethumb > div{
	display:block;
	float:left;
}
#'.$scroller_id.' div.masksinglethumb{
	position:relative;
	width:'.$maskwidth.'px;
	height:'.$maskheight.'px;
	overflow:hidden;
}
#'.$scroller_id.' div.imgbox a img{
	display:block;
}
#'.$scroller_id.' .prev1, .next1 {
	cursor: pointer;
}
#'.$scroller_id.' div.prev1, #'.$scroller_id.' div.next1{';
if ($tpltheme == 'black'){ 
	$dyncss .= '
	background-image: url("'.$navblack.'");';
}else{ 
	$dyncss .= '
	background-image: url("'.$navwhite.'");';
} 
if($liststyle == 'slidergridv'){

$dyncss .= '
	width: 66px;
	height: 40px;
	margin: 5px auto;
}
#'.$scroller_id.' div.prev1{
	background-position: -116px 0px;
}
#'.$scroller_id.' div.prev1:hover{
	background-position: -116px -41px;
}
#'.$scroller_id.' div.next1{
	background-position: -116px -80px;
}
#'.$scroller_id.' div.next1:hover{
	background-position: -116px -121px;
}';

}
else{
$dyncss .= 'width: 32px;
	height: 65px;
	margin: 5px;
}

#'.$scroller_id.' div.prev1{
	background-position: 0px -76px;
}
#'.$scroller_id.' div.prev1:hover{
	background-position: -51px -76px;
}
#'.$scroller_id.' div.next1{
	background-position: 0px 0px;
}
#'.$scroller_id.' div.next1:hover{
	background-position: -51px 0px;
}';
}

}
else if($liststyle == 'withdescription'){
$dyncss.='#'.$scroller_id.'{
	width: 100%;
}
#'.$scroller_id.' div.txtbox{
	margin-left: 15px;
}
#'.$scroller_id.' td.vtop{
	vertical-align: top;
}
#'.$scroller_id.' h3.videotitle{
	margin-top: 3px;
}
#'.$scroller_id.' div.videodecs{
	color: gray; 
}

#'.$scroller_id.' div.imgbox{
	margin: 5px;
}

#'.$scroller_id.' div.divider{
	height: 30px;
	width: 100%;
	height: 1px;
	background-color: #DDD;
	margin: 5px;
	position: relative;
	clear: both;
}';
}
if(isset($fromcomponent)){
    unset($fromcomponent);
    // pagination css
    $dyncss.='#tnt_pagination {
        display:block;
        text-align:left;
        height:22px;
        line-height:21px;
        clear:both;
        padding-top:3px;
        margin-top:10px;
        font-family:Arial, Helvetica, sans-serif;
        font-size:12px;
        font-weight:normal;
    }

    #tnt_pagination a:link, #tnt_pagination a:visited{
        padding:7px;
        padding-top:2px;
        padding-bottom:2px;
        border:1px solid #EBEBEB;
        margin-left:10px;
        text-decoration:none;
        background-color:#F5F5F5;
        color:#0072bc;
        width:22px;
        font-weight:normal;
    }

    #tnt_pagination a:hover {
        background-color:#DDEEFF;
        border:1px solid #BBDDFF;
        color:#0072BC;  
    }

    #tnt_pagination .active_tnt_link {
        padding:7px;
        padding-top:2px;
        padding-bottom:2px;
        border:1px solid #BBDDFF;
        margin-left:10px;
        text-decoration:none;
        background-color:#DDEEFF;
        color:#0072BC;
        cursor:default;
    }

    #tnt_pagination .disabled_tnt_pagination {
        padding:7px;
        padding-top:2px;
        padding-bottom:2px;
        border:1px solid #EBEBEB;
        margin-left:10px;
        text-decoration:none;
        background-color:#F5F5F5;
        color:#D7D7D7;
        cursor:default;
    }';
}
$doc = JFactory::getDocument();
$doc->addStyleDeclaration($dyncss);