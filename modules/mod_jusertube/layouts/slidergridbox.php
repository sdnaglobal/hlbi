<?php
/**
 * @package			JUserTube 
 * @version			6.1.0
 *
 * @author			Md. Afzal Hossain <afzal.csedu@gmail.com>
 * @link			http://www.srizon.com
 * @copyright		Copyright 2012 Md. Afzal Hossain All Rights Reserved
 * @license			http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
 // no direct access
defined( '_JEXEC' ) or die;
$hidecode = $hidebutton? ' style="display:none;"':'';
echo '<div class="jusertube"  id="'.$scroller_id.'"><table><tr><td colspan="3"><div id="vid'.$scroller_id.'"></div></td></tr><tr><td><div class="prev1"'.$hidecode.'></div></td><td><div class="masksinglethumb"><div class="boxsinglethumb">';
if($autoplay_l == 'yes'){
    $doautoplay = "&auto=1";
}
else{
    $doautoplay = "&auto=0";
}
if($GLOBALS['jqlibraryju'] == 'srizonjq') $fname = 'srizonjq';
else $fname = 'jQuery';
$i=0;
for($j=0;$j<$totcol_int;$j++){
    $width1 = $thumbwidth+$grid_margin_right+$grid_margin_right;
    echo '<div style="width:'.$width1.'px;">';
    for($k=0;$k<$gridrow;$k++){
        if($i>=count($videos)) continue;
        $video = $videos[$i++];
        echo '<div style="padding:'.$grid_margin_top.'px '.$grid_margin_right.'px;">';
        $link1 = JURI::base().'index.php?option=com_jusertube&view=videobox&rid='.$video['id'].'&'.$yorvuser .'='.$youtubeuser.$doautoplay.'&eh='.$eheight.'&ew='.$ewidth.'&st='.$showtitle.'&height='.$popupy.'&width='.$popupx;
        $onclickfunc = 'onclick="'.$fname.'(\'#vid'.$scroller_id.'\').load(\''.$link1.'\');"';
        $link = '<a href="javascript:void(0)" '.$onclickfunc.'>';
        if(!isset($initboxjs)){
		$link_n_auto = JURI::base().'index.php?option=com_jusertube&view=videobox&rid='.$video['id'].'&'.$yorvuser .'='.$youtubeuser.'&auto=0&eh='.$eheight.'&ew='.$ewidth.'&st='.$showtitle.'&height='.$popupy.'&width='.$popupx;
		$initboxjs = '<script type="text/javascript">'.$fname.'(\'#vid'.$scroller_id.'\').load(\''.$link_n_auto.'\');</script>';
	}
        $imgcode = str_replace("<img","<img alt=\"".$video['title']."\"",$video['img']);
        $imgcode = str_replace('alt=""','',$imgcode);
        if($vidicon == 'yes') $imgcode.='<div class="vid_icon"></div>';
        echo '<div class="imgbox">'.$link.$imgcode.'</a></div>';
        if($truncate_len!='' and strlen($video['title'])>$truncate_len){
            $video['title'] = substr($video['title'],0,$truncate_len).'...';
        }
        
        if($showtitlethumb == 'yes'){
            echo '<div class="titlebelow">'.$link.$video['title'].'</a></div>';
        }
        echo '</div>';
    }
    echo '</div>';
}
echo '</div></div></td><td><div class="next1"'.$hidecode.'></div></td></tr></table>'.$initboxjs.'</div>';
