<?php
/**
 * Zaragoza News Menu
 * Module Version 1.0.0 - Joomla! Version 1.6+, 2.5
 * Author: Ciro Artigot http://twitter.com/ciroartigot
 * http://aixeena.org
 * Copyright (c) 2011 Ciro Artigot. All Rights Reserved. 
 * License: GNU/GPL 2, http://www.gnu.org/licenses/gpl-2.0.html
 * This module is based on :
 		> @package		Joomla.Site
 		> @subpackage	mod_menu
 */

// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';
$document =& JFactory::getDocument();


$list	= modNewsMenuHelper::getList($params,1);
$list2	= modNewsMenuHelper::getList($params,2);
$list3	= modNewsMenuHelper::getList($params,3);

$app	= JFactory::getApplication();
$menu	= $app->getMenu();
$active	= $menu->getActive();
$active_id = isset($active) ? $active->id : $menu->getDefault()->id;
$path	= isset($active) ? $active->tree : array();
$showAll	= $params->get('showAllChildren');
$class_sfx	= htmlspecialchars($params->get('class_sfx'));
if($params->get('menu_style')!='custom') $document->addStyleSheet(JURI :: base().'modules/mod_newsmenu/css/'.$params->get('menu_style','blue').'.css');

if(count($list)) {
	require JModuleHelper::getLayoutPath('mod_newsmenu', $params->get('layout', 'default'));
} 
