<?php
/**
 * Zaragoza News Menu
 * Module Version 1.0.0 - Joomla! Version 1.6+, 2.5
 * Author: Ciro Artigot http://twitter.com/ciroartigot
 * http://aixeena.org
 * Copyright (c) 2011 Ciro Artigot. All Rights Reserved. 
 * License: GNU/GPL 2, http://www.gnu.org/licenses/gpl-2.0.html
 * This module is based on :
 		> @package		Joomla.Site
 		> @subpackage	mod_menu
 */

// no direct access
defined('_JEXEC') or die;
 ?>

<div class="newsmenu">

<div class="nivel1">
    <ul class="newsmenu-ul">
    <?php
    foreach ($list as $i => &$item) :
        $class = 'item-'.$item->id;
        if ($item->id == $active_id) {
            $class .= ' current';
        }
    
        if (in_array($item->id, $path)) {
            $class .= ' active';
        }
        elseif ($item->type == 'alias') {
            $aliasToId = $item->params->get('aliasoptions');
            if (count($path) > 0 && $aliasToId == $path[count($path)-1]) {
                $class .= ' active';
            }
            elseif (in_array($aliasToId, $path)) {
                $class .= ' alias-parent-active';
            }
        }
    
        if ($item->deeper) {
            $class .= ' deeper';
        }
    
        if ($item->parent) {
            $class .= ' parent';
        }
    
        if (!empty($class)) {
            $class = ' class="'.trim($class) .'"';
        }
    
        echo '<li'.$class.'>';
    
        // Render the menu item.
        switch ($item->type) :
            case 'separator':
            case 'url':
            case 'component':
                require JModuleHelper::getLayoutPath('mod_menu', 'default_'.$item->type);
                break;
    
            default:
                require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
                break;
        endswitch;
    
        // The next item is deeper.
        if ($item->deeper) {
            echo '<ul>';
        }
        // The next item is shallower.
        elseif ($item->shallower) {
            echo '</li>';
            echo str_repeat('</ul></li>', $item->level_diff);
        }
        // The next item is on the same level.
        else {
            echo '</li>';
        }
    endforeach;
    ?></ul>
    	<div class="clear"></div>
   </div>
   
   <?php if(count($list2)) { ?>
   
   <div class="nivel2">
   	<ul class="newsmenu-ul">
    <?php
	
	$list = $list2;
	
	
    foreach ($list as $i => &$item) :
        $class = 'item-'.$item->id;
        if ($item->id == $active_id) {
            $class .= ' current';
        }
    
        if (in_array($item->id, $path)) {
            $class .= ' active';
        }
        elseif ($item->type == 'alias') {
            $aliasToId = $item->params->get('aliasoptions');
            if (count($path) > 0 && $aliasToId == $path[count($path)-1]) {
                $class .= ' active';
            }
            elseif (in_array($aliasToId, $path)) {
                $class .= ' alias-parent-active';
            }
        }
    
        if ($item->deeper) {
            $class .= ' deeper';
        }
    
        if ($item->parent) {
            $class .= ' parent';
        }
    
        if (!empty($class)) {
            $class = ' class="'.trim($class) .'"';
        }
    
        echo '<li'.$class.'>';
    
        // Render the menu item.
        switch ($item->type) :
            case 'separator':
            case 'url':
            case 'component':
                require JModuleHelper::getLayoutPath('mod_menu', 'default_'.$item->type);
                break;
    
            default:
                require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
                break;
        endswitch;
    
        // The next item is deeper.
        if ($item->deeper) {
            echo '<ul>';
        }
        // The next item is shallower.
        elseif ($item->shallower) {
            echo '</li>';
            echo str_repeat('</ul></li>', $item->level_diff);
        }
        // The next item is on the same level.
        else {
            echo '</li>';
        }
    endforeach;
    ?></ul> 
    	<div class="clear"></div>
    </div>
    
      <?php 
	  
	  }
	  if(count($list3)) { ?>
    
    <div class="nivel3">
    <ul class="newsmenu-ul">
    <?php
	
	$list = $list3;
	
    foreach ($list as $i => &$item) :
        $class = 'item-'.$item->id;
        if ($item->id == $active_id) {
            $class .= ' current';
        }
    
        if (in_array($item->id, $path)) {
            $class .= ' active';
        }
        elseif ($item->type == 'alias') {
            $aliasToId = $item->params->get('aliasoptions');
            if (count($path) > 0 && $aliasToId == $path[count($path)-1]) {
                $class .= ' active';
            }
            elseif (in_array($aliasToId, $path)) {
                $class .= ' alias-parent-active';
            }
        }
    
        if ($item->deeper) {
            $class .= ' deeper';
        }
    
        if ($item->parent) {
            $class .= ' parent';
        }
    
        if (!empty($class)) {
            $class = ' class="'.trim($class) .'"';
        }
    
        echo '<li'.$class.'>';
    
        // Render the menu item.
        switch ($item->type) :
            case 'separator':
            case 'url':
            case 'component':
                require JModuleHelper::getLayoutPath('mod_menu', 'default_'.$item->type);
                break;
    
            default:
                require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
                break;
        endswitch;
    
        // The next item is deeper.
        if ($item->deeper) {
            echo '<ul>';
        }
        // The next item is shallower.
        elseif ($item->shallower) {
            echo '</li>';
            echo str_repeat('</ul></li>', $item->level_diff);
        }
        // The next item is on the same level.
        else {
            echo '</li>';
        }
    endforeach;
    ?></ul>
    	<div class="clear"></div>
    </div>
      <?php } ?>
    
    	<div class="clear"></div>
    </div>